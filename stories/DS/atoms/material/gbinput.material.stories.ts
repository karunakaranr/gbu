// also exported from '@storybook/angular' if you can deal with breaking changes in 6.1
import { CommonModule } from '@angular/common';
import { Story, Meta } from '@storybook/angular/types-6-0';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { GBFormControl } from '../../../../libs/uicore/src/lib/classes/GBFormControl';
import { GbInputComponent } from '../../../../libs/uifwk/uifwkmaterial/src/lib/gb-input/gb-input.component';

export default {
  title: 'Design systems/Atoms/Material/Input',
  component: GbInputComponent,
  argTypes: {
    backgroundColor: { control: 'color' },
  },

} as Meta;

const Template: Story<GbInputComponent> = (args: GbInputComponent) => ({
  component: GbInputComponent,
  props: args,
  moduleMetadata: {
    imports: [ReactiveFormsModule, CommonModule, FormsModule, MatInputModule]
  },
  template: '<form #f="ngForm"><app-gb-input></app-gb-input></form>'
});

export const Simple = Template.bind({});
Simple.args = {
  primary: true,
  label: 'Button',
  FormControl: new GBFormControl({})
};
