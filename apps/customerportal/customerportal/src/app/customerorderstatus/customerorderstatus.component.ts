import { Component, OnInit } from '@angular/core';
const ordercolumndata = require('./../../assets/ordercolumn.json')
const orderrowdata = require('./../../assets/orderrowdata.json')

@Component({
  selector: 'app-customerorderstatus',
  templateUrl: './customerorderstatus.component.html',
  styleUrls: ['./customerorderstatus.component.scss']
})
export class CustomerOrderStatusComponent implements OnInit {

  constructor() {
    console.log(ordercolumndata,orderrowdata);

   }

  ngOnInit(): void {
  }
  columnDefs = ordercolumndata
  rowData = orderrowdata

}
