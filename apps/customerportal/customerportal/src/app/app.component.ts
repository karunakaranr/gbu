import { Component } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { AuthState } from 'features/common/shared/stores/auth.state';
import { Observable } from 'rxjs';

@Component({
  selector: 'goodbooks-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'customerportal-customerportal';
  @Select(AuthState.AddDTO) loginDTOs$: Observable<any>;

  ngOnInit(): void {
    this.setStyle('--color','#6E3AB6');
  }

  setStyle(name,value) {
    document.documentElement.style.setProperty(name, value);
  }
}
