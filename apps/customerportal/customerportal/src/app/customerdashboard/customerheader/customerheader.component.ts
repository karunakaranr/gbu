import { Component, OnInit } from '@angular/core';
import { Store,Select } from '@ngxs/store';
import { Subscription,Observable } from 'rxjs';
import { GBHttpService } from '@goodbooks/gbcommon';
import { AuthState } from './../../../../../../../features/common/shared/stores/auth.state';
import { ILoginDTO } from './../../../../../../../features/common/shared/models/Login/Logindto';

@Component({
  selector: 'app-customerheader',
  templateUrl: './customerheader.component.html',
  styleUrls: ['./customerheader.component.scss']
})
export class CustomerHeaderComponent implements OnInit {
public viewtypes:any;
public Client:any;
// public CustomerID$:string;
@Select(AuthState.AddDTO) CustomerID$: Observable<ILoginDTO >;
// @Select(PortalState.CustomerID)
constructor(public HTTP: GBHttpService,public store:Store) { }
ngOnInit(): void {
    this.CustomerID$.subscribe(ID => {
      this.Client = ID;
      console.log("check",ID)
    })
}

}
