import { Component, Inject, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GBBasePageComponentNG, GBBasePageComponent, GBDataPageServiceNG, GBFormGroup } from '@goodbooks/uicore';
import { UserAccount } from './../../../../../libs/gbui/src/lib/models/addaccount.model'
import { GBBaseFormGroup } from '@goodbooks/uicore';
import { Versionservice } from './../../../../../Features/Common/shared/services/Login/Version.service';
import {addaccountservice} from './../Model/addaccount.service'
@Component({
    selector: 'app-login',
    templateUrl: './addaccount.component.html',
    styleUrls: ['./addaccount.component.scss'],
    encapsulation: ViewEncapsulation.Emulated,
    providers: [
        { provide: 'PageService', useClass: GBDataPageServiceNG },
        { provide: 'DataService', useClass: addaccountservice }
    ]
})
export class addaccountComponent extends GBBasePageComponentNG {
    form: GBBaseFormGroup;
    title = 'login';
    label = 'Login';
    thisConstructor() {

        this.form = this.gbps.fb.group({
            server: ['GoodBooks', Validators.required],
            domain: ['http://119.81.2.42:88', Validators.required],
            database: ['Unisoftgb4', Validators.required],
            username: ['', Validators.required],
            Password: ['', Validators.required]

        }) as GBBaseFormGroup;
    }
    public addaccount() {
        console.log("form value")
        console.log(this.form.value)
        
         const userdetails: UserAccount = this.form.value as UserAccount;
        (this.gbps.dataService as addaccountservice).adduseraccount(userdetails);
    }
    public clear() {
        this.form.reset();
    }
}

