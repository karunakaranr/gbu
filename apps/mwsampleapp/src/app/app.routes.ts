import { Routes } from '@angular/router';

import { HomeComponent } from '@src/app/home/home.component';
import { LoginComponent } from './login/login.component';
import {packComponent} from './pack/pack.component';
import {permissionComponent} from './permission/permission.component';
import {permissiontnsComponent} from './permission/permission.tns/permission.tns.component'
import{addaccountComponent} from './addaccount/addaccount.component'
import {attachmentComponent} from './attachment/attachment.component'
// export const routes: Routes = [
//   {
//       path: '',
//       redirectTo: '/home',
//       pathMatch: 'full',
//   },
//   {
//       path: 'home',
//       component: HomeComponent,
//   },
// ];


export const routes = [
  {
    path: 'homesample',
    component: HomeComponent,
},
{
  path: 'login',
  component: LoginComponent,
},
{
  path:'attach',
  component:attachmentComponent,
},
/*{
  path: 'main',
  component: packComponent,
},*/
/*{
  path: 'main',
  component: permissionComponent,
},*/
{
  path :'main',
  component : permissiontnsComponent,
},
{
  path:'addaccount',
  component:addaccountComponent
},
{
  path:'testform',
  loadChildren: () => import("./testform/testform.module").then((m) => m.TestFormModule),
},
{
  path: '',
  redirectTo: '/homesample',
  pathMatch: 'full',
},
//   {
//       path: "login",
//       loadChildren: () => import("./Adminmodule/components/Login/login.module").then(m => m.loginModule),
//       data: { title: "login" }
//   },
//   {
//       path: "home",
//       loadChildren: () => import("./Adminmodule/components/home/home.module").then(m => m.homeModule),
//       data: { title: "home" }
//   },
//   {
//       path: "punch",
//       loadChildren:  () => import("./Adminmodule/components/punch/punch.module").then(m => m.punchModule),
//       data: { title: "punch" }
//   },
//   {
//       path: "permission",
//       loadChildren:  () => import("./HRMS/components/permission/permission.module").then(m => m.permissionModule),
//       data: { title: "permission" }
//   },
//   {
//       path: "leave",
//       loadChildren:  () => import("./HRMS/components/leave/leave.module").then(m => m.leaveModule),
//       data: { title: "leave" }
//   },
//   {
//       path: "attendance",
//       loadChildren:  () => import("./HRMS/components/attendance/attendance.module").then(m => m.attendanceModule),
//       data: { title: "attendance" }
//   },
];

