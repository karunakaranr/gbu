// import { Observable } from "tns-core-modules/data/observable";

// export class PersonViewModel extends Observable {

//     constructor() {
//         super();
//         this.person = new Person("John", 23, "john@company.com", "New York", "5th Avenue", 11);
//     }

//     set person(value: Person) {
//         // this.set("_person", value);
//         this.person = value;
//     }

//     get person(): Person {
//         // return this.get("_person");
//         return this.person;
//     }
// }

export class Person {
    public name: string;
    public age: number;
    // public age1: number;
    public email: string;
    public city: string;
    public street: string;
    public streetNumber: number;
    public birthDate: Date;
    public married: boolean;
    public marriedDate: Date;

    constructor(name, age, email, city, street, streetNumber, married, marriedDate) {
        this.name = name;
        this.age = age;
        // this.age1 = age;
        this.email = email;
        this.city = city;
        this.street = street;
        this.streetNumber = streetNumber;
        this.birthDate = new Date(Date.now() - age);
        this.married = married;
        this.marriedDate = marriedDate;
    }
}

