import { ILoginDTO } from './app.modal';

export class AddLoginDTO {
    static readonly type = '[IloginDTO] Add';

    constructor(public DLoginDTO: ILoginDTO) {
    }
}
export class GetLoginDTO {
    static readonly type = '[ILoginDTO] Get';
    constructor(public DLoginDTO: ILoginDTO) {
    }
}

export class UpdateLogiDTO {
    static readonly type = '[ILoginDTO] Set';

    constructor(public DLoginDTO: ILoginDTO) {
    }
}

export class AddVersionDTO {
    static readonly type = '[IloginDTO] Addversion';

    constructor(public DVersionDTO: any) {
    }
}
export class Adduseraccount{
    static readonly type = '[IloginDTO] Adduser';

    constructor(public Duseraccount: any) {
    } 
}

export class ThemeOption {
    static readonly type= '[IThemeDto]Addtheme';
    constructor(public Dthemeoption:any){

    }
}