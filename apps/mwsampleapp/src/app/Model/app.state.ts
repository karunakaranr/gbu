import { Injectable } from '@angular/core';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { AddLoginDTO, GetLoginDTO, UpdateLogiDTO, ThemeOption, AddVersionDTO ,Adduseraccount} from './app.action';
import { ILoginDTO, IThemeDto } from './app.modal';
import {UserAccount} from './../../../../../libs/gbui/src/lib/models/addaccount.model'

export class AppStateModal {
    DLoginDTO: ILoginDTO;
    Getdto: ILoginDTO;
    DVersionDTO: any;
    Dthemeoption: IThemeDto;
    Duseraccount:UserAccount
}
@State<AppStateModal>({
    name: 'AppStore',
    defaults: {

        DLoginDTO: null,
        Getdto: null,
        DVersionDTO: [],
        Dthemeoption: null,
        Duseraccount:null

    }
})
@Injectable()
export class AppState {
    constructor() {
    }
    @Selector()
    static AddDTO(state: AppStateModal) {
        return state.DLoginDTO;
    }

    @Selector()
    static GetDto(state: AppStateModal) {
        return state.Getdto;
    }

    @Selector()
    static Getversion(state: AppStateModal) {
        return state.DVersionDTO;
    }

    @Selector()
    static Getuseraccount(state: AppStateModal) {
        return state.Duseraccount;
    }

    @Selector()

    static GetTheme(state: AppStateModal) {
        return state.Dthemeoption;
    }

    @Action(AddLoginDTO)
    add({ getState, setState }: StateContext<AppStateModal>, { DLoginDTO }: AddLoginDTO) {
        const state = getState();
        setState({
            ...state,
            DLoginDTO: DLoginDTO
        });
    }
    @Action(UpdateLogiDTO)
    update({ getState, setState }: StateContext<AppStateModal>, { DLoginDTO }: UpdateLogiDTO) {
        const state = getState();
        setState({
            ...state,
            Getdto: DLoginDTO
        });
    }
    @Action(GetLoginDTO)
    get({ getState, setState }: StateContext<AppStateModal>, { DLoginDTO }: GetLoginDTO) {
        const state = getState();
        setState({
            ...state,
            Getdto: DLoginDTO
        });
    }
    @Action(AddVersionDTO)
    getversion(context: StateContext<AppStateModal>, action: AddVersionDTO) {
        const state = context.getState();
        context.patchState({
            DVersionDTO: [...state.DVersionDTO, action.DVersionDTO]
        });
    }
    @Action(Adduseraccount)
    getuseraccount({ getState, setState }: StateContext<AppStateModal>, { Duseraccount }: Adduseraccount) {
        const state = getState();
        setState({
            ...state,
            Duseraccount: Duseraccount
        });
    }
    /*getuseraccount(context: StateContext<AppStateModal>, action: Adduseraccount) {
       
        const state = context.getState();
        context.patchState({
            Duseraccount: [...state.Duseraccount, action.Duseraccount]
        });
    }*/

    // @Action(ThemeOption)
    // addtheme(context: StateContext<AppStateModal>, action: ThemeOption) {
    //     const state = context.getState();
    //     context.patchState({
    //         Dthemeoption: [...state.Dthemeoption, action.Dthemeoption]
    //     });
    // }

    @Action(ThemeOption)
    addtheme({ getState, setState }: StateContext<AppStateModal>, { Dthemeoption }: ThemeOption) {
        const state = getState();
        setState({
            ...state,
            Dthemeoption: Dthemeoption
        });
    }
}
