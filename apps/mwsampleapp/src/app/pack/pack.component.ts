import { Component, OnInit } from '@angular/core';

// import { Loginservice } from '../../../../Goodbooks/Goodbooks/src/app/sharedscreens/Shared/services/Login/Login.service';
// import { ILoginDetail } from '../../../../Goodbooks/Goodbooks/src/app/sharedscreens/Shared/models/Login/LoginDetail';

import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from '@goodbooks/gbcommon';

import { AbstractDS, GBBaseDataService, GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService, GBFormGroup } from '@goodbooks/uicore';
import { Store,Select } from '@ngxs/store';

import { IPackserv } from './IPackserv';

import { PackservService } from './../../../../../Features/erpmodules/samplescreens/packs/Packserv/packserv.service';
import {AppState} from './../Model/app.state'
import { Observable, Subscription } from 'rxjs';




declare let require: any;
const urls = require('./urls.json');
@Component({
  selector: 'gbmw-pack',
  templateUrl: './pack.component.html',
  styleUrls: ['./pack.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'PackId'},
    { provide: 'url', useValue: urls.Pack},
    { provide: 'DataService', useClass: PackservService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url']},
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class packComponent extends GBBaseDataPageComponentWN<IPackserv> implements OnInit{
  title = 'Packserv';
  //form: GBBaseFormGroup;
    selectedModuleID: any
  @Select(AppState.AddDTO) selectedId: any;
  private formSubscription: Subscription = new Subscription();
  form: GBDataFormGroupWN<IPackserv> = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'SC002', {}, this.gbps.dataService);
  thisConstructor() {}

  // constructor(protected fb: FormBuilder, protected http: HttpClient, @Inject('DataService') public dataService: PackservService) {
  //   super(fb, http);
  // }
  async ngOnInit() {

   
    this.formSubscription.add(
      this.selectedId.subscribe( (res:String) => {
        console.log("versionid from home component  ")
        console.log(res);
        // if (res) {
        //   this.selectedModuleID = res;
        //   this.MenuList(this.selectedModuleID);
        // }
      })
    ); 
    
    this.gbps.dataService.getAll().subscribe(res => {
      this.gbps.dataService.moveFirst().subscribe(r => {
        this.form.patchValue(r);
      });
    });
  }
}

// export function getThisDataServiceWN(http: HttpClient, idField: string, url: string): GBBaseDataServiceWN<IPackserv> {
//   return new GBBaseDataServiceWN<IPackserv>(getThisDBDataService(http, url), idField);
// }
// export function getThisDataService(http: HttpClient, idField: string, url: string): GBBaseDataService<IPackserv> {
//   return new GBBaseDataServiceWN<IPackserv>(getThisDBDataService(http, url), idField);
// }
export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IPackserv> {
  const dbds: GBBaseDBDataService<IPackserv> = new GBBaseDBDataService<IPackserv>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IPackserv>, dbDataService: GBBaseDBDataService<IPackserv>, fb: FormBuilder, gbhttp:GBHttpService, activeroute:ActivatedRoute, router:Router): GBDataPageService<IPackserv> {
  return new GBDataPageService<IPackserv>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
