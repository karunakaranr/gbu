import { IPack } from '../../../models/IPack';
import { GBDataFormGroupStoreWN, GBDataFormGroupWN } from '@goodbooks/uicore';
import { HttpClient } from '@angular/common/http';
import { PackService } from '../../../services/Pack/pack.service';
import { Select, Store } from '@ngxs/store';
import { GBDataStateActionFactoryWN } from '@goodbooks/gbdata';
import { PackStateActionFactory } from '../../../stores/Pack/pack.actionfactory';
import { Observable } from 'rxjs';
import { PackState } from '../../../stores/Pack/pack.state';

export class PackFormgroup extends GBDataFormGroupWN<IPack> {
  constructor(http: HttpClient, dataService: PackService) {
    super(http, 'SC002', {}, dataService)
  }

  public clear() {
    super.clear();
    // .
    // or below code
    // .
    // this.reset();
  }

  public delete() {
    super.delete();
    // .
    // or below code
    // .
    // const pdata: IPack = this.value as IPack;
    // this.ds.deleteData(pdata[this.ds.IdField]);
    // this.reset();
  }

  public save() {
    super.save();
    // .
    // or below code
    // .
    // const pdata: IPack = this.value as IPack;
    // this.ds.saveData(pdata);
  }

  public moveFirst() {
    super.moveFirst();
    // .
    // or below code
    // .
    // this.ds.moveFirst().subscribe(r => {
    //   const pk: IPack = r;
    //   this.patchValue(pk);
    // });
  }

  public movePrev() {
    super.movePrev();
    // .
    // or below code
    // .
    // this.ds.movePrev().subscribe(r => {
    //   const pk: IPack = r;
    //   this.patchValue(pk);
    // });
  }

  public moveNext() {
    super.moveNext();
    // .
    // or below code
    // .
    // this.ds.moveNext().subscribe(r => {
    //   const pk: IPack = r;
    //   this.patchValue(pk);
    // });
  }

  public moveLast() {
    super.moveLast();
    // .
    // or below code
    // .
    // this.ds.moveLast().subscribe(r => {
    //   const pk: IPack = r;
    //   this.patchValue(pk);
    // });
  }
}

export class PackFormgroupStore extends GBDataFormGroupStoreWN<IPack> {
  constructor(http: HttpClient, store: Store) {
    super(http, 'SC002', {}, store, new PackStateActionFactory())
  }

  public clear() {
    super.clear();
    // .
    // or below code
    // .
    // this.store.dispatch(this.af.ClearData())
  }

  public delete() {
    super.delete();
    // .
    // or below code
    // .
    // const pdata: IPack = this.value as IPack;
    // this.ds.deleteData(pdata[this.ds.IdField]);
    // this.reset();
  }

  public save() {
    super.save();
    // .
    // or below code
    // .
    // const pdata: IPack = this.value as IPack;
    // this.ds.saveData(pdata);
  }

  public moveFirst() {
    super.moveFirst();
    // .
    // or below code
    // .
    // this.store.dispatch(this.af.MoveFirst());
  }

  public movePrev() {
    super.movePrev();
    // .
    // or below code
    // .
    // this.store.dispatch(this.af.movePrev());
  }

  public moveNext() {
    super.moveNext();
    // .
    // or below code
    // .
    // this.store.dispatch(this.af.moveNext());
  }

  public moveLast() {
    super.moveLast();
    // .
    // or below code
    // .
    // this.store.dispatch(this.af.moveLast());
  }
}

