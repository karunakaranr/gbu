export interface IPackserv {
  PackId: number;
  PackCode: string;
  PackName: string;
  PackConversionType: string;
  PackConversionFactor: number;
  PackStatus: number;
  PackVersion: number;
  sortorder: number;
  sourceType: string;
}

export interface IPack {
  PackId: number;

  // Specify other fields here...
  PackCode: string;
  PackName: string;
  PackConversionType: string;
  PackConversionFactor: number;

  PackStatus: number;
  PackVersion: number;
  sortorder: number;
  sourceType: string;
}



