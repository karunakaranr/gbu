import { HttpClient } from '@angular/common/http';
import { GBBaseDataService, GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { GBHttpService } from '@goodbooks/gbcommon';
import { IPack } from '../../../models/IPack';
import { PackService } from '../../../services/Pack/pack.service';
import { PackDBService } from '../../../dbservices/Pack/packdb.service';
import { Store } from '@ngxs/store';
import { FormBuilder } from '@angular/forms';
import { GBDataPageService, GBDataPageServiceWN } from '@goodbooks/uicore';
import { ActivatedRoute, Router } from '@angular/router';

export function getThisDBDataService(http: GBHttpService): GBBaseDBDataService<IPack> {
  return new PackDBService(http);
}
export function getThisDataServiceWN(dbDataService: GBBaseDBDataService<IPack>): GBBaseDataServiceWN<IPack> {
  return new PackService(dbDataService);
}
export function getThisDataService(dbDataService: GBBaseDBDataService<IPack>): GBBaseDataService<IPack> {
  return new PackService(dbDataService);
}
export function getThisGBPageService(store: Store, dataService: GBBaseDataService<IPack>, dbDataService: GBBaseDBDataService<IPack>, fb: FormBuilder, gbhttp:GBHttpService, activeroute:ActivatedRoute, router:Router): GBDataPageService<IPack> {
  return new GBDataPageService<IPack>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
export function getThisGBPageServiceWN(store: Store, dataService: GBBaseDataServiceWN<IPack>, dbDataService: GBBaseDBDataService<IPack>, fb: FormBuilder, gbhttp:GBHttpService, activeroute:ActivatedRoute, router:Router): GBDataPageServiceWN<IPack> {
  return new GBDataPageServiceWN<IPack>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
