import { Component } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { AuthState } from 'features/common/shared/stores/auth.state';
import { Observable } from 'rxjs';


@Component({
  selector: 'goodbooks-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent  {
  title = 'GoodBooks';
  deviceInfo= null;
  @Select(AuthState.AddDTO) loginDTOs$: Observable<any>;
  constructor(private store: Store){
  }
 public get loginDto() {
    let retvalue = null;
    let check=[] ;
    this.store.selectSnapshot((state) => {
      if (state.AuthStore.DLoginDTO != null)
        retvalue =JSON.stringify(state.AuthStore.DLoginDTO);
        check = JSON.parse(retvalue);
    });
    return retvalue;
  }
}
