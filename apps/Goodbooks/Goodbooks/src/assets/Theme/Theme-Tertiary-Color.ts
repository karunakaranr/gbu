export class ThemeTertiaryColor {
    public static Default = '#FFE2E7';
    public static Blue = '#2D6ADF';
    public static Pink = '#fd92a4';
    public static Red = '#f11d16';
    public static Aqua = '#16f1df';
    public static Yellow = '#ecf00c';
    public static Teal  = '#016f6f';
    public static Green  = '#008000';
}