import { Component, OnInit } from '@angular/core';
import { BrandService} from './../../../../../Features/erpmodules/inventory/services/brand/brand.service'
import { Observable } from 'rxjs';
import { GridProperties, GridSetting } from './../../../../../Features/erpmodules/inventory/models/IBrand'
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-brandlist',
  templateUrl: './brandlist.component.html',
  styleUrls: ['./brandlist.component.css']
})
export class BrandlistComponent implements OnInit {

  title = 'Brandlist'
  public rowData = [];
  public columnData = [];
  public defaultColDef = [];
  constructor(public service: BrandService, private http: HttpClient) { }

  ngOnInit(): void {
    this.brandlist();
  }
  
  public brandlist() {
    this.service.getAll().subscribe((res) => {
      this.rowData = res;
      this.getgridsetting();
    });
  }
  public getgridsetting() {
    this.gridSetting().subscribe((res) => {
      this.columnData = res;
      this.getgridproperties();
    });
  }
  public getgridproperties() {
    this.gridproperties().subscribe((res) => {
      this.defaultColDef = res;
    });
  }

  private grid_setting = "assets/data/brandlist.json";
  private grid_properties = "assets/data/grid-properties.json";
  gridSetting(): Observable<GridSetting[]> {
    return this.http.get<GridSetting[]>(this.grid_setting);
    // return this.http.get<GridSetting[]>(this.grid_setting);
  }
  gridproperties(): Observable<GridProperties[]> {
    return this.http.get<GridProperties[]>(this.grid_properties);
    // return this.http.get<GridProperties[]>(this.grid_properties);
  }

}
