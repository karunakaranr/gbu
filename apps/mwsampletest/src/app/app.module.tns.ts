import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptModule } from '@nativescript/angular';

import { AppRoutingModule } from '@src/app/app-routing.module';
import { AppComponent } from '@src/app/app.component';
import { HomeComponent } from '@src/app/home/home.component';

import { BarcelonaModule } from '@src/app/barcelona/barcelona.module';
import { BrandComponent } from '@src/app/brand/brand.component';
import { BrandlistComponent } from '@src/app/brandlist/brandlist.component';


import { HttpClientModule } from '@angular/common/http';
import { NgxsModule } from '@ngxs/store';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
import { NgxsLoggerPluginModule } from '@ngxs/logger-plugin';
import { NgxsStoragePluginModule, StorageOption } from '@ngxs/storage-plugin';
import { NgxsResetPluginModule } from 'ngxs-reset-plugin';
import { AppState } from './Model/app.state';
import { environment } from '../environments/environment';
import { NativeScriptFormsModule } from "@nativescript/angular";
import { GbcommonModule } from '@goodbooks/gbcommon';
import { ReactiveFormsModule } from '@angular/forms';
import { STORAGE_ENGINE } from '@ngxs/storage-plugin';
//import {InventoryModule} from "./../../../../Features/erpmodules/inventory/inventory.module"
//import { AngularFileUploaderModule } from "angular-file-uploader";
import { sqliteStorage } from './Model/storage'
//import { DropDownModule } from "nativescript-drop-down/angular";
//import {FeatureCommonmodule} from './../../../../Features/Common/featurecommon.module'
//(global as any).localStorage=new Storage()
import { NativeScriptCommonModule } from "@nativescript/angular";
//import { NativeScriptUIDataFormModule } from 'nativescript-ui-dataform/angular';

// Uncomment and add to NgModule imports if you need to use two-way binding and/or HTTP wrapper
// import { NativeScriptFormsModule, NativeScriptHttpClientModule } from '@nativescript/angular';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    BrandComponent,
    BrandlistComponent,
  ],
  imports: [
    NativeScriptModule,
    AppRoutingModule,
    BarcelonaModule,

    ReactiveFormsModule,
    NativeScriptFormsModule,
    NativeScriptCommonModule,
    //NativeScriptUIDataFormModule,
    // DropDownModule,
    // FeatureCommonmodule,
    HttpClientModule,
    NgxsModule.forRoot([AppState], { developmentMode: !environment.production }),
    GbcommonModule.forRoot(environment),
    NgxsReduxDevtoolsPluginModule.forRoot(),
    NgxsResetPluginModule.forRoot(),
    NgxsStoragePluginModule.forRoot(),
   
  ],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class AppModule { }
