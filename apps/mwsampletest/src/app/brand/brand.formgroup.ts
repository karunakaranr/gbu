
import {IBrand} from './../../../../../Features/erpmodules/inventory/models/IBrand'
import { GBDataFormGroupStoreWN, GBDataFormGroupWN} from './../../../../../libs/uicore/src/lib/classes/GBFormGroup'
import { HttpClient } from '@angular/common/http';
import {BrandService} from './../../../../../Features/erpmodules/inventory/services/brand/brand.service'
import { Select, Store } from '@ngxs/store';
import { BrandStateActionFactory } from './../../../../../Features/erpmodules/inventory/stores/brand/brand.actionfactory';

export class BrandFormgroup extends GBDataFormGroupWN<IBrand> {
  constructor(http: HttpClient, dataService: BrandService) {
    super(http, 'SC008', {}, dataService)
  }

  public clear() {
    super.clear();
    // .
    // or below code
    // .
    // this.reset();
  }

  public delete() {
    super.delete();
    // .
    // or below code
    // .
    // const pdata: IPacknone = this.value as IPacknone;
    // this.ds.deleteData(pdata[this.ds.IdField]);
    // this.reset();
  }

  public save() {
    super.save();
    // .
    // or below code
    // .
    // const pdata: IPacknone = this.value as IPacknone;
    // this.ds.saveData(pdata);
  }

  public moveFirst() {
    super.moveFirst();
    // .
    // or below code
    // .
    // this.ds.moveFirst().subscribe(r => {
    //   const pk: IPacknone = r;
    //   this.patchValue(pk);
    // });
  }

  public movePrev() {
    super.movePrev();
    // .
    // or below code
    // .
    // this.ds.movePrev().subscribe(r => {
    //   const pk: IPacknone = r;
    //   this.patchValue(pk);
    // });
  }

  public moveNext() {
    super.moveNext();
    // .
    // or below code
    // .
    // this.ds.moveNext().subscribe(r => {
    //   const pk: IPacknone = r;
    //   this.patchValue(pk);
    // });
  }

  public moveLast() {
    super.moveLast();
    // .
    // or below code
    // .
    // this.ds.moveLast().subscribe(r => {
    //   const pk: IPacknone = r;
    //   this.patchValue(pk);
    // });
  }
}
export class BrandFormgroupStore extends GBDataFormGroupStoreWN<IBrand> {
  constructor(http: HttpClient, store: Store) {
    super(http, 'SC008', {}, store, new BrandStateActionFactory())
  }

  public clear() {
    super.clear();
    // .
    // or below code
    // .
    // this.store.dispatch(this.af.ClearData())
  }

  public delete() {
    super.delete();
    // .
    // or below code
    // .
    // const pdata: IPack = this.value as IPack;
    // this.ds.deleteData(pdata[this.ds.IdField]);
    // this.reset();
  }

  public save() {
    super.save();
    // .
    // or below code
    // .
    // const pdata: IPack = this.value as IPack;
    // this.ds.saveData(pdata);
  }

  public moveFirst() {
    super.moveFirst();
    // .
    // or below code
    // .
    // this.store.dispatch(this.af.MoveFirst());
  }

  public movePrev() {
    super.movePrev();
    // .
    // or below code
    // .
    // this.store.dispatch(this.af.movePrev());
  }

  public moveNext() {
    super.moveNext();
    // .
    // or below code
    // .
    // this.store.dispatch(this.af.moveNext());
  }

  public moveLast() {
    super.moveLast();
    // .
    // or below code
    // .
    // this.store.dispatch(this.af.moveLast());
  }
}

