import { HttpClient } from '@angular/common/http';
import { GBBaseDataService, GBBaseDataServiceWN} from './../../../../../libs/gbdata/src/lib/services/gbbasedata.service'
import {GBBaseDBDataService} from './../../../../../libs/gbdata/src/lib/services/gbbasedbdata.service'
// import { GBBaseDataService, GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import {GBHttpService} from './../../../../../libs/gbcommon/src/lib/services/HTTPService/GBHttp.service';
import {GbToasterService} from './../../../../../libs/gbcommon/src/lib/services/toaster/gbtoaster.service';
import {IBrand} from './../../../../../Features/erpmodules/inventory/models/IBrand'
import { BrandService } from './../../../../../Features/erpmodules/inventory//services/brand/brand.service';
import { BrandDBService } from './../../../../../Features/erpmodules/inventory/dbservices/brand/branddb.service';
import { Store } from '@ngxs/store';
import { FormBuilder } from '@angular/forms';
import {GBDataPageService, GBDataPageServiceWN} from './../../../../../libs/uicore/src/lib/services/gbpage.service';
import { ActivatedRoute, Router } from '@angular/router';


export function getThisDBDataService(http: GBHttpService): GBBaseDBDataService<IBrand> {
  return new BrandDBService(http);
}
export function getThisDataServiceWN(dbDataService: GBBaseDBDataService<IBrand>): GBBaseDataServiceWN<IBrand> {
  return new BrandService(dbDataService);
}
export function getThisDataService(dbDataService: GBBaseDBDataService<IBrand>): GBBaseDataService<IBrand> {
  return new BrandService(dbDataService);
}
export function getThisGBPageService(store: Store, dataService: GBBaseDataService<IBrand>, dbDataService: GBBaseDBDataService<IBrand>, fb: FormBuilder, gbhttp:GBHttpService, activeroute:ActivatedRoute, router:Router,toaster:GbToasterService): GBDataPageService<IBrand> {
  return new GBDataPageService<IBrand>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
export function getThisGBPageServiceWN(store: Store, dataService: GBBaseDataServiceWN<IBrand>, dbDataService: GBBaseDBDataService<IBrand>, fb: FormBuilder, gbhttp:GBHttpService, activeroute:ActivatedRoute, router:Router,toaster:GbToasterService): GBDataPageServiceWN<IBrand> {
  return new GBDataPageServiceWN<IBrand>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
