import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'goodbooks-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  form: FormGroup;
  title = 'gb-gb';
  constructor(private fb: FormBuilder){
    this.form = this.fb.group({
      Server: ['GB4SC', Validators.required ],
      UserName: ['ADMIN', Validators.required ],
      Password: ['admin', Validators.required ]
    });
  }
}
