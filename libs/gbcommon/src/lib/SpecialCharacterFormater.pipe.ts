import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'SpecialCharacterFormatter'
})
export class SpecialCharacterPipe implements PipeTransform {
    transform(value:string): any {
        return decodeURIComponent(JSON.parse('"' + value.replace(/\"/g, '\\"') + '"'))
    }
}