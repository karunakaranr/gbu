import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';
import { Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { AuthState } from 'features/common/shared/stores/auth.state';
@Pipe({
    name: 'DateFormatter'
})
export class DateFormaterPipe implements PipeTransform {
    loginDTOs$: Observable<any>
    dateformat!: string;
    timeformat!:string;
    constructor(private store: Store){
      this.loginDTOs$ = this.store.select(AuthState.AddDTO);
    }
    transform(Date: any, time: string): any {
        this.loginDTOs$.subscribe(dto => {
            this.dateformat = dto.DateFormat
            this.timeformat = dto.TimeFormat
          })
        if (Date === null) return null;
        if (time == 'time'){
            if (typeof Date == 'string' && Date.substring(1,5) == 'Date'){
                var datePipe = new DatePipe("en-US");
                Date = datePipe.transform(JSON.parse(Date.replace("/Date(", "").replace(")/", "")), this.timeformat);
            }
        }
        else if(typeof Date == 'string' && Date.substring(1,5) == 'Date'){
            var datePipe = new DatePipe("en-US");
            Date = datePipe.transform(JSON.parse(Date.replace("/Date(", "").replace(")/", "")), this.dateformat)
        }
        return Date
    }
}