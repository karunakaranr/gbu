import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'Truncate',
})
export class TruncatePipe implements PipeTransform {
  public transform(
    value: string,
    length: number | null,
    suffix: string = '...'
  ): string {
    if (value == null) {
      return value;
    }
    const truncationLength: number = length ?? 40;
    return value.length > truncationLength
      ? `${value.slice(0, Math.ceil(truncationLength)).trim()}${suffix}`
      : value;
  }
}
