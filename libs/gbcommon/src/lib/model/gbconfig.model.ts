export interface IGBconfig{

   Livelog:{
      getcontentpath: string;
      drive: string;
      filepath: string;
      path: string;
      pocheck: string;
  }
   ServerDetails: {
      domain: string;
      proxylocation: string;
   }
    JasperDetails: {
      Server: string;
      Port: string;
      ContextName: string;
      Username: string;
      Password: string;
      pdfview: string;
  }
   WikiDetails: {
      Domain: string;
  }
   FayeDetails: {
      URL: string;
  }
}

