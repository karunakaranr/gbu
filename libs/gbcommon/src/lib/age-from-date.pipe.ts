import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'ageFromDate',
    pure: false
})
export class AgeFromDatePipe implements PipeTransform {

    transform(value: any, ...args: any[]) {
        // console.log(value)
        if (value) {
            // console.log(value)
            const date = new Date(value);
            const ageDifMs = Date.now() - date.getTime();
            const ageDate = new Date(ageDifMs)

            return Math.abs(ageDate.getUTCFullYear() - 1970);
        }

        return undefined;
    }
}
