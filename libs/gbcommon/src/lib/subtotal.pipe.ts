import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
  name: 'SubTotal'
})
export class SubTotalPipe implements PipeTransform {
  transform(rowdata: any, index: number, fieldgroup:string, fieldvalue:string): number {
    let subtotal : number = 0;
    for (let i = index; i < rowdata.length; i++) {
      if (i == rowdata.length-1 || rowdata[i][fieldgroup] == rowdata[i + 1][fieldgroup]) {
        subtotal += rowdata[i][fieldvalue];
      } else {
        subtotal += rowdata[i][fieldvalue];
        break;
      }
    }
    return subtotal;
  }
}