import { TestBed, waitForAsync } from '@angular/core/testing';
import { GbcommonModule } from './gbcommon.module';

describe('GbcommonModule', () => {
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [GbcommonModule],
    }).compileComponents();
  }));

  // TODO: Add real tests here.
  //
  // NB: This particular test does not do anything useful.
  //     It does NOT check for correct instantiation of the module.
  it('should have a module definition', () => {
    expect(GbcommonModule).toBeDefined();
  });
});
