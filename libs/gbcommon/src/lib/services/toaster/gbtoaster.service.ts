import { Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Select, Store } from '@ngxs/store';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { EventState } from '../../store/event.state';

@Injectable({
  providedIn: 'root'
})

export class GbToasterService {
  // @Select(EventState.Notify) notify$: Observable<any>;

  constructor(private store: Store, private toastr: ToastrService) {
    // this.notify$.subscribe(res => {
    //   const msg = res.Message;
    //   this.toastr.success(msg);
    // });
  }

  public showSuccess(message: string, title: string): void {
    this.toastr.success(message, title)
  }
  public showerror(message: string, title: string): void {
    this.toastr.error(message, title);
  }
}
