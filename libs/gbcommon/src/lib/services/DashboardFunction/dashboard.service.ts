import { Injectable } from '@angular/core';
const RandomColorPick = require('./RandomColorArray.json');

@Injectable({
  providedIn: 'root',
})
export class DashboardFunction {
  constructor() {}
  Random(number: number) {
    let randomarray = [];
    for (let i = 0; i < number; i++) {
      randomarray[i] = Math.floor(Math.random() * 10) + 1;
    }
    return randomarray;
  }
  RandomBackground(number: number) {
    let randomarray: number[] = [];
    let randomcolorarray = [];

    var min, max, n, p;

    min = 1;
    max = number;
    for (let i = 0; i < number; i++) {
      do {
        n = Math.floor(Math.random() * (max - min + 1)) + min;
        p = randomarray.includes(n);
        if (!p) {
          randomarray.push(n);
        }
      } while (p);
    }

    for (let j = 0; j < number; j++) {
      let temparandomnumber = randomarray[j].toString();
      if (temparandomnumber.length > 1) {
        let temparandomnumbersingle = parseInt(temparandomnumber.slice(-1));
        randomcolorarray.push(RandomColorPick[temparandomnumbersingle]);
      } else {
        randomcolorarray.push(RandomColorPick[randomarray[j]]);
      }
    }
    return randomcolorarray;
  }
}
