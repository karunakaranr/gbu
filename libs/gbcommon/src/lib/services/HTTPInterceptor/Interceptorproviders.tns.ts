import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { APIinterceptor } from './APIInterceptor.service';


export const interceptorProviders = [
  //  { provide: HTTP_INTERCEPTORS, useClass: SpinnerHttpInterceptor, multi: true },
  //  { provide: HTTP_INTERCEPTORS, useClass: NotifyErrorService, multi: true },
  { provide: HTTP_INTERCEPTORS, useClass: APIinterceptor, multi: true },
];
