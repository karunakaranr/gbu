import { HttpClient } from '@angular/common/http';
import { Inject, isDevMode } from '@angular/core';

import { GBHttpService } from './../../../../../libs/gbcommon/src/lib/services/HTTPService/GBHttp.service';

import { Observable, of } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { ISelectListCriteria } from '../interfaces/ISectionCriteriaList';
import { GBBaseDBDataService } from './gbbasedbdata.service';
import { EnumIntervalType, GBLocalStorageService, GBSessionStorageService } from './storage.service';
import { Select } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { LayoutState } from 'features/layout/store/layout.state';

export abstract class AbstractDS { }
export class GBBaseService implements AbstractDS { }

export class GBBaseDataService<T> implements GBBaseService {
  protected currentData: T;
  @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
  @Select(LayoutState.Movenxtpreid) movenextpreviousid$: Observable<any>;
  selectedid: any
  constructor(@Inject('DBDataService') protected dbDataService: GBBaseDBDataService<T>, public IdField: string,public Criteria?:any) {
  }

  saveData(criteria: T): Observable<any> {
    // console.log("inside saveData......")
    return this.dbDataService.postData(criteria);
  }

  getData(id: string | number): Observable<T> {
    // console.log("IDDD",id,"IDELELE",this.IdField)
    this.selectedid = id
    return this.dbDataService.getData(this.IdField, this.selectedid);
  }

  DynamicPostData(id: string | number,FieldName:string){
    this.selectedid = id
    return this.dbDataService.DynamicPostData(this.selectedid,FieldName,this.Criteria);
  }

  DgetData(id: string | number): Observable<T> {
    // console.log("IDDD",id,"IDELELE",this.IdField)
    this.selectedid = id
    return this.dbDataService.DgetData(this.selectedid);
  }

  public multiParameterGetData(id:string | number,FieldName:string, FieldValue: string): Observable<T>{
    this.selectedid = id
    return this.dbDataService.multiParameterGetData(this.IdField,this.selectedid,FieldName,FieldValue);
  }

  postData(id: string | number): Observable<T> {
    this.selectedid = id
    return this.dbDataService.postgetData(this.selectedid,this.IdField,this.Criteria);
  }

  deleteData(id: any): Observable<any> {
    return this.dbDataService.deleteData(this.IdField, id);
  }
}

export class GBBaseDataServiceWN<T> extends GBBaseDataService<T> {
  // pickListId: string;
  protected scl: ISelectListCriteria;
  protected plst: any;
  protected dataList: any[];
  curPos = 0;

  protected ls: GBLocalStorageService = new GBLocalStorageService();
  protected ss: GBSessionStorageService = new GBSessionStorageService();

  constructor(
    @Inject('DBDataService') public dbDataService: GBBaseDBDataService<T>,
    public IdField: string,
    public Criteria:any = '',
    private getAllCacheName: string = '',
    private bStoreInLocal: boolean = false,
    private expires: number | Date = 10,
    private numberIn: EnumIntervalType = EnumIntervalType.Seconds
  ) {
    super(dbDataService, IdField, Criteria);
    if (this.getAllCacheName === '') {
      this.getAllCacheName = IdField.replace('Id', '');
      this.expires = 10;
    }
  }

  getAll(): Observable<any> {
    return this.dbDataService.getList(this.scl).pipe(map(r => {
      this.dataList = r as any[];
      return this.dataList
    }));
  }
  deleteData(id: any): Observable<any> {
    return super.deleteData(id);
  }
  saveData(data: T): Observable<any> {
    return super.saveData(data);
  }

  moveFirst(): Observable<T> {
    if (this.dataList === undefined) {
      this.getAll().subscribe();
    }
    this.curPos = 0;
    return this.getData(this.dataList[this.curPos].Id);
  }

  movePrev(): Observable<any> {
    let data;
    return this.rowdatacommon$.pipe(
      switchMap(response => {
        this.movenextpreviousid$.subscribe(res => {
          if (res) {
            data = res
          }
        })
        this.dataList = response;
        this.curPos = data;
        if (this.curPos >= this.dataList.length) {
          this.curPos = this.dataList.length - 1;
        }
          return this.getData(this.dataList[this.curPos][this.IdField]);
      })
  );
  }

  moveNext(): Observable<T> {
    let data;
    return this.rowdatacommon$.pipe(
      switchMap(response => {
        this.movenextpreviousid$.subscribe(res => {
          if (res) {
            data = res
          }
        })
        this.dataList = response;
        this.curPos = data;
        if (this.curPos >= this.dataList.length) {
          this.curPos = this.dataList.length - 1;
        }
          return this.getData(this.dataList[this.curPos][this.IdField]);
      })
  );
    // if (this.dataList === null) {
    //   this.getAll().subscribe();
    // }
    // this.curPos = this.curPos + 1;
    // if (this.curPos >= this.dataList.length) {
    //   this.curPos = this.dataList.length - 1;
    // }
    // return this.getData(this.dataList[this.curPos].Id);
  }

  moveLast(): Observable<T> {
    if (this.dataList === null) {
      this.getAll().subscribe();
    }
    this.curPos = this.dataList.length - 1;
    return this.getData(this.dataList[this.curPos].Id);
  }
}

export class GBBaseListDataService<T> extends GBBaseDataService<T> {
  protected scl: ISelectListCriteria;
  protected plst: any;
  protected dataList: any[];
  constructor(
    @Inject('DBDataService') public dbDataService: GBBaseDBDataService<T>,
    public IdField: string,
  ) {
    super(dbDataService, IdField);
  }
  getAll(): Observable<any> {
    return this.dbDataService.getList(this.scl).pipe(map(r => {
      this.dataList = r as any[];
      return this.dataList
    }));
  }
}