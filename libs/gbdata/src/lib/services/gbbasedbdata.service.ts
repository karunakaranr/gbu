import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';
import { Inject, Injectable, Injector, isDevMode } from '@angular/core';
import { PicklistCriteria } from './../../../../../apps/Goodbooks/Goodbooks/src/assets/mockdata/_general/PicklistCriteria.json';
import {GBHttpService} from './../../../../../libs/gbcommon/src/lib/services/HTTPService/GBHttp.service';

// import { inflate } from 'pako';
import { Observable, of } from 'rxjs';
// import 'rxjs/add/operator/catch';
import { catchError, map } from 'rxjs/operators';
import { ISelectListCriteria } from '../interfaces/ISectionCriteriaList';
import { LibInjector } from './../lib-injector';
import { Select } from '@ngxs/store';
import { LayoutState } from 'features/layout/store/layout.state';
import { ILoginDTO } from 'features/common/shared/stores/auth.model';
import { AuthState } from 'features/common/shared/stores/auth.state';

@Injectable()
export class GBBaseDBService {
  // constructor(@Inject('env') private env) {}
}

@Injectable()
export  class GBBaseDBDataService<T> extends GBBaseDBService {
  // protected loginDTO: any;
  @Select(LayoutState.FormControlValue) formValue$: Observable<any>;
  @Select(AuthState.AddDTO) loginDTOs$: Observable<ILoginDTO>;
  endPoint: string;
  saveurl:string;
  deleteurl:string;
  IdField:string;
  PostingParameterFieldName:string;
  PostingParameterFieldValue:string;
  CopyFrom:string[]
  endPointList: string = 'SelectList';
  firstNumber: number = 1;
  maxResult: number = 50;
  previous:boolean;
  serviceType: number = 1; //1-Master, 2-Transcation
  bizTransactionTypeID:number;
  useAPIPagination:boolean=false;
  FormData: any;
  LoginDTO: ILoginDTO;
  // protected idField: string;
  constructor(protected http: GBHttpService) {
    super();
    //this.constructor.name gives the class name
    // Try and get from url assets using the class name
    // let a = {} as T;
    // if(this.endPoint === '') {
    // }
  }
get listEndPoint(){
  if (this.useAPIPagination){
return this.endPoint + '/' + this.endPointList + '/?FirstNumber=' + this.firstNumber + '&MaxResult=' + this.maxResult; 
}
else{
  return this.endPoint + '/' + this.endPointList; 
}
}
  get endpoint() {
    return this.endPoint
    // let apiURL = '';
    // if (LibInjector) {
    //   const env = LibInjector.get('env');
    //   apiURL = env.apiurl;
    // }
    // if (isDevMode()) {
    //   return apiURL + '/prox/' + this.endPoint;
    // }
    // else {
    //   return apiURL + this.endPoint;
    // }
  }
  putData(data: T) {
    const url = this.endpoint;
    return this.http.httpput(url, data);
  }

  postgetData(FieldValue: T, IdField:string, Criteria:any) {
    this.formValue$.subscribe(Res => {
      this.FormData = Res
    })
    this.loginDTOs$.subscribe(dto => {
      this.LoginDTO = dto;
  })
    const url = this.endpoint;
    let criteria
    if(Criteria == ''){
      criteria = {
        "SectionCriteriaList": [{
                "SectionId": 0,
                "AttributesCriteriaList": [{
                        "FieldName": IdField,
                        "OperationType": 1,
                        "FieldValue": FieldValue,
                        "InArray": null,
                        "JoinType": 0
                    }
                ],
                "OperationType": 2
            }
        ]
      }
    } else {
      let PostCriteriaValue = PicklistCriteria[Criteria]
      criteria = {
        "SectionCriteriaList": [{
            "SectionId": 0,
            "AttributesCriteriaList": []
          }
        ]
      }
      for(let data of PostCriteriaValue){
        if(data[3] == 'PeriodId'){
          criteria.SectionCriteriaList[0].AttributesCriteriaList.push(
            {
                FieldName: data[0],
                OperationType: data[1],
                FieldValue:this.LoginDTO.WorkPeriodId,
                InArray: null,
                JoinType: 2,
            }
          )
        } else if(data[3] == 'OrganizationId'){
          criteria.SectionCriteriaList[0].AttributesCriteriaList.push(
            {
                FieldName: data[0],
                OperationType: data[1],
                FieldValue:this.LoginDTO.WorkOUId,
                InArray: null,
                JoinType: 2,
            }
          )
        } else if(data[3] == 'PickListId' ){
          criteria.SectionCriteriaList[0].AttributesCriteriaList.push(
            {
                FieldName: data[0],
                OperationType: data[1],
                FieldValue: FieldValue,
                InArray: null,
                JoinType: 2,
            }
          )
        } else {
          criteria.SectionCriteriaList[0].AttributesCriteriaList.push(
            {
                FieldName: data[0],
                OperationType: data[1],
                FieldValue: this.FormData[data[3]],
                InArray: null,
                JoinType: 2,
            }
          )
          
        }
      }
    }
    return this.http.httppost(url, criteria);
  }
  postData(data: T) {
    const url = this.endpoint;  
    return this.http.httppost(url, data);
  }
  getData(idField: string, id: string): Observable<T> {
    const url = this.endpoint + '?' + idField + '=' + id;
    return this.http.httpget(url).pipe(
      map((r) => {
        const pd: T = r;
        return pd;
      })
    );
  }

  DgetData(id: string): Observable<T> {
    const url = this.endpoint + id;
    return this.http.httpget(url).pipe(
      map((r) => {
        const pd: T = r;
        return pd;
      })
    );
  } 

  DynamicPostData(FieldValue: string,FieldName:string, Criteria:any): Observable<T> {
    this.formValue$.subscribe(Res => {
      this.FormData = Res
    })
    this.loginDTOs$.subscribe(dto => {
      this.LoginDTO = dto;
    })
    const url = this.endpoint;
    let criteria
    let PostCriteriaValue = PicklistCriteria[Criteria]
      criteria = {
        "SectionCriteriaList": [{
            "SectionId": 0,
            "AttributesCriteriaList": []
          }
        ]
      }
      for(let data of PostCriteriaValue){
        if(data[3] == 'PeriodId'){
          criteria.SectionCriteriaList[0].AttributesCriteriaList.push(
            {
                FieldName: data[0],
                OperationType: data[1],
                FieldValue:this.LoginDTO.WorkPeriodId,
                InArray: null,
                JoinType: 2,
            }
          )
        } else if(data[3] == 'OrganizationId'){
          criteria.SectionCriteriaList[0].AttributesCriteriaList.push(
            {
                FieldName: data[0],
                OperationType: data[1],
                FieldValue:this.LoginDTO.WorkOUId,
                InArray: null,
                JoinType: 2,
            }
          )
        } else if(data[3] == 'PickListId' ){
          criteria.SectionCriteriaList[0].AttributesCriteriaList.push(
            {
                FieldName: FieldName,
                OperationType: data[1],
                FieldValue: FieldValue,
                InArray: null,
                JoinType: 2,
            }
          )
        } else {
          criteria.SectionCriteriaList[0].AttributesCriteriaList.push(
            {
                FieldName: data[0],
                OperationType: data[1],
                FieldValue: this.FormData[data[3]],
                InArray: null,
                JoinType: 2,
            }
          )
          
        }
      }
      return this.http.httppost(url, criteria);
  } 

  multiParameterGetData(idField: string,id:string,FieldName:string,FieldValue:string): Observable<T> {
    this.formValue$.subscribe(Res => {
      this.FormData = Res
      console.log("FormData:",this.FormData)
    })
    let PostingParameterFieldName = FieldName.split(',')
    let PostingParameterFieldValue = FieldValue.split(',')
    let url = this.endpoint + '?' + idField + '=' + id;
    for(let i=0; i< PostingParameterFieldName.length ; i++ ){
      url = url + '&' + PostingParameterFieldName[i] + '=' + this.FormData[PostingParameterFieldValue[i]]
    }
    console.log("Posting URL:",url)
    return this.http.httpget(url).pipe(
      map((r) => {
        const pd: T = r;
        return pd;
      })
    );
  }

  deleteData(idField: string, id: string): Observable<any> {
    const url = this.endpoint + '?' + idField + '=' + id;
    return this.http.httpdelete(url);
  }

  getList(scl: ISelectListCriteria): Observable<any> {
    let dataList: any[];
    // if (this.getAllCacheName !== '') {
    //   if (this.bStoreInLocal) {
    //     dataList = JSON.parse(this.ls.getItem(this.getAllCacheName));
    //   }
    //   else {
    //     dataList = JSON.parse(this.ss.getItem(this.getAllCacheName));
    //   }

    //   if (dataList !== null) {
    //     return of(dataList);
    //   }
    // }
    return this.http.httppost(this.listEndPoint, scl).pipe(map(r => {
      dataList = r as any[];
      // if (this.getAllCacheName !== '') {
      //   if (this.bStoreInLocal) {
      //     if (this.expires === null) {
      //       this.ls.setItem(this.getAllCacheName, JSON.stringify(this.dataList), 1, EnumIntervalType.Days);
      //     }
      //     else {
      //       this.ls.setItem(this.getAllCacheName, JSON.stringify(this.dataList), this.expires, this.numberIn);
      //     }
      //   }
      //   else {
      //     if (this.expires === null) {
      //       this.ss.setItem(this.getAllCacheName, JSON.stringify(this.dataList), 1, EnumIntervalType.Days);
      //     }
      //     else {
      //       this.ss.setItem(this.getAllCacheName, JSON.stringify(this.dataList), this.expires, this.numberIn);
      //     }
      //   }
      // }
      return dataList;
    }));
  }
}

// @Injectable()
// export abstract class GBBaseMasterDBDataService<T> extends GBBaseDBDataService<T> {

// }

// @Injectable()
// export abstract class GBBaseTransactionDBDataService<T> extends GBBaseDBDataService<T> {
// abstract bizTransactionTypeID:Number;
// }