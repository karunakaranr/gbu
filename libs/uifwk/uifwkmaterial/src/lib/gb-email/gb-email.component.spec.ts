import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { GbEmailComponent } from './gb-email.component';

describe('GbEmailComponent', () => {
  let component: GbEmailComponent;
  let fixture: ComponentFixture<GbEmailComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ GbEmailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GbEmailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
