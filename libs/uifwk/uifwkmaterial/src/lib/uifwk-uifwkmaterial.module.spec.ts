import { TestBed, waitForAsync } from '@angular/core/testing';
import { UifwkUifwkmaterialModule } from './uifwk-uifwkmaterial.module';

describe('UifwkUifwkmaterialModule', () => {
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [UifwkUifwkmaterialModule],
    }).compileComponents();
  }));

  // TODO: Add real tests here.
  //
  // NB: This particular test does not do anything useful.
  //     It does NOT check for correct instantiation of the module.
  it('should have a module definition', () => {
    expect(UifwkUifwkmaterialModule).toBeDefined();
  });
});
