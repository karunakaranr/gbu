import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Router } from '@angular/router';
import { GbFormGridComponent } from './gb-formgrid.component';
import { MatSelectModule } from '@angular/material/select';
import { MatRadioModule } from '@angular/material/radio';
import { ReactiveFormsModule } from '@angular/forms';
import { TranslocoRootModule } from './../../../../../../features/transloco/transloco-root.module'
import { EntitylookupModule } from './../../../../../../features/entitylookup/entitylookup.module';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatIconModule } from '@angular/material/icon';
import { FormsModule } from '@angular/forms';
import { NgxMaskModule, IConfig } from 'ngx-mask'

const maskConfig: Partial<IConfig> = {
  validation: false,
};
@NgModule({
  declarations: [GbFormGridComponent],
  imports: [
    CommonModule, 
    FormsModule, 
    MatCheckboxModule, 
    MatIconModule, 
    MatNativeDateModule, 
    MatDatepickerModule, 
    RouterModule, 
    MatRadioModule, 
    ReactiveFormsModule, 
    TranslocoRootModule, 
    MatSelectModule, 
    EntitylookupModule,
    NgxMaskModule.forRoot(maskConfig),
  ],
  exports: [GbFormGridComponent],
  entryComponents: [],

})
export class GbFormGridModule { }
