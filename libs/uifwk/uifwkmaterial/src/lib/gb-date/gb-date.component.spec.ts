import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { GbDateComponent } from './gb-date.component';

describe('GbDateComponent', () => {
  let component: GbDateComponent;
  let fixture: ComponentFixture<GbDateComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ GbDateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GbDateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
