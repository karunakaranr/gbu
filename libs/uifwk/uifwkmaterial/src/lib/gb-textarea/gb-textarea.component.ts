import { Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';

import { GBBaseUIFCComponent } from '@goodbooks/uicore';
import { Select } from '@ngxs/store';
import { LayoutState } from 'features/layout/store/layout.state';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-gb-textarea',
  templateUrl: './gb-textarea.component.html',
  styleUrls: ['./gb-textarea.component.scss']
})
export class GbTextareaComponent  extends GBBaseUIFCComponent {
@Input() rows: string;
@Input() minLen: number;
@Input() maxLen: number;
@Input() floatLabel; // 'always' | 'never' | 'auto' = 'always';
@Input() appearance; // 'legacy' | 'standard' | 'fill' | 'outline' = 'standard';
@Input() HtmlType = 'text';
@Input() HtmlPattern = '';
@Output() InputOutPut: EventEmitter<any> = new EventEmitter<string>();
@Select(LayoutState.FormEdit) edit$: Observable<any>;
  isreadable:boolean = false;
  @ViewChild('input') input: ElementRef;
  isFocused: boolean = false;
  // public ReadOnly = true;
  ngOnInit(): void {
    this.edit$.subscribe(res => {
      this.isreadable = res;
    })
  }

  onKeyDown(e: any): boolean {
    if ([8, 9, 27, 13, 46].indexOf(e.keyCode) !== -1 ||
      // Allow: Ctrl+A
      (e.keyCode === 65 && (e.ctrlKey || e.metaKey)) ||
      // Allow: Ctrl+C
      (e.keyCode === 67 && (e.ctrlKey || e.metaKey)) ||
      // Allow: Ctrl+V
      (e.keyCode === 86 && (e.ctrlKey || e.metaKey)) ||
      // Allow: Ctrl+X
      (e.keyCode === 88 && (e.ctrlKey || e.metaKey)) ||
      // Allow: home, end, left, right
      (e.keyCode >= 35 && e.keyCode <= 39)) {
      // let it happen, don't do anything
      return true;
    } else {
      return false
    }
  }

  validateInput(event: KeyboardEvent): void {
    const inputElement = event.target as HTMLInputElement;
    if(this.gbControl.field.Type.toLowerCase() == 'number'){
      inputElement.value = inputElement.value.replace(/[^0-9]/g, ''); // Remove non-numeric characters
    }
    this.InputOutPut.emit(inputElement.value)
  }
}
