import { Component, OnInit } from '@angular/core';

import { GBBaseUIFGComponent } from '@goodbooks/uicore';



@Component({
  selector: 'app-gb-phone',
  templateUrl: './gb-phone.component.html',
  styleUrls: ['./gb-phone.component.scss']
})
export class GbPhoneComponent extends GBBaseUIFGComponent implements OnInit {
  private regex: any;
  ngOnInit() {
    this.regex = this.gbFormGroup.controls;
    if (this.regex === '') {
      this.Field.ValidRegEx = '^((\\+91-?)|0)?[0-9]{10}$';
    }
  }
}
