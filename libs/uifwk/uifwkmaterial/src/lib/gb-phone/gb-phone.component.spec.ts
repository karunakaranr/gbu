import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { GbPhoneComponent } from './gb-phone.component';

describe('GbPhoneComponent', () => {
  let component: GbPhoneComponent;
  let fixture: ComponentFixture<GbPhoneComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ GbPhoneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GbPhoneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
