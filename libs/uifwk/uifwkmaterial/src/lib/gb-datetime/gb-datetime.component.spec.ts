import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { GbDateTimeComponent } from './gb-datetime.component';

describe('GbDateComponent', () => {
  let component: GbDateTimeComponent;
  let fixture: ComponentFixture<GbDateTimeComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ GbDateTimeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GbDateTimeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
