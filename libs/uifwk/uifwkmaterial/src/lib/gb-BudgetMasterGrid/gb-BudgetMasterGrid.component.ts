import { ChangeDetectionStrategy, Component, Input, OnChanges, OnInit, SimpleChanges, ElementRef, ViewChild, EventEmitter, Output } from '@angular/core';
import { FormArray, FormGroup } from '@angular/forms';
import { GBBaseUIFCComponent } from '@goodbooks/uicore';
import { Select } from '@ngxs/store';
import { LayoutState } from 'features/layout/store/layout.state';
import { Observable } from 'rxjs';
import { DialogBoxComponent } from 'features/erpmodules/samplescreens/screens/DialogBox/DialogBox.component';
import { IPickListDetail, IPicklistEmitValue } from 'libs/uicore/src/lib/interfaces/ibasefield';
import { DatePipe } from '@angular/common';
import { CostingCharges } from './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/CostingCharges.json'
import { ErectionCharges } from './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/ErectionCharges.json'
import { ChargeCostDetailArray } from './ChargeCostDetailArray.json'
import { ErectionCostDetailArray } from './ErectionCostDetailArray.json'
@Component({
  selector: 'app-gb-BudgetMasterGrid',
  templateUrl: './gb-BudgetMasterGrid.component.html',
  styleUrls: ['./gb-BudgetMasterGrid.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})

export class GbBudgetMasterGridComponent extends GBBaseUIFCComponent implements OnInit, OnChanges {
  // @ViewChild('datepicker', { static: true }) datepicker: ElementRef;
  @Output() TotalEmit: EventEmitter<any> = new EventEmitter<string>();
  @Output() GridOutPut: EventEmitter<any> = new EventEmitter<string>();
  @Output() LightBoxOutPut: EventEmitter<any> = new EventEmitter<string>();
  @Output() FieldChange: EventEmitter<any> = new EventEmitter<string>();
  @Select(LayoutState.FormEdit) edit$: Observable<any>;
  @Select(LayoutState.Resetformvalue) resetform$: Observable<any>;
  @ViewChild('tableRow') tableRows: ElementRef[];
  @Input() ArrayDetail: number | string;
  @Input() ArrayValues;
  changevalue:boolean = false
  isreadable: boolean = false;
  CostCharge: FormGroup;
  CostChargeitems: FormArray;
  orderForm: FormGroup;
  items: FormArray;
  selectedRowIndex: number = 0;
  AddrowNeeded: boolean = true;
  DeleteRowNeeded: boolean = true;
  CostingChargeJSON = CostingCharges
  CostingErectionJSON = ErectionCharges
  CostingChargeWidth: number = 0
  CostingErectionWidth: number = 0
  ChargeCostDetailArray = ChargeCostDetailArray
  ErectionCostDetailArray = ErectionCostDetailArray
  ChargeCostSLNO = 0
  flag: boolean = true;
  LevelHide : number[] = []
  LevelDefault='All';
  LevelArray :string[] = ['All'];
  PurchaseCost : boolean = false;
  ChargeCost: boolean = false;
  PurchaseIndex: number;
  ChargeContent: any;
  ChargeChangeValue: any;
  ProcessCost: boolean;
  ProcessIndex: number;
  LevelIndex:number;
  LevelSubIndex:number
  ngOnChanges(changes: SimpleChanges) {
    if (this.gbControl.field.IsAddrowRequired != undefined) {
      this.AddrowNeeded = false
    }
    if (this.gbControl.field.IsDeleterowRequired != undefined) {
      this.DeleteRowNeeded = false
    }
    if (changes.ArrayDetail) {
      this.selectedRowIndex = 0
      this.orderForm = new FormGroup({
        items: new FormArray([])
      });
      this.items = this.orderForm.get('items') as FormArray;
      if (this.gbControl.value != null) {
        this.patchFormArray(this.gbControl.value);
        this.FormCalculationPatch();
        this.FormSubTotal();
        this.value = this.items.value;
        this.GridOutPut.emit(this.value);
        // for(let i=0; i<this.gbControl.value.length;i++){
        //   console.log("CostingChargesArray Detail:",this.orderForm.get('items'))
        //   this.items.push(this.formBuilder.group(this.gbControl.value[i]));
        //   const itemFormGroup = this.items.at(i);
        //   itemFormGroup.get('COSTINGCHARGESArray')?.setValue(ChargeCostDetailArray);
        //   let COSTINGCHARGESArray = itemFormGroup.get('COSTINGCHARGESArray') as FormArray
        //   // for(let j=0;j<ChargeCostDetailArray.length;j++){
        //   //   console.log("COSTINGCHARGESArray:",itemFormGroup.get('COSTINGCHARGESArray')?.value)
        //   //   COSTINGCHARGESArray.push(this.formBuilder.group(ChargeCostDetailArray[j]))
        //   // }
        //   console.log("COSTINGCHARGESArray:",itemFormGroup.get('COSTINGCHARGESArray'))
        //   if(this.gbControl.field.SLNOField != undefined){
        //     itemFormGroup.get(this.gbControl.field.SLNOField)?.setValue(i+1);
        //   }
        // }
      }
    }
  }
  ngOnInit(): void {
    this.gbControl.field.Grid?.sort((a, b) => (a['SNo'] < b['SNo'] ? -1 : 1));
    this.CostingChargeJSON.sort((a, b) => (a['SNo'] < b['SNo'] ? -1 : 1))
    this.CostingChargeWidth = this.CostingChargeJSON.reduce((accumulator, currentValue) => accumulator + currentValue.width, 0);
    this.CostingErectionWidth = this.CostingErectionJSON.reduce((accumulator, currentValue) => accumulator + currentValue.width, 0);
    this.edit$.subscribe((res: boolean) => {
      this.isreadable = res;
    })
    this.resetform$.subscribe((res: boolean) => {
      this.selectedRowIndex = 0
      this.orderForm = new FormGroup({
        items: new FormArray([])
      });
      this.items = this.orderForm.get('items') as FormArray;
      if (this.gbControl.value != null) {
        this.patchFormArray(this.gbControl.value);
        this.FormCalculationPatch();
        this.FormSubTotal();
        // for(let i=0; i<this.gbControl.value.length;i++){
        //   console.log("CostingChargesArray Detail:",this.orderForm.get('items')?.value)
        //   this.items.push(this.formBuilder.group(this.gbControl.value[i]));
        //   const itemFormGroup = this.items.at(i);
        //   // itemFormGroup.get('COSTINGCHARGESArray')?.setValue([]);
        //   let COSTINGCHARGESArray = itemFormGroup.get('COSTINGCHARGESArray') as FormArray
        //   for(let j=0;j<ChargeCostDetailArray.length;j++){
        //     COSTINGCHARGESArray.push(this.formBuilder.group(ChargeCostDetailArray[j]))
        //   }
        //   console.log("COSTINGCHARGESArray:",itemFormGroup.get('COSTINGCHARGESArray')?.value)
        //   if(this.gbControl.field.SLNOField != undefined){
        //     itemFormGroup.get(this.gbControl.field.SLNOField)?.setValue(i+1);
        //   }
        // }
      }
    })
  }

  delay(milliseconds: number) {
    return new Promise(resolve => setTimeout(resolve, milliseconds));
  }

  public UpdateJ() {
    this.ChargeCostSLNO = this.ChargeCostSLNO + 1;
  }

  public ResetJ() {
    this.ChargeCostSLNO = 0;
  }



  public initItem(itemData: any, index: number, LevelIndex: number, LevelSubIndex: number): FormGroup {
    let ItemData = JSON.parse(JSON.stringify(itemData))
    // const itemFormGroup = itemsFormArray.at(index);
    //   if(this.gbControl.field.SLNOField != undefined){
    //     itemFormGroup.get(this.gbControl.field.SLNOField)?.setValue(index+1);
    //     console.log(this.gbControl.field.SLNOField,":",itemFormGroup.get(this.gbControl.field.SLNOField)?.value)
    //   }
    if(LevelSubIndex == 0){
      // console.log("LevelSubIndex111:",LevelSubIndex)
      ItemData[this.gbControl.field.SLNOField] = LevelIndex
    } else {
      ItemData[this.gbControl.field.SLNOField] = LevelIndex + '.' + LevelSubIndex
    }
    // ItemData[this.gbControl.field.SLNOField] = index + 1
    ItemData.COSTINGCHARGESArray = this.patchSubItems()
    ItemData.COSTINGERECTIONArray = this.patchSubErectionItems()
    ItemData.PURCHASECOSTARRAY = this.patchSubPurchaseItems(ItemData.PURCHASECOSTARRAY, index)
    ItemData.COSTINGPROCESSArray = this.patchSubPurchaseItems(ItemData.COSTINGPROCESSArray, index)
    return this.formBuilder.group(ItemData);
  }

  public patchSubItems(): FormArray {
    let subItemsFormArray = this.formBuilder.array([]);
    // console.log("ChargeCostDetailArray:",this.ChargeCostDetailArray)
    this.ChargeCostDetailArray.forEach((subItem, indexj) => {
      let SubItemData = JSON.parse(JSON.stringify(subItem))
      // SubItemData.ChargeDetailCalculatedValue = this.formulaExpression(subItems,ChargeCostDetailArray,indexj)
      subItemsFormArray.push(this.formBuilder.group(SubItemData));
    });
    // console.log("subItemsFormArray:",subItemsFormArray)
    return subItemsFormArray;
  }

  public patchSubErectionItems(): FormArray {
    let subItemsFormArray = this.formBuilder.array([]);
    ErectionCostDetailArray.forEach((subItem, indexj) => {
      let SubItemData = JSON.parse(JSON.stringify(subItem))
      // SubItemData.ChargeDetailCalculatedValue = this.formulaExpression(subItems,ChargeCostDetailArray,indexj)
      subItemsFormArray.push(this.formBuilder.group(SubItemData));
    });
    return subItemsFormArray;
  }

  public patchSubPurchaseItems(subItems: any[], index): FormArray {
    let subItemsFormArray = this.formBuilder.array([]);
    subItems.forEach((subItem, indexj) => {
      let SubItemData = JSON.parse(JSON.stringify(subItem))
      // SubItemData.ChargeDetailCalculatedValue = this.formulaExpression(subItems,ChargeCostDetailArray,indexj)
      subItemsFormArray.push(this.formBuilder.group(SubItemData));
    });
    return subItemsFormArray;
  }

  public patchFormArray(items: any[]): void {
    this.orderForm = new FormGroup({
      items: new FormArray([])
    });
    const itemsFormArray = this.orderForm.get('items') as FormArray;
    this.LevelIndex = 0
    items.forEach((item, index) => {
      if(!this.LevelArray.includes(item.Level)){
        this.LevelArray.push(item.Level)
      }
      if(item.Level == 1){
        this.LevelSubIndex = 0
        this.LevelIndex = this.LevelIndex + 1
      } else {
        this.LevelSubIndex = this.LevelSubIndex + 1
      }
      // console.log("LevelSubIndex333:",LevelSubIndex)
      // const itemFormGroup = itemsFormArray.at(index);
      // if(this.gbControl.field.SLNOField != undefined){
      //   itemFormGroup.get(this.gbControl.field.SLNOField)?.setValue(index+1);
      //   console.log(this.gbControl.field.SLNOField,":",itemFormGroup.get(this.gbControl.field.SLNOField)?.value)
      // }
      itemsFormArray.push(this.initItem(item, index, this.LevelIndex, this.LevelSubIndex));
    });
    console.log("itemsFormArray:",itemsFormArray)
  }

  public FormCalculationPatch() {
    this.items = this.orderForm.get('items') as FormArray;
    for (let i = 0; i < this.items.value.length; i++) {
      const itemFormGroup = this.items.at(i) as FormGroup;
      itemFormGroup.get('TotalValue')?.setValue(itemFormGroup.get('PROJECTEDRATE')?.value * itemFormGroup.get('FINALQUANTITY')?.value)
      const subItemsFormArray = itemFormGroup.get('COSTINGCHARGESArray') as FormArray;
      for (let j = 0; j < subItemsFormArray?.value.length; j++) {
        // if(itemFormGroup.get('Level')?.value != 1){
          this.FormulaCalculation(i, j)
        // }
      }
      const subItemsErectionArray = itemFormGroup.get('COSTINGERECTIONArray') as FormArray;
      for (let j = 0; j < subItemsErectionArray?.value.length; j++) {
        // if(itemFormGroup.get('Level')?.value != 1){
          this.FormulaErectionCalculation(i, j)
        // }
      }
    }
  }

  public ItemToggle(index:number,FieldIndex:number,value:number){
    console.log("Index:",index,"Value:",value,"FieldIndex:",FieldIndex)
    console.log("Value Type:",this.gbControl.field.Grid[5])
    if(value == 0){
      this.gbControl.field.Grid[5].Type = 'Picklist'
    } else {
      this.gbControl.field.Grid[5].Type = 'String'
    }
    console.log("Value Type After:",this.gbControl.field.Grid[5])
  }

  public FormSubTotal(){
    this.items = this.orderForm.get('items') as FormArray;
    let index = 0;
    let finalqty = 0;
    let projrate = 0;
    let value = 0;
    let chargecostsum = 0;
    let chargevalsum = 0;    
    let prcscostsum = 0;
    let prcsvalsum = 0;
    let expcostsum = 0;
    let expvalsum = 0;
    let errecostsum = 0
    let errevalsum = 0
    let totalcost = 0;
    let totalval = 0;
    let AllTotalValue = 0;
    let itemlength = this.items.value.length
    for (let i = 0; i < this.items.value.length; i++) {
      const itemFormGroup = this.items.at(i) as FormGroup;
      if(itemFormGroup.get('Level')?.value == 1 ){
        if(i != 0){
          const itemFormGroup1 = this.items.at(index) as FormGroup;
          if(itemFormGroup1.get('ItemType')?.value == 2){
            itemFormGroup1.get('FINALQUANTITY')?.setValue(finalqty)
          } else if(itemFormGroup1.get('ItemType')?.value == 1){
            itemFormGroup1.get('TOTALCOST')?.setValue(totalval.toFixed(2))
            itemFormGroup1.get('TOTALVALUE')?.setValue((itemFormGroup1.get('TOTALCOST')?.value * itemFormGroup1.get('FINALQUANTITY')?.value).toFixed(2))
            // console.log("Index:",itemFormGroup1.get('SLNO')?.value,"TotalValue:",(itemFormGroup1.get('TOTALCOST')?.value * itemFormGroup1.get('FINALQUANTITY')?.value).toFixed(2))
            // AllTotalValue = AllTotalValue + parseFloat((itemFormGroup1.get('TOTALCOST')?.value * itemFormGroup1.get('FINALQUANTITY')?.value).toFixed(2))
            itemFormGroup1.get('ErectionCost')?.setValue(errevalsum.toFixed(2))
            itemFormGroup1.get('ErectionValue')?.setValue((itemFormGroup1.get('ErectionCost')?.value * itemFormGroup1.get('FINALQUANTITY')?.value).toFixed(2))
          } else {
            // console.log("Index:",itemFormGroup.get('SLNO')?.value,"TotalValue:",parseFloat(itemFormGroup.get('TOTALVALUE')?.value))
            // AllTotalValue = AllTotalValue + parseFloat(itemFormGroup.get('TOTALVALUE')?.value)
          }
        }
        index = i
        finalqty = 0;
        projrate = 0;
        value = 0;
        chargecostsum = 0;
        chargevalsum = 0;    
        prcscostsum = 0;
        prcsvalsum = 0;
        expcostsum = 0;
        expvalsum = 0;
        errecostsum = 0
        errevalsum = 0
        totalcost = 0;
        totalval = 0;
      } else if(itemlength == i+1) {
        finalqty = finalqty + parseFloat(itemFormGroup.get('FINALQUANTITY')?.value)
        projrate = projrate + parseFloat(itemFormGroup.get('PROJECTEDRATE')?.value)
        value = value + parseFloat(itemFormGroup.get('TotalValue')?.value)
        chargecostsum = chargecostsum + parseFloat(itemFormGroup.get('MATERIALCHARGESCOST')?.value)
        chargevalsum = chargevalsum + parseFloat(itemFormGroup.get('MATERIALCHARGESVALUE')?.value)
        prcscostsum = prcscostsum + parseFloat(itemFormGroup.get('MATERIALPROCESSCOST')?.value)
        prcsvalsum = prcsvalsum + parseFloat(itemFormGroup.get('MATERIALPROCESSVALUE')?.value)
        errecostsum = errecostsum + parseFloat(itemFormGroup.get('ErectionCost')?.value)
        errevalsum = errevalsum + parseFloat(itemFormGroup.get('ErectionValue')?.value)
        totalcost = totalcost + parseFloat(itemFormGroup.get('TOTALCOST')?.value)
        totalval = totalval + parseFloat(itemFormGroup.get('TOTALVALUE')?.value)
        // console.log("Index:",itemFormGroup.get('SLNO')?.value,"TotalValue:",parseFloat(itemFormGroup.get('TOTALVALUE')?.value))
        // AllTotalValue = AllTotalValue + parseFloat(itemFormGroup.get('TOTALVALUE')?.value)
        const itemFormGroup1 = this.items.at(index) as FormGroup;
        if(itemFormGroup1.get('ItemType')?.value == 2){
          itemFormGroup1.get('FINALQUANTITY')?.setValue(finalqty)
        } else if(itemFormGroup1.get('ItemType')?.value == 1){
          itemFormGroup1.get('TOTALCOST')?.setValue(totalval.toFixed(2))
          itemFormGroup1.get('TOTALVALUE')?.setValue((itemFormGroup1.get('TOTALCOST')?.value * itemFormGroup1.get('FINALQUANTITY')?.value).toFixed(2))
          // console.log("Index:",itemFormGroup1.get('SLNO')?.value,"TotalValue:",parseFloat(itemFormGroup.get('TOTALVALUE')?.value))
          // AllTotalValue = AllTotalValue + parseFloat((itemFormGroup1.get('TOTALCOST')?.value * itemFormGroup1.get('FINALQUANTITY')?.value).toFixed(2))
          itemFormGroup1.get('ErectionCost')?.setValue(errevalsum.toFixed(2))
          itemFormGroup1.get('ErectionValue')?.setValue((itemFormGroup1.get('ErectionCost')?.value * itemFormGroup1.get('FINALQUANTITY')?.value).toFixed(2))
        } else {
          // console.log("Index:",itemFormGroup.get('SLNO')?.value,"TotalValue:",parseFloat(itemFormGroup.get('TOTALVALUE')?.value))
          // AllTotalValue = AllTotalValue + parseFloat(itemFormGroup.get('TOTALVALUE')?.value)
        }
      } else {
        finalqty = finalqty + parseFloat(itemFormGroup.get('FINALQUANTITY')?.value)
        projrate = projrate + parseFloat(itemFormGroup.get('PROJECTEDRATE')?.value)
        value = value + parseFloat(itemFormGroup.get('TotalValue')?.value)
        chargecostsum = chargecostsum + parseFloat(itemFormGroup.get('MATERIALCHARGESCOST')?.value)
        chargevalsum = chargevalsum + parseFloat(itemFormGroup.get('MATERIALCHARGESVALUE')?.value)
        prcscostsum = prcscostsum + parseFloat(itemFormGroup.get('MATERIALPROCESSCOST')?.value)
        prcsvalsum = prcsvalsum + parseFloat(itemFormGroup.get('MATERIALPROCESSVALUE')?.value)
        errecostsum = errecostsum + parseFloat(itemFormGroup.get('ErectionCost')?.value)
        errevalsum = errevalsum + parseFloat(itemFormGroup.get('ErectionValue')?.value)
        totalcost = totalcost + parseFloat(itemFormGroup.get('TOTALCOST')?.value)
        totalval = totalval + parseFloat(itemFormGroup.get('TOTALVALUE')?.value)
        // console.log("Index:",itemFormGroup.get('SLNO')?.value,"TotalValue:",parseFloat(itemFormGroup.get('TOTALVALUE')?.value))
        // AllTotalValue = AllTotalValue + parseFloat(itemFormGroup.get('TOTALVALUE')?.value)
      }
    }
    for (let i = 0; i < this.items.value.length; i++) {
      const itemFormGroup = this.items.at(i) as FormGroup;
      if(itemFormGroup.get('Level')?.value == 1){
        AllTotalValue = AllTotalValue + parseFloat(itemFormGroup.get('TOTALVALUE')?.value)
      }
    }
    console.log("AllTotalValue:",AllTotalValue)
    this.value = this.items.value;
    this.TotalEmit.emit(AllTotalValue)
    this.GridOutPut.emit(this.value);
  }

  LevelFilter(level){
    let items = this.gbControl.value
    this.orderForm = new FormGroup({
      items: new FormArray([])
    });
    this.items = this.orderForm.get('items') as FormArray;
    const itemsFormArray = this.orderForm.get('items') as FormArray;
    items.forEach((item, index) => {
      if(item.Level == level || level == 'All'){
        itemsFormArray.push(this.initItem(item, index,index,index));
      }
    });
    this.FormCalculationPatch();
    this.FormSubTotal();
  }

  public FormCalclationSum(itemIndex: number) {
    const itemsFormArray = this.orderForm.get('items') as FormArray;
    if (itemIndex >= 0 && itemIndex < itemsFormArray.length) {
      const itemFormGroup = itemsFormArray.at(itemIndex) as FormGroup;
      const subItemsFormArray = itemFormGroup.get('COSTINGCHARGESArray') as FormArray;
      let TotalCharge = 0;
      for (let j = 0; j < subItemsFormArray?.value.length; j++) {
        const subItemFormGroup = subItemsFormArray.at(j) as FormGroup;
        let Operation = subItemFormGroup.get('ChargeDetailOperationType')?.value == 1 ? -1 : 1
        TotalCharge = TotalCharge + parseFloat(subItemFormGroup.get('ChargeDetailCalculatedValue')?.value) * Operation
      }
      
      TotalCharge = Math.ceil(TotalCharge)
      const subItemsErrectionArray = itemFormGroup.get('COSTINGERECTIONArray') as FormArray;
      let TotalErrection = 0;
      for (let j = 0; j < subItemsErrectionArray?.value.length; j++) {
        const subItemFormGroup = subItemsErrectionArray.at(j) as FormGroup;
        let Operation = subItemFormGroup.get('ChargeDetailOperationType')?.value == 1 ? -1 : 1
        TotalErrection = TotalErrection + parseFloat(subItemFormGroup.get('ChargeDetailCalculatedValue')?.value) * Operation
      }
      TotalErrection = Math.ceil(TotalErrection)
      
      // itemFormGroup.get('MATERIALPROCESSCOST')?.setValue(itemFormGroup.get('MATERIALPROCESSCOST')?.value)
      itemFormGroup.get('MATERIALPROCESSVALUE')?.setValue(itemFormGroup.get('MATERIALPROCESSCOST')?.value * itemFormGroup.get('FINALQUANTITY')?.value)
      // itemFormGroup.get('MATERIALEXPENSECOST')?.setValue((itemFormGroup.get('MATERIALEXPENSEVALUE')?.value / itemFormGroup.get('FINALQUANTITY')?.value).toFixed(2))
      // itemFormGroup.get('MATERIALEXPENSEVALUE')?.setValue(itemFormGroup.get('MATERIALEXPENSEVALUE')?.value)
      itemFormGroup.get('MATERIALCHARGESCOST')?.setValue((TotalCharge))
      itemFormGroup.get('MATERIALCHARGESVALUE')?.setValue(TotalCharge * itemFormGroup.get('FINALQUANTITY')?.value)
      itemFormGroup.get('ErectionCost')?.setValue((TotalErrection + itemFormGroup.get('ErectionRate')?.value).toFixed(2))
      itemFormGroup.get('ErectionValue')?.setValue(((TotalErrection + itemFormGroup.get('ErectionRate')?.value) * itemFormGroup.get('FINALQUANTITY')?.value).toFixed(2))
      let TotalValue = parseFloat(itemFormGroup.get('MATERIALCHARGESVALUE')?.value) + parseFloat(itemFormGroup.get('MATERIALPROCESSVALUE')?.value) +  parseFloat(itemFormGroup.get('TotalValue')?.value)
      itemFormGroup.get('TOTALCOST')?.setValue((TotalValue / itemFormGroup.get('FINALQUANTITY')?.value).toFixed(2))
      itemFormGroup.get('TOTALVALUE')?.setValue((TotalValue).toFixed(2))
    }
    this.value = this.items.value;
    this.GridOutPut.emit(this.value);
  }

  public FormArrayPatch(itemIndex: number, subItemIndex: number, fieldName: string, event: KeyboardEvent) {
    const itemsFormArray = this.orderForm.get('items') as FormArray;
    if (itemIndex >= 0 && itemIndex < itemsFormArray.length) {
      const itemFormGroup = itemsFormArray.at(itemIndex) as FormGroup;
      const subItemsFormArray = itemFormGroup.get('COSTINGCHARGESArray') as FormArray;
      const subItemFormGroup = subItemsFormArray.at(subItemIndex) as FormGroup;
      const inputElement = event.target as HTMLInputElement;
      if (Number.isNaN(parseInt(inputElement.value.replace(/[^0-9]/g, '')))) {
        subItemFormGroup.get(fieldName)?.setValue("")
      } else {
        subItemFormGroup.get(fieldName)?.setValue(parseInt(inputElement.value.replace(/[^0-9]/g, '')));
      }
      this.changevalue = true
    }
  }

  public ChargeChange(itemIndex: number, subItemIndex: number){
    if(this.changevalue){
      this.FormulaCalculation(itemIndex, subItemIndex)
      this.FormSubTotal();
      this.changevalue = false
    }
  }

  public FormErrectionArrayPatch(itemIndex: number, subItemIndex: number, fieldName: string, event: KeyboardEvent) {
    const itemsFormArray = this.orderForm.get('items') as FormArray;
    if (itemIndex >= 0 && itemIndex < itemsFormArray.length) {
      const itemFormGroup = itemsFormArray.at(itemIndex) as FormGroup;
      const subItemsFormArray = itemFormGroup.get('COSTINGERECTIONArray') as FormArray;
      const subItemFormGroup = subItemsFormArray.at(subItemIndex) as FormGroup;
      const inputElement = event.target as HTMLInputElement;
      if (Number.isNaN(parseInt(inputElement.value.replace(/[^0-9]/g, '')))) {
        subItemFormGroup.get(fieldName)?.setValue("")
      } else {
        subItemFormGroup.get(fieldName)?.setValue(parseInt(inputElement.value.replace(/[^0-9]/g, '')));
      }
      this.changevalue = true
    }
  }

  public ErectionChange(itemIndex: number, subItemIndex: number){
    if(this.changevalue){
      this.FormulaErectionCalculation(itemIndex, subItemIndex)
      this.FormSubTotal();
      this.changevalue = false
    }
  }

  public FormulaCalculation(itemIndex: number, subItemIndex: number): void {
    const itemsFormArray = this.orderForm.get('items') as FormArray;
    const itemFormGroup = itemsFormArray.at(itemIndex) as FormGroup;
    const subItemsFormArray = itemFormGroup.get('COSTINGCHARGESArray') as FormArray;
    const subItemFormGroup = subItemsFormArray.at(subItemIndex) as FormGroup;
    subItemFormGroup.patchValue({ ChargeDetailCalculatedValue: this.formulaExpression(itemFormGroup.value, subItemsFormArray.value, subItemIndex).toFixed(2) });
    this.FormCalclationSum(itemIndex);
  }

  public FormulaErectionCalculation(itemIndex: number, subItemIndex: number): void {
    const itemsFormArray = this.orderForm.get('items') as FormArray;
    const itemFormGroup = itemsFormArray.at(itemIndex) as FormGroup;
    const subItemsFormArray = itemFormGroup.get('COSTINGERECTIONArray') as FormArray;
    const subItemFormGroup = subItemsFormArray.at(subItemIndex) as FormGroup;
    subItemFormGroup.patchValue({ ChargeDetailCalculatedValue: this.formulaErectionExpression(itemFormGroup.value, subItemsFormArray.value, subItemIndex).toFixed(2) });
    this.FormCalclationSum(itemIndex);
  }

  public formulaExpression(SubItem: any, griddata: any, arrayIndex: number): number {
    let result = 0
    let Operation = griddata[arrayIndex].ChargeDetailOperationType == 1 ? -1 : 1
    let FormulaExpression = griddata[arrayIndex].FormulaExpression;
    let clearExpression = FormulaExpression.replace(/\{\{|\}\}/g, '');
    let round = false
    if(clearExpression.includes('Round')){
      round = true
    }
    let evaluatedExpression = clearExpression
      .replace(/SLBASIC_CalcFC/g, SubItem.PROJECTEDRATE)
      .replace(/OverHeads_Input/g, griddata[0].ChargeDetailDefaultInputValue)
      .replace(/Margin_Input/g, griddata[1].ChargeDetailDefaultInputValue)
      .replace(/Freight_Input/g, griddata[2].ChargeDetailDefaultInputValue)
      .replace(/Packing&Forwarding_Input/g, griddata[3].ChargeDetailDefaultInputValue)
      .replace(/ExpenseOnSite_Input/g, griddata[4].ChargeDetailDefaultInputValue)
      .replace(/Miscellanous_Input/g, griddata[5].ChargeDetailDefaultInputValue)
      .replace(/Discount_Input/g, griddata[6].ChargeDetailDefaultInputValue)
      .replace(/Round/g, '');
    result = eval(evaluatedExpression);
    if(round){
      result = this.Round(result)
    }
    return Math.floor(result * 100) / 100;
  }

  public formulaErectionExpression(SubItem: any, griddata: any, arrayIndex: number): number {
    let result = 0
    let Operation = griddata[arrayIndex].ChargeDetailOperationType == 1 ? -1 : 1
    let FormulaExpression = griddata[arrayIndex].FormulaExpression;
    let clearExpression = FormulaExpression.replace(/\{\{|\}\}/g, '');
    let round = false
    if(clearExpression.includes('Round')){
      round = true
    }
    let evaluatedExpression = clearExpression
      .replace(/SLBASIC_CalcFC/g, SubItem.ErectionRate)
      .replace(/Margin_Input/g, griddata[0].ChargeDetailDefaultInputValue)
      .replace(/Round/g, '');
    result = eval(evaluatedExpression);
    if(round){
      result = this.Round(result)
    }
    return Math.floor(result * 100) / 100;
  }

  public Round(num){
    return Math.round(num);
  }

  public onArrowDown() {
    const items = this.orderForm.get('items') as FormArray;
    if (this.selectedRowIndex != items.length - 1) {
      this.selectedRowIndex++;
    }
  }

  public onArrowUp() {
    if (this.selectedRowIndex != 0) {
      this.selectedRowIndex--;
    }
  }

  public onRowClick(index: number): void {
    this.selectedRowIndex = index;
    if (this.tableRows && this.tableRows[index]) {
      this.renderer.selectRootElement(this.tableRows[index].nativeElement).focus();
    }
  }

  public ToggleExpansion(index: number): void {
    this.gbControl.field.Grid[index].Expansion = !this.gbControl.field.Grid[index].Expansion
    // this.gbControl.field.Grid[index-1].Expansion = !this.gbControl.field.Grid[index-1].Expansion
    // let emitvalue = { SelectedData: this.gbControl.field.Grid[index].Expansion, id: FieldName }
    // this.FieldChange.emit(emitvalue)
  }

  public PicklistPatchValue(PicklistData: IPicklistEmitValue, index: number, PicklistDetail: IPickListDetail): void {
    let field = PicklistData.id.split(',')
    const items = this.orderForm.get('items') as FormArray;
    if (index >= 0 && index < items.length) {
      const itemFormGroup = items.at(index);
      if (PicklistData.value != undefined) {
        let linkids = PicklistData.id.split(',')
        let linkvalues = PicklistData.value.split(',')
        for (let i = 0; i < linkids.length; i++) {
          itemFormGroup.get(linkids[i])?.setValue(PicklistData.Selected[linkvalues[i]])
        }
      }
      else if (field.length == 2) {
        itemFormGroup.get(field[0])?.setValue(PicklistData.SelectedData);
        itemFormGroup.get(field[1])?.setValue(PicklistData.Selected[PicklistDetail.PicklistTitle]);
      } else {
        itemFormGroup.get(field[0])?.setValue(PicklistData.SelectedData);
      }
    }
    this.value = this.items.value;
    this.GridOutPut.emit(this.value);
    this.GridOutPut.emit(this.value);
  }

  public PicklistLoadCurrentRow(PicklistId: number, index: number, PicklistRetrivalURL: string, PicklistRetrivalIdField: string) {
    const items = this.orderForm.get('items') as FormArray;
    if (index >= 0 && index < items.length) {
      const itemFormGroup = items.at(index);
      this.service.getGridService(PicklistRetrivalURL, PicklistId, PicklistRetrivalIdField).subscribe(res => {
        itemFormGroup.patchValue(res)
        this.value = this.items.value;
        this.GridOutPut.emit(this.value);
      })

    }
  }

  public ComboBoxValue(ComboJSON, FieldName: number): void {
    let KeyPair
    for (let i = 0; i < ComboJSON.length; i++) {
      if (FieldName == ComboJSON[i]['key']) {
        KeyPair = i
        KeyPair = ComboJSON[KeyPair]
        break;
      }
    }
    return KeyPair
  }

  public getZerothIndexValue(index: number): any {
    const items = this.orderForm.get('items') as FormArray;
    if (items.length > 0) {
      const selctedItem = items.at(index);
      return selctedItem.value;
    }
    return null;
  }

  public getTimeValue(index: number, fieldvalue: string): any {
    const items = this.orderForm.get('items') as FormArray;
    if (items.length > 0) {
      const selctedItem = items.at(index)
      return selctedItem.value[fieldvalue];
    }
    return null;
  }

  public editField(index: number, fieldName: string, updatedValue: number,FieldIndex:number): void {
    const items = this.orderForm.get('items') as FormArray;
    if (index >= 0 && index < items.length) {
      const itemFormGroup = items.at(index);
      itemFormGroup.get(fieldName)?.setValue(updatedValue);
    }
    this.value = this.items.value;
    if (fieldName == 'ItemType') {
      this.FormSubTotal();
    }
    // if(fieldName == 'SectionType'){
    //   this.ItemToggle(index,FieldIndex,updatedValue);
    // }
    this.GridOutPut.emit(this.value);
  }

  // clearStartDate() {
  //   this.datepicker.nativeElement.value = null;
  // }
  public onDateChange(index: number, fieldName: string, event: any): void {
    // var datePipe = new DatePipe("en-US");
    const items = this.orderForm.get('items') as FormArray;
    if (index >= 0 && index < items.length) {
      const itemFormGroup = items.at(index);
      itemFormGroup.get(fieldName)?.setValue("/Date(" + ((event).getTime() + 43200000).toString() + ")/");
    }
    this.value = this.items.value;
    this.GridOutPut.emit(this.value);
    // console.log("Date Picker:",this.datepicker)
    // this.datepicker.nativeElement.value = datePipe.transform(event,'dd/MMM/yyyy');
  }

  public getDateValue(DateValue: string) {
    var datePipe = new DatePipe("en-US");
    if (DateValue.includes('Date')) {
      return datePipe.transform(JSON.parse(DateValue.replace("/Date(", "").replace(")/", "")), 'dd/MMM/yyyy')
    } else {
      return ""
    }
  }

  public LightBox(index: number, FieldName: string): void {
    let emitvalue = { index: index, FieldValue: this.getZerothIndexValue(index)[FieldName] }
    this.LightBoxOutPut.emit(emitvalue)
  }

  public ValueSave(index: number, fieldName: string, event: KeyboardEvent, type: string): void {
    const items = this.orderForm.get('items') as FormArray;
    if (index >= 0 && index < items.length && type.toLowerCase() == 'number') {
      const itemFormGroup = items.at(index);
      const inputElement = event.target as HTMLInputElement;
      if (Number.isNaN(parseInt(inputElement.value.replace(/[^0-9]/g, '')))) {
        itemFormGroup.get(fieldName)?.setValue("")
      } else {
        itemFormGroup.get(fieldName)?.setValue(parseInt(inputElement.value.replace(/[^0-9]/g, '')));
      }
    }
    if (fieldName == 'RATEFC' || fieldName == 'FINALQUANTITY' || fieldName == 'PROJECTEDRATE' || fieldName == 'ErectionRate' ||fieldName == 'ErectionCost' || fieldName == 'ErectionValue') {
      this.changevalue = true
    }
    this.value = this.items.value;
    this.GridOutPut.emit(this.value);
  }

  public InputChange(fieldName: string,event){
    if (fieldName == 'RATEFC' || fieldName == 'FINALQUANTITY' || fieldName == 'PROJECTEDRATE' || fieldName == 'ErectionRate' ||fieldName == 'ErectionCost' || fieldName == 'ErectionValue') {
      if(this.changevalue){
        this.FormSubTotal();
        this.FormCalculationPatch();
        this.FormSubTotal();
        this.changevalue = false
      }
    }
  }

  private createItem(index: number): FormGroup {
    const formGroupConfig: { [key: string]: any } = {};
    this.gbControl.field.Grid?.forEach((field, fieldIndex) => {
      if (field.PostData != false) {
        if (field.DependendChangeField != undefined) {
          if (this.ArrayValues[field.DependendChangeField[0]]    > this.ArrayValues[field.DependendChangeField[1]]) {
            formGroupConfig[field.value] = 1;
          } else {
            formGroupConfig[field.value] = 0;
          }
        }
        else if (field.Type?.toLowerCase() == 'sno' || field.Type?.toLowerCase() == 'defaultsno') {
          if(this.LevelSubIndex == 0){
            // console.log("LevelSubIndex111:",LevelSubIndex)
            this.LevelIndex = this.LevelIndex + 1;
            formGroupConfig[field.value] = this.LevelIndex;
          } else {
            this.LevelSubIndex = this.LevelSubIndex + 1;
            formGroupConfig[field.value] = this.LevelIndex + '.' + this.LevelSubIndex;
          }
          // formGroupConfig[field.value] = index + 1;
        } else if (field.Type?.toLowerCase() == 'fieldbasedpicklist' || field.Type?.toLowerCase() == 'fieldbasedcombobox') {
          if (field.DefaultValue == undefined) {
            formGroupConfig[field.value[0]] = '';
          } else {
            formGroupConfig[field.value[0]] = field.DefaultValue;
          }
        } else if (field.Type?.toLowerCase() == 'array') {
          if(field.value == 'COSTINGCHARGESArray'){
            formGroupConfig[field.value] = this.patchSubItems()
          } else if(field.value == 'COSTINGERECTIONArray'){
            formGroupConfig[field.value] = this.patchSubErectionItems()
          } else {
            formGroupConfig[field.value] = this.formBuilder.array([]);
          }
        } else {
          if (field.DefaultValue == undefined) {
            formGroupConfig[field.value] = '';
          } else if (field.DefaultId == true) {
            formGroupConfig[field.value] = this.ArrayDetail
          } else if (field.DefaultValue == 'today') {
            let todaydate = new Date();
            formGroupConfig[field.value] = "/Date(" + ((todaydate).getTime()).toString() + ")/";
          } else if (typeof field.DefaultValue == 'object') {
            if (this.ArrayValues[field.DefaultValue[0]] - this.ArrayValues[field.DefaultValue[1]] < 0) {
              formGroupConfig[field.value] = (this.ArrayValues[field.DefaultValue[0]] - this.ArrayValues[field.DefaultValue[1]]) * -1;
            } else {
              formGroupConfig[field.value] = this.ArrayValues[field.DefaultValue[0]] - this.ArrayValues[field.DefaultValue[1]];
            }
          } else {
            if(field.value == 'Level'){
              if(this.LevelSubIndex == 0){
                formGroupConfig[field.value] = 1
              } else {
                formGroupConfig[field.value] = 2
              }
            } else {
              formGroupConfig[field.value] = field.DefaultValue;
            }
            
          }
        }
      }
    });
    console.log("formGroupConfig:",formGroupConfig)
    return this.formBuilder.group(formGroupConfig);
  }

  getRowColor(index: number): string {
    const colors = ['#FFFFFF', '#F9F6FE', '#FFFFFF', '#FDF5FB', '#FFFFFF', '#FFF0F2']; // Define your colors array
    return colors[index % colors.length]; // Get the color based on index modulo colors array length
  }


  public addrow(): void {
    if (this.isreadable) {
      let flag = 1;
      let alertdata: string[] = []
      const items = this.orderForm.get('items') as FormArray;
      const itemFormGroup = items.at(items.length - 1);
      if (itemFormGroup) {
        this.gbControl.field.Grid?.forEach((field, index) => {
          if (itemFormGroup.get(field.value)?.value === '' && field.Required != false) {
            alertdata.push("Please Fill " + " - " + items.length + " - " + field.Label)
            flag = 0;
          }
        })
      }
      if (flag) {
        this.items = this.orderForm.get('items') as FormArray;
        this.items.push(this.createItem(this.items.value.length));
        this.selectedRowIndex = this.items.value.length - 1;
        this.value = this.items.value;
        this.GridOutPut.emit(this.value);
      } else {
        const dialogRef = this.dialog.open(DialogBoxComponent, {
          width: '400px',
          data: {
            message: alertdata,
            heading: 'Error'
          }
        })
      }
    }
  }

  public insertItem(index: number): void {
    const items = this.orderForm.get('items') as FormArray;
    if (index >= 0 && index < items.length) {
      items.insert(index, this.createItem(index));
      this.selectedRowIndex = index
    }
  }

  private removedata(index: number): void {
    const items = this.orderForm.get('items') as FormArray;
    if (index >= 0 && index < items.length) {
      items.removeAt(index);
    }
    this.value = this.items.value;
    this.GridOutPut.emit(this.value);
  }

  public removeItem(index: number): void {
    const dialogRef = this.dialog.open(DialogBoxComponent, {
      width: '400px',
      data: {
        message: "Do You want to Delete this data?",
        heading: 'Info',
        button1: 'Yes',
        button2: 'No'
      }
    })
    dialogRef.afterClosed().subscribe(result => {
      if (result == 'Yes') {
        this.removedata(index);
      }
    });
  }

  public LightBoxOpen(index: number, fieldName: string, event: KeyboardEvent, type: string): void{
    if (fieldName == 'PROJECTEDRATE'){
      this.PurchaseCost = true
      this.PurchaseIndex = index
    } else if(fieldName == 'MATERIALPROCESSCOST'){
      this.ProcessCost = true
      this.ProcessIndex = index
    } else if(fieldName == 'SLNO'){
      const itemsFormArray = this.orderForm.get('items') as FormArray;
      const itemFormGroup = itemsFormArray.at(index) as FormGroup;
      if(itemFormGroup.get('Level')?.value == 1){
        let i=1
        let count = 0
        while(true){
          if(index + i >= itemsFormArray.value.length){
            break
          }
          const itemFormGroup1 = itemsFormArray.at(index + i) as FormGroup;
          if(itemFormGroup1.get('Level')?.value == 2){
            if(!this.LevelHide.includes(index + i)){
              this.LevelHide.push(index + i)
            } else {
              const indexOfItem = this.LevelHide.indexOf(index + i);
              if (indexOfItem !== -1) {
                this.LevelHide.splice(indexOfItem, 1);
              }
            }
            count ++;
            i++;
          } else {
            break
          }
        }
      }
    }
  }

  public LightBoxClose(): void{
    this.PurchaseCost = false
  }

  public savePurchaseCost(): void{
    this.PurchaseCost = false
    console.log("Item Value:",this.items.value)
    const itemsFormArray = this.orderForm.get('items') as FormArray;
    const itemFormGroup = itemsFormArray.at(this.PurchaseIndex) as FormGroup;
    const subItemsFormArray = itemFormGroup.get('PURCHASECOSTARRAY') as FormArray;
    let purchasecost = 0
    for(let data of subItemsFormArray.value){
      purchasecost = purchasecost + parseInt(data.ItemCost)
    }
    console.log("purchasecost:",purchasecost)
    itemFormGroup.get('PROJECTEDRATE')?.setValue(purchasecost)
    this.FormCalculationPatch();
    this.FormSubTotal();
  }

  public Purchaseaddrow():void{
    const itemsFormArray = this.orderForm.get('items') as FormArray;
    const itemFormGroup = itemsFormArray.at(this.PurchaseIndex) as FormGroup;
    const subItemsFormArray = itemFormGroup.get('PURCHASECOSTARRAY') as FormArray;
    subItemsFormArray.push(this.createPurchaseCost(subItemsFormArray.value.length));
  }
  
  private createPurchaseCost(index: number): FormGroup {
    const formGroupConfig: { [key: string]: any } = {};
    formGroupConfig['SLNo'] = index + 1;
    formGroupConfig['ItemName'] = "";
    formGroupConfig['ItemCost'] = "";
    formGroupConfig['ItemRemarks'] = "";
    return this.formBuilder.group(formGroupConfig);
  }

  public removePurchaseItem(index: number){
    const dialogRef = this.dialog.open(DialogBoxComponent, {
      width: '400px',
      data: {
        message: "Do You want to Permentely Delete this data?",
        heading: 'Info',
        button1: 'Yes',
        button2: 'No'
      }
    })
    dialogRef.afterClosed().subscribe(result => {
      if (result == 'Yes') {
        const itemsFormArray = this.orderForm.get('items') as FormArray;
        const itemFormGroup = itemsFormArray.at(this.PurchaseIndex) as FormGroup;
        const subItemsFormArray = itemFormGroup.get('PURCHASECOSTARRAY') as FormArray;
        subItemsFormArray.removeAt(index)
        if (index >= 0 && index < subItemsFormArray.length) {
          for (let i = index; i < subItemsFormArray.length; i++) {
            const itemFormGroup = subItemsFormArray.at(i);
            itemFormGroup.get('SLNo')?.setValue(i + 1);
          }
        }
      }
    });
  }

  public ProcessLightBoxClose(): void{
    this.ProcessCost = false
  }

  public saveProcessCost(): void{
    this.ProcessCost = false
    console.log("Item Value:",this.items.value)
    const itemsFormArray = this.orderForm.get('items') as FormArray;
    const itemFormGroup = itemsFormArray.at(this.ProcessIndex) as FormGroup;
    const subItemsFormArray = itemFormGroup.get('COSTINGPROCESSArray') as FormArray;
    let processcost = 0
    for(let data of subItemsFormArray.value){
      processcost = processcost + parseInt(data.ItemCost)
    }
    itemFormGroup.get('MATERIALPROCESSCOST')?.setValue(processcost)
    this.FormCalculationPatch();
    this.FormSubTotal();
  }

  public Processaddrow():void{
    const itemsFormArray = this.orderForm.get('items') as FormArray;
    const itemFormGroup = itemsFormArray.at(this.ProcessIndex) as FormGroup;
    const subItemsFormArray = itemFormGroup.get('COSTINGPROCESSArray') as FormArray;
    subItemsFormArray.push(this.createProcessCost(subItemsFormArray.value.length));
  }
  
  private createProcessCost(index: number): FormGroup {
    const formGroupConfig: { [key: string]: any } = {};
    formGroupConfig['SLNo'] = index + 1;
    formGroupConfig['ItemName'] = "";
    formGroupConfig['ItemCost'] = "";
    formGroupConfig['ItemRemarks'] = "";
    return this.formBuilder.group(formGroupConfig);
  }

  public removeProcessItem(index: number){
    const dialogRef = this.dialog.open(DialogBoxComponent, {
      width: '400px',
      data: {
        message: "Do You want to Permentely Delete this data?",
        heading: 'Info',
        button1: 'Yes',
        button2: 'No'
      }
    })
    dialogRef.afterClosed().subscribe(result => {
      if (result == 'Yes') {
        const itemsFormArray = this.orderForm.get('items') as FormArray;
        const itemFormGroup = itemsFormArray.at(this.ProcessIndex) as FormGroup;
        const subItemsFormArray = itemFormGroup.get('COSTINGPROCESSArray') as FormArray;
        subItemsFormArray.removeAt(index)
        if (index >= 0 && index < subItemsFormArray.length) {
          for (let i = index; i < subItemsFormArray.length; i++) {
            const itemFormGroup = subItemsFormArray.at(i);
            itemFormGroup.get('SLNo')?.setValue(i + 1);
          }
        }
      }
    });
  }

  public ChargePercentChange(Charge){
    if(Charge.Type == 'CalculatedInput'){
      this.ChargeCost = true
    }
    this.ChargeChangeValue = Charge
    console.log("Charge:",Charge)
  }

  public ChargeLightBoxClose(){
    this.ChargeCost = false
  }

  public savechargeCost(){
    // console.log("ChargeCostDetailArray:",this.ChargeCostDetailArray)
    let charge = JSON.stringify(this.ChargeCostDetailArray)
    let charge1 = JSON.parse(charge)
    charge1.forEach((field, fieldIndex:number) => {
      if(field.ChargeElementName == this.ChargeChangeValue.value){
        field.ChargeDetailDefaultInputValue = parseInt(this.ChargeContent)
      }
    })
    this.ChargeCostDetailArray = charge1
    // console.log("ChargeCostDetailArray After:",this.ChargeCostDetailArray)
    const itemsFormArray = this.orderForm.get('items') as FormArray;
    for(let i = 0; i < this.items.length;i++){
      const itemFormGroup = itemsFormArray.at(i) as FormGroup;
      const subItemsFormArray = itemFormGroup.get('COSTINGCHARGESArray') as FormArray;
      for (let j = 0; j < subItemsFormArray?.value.length; j++) {
        const subItemFormGroup = subItemsFormArray.at(j) as FormGroup;
        if(subItemFormGroup.get('ChargeElementName')?.value == this.ChargeChangeValue.value){
          subItemFormGroup.get('ChargeDetailDefaultInputValue')?.setValue(this.ChargeContent)
        }
      }
    } 
    // this.patchFormArray(this.gbControl.value);
    this.FormCalculationPatch();
    this.FormSubTotal();
    this.ChargeContent = ""
    this.value = this.items.value;
    this.GridOutPut.emit(this.value);
    this.ChargeCost = false
  }

  public LevelIndent(index:number,Position:string){
    const itemsFormArray = this.orderForm.get('items') as FormArray;
    console.log("Index:",index)
    const itemFormGroup = itemsFormArray.at(index) as FormGroup;
    if(Position == 'Back'){
      this.LevelSubIndex = 0
      this.LevelIndex = this.LevelIndex + 1;
      itemFormGroup.get('SLNO')?.setValue(this.LevelIndex)
      itemFormGroup.get('Level')?.setValue(1)
    } else {
      this.LevelIndex = this.LevelIndex - 1;
      const itemFormGroup1 = itemsFormArray.at(index-1) as FormGroup;
      let a = (String(itemFormGroup1.get('SLNO')?.value)).split('.')
      if(a[1]==undefined){
        this.LevelSubIndex = 0
      } else {
        this.LevelSubIndex = parseInt(a[1])
      }
      this.LevelSubIndex = this.LevelSubIndex + 1;
      itemFormGroup.get('SLNO')?.setValue(this.LevelIndex + '.' + this.LevelSubIndex)
      itemFormGroup.get('Level')?.setValue(2)
    }
    console.log("Position:",Position)
  }
}