import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Router } from '@angular/router';
import { GbInputComponent } from './gb-input.component';
import { MatInputModule } from '@angular/material/input';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxMaskModule, IConfig } from 'ngx-mask'

const maskConfig: Partial<IConfig> = {
  validation: false,
};
@NgModule({
  declarations: [GbInputComponent],
  imports: [CommonModule, RouterModule, MatInputModule, ReactiveFormsModule, NgxMaskModule.forRoot(maskConfig)],
  exports: [GbInputComponent],
  entryComponents: [],
})
export class GbInputModule {}
