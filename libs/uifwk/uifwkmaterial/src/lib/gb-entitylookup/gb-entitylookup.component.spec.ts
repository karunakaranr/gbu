import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { GbEntitylookupComponent } from './gb-entitylookup.component';

describe('GbEntitylookupComponent', () => {
  let component: GbEntitylookupComponent;
  let fixture: ComponentFixture<GbEntitylookupComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ GbEntitylookupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GbEntitylookupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
