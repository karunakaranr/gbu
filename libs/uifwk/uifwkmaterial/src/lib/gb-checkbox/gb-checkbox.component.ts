import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { GBBaseUIFCComponent } from '@goodbooks/uicore';
import { Select } from '@ngxs/store';
import { LayoutState } from 'features/layout/store/layout.state';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-gb-checkbox',
  templateUrl: './gb-checkbox.component.html',
  styleUrls: ['./gb-checkbox.component.scss']
})
export class GbCheckboxComponent extends GBBaseUIFCComponent implements OnInit {
  @Output() CheckBoxOutPut: EventEmitter<any> = new EventEmitter<string>();
  @Input() labelPosition: 'before' | 'after' = 'before';
  @Select(LayoutState.FormEdit) edit$: Observable<any>;
  isreadable:boolean = false;
  isFocused: boolean = false;
  ngOnInit(): void {
    this.edit$.subscribe(res => {
      this.isreadable = res;
    })
  }

  onKeyDown(event: KeyboardEvent) {
    if (event.key === 'Tab') {
      this.isFocused = false
    }
  }

  onKeyUp(event: KeyboardEvent) {
    if (event.key === 'Tab') {
      this.isFocused = true
    }
  }
  
  chkChanged(evt) {
    this.value = evt;
    this.CheckBoxOutPut.emit(this.value)
  }
}



// import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';

// import { GBBaseUIFCComponent } from '@goodbooks/uicore';

// @Component({
//   selector: 'app-gb-checkbox',
//   templateUrl: './gb-checkbox.component.html',
//   styleUrls: ['./gb-checkbox.component.scss']
// })
// export class GbCheckboxComponent extends GBBaseUIFCComponent implements  OnChanges {
//   @Input() isChecked;
//   @Output() childEvent : EventEmitter<any> = new EventEmitter();
//   check : boolean = false
//   ngOnChanges(changes: SimpleChanges): void {
//     if(changes.isChecked){
//       this.check = this.isChecked == 0 ? true : false;
//       if(this.isChecked == undefined){
//         this.check = false
//       }
//     }
//   }

//   chkChanged(evt) {
//     let value = evt.checked ? 1 : 0;
//     this.childEvent.emit(value);

//   }
// }
