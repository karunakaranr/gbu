import { Component, OnInit } from '@angular/core';
import { GBBaseUIFCComponent } from '@goodbooks/uicore';


@Component({
  selector: 'app-gb-email',
  template: '{{Field?.Label}}: <input [formControl]="gbControl"><br/>'
})
export class GbEmailComponent extends GBBaseUIFCComponent {
}
