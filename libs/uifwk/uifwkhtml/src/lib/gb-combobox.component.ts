import { Component, OnInit } from '@angular/core';
import { GBBaseUIFCComponent } from '@goodbooks/uicore';

@Component({
  selector: 'app-gb-combobox',
  template: '{{Field?.Label}}: <select [formControl]="gbControl"><option *ngFor="let val of Field.PickLists">{{val}}</option></select><br/>'
})
export class GbComboboxComponent extends GBBaseUIFCComponent {
}
