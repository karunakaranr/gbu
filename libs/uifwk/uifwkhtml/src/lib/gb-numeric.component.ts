import { Component, OnInit } from '@angular/core';
import { GBBaseUIFCComponent } from '@goodbooks/uicore';



@Component({
  selector: 'app-gb-numeric',
  template: '{{Field?.Label}}: <input [formControl]="gbControl" type="number"><br/>'
})
export class GbNumericComponent extends GBBaseUIFCComponent {
}
