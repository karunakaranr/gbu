import { Component, OnInit } from '@angular/core';
import { GBBaseUIFGComponent } from '@goodbooks/uicore';



@Component({
  selector: 'app-gb-address',
  template: '{{Field?.Label}}: html address<br/>'
})
export class GbAddressComponent extends GBBaseUIFGComponent {
}
