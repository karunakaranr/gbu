import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
// import { IMenulist } from './../../models/Imenu.model';
import { attachdbService } from './../../dbservices/attachment/attachdb.service'
@Injectable({
  providedIn: 'root'
})
export class attachserviceService {
  constructor(private attachdbService: attachdbService) { }

  
  fileattachmentservice(filedata:any,objecttypeid:any){

    return this.attachdbService.uploadservice1(filedata,objecttypeid)
  }
  fileattachmentupdateservice(alfrescodata:any,objecttypeid:any){

    return this.attachdbService.uploadservice2(alfrescodata,objecttypeid)
  }

  filedownloadservice(id:any){
    return this.attachdbService.entityfiledownloadservice(id)
  }
}
