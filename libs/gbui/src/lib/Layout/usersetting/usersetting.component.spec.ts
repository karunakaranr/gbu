import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { usersettingComponent } from './usersetting.component';

describe('UserpreferenceComponent', () => {
  let component: usersettingComponent;
  let fixture: ComponentFixture<usersettingComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ usersettingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(usersettingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
