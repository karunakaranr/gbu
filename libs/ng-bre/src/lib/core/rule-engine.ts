import { EventEmitter, Injectable } from '@angular/core';
import { AbstractControl, FormGroup } from '@angular/forms';
import { Observable, Subscriber } from 'rxjs';
// import { ExecuteCondition } from '../functions/actions';

import { EnumComparers, EnumValueType, IAction, IActionCheckCondition, IActionResult, ICondition, IFormula, IRule, IRuleContext } from '../interfaces/RulesModel';
import { RuleFunctions } from './rulefunctions';
// import { ExecuteCondition, RuleFunctionsSpl } from './rulefunctionsSpl';




@Injectable({providedIn: 'root'})
export class RuleEngine { //extends EventEmitter {

  private context: IRuleContext = {engine: this, data: null, obs: new Subscriber<IActionResult>(), currentRule: {} as IRule, currentCondition: {} as ICondition, currentAction: {} as IAction};

  constructor(private rules: IRule[], private options: unknown) {
    // super();
    RuleFunctions.Initialize();
    // RuleFunctions.addAction(ExecuteCondition);
    // RuleFunctionsSpl.Initialize();
  }

  public addRule(rule: IRule) {
    this.rules.push(rule);
  }

  public setContext(fg: FormGroup) {
    this.context.data = fg;
    if (this.context.data instanceof FormGroup) {
      // const fg: FormGroup = context as FormGroup;
      for(let i = 0; i < this.rules.length; i++) {
        const rl: IRule = this.rules[i];
        const fc = fg.controls[rl.Condition.Fact];
        fc.valueChanges.subscribe((value) => {
          try {
            this.executeRule(rl)
          }
          catch(e) {
          }
        });
      }
    }
  }

  private executeRule(rl: IRule) {
    
    this.context.currentRule = rl;
    this.checkAndRunCondition(rl.Condition);
  }

  public checkAndRunCondition(cond: ICondition) {
    this.context.currentCondition = cond;
    if (this.checkCondition(cond)) {
      this.runConditionActions(cond.Truthy);
    }
    else {
      if (cond.Falsy)
        this.runConditionActions(cond.Falsy);
    }
  }

  private runConditionActions(condActs: ICondition | IAction[]) {
    if (Array.isArray(condActs)) {
      this.executeActions(condActs);
    }
    else {
      this.checkAndRunCondition(condActs);
    }
  }

  private checkCondition(cond: ICondition) {
    const en = cond.Comparer;
    const ennm = EnumComparers[en];
    const cfn = RuleFunctions.getComparer(ennm);
    if (en < 0) {
      return true;
    }

    if (cfn) {
      const factValue = this.getValue(EnumValueType.Field, cond.Fact);
      const compareValue = this.getValue(cond.ValueType, cond.Value);
      return (cfn.fn(this.context, factValue, compareValue));
    }
    return false;
  }

  private executeActions(acts: IAction[]) {
    for(let i = 0; i < acts.length; i++) {
      this.executeAction(acts[i]);
    }
  }
  private executeAction(act: IAction) {
    const en = act.Name;
    this.context.currentAction = act;
    const afn = RuleFunctions.getAction(en);
    if (afn)
      this.emitEvent(afn.fn(this.context, act));
  }

  public getValue(vt: EnumValueType | undefined, val: unknown): unknown {
    let data: any = this.context.data;
    if (data instanceof FormGroup) data = data.value;
    switch(vt) {
      case EnumValueType.Field: {
        const strVal = val as string;
        return this.getFieldValue(strVal); //data[strVal];
        break;
      }
      case EnumValueType.Constant: {
        return val;
        break;
      }
      case EnumValueType.Formula: {
        return this.getFormulaValue(val as IFormula);
        break;
      }
    }
    return null;
  }

  private getFieldValue(fldName: string): unknown {
    const data: any = this.context.data;
    if (data instanceof FormGroup) {
      const fg: FormGroup = data as FormGroup;
      const fc: AbstractControl = fg.controls[fldName];
      return fc.value;
    }
    else {
      return data[fldName];
    }
  }

  private getFormulaValue(formula: IFormula): unknown {
    let data: any = this.context.data;
    if (data instanceof FormGroup) data = data.value;

    const leftValue = this.getFieldValue(formula.Field); //data[formula.Field];
    let rightValue = null;

    switch (formula.ValueType) {
      case EnumValueType.Clear: {
        throw "Invalid formula value type!";
        break;
      }
      case EnumValueType.Constant: {
        rightValue = formula.Value;
        break;
      }
      case EnumValueType.Field: {
        rightValue = this.getFieldValue(formula.Value as string); //data[formula.Value as string];
        break;
      }
      case EnumValueType.Formula: {
        const newformula = formula.Value as IFormula
        // rightValue = rf.calcFormula(context, newformula, rf);
        rightValue = this.getFormulaValue(newformula);
        break;
      }
      case EnumValueType.Function: {
        const newformula = formula as IFormula
        const newFunctionName = newformula.Value as string;
        const fn = RuleFunctions.getFunction(newFunctionName);
        rightValue = fn?.fn(this.context);
        break;
      }
    }

    const fn = RuleFunctions.getOperator(formula.Operator);
    return fn?.fn(this.context, leftValue, rightValue);
  }

  // public get results$(): Observable<IActionResult> {
  //   return this.context.results$;
  // }
  public run(runtimeFacts: unknown): Observable<IActionResult> {
    this.context.data = runtimeFacts;
    // this.context.results$ = new EventEmitter<IActionResult>();

    return new Observable(observer => {
      // this.logs.push('>>>>> Created Observable')
      // observer.next('Hello World');
      // observer.next('Something else');
      // setTimeout(() => {
      //   observer.next('Great work');
      //   observer.complete();
      // }, 3000);
      this.context.obs = observer;
      for (let i = 0; i < this.rules.length; i++) {
        const rl: IRule = this.rules[i];
        this.executeRule(rl);
      }
    });
    for (let i = 0; i < this.rules.length; i++) {
      const rl: IRule = this.rules[i];
      this.executeRule(rl);
    }
  }

  public emitEvent(ar: IActionResult) {
    ar.Rule = this.context.currentRule;
    ar.Action = this.context.currentAction;
    ar.Context = this.context.data;
    this.context.obs.next(ar);
  }
}

