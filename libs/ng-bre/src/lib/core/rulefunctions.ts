import { AreEqual, AreNotEqual, IsLessThan, IsLessThanOrEqual, IsGreaterThan, IsGreaterThanEqual, ContainsData, DoesNotContainData, BeginsWith, DoesNotBeginWith, EndsWith, DoesNotEndWith, Contains, DoesNotContain, NotApplicable } from '../functions/comparers';
import { SetLock, SetRequired, SetValue, SetVisible, SetError, ActOnCondition } from '../functions/actions';
import { add, diffDays, diffYears, divide, multiply, subtract } from '../functions/operators';
import { getDayInSecs, negToday, newgGetDayInSecs, today } from '../functions/customfunctions';
import { EnumOperators } from '../interfaces/RulesModel';
import { Comparer, ComparerFn, Action, ActionFn, Operator, OperatorFn, CustomFunction, CustomFunctionFn } from '../interfaces/FunctionInterfaces';

export class RuleFunctions { //implements IRuleFunctions {
  public static initialized = false;
  public static comparers: Comparer[] = [];
  public static actions: Action[] = [];
  public static operators: Operator[] = [];
  public static functions: CustomFunction[] = [];
  static Initialize() {
    // Add Comparers
    this.addComparer(AreEqual);
    this.addComparer(AreNotEqual);
    this.addComparer(IsLessThan);
    this.addComparer(IsLessThanOrEqual);
    this.addComparer(IsGreaterThan);
    this.addComparer(IsGreaterThanEqual);
    this.addComparer(ContainsData);
    this.addComparer(DoesNotContainData);
    this.addComparer(BeginsWith);
    this.addComparer(DoesNotBeginWith);
    this.addComparer(EndsWith);
    this.addComparer(DoesNotEndWith);
    this.addComparer(Contains);
    this.addComparer(DoesNotContain);
    this.addComparer(NotApplicable);

    // Add Actions
    this.addAction(SetLock);
    this.addAction(SetRequired);
    this.addAction(SetValue);
    this.addAction(SetVisible);
    this.addAction(SetError);
    this.addAction(ActOnCondition);

    // Add Operators
    this.addOperator(add);
    this.addOperator(subtract);
    this.addOperator(multiply);
    this.addOperator(divide);
    this.addOperator(diffDays);
    this.addOperator(diffYears);

    // Add Functions
    this.addFunction(today);
    this.addFunction(negToday);
    this.addFunction(getDayInSecs);
    this.addFunction(newgGetDayInSecs);

    this.initialized = true;
  }
  static addComparer(comparer: ComparerFn, comparerName?: string) {
    RuleFunctions.comparers.push({name: comparerName ? comparerName : comparer.name, fn: comparer});
  }
  static addAction(action: ActionFn, actionName?: string) {
    RuleFunctions.actions.push({name: actionName ? actionName : action.name, fn: action});
  }
  static addOperator(operator: OperatorFn, operatorName?: string) {
    RuleFunctions.operators.push({name: operatorName ? operatorName : operator.name, fn: operator});
  }
  static addFunction(foo: CustomFunctionFn, fooName?: string) {
    RuleFunctions.functions.push({name: fooName ? fooName : foo.name, fn: foo});
  }

  static getComparer(name: string): Comparer | null {
    const fn: Comparer[] = RuleFunctions.comparers.filter((o) => o.name === name);
    if (fn.length > 0) return fn[0]
    return null;
  }
  static getAction(name: string): Action | null {
    const fn: Action[] = RuleFunctions.actions.filter((o) => o.name === name);
    if (fn.length > 0) return fn[0]
    return null;
  }
  static getOperator(name: string | number): Operator | null {
    let fnName = name;
    if(typeof name === "number") fnName = EnumOperators[name];
    const fn: Operator[] = RuleFunctions.operators.filter((o) => o.name === fnName);
    if (fn.length > 0) return fn[0]
    return null;
  }
  static getFunction(name: string): CustomFunction | null {
    const fn: CustomFunction[] = RuleFunctions.functions.filter((o) => o.name === name.toLowerCase());
    if (fn.length > 0) return fn[0]
    return null;
  }
}

