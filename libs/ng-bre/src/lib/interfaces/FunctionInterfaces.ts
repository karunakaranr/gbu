import { IActionResult, IRuleContext } from "./RulesModel";

export type ComparerFn = (context: IRuleContext, factValue: unknown, compareValue: unknown) => boolean;
export type ActionFn = (context: IRuleContext, options: unknown) => IActionResult;
export type OperatorFn = (context: IRuleContext, leftValue: unknown, rightValue: unknown) => unknown;
export type CustomFunctionFn = (context: IRuleContext) => unknown;

export interface Comparer {
  name: string;
  fn: ComparerFn;
}

export interface Action {
  name: string;
  fn: ActionFn;
}

export interface Operator {
  name: string;
  fn: OperatorFn;
}

export interface CustomFunction {
  name: string;
  fn: CustomFunctionFn;
}
