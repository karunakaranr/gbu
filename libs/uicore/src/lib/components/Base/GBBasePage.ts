// import { GBFormService } from '@goodbooks/uicore';
import { HttpClient } from '@angular/common/http';
import { AfterViewInit, Component, Inject, QueryList, ViewChildren } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';

import { Subscription } from 'rxjs';

import { GBBaseFormGroup, GBDataFormGroup, GBDataFormGroupStore, GBDataFormGroupStoreWN, GBDataFormGroupWN, GBFormGroup } from '../../classes/GBFormGroup';
import { GBDataPageService, GBDataPageServiceNG, GBDataPageServiceWN, GBPageService } from '../../services/gbpage.service';
import { FormTemplateDirective } from '../../directives/formtemplate.directive';
import { Store } from '@ngxs/store';
import { GBHttpService } from '@goodbooks/gbcommon';
import { MatDialog } from '@angular/material/dialog';
import { FormService } from '../../services/FormServiceCall';
import { GbformactionsComponent } from 'features/layout/components/gbmaincontent/gbformheader/gbformactions/gbformactions.component'
// import { GBFormService } from '../services/gbform.service';

@Component({
  template: 'app-gb-page',
})
export abstract class GBBasePageComponent {
  abstract title: string;
  abstract isDataPage: boolean = false;
  abstract hasNavigation: boolean = false;
  // abstract form: GBBaseFormGroup;
  // constructor(protected gbps: GBPageService) {

  // }
  constructor(@Inject('GBPageService') protected gbps: GBPageService, public activeroute: ActivatedRoute,  public store : Store) {
    this.thisConstructor();
  }
  abstract thisConstructor(): void;
}
@Component({
  template: 'app-gb-page',
})
export abstract class GBBasePageComponentNG extends GBBasePageComponent {
  isDataPage = true;
  hasNavigation = false;
  abstract form: GBBaseFormGroup;
  constructor(@Inject('PageService') protected gbps: GBDataPageServiceNG,public activeroute: ActivatedRoute,  public store : Store) {
    super(gbps,activeroute,store);
  }

  // template support...
  @ViewChildren(FormTemplateDirective) formTemplates: QueryList<FormTemplateDirective>;
  _currentFormName: string = 'default';
  public get currentFormName(): string {
    return this._currentFormName;
  }
  public set currentFormName(newVal: string) {
    this._currentFormName = newVal;
  }
  public get templateNames(): string[] {
    if (this.formTemplates) {
      return this.formTemplates.map(e => e.name).filter(e => e && e != '');
    }
    else {
      return [];
    }
  }
  public get currentForm() {
    if (this.currentFormName === null || this.currentFormName === '') return null;
    return this.formTemplates?.toArray().find(x => x.name.toLowerCase() == this.currentFormName.toLowerCase())?.template;
  }
}
@Component({
  template: 'app-gb-page',
})
export abstract class GBBaseDataPageComponent<T> extends GBBasePageComponentNG {
  abstract form: GBDataFormGroup<T>;
  isDataPage = true;
  hasNavigation = false;
  constructor(@Inject('PageService') protected gbps: GBDataPageService<T>,public activeroute: ActivatedRoute, public store: Store) {
    super(gbps,activeroute,store);
  }
  // constructor(protected fb: FormBuilder, protected http: HttpClient) {
  // }
}

@Component({
  template: 'app-gb-page',
})
export abstract class GBBaseDataPageComponentWN<T> extends GBBaseDataPageComponent<T> {
  abstract form: GBDataFormGroupWN<T>;
  hasNavigation = true;
  constructor(public formaction:GbformactionsComponent,@Inject('PageService') protected gbps: GBDataPageServiceWN<T>,public route: ActivatedRoute, private router: Router, public store : Store, public http: GBHttpService,public dialog: MatDialog,public service: FormService,public localhttp: HttpClient, public formBuilder: FormBuilder) {
    super(gbps,route,store);
  }
}

@Component({
  template: 'app-gb-page',
})
export abstract class GBBaseDataPageStoreComponent<T> extends GBBasePageComponentNG {
  abstract form: GBDataFormGroupStore<T>;
  isDataPage = true;
  protected formSubscription: Subscription = new Subscription();
  constructor(@Inject('PageService') protected gbps: GBDataPageService<T>,public activeroute: ActivatedRoute, public store : Store) {
    super(gbps,activeroute,store);
  }
  // constructor(protected fb: FormBuilder, protected http: HttpClient) {
  // }
}

@Component({
  template: 'app-gb-page',
})
export abstract class GBBaseDataPageStoreComponentWN<T> extends GBBaseDataPageStoreComponent<T> {
  abstract form: GBDataFormGroupStoreWN<T>;
  hasNavigation = true;
  constructor(@Inject('PageService') protected gbps: GBDataPageServiceWN<T>,public activeroute: ActivatedRoute,  public store : Store) {
    super(gbps,activeroute,store);
  }
}
//List component base class's
@Component({
  template: 'app-gb-page',
})
export abstract class GBBaseListPageComponent {
  abstract title: string;
  abstract isDataPage: boolean = false;
  abstract hasNavigation: boolean = false;
  // abstract formurl:string;
  // abstract rowdata:[];
  // abstract colmdata:[];
  // abstract defaultcolm:[];

  constructor(@Inject('GBPageService') protected gbps: GBPageService) {
    this.thisConstructor();
  }
  abstract thisConstructor(): void;
}

@Component({
  template: 'app-gb-page',
})
export abstract class GBBaseListPageComponentNG extends GBBaseListPageComponent {
  isDataPage = true;
  hasNavigation = false;
  constructor(@Inject('PageService') protected gbps: GBDataPageServiceNG) {
    super(gbps);
  }
}

export abstract class GBBaseListDataPageStoreComponent<T> extends GBBaseListPageComponentNG {
  isDataPage = true;
  protected formSubscription: Subscription = new Subscription();
  constructor(@Inject('PageService') protected gbps: GBDataPageService<T>) {
    super(gbps);
  }
  // constructor(protected fb: FormBuilder, protected http: HttpClient) {
  // }
}


@Component({
  template: 'app-gb-page',
})
export abstract class GBBaseListDataStoreComponentWN<T> extends GBBaseListDataPageStoreComponent<T> {
  hasNavigation = false;
  constructor(@Inject('PageService') protected gbps: GBDataPageServiceWN<T>) {
    super(gbps);
  }
}