import { Injectable } from '@angular/core';
import { GBHttpService } from '@goodbooks/gbcommon';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { IScreen } from '../interfaces/ibasefield';
import { HttpClient } from '@angular/common/http';
const screenjson = require('./../assets/FormJSONPath.json');
@Injectable()
export class GBFormService {
  endPoint: string;
  constructor(protected http: GBHttpService,private https:HttpClient) {
  }

  getByID(id: string): Observable<IScreen> {
    const jsonFile = `assets/FormJSONS/`+id+`.json`;
    return this.https.get(jsonFile)
      .pipe(
        map(res => {
          console.log("JSON Detail:", res)
          return (res as IScreen);
        })
      );
  }

  getByName(name: string): Observable<IScreen> {
    const jsonFile = `assets/FormJSONS/`+name+`.json`;
    return this.https.get(jsonFile)
      .pipe(
        map(res => {
          console.log("JSON Detail:", res)
          return (res as IScreen);
        })
      );
  }

  getformdata(id: string): Observable<IScreen> {
    const jsonFile = `assets/FormJSONS/`+id+`.json`;
    return this.https.get(jsonFile)
      .pipe(
        map(res => {
          console.log("JSON Detail:", res)
          return (res as IScreen);
        })
      );
  }
}
