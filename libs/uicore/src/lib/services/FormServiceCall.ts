import { Injectable } from '@angular/core';
import { GBHttpService } from '@goodbooks/gbcommon';
import { GridUrls } from '../URL/GridUrl';
import { IdFields } from '../URL/IdField';
import { FormdbService } from '../dbservice/FormDb.Service';
@Injectable({
  providedIn: 'root'    
})
export class FormService {
  constructor(public http: GBHttpService,public dbservice:FormdbService) {
  }

  public getcheckbalance() {
    console.log("Service")
    return this.dbservice.getcheckbalance();
  }

  public getService(url:string,id:number,idfield:string){
    let serviceurl = url+idfield+id;
    return this.http.httpget(serviceurl);
  }

  public getGridService(url:string,id:number,idfield:string){
    let serviceurl = GridUrls[url]+IdFields[idfield]+id;
    return this.http.httpget(serviceurl);
  }

  public DeleteRowData(url:string){
    return this.http.httpdelete(url);
  }

}
