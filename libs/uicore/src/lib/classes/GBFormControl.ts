import { AbstractControl, AbstractControlOptions, AsyncValidatorFn, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';

import { IField } from '../interfaces/ibasefield';

export class GBFormControl extends FormControl {
  myvalidatorOrOpts?: ValidatorFn | ValidatorFn[] | AbstractControlOptions;
  public ErrMessages: { [key: string]: string } = {};

  constructor(public field: IField, formState?: any, validatorOrOpts?: ValidatorFn | ValidatorFn[] | AbstractControlOptions, asyncValidator?: AsyncValidatorFn | AsyncValidatorFn[]) {
    super(formState, validatorOrOpts, asyncValidator);
    // let newValidatorOrOpts: ValidatorFn | ValidatorFn[] | AbstractControlOptions;
    // newValidatorOrOpts = validatorOrOpts;
    let newValidFns: ValidatorFn[] = [];

    if (Array.isArray(validatorOrOpts)) {
      newValidFns = newValidFns.concat(validatorOrOpts);
    }
    if (field.Required) {
      newValidFns.push(Validators.required);
    }
    if (field.ValidRegEx) {
      newValidFns.push(Validators.pattern(field.ValidRegEx));
    }
    if (field.Length) {
      newValidFns.push(Validators.maxLength(field.Length));
    }
    if (field.MaxValue) {
      newValidFns.push(Validators.max(field.MaxValue));
    }
    if (field.MinValue) {
      newValidFns.push(Validators.min(field.MinValue));
    }
    if (field.Type?.toLowerCase() === 'email') {
      newValidFns.push(Validators.email);
    }
    this.setValidators(newValidFns);
    this.updateValueAndValidity();

    this.myvalidatorOrOpts = newValidFns;
  }

  public addCustomvalidator(name: string, customValidatorOrOpts: ValidatorFn | ValidatorFn[] | AbstractControlOptions, errMessage?: string) {
    let newValidFns: ValidatorFn[] = [];
    if (Array.isArray(this.myvalidatorOrOpts)) {
      newValidFns = newValidFns.concat(this.myvalidatorOrOpts);
    }
    if (Array.isArray(customValidatorOrOpts)) {
      newValidFns = newValidFns.concat(customValidatorOrOpts);
    }
    else if (typeof customValidatorOrOpts === 'function') {
      newValidFns.push(customValidatorOrOpts);
    }
    this.ErrMessages[name] = errMessage || '';
    this.setValidators(newValidFns);
    this.updateValueAndValidity();
  }

  public get hasCustomError(): boolean {
    let retValue = false;
    const controlErrors: ValidationErrors | null = this.errors;
    if (controlErrors != null) {
      Object.keys(controlErrors).forEach(element => {
        if (this.ErrMessages[element]) {
          retValue = true;
        }
      });
    }
    return retValue;
  }

  public get customErrorMessages(): string {
    let retMsg = '';
    const controlErrors: ValidationErrors | null = this.errors;
    if (controlErrors != null) {
      Object.keys(controlErrors).forEach(element => {
        if (this.ErrMessages[element]) {
          retMsg += this.ErrMessages[element] + '; ';
        }
      });
    }
    return retMsg;
  }
}

