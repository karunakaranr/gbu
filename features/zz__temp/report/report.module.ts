import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { AgGridModule } from 'ag-grid-angular';
import { AccountopeningComponent } from './Components/accountopening/accountopening.component';
import { ReportviewComponent } from './Components/reportview/reportview.component';

const routes: Routes = [
  {
    path: 'app-accountopening',
    component: AccountopeningComponent
  },
  {
    path: 'app-reportview',
    component: ReportviewComponent
  },
];

@NgModule({
  declarations: [
    AccountopeningComponent,
    ReportviewComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    AgGridModule.withComponents([])
  ],
  exports: [
    RouterModule,
    AccountopeningComponent,
    ReportviewComponent,
  
  ]
})
export class ReportModule { }
