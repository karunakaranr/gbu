import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ReportDBservice } from '../services/reportDB.service';
import { ReportObject } from '../models/grid';
import { Gridsetting } from '../models/gridsetting.model';

@Injectable({
  providedIn: 'root',
})

export class Reportservice {

  public columnDatareport: Gridsetting;
  public reportmeta: ReportObject;
  constructor(public ReportDB: ReportDBservice, private http: HttpClient) { }

  public reportmetadataservice(): Observable<ReportObject> {
    return this.ReportDB.reportmetadataDb(); 
  }
  public preparecolumndata(): Observable<Gridsetting[]> {
    return this.ReportDB.reportmetadataDb().pipe(
      map((metadata: ReportObject) => {
        const reportfields = metadata[0].ReportVsFieldsArray;
        const columnfields = Array<Gridsetting>();
        for (let i = 0; i < reportfields.length; i++) {
          if (reportfields[i].ReportVsFieldsIsDisplay === 0) {
            const columnrow = new Gridsetting();
            columnrow.headerName = reportfields[i].ReportVsFieldsFieldTitle;
            columnrow.field = reportfields[i].ReportVsFieldsFieldName;
            columnfields.push(columnrow);
          }
        }
        return columnfields;
      })
    );
  }

  public reportdataservice(): Observable<any> {
    return this.ReportDB.reportdataDb();
  }
 
}

