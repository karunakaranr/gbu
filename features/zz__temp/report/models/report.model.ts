export class ReportDetail {
  Id: number;
  Code?: any;
  Name?: any;
  AccountId: number;
  AccountCode: string;
  AccountName: string;
  SubAccountId: number;
  SubAccountCode?: any;
  SubAccountName?: any;
  ControlAccountId: number;
  ControlAccountCode: string;
  ControlAccountName: string;
  AccountGroupId: number;
  AccountGroupCode: string;
  AccountGroupName: string;
  AccountScheduleId: number;
  AccountScheduleCode: string;
  AccountScheduleName: string;
  OUId: number;
  OUCode?: any;
  OUName?: any;
  PeriodId: number;
  PeriodCode?: any;
  PeriodName?: any;
  Opening: number;
  Debit: number;
  Credit: number;
  Closing: number;
  ReportType: string;
  GroupType?: any;
  SubPeriodName?: any;
  SubPeriodFromDate: Date;
  SubPeriodToDate: Date;
  NumberOfReocrds: number;
  Label: string;
  TotalDebit: number;
  TotalCredit: number;
  FinalOpening: number;
  FinalDebit: number;
  FinalCredit: number;
  FinalClosing: number;
}

export class AttributesCriteriaList {
  FieldName: string;
  OperationType: number;
  FieldValue: string;
  InArray?: any;
  JoinType: number;
  CriteriaAttributeName: string;
  CriteriaAttributeValue: string;
  IsCompulsory: number;
  IsHeader: number;
  CriteriaAttributeType: number;
  CriteriaAttributeId: number;
  FilterType: number;
  CriteriaFieldDisplayValue: string;
}

export class SectionCriteriaList {
  SectionId: number;
  AttributesCriteriaList: AttributesCriteriaList[];
  OperationType: number;
}

export class ReportCriteria {
  Id: number;
  SectionCriteriaList: SectionCriteriaList[];
}

export class ReportAttributeCriteria {
  FieldName: string;
  OperationType: number;
  FieldValue: string;
  InArray?: any;
  JoinType: number;
  CriteriaAttributeName: string;
  CriteriaAttributeValue: string;
  IsCompulsory: number;
  IsHeader: number;
  CriteriaAttributeType: number;
  CriteriaAttributeId: number;
  FilterType: number;
  CriteriaFieldDisplayValue: string;
}

export interface ReportPreferenceSetting {
  UserSettingsId: number;
  UserSettingsUserId: number;
  UserSettingsRoleId: number;
  UserSettingsDateFormat: string;
  UserSettingsCurrencyFormat: string;
  UserSettingsIsZeroSupressed: boolean;
  UserSettingsIsTruncate: boolean;
  UserSettingsQuantityFormat: string;
}

export interface ReportHeader {
  CompanyId: number;
  CompanyCode: string;
  CompanyName: string;
  BranchId: number;
  BranchCode: string;
  BranchName: string;
  DivisionId: number;
  DivisionCode: string;
  DivisionName: string;
  OuId: number;
  OuCode: string;
  OuName: string;
  OUDisplayName: string;
  AddressDetail?: any;
  CompanyAddressDetail?: any;
  ReportTitle: string;
  ReportTitleOptional: string;
  ReportPath: string;
  ReportFolder: string;
  CompanyLogo1: string;
  CompanyLogo2: string;
  IsPrintLogo: boolean;
}

export interface ReportFooter {
  IsoReferenceNumber?: any;
  ReportCode: string;
  ReportFormatCode: string;
  DataSourceFileName: string;
  ReportFile: string;
  ReportFormatVersion: number;
  ReportFormatModifiedOn: Date;
}

export interface RootObject {
  CompanyId: number;
  CompanyCode: string;
  CompanyName: string;
  CINNumber: string;
  BranchId: number;
  BranchCode: string;
  BranchName: string;
  DivisionId: number;
  DivisionCode: string;
  DivisionName: string;
  OuId: number;
  OuCode: string;
  OuName: string;
  AddressDetail?: any;
  CompanyAddressDetail?: any;
  ReportDetail: ReportDetail[];
  ReportCriteria: ReportCriteria;
  ReportAttributeCriteria: ReportAttributeCriteria[];
  ReportPreferenceSetting: ReportPreferenceSetting;
  ReportTitle: string;
  ReportTitleOptional: string;
  ReportPath: string;
  ReportFolder: string;
  IsoReferenceNumber?: any;
  IsPrintLogo: boolean;
  IsTimeStamp: boolean;
  ReportPrintMode: number;
  ReportChartType: number;
  CompanyLogo1: string;
  CompanyLogo2: string;
  OULogo1: string;
  OULogo2: string;
  OUDisplayName: string;
  OUShortName: string;
  ReportHeader: ReportHeader;
  ReportFooter: ReportFooter;
  ReportViewId: number;
  DatabaseName: string;
  ReportViewDynamicFooter?: any;
  TimeFormat: string;
  QuantityFormat: string;
  ServiceOffSet: number;
}

export interface GridSetting {
  headerName: string;
  field: string;
  sortable: boolean;
  filter: boolean;
  checkboxSelection: boolean;
  resizable: boolean;
  editable: boolean;
  pinned: string;
  cellEditor: string;
  cellEditorParams: string;
}
export interface Criteria {
  FieldName: string;
  OperationType: number;
  FieldValue: number;
  JoinType: number;
  CriteriaAttributeName: string;
  CriteriaAttributeValue: string;
  IsHeader: number;
  IsCompulsory: number;
  CriteriaAttributeId: number;
  CriteriaAttributeType: number;
  FilterType: number;
}
