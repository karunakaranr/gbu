export class AttributesCriteria {
    FieldName: string;
    OperationType: number;
    FieldValue: string;
    JoinType: number;
    CriteriaAttributeName: string;
    CriteriaAttributeValue: string;
    IsHeader: number;
    IsCompulsory: number;
    CriteriaAttributeId: number;
    CriteriaAttributeType: number;
    FilterType: number;
}

export class CriteriaObject {
    SectionId: number;
    AttributesCriteriaList: AttributesCriteria[];
    OperationType: number;
}
