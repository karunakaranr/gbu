import { Component} from '@angular/core';

@Component({
  selector: 'app-mapgeoplotting',
  templateUrl: './mapgeoplotting.component.html',
  styleUrls: ['./mapgeoplotting.component.scss']
})
export class mapgeoplottingComponent  {
  title = 'Markers';
   

  lat = 10.8091781;

  long = 78.2885026;
  
  zoom=7;
 

  markers = [
        { 
            lat:11.0116775,

            lng: 76.8271464,

            label: 'coimbatore'

        },
        {
    
            lat: 11.1085742,

            lng: 77.2937718,

            label: 'thirupur'

        },
        {
          
            lat: 11.6537266,

            lng: 78.0682561,

            label: 'salem'

        }
    ];
  }