import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgSelectModule } from '@ng-select/ng-select';
import {MapcomponentComponent} from './components/mapcomponent/mapcomponent.component'
import {mapdirectionComponent} from './components/mapdirection/mapdirection.component'
import {mapgeoplottingComponent} from './components/mapgeoplotting/mapgeoplotting.component'
import {geofencingComponent} from './components/geofencing/geofencing.component'
import { AgmCoreModule } from '@agm/core';
import { AgmDirectionModule } from 'agm-direction';
import { MatListModule } from '@angular/material/list'
import { MatFormFieldModule } from '@angular/material/form-field';
import { TranslocoRootModule } from './../transloco/transloco-root.module';
@NgModule({
  declarations: [  MapcomponentComponent,
    mapdirectionComponent,
    mapgeoplottingComponent,
    geofencingComponent
  ],
  imports: [
    CommonModule,
    NgSelectModule,
    MatListModule,
    MatFormFieldModule,
    TranslocoRootModule,
    AgmCoreModule.forRoot({ 
      // please get your own API key here:
      // https://developers.google.com/maps/documentation/javascript/get-api-key?hl=en
      apiKey: 'AIzaSyDvFSDxJXXR1nrdRDIzvp6DRZB9v649My8' ,
      libraries: ['places' ,'geometry','drawing']
    }),
    AgmDirectionModule,
  ],
  exports:[  MapcomponentComponent,
    mapdirectionComponent,
    mapgeoplottingComponent,
    geofencingComponent]
})
export class geolocationModule { }
