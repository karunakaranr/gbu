import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxsModule } from '@ngxs/store';
import { NgxsFormPluginModule } from '@ngxs/form-plugin';
import { ReportState} from './datastore/commonreport.state';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonreportComponent } from './screens/commonreport.component';
import { HTMLViewComponent } from './screens/html/htmlview.component';
import { GbgridModule } from './../gbgrid/components/gbgrid/gbgrid.module';
import { RouterModule, Routes } from '@angular/router';
import { CommonReportRoutingModule } from './../commonreport/commonreport.routing.module'
import { DynamicModule } from 'ng-dynamic-component';
import { CommonCustomHTMLComponent } from './screens/CommonCustomHTML/CommonCustomHTML.component';
import { SharedService } from './service/datapassing.service';

@NgModule({
  declarations: [CommonreportComponent, HTMLViewComponent, CommonCustomHTMLComponent],
  imports: [CommonModule,
    FormsModule,
    DynamicModule,
    ReactiveFormsModule,
    CommonReportRoutingModule,
    RouterModule,
    NgxsModule.forFeature([ReportState]),
    NgxsFormPluginModule.forRoot(),
    GbgridModule],
  providers: [SharedService],
  exports: [CommonreportComponent, HTMLViewComponent, CommonCustomHTMLComponent]
})
export class CommonReportModule { }
