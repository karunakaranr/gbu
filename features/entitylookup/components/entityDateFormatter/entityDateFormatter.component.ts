import {Component} from "@angular/core";
// import { DateFormaterPipe } from "libs/gbcommon/src/lib/Date-Formater.pipe";
import { DatePipe } from '@angular/common';
@Component({
    selector: 'app-entityDateFormatter',
    templateUrl: './entityDateFormatter.component.html',
    styleUrls: ['./entityDateFormatter.component.scss'],
})
export class EntityDateFormatteromponent {
    public params: any;
    // constructor(private DatePipe: DateFormaterPipe) {}
    agInit(params: any): void {
        var datePipe = new DatePipe("en-US");
        this.params = params.value
        if(typeof this.params == 'string' && this.params.substring(1,5) == 'Date'){
            this.params = datePipe.transform(JSON.parse(this.params.replace("/Date(", "").replace(")/", "")), 'dd/MMM/YYYY')
        }
    }
}