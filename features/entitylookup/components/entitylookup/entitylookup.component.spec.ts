import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EntitylookupComponent } from './entitylookup.component';

describe('EntitylookupComponent', () => {
  let component: EntitylookupComponent;
  let fixture: ComponentFixture<EntitylookupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EntitylookupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EntitylookupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
