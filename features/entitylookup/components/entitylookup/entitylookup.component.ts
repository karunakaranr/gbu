import { Component, OnInit, Input, Output, EventEmitter, ViewChild, forwardRef, ElementRef, Injector, ChangeDetectorRef, OnChanges } from '@angular/core';
import { NG_VALUE_ACCESSOR, FormControl, ControlValueAccessor, NgControl, FormBuilder, FormGroup, Validators, NG_VALIDATORS, ValidationErrors, AbstractControl } from '@angular/forms';
import { Entitylookupservice } from './../../services/entitylookup/entitylookup.service';
import { NgSelectComponent } from '@ng-select/ng-select';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { EntitygridComponent } from './../../components/entitygrid/entitygrid.component';
import { Select, Store } from '@ngxs/store';
import { Entitylookupdependpicklist, GbentityDTO } from './../../store/entitylookup.action';
import { EntityState } from './../../store/entitylookup.state';
import { Observable } from 'rxjs';
import { IcolumnDefs, IPicklistObj, IDependentPicklist, IConfig } from './../../model/picklist.model';
import { LayoutState } from 'features/layout/store/layout.state';
import { IPickListDetail } from 'libs/uicore/src/lib/interfaces/ibasefield';
import { EntityDateFormatteromponent } from '../entityDateFormatter/entityDateFormatter.component';

const datajson = require('./../../../../apps/Goodbooks/Goodbooks/src/assets/mockdata/_general/entitylookupdata.json');

@Component({
  selector: 'app-entitylookup',
  templateUrl: './entitylookup.component.html',
  styleUrls: ['./entitylookup.component.scss'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => EntitylookupComponent),
    multi: true
  },
  {
    provide: NG_VALIDATORS,
    useExisting: forwardRef(() => EntitylookupComponent),
    multi: true
  }]
})
export class EntitylookupComponent implements OnInit, OnChanges, ControlValueAccessor {
  lookupform: FormGroup;
  config: IConfig;
  selectedAccount;
  selectedAccountId: string[] = [];
  isFocused: boolean = false;
  columnDefs;
  @Input() PicklistObj: IPickListDetail;
  @Input() picklisturl: string;
  @Input() dependpicklist: IDependentPicklist | string;
  @Input() defaultvalue: any;
  @Input() picklistsearchfields: string;
  @Input() label: string;
  @Input() picklistselection: string;
  @Input() picklisttitle: string;
  @Input() picklistdisplaytext: string;
  @Input() FieldName: string;
  @Input() multiple: boolean;
  @Input() width: number;
  @Input() isFilter: boolean;
  @Input() LinkId: string;
  @Input() GridIndex: number;
  @Output() Onselectdata: EventEmitter<any> = new EventEmitter<string>();
  @Output() Onselectdataids: EventEmitter<any> = new EventEmitter<string>();
  @Output() PickListOutput: EventEmitter<any> = new EventEmitter<string>();
  @ViewChild(NgSelectComponent) ngselectbox: NgSelectComponent;
  @Select(LayoutState.FormEdit) edit$: Observable<boolean>;
  isnoteditable: boolean = true;
  isPicklistChange: boolean = true;
  PicklistDisplayLabel: string;
  ngControl;
  readonly: boolean = false;
  picklistobj: IPicklistObj;
  PicklistURL: string;
  AddNew: boolean = false;
  PlaceHolder: string = 'Please Select';
  MaxLength: number = 9999;
  ColumnWidth: string = "";
  ColumnType: string = "";
  isKeyField: boolean = false;
  PickListWithCommas: boolean = false;
  LabelWidth: number;
  LinkValue: string;
  Filterdata: string[] = [];
  public typeahead = new EventEmitter<string>();
  @ViewChild("myinput") myInputField: ElementRef;

  constructor(public service: Entitylookupservice, public dialog: MatDialog,
    public elRef: ElementRef, public injector: Injector, private fb: FormBuilder, public store: Store, public ref: ChangeDetectorRef) {
    this.lookupform = this.fb.group({
      lookupdata: new FormControl('', [Validators.required]),
    });
  }

  ngAfterViewInit() {
  }

  ngOnChanges(): void {
    if (this.isFilter == undefined && (this.isPicklistChange || this.GridIndex != undefined)) {
      if (this.PicklistObj != undefined) {
        this.isPicklistChange = false;
        this.picklisturl = this.PicklistObj.PicklistUrl;
        this.picklistsearchfields = this.PicklistObj.PicklistSearchFields;
        this.label = this.PicklistObj.Label;
        this.picklistselection = this.PicklistObj.PicklistSelection;
        this.picklisttitle = this.PicklistObj.PicklistTitle;
        this.picklistdisplaytext = this.PicklistObj.PicklistDisplayText;
        this.multiple = this.PicklistObj.Multiple;
        this.width = this.PicklistObj.Width;
        this.LinkId = this.PicklistObj.LinkId;
        this.AddNew = this.PicklistObj.AddNew;
        this.PlaceHolder = this.PicklistObj.PlaceHolder;
        this.MaxLength = this.PicklistObj.MaxLength;
        this.ColumnWidth = this.PicklistObj.ColumnWidth;
        this.ColumnType = this.PicklistObj.ColumnType;
        this.isKeyField = this.PicklistObj.isKeyField;
        this.LabelWidth = this.PicklistObj.LabelWidth;
        this.LinkValue = this.PicklistObj.LinkValue;
        if (this.PicklistObj.PickListWithCommas != undefined) {
          this.PickListWithCommas = this.PicklistObj.PickListWithCommas
        }
        if (this.PicklistObj.PicklistCriteria != undefined) {
          this.dependpicklist = this.PicklistObj.PicklistCriteria
        }
        if (this.PicklistObj.ReadOnly != undefined) {
          console.log("ReadOnly:", this.PicklistObj.ReadOnly)
          this.readonly = this.PicklistObj.ReadOnly
        } else {
          this.readonly = false
        }
        if (this.PicklistObj.PicklistDisplayLabel != undefined) {
          this.PicklistDisplayLabel = this.PicklistObj.PicklistDisplayLabel
        } else {
          this.PicklistDisplayLabel = this.PicklistObj.PicklistDisplayText
        }
        // if(this.isKeyField == true){
        //   this.myInputField.nativeElement.focus();
        // }
        if (this.multiple == false) {
          this.multiple = false;
        } else {
          this.multiple = true;
        }
        if (this.isFilter != true && this.picklisturl != undefined) {
          this.PicklistURL = datajson[this.picklisturl]
        }
        else if (this.picklisturl != undefined) {
          this.PicklistURL = this.picklisturl
        }
        this.columnDefs = [];
        let columnlabel = this.PicklistDisplayLabel.split(',')
        let columnDefs = this.picklistdisplaytext.split(",")
        let ColumnWidth = this.ColumnWidth.split(",")
        let ColumnType = this.ColumnType.split(",")
        for (let i = 0; i < columnDefs.length; i++) {
          if (ColumnType[i] == undefined) {
            ColumnType[i] = "";
          }
          if (ColumnWidth[i] == undefined) {
            ColumnWidth[i] = "";
          }
          if (i == 0) {
            this.columnDefs.push({
              "headerName": columnlabel[i],
              "field": columnDefs[i],
              "sortable": true,
              "headerCheckboxSelection":this.multiple,
              "checkboxSelection": this.multiple,
              "width": ColumnWidth[i] == "" ? 200 : parseInt(ColumnWidth[i]),
              "cellRendererFramework": ColumnType[i].toLowerCase() == 'date' ? EntityDateFormatteromponent : ''
            })
          }
          else {
            this.columnDefs.push({
              "headerName": columnlabel[i],
              "field": columnDefs[i],
              "sortable": true,
              "headerCheckboxSelection":false,
              "checkboxSelection": false,
              "width": ColumnWidth[i] == "" ? 200 : parseInt(ColumnWidth[i]),
              "cellRendererFramework": ColumnType[i].toLowerCase() == 'date' ? EntityDateFormatteromponent : ''
            })
          }
        }
        this.config = datajson;
        let displaycolumnsforadfilter: [
          {
            "label": "Code",
            "field": "Code"
          },
          {
            "label": "Name",
            "field": "Name"
          }
        ]
        this.config = {
          expandable: true, displaycolumns: displaycolumnsforadfilter, bindlabel: "Code", AllowAdvanceFinder: true, griddata:
          {
            "columnDefs": this.columnDefs
          }
        }
        if (this.isFilter != true) {
          if (this.defaultvalue != undefined || this.defaultvalue != null) {
            let obj = Object.keys(this.defaultvalue)
            if (obj[0] != 'lookupdata') {
              if (typeof this.defaultvalue == 'object') {
                if (this.defaultvalue.lookupdata != null && this.defaultvalue.lookupdata.toString().length > 0) {
                  this.PlaceHolder = ''
                }
                else {
                  this.PlaceHolder = this.PicklistObj.PlaceHolder;
                  this.selectedAccount = null
                }
              }
              else {
                if (this.defaultvalue.length == 0) {
                  this.PlaceHolder = this.PicklistObj.PlaceHolder
                  this.defaultvalue = null
                }
                else {
                  this.PlaceHolder = ''
                }
              }
              if (this.multiple == true) {
                if (this.defaultvalue == "") {
                  this.lookupform.get('lookupdata').patchValue("");
                } else {
                  if(this.defaultvalue!=null){
                    if(typeof this.defaultvalue == 'string'){
                      this.defaultvalue = this.defaultvalue.split(',')
                    }
                    let Selectedlookupdata = [];
                    for (var i = 0; i < this.defaultvalue.length; i++) {
                      Selectedlookupdata.push(this.defaultvalue[i]);
                    }
                    this.lookupform.get('lookupdata').patchValue(Selectedlookupdata);
                    this.selectedAccount = Selectedlookupdata;
                    this.PlaceHolder = '';
                  } else {
                    let Selectedlookupdata = [];
                    this.lookupform.get('lookupdata').patchValue(Selectedlookupdata);
                    this.selectedAccount = Selectedlookupdata;
                  }
                }
              }
              else {
                this.selectedAccount = this.defaultvalue
              }
            } else {
              this.selectedAccount = this.defaultvalue
            }
          }
          else {
            this.defaultvalue = null
            this.selectedAccount = null
          }
        }
      }
    } else {
      if (this.multiple == false) {
        this.multiple = false;
      } else {
        this.multiple = true;
      }
      if (this.isFilter != true) {
        // this.AddNew = true;
        if (this.PicklistObj.SLno == 1) {
          this.myInputField.nativeElement.focus();
        }
        if (this.defaultvalue != undefined || this.defaultvalue != null) {
          if (typeof this.defaultvalue == 'object') {
            if (this.defaultvalue.lookupdata != null && this.defaultvalue.lookupdata.toString().length > 0) {
              this.PlaceHolder = ''
            }
            else {
              this.PlaceHolder = this.PicklistObj.PlaceHolder;
              this.selectedAccount = null
            }
          }
          else {
            this.PlaceHolder = ''
          }
          let obj = Object.keys(this.defaultvalue)
          if (obj[0] != 'lookupdata') {
            if (this.multiple == true) {
              if (this.defaultvalue == "") {
                this.lookupform.get('lookupdata').patchValue("");
              } else {
                if(typeof this.defaultvalue == 'string'){
                  this.defaultvalue = this.defaultvalue.split(',')
                }
                let Selectedlookupdata = [];
                for (var i = 0; i < this.defaultvalue.length; i++) {
                  Selectedlookupdata.push(this.defaultvalue[i]);
                }
                this.lookupform.get('lookupdata').patchValue(Selectedlookupdata);
                this.selectedAccount = Selectedlookupdata;
              }
            }
            else {
              this.selectedAccount = this.defaultvalue
            }
          }
        }
        else {
          this.defaultvalue = ''
          this.selectedAccount = ''
        }
      }
    }
  }

  setStyle(name: string, value: string) {
    document.documentElement.style.setProperty(name, value);
  }

  ngOnInit(): void {
    this.setStyle('--editmode', 'block');
    if (this.isFilter != true) {
      this.edit$.subscribe(res => {
        this.isnoteditable = res;
        if (this.isnoteditable) {
          this.setStyle('--editmode', 'block');
        } else {
          this.setStyle('--editmode', 'none');
        }
      })
    }
    if (this.picklistdisplaytext == undefined && (this.isPicklistChange || this.GridIndex != undefined)) {
      if (this.PicklistObj != undefined) {
        this.isPicklistChange = false;
        this.picklisturl = this.PicklistObj.PicklistUrl;
        this.picklistsearchfields = this.PicklistObj.PicklistSearchFields;
        this.label = this.PicklistObj.Label;
        this.picklistselection = this.PicklistObj.PicklistSelection;
        this.picklisttitle = this.PicklistObj.PicklistTitle;
        this.picklistdisplaytext = this.PicklistObj.PicklistDisplayText;
        this.multiple = this.PicklistObj.Multiple;
        this.width = this.PicklistObj.Width;
        this.LinkId = this.PicklistObj.LinkId;
        this.AddNew = this.PicklistObj.AddNew;
        this.MaxLength = this.PicklistObj.MaxLength;
        this.ColumnWidth = this.PicklistObj.ColumnWidth;
        this.ColumnType = this.PicklistObj.ColumnType;
        this.isKeyField = this.PicklistObj.isKeyField;
        this.LinkValue = this.PicklistObj.LinkValue;
        if (this.PicklistObj.PickListWithCommas != undefined) {
          this.PickListWithCommas = this.PicklistObj.PickListWithCommas
        }
        if (this.PicklistObj.PicklistCriteria != undefined) {
          this.dependpicklist = this.PicklistObj.PicklistCriteria
        }
        if (this.PicklistObj.ReadOnly != undefined) {
          console.log("ReadOnly:", this.PicklistObj.ReadOnly)
          this.readonly = this.PicklistObj.ReadOnly
        } else {
          this.readonly = false
        }
        if (this.PicklistObj.PicklistDisplayLabel != undefined) {
          this.PicklistDisplayLabel = this.PicklistObj.PicklistDisplayLabel
        } else {
          this.PicklistDisplayLabel = this.PicklistObj.PicklistDisplayText
        }
        if (this.isFilter != true && this.picklisturl != undefined) {
          this.PicklistURL = datajson[this.picklisturl]
        }
        else if (this.picklisturl != undefined) {
          this.PicklistURL = this.picklisturl
        }
        if (this.isFilter != true) {
          this.edit$.subscribe(res => {
            if (typeof this.defaultvalue == 'object') {
              this.defaultvalue = this.defaultvalue.lookupdata;
            }
            this.isnoteditable = res;

            if (this.isnoteditable) {
              this.setStyle('--editmode', 'block');
            } else {
              this.setStyle('--editmode', 'none');
            }
          })
          if (this.defaultvalue != undefined || this.defaultvalue != null || this.defaultvalue.length > 0) {
            let obj = Object.keys(this.defaultvalue)
            if (obj[0] != 'lookupdata') {
              if (this.multiple == true) {
                if (this.defaultvalue == "") {
                  this.lookupform.get('lookupdata').patchValue("");
                } else {
                  if(typeof this.defaultvalue == 'string'){
                    this.defaultvalue = this.defaultvalue.split(',')
                  }
                  let Selectedlookupdata = [];
                  for (var i = 0; i < this.defaultvalue.length; i++) {
                    Selectedlookupdata.push(this.defaultvalue[i]);
                  }
                  this.lookupform.get('lookupdata').patchValue(Selectedlookupdata);
                  this.selectedAccount = Selectedlookupdata;
                  this.PlaceHolder = '';
                }
              }
              else {
                this.PlaceHolder = '';
                this.selectedAccount = this.defaultvalue
              }
            }
          }
        }
        this.ngControl = this.injector.get(NgControl);
        if (this.ngControl != null) {
          this.ngControl.valueAccessor = this;
        }
        this.columnDefs = [];
        let columnlabel = this.PicklistDisplayLabel.split(',')
        let columnDefs = this.picklistdisplaytext.split(",")
        let ColumnWidth = this.ColumnWidth.split(",")
        let ColumnType = this.ColumnType.split(",")
        for (let i = 0; i < columnDefs.length; i++) {
          if (ColumnType[i] == undefined) {
            ColumnType[i] = "";
          }
          if (ColumnWidth[i] == undefined) {
            ColumnWidth[i] = "";
          }
          if (i == 0) {
            this.columnDefs.push({
              "headerName": columnlabel[i],
              "field": columnDefs[i],
              "sortable": true,
              "headerCheckboxSelection":this.multiple,
              "checkboxSelection": this.multiple,
              "width": ColumnWidth[i] == "" ? 200 : parseInt(ColumnWidth[i]),
              "cellRendererFramework": ColumnType[i].toLowerCase() == 'date' ? EntityDateFormatteromponent : ''
            })
          }
          else {
            this.columnDefs.push({
              "headerName": columnlabel[i],
              "field": columnDefs[i],
              "sortable": true,
              "headerCheckboxSelection":false,
              "checkboxSelection": false,
              "width": ColumnWidth[i] == "" ? 200 : parseInt(ColumnWidth[i]),
              "cellRendererFramework": ColumnType[i].toLowerCase() == 'date' ? EntityDateFormatteromponent : ''
            })
          }
        }
        this.config = datajson;
        let displaycolumnsforadfilter: [
          {
            "label": "Code",
            "field": "Code"
          },
          {
            "label": "Name",
            "field": "Name"
          }
        ]
        this.config = {
          expandable: true, displaycolumns: displaycolumnsforadfilter, bindlabel: "Code", AllowAdvanceFinder: true, griddata:
          {
            "columnDefs": this.columnDefs
          }
        }
        if (this.defaultvalue && this.isFilter) {
          this.fetchconfig(this.defaultvalue);
        }
      }
    } else if (this.PicklistObj == undefined) {
      if (this.isFilter != true && this.picklisturl != undefined) {
        this.PicklistURL = datajson[this.picklisturl]
      }
      else if (this.picklisturl != undefined) {
        this.PicklistURL = this.picklisturl
      }
      if (this.isFilter != true) {
        // this.AddNew = true
        this.edit$.subscribe(res => {
          if (typeof this.defaultvalue == 'object') {
            this.defaultvalue = this.defaultvalue.lookupdata;
          }
          this.isnoteditable = res;
          if (this.isnoteditable) {
            this.setStyle('--editmode', 'block');
          } else {
            this.setStyle('--editmode', 'none');
          }
        })
        if (this.defaultvalue != undefined || this.defaultvalue != null || this.defaultvalue.length > 0) {
          let obj = Object.keys(this.defaultvalue)
          if (obj[0] != 'lookupdata') {
            if (typeof this.defaultvalue == 'object') {
              if (this.defaultvalue.lookupdata.toString().length > 0) {
                this.PlaceHolder = ''
              }
              else {
                this.PlaceHolder = this.PicklistObj.PlaceHolder;
                this.selectedAccount = null
              }
            }
            else {
              this.PlaceHolder = ''
            }
            if (this.multiple == true) {
              if (this.defaultvalue == "") {
                this.lookupform.get('lookupdata').patchValue("");
              } else {
                if(typeof this.defaultvalue == 'string'){
                  this.defaultvalue = this.defaultvalue.split(',')
                }
                let Selectedlookupdata = [];
                for (var i = 0; i < this.defaultvalue.length; i++) {
                  Selectedlookupdata.push(this.defaultvalue[i]);
                }
                this.lookupform.get('lookupdata').patchValue(Selectedlookupdata);
                this.selectedAccount = Selectedlookupdata;
                this.PlaceHolder = '';
              }
            }
            else {
              this.PlaceHolder = '';
              this.selectedAccount = this.defaultvalue
            }
          }
        }
      }
      this.ngControl = this.injector.get(NgControl);
      if (this.ngControl != null) {
        this.ngControl.valueAccessor = this;
      }
      this.columnDefs = []
      let columnDefs = this.picklistdisplaytext.split(",")
      let ColumnWidth = this.ColumnWidth.split(",")
      let ColumnType = this.ColumnType.split(",")
      for (let i = 0; i < columnDefs.length; i++) {
        if (ColumnType[i] == undefined) {
          ColumnType[i] = "";
        }
        if (ColumnWidth[i] == undefined) {
          ColumnWidth[i] = "";
        }
        if (i == 0) {
          this.columnDefs.push({
            "headerName": columnDefs[i],
            "field": columnDefs[i],
            "sortable": true,
            "headerCheckboxSelection":this.multiple,
            "checkboxSelection": this.multiple,
            "width": ColumnWidth[i] == "" ? 300 : parseInt(ColumnWidth[i]),
            "cellRendererFramework": ColumnType[i].toLowerCase() == 'date' ? EntityDateFormatteromponent : ''
          })
        }
        else {
          this.columnDefs.push({
            "headerName": columnDefs[i],
            "field": columnDefs[i],
            "sortable": true,
            "headerCheckboxSelection":false,
            "checkboxSelection": false,
            "width": ColumnWidth[i] == "" ? 200 : parseInt(ColumnWidth[i]),
            "cellRendererFramework": ColumnType[i].toLowerCase() == 'date' ? EntityDateFormatteromponent : ''
          })
        }
      }
      this.config = datajson;
      let displaycolumnsforadfilter: [
        {
          "label": "Code",
          "field": "Code"
        },
        {
          "label": "Name",
          "field": "Name"
        }
      ]
      this.config = {
        expandable: true, displaycolumns: displaycolumnsforadfilter, bindlabel: "Code", AllowAdvanceFinder: true, griddata:
        {
          "columnDefs": this.columnDefs
        }
      }
      if (this.defaultvalue && this.isFilter) {
        this.fetchconfig(this.defaultvalue);
      }
    }
    // if (this.defaultvalue && this.isFilter) {
    //   this.fetchconfig(this.defaultvalue);
    // }
  }

  public fetchconfig(defaultvalue: string) {
    this.picklistobj = {
      'picklistName': this.label,
      'searcheithercoloumn': this.picklistsearchfields,
      'url': this.PicklistURL,
      'searcheither': true,
      'dependpicklistvalue': this.dependpicklist,
      'isfilter': this.isFilter,
      'ismultiple':this.multiple
    }
    if (defaultvalue.length > 0) {
      let defaultvaluecri = this.picklistobj;
      this.service.defaultvalue(defaultvaluecri, this.GridIndex, '').subscribe(res => {
        for (let data of res) {
          if (data.Id == defaultvalue) {
            this.selectedAccount = [data]
          }
        }
      })
    }
  }
  @Select(EntityState.Entitylookupdependpicklistdata) filterformdata$: Observable<string>;

  // public Callservice(){
  //   // this.myInputField.nativeElement.focus();
  // }

  public Callservice() {
    this.PlaceHolder = '';
    if (this.isnoteditable) {
      let filterform;
      if (this.isFilter) {
        this.AddNew = false
        this.filterformdata$.subscribe(data => {
          filterform = JSON.stringify(data);
        })
        filterform = JSON.parse(filterform)
      } else {
        // this.isFocused=true;
        // this.myInputField.nativeElement.focus();
      }
      console.log("this.dependpicklist",this.dependpicklist)
      this.picklistobj = {
        'picklistName': this.label,
        'searcheithercoloumn': this.picklistsearchfields,
        'url': this.PicklistURL,
        'searcheither': true,
        'dependpicklistvalue': this.dependpicklist,
        'isfilter': this.isFilter,
        'ismultiple':this.multiple
      }
      let objvalue = this.picklistobj;
      this.service.Dataservice(objvalue, this.GridIndex, '').subscribe(response => {
        if (response) {
          console.log("this.picklistselection:", this.picklistselection)
          let dialogdata: MatDialogConfig = {
            data: {
              config: this.config,
              columnDefs: this.config.griddata.columnDefs,
              rowData: response,
              title: this.label,
              picklistsearchfields: this.picklistsearchfields,
              picklisturl: this.PicklistURL,
              dependpicklist: this.dependpicklist,
              multiple: this.multiple,
              button: true,
              addnew: this.AddNew,
              maxlength: this.MaxLength,
              gridindex: this.GridIndex,
              inputvalue: this.selectedAccount,
              inputid: this.selectedAccountId,
              filtervalue: this.Filterdata,
              displaytitle: this.picklisttitle,
              selectionid:this.picklistselection,
              IsFilter: this.isFilter
            }
          }
          this.dialog.open(EntitygridComponent, dialogdata).afterClosed().subscribe(data => {
            // console.log("Picklist Result Data:",data)
            let picklistdata = data
              if (this.isFilter) {
                let Selectedlookupdata
                if (data.length != 0) {
                  this.selectedAccount = data[0][this.picklisttitle]
                  let datalength = data.length;
                  let formdata = this.lookupform.get('lookupdata')
                  this.selectedAccount = data[0][this.picklisttitle]
                  Selectedlookupdata = data[0][this.picklisttitle];
                  this.lookupform.get('lookupdata').patchValue(Selectedlookupdata)
                  if (this.multiple == true) {
                    Selectedlookupdata = [];
                    // if (formdata.value != undefined) {
                    //   for (let data of formdata.value) {
                    //     Selectedlookupdata.push(data)
                    //   }
                    // }
                    this.Filterdata = []
                    for (var i = 0; i < datalength; i++) {
                      Selectedlookupdata.push(data[i]);
                      this.Filterdata.push(data[i][this.picklistselection])
                    }
                    this.lookupform.get('lookupdata').patchValue(Selectedlookupdata);
                  }
                } else {
                  this.PlaceHolder = "Please Select";
                  this.selectedAccount = data
                  let emitvalue = { SelectedData: -1, id: this.LinkId }
                  this.Onselectdataids.emit(emitvalue)
                }
                for (let data of filterform.AttributesCriteriaList) {
                  if (this.FieldName == data.FieldName) {
                    let flag = 0;
                    data['defaultvalue'] = ''
                    if(this.multiple == true && picklistdata.length != 0){
                      for (let item of Selectedlookupdata) {
                        flag = 1;
                        data['defaultvalue'] = data['defaultvalue'] + item[this.picklistselection] + ','
                      }
                      if (flag == 1) {
                        data['defaultvalue'] = data['defaultvalue'].slice(0, -1)
                      }
                      else {
                        data['defaultvalue'] = 'NONE'
                      }
                    } else {
                      if(typeof picklistdata == 'string'){
                        data['defaultvalue'] = picklistdata
                      } else {
                        data['defaultvalue'] = picklistdata[0][this.picklistselection]
                      }
                    }
                  }
                }
                this.store.dispatch(new Entitylookupdependpicklist(filterform));
                let emitvalue = [{ Picklist: this.FieldName, value: this.lookupform.value }]
                this.Onselectdata.emit(JSON.stringify(emitvalue))
              }
              else {
                if (data.length != 0) {
                  if (typeof data == "string") {
                    this.selectedAccount = data
                    let emitvalue = { SelectedData: -1, id: this.LinkId }
                    this.Onselectdataids.emit(emitvalue)
                    this.PickListOutput.emit(this.selectedAccount)
                  } else if (data[0] == undefined) {
                    this.selectedAccount = ""
                    let emitvalue = { SelectedData: -1, id: this.LinkId }
                    this.Onselectdataids.emit(emitvalue)
                    this.PickListOutput.emit(this.selectedAccount)
                  } else {
                    this.selectedAccount = data[0][this.picklisttitle]
                    let Selectedlookupdata = data[0][this.picklisttitle];
                    this.lookupform.get('lookupdata').patchValue(Selectedlookupdata);
                    if (this.multiple == true) {
                      let ids: string;
                      if (this.PickListWithCommas) {
                        ids = ",";
                      } else {
                        ids = "";
                      }
                      let codes: string = "";
                      let names: string = "";
                      for (let item of data) {
                        // for( let field of this.picklistsearchfields){(

                        // }
                        ids = ids + item[this.picklistselection] + ","
                        codes = codes + item.Code + ","
                        names = names + item.Name + ","
                      }
                      ids = ids.slice(0, -1);
                      codes = codes.slice(0, -1);
                      names = names.slice(0, -1);
                      Selectedlookupdata = []
                      this.selectedAccountId = []
                      for (var i = 0; i < data.length; i++) {
                        Selectedlookupdata.push(data[i][this.picklisttitle]);
                        this.selectedAccountId.push(data[i][this.picklistselection])
                      }
                      this.lookupform.get('lookupdata').patchValue(Selectedlookupdata);
                      let emitvalue = { SelectedData: ids, id: this.LinkId, value: this.LinkValue, Selected: data }
                      this.Onselectdataids.emit(emitvalue)
                      this.PickListOutput.emit(this.selectedAccount)
                    }
                    else {
                      let emitvaue = { SelectedData: data[0][this.picklistselection], Selected: data[0], id: this.LinkId, value: this.LinkValue }
                      this.Onselectdataids.emit(emitvaue)
                      this.Onselectdata.emit(data[0][this.picklistselection])
                      this.PickListOutput.emit(this.selectedAccount)
                    }
                  }
                } else{
                  this.PlaceHolder = this.PicklistObj.PlaceHolder;
                  this.selectedAccount = data
                  this.selectedAccountId = []
                  let emitvalue = { SelectedData: -1, id: this.LinkId }
                  this.Onselectdataids.emit(emitvalue)
                  this.PickListOutput.emit(this.selectedAccount)
                }
              }
          });
        }
      })
    }
  }

  public onRemove(event: any): void {
    if (this.isFilter) {
      let filterform;
      this.filterformdata$.subscribe(data => {
        filterform = JSON.stringify(data);
      })
      filterform = JSON.parse(filterform)
      for (let data of filterform.AttributesCriteriaList) {
        if (this.FieldName == data.FieldName) {
          let selectedvalue = data['defaultvalue'].split(',')
          let selected = ''
          for (let item of selectedvalue) {
            if (event.value[this.picklistselection] != item) {
              selected = selected + item + ','
            }
          }
          if (selected.length > 0) {
            data['defaultvalue'] = selected.slice(0, -1)
          }
        }
      }
      this.store.dispatch(new Entitylookupdependpicklist(filterform));
    } else {
      let ids = ""
      if (typeof this.selectedAccount[0] == 'object') {
        for (let item of this.selectedAccount) {
          ids = ids + item[this.picklistselection] + ","
        }
        ids = ids.slice(0, -1)
        let emitvalue = { SelectedData: ids, id: this.LinkId }
        this.Onselectdataids.emit(emitvalue)
      }
    }

  }

  public onClear() {
    console.log("onClear")
    this.Filterdata = []
    this.selectedAccountId = []
    if (this.isFilter) {
      let filterform;
      this.filterformdata$.subscribe(data => {
        filterform = JSON.stringify(data);
      })
      filterform = JSON.parse(filterform)
      for (let data of filterform.AttributesCriteriaList) {
        if (this.FieldName == data.FieldName) {
          data['defaultvalue'] = 'NONE'
        }
      }
      this.store.dispatch(new Entitylookupdependpicklist(filterform));
    } else {
      let emitvalue = { SelectedData: -1, id: this.LinkId }
      this.Onselectdataids.emit(emitvalue)
    }
  }

  public customSearchFn(term: string, item: any) {
    let returnval = false;
    term = term.toLocaleLowerCase();
    let fieldvalue = '';
    for (var pro in item) {
      fieldvalue = String(item[pro]);
      fieldvalue = fieldvalue.toLocaleLowerCase();
      returnval = returnval || fieldvalue.indexOf(term) > -1
    }
    return returnval
  }


  get value(): any {
    return this.lookupform.value;
  }
  set value(value) {
    this.lookupform.setValue(value);
    this.onChange(value);
    this.onTouched();
  }
  onChange: any = () => { };
  onTouched: any = () => { };

  registerOnChange(fn: any): void {
    this.lookupform.valueChanges.subscribe(fn);
  }

  writeValue(val: any): void {

  }

  setDisabledState?(isDisabled: boolean): void {
    isDisabled ? this.lookupform.disable() : this.lookupform.enable();
  }
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  validate(c: AbstractControl): ValidationErrors | null {
    this.store.dispatch(new GbentityDTO(this.lookupform.value, this.ref));
    return this.lookupform.valid ? null : {
      invalidForm: { valid: false, message: "invalid" }
    };
  }

  CreateNew(value) {
    this.lookupform = value;
  }


  check() {
    const data = "this.lookupform.value";
    // should hide on test
  }
}