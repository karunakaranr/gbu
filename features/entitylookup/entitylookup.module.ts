import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgOptionHighlightModule } from '@ng-select/ng-option-highlight';
import { EntitylookupComponent } from './../entitylookup/components/entitylookup/entitylookup.component';
import { MatCardModule} from '@angular/material/card';
import { MatIconModule} from '@angular/material/icon';
import { MatButtonModule} from '@angular/material/button';
import { MatToolbarModule} from '@angular/material/toolbar';
import { AgGridModule } from 'ag-grid-angular';
import { EntitygridComponent} from './../entitylookup/components/entitygrid/entitygrid.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { EntityState } from './store/entitylookup.state';
import { NgxsModule } from '@ngxs/store';
import { NgxsFormPluginModule } from '@ngxs/form-plugin';
import { EntityDateFormatteromponent } from './components/entityDateFormatter/entityDateFormatter.component';

@NgModule({
  declarations: [EntityDateFormatteromponent,EntitylookupComponent,EntitygridComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgSelectModule,
    NgOptionHighlightModule,
    MatCardModule,
    MatIconModule,
    MatButtonModule,
    MatToolbarModule,
    AgGridModule,
    DragDropModule,
    NgxsModule.forFeature([EntityState]),
    NgxsFormPluginModule.forRoot(),
    AgGridModule.withComponents(
      [EntityDateFormatteromponent]
  )
  ],
  exports:[EntitylookupComponent,EntitygridComponent]
})
export class EntitylookupModule { }
