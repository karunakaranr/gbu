import { Injectable, Input } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { GblistdataDTO } from './../../store/entitylookup.action';

import { Observable } from 'rxjs';
import { GBHttpService } from '@goodbooks/gbcommon';
import { EntityURLS } from './../../URLS/urls';
import { EntityState } from 'features/entitylookup/store/entitylookup.state';
import { PicklistCriteria } from './../../../../apps/Goodbooks/Goodbooks/src/assets/mockdata/_general/PicklistCriteria.json';
import { ILoginDTO } from 'features/common/shared/stores/auth.model';
import { AuthState } from 'features/common/shared/stores/auth.state';
import { LayoutState } from 'features/layout/store/layout.state';
import { IPicklistObj } from 'features/entitylookup/model/picklist.model';
@Injectable({
    providedIn: 'root',
})
export class Entitylookupdbservice {
    @Select(EntityState.Entitylookupdependpicklistdata) filterformdata$: Observable<string>;
    @Select(LayoutState.FormControlValue) formValue$: Observable<any>;
    @Select(LayoutState.BizTransactionClassId) biztransactionclassid$: Observable<any>;
    @Select(LayoutState.GCMTypeId) GCMTypeId$: Observable<any>;
    @Select(AuthState.AddDTO) loginDTOs$: Observable<ILoginDTO>;
    LoginDTO: ILoginDTO;
    FormData: any;
    BizTransactionClassId:number
    GCMTypeId:number
    constructor(public http: GBHttpService, public store: Store) {
        this.loginDTOs$.subscribe(dto => {
            this.LoginDTO = dto;
        })
        this.biztransactionclassid$.subscribe((biztransclassid:number)=>{
            console.log("biztransclassid:",biztransclassid)
            this.BizTransactionClassId = biztransclassid;
        })
        this.GCMTypeId$.subscribe((gcmtypeid:number)=>{
            console.log("gcmtypeid:",gcmtypeid)
            this.GCMTypeId = gcmtypeid;
        })
    }

    public picklist(id: string): Observable<number> {
        const url = EntityURLS.picklistid + id;
        return this.http.httpget(url);
    }


    public PicklistData(pickobj, GridIndex: number, data: string) {
        this.formValue$.subscribe(Res => {
            this.FormData = Res
        })
        let criterias
        if (pickobj['isfilter']) {
            if (pickobj['dependpicklistvalue'] != undefined && pickobj['dependpicklistvalue'].length > 0) {
                let filterform;
                this.filterformdata$.subscribe(data => {
                    filterform = data;
                })
                let criteria = {

                    "SectionCriteriaList": [
                        {
                            "SectionId": 0,
                            "AttributesCriteriaList": [
                            ],
                            "OperationType": 2
                        }
                    ],
                }
                let dependentpicklistvalues = pickobj['dependpicklistvalue']
                if (dependentpicklistvalues != undefined) {
                    for (let data of dependentpicklistvalues) {
                        let flag = 0;
                        for (let item of filterform.AttributesCriteriaList) {
                            if (data.DependPicklistId == item.FieldName && item.defaultvalue != "" && item.defaultvalue != undefined) {
                                flag = 1;
                                if (item.defaultvalue != 'NONE') {
                                    if (criteria.SectionCriteriaList.length < 2) {
                                        criteria.SectionCriteriaList.push({
                                            "SectionId": 1,
                                            "AttributesCriteriaList": [
                                            ],
                                            "OperationType": 0
                                        })
                                    }
                                    let FieldValue;
                                    FieldValue = parseInt(item.defaultvalue)
                                    if (Number.isNaN(parseInt(item.defaultvalue))) {
                                        FieldValue = item.defaultvalue
                                    }
                                    else {
                                        FieldValue = parseInt(item.defaultvalue)
                                    }
                                    criteria.SectionCriteriaList[1].AttributesCriteriaList.push(
                                        {
                                            FieldName: data.DependPicklistName,
                                            OperationType: data.Operationtpyevalue,
                                            FieldValue: FieldValue,
                                            InArray: null,
                                            JoinType: "2",
                                        }
                                    )
                                }
                            }
                        }
                        if (flag == 0 && data.values != 'NONE' && data.values != -1) {
                            if (criteria.SectionCriteriaList.length < 2) {
                                criteria.SectionCriteriaList.push({
                                    "SectionId": 1,
                                    "AttributesCriteriaList": [
                                    ],
                                    "OperationType": 0
                                })
                            }
                            criteria.SectionCriteriaList[1].AttributesCriteriaList.push(
                                {
                                    FieldName: data.DependPicklistName,
                                    OperationType: data.Operationtpyevalue,
                                    FieldValue: data.values,
                                    InArray: null,
                                    JoinType: "2",
                                }
                            )
                        }
                    }
                }
                if (pickobj['searcheither'] == true) {
                    var searcheithercoloumn = pickobj['searcheithercoloumn'].split(',');
                    for (var i = 0; i < searcheithercoloumn.length; i++) {
                        var ts = 1;
                        if (i == searcheithercoloumn.length) {
                            ts = 0;
                        }
                        criteria.SectionCriteriaList[0].AttributesCriteriaList.push(
                            {
                                "FieldName": searcheithercoloumn[i],
                                "OperationType": 2,
                                "FieldValue": data,
                                "InArray": null,
                                "JoinType": ts
                            }
                        )

                    }
                }
                criterias = criteria
            }
            else {
                let criteria = {

                    "SectionCriteriaList": [
                        {
                            "SectionId": 0,
                            "AttributesCriteriaList": [

                            ]
                        }
                    ],
                }
                if (pickobj['searcheither'] == true) {
                    var searcheithercoloumn = pickobj['searcheithercoloumn'].split(',');
                    for (var i = 0; i < searcheithercoloumn.length; i++) {
                        var ts = 1;
                        if (i == searcheithercoloumn.length) {
                            ts = 0;
                        }
                        criteria.SectionCriteriaList[0].AttributesCriteriaList.push(
                            {
                                "FieldName": searcheithercoloumn[i],
                                "OperationType": 2,
                                "FieldValue": data,
                                "InArray": null,
                                "JoinType": ts
                            }
                        )

                    }
                }
                criterias = criteria
            }
        } else {
            let criteria = {

                "SectionCriteriaList": [
                    {
                        "SectionId": 0,
                        "AttributesCriteriaList": [

                        ]
                    }
                ],
            }
            if (pickobj['searcheither'] == true) {
                console.log("FormData:",this.FormData)
                var searcheithercoloumn = pickobj['searcheithercoloumn'].split(',');
                let dependentpicklistvalues = pickobj['dependpicklistvalue']
                if (dependentpicklistvalues) {
                    if (typeof dependentpicklistvalues == 'string') {
                        dependentpicklistvalues = PicklistCriteria[dependentpicklistvalues]
                        for (let data of dependentpicklistvalues) {
                            if (data[0] == 'OrganizationUnit.Id' || data[0] == 'OUId') {
                                data[2] = this.LoginDTO.WorkOUId
                            } else if (data[0] == 'Period.Id') {
                                data[2] = this.LoginDTO.WorkPeriodId
                            } else if( data[0] == 'BIZTransactionTypeClassId' && data[2] == -1){
                                data[2] = this.BizTransactionClassId
                            }
                            if (data.length == 4) {
                                if (data[3] == 'GcmType.Id') {
                                    if(this.FormData['GcmTypeId'] == -1){
                                        criteria.SectionCriteriaList[0].AttributesCriteriaList.push(
                                            {
                                                FieldName: data[0],
                                                OperationType: data[1],
                                                FieldValue: this.GCMTypeId,
                                                InArray: null,
                                                JoinType: 2,
                                            }
                                        )
                                    } else {
                                        criteria.SectionCriteriaList[0].AttributesCriteriaList.push(
                                            {
                                                FieldName: data[0],
                                                OperationType: data[1],
                                                FieldValue: this.FormData['GcmTypeId'],
                                                InArray: null,
                                                JoinType: 2,
                                            }
                                        )
                                    }
                                }
                                else if (typeof data[3] == 'string') {
                                    if (this.FormData[data[3]] != -1) {
                                        criteria.SectionCriteriaList[0].AttributesCriteriaList.push(
                                            {
                                                FieldName: data[0],
                                                OperationType: data[1],
                                                FieldValue: this.FormData[data[3]],
                                                InArray: null,
                                                JoinType: 2,
                                            }
                                        )
                                    }
                                } else {
                                    criteria.SectionCriteriaList[0].AttributesCriteriaList.push(
                                        {
                                            FieldName: data[0],
                                            OperationType: data[1],
                                            FieldValue: data[2],
                                            InArray: null,
                                            JoinType: data[3],
                                        }
                                    )
                                }
                            } else if (data.length == 5) {
                                if(GridIndex == undefined){
                                    if (this.FormData[data[3]][data[4]] != -1) {
                                        criteria.SectionCriteriaList[0].AttributesCriteriaList.push(
                                            {
                                                FieldName: data[0],
                                                OperationType: data[1],
                                                FieldValue: this.FormData[data[3]][data[4]],
                                                InArray: null,
                                                JoinType: 2,
                                            }
                                        )
                                    }
                                }
                                else if (typeof data[3] == 'string' && typeof data[4] == 'string') {
                                    if (this.FormData[data[3]][GridIndex][data[4]] != -1) {
                                        criteria.SectionCriteriaList[0].AttributesCriteriaList.push(
                                            {
                                                FieldName: data[0],
                                                OperationType: data[1],
                                                FieldValue: this.FormData[data[3]][GridIndex][data[4]],
                                                InArray: null,
                                                JoinType: 2,
                                            }
                                        )
                                    }
                                } else {
                                    if (this.FormData[data[3]] != -1) {
                                        criteria.SectionCriteriaList[0].AttributesCriteriaList.push(
                                            {
                                                FieldName: data[0],
                                                OperationType: data[1],
                                                FieldValue: this.FormData[data[3]],
                                                InArray: null,
                                                JoinType: data[4],
                                            }
                                        )
                                    }
                                }
                            } else if (data.length == 6) {
                                if (this.FormData[data[3]][GridIndex][data[4]] != -1) {
                                    criteria.SectionCriteriaList[0].AttributesCriteriaList.push(
                                        {
                                            FieldName: data[0],
                                            OperationType: data[1],
                                            FieldValue: this.FormData[data[3]][GridIndex][data[4]],
                                            InArray: null,
                                            JoinType: data[5],
                                        }
                                    )
                                }
                            } else {
                                console.log("Condition",this.FormData.ShowArchived == undefined || (this.FormData.ShowArchived==1 && data[0] == 'Status'))
                                if(this.FormData.ShowArchived == undefined || (this.FormData.ShowArchived==1 && data[0] == 'Status')){
                                    criteria.SectionCriteriaList[0].AttributesCriteriaList.push(
                                        {
                                            FieldName: data[0],
                                            OperationType: data[1],
                                            FieldValue: data[2],
                                            InArray: null,
                                            JoinType: 2,
                                        }
                                    )
                                } else if(data[0] != 'Status') {
                                    criteria.SectionCriteriaList[0].AttributesCriteriaList.push(
                                        {
                                            FieldName: data[0],
                                            OperationType: data[1],
                                            FieldValue: data[2],
                                            InArray: null,
                                            JoinType: 2,
                                        }
                                    )
                                }
                            }
                        }
                    } else {
                        for (let data of dependentpicklistvalues) {
                            criteria.SectionCriteriaList[0].AttributesCriteriaList.push(
                                {
                                    FieldName: data.DependPicklistName,
                                    OperationType: data.Operationtpyevalue,
                                    FieldValue: data.values,
                                    InArray: null,
                                    JoinType: 2,
                                }
                            )
                        }
                    }
                }
                for (var i = 0; i < searcheithercoloumn.length; i++) {
                    var ts = 1;
                    if (i == searcheithercoloumn.length - 1) {
                        ts = 0;
                    }
                    criteria.SectionCriteriaList[0].AttributesCriteriaList.push(
                        {
                            "FieldName": searcheithercoloumn[i],
                            "OperationType": 2,
                            "FieldValue": data,
                            "InArray": null,
                            "JoinType": ts
                        }
                    )

                }
            }
            criterias = criteria
        }
        let url;


        let fullurl = pickobj.url;
        let str = fullurl.split('/gb4');
        let urlstr = str[str.length - 1];
        if (urlstr[urlstr.length - 1] == '/') {
            urlstr = urlstr.slice(0, -1)
        }
        const FirstNumberMaxresult = '/?FirstNumber=1&MaxResult=50'
        if (urlstr.includes('FirstNumber')) {
            let FN = urlstr.replace("{FirstNumber}", '1');
            url = FN.replace("{MaxResult}", '50');
        }
        else {
            // if(pickobj['isfilter'] && !pickobj['ismultiple']){
            //     url = urlstr + FirstNumberMaxresult;
            // } else {
                url = urlstr
            // }
        }
        let listdata = ""
        // this.http.httppost(url, criterias).subscribe(res => {
        //     listdata = res;
        //     this.store.dispatch(new GblistdataDTO(listdata));
        // });
        return this.http.httppost(url, criterias);
    }

    public defaultvalueData(pickobj, data) {
        let criteria = {

            "SectionCriteriaList": [
                {
                    "SectionId": 0,
                    "AttributesCriteriaList": [

                    ]
                }
            ] as any,
        }
        if (pickobj['dependpicklistvalue'].length > 0) {
            criteria.SectionCriteriaList.push({
                AttributesCriteriaList: [
                    {
                        FieldName: pickobj['dependpicklistvalue'][0].DependPicklistName,
                        OperationType: pickobj['dependpicklistvalue'][0].Operationtpyevalue,
                        FieldValue: '1',
                        InArray: null,
                        JoinType: 0,
                    },
                ],
                SectionId: 0,
                OperationType: 2,
            });
        }
        else {
        }
        if (pickobj['searcheither'] == true) {
            var searcheithercoloumn = pickobj['searcheithercoloumn'].split(',');
            for (var i = 0; i < searcheithercoloumn.length; i++) {
                var ts = 1;
                if (i == searcheithercoloumn.length) {
                    ts = 0;
                }
                criteria.SectionCriteriaList[0].AttributesCriteriaList.push(
                    {
                        "FieldName": searcheithercoloumn[i],
                        "OperationType": 2,
                        "FieldValue": data,
                        "InArray": null,
                        "JoinType": ts
                    }
                )

            }
        }
        let url;
        let fullurl = pickobj.url;
        let str = fullurl.split('/gb4');
        let urlstr = str[str.length - 1];
        if (urlstr[urlstr.length - 1] == '/') {
            urlstr = urlstr.slice(0, -1)
        }
        const FirstNumberMaxresult = '/?FirstNumber=1&MaxResult=50'
        if (urlstr.includes('FirstNumber')) {
            let FN = urlstr.replace("{FirstNumber}", '1');
            url = FN.replace("{MaxResult}", '50');
        }
        else {
            url = urlstr;
        }
        let listdata = ""
        // this.http.httppost(url, criteria).subscribe(res => {
        //     listdata = res;
        //     this.store.dispatch(new GblistdataDTO(listdata));
        // });
        return this.http.httppost(url, criteria);
    }
}
