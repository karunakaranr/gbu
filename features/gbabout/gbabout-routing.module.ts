import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GbaboutComponent } from '../gbabout/components/gbabout/gbabout.component';

const routes: Routes = [
 
  {
    path: 'gbabout',
    component: GbaboutComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GbaboutRoutingModule { }
