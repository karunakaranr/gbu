import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
const urls = require('./../../URLS/urls.json');

@Injectable({
  providedIn: 'root',
})
export class Gbaboutdbservice {
 endPoint: string = urls.Pack || urls.UOM;
  constructor(private http: HttpClient) { }

  public AboutDbServices() {
    return this.http.get(this.endPoint);
  }

}



