import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { GbaboutComponent } from './gbabout.component';

describe('GbaboutComponent', () => {
  let component: GbaboutComponent;
  let fixture: ComponentFixture<GbaboutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GbaboutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GbaboutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
