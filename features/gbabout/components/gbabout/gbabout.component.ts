import { Component, OnInit } from '@angular/core';
import { GbaboutService } from 'features/gbabout/services/gbabout/gbabout.service';

@Component({
  selector: 'app-gbabout',
  templateUrl: './gbabout.component.html',
  styleUrls: ['./gbabout.component.scss'],
})

export class GbaboutComponent implements OnInit {
  title = 'About for Pack'
  public aboutData;
  id: string;
  

  constructor(public service: GbaboutService) {
   }

  ngOnInit(): void {
     this.gbabout();
}

  public gbabout() {
    this.service.getaboutservice().subscribe((res) => {
      this.aboutData = res;
     alert(this.aboutData)
    });
  }
  
   
  

}
