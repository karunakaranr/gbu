import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-bsform',
  templateUrl: './bsform.component.html',
  styleUrls: ['./bsform.component.scss']
})
export class BsformComponent implements OnInit {

  submitted = false;
  allstatus = ['Single', 'Married', 'Divorced', 'Widowed'];
  form: FormGroup;

  constructor() {
    this.form = new FormGroup(
      {
        title: new FormControl('', Validators.required),
        firstName: new FormControl('', Validators.required),
        lastName: new FormControl('', Validators.required),
        dob: new FormControl('', Validators.required),
        age: new FormControl('', Validators.required),
        ms: new FormControl('', Validators.required),
        snm: new FormControl(''),
        dom: new FormControl(''),
        dod: new FormControl(''),
        dodt: new FormControl(''),
        ccnum: new FormControl(''),

        email: new FormControl('', [Validators.required, Validators.email]),
        password: new FormControl('', [
          Validators.required,
          Validators.minLength(6),
        ]),
        confirmPassword: new FormControl('', Validators.required),
        acceptTerms: new FormControl(false, Validators.requiredTrue),
      },
    );
   }

  ngOnInit(): void {
  }
  get f() {
    return this.form.controls;
  }
  onSubmit() {}
  onReset() {
    this.submitted = false;
    this.form.reset();
  }
}
