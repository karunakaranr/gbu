import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms'
import { LoginModule } from './shared/screens/Login/login.module';
import { GbcommonModule } from '@goodbooks/gbcommon';
import { TranslateModule } from './shared/Translate/Components/translate.module';
import { NgxsModule } from '@ngxs/store';
import { environment } from '../environments/environment';
import { AuthState } from './shared/stores/auth.state';
import { DashboardModule } from './shared/screens/dashboard/dashboard.module';
import { PortletModule } from './shared/screens/portlet/portlet.module';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { GbgridModule } from 'features/gbgrid/components/gbgrid/gbgrid.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule,
    GbcommonModule,
    TranslateModule,
    DashboardModule,
    PortletModule,
    NgxsModule.forFeature([AuthState]),
    NgxChartsModule
  ],
  exports: [LoginModule,GbgridModule,DashboardModule],
})
export class FeatureCommonmodule { }
