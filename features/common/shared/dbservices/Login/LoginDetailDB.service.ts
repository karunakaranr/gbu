import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { GBHttpService } from '@goodbooks/gbcommon';
@Injectable({
    providedIn: 'root',
})
export class LoginDetailDbService {
    constructor(public http: GBHttpService) { }

    public logindetailservice(UserId: string): Observable<any> {

        const url = '/ads/UserLogin.svc/LoginDetail/?UserId=' + UserId;
        return this.http.httpget(url);
    }
}