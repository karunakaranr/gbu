import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IDashboard } from './../../models/dashboard/dashboard.model';
import { GBBaseDBService } from '@goodbooks/gbdata';
import { GBHttpService } from '@goodbooks/gbcommon';


const userVsportlet ='apiu/fws/UserVsPortlet.svc/?UserId=&PageId=';
const saveuserpage = 'apiu/prox/fws/UserVsPortlet.svc/';
import * as data from './../../../../../apps/Goodbooks/Goodbooks/src/assets/mockdata/_general/page.json';
import { of } from 'rxjs';



@Injectable({
    providedIn: 'root'
})
export class DashboarddbService {
    public loginDTO;
    public userid;
    public pageid = -1499999983;

    sampledata = 'assets/mockdata/_general/page.json';


    constructor(private httpclient: HttpClient ,public http: GBHttpService) {
    }

    public Viewuserpage() {
        const url = 'fws/Portlet.svc/PortletListWithCriteria/?UserId=-1499998941&PageId=-1900000000';
        console.log("url",url);
        return this.http.httpget(url);
        // return of((data as any).default);
    }

    public saveuserpage(savedata) {
        return this.httpclient.get(savedata);
    }
    public removepages(event, item) {
        return this.httpclient.delete(this.sampledata + item);
    }

}

