import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { TranslocoService} from '@ngneat/transloco';
@Component({
  selector: 'app-translate',
  templateUrl: './translate.component.html',
  styleUrls: ['./translate.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TranslateComponent implements OnInit  {
  def="English";
  activelang: string;
  avaliblelang: string[] | { id: string, label: string }[];

  constructor(public transloco: TranslocoService){}
  ngOnInit(){
    this.activelang = this.transloco.getActiveLang();
    this.avaliblelang = this.transloco.getAvailableLangs();
  }
  
  get activeLang() {
    return this.transloco.getActiveLang();
  }

  changelang(lang: string) {
    this.transloco.setActiveLang(lang);
  }
}

