export class IGridsterProperty {
  margins: string;
  minCols: Number;
  maxCols: Number;
  minRows: Number;
  draggable: {
    enabled: string;
  };
  pushItems: string;
  resizable: {
    enabled: string;
  };
}
