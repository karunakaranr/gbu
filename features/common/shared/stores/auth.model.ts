export interface ILoginDTO {
    'UserCode': string;
    'DatabaseName': string;
    'SessionId': number;
    'ValidToTime': string;
    'ValidityOfSession': string;
    'UserId': string;
    'UserPrimaryMailId': string;
    'ServerId': string;
    'RoleId': string;
    'AppId': string;
    'DeviceId': string;
    'WorkOUId': string;
    'WorkPeriodId': string;
    'WorkPartyBranchId': string;
    'WorkStoreId': string;
    'Realm': string;
    'ImgPath': '../images/user/';
    'ModeOfOperation': string;
    'MachineIP': '192.168.0.2'; // authDTO.MachineIP;
    'UserName': string;
    'TimeZone': number;
    'DatabaseOffset': string;
    'DatabaseType': string;
    'UserCriteriaConfigId': string;
    'ClientId': string;
    'SourceType': string;
    'UserCriteriaDTO': string;
    'StartTime': string;
    'DateFormat': string;
    'CurrencyFormat': string;
    'TimeFormat': string;
    'QuantityFormat': string;
    'FromMailMenuId': -1;
    'FromMailvalueId': -1;
    'FEUri': string;
    'IsAdmin': string;
    'ServerConfigId': string;
    'ModeOfWorking': 1; 
    'BaseUri': string;
    'Geo': string;
    'LoginServerDate': string;
    'LoginEventLogId': string;
    'ServerPopup': string;
    'IsValidationRequired': 1;
    'OuCode': string;
    'OuName': string;
  }
  
  export class IThemeDto{
    'ThemeOption': string;
  }
  