import { Component, OnInit } from '@angular/core';
import { IPortlet } from './../../models/portlet/portlet.modal';
import {PortletService} from './../../services/portlet/portlet.service';
import {GridsterService} from './../../services/gridster/gridster.service';
@Component({
  selector: 'app-portlet',
  templateUrl: './portlet.component.html',
  styleUrls: ['./portlet.component.scss']
})
export class PortletComponent implements OnInit {
portlet=[];
portletlist : Array<IPortlet>=[];
  constructor(public portletservice : PortletService, public gridster:GridsterService) { }

  ngOnInit() {
    this.viewportlet();
  }
public viewportlet()
{
  this.portletservice.getportlet().subscribe(Data=>{
    this.portlet=Data;
  });
}
}
