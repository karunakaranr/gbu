import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import {PortletComponent} from './portlet.component';
import {MatDialogModule} from '@angular/material/dialog';
import {MatButtonModule} from '@angular/material/button';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MatDialogModule,
    MatButtonModule    
  ],
  declarations: [PortletComponent],
  exports: [PortletComponent],
  providers: [],
  entryComponents:[]
})
export class PortletModule { }
