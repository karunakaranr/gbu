import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { GbchartComponent } from './gbchart.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';

@NgModule({
  declarations: [
    GbchartComponent
  ],
  imports: [
    CommonModule,
    CommonModule,
    NgxChartsModule
  ],
  exports: [
    GbchartComponent
  ],
  
})
export class GbchartModule { }
