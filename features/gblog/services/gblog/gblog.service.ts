import { Injectable } from '@angular/core';

import { Gblogdbservice } from './../../dbservices/gblog/gblogdb.service';
@Injectable({
  providedIn: 'root',
})
export class GbLogService {

  constructor(public Gblogdbservice: Gblogdbservice) { }

  public getlogservice() {
    return this.Gblogdbservice.LogDbServices();
  }

}
