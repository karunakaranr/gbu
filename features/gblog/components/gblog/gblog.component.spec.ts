import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { GblogComponent } from './gblog.component';

describe('GblogComponent', () => {
  let component: GblogComponent;
  let fixture: ComponentFixture<GblogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GblogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GblogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
