# Gb Log

The purpose if this componet is to display the datas.

| Name  | Details |
| ------------- | ------------- |
| Component Name  | gblog  |
| Has Services?  | yes  |
| Service Name  | gblogservice |

- Component Name
  gblog

- Has Services: yes (gblogservice)
- Has DB serives: yes (gblogdbservice)

## Model

    export interface GbLog {
        EventLogId: number;
        EventLogEventText: string;
        EventLogLink: string;
        SourceId: number;
        SourceCode?: any;
        SourceName?: any;
        UserId: number;
        UserCode: string;
        UserName: string;
        EventLogTags: string;
        EventLogEventValue: string;
        EventLogData: string;
        EventLogGeo?: any;
        EventTypeId: number;
        EventTypeCode: string;
        EventTypeName: string;
        EventClassId: number;
        EventClassCode: string;
        EventClassName: string;
        EventLogLogBookId: number;
        EventLogDataId: number;
        EventLogTimeStamp: Date;
        EventChannelId: number;
        EventChannelCode: string;
        EventChannelName: string;
        EventLogOUId: number;
        IsEventSave: boolean;
        EventLogMachineIp?: any;
        EventLogModeOfWorking: number;
        EventLogLoginEventLogId: number;
    }


## Properties -

## Methods -

## Actions -

## Test Cases -
