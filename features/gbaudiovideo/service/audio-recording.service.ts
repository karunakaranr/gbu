import { Injectable } from '@angular/core';
import RecordRTC from 'recordrtc';
import * as moment from 'moment';
import { Observable, Subject } from 'rxjs';

interface RecordedAudioOutput {
  blob: Blob;
  title: string;
}

@Injectable()
export class AudioRecordingService {

  private stream: MediaStream;
  private recorder: RecordRTC | any;
  private interval: number | undefined; // Changed type to number

  private startTime: moment.MomentInput;
  private _recorded = new Subject<RecordedAudioOutput>();
  private _recordingTime = new Subject<string>();
  private _recordingFailed = new Subject<void>();

  getRecordedBlob(): Observable<RecordedAudioOutput> {
    return this._recorded.asObservable();
  }

  getRecordedTime(): Observable<string> {
    return this._recordingTime.asObservable();
  }

  recordingFailed(): Observable<void> {
    return this._recordingFailed.asObservable();
  }

  startRecording(): Promise<void> {
    return new Promise((resolve, reject) => {
      navigator.mediaDevices.getUserMedia({ audio: true }).then((stream: MediaStream) => {
        this.stream = stream;
        this.record();
        resolve();
      }).catch(() => {
        this._recordingFailed.next();
        reject();
      });
    });
  }

  abortRecording() {
    this.stopMedia();
  }

  private record() {
    this.recorder = new RecordRTC(this.stream, {
      type: 'audio',
      mimeType: 'audio/webm',
      bitsPerSecond: 128000
    });
    this.recorder.startRecording();
    this.startTime = moment();
    this.interval = setInterval(
      () => {
        const currentTime = moment();
        const diffTime = moment.duration(currentTime.diff(this.startTime));
        const time = this.toString(diffTime.minutes()) + ':' + this.toString(diffTime.seconds());
        this._recordingTime.next(time);
      },
      500
    ) as any; // Cast setInterval return value to any
  }

  private toString(value: number) {
    let val = JSON.stringify(value);
    if (!value) {
      val = '00';
    }
    if (value < 10) {
      val = '0' + value;
    }
    return val;
  }

  stopRecording() {
    if (this.recorder) {
      this.recorder.stopRecording(() => {
        const recordedBlob = this.recorder.getBlob();
        const recordedName = 'audio_' + new Date().getTime() + '.webm';
        this._recorded.next({ blob: recordedBlob, title: recordedName });
        this.stopMedia();
      });
    }
  }

  private stopMedia() {
    if (this.recorder) {
      this.recorder = null;
      clearInterval(this.interval!);
      if (this.stream) {
        this.stream.getAudioTracks().forEach(track => track.stop());
      }
    }
  }
}
