import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'goodbooks-admin',
  template: `
  `,
  styles: ['ul li a {margin-bottom: 0px; color: black}'],
  // encapsulation: ViewEncapsulation.None
})
export class AdminComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
