import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { DBJoinService } from '../../services/DBJoin.service';
import { IDBJoin } from '../../models/IDBJoin';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as DBJoinJson from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/DBJoin.json';
import { ADMINURLS } from '../../URLS/urls';


@Component({
  selector: 'app-dbjoin',
  templateUrl: './dbjoin.component.html',
  styleUrls: ['./dbjoin.component.scss'], 
  providers: [
    { provide: 'IdField', useValue: 'DBJoinId'},
    { provide: 'url', useValue: ADMINURLS.DBJoin},
    { provide: 'DataService', useClass: DBJoinService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class DBJoinComponent extends GBBaseDataPageComponentWN<IDBJoin> {
  title: string = 'DBJoin'
  DBJoinJson = DBJoinJson;
 
  public DBJoinIndirect(value): void {
        if(value == '0') {
          console.log("this.form.get('DBJoinRelationType').value:", this.form.get('DBJoinRelationType').value)
   
        this.form.get('IndirectFromDbobjectId').patchValue('-1');
        this.form.get('IndirectFromDbobjectName').patchValue('NONE'); 
        this.form.get('IndirectToDbobjectId').patchValue('-1');
        this.form.get('IndirectToDbobjectName').patchValue('NONE'); 
      }
      
    }

  form: GBDataFormGroupWN<IDBJoin> = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'DBJoin', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.DBJoinRetrivalValue(arrayOfValues.DBJoinId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }
  public DBJoinRetrivalValue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(DBJoin => {
        this.form.patchValue(DBJoin);
      })
    }
  }
  public DBJoinPatchVaue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}



export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IDBJoin> {
  const dbds: GBBaseDBDataService<IDBJoin> = new GBBaseDBDataService<IDBJoin>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IDBJoin>, dbDataService: GBBaseDBDataService<IDBJoin>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IDBJoin> {
  return new GBDataPageService<IDBJoin>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
