import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { TestRunService } from '../../services/TestRun/TestRun.service';
import { ITestRun } from '../../models/ITestRun';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
// import * as JsonName from ‘JsonPath’ './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/PackSet.json';
import * as TestRunJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/TestRun.json'
import { ADMINURLS } from '../../URLS/urls';


@Component({
  selector: 'app-TestRun',
  templateUrl: './TestRun.component.html',
  styleUrls: ['./TestRun.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'TestsetId'},
    { provide: 'url', useValue: ADMINURLS.TestSet },
    { provide: 'DataService', useClass: TestRunService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class TestRunComponent extends GBBaseDataPageComponentWN<ITestRun > {
  title: string = 'TestRun'
  TestRunJSON = TestRunJSON;


  form: GBDataFormGroupWN<ITestRun > = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'TestRun', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.TestRunFillFunction(arrayOfValues.TestsetId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }


  public TestRunFillFunction(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(PackSet => {
        this.form.patchValue(PackSet);
      })
    }
  }


  public TestRunPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<ITestRun > {
  const dbds: GBBaseDBDataService<ITestRun > = new GBBaseDBDataService<ITestRun >(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<ITestRun >, dbDataService: GBBaseDBDataService<ITestRun >, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<ITestRun > {
  return new GBDataPageService<ITestRun >(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}


