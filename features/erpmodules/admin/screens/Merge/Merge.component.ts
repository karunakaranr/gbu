import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';

import { MergeService } from '../../services/Merge/Merge.service';
import { IMerge } from '../../models/IMerge';
import { ButtonVisibility } from 'features/layout/store/layout.actions';

import { ADMINURLS } from '../../URLS/urls';
import * as Mergejson from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/Merge.json'
@Component({
  selector: "app-Merge",
  templateUrl: "./Merge.component.html",
  styleUrls: ["./Merge.component.scss"],
  providers: [
    { provide: 'IdField', useValue: 'MergeId' },
    { provide: 'url', useValue: ADMINURLS.Merge },
    { provide: 'DataService', useClass: MergeService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class MergeComponent extends GBBaseDataPageComponentWN<IMerge> {
  title: string = "Merge"
  Mergejson = Mergejson;



  form: GBDataFormGroupWN<IMerge> = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'Merge', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.MergeFillFunction(arrayOfValues.MergeId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }


  public MergeFillFunction(SelectedPicklistData: string): void {

   console.log("filling",SelectedPicklistData)
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(Merge => {
        this.form.patchValue(Merge);
      })
    }
  }


  public MergePatchValue(SelectedPicklistDatas: any): void {
    
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
    console.log("item==>",SelectedPicklistDatas)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IMerge> {
  const dbds: GBBaseDBDataService<IMerge> = new GBBaseDBDataService<IMerge>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IMerge>, dbDataService: GBBaseDBDataService<IMerge>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IMerge> {
  return new GBDataPageService<IMerge>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
