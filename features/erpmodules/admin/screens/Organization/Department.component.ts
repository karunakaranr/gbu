import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { IDepartment } from '../../models/IDepartment';
import { DepartmentService } from '../../services/Organization/Department.service';
import { ADMINURLS } from '../../URLS/urls';
import * as DepartmentJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/Department.json'

@Component({
    selector: 'app-Organization',
    templateUrl:'./Department.component.html',
    styleUrls: ['./Department.component.scss'],
    providers: [
        { provide: 'IdField', useValue: 'DepartmentId' },
        { provide: 'url', useValue: ADMINURLS.Department },
        { provide: 'DataService', useClass: DepartmentService },
        { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
        { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
    ]
})
export class DepartmentComponent extends GBBaseDataPageComponentWN<IDepartment> {
    title: string = "Department"
    DepartmentJSON  = DepartmentJSON
    form: GBDataFormGroupWN<IDepartment> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "Department", {}, this.gbps.dataService);
    thisConstructor() {
        let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
        if (arrayOfValues != null) {
            this.DepartmentPicklistValue(arrayOfValues.DepartmentId)
            this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
        } else {
            this.store.dispatch(new ButtonVisibility("Forms"))
        }
    }

    public DepartmentPicklistValue(SelectedPicklistData: string): void {
        // console.log("id",SelectedPicklistData)
        if (SelectedPicklistData) {
            // console.log("id",SelectedPicklistData)
            this.gbps.dataService.getData(SelectedPicklistData).subscribe(Department => {
                console.log("id",SelectedPicklistData)

                    this.form.patchValue(Department);        
            })
        }
    }

    public DepartmentPicklistFillValue(SelectedPicklistDatas: any): void {
        // console.log("id",SelectedPicklistDatas)
        this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
    }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IDepartment> {
    const dbds: GBBaseDBDataService<IDepartment> = new GBBaseDBDataService<IDepartment>(http);
    dbds.endPoint = url;
    return dbds;
}


export function getThisPageService(store: Store, dataService: GBBaseDataService<IDepartment>, dbDataService: GBBaseDBDataService<IDepartment>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IDepartment> {
    return new GBDataPageService<IDepartment>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
