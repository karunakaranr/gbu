import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { ICurrency } from 'features/erpmodules/admin/models/ICurrency';
import { CurrencyService } from 'features/erpmodules/admin/services/Currency/currency.service';
import { ADMINURLS } from 'features/erpmodules/admin/URLS/urls';
import * as CurrencyJSON from './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/Currency.json'

@Component({
    selector: 'app-Currency',
    templateUrl:'./currency.component.html',
    styleUrls: ['./currency.component.scss'],
    providers: [
        { provide: 'IdField', useValue: 'CurrencyId' },
        { provide: 'url', useValue: ADMINURLS.Currency },
        { provide: 'DataService', useClass: CurrencyService },
        { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
        { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
    ]
})
export class CurrencyComponent extends GBBaseDataPageComponentWN<ICurrency> {
    title: string = "Currency"
    CurrencyJSON = CurrencyJSON

    form: GBDataFormGroupWN<ICurrency> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "Currency", {}, this.gbps.dataService);
    thisConstructor() {
        let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
        if (arrayOfValues != null) {
            this.CurrencyPicklistFillValue(arrayOfValues.CurrencyId)
            this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
        } else {
            this.store.dispatch(new ButtonVisibility("Forms"))
        }
    }

    public CurrencyPicklistFillValue(SelectedPicklistData: string): void {
        // console.log("id",SelectedPicklistData)
        if (SelectedPicklistData) {
            // console.log("id",SelectedPicklistData)
            this.gbps.dataService.getData(SelectedPicklistData).subscribe(Currency => {
                // console.log("Currency",Currency)              
                    this.form.patchValue(Currency);             
            })
        }
    }

    public CurrencyPicklistPatchValue(SelectedPicklistDatas: any): void {
        // console.log("id",SelectedPicklistDatas)
        this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
    }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<ICurrency> {
    const dbds: GBBaseDBDataService<ICurrency> = new GBBaseDBDataService<ICurrency>(http);
    dbds.endPoint = url;
    return dbds;
}


export function getThisPageService(store: Store, dataService: GBBaseDataService<ICurrency>, dbDataService: GBBaseDBDataService<ICurrency>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<ICurrency> {
    return new GBDataPageService<ICurrency>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
