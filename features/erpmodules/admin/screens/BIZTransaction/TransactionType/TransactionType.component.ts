import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { TransactionTypeService } from 'features/erpmodules/admin/services/BIZTransactionType/TransactionType.service';
import { ITransactionType } from 'features/erpmodules/admin/models/ITransactionType';
import { ADMINURLS } from 'features/erpmodules/admin/URLS/urls';
import * as TransactionTypeJSON from './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/TransactionType.json';


@Component({
    selector: 'app-Transactiontype',
    templateUrl:'./TransactionType.component.html',
    styleUrls: ['./TransactionType.component.scss'],
    providers: [
        { provide: 'IdField', useValue: 'TransactionTypeId' },
        { provide: 'url', useValue: ADMINURLS.TransactionType },
        { provide: 'DataService', useClass: TransactionTypeService },
        { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
        { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
    ]
})
export class TransactionTypeComponent extends GBBaseDataPageComponentWN<ITransactionType> {
    title: string = "TransactionType"
    TransactionTypeJSON = TransactionTypeJSON 

    form: GBDataFormGroupWN<ITransactionType> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "TransactionType", {}, this.gbps.dataService);
    thisConstructor() {
        let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
        if (arrayOfValues != null) {
            this.TransactionTypePicklistFillValue(arrayOfValues.TransactionTypeId)
            this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
        } else {
            this.store.dispatch(new ButtonVisibility("Forms"))
        }
    }


    public setPatchValueToZero(): void {
        if (this.form.get('TransactionTypeIsFormReceived').value == "0" || this.form.get('TransactionTypeIsFormReceived').value.length > 0) {
            // console.log('LotTypeType value set to 0');
            this.form.get('TransactionTypeFormReceived').patchValue('NONE');
        }
        if(this.form.get('TransactionTypeIsFormReceived').value == "1"){
            this.form.get('TransactionTypeFormReceived').patchValue('NONE')
        }
        else{          
            this.form.get('TransactionTypeFormReceived').patchValue('NONE'); 
        }
    }
    public setPatchValue(): void {
        if (this.form.get('TransactionTypeIsFormIssued').value == "0" ) {
            // console.log('LotTypeType value set to 0');
            this.form.get('TransactionTypeFormIssued').patchValue('NONE');
        }
        if(this.form.get('TransactionTypeIsFormIssued').value == "1"){
            this.form.get('TransactionTypeFormIssued').patchValue('NONE')
        }
        else{          
            this.form.get('TransactionTypeFormIssued').patchValue('NONE'); 
        }
    }

    public TransactionTypePicklistFillValue(SelectedPicklistData: string): void {
        if (SelectedPicklistData) {
            this.gbps.dataService.getData(SelectedPicklistData).subscribe(TransactionType => {             
                    this.form.patchValue(TransactionType);               
            })
        }
    }

    public TransactionTypePicklistPatchValue(SelectedPicklistDatas: any): void {
        this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
    }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<ITransactionType> {
    const dbds: GBBaseDBDataService<ITransactionType> = new GBBaseDBDataService<ITransactionType>(http);
    dbds.endPoint = url;
    return dbds;
}


export function getThisPageService(store: Store, dataService: GBBaseDataService<ITransactionType>, dbDataService: GBBaseDBDataService<ITransactionType>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<ITransactionType> {
    return new GBDataPageService<ITransactionType>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
   


