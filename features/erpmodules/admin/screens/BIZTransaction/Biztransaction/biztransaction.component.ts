import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { IBaseField, IPickListDetail } from 'libs/uicore/src/lib/interfaces/ibasefield';
import { IBizTransaction } from 'features/layout/models/IBizTransaction';
import { BIZTransactionService } from 'features/erpmodules/admin/services/Biztransaction/biztransaction.service';
import { ADMINURLS } from 'features/erpmodules/admin/URLS/urls';
import * as BizTransactionJSON from './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/BizTransaction.json'

@Component({
    selector: 'app-Biztransaction',
    templateUrl:'./biztransaction.component.html',
    styleUrls: ['./biztransaction.component.scss'],
    providers: [
        { provide: 'IdField', useValue: 'BIZTransactionId' },
        { provide: 'url', useValue: ADMINURLS.Biztransaction },
        { provide: 'DataService', useClass: BIZTransactionService },
        { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
        { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
    ]
})
export class BizTransactionComponent extends GBBaseDataPageComponentWN<IBizTransaction> {
    title: string = "BizTransaction"
    BizTransactionJSON = BizTransactionJSON
    form: GBDataFormGroupWN<IBizTransaction> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "BizTransaction", {}, this.gbps.dataService);
    thisConstructor() {
        let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
        if (arrayOfValues != null) {
            this.BizTransactionPicklistFillValue(arrayOfValues.BIZTransactionId)
            this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
        } else {
            this.store.dispatch(new ButtonVisibility("Forms"))
        }
    }

      public BizTransactionPicklistFillValue(SelectedPicklistData: string): void {
        // console.log("id",SelectedPicklistData)
        if (SelectedPicklistData) {
            // console.log("id",SelectedPicklistData)
            this.gbps.dataService.getData(SelectedPicklistData).subscribe(BIZTransaction => {
                // console.log("id",SelectedPicklistData)
                    this.form.patchValue(BIZTransaction);                           
            })
        }
    }

    public BizTransactionPicklistPatchValue(SelectedPicklistDatas: any): void {
        // console.log("id",SelectedPicklistDatas)
        this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
    }
}

export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IBizTransaction> {
    const dbds: GBBaseDBDataService<IBizTransaction> = new GBBaseDBDataService<IBizTransaction>(http);
    dbds.endPoint = url;
    return dbds;
}


export function getThisPageService(store: Store, dataService: GBBaseDataService<IBizTransaction>, dbDataService: GBBaseDBDataService<IBizTransaction>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IBizTransaction> {
    return new GBDataPageService<IBizTransaction>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
