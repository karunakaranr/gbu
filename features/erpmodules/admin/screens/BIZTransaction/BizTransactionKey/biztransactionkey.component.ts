import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { IBizTransactionKey } from 'features/erpmodules/admin/models/IBizTransactionKey';
import { ADMINURLS } from 'features/erpmodules/admin/URLS/urls';
import { BIZTransactionKeyService } from 'features/erpmodules/admin/services/Biztransaction/BizTransactionKey.service';
import * as BizTransactionKeyJSON from './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/BizTransactionKey.json'
@Component({
    selector: 'app-BizTransactionKey',
    templateUrl:'./biztransactionkey.component.html',
    styleUrls: ['./biztransactionkey.component.scss'],
    providers: [
        { provide: 'IdField', useValue: 'BIZTransactionTypeId' },
        { provide: 'url', useValue: ADMINURLS.BizTransactionKey },
        { provide: 'saveurl', useValue: ADMINURLS.BizTransactionKeysave },
        { provide: 'PostingParameterFieldName', useValue: 'BizTransactionKeysId,LastNo' },
        { provide: 'PostingParameterFieldValue', useValue: 'BizTransactionKeysId,BIZTransactionKeysLastNumber' },
        { provide: 'DataService', useClass: BIZTransactionKeyService },
        { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url','saveurl','PostingParameterFieldName','PostingParameterFieldValue'] },
        { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
    ]
})
export class BIZTransactionKeyComponent extends GBBaseDataPageComponentWN<IBizTransactionKey> {
    title: string = "BizTransactionKey"
    BizTransactionKeyJSON = BizTransactionKeyJSON

    form: GBDataFormGroupWN<IBizTransactionKey> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "BizTransactionKey", {}, this.gbps.dataService);
    thisConstructor() {
        let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
        if (arrayOfValues != null) {
            this.BizTransactionKeyPicklistFillValue(arrayOfValues.BIZTransactionTypeId)
            this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
        } else {
            this.store.dispatch(new ButtonVisibility("Forms"))
        }
    }
  
    public BizTransactionKeyPicklistFillValue(SelectedPicklistData: string): void {
        if (SelectedPicklistData) {
            this.gbps.dataService.postData(SelectedPicklistData).subscribe(BizTransactionKey => {
                this.form.patchValue(BizTransactionKey[0]);
                this.form.get("BIZTransactionKeysLastNumber").patchValue(BizTransactionKey[0].BIZTransactionKeysLastNo);
                this.form.get("BizTransactionKeysId").patchValue(BizTransactionKey[0].BIZTransactionKeysId);
            })
        }
    }

    public BizTransactionKeyPicklistPatchValue(SelectedPicklistDatas: any): void {
        console.log("SelectedPicklistDatasRetrival",SelectedPicklistDatas)
        this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
    }
}


export function getThisDBDataService(http: GBHttpService, url: string, saveurl:string, PostingParameterFieldName: string, PostingParameterFieldValue:string): GBBaseDBDataService<IBizTransactionKey> {
    const dbds: GBBaseDBDataService<IBizTransactionKey> = new GBBaseDBDataService<IBizTransactionKey>(http);
    dbds.endPoint = url;
    dbds.saveurl = saveurl;
    dbds.PostingParameterFieldName = PostingParameterFieldName
    dbds.PostingParameterFieldValue = PostingParameterFieldValue
    return dbds;
}


export function getThisPageService(store: Store, dataService: GBBaseDataService<IBizTransactionKey>, dbDataService: GBBaseDBDataService<IBizTransactionKey>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IBizTransactionKey> {
    return new GBDataPageService<IBizTransactionKey>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
