import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { ADMINURLS } from 'features/erpmodules/admin/URLS/urls';
import { IBizTransactionType } from 'features/erpmodules/admin/models/IBizTransactionType';
import { BizTransactionTypeService } from 'features/erpmodules/admin/services/Biztransaction/biztransactiontype.service';
import * as BizTransactionTypeJSON from './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/BizTransactionType.json'
@Component({
    selector: 'app-BizTransactionType',
    templateUrl:'./biztransactiontype.component.html',
    styleUrls: ['./biztransactiontype.component.scss'],
    providers: [
        { provide: 'IdField', useValue: 'BIZTransactionTypeId' }, 
        { provide: 'url', useValue: ADMINURLS.BizTransactionType },
        { provide: 'DataService', useClass: BizTransactionTypeService },
        { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
        { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
    ]
})
export class BizTransactionTypeComponent extends GBBaseDataPageComponentWN<IBizTransactionType> {
    title: string = "BizTransactionType"
    BizTransactionTypeJSON = BizTransactionTypeJSON
    showTab: boolean = false;

    form: GBDataFormGroupWN<IBizTransactionType> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "BizTransactionType", {}, this.gbps.dataService);
    thisConstructor() {
        let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
        if (arrayOfValues != null) {
            this.BizTransactionTypePicklistFillValue(arrayOfValues.BIZTransactionTypeId)
            this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
        } else {
            this.store.dispatch(new ButtonVisibility("Forms"))
        }
    }

    public BizTransactionTypePicklistFillValue(SelectedPicklistData: string): void {
        console.log("SelectedPicklistData",SelectedPicklistData)
        if (SelectedPicklistData) {
            this.gbps.dataService.getData(SelectedPicklistData).subscribe(BizTransactionType => {
                console.log("BizTransactionType",BizTransactionType)
                    this.form.patchValue(BizTransactionType);
                 
            })
        }
    }
    // public YesNo(){
    //     if({

    //     }
    // }

    // public YesNo(value): void {
        
        
    //     if(value == '0') {
        
    //     this.form.get('BIZTransactionTypeIsPendingPost').patchValue('1');

    //   }
    //   else  {
      
    //     this.form.get('BIZTransactionTypeIsPendingPost').patchValue('0');
      
    //   }

    //   }
    public RVLEmployeeMasterRetrievalFillFunction(SelectedPicklistDatas: any){
        console.log("SelectedPicklistDatas",SelectedPicklistDatas)
        let linkids = SelectedPicklistDatas.id.split(',')
        console.log("linkidsfsds",linkids)

        let linkvalues = SelectedPicklistDatas.value.split(',')
        console.log("linkvalues",SelectedPicklistDatas)

        for(let i = 0;i<linkids.length;i++){
          this.form.get(linkids[i])?.setValue(SelectedPicklistDatas.Selected[linkvalues[i]])
        }
      }
    
    toggleTabVisibility(event: any) {
        console.log('Event:', event); // Check the event object
        this.showTab = event.target.value === 'yes'; // or whatever value represents "yes"
        console.log('showTab:', this.showTab); // Check the value of showTab
    }

    public InputPatch(BizType){
        console.log("BizType",BizType)
            this.form.get('BIZTransactionTypeSubClassName').patchValue(BizType)
            this.form.get('BIZTransactionName').patchValue(BizType)

          }

    public BizTransactionTypePicklistPatchValue(SelectedPicklistDatas: any): void {
        this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
    }
    public ChangeTab(event, TabName:string) :void{
        var i:number, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
          tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("FormTabLinks");
        for (i = 0; i < tablinks.length; i++) {
          tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(TabName).style.display = "block";
        console.log("Event:",event)
        event.currentTarget.className += " active";
      }
    
    }



export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IBizTransactionType> {
    const dbds: GBBaseDBDataService<IBizTransactionType> = new GBBaseDBDataService<IBizTransactionType>(http);
    dbds.endPoint = url;
    return dbds;
}


export function getThisPageService(store: Store, dataService: GBBaseDataService<IBizTransactionType>, dbDataService: GBBaseDBDataService<IBizTransactionType>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IBizTransactionType> {
    return new GBDataPageService<IBizTransactionType>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}