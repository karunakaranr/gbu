import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { BIZTransactionSubClassService } from 'features/erpmodules/admin/services/Biztransaction/biztransactionsubclass.service';
import { IBizTransactionSubClass } from 'features/erpmodules/admin/models/IBizTransactionSubClass';
import * as BizTransactionSubClassJSON from './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/BizTransactionSubclass.json'
import { ADMINURLS } from 'features/erpmodules/admin/URLS/urls';
@Component({
    selector: 'app-BizTransactionSubClass',
    templateUrl:'./biztransactionsubclass.component.html',
    styleUrls: ['./biztransactionsubclass.component.scss'],
    providers: [
        { provide: 'IdField', useValue: 'BIZTransactionSubClassId' },
        { provide: 'url', useValue: ADMINURLS.BizTransactionSubClass },
        { provide: 'DataService', useClass: BIZTransactionSubClassService },
        { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
        { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
    ]
})
export class BIZTransactionSubClassComponent extends GBBaseDataPageComponentWN<IBizTransactionSubClass> {
    title: string = "BizTransactionSubclass"
    BizTransactionSubClassJSON = BizTransactionSubClassJSON

    form: GBDataFormGroupWN<IBizTransactionSubClass> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "BizTransactionSubclass", {}, this.gbps.dataService);
    thisConstructor() {
        let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
        if (arrayOfValues != null) {
            this.BizTransactionSubClassPicklistFillValue(arrayOfValues.BIZTransactionSubClassId)
            this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
        } else {
            this.store.dispatch(new ButtonVisibility("Forms"))
        }
    }
  
    public BizTransactionSubClassPicklistFillValue(SelectedPicklistData: string): void {
        // console.log("id",SelectedPicklistData)
        if (SelectedPicklistData) {
            // console.log("id",SelectedPicklistData)

            this.gbps.dataService.getData(SelectedPicklistData).subscribe(BizTransactionSubClass => {
                // console.log("BizTransactionSubClass",BizTransactionSubClass)
                    this.form.patchValue(BizTransactionSubClass);                
               
            })
        }
    }

    public BizTransactionSubClassPicklistPatchValue(SelectedPicklistDatas: any): void {
        // console.log("id",SelectedPicklistDatas)
        console.log("SelectedPicklistDatasRetrival",SelectedPicklistDatas)
        this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
    }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IBizTransactionSubClass> {
    const dbds: GBBaseDBDataService<IBizTransactionSubClass> = new GBBaseDBDataService<IBizTransactionSubClass>(http);
    dbds.endPoint = url;
    return dbds;
}


export function getThisPageService(store: Store, dataService: GBBaseDataService<IBizTransactionSubClass>, dbDataService: GBBaseDBDataService<IBizTransactionSubClass>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IBizTransactionSubClass> {
    return new GBDataPageService<IBizTransactionSubClass>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
