import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { IBaseField, IPickListDetail } from 'libs/uicore/src/lib/interfaces/ibasefield';

import { PaymentTermService } from 'features/erpmodules/admin/services/General/paymentterm.service';
import { IPaymentTerm } from 'features/erpmodules/admin/models/IPaymentTerm';
import { ADMINURLS } from 'features/erpmodules/admin/URLS/urls';
import * as PaymentTermJSON from './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/PaymentTerm.json'


@Component({


    selector: 'app-PaymentTerm',
    templateUrl: './paymentterm.component.html',
    styleUrls: ['./paymentterm.component.scss'],

    providers: [
        { provide: 'IdField', useValue: 'PaymentTermId' },
        { provide: 'url', useValue: ADMINURLS.PaymentTerm },
        { provide: 'DataService', useClass: PaymentTermService },
        { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
        { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
    ]
})
export class PaymentTermComponent extends GBBaseDataPageComponentWN<IPaymentTerm> {
    title: string = "PaymentTerm"
    PaymentTermJSON =  PaymentTermJSON

    form: GBDataFormGroupWN<IPaymentTerm> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "PaymentTerm", {}, this.gbps.dataService);
    thisConstructor() {
        let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
        if (arrayOfValues != null) {
            this.PaymentTermPicklistFillValue(arrayOfValues.PaymentTermId)
            this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
        } else {
            this.store.dispatch(new ButtonVisibility("Forms"))
        }
    }



    public PaymentTermPicklistFillValue(SelectedPicklistData: string): void {
        // console.log("SelectedPicklistData:",SelectedPicklistData)
        if (SelectedPicklistData) {
            this.gbps.dataService.getData(SelectedPicklistData).subscribe(FormVariable => {
                    this.form.patchValue(FormVariable);
                
            })
        }
    }


    public PaymentTermPicklistPatchValue(SelectedPicklistDatas: any): void {
        this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
    }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IPaymentTerm> {
    const dbds: GBBaseDBDataService<IPaymentTerm> = new GBBaseDBDataService<IPaymentTerm>(http);
    dbds.endPoint = url;
    return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IPaymentTerm>, dbDataService: GBBaseDBDataService<IPaymentTerm>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IPaymentTerm> {
    return new GBDataPageService<IPaymentTerm>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
