import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { BankBranchService } from '../../services/General/bankbranch.service';
import { IBankBranch } from '../../models/IBankBranch';
import { ADMINURLS } from 'features/erpmodules/admin/URLS/urls';
import *  as BankBranchJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/BankBranch.json'
@Component({


  selector: 'app-General',
  templateUrl: './bankbranch.component.html',
  styleUrls: ['./bankbranch.component.scss'],

  providers: [
    { provide: 'IdField', useValue: 'BankBranchId'},
    { provide: 'url', useValue: ADMINURLS.BankBranch },
    { provide: 'DataService', useClass: BankBranchService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class BankBranchComponent extends GBBaseDataPageComponentWN<IBankBranch> {
  title : string = "BankBranch"
  BankBranchJSON = BankBranchJSON

form: GBDataFormGroupWN<IBankBranch> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "BankBranch", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if(arrayOfValues != null){
      this.BankBranchPicklistFillValue(arrayOfValues. BankBranchId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
  //  this.Getselectedid('0')
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
   }

 
  public BankBranchPicklistFillValue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(BankBranch => {
          // if(SelectedPicklistData != '0'){
            // console.log("SelectedPicklistData",SelectedPicklistData);
          this.form.patchValue(BankBranch);

        })
    }
    
  }
  public addnew(templateRef) {
    let dialogRef = this.dialog.open(templateRef, {
      width: '600px',
      height:"500PX"
    });
  }

  savedata

  public BankFillFunction(formdata): void {
    if (formdata) {
      this.savedata = formdata
      console.log("PARAMTERUOM",this.savedata)
    }
  }

  public newsave(){
    console.log("SAVEEE",this.savedata)
    let title = 'Bank';
    let url = ADMINURLS.Bank
    this.formaction.savemodel(this.savedata,title,url)
    this.dialog.closeAll()
  }

  public BankBranchPicklistPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)

    let BankBranchCode = this.form.get('BankBranchIfsc').value
    this.form.get('BankBranchCode').patchValue(BankBranchCode)
    console.log("MMM",this.form.value);

  }

}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IBankBranch> {
  const dbds: GBBaseDBDataService<IBankBranch> = new GBBaseDBDataService<IBankBranch>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IBankBranch>, dbDataService: GBBaseDBDataService<IBankBranch>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IBankBranch> {
  return new GBDataPageService<IBankBranch>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
