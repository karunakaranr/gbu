import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { IBaseField, IPickListDetail } from 'libs/uicore/src/lib/interfaces/ibasefield';
import { ICostAnalysis } from 'features/erpmodules/admin/models/ICostAnalysis';
import { CostAnalysisService } from 'features/erpmodules/admin/services/General/costanalysis.service';
import { ADMINURLS } from 'features/erpmodules/admin/URLS/urls';
import * as CostAnalysisJSON from './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/CostAnalysis.json'
@Component({


    selector: 'app-CostAnalysis',
    templateUrl: './costanalysis.component.html',
    styleUrls: ['./costanalysis.component.scss'],

    providers: [
        { provide: 'IdField', useValue: 'CostAnalysisId' },
        { provide: 'url', useValue: ADMINURLS.CostAnalysis },
        { provide: 'DataService', useClass: CostAnalysisService },
        { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
        { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
    ]
})
export class CostAnalysisComponent extends GBBaseDataPageComponentWN<ICostAnalysis> {
    title: string = "CostAnalysis"
    CostAnalysisJSON = CostAnalysisJSON

    form: GBDataFormGroupWN<ICostAnalysis> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "CostAnalysis", {}, this.gbps.dataService);
    thisConstructor() {
        let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
        if (arrayOfValues != null) {
            this.CostAnalysisPicklistFillValue(arrayOfValues.CostAnalysisId)
            this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
        } else {
            this.store.dispatch(new ButtonVisibility("Forms"))
        }
    }


    public CostAnalysisPicklistFillValue(SelectedPicklistData: string): void {
        // console.log("NotRetrive")
        if (SelectedPicklistData) {
            this.gbps.dataService.getData(SelectedPicklistData).subscribe(FormVariable => {
                    // console.log("SelectedPicklistData", SelectedPicklistData);
                    this.form.patchValue(FormVariable);
     

            })
        }
    }


    public CostAnalysisPicklistValue(SelectedPicklistDatas: any): void {
        // console.log("retrive")
        // console.log("SelectedPicklistDatasIDS", SelectedPicklistDatas)
        this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)

    }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<ICostAnalysis> {
    const dbds: GBBaseDBDataService<ICostAnalysis> = new GBBaseDBDataService<ICostAnalysis>(http);
    dbds.endPoint = url;
    return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<ICostAnalysis>, dbDataService: GBBaseDBDataService<ICostAnalysis>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<ICostAnalysis> {
    return new GBDataPageService<ICostAnalysis>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
