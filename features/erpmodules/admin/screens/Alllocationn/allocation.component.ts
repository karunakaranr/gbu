import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { AllocationService } from '../../services/Allocation/Allocation.service';
import { IAllocation } from '../../models/IAllocation';

import { ADMINURLS } from '../../URLS/url';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as AllocationJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/Allocation.json'

@Component({
  selector: 'app-Allocationserve',
  templateUrl:'allocation.component.html',
  styleUrls:['allocation.component.scss'],
  providers: [
    { provide: 'IdField', useValue:'MAllocationId' },
    { provide: 'url', useValue: ADMINURLS.Allocationserve},
    { provide: 'DataService', useClass: AllocationService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class AllocationComponent extends GBBaseDataPageComponentWN<IAllocation> {
  title: string = "Allocation"
  AllocationJSON = AllocationJSON;

  form: GBDataFormGroupWN<IAllocation> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "Allocation", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.AllocationVal(arrayOfValues.MAllocationId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }
  public AllocationVal(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(Allocation => {
          this.form.patchValue(Allocation);
      })
    }
  }
  public AllocationNot(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }

  public ChangeTab(event, TabName:string) :void{
    var i:number, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("FormTabLinks");
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(TabName).style.display = "block";
    console.log("Event:",event)
    event.currentTarget.className += " active";
  }
}
export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IAllocation> {
  const dbds: GBBaseDBDataService<IAllocation> = new GBBaseDBDataService<IAllocation>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IAllocation>, dbDataService: GBBaseDBDataService<IAllocation>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IAllocation> {
  return new GBDataPageService<IAllocation>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
