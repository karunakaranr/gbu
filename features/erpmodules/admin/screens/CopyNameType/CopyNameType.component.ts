import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ICopyNameType } from '../../models/ICopyNameType';
import { CopyNameTypeService } from '../../services/CopyNameType/CopyNameType.service';
import { ButtonVisibility } from 'features/layout/store/layout.actions';


import * as CopyNameTypejson from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/CopyNameType.json'

import { ADMINURLS } from '../../URLS/urls';


@Component({
  selector: "app-CopyNameType",
  templateUrl: "./CopyNameType.component.html",
  styleUrls: ["./CopyNameType.component.scss"],
  providers: [
    { provide: 'IdField', useValue: 'CopyNameTypeId' },
    { provide: 'url', useValue: ADMINURLS.CopyTypeName },
    { provide: 'DataService', useClass: CopyNameTypeService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class CopyNameTypeComponent extends GBBaseDataPageComponentWN<ICopyNameType> {
  title: string = "CopyNameType"
  CopyNameTypejson = CopyNameTypejson;


 

  form: GBDataFormGroupWN<ICopyNameType> = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'CopyNameType', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.CopyNameTypePicklist(arrayOfValues.CopyNameTypeId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }



  public CopyNameTypePicklist(SelectedPicklistData: string): void {
   
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(CopyNameType => {
        this.form.patchValue(CopyNameType);
      })
    }
  }


  public CopyNameTypePicklists(SelectedPicklistDatas: any): void {
    
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
    console.log("iteem==>",SelectedPicklistDatas)
  }
}




export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<ICopyNameType> {
  const dbds: GBBaseDBDataService<ICopyNameType> = new GBBaseDBDataService<ICopyNameType>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<ICopyNameType>, dbDataService: GBBaseDBDataService<ICopyNameType>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<ICopyNameType> {
  return new GBDataPageService<ICopyNameType>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
