import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { IBaseField, IPickListDetail } from 'libs/uicore/src/lib/interfaces/ibasefield';
import { IDataValidation } from 'features/erpmodules/admin/models/IDataValidation';
import { DataValidationService } from 'features/erpmodules/admin/services/DataSet/DataValidation/datavalidation.service';
import * as DataValidationJSON from './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/DataValidation.json'
import { ADMINURLS } from 'features/erpmodules/admin/URLS/urls';
@Component({
    selector: 'app-DataValidation',
    templateUrl:'./datavalidation.component.html',
    styleUrls: ['./datavalidation.component.scss'],
    providers: [
        { provide: 'IdField', useValue: 'DataValidationId' },
        { provide: 'url', useValue: ADMINURLS.DataValidation },
        { provide: 'DataService', useClass: DataValidationService },
        { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
        { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
    ]
})
export class DataValidationComponent extends GBBaseDataPageComponentWN<IDataValidation> {
    title: string = "DataValidation"
    DataValidationJSON = DataValidationJSON

    form: GBDataFormGroupWN<IDataValidation> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "DataValidation", {}, this.gbps.dataService);
    thisConstructor() {
        let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
        if (arrayOfValues != null) {
            this.DataValidationPicklistFillValue(arrayOfValues.DataValidationId)
            this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
        } else {
            this.store.dispatch(new ButtonVisibility("Forms"))
        }
    }


    public DataValidationPicklistFillValue(SelectedPicklistData: string): void {
        // console.log("id",SelectedPicklistData)
        if (SelectedPicklistData) {
            // console.log("id",SelectedPicklistData)

            this.gbps.dataService.getData(SelectedPicklistData).subscribe(DataValidation => {
                // console.log("id",SelectedPicklistData)

                    this.form.patchValue(DataValidation);
                
                
            })
        }
    }



    public DataValidationPicklistPatchValue(SelectedPicklistDatas: any): void {
        // console.log("id",SelectedPicklistDatas)
        this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
    }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IDataValidation> {
    const dbds: GBBaseDBDataService<IDataValidation> = new GBBaseDBDataService<IDataValidation>(http);
    dbds.endPoint = url;
    return dbds;
}


export function getThisPageService(store: Store, dataService: GBBaseDataService<IDataValidation>, dbDataService: GBBaseDBDataService<IDataValidation>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IDataValidation> {
    return new GBDataPageService<IDataValidation>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
