import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { OrganizationUnitService } from '../../services/OrganizationUnit.ts/OrganizationUnit.service';
import { IOrganizationUnit } from '../../models/IOrganizationUnit';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
// import * as JsonName from ‘JsonPath’ './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/PackSet.json';
import * as OrganizationUnitJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/OrganizationUnit.json'
import { ADMINURLS } from '../../URLS/urls';


@Component({
  selector: 'app-OrganizationUnit',
  templateUrl: './OrganizationUnit.component.html',
  styleUrls: ['./OrganizationUnit.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'OrganizationUnitId'},
    { provide: 'url', useValue: ADMINURLS.OrgUnit },
    { provide: 'DataService', useClass: OrganizationUnitService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class OrganizationUnitComponent extends GBBaseDataPageComponentWN<IOrganizationUnit > {
  title: string = 'OrganizationUnit'
  OrganizationUnitJSON = OrganizationUnitJSON;


  form: GBDataFormGroupWN<IOrganizationUnit > = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'OrganizationUnit', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.OrganizationUnitFillFunction(arrayOfValues.OrganizationUnitId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }


  public OrganizationUnitFillFunction(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(PackSet => {
        this.form.patchValue(PackSet);
      })
    }
  }


  public OrganizationUnitPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IOrganizationUnit > {
  const dbds: GBBaseDBDataService<IOrganizationUnit > = new GBBaseDBDataService<IOrganizationUnit >(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IOrganizationUnit >, dbDataService: GBBaseDBDataService<IOrganizationUnit >, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IOrganizationUnit > {
  return new GBDataPageService<IOrganizationUnit >(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
