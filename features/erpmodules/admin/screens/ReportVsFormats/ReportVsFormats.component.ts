import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { IBaseField, IPickListDetail } from 'libs/uicore/src/lib/interfaces/ibasefield';
import { ADMINURLS } from 'features/erpmodules/admin/URLS/urls';
import { ReportVsFormatService } from '../../services/ReportVsFormat/ReportVsFormat.service';
import { IReportVsFormats } from '../../models/IReportVsFormats';
import * as ReportVsFormatJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/ReportVsFormats.json'
@Component({


  selector: 'app-ReportVsFormats',
  templateUrl: './ReportVsFormats.component.html',
  styleUrls: ['./ReportVsFormats.component.scss'],

  providers: [
    { provide: 'IdField', useValue: 'ReportId'},
    { provide: 'url', useValue: ADMINURLS.ReportVsFormats },
    { provide: 'DataService', useClass: ReportVsFormatService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class ReportVsFormatsComponent extends GBBaseDataPageComponentWN<IReportVsFormats[]> {
  title : string = "ReportVsFormats"
  ReportVsFormatJSON =  ReportVsFormatJSON

form: GBDataFormGroupWN<IReportVsFormats[]> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "ReportVsFormats", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if(arrayOfValues != null){
      this.ReportVsFormatsPicklistFillValue(arrayOfValues. ReportId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
   }


   public ReportVsFormatsPicklistFillValue(SelectedPicklistData: string): void {
    console.log("SelectedPicklistData", SelectedPicklistData);
  
    if (SelectedPicklistData) {
      console.log("SelectedPicklistData", SelectedPicklistData);
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(ReportVsFormats => {
        this.form.patchValue(ReportVsFormats);
        console.log("ReportVsFormats:", ReportVsFormats);
        this.form.get('ReportId').patchValue(ReportVsFormats[0].ReportId);
        this.form.get('ReportName').patchValue(ReportVsFormats[0].ReportName);
        this.form.get('Reportvsformatarray').patchValue(ReportVsFormats);
      });
    }
  }
  
  public InstrumentLightBox(LightBoxDetail:any): void{
    document.getElementById("myModal").style.display = "block";

    console.log("LightBoxDetail:",LightBoxDetail)
  }

  public ReportVsFormatsPicklistPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IReportVsFormats[]> {
  const dbds: GBBaseDBDataService<IReportVsFormats[]> = new GBBaseDBDataService<IReportVsFormats[]>(http);
  dbds.endPoint = url;

  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IReportVsFormats[]>, dbDataService: GBBaseDBDataService<IReportVsFormats[]>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IReportVsFormats[]> {
  return new GBDataPageService<IReportVsFormats[]>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
