import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { ADMINURLS } from 'features/erpmodules/admin/URLS/urls';
import { ChargeElementService } from 'features/erpmodules/admin/services/ChargeElement/chargeelement.service';
import { IChargeElement } from 'features/erpmodules/admin/models/IChargeElement';
import * as ChargeElementJSON from './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/ChargeElement.json'

@Component({


  selector: 'app-ChargeElement',
  templateUrl: './chargeelement.component.html',
  styleUrls: ['./chargeelement.component.scss'],

  providers: [
    { provide: 'IdField', useValue: 'ChargeElementId'},
    { provide: 'url', useValue: ADMINURLS.ChargeElement },
    { provide: 'DataService', useClass: ChargeElementService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class ChargeElementComponent extends GBBaseDataPageComponentWN<IChargeElement> {
  title : string = "ChargeElement"
  ChargeElementJSON = ChargeElementJSON

form: GBDataFormGroupWN<IChargeElement> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "ChargeElement", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if(arrayOfValues != null){
      this.ChargeElementPicklistFillValue(arrayOfValues. ChargeElementId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
  //  this.Getselectedid('0')
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
   }
   
   

  public ChargeElementPicklistFillValue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(FormVariable => {
          // if(SelectedPicklistData != '0'){
            // console.log("SelectedPicklistData",SelectedPicklistData);
          this.form.patchValue(FormVariable);
        })
    }
  }


  public ChargeElementPicklistPatchValue(SelectedPicklistDatas: any): void {
    // console.log("retrive")
    // console.log("SelectedPicklistDatasIDS",SelectedPicklistDatas)
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)

  }

  public ChangeTab(event, TabName:string) :void{
    var i:number, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("FormTabLinks");
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(TabName).style.display = "block";
    console.log("Event:",event)
    event.currentTarget.className += " active";
  }

}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IChargeElement> {
  const dbds: GBBaseDBDataService<IChargeElement> = new GBBaseDBDataService<IChargeElement>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IChargeElement>, dbDataService: GBBaseDBDataService<IChargeElement>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IChargeElement> {
  return new GBDataPageService<IChargeElement>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
