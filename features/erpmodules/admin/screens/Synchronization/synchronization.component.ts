import { Component, OnInit } from '@angular/core';
import { CommonReportService } from 'features/commonreport/service/commonreport.service';
import { MenuserviceService } from 'features/layout/services/menu/menuservice.service';
import { Moduleservice } from 'features/layout/services/modulelist/module.service';

@Component({
    selector: 'app-synchronization',
    templateUrl: './synchronization.component.html',
    styleUrls: ['./synchronization.component.scss']
})

export class SynchronizationComponent implements OnInit {
    title = 'synchronization setting'
    Errorlog = []
    constructor(public moduleservice: Moduleservice, public reportService: CommonReportService, public menuserviceService: MenuserviceService) { }
    ngOnInit(): void {
    }



    public async reporttesting() {
        const moduledataresponse: any = await this.moduleservice.modulelistservice().toPromise();
    
        for (let data of moduledataresponse) {
            const Menudataresponse: any = await this.menuserviceService.menudetailsservice(data.ModuleId).toPromise();
    
            for (let item of Menudataresponse) {
                if (item.MenuType == 2 && (item.DisplayName == "Report" || item.DisplayName == "Reports")) {
                    await this.children(item);
                }
            }
        }
    
        console.log('Report testing complete');

        this.xl(this.Errorlog)
    }
    
    
    
    public async children(child) {
        if (child.children.length == 0) {
            const menures = await this.reportService.Reportdetailservice(child.Id).toPromise();
            const res = await this.reportService.Rowservice(menures, '').toPromise();
            console.log("ressss",res)
    
            if (res.ReportDetail && res.ReportDetail != undefined && res.ReportDetail.length > 0 ) {
                console.log("report working", res);
            } else {
                console.log("report not working", res);
                let error = "Server Error"
                this.Errorlog.push(child, error);
            }
        } else {
            for (let data of child.children) {
                await this.children(data);
            }
        }
    }

    public xl(data) {
        let finalizedArray = []

        for(let logdata of data){
            finalizedArray.push({
                Slno: '1',
                ModuleName: logdata['ModuleName'],
                MenuName: logdata['ReportName'],
                InchargeName: logdata['InchargeName'],
                MenuId:logdata['Id']
              })
        }
        console.log("xldata",finalizedArray)
        // let fulldata = [
        //     {
        //         Slno:"1",
        //         ModuleName: -1499999607,
        //         MenuName: -1499998941,
        //         InchargeName: "ADMIN",
        //         MenuId: "ADMIN",
        //     },
        // ]
        const csvContent = this.convertToCSV(finalizedArray);
        const blob = new Blob([csvContent], { type: 'text/csv' });
        const link = document.createElement('a');

        link.href = window.URL.createObjectURL(blob);
        link.download = 'ReportTestLog.csv';

        document.body.appendChild(link);
        link.click();

        document.body.removeChild(link);
    }

    convertToCSV(data: any): string {
        const header = Object.keys(data[0]).join(',') + '\n';
        const rows = data.map(item => Object.values(item).join(',') + '\n');
        return header + rows.join('');
    }
}
