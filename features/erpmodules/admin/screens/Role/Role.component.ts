import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { RoleService } from '../../services/Role/Role.service';
import { IRole } from '../../models/IRole';
// import { ServiceName } from ‘ServicePath’;
// import { ModelName } from ‘ModelPath’;
import { ButtonVisibility } from 'features/layout/store/layout.actions';
// import * as JsonName from ‘JsonPath’ './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/PackSet.json';
// import { url} from ‘UrlPath’;
import * as RoleJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/Role.json'
import { ADMINURLS } from '../../URLS/urls';
@Component({
  selector: 'app-Role',
  templateUrl: './Role.component.html',
  styleUrls: ['./Role.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'RoleId'},
    { provide: 'url', useValue: ADMINURLS.Role },
    { provide: 'DataService', useClass: RoleService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class RoleComponent extends GBBaseDataPageComponentWN<IRole > {
  title: string = 'Role'
  RoleJSON = RoleJSON;


  form: GBDataFormGroupWN<IRole > = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'Role', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.RoleRetrivalValue(arrayOfValues.RoleId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }


  public RoleRetrivalValue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(PackSet => {
        this.form.patchValue(PackSet);
      })
    }
  }


  public RolePatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IRole > {
  const dbds: GBBaseDBDataService<IRole > = new GBBaseDBDataService<IRole >(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IRole >, dbDataService: GBBaseDBDataService<IRole >, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IRole > {
  return new GBDataPageService<IRole >(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}


