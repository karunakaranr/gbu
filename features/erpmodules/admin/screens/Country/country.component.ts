import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { CountryService } from '../../services/country.service';
import { ICountry } from '../../models/ICountry';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as CountryJson from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/Country.json';
import { ADMINURLS } from '../../URLS/urls';


@Component({
  selector: 'app-country',
  templateUrl: './country.component.html',
  styleUrls: ['./country.component.scss'], 
  providers: [
    { provide: 'IdField', useValue: 'CountryId'},
    { provide: 'url', useValue: ADMINURLS.Country},
    { provide: 'DataService', useClass: CountryService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class CountryComponent extends GBBaseDataPageComponentWN<ICountry> {
  title: string = 'Country'
  CountryJson = CountryJson;

  form: GBDataFormGroupWN<ICountry> = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'Country', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.CountryRetrivalValue(arrayOfValues.CountryId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }
  public CountryRetrivalValue(SelectedPicklistData: string): void { 
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(Country => {
        this.form.patchValue(Country);
      })
    }
  }
  public CountryPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<ICountry> {
  const dbds: GBBaseDBDataService<ICountry> = new GBBaseDBDataService<ICountry>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<ICountry>, dbDataService: GBBaseDBDataService<ICountry>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<ICountry> {
  return new GBDataPageService<ICountry>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
