import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { IEventTypeAction } from '../../models/IEventTypeAction';
import { EventTypeActionService } from '../../services/EventTypeAction/EventTypeAction.service';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { ADMINURLS } from '../../URLS/urls';

import * as EventTypeActionJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/EventTypeAction.json'

@Component({
  selector: 'app-EventTypeAction',
  templateUrl: 'eventtypeaction.component.html',
  styleUrls: ['eventtypeaction.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'EventTypeActionId' },
    { provide: 'url', useValue:ADMINURLS.EventTypeAction },
    { provide: 'saveurl', useValue: ADMINURLS.EventTypeSave },
    { provide: 'deleteurl', useValue: ADMINURLS.EventTypeSave },
    { provide: 'DataService', useClass: EventTypeActionService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url','saveurl', 'deleteurl'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class EventTypeActionComponent extends GBBaseDataPageComponentWN<IEventTypeAction> {
  title: string = "EventTypeAction"
  EventTypeActionJSON = EventTypeActionJSON;

  form: GBDataFormGroupWN<IEventTypeAction> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "EventTypeAction", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.EventTypeActionValue(arrayOfValues.EventTypeActionId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }
  public EventTypeActionValue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.postData(SelectedPicklistData).subscribe(SelectionRound => {
        this.form.patchValue(SelectionRound);
      })
    }
  }
  public  EventTypeActionNot(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}
export function getThisDBDataService(http: GBHttpService, url: string, saveurl: string, deleteurl:string): GBBaseDBDataService<IEventTypeAction> {
  const dbds: GBBaseDBDataService<IEventTypeAction> = new GBBaseDBDataService<IEventTypeAction>(http);
  dbds.endPoint = url;
  dbds.saveurl = saveurl;
  dbds.deleteurl = deleteurl;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IEventTypeAction>, dbDataService: GBBaseDBDataService<IEventTypeAction>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IEventTypeAction> {
  return new GBDataPageService<IEventTypeAction>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
