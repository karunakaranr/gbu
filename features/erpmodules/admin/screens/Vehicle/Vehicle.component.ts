import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';

import { VehicleService } from '../../services/Vehicle/Vehicle.service';
import { IVehicle } from '../../models/IVehicle';
import { ButtonVisibility } from 'features/layout/store/layout.actions';

import  * as Vehiclejson from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/Vehicle.json'
import { ADMINURLS } from '../../URLS/urls';
@Component({
  selector: "app-Vehicle",
  templateUrl: "./Vehicle.component.html",
  styleUrls: ["./Vehicle.component.scss"],
  providers: [
    { provide: 'IdField', useValue: 'VehicleId' },
    { provide: 'url', useValue: ADMINURLS.Vehicle },
    { provide: 'DataService', useClass: VehicleService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class VehicleComponent extends GBBaseDataPageComponentWN<IVehicle> {
  title: string = "Vehicle"
  Vehiclejson = Vehiclejson;



  form: GBDataFormGroupWN<IVehicle> = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'Vehicle', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.VehicleFillFunction(arrayOfValues.VehicleId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }


  public VehicleFillFunction(SelectedPicklistData: string): void {

   console.log("filling",SelectedPicklistData)
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(Vehicle => {
        this.form.patchValue(Vehicle);
      })
    }
  }


  public VehiclePatchValue(SelectedPicklistDatas: any): void {
    
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
    console.log("iteem==>",SelectedPicklistDatas)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IVehicle> {
  const dbds: GBBaseDBDataService<IVehicle> = new GBBaseDBDataService<IVehicle>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IVehicle>, dbDataService: GBBaseDBDataService<IVehicle>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IVehicle> {
  return new GBDataPageService<IVehicle>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
