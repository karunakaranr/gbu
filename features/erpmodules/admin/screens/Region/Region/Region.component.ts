import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { ADMINURLS } from 'features/erpmodules/admin/URLS/url';
import { RegionService } from 'features/erpmodules/admin/services/Region/Region.service';
import { IRegion } from 'features/erpmodules/admin/models/IRegion';
import * as RegionJSON from './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/Region.json'



@Component({
  selector: 'app-Region',
  templateUrl: './Region.component.html',
  styleUrls: ['./Region.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'RegionId'},
    { provide: 'url', useValue: ADMINURLS.Region },
    { provide: 'DataService', useClass: RegionService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class RegionComponent extends GBBaseDataPageComponentWN<IRegion > {
  title: string = 'Region'
  RegionJSON = RegionJSON;


  form: GBDataFormGroupWN<IRegion > = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'Region', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.FormFillFunction(arrayOfValues.RegionId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }


  public FormFillFunction(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(PackSet => {
        this.form.patchValue(PackSet);
      })
    }
  }


  public PicklistPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IRegion > {
  const dbds: GBBaseDBDataService<IRegion > = new GBBaseDBDataService<IRegion >(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IRegion >, dbDataService: GBBaseDBDataService<IRegion >, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IRegion > {
  return new GBDataPageService<IRegion >(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
