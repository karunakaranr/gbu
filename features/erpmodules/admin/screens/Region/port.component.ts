import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { IPort } from '../../models/IPort';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { PortService } from '../../services/Region/port.service';
import * as PortJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/Port.json';
import {ADMINURLS} from 'features/erpmodules/admin/URLS/urls'
@Component({


  selector: 'app-Region',
  templateUrl: './port.component.html',
  styleUrls: ['./port.component.scss'],

  providers: [
    { provide: 'IdField', useValue: 'PortId'},
    { provide: 'url', useValue: ADMINURLS.Port },
    { provide: 'DataService', useClass: PortService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class PortComponent extends GBBaseDataPageComponentWN<IPort> {
  title : string = "Port"
  PortJSON = PortJSON

form: GBDataFormGroupWN<IPort> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "Port", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if(arrayOfValues != null){
      this.PortPicklistFillValue(arrayOfValues. PortId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
   }



  public PortPicklistFillValue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(FormVariable => {      
          this.form.patchValue(FormVariable);    
        
        })
    }
  }

  public PortPicklistPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IPort> {
  const dbds: GBBaseDBDataService<IPort> = new GBBaseDBDataService<IPort>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IPort>, dbDataService: GBBaseDBDataService<IPort>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IPort> {
  return new GBDataPageService<IPort>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
