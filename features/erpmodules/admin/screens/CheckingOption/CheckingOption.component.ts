import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { CheckingOptionService } from '../../services/CheckingOption/CheckingOption.service';
import { ICheckingOption } from '../../models/ICheckingOption';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
// import * as JsonName from ‘JsonPath’ './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/PackSet.json';
import * as CheckingOptionJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/CheckingOption.json'
// import { url} from ‘UrlPath’;
import { ADMINURLS } from '../../URLS/urls';


@Component({
  selector: 'app-CheckingOption',
  templateUrl: './CheckingOption.component.html',
  styleUrls: ['./CheckingOption.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'ModuleId'},
    { provide: 'url', useValue: ADMINURLS.BIZTransactionCheckingOption },
    { provide: 'DataService', useClass: CheckingOptionService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class CheckingOptionComponent extends GBBaseDataPageComponentWN<ICheckingOption > {
  title: string = 'CheckingOption'
  CheckingOptionJSON = CheckingOptionJSON;


  form: GBDataFormGroupWN<ICheckingOption > = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'CheckingOption', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.CheckingOptionFillFunction(arrayOfValues.ModuleId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }


  public CheckingOptionFillFunction(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      console.log("SelectedPicklistData",SelectedPicklistData)
      this.gbps.dataService.postData(SelectedPicklistData).subscribe(PackSet => {
        this.form.patchValue(PackSet);
        console.log("PackSet",PackSet)
      })
    }
  }


  public CheckingOptionPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<ICheckingOption > {
  const dbds: GBBaseDBDataService<ICheckingOption > = new GBBaseDBDataService<ICheckingOption >(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<ICheckingOption >, dbDataService: GBBaseDBDataService<ICheckingOption >, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<ICheckingOption > {
  return new GBDataPageService<ICheckingOption >(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
