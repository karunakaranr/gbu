import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { ADMINURLS } from 'features/erpmodules/admin/URLS/urls';
import * as UserAccessRightsJSON from './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/UserAccessRights.json'
import { UserAccessRightService } from 'features/erpmodules/admin/services/Roles&Rights/useraccessrights.service';
import { IUserAccessRights } from 'features/erpmodules/admin/models/IUserAccessRights';
@Component({


  selector: 'app-UserAccessRights',
  templateUrl: './useraccessrights.component.html',
  styleUrls: ['./useraccessrights.component.scss'],

  providers: [
    { provide: 'IdField', useValue: 'UserAccessRightsId'},
    { provide: 'url', useValue: ADMINURLS.UserAccessRights },
    { provide: 'saveurl', useValue: ADMINURLS.UserAccessRightssave },
    { provide: 'DataService', useClass: UserAccessRightService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url','saveurl'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class UserAccessRightComponent extends GBBaseDataPageComponentWN<IUserAccessRights> {
  title : string = "UserAccessRights"
  UserAccessRightsJSON =  UserAccessRightsJSON

form: GBDataFormGroupWN<IUserAccessRights> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "UserAccessRights", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if(arrayOfValues != null){
      this.UserAccessRightsPicklistFillValue(arrayOfValues. UserAccessRightsId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
   }


  public UserAccessRightsPicklistFillValue(SelectedPicklistData: string): void {
    console.log("SelectedPicklistData",SelectedPicklistData)
    if (SelectedPicklistData) {
      this.gbps.dataService.postData(SelectedPicklistData).subscribe(UserAccessRights => {
        this.form.get('UserAccessRightsId').patchValue(UserAccessRights[0].UserAccessRightsId)
        // this.form.get('UserId').patchValue(UserAccessRights[0].UserId)
        this.form.get('UserName').patchValue(UserAccessRights[0].UserName)
          this.form.get('UserAccessRightsArray').patchValue(UserAccessRights)
        })
    }
  }


  public UserAccessRightsPicklistPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string,saveurl:string): GBBaseDBDataService<IUserAccessRights> {
  const dbds: GBBaseDBDataService<IUserAccessRights> = new GBBaseDBDataService<IUserAccessRights>(http);
  dbds.endPoint = url;
  dbds.saveurl = saveurl;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IUserAccessRights>, dbDataService: GBBaseDBDataService<IUserAccessRights>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IUserAccessRights> {
  return new GBDataPageService<IUserAccessRights>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
