import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { IBaseField, IPickListDetail } from 'libs/uicore/src/lib/interfaces/ibasefield';
import { IRoleModule } from 'features/erpmodules/admin/models/IRoleModule';
import { RoleModuleService } from 'features/erpmodules/admin/services/Roles&Rights/rolemodule.service';
import { ADMINURLS } from 'features/erpmodules/admin/URLS/urls';
import * as RoleModuleJSON from './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/RoleModule.json'
@Component({


  selector: 'app-RoleModule',
  templateUrl: './rolemodule.component.html',
  styleUrls: ['./rolemodule.component.scss'],

  providers: [
    { provide: 'IdField', useValue: 'RoleId'},
    { provide: 'url', useValue: ADMINURLS.RoleModule },
    { provide: 'saveurl', useValue: ADMINURLS.RoleModuleSave},
    { provide: 'deleteurl', useValue: ADMINURLS.RoleModuleSave},
    { provide: 'DataService', useClass: RoleModuleService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url', 'saveurl','deleteurl'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class RoleModuleComponent extends GBBaseDataPageComponentWN<IRoleModule[]> {
  title : string = "RoleModule"
  RoleModuleJSON =  RoleModuleJSON

form: GBDataFormGroupWN<IRoleModule[]> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "RoleModule", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if(arrayOfValues != null){
      this.RoleModulePicklistFillValue(arrayOfValues. RoleId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
   }


   public RoleModulePicklistFillValue(SelectedPicklistData: string): void {
    console.log("SelectedPicklistData", SelectedPicklistData);
  
    if (SelectedPicklistData) {
      console.log("SelectedPicklistData", SelectedPicklistData);
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(RoleModule => {
        console.log("RoleModule:", RoleModule);
  
        if (Array.isArray(RoleModule) && RoleModule.length > 0) {
          console.log("RoleModule is not empty");
          this.form.get('RoleId').patchValue(RoleModule[0].RoleId);
          this.form.get('RoleName').patchValue(RoleModule[0].RoleName);
          this.form.get('RoleModuleArray').patchValue(RoleModule);
          console.log("Form Patch:", this.form.value);
        } else {
          console.log("RoleModule is empty");
          this.form.get('RoleId').patchValue(SelectedPicklistData);
        }

      });
    }
  }
  

  public RoleModulePicklistPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string, saveurl: string, deleteurl:string): GBBaseDBDataService<IRoleModule[]> {
  const dbds: GBBaseDBDataService<IRoleModule[]> = new GBBaseDBDataService<IRoleModule[]>(http);
  dbds.endPoint = url;
  dbds.saveurl = saveurl;
  dbds.deleteurl = deleteurl;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IRoleModule[]>, dbDataService: GBBaseDBDataService<IRoleModule[]>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IRoleModule[]> {
  return new GBDataPageService<IRoleModule[]>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
