import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { IPage } from '../../models/IPage';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { IBaseField, IPickListDetail } from 'libs/uicore/src/lib/interfaces/ibasefield';
import { PageService } from '../../services/Roles&Rights/page.service';
import { ADMINURLS } from 'features/erpmodules/admin/URLS/urls';
import * as PageJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/Page.json'
@Component({


  selector: 'app-Role&Rights',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.scss'],

  providers: [
    { provide: 'IdField', useValue: 'PageId' },
    { provide: 'url', useValue: ADMINURLS.Page },
    { provide: 'DataService', useClass: PageService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class PageComponent extends GBBaseDataPageComponentWN<IPage> {
  title: string = "Page"
  PageJSON = PageJSON

  form: GBDataFormGroupWN<IPage> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "Page", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.PagePicklistFillValue(arrayOfValues.PageId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }


  public PagePicklistFillValue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(FormVariable => {
        this.form.patchValue(FormVariable);

      })
    }
  }


  public PagePicklistPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IPage> {
  const dbds: GBBaseDBDataService<IPage> = new GBBaseDBDataService<IPage>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IPage>, dbDataService: GBBaseDBDataService<IPage>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IPage> {
  return new GBDataPageService<IPage>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
