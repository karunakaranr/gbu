import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { RegionTypeService } from '../../services/RegionType/RegionType.service';
import { IRegionType } from '../../models/IRegionType';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { ADMINURLS } from '../../URLS/url';
import * as RegionTypeJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/RegionType.json';
@Component({
  selector: 'app-RegionType',
  templateUrl: 'regiontype.component.html',
  styleUrls:['regiontype.component.scss'],
  providers: [
    { provide: 'IdField', useValue:'RegionTypeId' },
    { provide: 'url', useValue:ADMINURLS.RegionType},
    { provide: 'DataService', useClass: RegionTypeService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class RegionTypeComponent extends GBBaseDataPageComponentWN<IRegionType> {
  title: string = "RegionType"
  RegionTypeJSON= RegionTypeJSON;

  form: GBDataFormGroupWN<IRegionType> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "RegionType", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.RegionTypevalue(arrayOfValues.RegionTypeId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }
  public RegionTypevalue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(RegionType => {
          this.form.patchValue(RegionType);
      })
    }
  }
  public RegionTypeNot(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}
export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IRegionType> {
  const dbds: GBBaseDBDataService<IRegionType> = new GBBaseDBDataService<IRegionType>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IRegionType>, dbDataService: GBBaseDBDataService<IRegionType>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IRegionType> {
  return new GBDataPageService<IRegionType>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
