
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin.component';
import { CheckSettingComponent } from './screens/CheckSetting.component'
import { SynchronizationComponent } from './screens/Synchronization/synchronization.component';

import { DBJoinComponent } from './screens/DBJoin/dbjoin.component';
import { CountryComponent } from './screens/Country/country.component';
import { TransactionTypeComponent } from './screens/BIZTransaction/TransactionType/TransactionType.component';
import { DepartmentComponent } from './screens/Organization/Department.component';
import { CompanyComponent } from './screens/Organization/Company/company.component';
import { CurrencyComponent } from './screens/Organization/Currency/currency.component';
import { BranchComponent } from './screens/Organization/Branch/branch.component';
import { BizTransactionComponent } from './screens/BIZTransaction/Biztransaction/biztransaction.component';
import { StateComponent } from './screens/State/state.component';
import { PeriodComponent } from './screens/Period/period.component';
import { FinanceBookComponent } from './screens/Organization/FinanceBook/financebook.component';
import { PortComponent } from './screens/Region/port.component';
import { CostCenterTypeComponent } from './screens/CostCenter/costcentertype.component';
import { AllocationTypeComponent } from './screens/Allocation/allocationtype.component';
import { JobDefineComponent } from './screens/Event/jobdefine.component';
import { PageComponent } from './screens/Role&Rights/page.component';
import { BankBranchComponent } from './screens/General/bankbranch.component';
import { CostAnalysisComponent } from './screens/General/CostAnalysis/costanalysis.component';
import { RoleModuleComponent } from './screens/Role&Rights/RoleModule/rolemodule.component';
import { UserGroupComponent } from './screens/Role&Rights/UserGroup/usergroup.component';
import { PaymentTermComponent } from './screens/General/PaymentTerm/paymentterm.component';
import { OrganizationGroupComponent } from './screens/Organization/OrganizationGroup/organizationgroup.component';
import { PartyTaxTypeComponent } from './screens/PartyTaxType/PartyTaxType.component';
import { CopyNameTypeComponent } from './screens/CopyNameType/CopyNameType.component';
import { CheckingComponent } from './screens/Checking/checking/checking.component';
import { DataValidationComponent } from './screens/DataSet/DataValidation/datavalidation.component';
import { UserAccessRightComponent } from './screens/Role&Rights/UserAccessRights/useraccessrights.component';
import { DivisionComponent } from './screens/Division/Division.component';
import { AnalysisComponent } from './screens/Analysis/analysis.component';
import { RoleComponent } from './screens/Role/Role.component';
import { InspectionAreaComponent } from './screens/InspectionArea/inspectionarea.component';
import { BIZTransactionSubClassComponent } from './screens/BIZTransaction/BizTransactionSubClass/biztransactionsubclass.component';
import { ChargeElementComponent } from './screens/Formula&Charge/ChargeElement/chargeelement.component';
import { RoleAlertComponent } from './screens/Role&Rights/RoleAlert/rolealert.component';
import { BIZTransactionKeyComponent } from './screens/BIZTransaction/BizTransactionKey/biztransactionkey.component';
import { CalenderComponent } from './screens/Calender/Calender.component';
import { WorkFlowRuleComponent } from './screens/WorkFlowRule/workflowrule.component';
import { EventTypeActionComponent } from './screens/EventTypeAction/eventtypeaction.component';
import { RegionTypeComponent } from './screens/RegionType/regiontype.component';
import { CityComponent } from './screens/City/city.component';
import { FormulaFieldComponent } from './screens/FormulaField/formulafield.component';
import { FileComponent } from './screens/File/file.component';
import { BizTransactionTypeComponent } from './screens/BIZTransaction/BizTransactionType/biztransactiontype.component';
import { CodeDefineComponent } from './screens/Organization/CodeDefine/codedefine.component';
import { VehicleComponent } from './screens/Vehicle/Vehicle.component';
import { SecurityQuestionComponent } from './screens/SecurityQuestion/SecurityQuestion.component';
import { AllocationComponent } from './screens/Alllocationn/allocation.component';
import { OrganizationUnitComponent } from './screens/OrganizationUnit/OrganizationUnit.component';
import { CheckingOptionComponent } from './screens/CheckingOption/CheckingOption.component';
import { TestRunComponent } from './screens/TestRun/TestRun.component';
import { RegionComponent } from './screens/Region/Region/Region.component';
import { ReportVsFormatsComponent } from './screens/ReportVsFormats/ReportVsFormats.component';
const routes: Routes = [
  {
    path: 'testrun',
    component:TestRunComponent
  },
  {
    path: 'ReportVsFormat',
    component:ReportVsFormatsComponent
  },
  
  {
    path: 'checkingoption',
    component:CheckingOptionComponent
  },
  {
    path: 'Region',
    component: RegionComponent

  },
  {
    path: 'allocation',
    component: AllocationComponent

  },
  {
    path: 'OrganizationUnit',
    component: OrganizationUnitComponent
  },
  {
    path: 'EventTypeAction',
    component: EventTypeActionComponent
  },
  {
    path: 'WorkFlowRule',
    component: WorkFlowRuleComponent
  },
  {
    path: 'File',
    component: FileComponent

  },
  {
    path: 'vehicle',
    component: VehicleComponent

  },
  {
    path: 'securityquestion',
    component: SecurityQuestionComponent

  },
  {
    path: 'codedefine',
    component: CodeDefineComponent

  },
  {
    path: 'biztransactiontype',
    component: BizTransactionTypeComponent

  },

  {
    path: 'FormulaField',
    component: FormulaFieldComponent
  },
  {
    path: 'city',
    component: CityComponent
  },
  {
    path: 'RegionType',
    component: RegionTypeComponent
  },
  // {
  //   path:'EventTypeAction',
  //   component:EventTypeActionComponent
  // },
  // {
  //   path:'WorkFlowRule',
  //   component:WorkFlowRuleComponent
  // },
  {
    path: 'role',
    component: RoleComponent

  },
  {
    path: '',
    component: AdminComponent

  },
  {
    path: 'biztransactionkey',
    component: BIZTransactionKeyComponent

  },
  {
    path: 'calender',
    component: CalenderComponent

  },
  {
    path: 'rolealert',
    component: RoleAlertComponent

  },
  {
    path: 'chargeelement',
    component: ChargeElementComponent

  },
  {
    path: 'inspectionarea',
    component: InspectionAreaComponent

  },
  {
    path: 'biztransactionsubclass',
    component: BIZTransactionSubClassComponent
  },
  {
    path: 'analysis',
    component: AnalysisComponent

  },
  {
    path: 'Division',
    component: DivisionComponent
  },
  {
    path: 'checking',
    component: CheckingComponent

  },
  {
    path: 'datavalidation',
    component: DataValidationComponent
  },
  {
    path: 'copynametype',
    component: CopyNameTypeComponent

  },
  {
    path: 'partytaxtype',
    component: PartyTaxTypeComponent
  },
  {
    path: 'paymentterm',
    component: PaymentTermComponent

  },
  {
    path: 'useraccessright',
    component: UserAccessRightComponent

  },
  {
    path: 'usergroup',
    component: UserGroupComponent

  },
  {
    path: 'rolemodule',
    component: RoleModuleComponent

  },
  {
    path: 'organizationgroup',
    component: OrganizationGroupComponent

  },
  {
    path: 'costanalysis',
    component: CostAnalysisComponent

  },
  {
    path: 'bankbranch',
    component: BankBranchComponent

  },
  {
    path: 'page',
    component: PageComponent

  },
  {
    path: 'Period',
    component: PeriodComponent

  },
  {
    path: 'allocationtype',
    component: AllocationTypeComponent

  },
  {
    path: 'jobdefine',
    component: JobDefineComponent

  },
  {
    path: 'port',
    component: PortComponent

  },
  {
    path: 'costcentertype',
    component: CostCenterTypeComponent

  },
  {
    path: 'financebook',
    component: FinanceBookComponent

  }
  ,
  {
    path: 'State',
    component: StateComponent

  },
  {
    path: 'biztransaction',
    component: BizTransactionComponent

  },
  {
    path: 'branch',
    component: BranchComponent

  },
  {
    path: 'company',
    component: CompanyComponent

  },
  {
    path: 'transactiontype',
    component: TransactionTypeComponent
  },
  {
    path: 'department',
    component: DepartmentComponent
  },
  {
    path: 'currency',
    component: CurrencyComponent

  },

  {
    path: 'Country',
    component: CountryComponent

  },

  {
    path: 'DBJoin',
    component: DBJoinComponent

  },
  {
    path: 'checksetting',
    children: [
      {
        path: 'list',
        component: CheckSettingComponent
      },
    ]
  },
  {
    path: 'synchronization',
    children: [
      {
        path: 'list',
        component: SynchronizationComponent
      },
    ]
  },
  { path: 'themestest', loadChildren: () => import(`./../../themes/themes.module`).then(m => m.ThemesModule) },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
