export interface IPartyTaxType {
    PartyTaxTypeId: number
    PartyTaxTypeCode: string
    PartyTaxTypeName: string
    PartyTaxTypeLocationType: number
    PartyTaxTypeRegistration: number
    PartyTaxTypeDetailArray: PartyTaxTypeDetailArray[]
    PartyTaxTypeCreatedById: number
    PartyTaxTypeCreatedOn: string
    PartyTaxTypeModifiedById: number
    PartyTaxTypeModifiedOn: string
    PartyTaxTypeSortOrder: number
    PartyTaxTypeStatus: number
    PartyTaxTypeVersion: number
  }
  
  export interface PartyTaxTypeDetailArray {
    PartyTaxTypeDetailId: number
    PartyTaxTypeId: number
    PartyTaxTypeCode: string
    PartyTaxTypeName: string
    PartyTaxTypeDetailSlNo: number
    BIZTransactionClassId: number
    BIZTransactionClassCode: string
    BIZTransactionClassName: string
    TransactionTypeId: number
    TransactionTypeCode: string
    TransactionTypeName: string
    TransactionTypeType: number
    DutyTransactionTypeId: number
    DutyTransactionTypeCode: string
    DutyTransactionTypeName: string
  }
  