export interface IReportVsFormats {
    ReportFormatName: string
    ReportFormatId: number
    ReportFormatReportFile: string
    ReportVsFormatsId: number
    ReportId: number
    ReportName: string
    SlNo: number
    ReportFormatDataSourceFileName: string
    ApplicableMenuIds: string
    ApplicableMenuIdsName: string
    ReportVsFormatsISOReferenceNumber: string
    ReportVsFormatsIsVisible: number
    ReportVsFormatsDisplayName: string
  }
  