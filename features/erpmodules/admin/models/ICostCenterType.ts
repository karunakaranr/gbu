export interface ICostCenterType {
    CostCenterTypeId: number
    CostCenterTypeCode: string
    CostCenterTypeName: string
    CostCenterTypeType: number
    CostCenterTypeIsHierachicalApplicable: number
    OrganizationUnitId: number
    OrganizationUnitCode: string
    OrganizationUnitName: string
    CostCenterTypeIsCodeRequired: number
    CostCenterTypeCreatedById: number
    CostCenterTypeCreatedOn: string
    CostCenterTypeModifiedById: number
    CostCenterTypeModifiedOn: string
    CostCenterTypeSortOrder: number
    CostCenterTypeStatus: number
    CostCenterTypeVersion: number
  }
  