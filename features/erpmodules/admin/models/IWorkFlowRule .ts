export interface IWorkFlowRule  {
    WorkflowRuleId: number
    WorkflowRuleCode: string
    WorkflowRuleName: string
    WorkflowDeploymentId: number
    WorkflowDeploymentName: string
    EntityId: number
    EntityCode: string
    EntityName: string
    TenantId: number
    TenantCode: string
    TenantName: string
    Remarks: any
    WorkflowRuleCreatedById: number
    WorkflowRuleCreatedOn: string
    WorkflowRuleModifiedById: number
    WorkflowRuleModifiedOn: string
    WorkflowRuleSortOrder: number
    WorkflowRuleStatus: number
    WorkflowRuleVersion: number
    WorkflowRuleSourceType: number
    WorkflowRuleDetailArray: WorkflowRuleDetailArray[]
    WorkflowRuleEvalCondition: string
    WorkflowDeploymentErpId: number
    WorkflowDeploymentErpName: string
  }
  
  export interface WorkflowRuleDetailArray {
    WorkflowRuleDetailId: number
    WorkflowRuleId: number
    WorkflowRuleWorkflowDeploymentId: any
    WorkflowRuleDetailSlNo: number
    WorkflowRuleDetailApproverExpression: string
    WorkflowRuleDetailRemarks: string
    WorkflowRuleDetailEscalationExpression: string
    WorkflowRuleDetailVariablesForApproval: string
  }
  