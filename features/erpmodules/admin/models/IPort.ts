export interface IPort {
    PortId: number
    PortName: string
    PortCode: string
    CountryId: number
    CountryCode: string
    CountryName: string
    PortGstPortCode: string
    PortCreatedById: number
    PortCreatedOn: string
    PortCreatedByName: string
    PortModifiedById: number
    PortModifiedOn: string
    PortModifiedByName: string
    PortSortOrder: number
    PortStatus: number
    PortVersion: number
  }
  