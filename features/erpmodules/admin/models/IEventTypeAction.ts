export interface IEventTypeAction {
  EventTypeActionId: number
  EventTypeActionNature: number
  EventTypeId: number
  EventTypeCode: string
  EventTypeName: string
  TriggerReportId: number
  TriggerReportCode: string
  TriggerReportName: string
  EventTypeActionDetailArray: EventTypeActionDetailArray[]
  MailId: number
  MailDescription: string
  EventTypeActionCreatedById: number
  EventTypeActionCreatedOn: string
  EventTypeActionModifiedById: number
  EventTypeActionModifiedOn: string
  EventTypeActionSortOrder: number
  EventTypeActionStatus: number
  EventTypeActionVersion: number
  EventTypeActionSourceType: number
}

export interface EventTypeActionDetailArray {
  EventTypeActionDetailId: number
  EventTypeActionId: number
  EventTypeActionDetailSlNo: number
  ActionId: number
  ActionDescription: string
  EventTypeActionDetailDescription: string
  EventTypeActionDetailIsActive: number
  EventTypeActionDetailIsDirectCall: number
  EventTypeActionDetailIsSchedulerCall: number
  EventTypeActionDetailEvalCondition: string
  EventTypeActionDetailSourceType: number
}
