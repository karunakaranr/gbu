export interface IPage {
    PageId: number
    PageCode: string
    PageName: string
    PageRemarks: string
    PageLayOut: string
    PageType: number
    ModuleId: number
    ModuleCode: string
    ModuleName: string
    ImageId: number
    ImageName: string
    ImageLinkURL: string
    PageCreatedById: number
    PageCreatedOn: string
    PageModifiedById: number
    PageModifiedOn: string
    PageCreatedByName: string
    PageModifiedByName: string
    PageSortOrder: number
    PageStatus: number
    PageVersion: number
    PageSourceType: number
  }
  