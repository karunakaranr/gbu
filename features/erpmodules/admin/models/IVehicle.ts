export interface IVehicle {
    VehicleId: number
    VehicleNumber: string
    VehicleTypeId: number
    VehicleTypeCode: string
    VehicleTypeName: string
    DriverId: number
    DriverDriverName: string
    VehicleEmptyWeight: number
    VehicleTonnage: number
    VehicleOwnerShipType: number
    TransporterId: number
    TransporterCode: string
    TransporterName: string
    AssetId: number
    AssetCode: string
    AssetName: string
    VehicleOUIds: string
    VehicleOUIdsName: string
    VehicleYOM: number
    VehicleRoadTaxValidity: string
    VehicleFCDate: string
    VehicleFCExpiryDate: string
    InsuranceId: number
    InsurancePolicyNumber: string
    InsuranceEndDate: string
    OwnerContactId: number
    OwnerContactName: string
    MakeId: number
    MakeCode: string
    MakeName: string
    FuelTypeId: number
    FuelTypeCode: string
    FuelTypeName: string
    CapacityRangeId: number
    CapacityRangeCode: string
    CapacityRangeName: string
    VehicleMinimumTonnage: number
    VehicleAddon: VehicleAddon[]
    VehicleCreatedById: number
    VehicleCreatedOn: string
    VehicleModifiedById: number
    VehicleModifiedOn: string
    VehicleSortOrder: number
    VehicleStatus: number
    VehicleVersion: number
  }
  
  export interface VehicleAddon {
    Id: number
    FeAddon: FeAddon
    GetAddon: string
    Attributes: Attributes
  }
  
  export interface FeAddon {
    PETROL: string
    VEHPETROL: number
  }
  
  export interface Attributes {}
  