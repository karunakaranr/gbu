export interface IPaymentTerm {
    PaymentTermId: number
    PaymentTermCode: string
    PaymentTermName: string
    PaymentTermRemarks: string
    PaymentTermDetailArray: PaymentTermDetailArray[]
    PaymentTermCreatedById: number
    PaymentTermCreatedOn: string
    PaymentTermCreatedByName: string
    PaymentTermModifiedById: number
    PaymentTermModifiedOn: string
    PaymentTermModifiedByName: string
    PaymentTermStatus: number
    PaymentTermVersion: number
    PaymentTermSortOrder: number
  }
  
  export interface PaymentTermDetailArray {
    PaymentTermDetailId: number
    PaymentTermId: number
    PaymentTermCode: string
    PaymentTermName: string
    PaymentTermDetailSlNo: number
    PaymentTermDetailPaymentType: number
    PaymentTermDetailNumberOfDays: number
    PaymentTermDetailWEFDateType: string
    PaymentTermDetailPaymentPercentage: number
    CashDiscountId: number
    CashDiscountCode: string
    CashDiscountName: string
  }
  