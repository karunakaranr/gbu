export interface IRoleAlert {
    RoleAlertId: number
    RoleAlertSlno: number
    RoleId: number
    RoleCode: string
    RoleName: string
    ModuleId: number
    ModuleCode: string
    ModuleName: string
    RoleAlertRoleOption: number
    DataAlertId: number
    DataAlertCode: string
    DataAlertName: string
    NumberOfRecords: number
  }
  