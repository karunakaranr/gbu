export interface IAllocationType {
    AllocationTypeId: number
    AllocationTypeCode: string
    AllocationTypeName: string
    AllocationTypeIsPostCostCenter: number
    CostCategoryId: number
    CostCategoryCode: string
    CostCategoryName: string
    AllocationTypeIsMultiLevel: number
    AllocationTypeIsDateBased: number
    AllocationTypeIsCapital: number
    EntityId: number
    EntityCode: string
    EntityName: string
    AllocationTypeCreatedById: number
    AllocationTypeCreatedOn: string
    AllocationTypeCreatedByName: string
    AllocationTypeModifiedById: number
    AllocationTypeModifiedOn: string
    AllocationTypeModifiedByName: string
    AllocationTypeSortOrder: number
    AllocationTypeStatus: number
    AllocationTypeVersion: number
  }
  