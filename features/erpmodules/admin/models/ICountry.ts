export interface ICountry {
    CountryId: number
    CountryCode: string
    CountryName: string
    CountryCreatedOn: string
    CountryModifiedOn: string
    CountryModifiedByName: string
    CountryCreatedByName: string
    CountryISDCode: string
    CountryStatus: number
    CountryGstCountryCode: string
    CountryVersion: number
  }