export interface IBranch {
    BranchId: number
    BranchCode: string
    BranchName: string
    BranchShortName: string
    CompanyId: number
    CompanyCode: string
    CompanyName: string
    CompanyShortName: string
    BranchCreatedById: number
    BranchCreatedByName: string
    BranchCreatedOn: string
    BranchModifiedById: number
    BranchModifiedByName: string
    BranchModifiedOn: string
    BranchSortOrder: number
    BranchStatus: number
    BranchVersion: number
  }
  