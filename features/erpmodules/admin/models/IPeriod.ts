export interface IPeriod {
    PeriodId: number;
    PeriodCode: string;
    PeriodName: string;
    PeriodFromDate: string;
    PeriodToDate: string;
    PeriodCreatedOn: string;
    PeriodModifiedOn: string;
    PeriodModifiedByName: string;
    PeriodCreatedByName: string;
    PeriodStatus: number;
    PeriodGroup: number;
    PeriodVersion: number;
  }