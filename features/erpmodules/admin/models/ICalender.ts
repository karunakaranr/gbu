export interface ICalender {
    CalendarId: number
    CalendarCode: string
    CalendarName: string
    CalendarCreatedById: number
    CalendarCreatedOn: string
    CalendarModifiedById: number
    CalendarModifiedOn: string
    CalendarSortOrder: number
    CalendarStatus: number
    CalendarVersion: number
    CalendarDetailArray: CalendarDetailArray[]
  }
  
  export interface CalendarDetailArray {
    CalendarDetailId: number
    CalendarId: number
    CalendarCode: string
    CalendarName: string
    CalendarDetailSlNo: number
    CalendarDetailDate: string
    CalendarDetailDay: string
    CalendarDetailType: number
    CalendarDetailParticulars: string
  }