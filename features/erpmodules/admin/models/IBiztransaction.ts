export interface IBizTransaction {
    BIZTransactionId: number
    BIZTransactionCode: string
    BIZTransactionName: string
    BIZTransactionSubClassId: number
    BIZTransactionSubClassCode: string
    BIZTransactionSubClassName: string
    ProcessId: number
    ProcessCode: string
    ProcessName: string
    BIZTransactionCreatedById: number
    BIZTransactionCreatedOn: string
    BIZTransactionModifiedById: number
    BIZTransactionModifiedOn: string
    BIZTransactionModifiedByName: string
    BIZTransactionCreatedByName: string
    OrganizationUnitName: string
    BIZTransactionSortOrder: number
    BIZTransactionStatus: number
    BIZTransactionVersion: number
    IsAlloc: number
    FinanceBookId: number
    FinanceBookName: string
  }
  