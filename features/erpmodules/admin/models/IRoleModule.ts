export interface IRoleModule {
    RoleId: number
    RoleCode: string
    RoleName: string
    RoleVsModulesId: number
    RoleVsModulesSlNo: number
    ModuleId: number
    ModuleCode: string
    ModuleName: string
    DefaultMenuId: number
    DefaultMenuCode: string
    DefaultMenuName: string
  }
  

  