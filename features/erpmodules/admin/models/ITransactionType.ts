export interface ITransactionType {
    RoleId: number
    RoleName: string
    MenuId: number
    MenuName: string
    BizTransactionTypeId: number
    BizTransactionTypeCode: string
    BizTransactionTypeName: string
    BizTransactionId: number
    BizTransactionCode: string
    BizTransactionName: string
    BizTransactionClassId: number
    BizTransactionClassCode: string
    BizTransactionClassName: string
    BizTransactionSubClassId: number
    BizTransactionSubClassCode: string
    BizTransactionSubClassName: string
    Allow: string
    View: string
    Insert: string
    Update: string
    Delete: string
    Print: string
    Export: string
    BackDateEntryDay: string
    BackDateUpdateDay: string
    BackDateDeleteDay: string
    ForwardEntryDate: string
    OverAllLockDate: string
    BackDateCheckType: number
    LockOpenCloseStatus: number
    LockOpenCloseDate: string
    EntityId: number
    URIParameterValue: string
  }
  