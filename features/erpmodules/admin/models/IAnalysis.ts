export interface IAnalysis {
    AnalysisFieldsId: number
    AnalysisId: number
    AnalysisCode: string
    AnalysisName: string
    AnalysisFieldsSlNo: number
    DBObjectFieldsId: number
    DBObjectFieldsName: string
    DBObjectName: string
    DBObjectId: number
    AnalysisFieldsFilterExpression: string
    AnalysisFieldsVariables: string
    AnalysisFieldsFieldType: number
    AnalysisFieldsExpression: string
    AnalysisFieldsDisplayName: string
    AnalysisObjectId: number
    CriteriaAttributeId: number 
    CriteriaAttributeName: string
    IsFilter: number
    AnalysisFieldsSourceType: number
    AnalysisFieldsSortOrder: number
    AnalysisFieldsStatus: number
    AnalysisFieldsVersion: number
    AnalysisFieldsCreatedById: number
    AnalysisFieldsCreatedOn: string
    AnalysisFieldsModifiedById: number
    AnalysisFieldsModifiedOn: string
    AnalysisFieldsCreatedByName: string
    AnalysisModifiedByName: string
  }
  