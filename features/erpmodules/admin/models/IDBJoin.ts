export interface IDBJoin {
    DBJoinId: number
    FromDBObjectId: number
    FromDBObjectName: string
    ToObjectId: number
    ToObjectName: string
    DBJoinDescription: string
    DBJoinRelationType: number
    DBJoinJoinType: number
    DBJoinExpression: string
    IndirectFromDbobjectId: number
    IndirectFromDbobjectName: string
    IndirectToDbobjectId: number
    IndirectToDbobjectName: string
    DBJoinSourceType: number
    DBJoinSortOrder: number
    DBJoinStatus: number
    DBJoinVersion: number
    DBJoinCreatedById: number
    DBJoinCreatedOn: string
    DBJoinModifiedById: number
    DBJoinModifiedOn: string
    DBJoinCreatedByName: string
    DBJoinModifiedByName: string
  }
  
  