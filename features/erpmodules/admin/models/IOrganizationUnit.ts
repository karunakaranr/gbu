export interface IOrganizationUnit {
    OrganizationUnitId: number
    OrganizationUnitCode: string
    OrganizationUnitName: string
    OrganizationUnitShortName: string
    CompanyId: number
    CompanyCode: string
    CompanyName: string
    CompanyShortName: string
    BranchId: number
    BranchCode: string
    BranchName: string
    BranchShortName: string
    DivisionId: number
    DivisionCode: string
    DivisionName: string
    DivisionShortName: string
    OrganizationUnitOverLockDate: string
    CurrencyId: number
    CurrencyCode: string
    CurrencyName: string
    ColorId: number
    ColorCode: string
    ColorName: string
    CalendarId: number
    CalendarCode: string
    CalendarName: string
    OrganizationUnitLogo1: string
    OrganizationUnitLogo2: string
    OrganizationUnitDisplayName: string
    OrganizationUnitAddon: OrganizationUnitAddon[]
    DefaultAddressId: number
    OrganizationUnitCreatedById: number
    OrganizationUnitCreatedByName: any
    OrganizationUnitCreatedOn: string
    OrganizationUnitModifiedById: number
    OrganizationUnitModifiedByName: any
    OrganizationUnitModifiedOn: string
    OrganizationUnitSortOrder: number
    OrganizationUnitStatus: number
    OrganizationUnitVersion: number
    OrganizationUnitSourceType: number
    PeriodGroup: number
  }
  
  export interface OrganizationUnitAddon {
    Id: number
    FeAddon: string
    GetAddon: string
    Attributes: Attributes
  }
  
  export interface Attributes {}
  