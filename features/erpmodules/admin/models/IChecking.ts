export interface IChecking {
    CheckingId: number
    CheckingCode: string
    CheckingName: string
    CheckingDisplayText: string
    CheckingCreatedById: number
    CheckingCreatedByName: string
    CheckingCreatedOn: string
    CheckingModifiedById: number
    CheckingModifiedByName: string
    CheckingModifiedOn: string
    CheckingSortOrder: number
    CheckingStatus: number
    CheckingVersion: number
    CheckingSourceType: number
  }
  