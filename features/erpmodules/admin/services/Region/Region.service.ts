import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { IRegion } from '../../models/IRegion';


 
@Injectable({
  providedIn: 'root'
})
export class RegionService extends GBBaseDataServiceWN<IRegion> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IRegion>) {
    super(dbDataService, 'RegionId');
  }
}
