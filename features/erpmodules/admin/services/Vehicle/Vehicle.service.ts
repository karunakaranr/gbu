import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN,GBBaseDBDataService } from 'libs/gbdata/src';


import { IVehicle } from '../../models/IVehicle';

 
@Injectable({
  providedIn: 'root'
})
export class VehicleService extends GBBaseDataServiceWN<IVehicle> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IVehicle>) {
    super(dbDataService, 'VehicleId');
  }
}
