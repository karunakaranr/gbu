import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { IBizTransactionSubClass } from '../../models/IBizTransactionSubClass';


 
@Injectable({
  providedIn: 'root'
})
export class BIZTransactionSubClassService extends GBBaseDataServiceWN<IBizTransactionSubClass> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IBizTransactionSubClass>) {
    super(dbDataService, 'BIZTransactionSubClassId');
  }
}
