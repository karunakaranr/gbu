import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { IBizTransaction } from 'features/layout/models/IBizTransaction';


 
@Injectable({
  providedIn: 'root'
})
export class BIZTransactionService extends GBBaseDataServiceWN<IBizTransaction> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IBizTransaction>) {
    super(dbDataService, 'BIZTransactionId');
  }
}
