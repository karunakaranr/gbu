import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';


import { IDBJoin } from '../models/IDBJoin';


 
@Injectable({
  providedIn: 'root'
})
export class DBJoinService extends GBBaseDataServiceWN<IDBJoin> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IDBJoin>) {
    super(dbDataService, 'DBJoinId');
  }
}
