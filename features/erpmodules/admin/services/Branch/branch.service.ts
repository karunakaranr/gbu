import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { IBranch } from '../../models/IBranch';


 
@Injectable({
  providedIn: 'root'
})
export class BranchService extends GBBaseDataServiceWN<IBranch> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IBranch>) {
    super(dbDataService, 'BranchId');
  }
}
