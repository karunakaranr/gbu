import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';


import { IOrganizationUnit } from '../../models/IOrganizationUnit';


 
@Injectable({
  providedIn: 'root'
})
export class OrganizationUnitService extends GBBaseDataServiceWN<IOrganizationUnit> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IOrganizationUnit>) {
    super(dbDataService, 'OrganizationUnitId');
  }
}
