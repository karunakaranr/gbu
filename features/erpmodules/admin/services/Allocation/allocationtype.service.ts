import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { IAllocationType } from '../../models/IAllocationType';


 
@Injectable({
  providedIn: 'root'
})
export class AllocationTypeService extends GBBaseDataServiceWN<IAllocationType> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IAllocationType>) {
    super(dbDataService, 'AllocationTypeId');
  }
}
