import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { IPeriod } from './../../models/IPeriod';


 
@Injectable({
  providedIn: 'root'
})

export class PeriodService extends GBBaseDataServiceWN<IPeriod> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IPeriod>) {
    super(dbDataService, 'PeriodId');
  }
}
