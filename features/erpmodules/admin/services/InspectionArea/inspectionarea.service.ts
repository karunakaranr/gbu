import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { IInspectionArea } from '../../models/IInspectionArea';



 
@Injectable({
  providedIn: 'root'
})
export class InspectionAreaService extends GBBaseDataServiceWN<IInspectionArea> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IInspectionArea>) {
    super(dbDataService, 'InspectionAreaId');
  }
}
