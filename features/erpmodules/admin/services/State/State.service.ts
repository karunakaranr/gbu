import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';


import { IState } from '../../models/IState';


 
@Injectable({
  providedIn: 'root'
})
export class StateService extends GBBaseDataServiceWN<IState> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IState>) {
    super(dbDataService, 'StateId');
  }
}
