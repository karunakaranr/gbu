import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';

import { IWorkFlowRule } from '../../models/IWorkFlowRule ';




 
@Injectable({
  providedIn: 'root'
})
export class WorkFlowRuleService extends GBBaseDataServiceWN<IWorkFlowRule> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IWorkFlowRule>) {
    super(dbDataService, 'WorkFlowRuleId');
  }
}
