import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { ICodeDefine } from '../../models/ICodeDefine';


 
@Injectable({
  providedIn: 'root'
})
export class CodeDefineService extends GBBaseDataServiceWN<ICodeDefine> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<ICodeDefine>) {
    super(dbDataService, '');
  }
}
