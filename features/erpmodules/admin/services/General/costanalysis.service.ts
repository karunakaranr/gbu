import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { ICostAnalysis } from '../../models/ICostAnalysis';




 
@Injectable({
  providedIn: 'root'
})
export class CostAnalysisService extends GBBaseDataServiceWN<ICostAnalysis> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<ICostAnalysis>) {
    super(dbDataService, 'CostAnalysisId');
  }
}
