import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { IBankBranch } from '../../models/IBankBranch';




 
@Injectable({
  providedIn: 'root'
})
export class BankBranchService extends GBBaseDataServiceWN<IBankBranch> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IBankBranch>) {
    super(dbDataService, 'BankBranchId');
  }
}
