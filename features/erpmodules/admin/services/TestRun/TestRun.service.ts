import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';


import { ITestRun } from '../../models/ITestRun';


 
@Injectable({
  providedIn: 'root'
})
export class TestRunService extends GBBaseDataServiceWN<ITestRun> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<ITestRun>) {
    super(dbDataService, 'TestsetId');
  }
}
