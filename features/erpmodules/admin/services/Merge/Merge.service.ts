import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN,GBBaseDBDataService } from 'libs/gbdata/src';


import { IMerge } from '../../models/IMerge';


 
@Injectable({
  providedIn: 'root'
})
export class MergeService extends GBBaseDataServiceWN<IMerge> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IMerge>) {
    super(dbDataService, 'MergeId');
  }
}
