import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';


// import { ModelName } from ‘ModelPath’;
import { IRole } from '../../models/IRole';


 
@Injectable({
  providedIn: 'root'
})
export class RoleService extends GBBaseDataServiceWN<IRole> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IRole>) {
    super(dbDataService, 'RoleId');
  }
}
