import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { IAnalysis } from '../../models/IAnalysis';


 
@Injectable({
  providedIn: 'root'
})
export class AnalysisService extends GBBaseDataServiceWN<IAnalysis> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IAnalysis>) {
    super(dbDataService, 'AnalysisId');
  }
}
