import { Inject, Injectable } from '@angular/core';

import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';

import { IRegionType } from '../../models/IRegionType';



@Injectable({
  providedIn: 'root'
})
export class RegionTypeService extends GBBaseDataServiceWN<IRegionType> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IRegionType>) {
    super(dbDataService, 'RegionTypeId');
  }
}