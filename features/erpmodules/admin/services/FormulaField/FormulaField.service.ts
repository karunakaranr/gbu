import { Inject, Injectable } from '@angular/core';

import { GBBaseDataServiceWN,GBBaseDBDataService,GBBaseDBService } from 'libs/gbdata/src';

import { IFormulaField } from '../../models/IFormulaField';


@Injectable({
  providedIn: 'root'
})
export class FormulaFieldService extends GBBaseDataServiceWN<IFormulaField> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IFormulaField>) {
    super(dbDataService, 'FormulaFieldId');
  }
}
