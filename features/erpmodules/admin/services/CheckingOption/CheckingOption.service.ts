import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';

import { ICheckingOption } from '../../models/ICheckingOption';

 
@Injectable({
  providedIn: 'root'
})
export class CheckingOptionService extends GBBaseDataServiceWN<ICheckingOption> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<ICheckingOption>) {
    super(dbDataService, 'CheckingOptionId','CheckingOption');
  }
}
