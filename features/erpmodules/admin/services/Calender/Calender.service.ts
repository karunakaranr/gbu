import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';


import { ICalender } from '../../models/ICalender';


 
@Injectable({
  providedIn: 'root'
})
export class CalenderService extends GBBaseDataServiceWN<ICalender> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<ICalender>) {
    super(dbDataService, 'CalenderId');
  }
}
