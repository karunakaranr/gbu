import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { ICurrency } from '../../models/ICurrency';


 
@Injectable({
  providedIn: 'root'
})
export class CurrencyService extends GBBaseDataServiceWN<ICurrency> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<ICurrency>) {
    super(dbDataService, 'CurrencyId');
  }
}
