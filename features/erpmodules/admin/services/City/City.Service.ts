import { Inject, Injectable } from '@angular/core';

import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';

import { ICity } from '../../models/ICity';


@Injectable({
  providedIn: 'root'
})
export class CityService extends GBBaseDataServiceWN<ICity> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<ICity>) {
    super(dbDataService, 'CityId');
  }
}