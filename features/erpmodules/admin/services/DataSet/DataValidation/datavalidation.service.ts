import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { IDataValidation } from 'features/erpmodules/admin/models/IDataValidation';




 
@Injectable({
  providedIn: 'root'
})
export class DataValidationService extends GBBaseDataServiceWN<IDataValidation> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IDataValidation>) {
    super(dbDataService, 'DataValidationId');
  }
}
