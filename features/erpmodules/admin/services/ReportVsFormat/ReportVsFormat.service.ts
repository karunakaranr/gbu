import { Inject, Injectable } from '@angular/core';

import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { IReportVsFormats } from '../../models/IReportVsFormats';




@Injectable({
  providedIn: 'root'
})
export class ReportVsFormatService extends GBBaseDataServiceWN<IReportVsFormats> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IReportVsFormats>) {
    super(dbDataService, 'ReportId');
  }
}