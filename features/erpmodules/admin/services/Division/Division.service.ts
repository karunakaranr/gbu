import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';


import { IDivision } from '../../models/IDivision';

 
@Injectable({
  providedIn: 'root'
})
export class DivisionService extends GBBaseDataServiceWN<IDivision> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IDivision>) {
    super(dbDataService, 'DivisionId');
  }
}
