import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';


import { UicoreModule } from '@goodbooks/uicore';
import { UifwkUifwkmaterialModule } from '@goodbooks/uifwk/material';
import { NgxsModule } from '@ngxs/store';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


import { NgxsFormPluginModule } from '@ngxs/form-plugin';

import { TranslocoRootModule } from './../../transloco/transloco-root.module';
import { GbdataModule } from '@goodbooks/gbdata';
import { environment } from '../../environments/environment';
import { NgBreModule } from '@unisoft/ng-bre';

import { NgxChartsModule } from '@swimlane/ngx-charts';
import { GbchartModule } from '../../common/shared/screens/gbchart/gbchart.module';

import { EntitylookupModule } from './../../entitylookup/entitylookup.module';
import { geolocationModule } from './../../geolocation/geolocation.module'
import { NgSelectModule } from '@ng-select/ng-select';

import { AgGridModule } from 'ag-grid-angular';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule } from '@angular/material/button';
import { MatListModule } from '@angular/material/list'
import { MatIconModule } from '@angular/material/icon';
import { MatDividerModule } from '@angular/material/divider';
import { MatExpansionModule } from '@angular/material/expansion';

import { MatTabsModule } from '@angular/material/tabs'
import { MatCheckboxModule } from '@angular/material/checkbox';
import { AgmCoreModule } from '@agm/core';
import { AgmDirectionModule } from 'agm-direction';

import { GbgridModule } from 'features/gbgrid/components/gbgrid/gbgrid.module';
// import { GbfilterModule } from './../../gbfilter/gbfilter.module';
import { CommonReportModule } from './../../commonreport/commonreport.module';
import { AdminRoutingModule } from './admin.routing.module';
import { AdminComponent } from './admin.component';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MaterialModule } from 'features/common/material.module';

import { MatInputModule } from '@angular/material/input';
import { CheckSettingComponent } from './screens/CheckSetting.component'
import { SynchronizationComponent } from './screens/Synchronization/synchronization.component';
import { DBJoinComponent } from './screens/DBJoin/dbjoin.component';
import { CountryComponent } from './screens/Country/country.component';
import { TransactionTypeComponent } from './screens/BIZTransaction/TransactionType/TransactionType.component';
import { DepartmentComponent } from './screens/Organization/Department.component';
import { CompanyComponent } from './screens/Organization/Company/company.component';
import { CurrencyComponent } from './screens/Organization/Currency/currency.component';
import { BranchComponent } from './screens/Organization/Branch/branch.component';
import { BizTransactionComponent } from './screens/BIZTransaction/Biztransaction/biztransaction.component';
import { StateComponent } from './screens/State/state.component';
import { PeriodComponent } from './screens/Period/period.component';
import { FinanceBookComponent } from './screens/Organization/FinanceBook/financebook.component';
import { PortComponent } from './screens/Region/port.component';
import { CostCenterTypeComponent } from './screens/CostCenter/costcentertype.component';
import { AllocationTypeComponent } from './screens/Allocation/allocationtype.component';
import { JobDefineComponent } from './screens/Event/jobdefine.component';
import { PageComponent } from './screens/Role&Rights/page.component';
import { BankBranchComponent } from './screens/General/bankbranch.component';
import { CostAnalysisComponent } from './screens/General/CostAnalysis/costanalysis.component';
import { OrganizationGroupComponent } from './screens/Organization/OrganizationGroup/organizationgroup.component';
import { RoleModuleComponent } from './screens/Role&Rights/RoleModule/rolemodule.component';
import { UserGroupComponent } from './screens/Role&Rights/UserGroup/usergroup.component';
import { UserAccessRightComponent } from './screens/Role&Rights/UserAccessRights/useraccessrights.component';
import { PaymentTermComponent } from './screens/General/PaymentTerm/paymentterm.component';
import { CheckingComponent } from './screens/Checking/checking/checking.component';
import { DataValidationComponent } from './screens/DataSet/DataValidation/datavalidation.component';
import { PartyTaxTypeComponent } from './screens/PartyTaxType/PartyTaxType.component';
import { CopyNameTypeComponent } from './screens/CopyNameType/CopyNameType.component';
import { DivisionComponent } from './screens/Division/Division.component';
import { AnalysisComponent } from './screens/Analysis/analysis.component';
import { RoleComponent } from './screens/Role/Role.component';
import { InspectionAreaComponent } from './screens/InspectionArea/inspectionarea.component';
import { BIZTransactionSubClassComponent } from './screens/BIZTransaction/BizTransactionSubClass/biztransactionsubclass.component';
import { ChargeElementComponent } from './screens/Formula&Charge/ChargeElement/chargeelement.component';
import { RoleAlertComponent } from './screens/Role&Rights/RoleAlert/rolealert.component';
import { BIZTransactionKeyComponent } from './screens/BIZTransaction/BizTransactionKey/biztransactionkey.component';
import { CalenderComponent } from './screens/Calender/Calender.component';
 import { WorkFlowRuleComponent } from './screens/WorkFlowRule/workflowrule.component';
 import { EventTypeActionComponent } from './screens/EventTypeAction/eventtypeaction.component';
import { RegionTypeComponent } from './screens/RegionType/regiontype.component';
import { CityComponent } from './screens/City/city.component';
import { FormulaFieldComponent } from './screens/FormulaField/formulafield.component';
import { FileComponent } from './screens/File/file.component';
import { BizTransactionTypeComponent } from './screens/BIZTransaction/BizTransactionType/biztransactiontype.component';
import { CodeDefineComponent } from './screens/Organization/CodeDefine/codedefine.component';
import { VehicleComponent } from './screens/Vehicle/Vehicle.component';
import { SecurityQuestionComponent } from './screens/SecurityQuestion/SecurityQuestion.component';
import { AllocationComponent } from './screens/Alllocationn/allocation.component';
import { OrganizationUnitComponent } from './screens/OrganizationUnit/OrganizationUnit.component';
import { CheckingOptionComponent } from './screens/CheckingOption/CheckingOption.component';
import { TestRunComponent } from './screens/TestRun/TestRun.component';
import { RegionComponent } from './screens/Region/Region/Region.component';
import { ReportVsFormatsComponent } from './screens/ReportVsFormats/ReportVsFormats.component';
@NgModule({
  declarations: [
    WorkFlowRuleComponent,
    ReportVsFormatsComponent,
    RegionComponent,
    EventTypeActionComponent,
    OrganizationUnitComponent,
	 AllocationComponent,
    CalenderComponent,
    VehicleComponent,
    CodeDefineComponent,
    CheckingOptionComponent,
    TestRunComponent,
    BizTransactionTypeComponent,
    SecurityQuestionComponent,
    DivisionComponent,
    FormulaFieldComponent,
    CityComponent,
    BIZTransactionKeyComponent,
    RoleAlertComponent,
    ChargeElementComponent,
    InspectionAreaComponent,
    BIZTransactionSubClassComponent,
    AnalysisComponent,
    RoleComponent,
    TransactionTypeComponent,
    PartyTaxTypeComponent,

    CheckingComponent,
    DataValidationComponent,
    PaymentTermComponent,
    CopyNameTypeComponent,
    FileComponent,
    UserAccessRightComponent,
    UserGroupComponent,
    RoleModuleComponent,
    OrganizationGroupComponent,
    CostAnalysisComponent,

    BankBranchComponent,
    PageComponent,
    JobDefineComponent,
    AllocationTypeComponent,
    CostCenterTypeComponent,
    PortComponent,
    FinanceBookComponent,
    StateComponent,
    PeriodComponent,
    BizTransactionComponent,
    BranchComponent,
    CurrencyComponent,
    CompanyComponent,
    DepartmentComponent,
    DBJoinComponent,
    AdminComponent,
    CheckSettingComponent,
    CountryComponent,
    SynchronizationComponent,
    RegionTypeComponent
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    UicoreModule,
    NgBreModule,
    UifwkUifwkmaterialModule,
    TranslocoRootModule,
    // NgxsModule.forFeature([
    //  EmployeeState]),
    // NgxsFormPluginModule.forRoot(),
    AgGridModule,
    GbgridModule,
    GbdataModule.forRoot(environment),
    AgmCoreModule.forRoot({
      // please get your own API key here:
      // https://developers.google.com/maps/documentation/javascript/get-api-key?hl=en
      apiKey: 'AIzaSyDvFSDxJXXR1nrdRDIzvp6DRZB9v649My8',
      libraries: ['places', 'geometry', 'drawing']
    }),
    AgmDirectionModule,
    NgxChartsModule,
    GbchartModule,
    EntitylookupModule,
    geolocationModule,
    NgSelectModule,
    MaterialModule,
    MatSortModule,
    MatFormFieldModule,
    MatSelectModule,
    MatButtonModule,
    MatListModule,
    MatTabsModule,
    MatCheckboxModule,
    MatTableModule,
    MatIconModule,
    MatExpansionModule,
    MatDividerModule,
    MatPaginatorModule,
    CommonReportModule,
    MatFormFieldModule,
    MatInputModule,

  ],
  exports: [
    AdminComponent,
    BankBranchComponent,
    MatSortModule,
    MatFormFieldModule,
    MatInputModule
    
  ],
})
export class AdminModule {


}
