import { Injectable } from '@angular/core';
import {GBHttpService} from './../../../../libs/gbcommon/src/lib/services/HTTPService/GBHttp.service';
import { max } from 'rxjs/operators';
const urls = require('./../URLS/urls.json');
@Injectable({
  providedIn: 'root',
})
export class contactlistdetaildbservice{
  ContactURL:string =urls.ContactListDetail;
  ImageURL:string =urls.fileUpload;
  idField1:string = "?ContactId="; 
  idField2:string = "?ObjectId=";
 
  constructor(public http: GBHttpService) {
  }

  ContactListwithDetailURL(contactid: number){ 
    const url=this.ContactURL+ this.idField1+contactid;
    return this.http.httpget(url);
  }
  uploadImage(objectid:number){
    const url=this.ImageURL+ this.idField2+objectid;
    return this.http.httpget(url);
  }

}
