export interface Icontactlistinformation {
    PartyCode: string
    PartyName: string
    PartyBranchCode: string
    PartyBranchName: string
    DefaultImageId: string
    RelationShip: string
    ContactSexName: string
    ContactSex: number
    ContactId: number
    EntityId: number
    EntityCode: string
    EntityName: string
    ContactObjectId: number
    ContactContactNature: number
    ContactName: string
    ContactFirstName: string
    ContactMiddleName: string
    ContactLastName: string
    ContactSalutation: string
    ContactDob: string
    ContactMailId: string
    ContactPhoneNo: string
    ContactMobileNo: string
    ContactRelationShip: string
    ContactDesignation: string
    ContactCompany: string
    ContactDefaultImageId: string
    ContactLinkType: number
    ReportingToId: number
    ReportingToName: string
    AddressDTO: AddressDto
    ContactImageViewUrl: string
    ContactCreatedById: number
    ContactCreatedOn: string
    ContactModifiedById: number
    ContactModifiedOn: string
    ContactSortOrder: number
    ContactStatus: number
    ContactVersion: number
    ContactSourceType: number
    ContactAddon: ContactAddon[]
    ContactIsUser: number
    UserLinkId: number
    NumberOfRecords: number
  }
  
  export interface AddressDto {
    PartyCode: string
    PartyName: string
    PartyBranchCode: string
    PartyBranchName: string
    AddressId: number
    AddressObjectTypeId: number
    AddressObjectId: number
    AddressAddressType: number
    AddressAddressNature: number
    AddressLine1: string
    AddressLine2: string
    AddressLine3: string
    AddressLine4: string
    AddressLine5: string
    AddressZipCode: string
    CityId: number
    CityCode: string
    CityName: string
    CountryId: number
    CountryCode: string
    CountryName: string
    AddressPhone: string
    AddressMobile: string
    AddressFax: string
    AddressWeb: string
    AddressGeoCode: string
    AddressMail: string
    AddressVATTINNumber: string
    AddressVATCSTNumber: string
    AddressPAN: string
    AddressTAN: string
    AddressServiceTaxNumber: string
    AddressExciseDivision: string
    AddressExciseRange: string
    AddressExciseCodeNumber: string
    AddressExciseNotification: string
    AddressExciseCircle: string
    AddressExciseCollectrate: string
    AddressExciseRegisterNumber: string
    AddressTDSCircle: string
    StandardRegionId: number
    StandardRegionCode: string
    StandardRegionName: string
    LocationId: number
    LocationCode: string
    LocationName: string
    AddressGSTIn: string
    StateId: number
    StateCode: string
    StateName: string
    StateGSTStateCode: string
    StateTINStart: string
    UpdateShippingAddress: number
    TempPartyId: number
    TempPartyProductId: number
    AddressCreatedById: number
    AddressCreatedOn: string
    AddressModifiedById: number
    AddressModifiedOn: string
    AddressSortOrder: number
    AddressVersion: number
    AddressSourceType: number
    AddressStatus: number
    AddressGeolocation: any
    AddressBorder: any
  }
  
  export interface ContactAddon {
    Id: number
    FeAddon: string
    GetAddon: string
    Attributes: Attributes
  }

  export type ContactADDonFields = Root2[]

  export interface Root2 {
    AddOnFieldId: number
    EntityId: number
    EntityCode: string
    EntityName: string
    AddOnFieldSlNo: number
    AddOnFieldFieldName: string
    AddOnFieldDataType: number
    AddOnFieldDataSize: number
    AddOnFieldTypeOfInfo: number
    LovTypeId: number
    LovTypeCode: string
    LovTypeName: string
    CriteriaAttributeId: number
    CriteriaAttributeCode: any
    CriteriaAttributeName: string
    AddOnFieldRemarks: string
    AddOnFieldFieldDescription: string
    AddOnFieldCreatedById: number
    AddOnFieldCreatedOn: string
    AddOnFieldModifiedById: number
    AddOnFieldModifiedOn: string
    AddOnFieldSortOrder: number
    AddOnFieldStatus: number
    AddOnFieldVersion: number
    AddOnFieldSourceType: number
    AddOnFieldIsMandatory: number
    AddOnFieldIsMultiselect: number
    OperationType: number
  }
  
  export interface Attributes {}
  