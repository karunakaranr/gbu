export interface ILeadSource {
    LeadSourceId: string
    LeadSourceCode: string
    LeadSourceName: string
    LeadActualSourceTypeId: string
    LeadActualSourceTypeName: string
    LeadSourceRemarks: string
    LeadSourceVersion: number
    LeadSourceStatus: number
    LeadSourceDetailArray: LeadSourceDetailArray[]
    LeadSourceCreatedOn: string
    LeadSourceModifiedOn: string
    LeadSourceModifiedByName: string
    LeadSourceCreatedByName: string
    LeadSourceSourceType: number
  }
  
  export interface LeadSourceDetailArray {
    LeadSourceDetailId: string
    LeadSourceDetailSlNo: string
    LeadSourceId: string
    LeadSourceDetailDetail: string
    LeadSourceDetailRemarks: string
    LeadSourceDetailIsActive: number
  }
  