export interface Iaddresslist {
    NumberOfRecords: number
    ObjectTypeId: number
    ObjectId: number
    TypeAddress: number
    Code: string
    ObjectName: string
    Name: string
    IdAddress: number
    NatureAddress: number
    Line1: string
    Line2: string
    Line3: string
    Line4: string
    Line5: string
    ZipCode: string
    City: string
    Country: string
    Region: string
    Phone: string
    Mobile: string
    Fax: string
    Mail: string
    Web: string
    Vat: string
    Cst: string
    Pan: string
    Tan: string
    Division: string
    Range: string
    CodeNumber: string
    ExciseCircle: string
    CollectRate: string
    RegisterNumber: string
    ExciseNotification: string
  }
  