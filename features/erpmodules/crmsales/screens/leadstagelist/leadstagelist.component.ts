import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { ICriteriaDTODetail } from 'features/erpmodules/samplescreens/models/IFilterDetail';
import { ILeadStageListDetail } from '../../models/ILeadStage';
import { LeadStageListService } from '../../services/LeadStageList.service';

@Component({
  selector: 'app-leadstagelist',
  templateUrl: './leadstagelist.component.html',
  styleUrls: ['./leadstagelist.component.scss'],
})
export class LeadStageListComponent implements OnChanges {
  @Input() StageName: string;
  @Input() FilterData: ICriteriaDTODetail;
  @Input() selected: number;
  @Input() EmployeeId: number;
  @Input() MonthTypeselected: number;
  @Input() TotalRecord: number;
  firstnumber: number = 1;
  lastnumber: number = 50;
  TableSortType: string = 'ascending';
  TableModifiedHead: string = '';

  RowData: ILeadStageListDetail[];
  constructor(public listservice: LeadStageListService) { }
  ngOnChanges(changes: SimpleChanges): void {
    if (changes.StageName || changes.EmployeeId || changes.MonthTypeselected) {
      if (this.TotalRecord < 50) {
        this.lastnumber = this.TotalRecord
      } else {
        this.lastnumber = 50
      }
      this.firstnumber = 1

      this.listservice
        .LeadStageListview(
          this.FilterData,
          this.selected,
          this.StageName,
          this.EmployeeId,
          this.MonthTypeselected,
          this.firstnumber,
          50
        )
        .subscribe((menudetails) => {
          this.RowData = menudetails;
          console.log("this.RowData:", this.RowData)
        });
    }
  }

  onSortClick(sortitem: string) {
    if (this.TableModifiedHead !== sortitem) {
      this.TableSortType = 'ascending';
    }
    this.TableModifiedHead = sortitem;
    if (this.TableSortType === 'ascending') {
      this.RowData.sort((a, b) => (a[sortitem] > b[sortitem] ? 1 : -1));
      this.TableSortType = 'descending';
    } else {
      this.RowData.sort((a, b) => (a[sortitem] > b[sortitem] ? -1 : 1));
      this.TableSortType = 'ascending';
    }
  }

  nextset() {
    if (this.TotalRecord < 50 || this.lastnumber == this.TotalRecord) {
      return;
    }

    this.lastnumber += 50;
    if (this.TotalRecord < this.lastnumber) {
      this.lastnumber = this.TotalRecord

    }

    this.firstnumber += 50;
    if (this.firstnumber > this.lastnumber) {
      this.firstnumber -= 50
      this.lastnumber -= 50
      return;
    }
    this.listservice
      .LeadStageListview(
        this.FilterData,
        this.selected,
        this.StageName,
        this.EmployeeId,
        this.MonthTypeselected,
        this.firstnumber,
        50
      )
      .subscribe((menudetails) => {
        this.RowData = menudetails;
      });

  }

  previousset() {
    if (this.firstnumber == 1) {
      return;

    }
    if (this.lastnumber % 50 != 0) {
      this.lastnumber = Math.ceil(this.lastnumber / 50) * 50;
    }
    this.firstnumber -= 50;
    this.lastnumber -= 50;
    this.listservice
      .LeadStageListview(
        this.FilterData,
        this.selected,
        this.StageName,
        this.EmployeeId,
        this.MonthTypeselected,
        this.firstnumber,
        50
      )
      .subscribe((menudetails) => {
        this.RowData = menudetails;
      });

  }
  nextnextset() {

    if (this.lastnumber == this.TotalRecord) {
      return;
    }

    this.firstnumber = Math.floor(this.TotalRecord / 50) * 50 + 1;
    this.lastnumber = this.TotalRecord;
    this.listservice
      .LeadStageListview(
        this.FilterData,
        this.selected,
        this.StageName,
        this.EmployeeId,
        this.MonthTypeselected,
        this.firstnumber,
        50
      )
      .subscribe((menudetails) => {
        this.RowData = menudetails;
      });
  }
  prepreviousset() {
    if (this.firstnumber == 1 || this.TotalRecord < 50) {
      return;

    }
    this.firstnumber = 1;
    this.lastnumber = 50;
    this.listservice
      .LeadStageListview(
        this.FilterData,
        this.selected,
        this.StageName,
        this.EmployeeId,
        this.MonthTypeselected,
        this.firstnumber,
        50
      )
      .subscribe((menudetails) => {
        this.RowData = menudetails;
      });
  }
}
