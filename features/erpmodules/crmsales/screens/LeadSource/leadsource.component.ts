import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { LeadSourceDetailService } from '../../services/leadsource.service'; 
import { ILeadSource } from '../../models/ILeadSource';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as LeadSourceJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/LeadSource.json'
import { URLS } from '../../urls/Url';

@Component({
  selector: 'app-leadsource',
  templateUrl: './leadsource.component.html',
  styleUrls: ['./leadsource.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'LeadSourceId'},
    { provide: 'url', useValue: URLS.LeadSource },
    { provide: 'DataService', useClass: LeadSourceDetailService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class LeadSourceComponent extends GBBaseDataPageComponentWN<ILeadSource> {
  title : string = "LeadSource"
  LeadSourceJSON =LeadSourceJSON;
  form: GBDataFormGroupWN<ILeadSource> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "LeadSource", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if(arrayOfValues != null){
      this.leadsourceFillData(arrayOfValues.LeadSourceId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
   }

  public leadsourceFillData(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(FormVariable => {
          this.form.patchValue(FormVariable);
        })
    }
  }


  public leadsourcePatchids(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<ILeadSource> {
  const dbds: GBBaseDBDataService<ILeadSource> = new GBBaseDBDataService<ILeadSource>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<ILeadSource>, dbDataService: GBBaseDBDataService<ILeadSource>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<ILeadSource> {
  return new GBDataPageService<ILeadSource>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
