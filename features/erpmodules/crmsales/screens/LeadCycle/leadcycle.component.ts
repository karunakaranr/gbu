import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { LeadCycleDetailService } from '../../services/leadcycle.service';
import { ILeadCycle } from '../../models/ILeadCycle';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as LeadCycleJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/LeadCycle.json'
import { URLS } from '../../urls/Url';

@Component({
  selector: 'app-leadcycle',
  templateUrl: './leadcycle.component.html',
  styleUrls: ['./leadcycle.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'LeadCycleId'},
    { provide: 'url', useValue: URLS.LeadCycle },
    { provide: 'DataService', useClass: LeadCycleDetailService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class LeadCycleComponent extends GBBaseDataPageComponentWN<ILeadCycle> {
  title : string = "LeadCycle"
  LeadCycleJSON =LeadCycleJSON;
  form: GBDataFormGroupWN<ILeadCycle> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "LeadCycle", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if(arrayOfValues != null){
      this.LeadCycleFillData(arrayOfValues.LeadCycleId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
   }

  public LeadCycleFillData(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(FormVariable => {
          this.form.patchValue(FormVariable);
        })
    }
  }


  public LeadCyclePatchids(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<ILeadCycle> {
  const dbds: GBBaseDBDataService<ILeadCycle> = new GBBaseDBDataService<ILeadCycle>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<ILeadCycle>, dbDataService: GBBaseDBDataService<ILeadCycle>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<ILeadCycle> {
  return new GBDataPageService<ILeadCycle>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
