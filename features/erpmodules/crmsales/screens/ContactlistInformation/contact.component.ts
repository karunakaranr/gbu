import { Component, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Select, Store } from '@ngxs/store';
import { ButtonVisibility, FormUnEditable } from 'features/layout/store/layout.actions';
import { URLS } from '../../urls/Url';
import { ContactlistDetailService } from '../../services/contactlistdetail.service';
import {  Icontactlistinformation } from '../../models/Icontactlistinformation';
import * as ContactJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/ContactDetail.json'
import { Observable } from 'rxjs';
import { LayoutState } from 'features/layout/store/layout.state';

@Component({
  selector: 'app-ContactlistInformation', 
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'ContactId' },
    { provide: 'url', useValue: URLS.ContactListDetail},
    { provide: 'saveurl', useValue: URLS.ContactSave},
    { provide: 'DataService', useClass: ContactlistDetailService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url', 'saveurl'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class ContactComponent extends GBBaseDataPageComponentWN<Icontactlistinformation> {
  @ViewChild('fileInput') fileInput: ElementRef;
  @ViewChild("myinput") myInputField: ElementRef;

  @Select(LayoutState.FormEdit) edit$: Observable<any>;
  isFocused: boolean = false;

  isreadables:boolean;
  flag = false; 
  editMode = false; // Set edit mode initially to false
  screenHeight: number;
  fileUpload: string = ''; 
  title : string = "ContactDetail"
  ContactJSON =ContactJSON;
  form: GBDataFormGroupWN<Icontactlistinformation> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "ContactDetail", {}, this.gbps.dataService);
 
  ngOnInit() {
    this.myInputField.nativeElement.focus();
  }
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('ContactId'))
    console.log("HHDD",arrayOfValues)
    if(arrayOfValues != null){
      this.ContactFillFunction(arrayOfValues)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
      this.editMode = true; // Set edit mode to true if there are values
      this.store.dispatch(new FormUnEditable);

    // Initialize flag with a default value
    this.flag = false;

    // Subscribe to the 'edit$' observable
    this.edit$.subscribe(res => {
      this.isreadables = res;
      console.log("Flag:", this.flag);

      // Update 'flag' when 'isreadables' is true
      if (this.isreadables === true) {
        this.flag = true;
        this.editMode = true;
      } else {
        this.editMode = false;
      }
      console.log("this.isreadable:",this.isreadables)
    })
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
    
   }

  public ContactFillFunction(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(FormVariable => {
          this.form.patchValue(FormVariable);
        })
    }
  }


  public ContactPatchFunction(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
  openFileInput() {
    // Programmatically trigger the click event of the hidden file input
    this.fileInput.nativeElement.click();

    console.log("open",this.fileInput.nativeElement)
  }
  onFileSelected(event: any) {
    const selectedFile = event.target.files[0];
    console.log('file:', selectedFile);

    if (selectedFile) {
     
      console.log('Selected file:', selectedFile);
      this.uploadImage(selectedFile);
    }
  }
  uploadImage(file: File): void {
    const formData = new FormData();
    const objectId = Math.floor(Math.random() * 1499999999) + 1;
    const objectTypeId = '-2147481759';
    const objectIdParam = objectId;
    
    formData.append('filename', file.name);
    formData.append('filetype', file.type);
    formData.append('file', file);
    
    this.http.httppost(URLS.fileUpload + objectIdParam + '&ObjectTypeId=' + objectTypeId, formData).subscribe(
      (response: any) => {
        console.log('Upload successful:', response);
        this.fileUpload = response.imageUrl;
        console.log("this.fileUpload:", this.fileUpload);
        this.http.httpget("/fws/Entity.svc/FileDownload/" + response).subscribe(result => {
          console.log("Result:", result);
          this.form.get('ContactImageViewUrl')?.patchValue(result.ViewUrl);
          this.form.get('ContactDefaultImageId')?.patchValue(result.ViewUrl);
          this.form.get('DefaultImageId')?.patchValue(result.Id);
        });
      },
      (error) => {
        console.log('Error uploading file:', error);
        if (error.status === 0) {
          console.log('Network error: Unable to reach the server.');
        } else {
          console.log('Server error:', error.error);
        }
      }
    );
  }
  
  
  setStyle(name: string, value: string): void { //height
    document.documentElement.style.setProperty(name, value); 
  }
}

export function getThisDBDataService(http: GBHttpService, url: string, saveurl: string): GBBaseDBDataService<Icontactlistinformation> {
  const dbds: GBBaseDBDataService<Icontactlistinformation> = new GBBaseDBDataService<Icontactlistinformation>(http);
  dbds.endPoint = url;
  dbds.saveurl = saveurl
  return dbds;
}

export function getThisPageService(store: Store, dataService: GBBaseDataService<Icontactlistinformation>, dbDataService: GBBaseDBDataService<Icontactlistinformation>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<Icontactlistinformation> {
  return new GBDataPageService<Icontactlistinformation>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
