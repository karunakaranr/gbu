import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';

import { Iaddress } from './../models/Iaddress';

 
@Injectable({
  providedIn: 'root'
})
export class addressService extends GBBaseDataServiceWN<Iaddress> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<Iaddress>) {
    super(dbDataService, 'AddressId');
  }
}