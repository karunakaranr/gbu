import { Injectable } from '@angular/core';
import { LeadStageListDbService } from '../dbservices/LeadStageListDB.service';
import { ICriteriaDTODetail } from 'features/erpmodules/samplescreens/models/IFilterDetail';
@Injectable({
  providedIn: 'root',
})
export class LeadStageListService {
  constructor(public dbservice: LeadStageListDbService) { }

  public LeadStageListview(
    FilterData: ICriteriaDTODetail,
    selected: number,
    StageName: string,
    EmployeeId: number,
    MonthTypeselected: number,
    firstnumber: number,
    lastnumber: number
  ) {
    return this.dbservice.LeadStageListService(
      FilterData,
      selected,
      StageName,
      EmployeeId,
      MonthTypeselected,
      firstnumber,
      lastnumber
    );
  }
}
