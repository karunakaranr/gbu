import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';

import { ILeadCycle } from '../models/ILeadCycle';
 
@Injectable({
  providedIn: 'root'
})
export class LeadCycleDetailService extends GBBaseDataServiceWN<ILeadCycle> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<ILeadCycle>) {
    super(dbDataService, 'LeadCycleId');
  }
}


