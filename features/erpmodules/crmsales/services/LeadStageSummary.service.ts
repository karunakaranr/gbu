import { Injectable } from '@angular/core';
import { LeadStageSummaryDbService } from '../dbservices/LeadStageSummaryDB.service';
import { ICriteriaDTODetail } from 'features/erpmodules/samplescreens/models/IFilterDetail';
@Injectable({
  providedIn: 'root',
})
export class LeadStageSummaryService {
  constructor(public dbservice: LeadStageSummaryDbService) {}

  public LeadStageSummaryview(
    FilterData: ICriteriaDTODetail,
    selected: number
  ) {
    return this.dbservice.LeadStageSummaryService(FilterData, selected);
  }
}
