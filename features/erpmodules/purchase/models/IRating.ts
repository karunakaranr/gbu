export interface IRating {
    RatingId: number
    RatingType: number
    RatingSortOrder: number
    RatingStatus: number
    RatingVersion: number
    RatingCreatedById: number
    RatingCreatedOn: string
    RatingModifiedById: number
    RatingModifiedOn: string
    RatingDetailArray: RatingDetailArray[]
  }
  
  export interface RatingDetailArray {
    RatingDetailId: number
    RatingId: number
    RatingDetailSlNo: number
    RatingDetailSlabFrom: number
    RatingDetailSlabTo: number
    RatingDetailPoint: number
    RatingDetailGrade: string
  }
  