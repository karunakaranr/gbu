import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Select, Store } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { IPendingRWOItem } from 'features/erpmodules/purchase/models/IPendingRWOItem';
import { SharedService } from 'features/commonreport/service/datapassing.service';
import { DateFormaterPipe } from 'libs/gbcommon/src/lib/Date-Formater.pipe';



@Component({
  selector: 'app-pendingregisterwoitem',
  templateUrl: './pendingregisterwoitem.component.html',
  styleUrls: ['./pendingregisterwoitem.component.scss'],
})
export class PendingRegisterWOItemComponent implements OnInit {
  @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
  private dateFormatter: DateFormaterPipe = new DateFormaterPipe(this.store);

  public rowData: IPendingRWOItem[];
  public tabledata;

  public valuefc:number;
  public value:number;
  
  constructor(public store: Store, public SharedService: SharedService) { }
  ngOnInit(): void {
    this.getpendingregisterwoitem();
  }
  
  public getpendingregisterwoitem() {
   this.SharedService.getRowInfo().subscribe(data => {
            if (data.length > 0) {
      this.rowData = data;
      console.log("WO Item :", this.rowData)

      this.valuefc = 0;
      this.value = 0;
    
    

      let json = this.rowData;
      var finalizedArray = [];

      for (let data of this.rowData) {

        this.valuefc = this.valuefc + data.TotalNetValueFC;
        this.value = this.value + data.TotalNetValue;
      }

      json.map((row,index) => {
          finalizedArray.push({
              sno: index,
			  docdate:row['DocumentDate'],
			  btypename:row['BIZTransactionTypeName'],

              docno:row['DocumentNumber'],
              refno: row['ReferenceNumber'],
              refdate:this.dateFormatter.transform(row['ReferenceDate'], 'date'),
              partyname: row['PartyName'],
              deliverydate: row['RequiredDeliveryDate'],
              totalvalfc: row['TotalNetValueFC'],
              totalval: row['TotalNetValue'],
              allocatname: row['AllocationName'],
              inchargename: row['InchargeName']
             
          });
      });


      const final = {};
      finalizedArray.forEach((detail) => {
          final[detail.btypename] = {
              accountGroups: {},
              docno: detail.btypename,
              refno: "",
              refdate: "",
              partyname: "",
              deliverydate: "",
              totalvalfc: "",
              totalval: "",
              allocatname:"",
			  inchargename:"",
              ...final[detail.btypename],
          };
          final[detail.btypename].accountGroups[detail.docdate] = {
            accountGroup2: {},
				docno: detail.docdate,
				refno: "",
				refdate: "",
				partyname: "",
				deliverydate: "",
				totalvalfc: "",
				totalval: "",
                allocatname:"",
				inchargename:"",
            ...final[detail.btypename].accountGroups[detail.docdate],
        }

        final[detail.btypename].accountGroups[detail.docdate].accountGroup2[detail.sno] = {
           
            docno: detail.docno,
            refno: detail.refno,
            refdate: detail.refdate,
            partyname: detail.partyname,
            deliverydate: detail.deliverydate,
            totalvalfc: detail.totalvalfc,
            totalval:detail.totalval,
            allocatname:detail.allocatname,
			inchargename:detail.inchargename,
          };
      });

      const woitem = Object.keys(final);

      const tableData = [];
      woitem.forEach((stopdetail) => {
          tableData.push({
            docno: final[stopdetail].docno,
              refno: "",
              refdate: "",
              partyname: "",
              deliverydate: "",
              totalvalfc: "",
              totalval: "",
              allocatname:"",
			  inchargename:"",
              bold: true,
          });

          const accountGroups = Object.keys(final[stopdetail].accountGroups);
          accountGroups.forEach(stdetail => {
                    
                    
                    tableData.push({
                        docno: final[stopdetail].accountGroups[stdetail].docno,
                        refno: "",
                        refdate: "",
                        partyname: "",
                        deliverydate: "",
                        totalvalfc: "",
                        totalval: "",
                        allocatname:"",
						inchargename:"",
                        bold: true,
                    })

          const accountGroup2 = Object.keys(final[stopdetail].accountGroups[stdetail].accountGroup2);
          accountGroup2.forEach((sdetail) => {
              tableData.push({
                docno: final[stopdetail].accountGroups[stdetail].accountGroup2[sdetail].docno,
                refno: final[stopdetail].accountGroups[stdetail].accountGroup2[sdetail].refno,
                refdate: final[stopdetail].accountGroups[stdetail].accountGroup2[sdetail].refdate,
                partyname: final[stopdetail].accountGroups[stdetail].accountGroup2[sdetail].partyname,
                deliverydate: final[stopdetail].accountGroups[stdetail].accountGroup2[sdetail].deliverydate,
                totalvalfc: final[stopdetail].accountGroups[stdetail].accountGroup2[sdetail].totalvalfc,
                totalval: final[stopdetail].accountGroups[stdetail].accountGroup2[sdetail].totalval,
                allocatname:final[stopdetail].accountGroups[stdetail].accountGroup2[sdetail].allocatname,
		      inchargename:final[stopdetail].accountGroups[stdetail].accountGroup2[sdetail].inchargename,
             

              })
          })
        })
      });
  
      this.tabledata = tableData;

      console.log("Final Data:", this.rowData)
            }  
    });
  }
}