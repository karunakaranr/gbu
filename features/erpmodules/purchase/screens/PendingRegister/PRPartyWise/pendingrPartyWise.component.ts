import { Component, OnInit } from '@angular/core';
import { IPendingRPartyWise } from 'features/erpmodules/purchase/models/IPendingRPartyWise';
import { SharedService } from 'features/commonreport/service/datapassing.service';

@Component({
  selector: 'app-pendingrpartywise',
  templateUrl: './pendingrpartywise.component.html',
  styleUrls: ['./pendingrpartywise.component.scss'],
})
export class PendingRegisterPartyWiseComponent implements OnInit {
  public rowData: IPendingRPartyWise[];
  constructor(public SharedService: SharedService) { }
  ngOnInit(): void {
    this.SharedService.getRowInfo().subscribe(data => {
        this.rowData = data;
    });
  }
  public subtotal(rowdata : IPendingRPartyWise[], index : number, fieldgroup : string, fieldvalue : string): number {
    let subtotal : number = 0;
    for (let i = index; i < rowdata.length; i++) {
      if (i == rowdata.length-1 || rowdata[i][fieldgroup] == rowdata[i + 1][fieldgroup]) {
        subtotal += rowdata[i][fieldvalue];
      } else {
        subtotal += rowdata[i][fieldvalue];
        break;
      }
    }
    return subtotal;
  }
}




// import { Component, OnInit } from '@angular/core';
// import { Observable } from 'rxjs';
// import { Select, Store } from '@ngxs/store';
// import { ReportState } from 'features/commonreport/datastore/commonreport.state';
// import { IPendingRPartyWise } from 'features/erpmodules/purchase/models/IPendingRPartyWise';
// import { SharedService } from 'features/commonreport/service/datapassing.service';
// import { DateFormaterPipe } from 'libs/gbcommon/src/lib/Date-Formater.pipe';


// @Component({
//   selector: 'app-pendingrpartywise',
//   templateUrl: './pendingrpartywise.component.html',
//   styleUrls: ['./pendingrpartywise.component.scss'],
// })
// export class PendingRegisterPartyWiseComponent implements OnInit {
//   @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
//   private dateFormatter: DateFormaterPipe = new DateFormaterPipe(this.store);

//   public rowData: IPendingRPartyWise[];
//   public tabledata;
//   public qty: number;
//   public penqty: number;
//   public cost: number;
//   public value: number;
 

//   constructor(public store: Store, public SharedService: SharedService) { }
//   ngOnInit(): void {
//     this.getpendingregisterpartywise();
//   }

//   public getpendingregisterpartywise() {
//     this.SharedService.getRowInfo().subscribe(data => {
//       if (data.length > 0) {
//       this.rowData = data;
//       console.log("Partywise :", this.rowData)

//       this.qty = 0;
//       this.penqty = 0;
//       this.cost = 0;
//       this.value = 0;

//       let json = this.rowData;
//       var finalizedArray = [];


//       for (let data of this.rowData) {

//         this.qty = this.qty + data.TransactionActualQuantity;
//         this.penqty = this.penqty + data.PendingQuantity;
//         this.cost = this.cost + data.ItemNetCost;
//         this.value = this.value + data.PendingValues;
//       }


//       json.map((row, index) => {
//         finalizedArray.push({
//           sno: index,
//           acccode: row['AccountCode'],
//           accname: row['AccountName'],
//           docno: row['DocumentNumber'],
//           refdate:this.dateFormatter.transform(row['ReferenceDate'], 'date'), 
//           ReferenceNumber: row['ReferenceNumber'],
//           icode: row['ItemCode'],
//           iname: row['ItemName'],
//           tactqty: row['TransactionActualQuantity'],
//           schqty: row['ScheduleQuantity'],
//           schdate: row['ScheduleDate'],
//           pendqty: row['PendingQuantity'],
//           uomcode: row['UOMCode'],
//           ratefc: row['RateFC'],
//           inetcost: row['ItemNetCost'],
//           pendval: row['PendingValues'],
//         });
//       });

//       console.log("finalizedArray:", finalizedArray)
//       const final = {};
//       finalizedArray.forEach((detail) => {
//         final[detail.acccode] = {
//           accountGroups: {},
//           icode: detail.acccode,
//           iname: detail.accname,
//           tactqty: 0,
//           schqty: "",
//           schdate: "",
//           pendqty: 0,
//           uomcode: "",
//           ratefc: 0,
//           inetcost: 0,
//           pendval: 0,
//           ...final[detail.acccode],
//         };
//         final[detail.acccode].accountGroups[detail.docno] = {
//           accountGroup2: {},
//           icode: detail.docno,
//           iname:detail.refdate + "  " + detail.ReferenceNumber,
//           tactqty: "",
//           schqty: "",
//           schdate:"",
//           pendqty: "",
//           uomcode: "",
//           ratefc: "",
//           inetcost: "",
//           pendval: "",
//           ...final[detail.acccode].accountGroups[detail.docno],
//         }

//         final[detail.acccode].accountGroups[detail.docno].accountGroup2[detail.sno] = {
//           icode: detail.icode,
//           iname: detail.iname,
//           tactqty: detail.tactqty,
//           schqty: detail.schqty,
//           schdate:detail.schdate,
//           pendqty: detail.pendqty,
//           uomcode: detail.uomcode,
//           ratefc: detail.ratefc,
//           inetcost: detail.inetcost,
//           pendval: detail.pendval,
//         };
//         // console.log("currency code:", final[detail.acccode].accountGroups[detail.docno].actualqty)
//       });
//       console.log("Final:",final)


//       const registerdetails = Object.keys(final);

//       const tableData = [];
//       registerdetails.forEach((registerdetail) => {

//         const accountGroup = Object.keys(final[registerdetail].accountGroups);
//         let qty1 = 0;
//         let pqty1 = 0;
//         let rate1 = 0;
//         let cost1 = 0;
//         let value1 = 0;



//         accountGroup.forEach((rgdetail) => {
//           const accountGroup22 = Object.keys(final[registerdetail].accountGroups[rgdetail].accountGroup2)
//           accountGroup22.forEach((sdetail) => {
//             qty1 = qty1 + parseFloat(final[registerdetail].accountGroups[rgdetail].accountGroup2[sdetail].tactqty);
//             pqty1 = pqty1 + parseFloat(final[registerdetail].accountGroups[rgdetail].accountGroup2[sdetail].pendqty);
//             rate1 = rate1 + parseFloat(final[registerdetail].accountGroups[rgdetail].accountGroup2[sdetail].ratefc);
//             cost1 = cost1 + parseFloat(final[registerdetail].accountGroups[rgdetail].accountGroup2[sdetail].inetcost);
//             value1 = value1 + parseFloat(final[registerdetail].accountGroups[rgdetail].accountGroup2[sdetail].pendval);
//           })
//         })
//           final[registerdetail].tactqty = qty1;
//           final[registerdetail].pendqty = pqty1;
//           final[registerdetail].ratefc = rate1;
//           final[registerdetail].inetcost = cost1;
//           final[registerdetail].pendval = value1;


//           tableData.push({

//             icode: final[registerdetail].icode,
//             iname: final[registerdetail].iname,
//             tactqty: final[registerdetail].tactqty,
//             schqty: "",
//             schdate:"",
//             pendqty: final[registerdetail].pendqty,
//             uomcode: "",
//             ratefc: final[registerdetail].ratefc,
//             inetcost: final[registerdetail].inetcost,
//             pendval:final[registerdetail].pendval,
//             bold: true,
//           });

//           accountGroup.forEach((rgdetail) => {
       
          
//           tableData.push({
//             icode: final[registerdetail].accountGroups[rgdetail].icode,
//             iname: final[registerdetail].accountGroups[rgdetail].iname,
//             tactqty:"",
//             schqty:"" ,
//             schdate:"",
//             pendqty:"",
//             uomcode:"",
//             ratefc: "",
//             inetcost:"",
//             pendval:"" ,
//             bold: true,
//           });


//          const accountGroup22 = Object.keys(final[registerdetail].accountGroups[rgdetail].accountGroup2)
//          accountGroup22.forEach(sdetail => {


//             tableData.push({
//               icode: final[registerdetail].accountGroups[rgdetail].accountGroup2[sdetail].icode,
//               iname: final[registerdetail].accountGroups[rgdetail].accountGroup2[sdetail].iname,
//               tactqty: final[registerdetail].accountGroups[rgdetail].accountGroup2[sdetail].tactqty,
//               schqty: final[registerdetail].accountGroups[rgdetail].accountGroup2[sdetail].schqty,
//               schdate:final[registerdetail].accountGroups[rgdetail].accountGroup2[sdetail].schdate,
//               pendqty: final[registerdetail].accountGroups[rgdetail].accountGroup2[sdetail].pendqty,
//               uomcode: final[registerdetail].accountGroups[rgdetail].accountGroup2[sdetail].uomcode,
//               ratefc: final[registerdetail].accountGroups[rgdetail].accountGroup2[sdetail].ratefc,
//               inetcost: final[registerdetail].accountGroups[rgdetail].accountGroup2[sdetail].inetcost,
//               pendval: final[registerdetail].accountGroups[rgdetail].accountGroup2[sdetail].pendval,
//             })


//           })
//         })
//         })
//         this.tabledata = tableData;

//         console.log("Final Data:", this.tabledata)
//       }
//       });

  
//   }
// }