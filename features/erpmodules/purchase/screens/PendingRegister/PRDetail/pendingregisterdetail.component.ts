import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Select, Store } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { IPendingRDetail } from 'features/erpmodules/purchase/models/IPendingRDetail';
import { SharedService } from 'features/commonreport/service/datapassing.service';
import { DateFormaterPipe } from 'libs/gbcommon/src/lib/Date-Formater.pipe';



@Component({
  selector: 'app-pendingregisterdetail',
  templateUrl: './pendingregisterdetail.component.html',
  styleUrls: ['./pendingregisterdetail.component.scss'],
})
export class PendingRegisterDetailComponent implements OnInit {
  @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
  private dateFormatter: DateFormaterPipe = new DateFormaterPipe(this.store);

  public rowData: IPendingRDetail[];
  public tabledata;
  
  public pquality: number;
  public value: number;

  constructor(public store: Store , public SharedService: SharedService) { }
  ngOnInit(): void {
    this.getpendingregisterdetail();
  }
  
  public getpendingregisterdetail() {
    this.SharedService.getRowInfo().subscribe(data => {
      if (data.length > 0) {

      this.rowData = data;
      console.log("Pending Register Detail :", this.rowData)

      this.pquality = 0;
      this.value = 0;
    

      let json = this.rowData;
      var finalizedArray = [];

      for (let data of this.rowData) {

        this.pquality = this.pquality + data.TransactionActualQuantity;
        this.value = this.value + data.PendingGoodValue;
      }

      json.map((row,index) => {
          finalizedArray.push({
              sno: index,
              docno:row['DocumentNumber'],
              inchargename: row['InchargeName'],
              createname: row['CreatedByName'],
              modifyname: row['ModifiedByName'],
              docdate: row['DocumentDate'],
              refno: row['ReferenceNumber'],
              refdate: this.dateFormatter.transform(row['PartyReferenceDate'], 'date'),
             
              pdocno:row['PreDocumentNumber'],
              icode: row['ItemCode'],
              pdes: row['ProductDescription'],
              tactqty: row['TransactionActualQuantity'],
              pendqty: row['PendingQuantity'],
              uomcode: row['UOMCode'],
              rate: row['Rate'],
              inetcose: row['ItemNetCost'],
              pgoodval: row['PendingGoodValue']
             
          });
      });


      const final = {};
      finalizedArray.forEach((detail) => {
          final[detail.docdate] = {
              accountGroups: {},
              pdocno: detail.docdate,
              icode: detail.refno,
              pdes: detail.refdate,
              tactqty:detail.tactqty,
              pendqty: detail.pendqty,
              uomcode: "",
              rate: detail.rate,
              inetcose:detail.inetcose,
              pgoodval:detail.pgoodval,
              
              ...final[detail.docdate],
          };
          final[detail.docdate].accountGroups[detail.docno] = {
            accountGroup2: {},
              pdocno: detail.docno,
              icode: detail.inchargename,
              pdes: detail.createname,
              tactqty:detail.modifyname,
              pendqty: "",
              uomcode: "",
              rate: "",
              inetcose:"",
              pgoodval:"",
            ...final[detail.docdate].accountGroups[detail.docno],
        }

        final[detail.docdate].accountGroups[detail.docno].accountGroup2[detail.sno] = {
           
              pdocno: detail.pdocno,
              icode: detail.icode,
              pdes: detail.pdes,
              tactqty:detail.tactqty,
              pendqty: detail.pendqty,
              uomcode: detail.uomcode,
              rate: detail.rate,
              inetcose:detail.inetcose,
              pgoodval:detail.pgoodval,
          };
      });

      const detail2 = Object.keys(final);

      const tableData = [];
      detail2.forEach((stopdetail) => {
          tableData.push({
           
            pdocno: final[stopdetail].pdocno,
            icode: final[stopdetail].icode,
            pdes: final[stopdetail].pdes,
            tactqty:final[stopdetail].tactqty,
            pendqty: final[stopdetail].pendqty,
            uomcode: final[stopdetail].uomcode,
            rate: final[stopdetail].rate,
            inetcose:final[stopdetail].inetcose,
            pgoodval:final[stopdetail].pgoodval,
              bold: true,
          });

          const accountGroups = Object.keys(final[stopdetail].accountGroups);
          accountGroups.forEach(stdetail => {
                    
                    
                    tableData.push({
                        
                        pdocno: final[stopdetail].accountGroups[stdetail].pdocno,
                        icode: final[stopdetail].accountGroups[stdetail].icode,
                        pdes: final[stopdetail].accountGroups[stdetail].pdes,
                        tactqty:final[stopdetail].accountGroups[stdetail].tactqty,
                        pendqty:"",
                        uomcode: "",
                        rate: "",
                        inetcose:"",
                        pgoodval:"",
                        bold: true,
                    })

          const accountGroup2 = Object.keys(final[stopdetail].accountGroups[stdetail].accountGroup2);
          accountGroup2.forEach((sdetail) => {
              tableData.push({
            
                        pdocno: final[stopdetail].accountGroups[stdetail].accountGroup2[sdetail].pdocno,
                        icode: final[stopdetail].accountGroups[stdetail].accountGroup2[sdetail].icode,
                        pdes: final[stopdetail].accountGroups[stdetail].accountGroup2[sdetail].pdes,
                        tactqty:final[stopdetail].accountGroups[stdetail].accountGroup2[sdetail].tactqty,
                        pendqty: final[stopdetail].accountGroups[stdetail].accountGroup2[sdetail].pendqty,
                        uomcode: final[stopdetail].accountGroups[stdetail].accountGroup2[sdetail].uomcode,
                        rate: final[stopdetail].accountGroups[stdetail].accountGroup2[sdetail].rate,
                        inetcose:final[stopdetail].accountGroups[stdetail].accountGroup2[sdetail].inetcose,
                        pgoodval:final[stopdetail].accountGroups[stdetail].accountGroup2[sdetail].pgoodval,

              })
          })
        })
      });
  
      this.tabledata = tableData;

      console.log("Final Data:", this.rowData)
    }
    });
  }
}