import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Select, Store } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { IRDetailone } from 'features/erpmodules/purchase/models/IRDetailone';
import { SharedService } from 'features/commonreport/service/datapassing.service';
import { DateFormaterPipe } from 'libs/gbcommon/src/lib/Date-Formater.pipe';

@Component({
  selector: 'app-registerdetailone',
  templateUrl: './registerdetailone.component.html',
  styleUrls: ['./registerdetailone.component.scss'],
})
export class RegisterDetailoneComponent implements OnInit {
  @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
  private dateFormatter: DateFormaterPipe = new DateFormaterPipe(this.store);

  public rowData: IRDetailone[];
  public tabledata;
  public quality: number;
  public total: number;

  constructor(public store: Store, public SharedService: SharedService) { }
  ngOnInit(): void {
    this.getregisterdetailonefun();
  }

  public getregisterdetailonefun() {
    this.SharedService.getRowInfo().subscribe(data => {
      if (data.length > 0) {
      this.rowData = data;
      console.log("Register Detail :", this.rowData)

      this.quality = 0;
      this.total = 0;

      let json = this.rowData;
      var finalizedArray = [];

      for (let data of this.rowData) {

        this.quality = this.quality + data.ActualQuantity;
        this.total = this.total + data.TotalValue;
      }

      json.map((row, index) => {
        finalizedArray.push({
          sno: index,
          docdate: row['DocumentDate'],
          docno: row['DocumentNumber'],
          refno: row['ReferenceNumber'],
          refdate:this.dateFormatter.transform(row['ReferenceDate'], 'date'), 
          depname: row['DepartmentName'],
          currencycode: row['TransactionCurrencyCode'],
          netval: row['NetValue'],
          predocno: row['PreDocumentNumber'],
          itemcode: row['ItemCode'],
          itemname: row['ItemName'],
          actualqty: row['ActualQuantity'],
          transuom: row['TransactionUOM'],
          transrate: row['TransactionRate'],
          totalval: row['TotalValue'],
        });
      });

      console.log("finalizedArray:", finalizedArray)
      const final = {};
      finalizedArray.forEach((detail) => {
        final[detail.docdate] = {
          accountGroups: {},
          predocno: detail.docdate,
          itemcode: "",
          itemname: "",
          actualqty: "0",
          transuom: "",
          transrate: "",
          totalval: "0",
          ...final[detail.docdate],
        };
        final[detail.docdate].accountGroups[detail.docno] = {
          accountGroup2: {},
          predocno: detail.docno,
          itemcode: detail.refno,
          itemname: detail.refdate + "  " + detail.depname,
          actualqty: detail.currencycode,
          transuom: "",
          transrate: "",
          totalval: detail.total,
          ...final[detail.docdate].accountGroups[detail.docno],
        }

        final[detail.docdate].accountGroups[detail.docno].accountGroup2[detail.sno] = {

          predocno: detail.predocno,
          itemcode: detail.itemcode,
          itemname: detail.itemname,
          actualqty: detail.actualqty,
          transuom: detail.transuom,
          transrate: detail.transrate,
          totalval: detail.totalval,
        };
        console.log("currency code:", final[detail.docdate].accountGroups[detail.docno].actualqty)
      });


      const registerdetails = Object.keys(final);

      const tableData = [];
      registerdetails.forEach((registerdetail) => {

        const accountGroup = Object.keys(final[registerdetail].accountGroups);
        let quality = 0;
        let total = 0;

        accountGroup.forEach((rgdetail) => {


          let qualityr = 0;
          let totalr = 0;


          const accountGroup22 = Object.keys(final[registerdetail].accountGroups[rgdetail].accountGroup2)
          accountGroup22.forEach((sdetail) => {
            qualityr = qualityr + parseFloat(final[registerdetail].accountGroups[rgdetail].accountGroup2[sdetail].actualqty);
            totalr = totalr + parseFloat(final[registerdetail].accountGroups[rgdetail].accountGroup2[sdetail].totalval);

          })
          final[registerdetail].accountGroups[rgdetail].actualqty = final[registerdetail].accountGroups[rgdetail].actualqty + "  " + qualityr;
          final[registerdetail].accountGroups[rgdetail].totalval = totalr;

          quality += qualityr;
          total += totalr;

          final[registerdetail].actualqty = quality;
          final[registerdetail].totalval = total;

          tableData.push({
            predocno: final[registerdetail].predocno,
            itemcode: "",
            itemname: "",
            actualqty: "",
            transuom: "",
            transrate: "",
            totalval: "",
            bold: true,
          });

          tableData.push({
            predocno: final[registerdetail].accountGroups[rgdetail].predocno,
            itemcode: final[registerdetail].accountGroups[rgdetail].itemcode,
            itemname: final[registerdetail].accountGroups[rgdetail].itemname,
            actualqty: final[registerdetail].accountGroups[rgdetail].actualqty,
            transuom: final[registerdetail].accountGroups[rgdetail].transuom,
            transrate: final[registerdetail].accountGroups[rgdetail].transrate,
            totalval: final[registerdetail].accountGroups[rgdetail].totalval,
            bold: true,
          });




          //   const accountGroups = Object.keys(final[registerdetail].accountGroups);
          accountGroup22.forEach(sdetail => {


            tableData.push({
              predocno: final[registerdetail].accountGroups[rgdetail].accountGroup2[sdetail].predocno,
              itemcode: final[registerdetail].accountGroups[rgdetail].accountGroup2[sdetail].itemcode,
              itemname: final[registerdetail].accountGroups[rgdetail].accountGroup2[sdetail].itemname,
              actualqty: final[registerdetail].accountGroups[rgdetail].accountGroup2[sdetail].actualqty,
              transuom: final[registerdetail].accountGroups[rgdetail].accountGroup2[sdetail].transuom,
              transrate: final[registerdetail].accountGroups[rgdetail].accountGroup2[sdetail].transrate,
              totalval: final[registerdetail].accountGroups[rgdetail].accountGroup2[sdetail].totalval,
            })


          })
        })
      });

      this.tabledata = tableData;

      console.log("Final Data:", this.tabledata)
    }
    });
  }
}