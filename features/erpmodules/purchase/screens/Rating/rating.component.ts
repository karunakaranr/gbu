import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Select, Store } from '@ngxs/store';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { PurchaseURLS } from '../../URLS/url';
import { RatingService } from '../../services/Rating/Rating.service';
import * as RatingJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/Rating.json'
import { IRating } from '../../models/IRating';
import { LayoutState } from 'features/layout/store/layout.state';
import { Observable } from 'rxjs';
@Component({
    selector: 'app-Rating',
    templateUrl: './rating.component.html',
    styleUrls: ['./rating.component.scss'],
    providers: [
        { provide: 'IdField', useValue: 'Type' },
        { provide: 'url', useValue: PurchaseURLS.Rating },
        { provide: 'saveurl', useValue: PurchaseURLS.SaveRating },
        { provide: 'DataService', useClass: RatingService },
        { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url','saveurl'] },
        { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
    ]
})
export class RatingComponent extends GBBaseDataPageComponentWN<IRating> {
    title: string = "Rating"
    RatingJSON = RatingJSON
    @Select(LayoutState.Resetformvalue) resetform$: Observable<any>;

    form: GBDataFormGroupWN<IRating> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "Rating", {}, this.gbps.dataService);

    thisConstructor() {
        let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
        if (arrayOfValues != null) {
            // this.RatingPicklistFillValue(arrayOfValues.RatingId)
            this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
        } else {
            this.store.dispatch(new ButtonVisibility("Forms"))
        }
        this.resetform$.subscribe((res:boolean)=>{
			this.Rating('0')
		})
    }

    public Rating(Rating) {
        console.log("payperiod", Rating)
        this.gbps.dataService.getData(Rating).subscribe(res =>{
            console.log("rrr",res)
            this.form.patchValue(res[0])
            console.log("NNA",this.form.value)
        })
    }
}


export function getThisDBDataService(http: GBHttpService, url: string,saveurl:string): GBBaseDBDataService<IRating> {
    const dbds: GBBaseDBDataService<IRating> = new GBBaseDBDataService<IRating>(http);
    dbds.endPoint = url;
    dbds.saveurl = saveurl;
    return dbds;
}


export function getThisPageService(store: Store, dataService: GBBaseDataService<IRating>, dbDataService: GBBaseDBDataService<IRating>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IRating> {
    return new GBDataPageService<IRating>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
