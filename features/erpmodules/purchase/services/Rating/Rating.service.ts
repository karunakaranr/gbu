import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { IRating } from '../../models/IRating';

 
@Injectable({
  providedIn: 'root'
})
export class RatingService extends GBBaseDataServiceWN<IRating> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IRating>) {
    super(dbDataService, 'Type');
  }
}
