import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

 import { ProblemVsSolutionComponent } from './screens/ProblemVsSolution/ProblemVsSolution.component';
import { CallSetComponent } from './screens/CallSet/CallSet.component';
import { WorkTypeComponent } from './screens/WorkType/WorkType.Component';

const routes: Routes = [

    {
      path: 'themestest',
      loadChildren: () =>
        import(`../../themes/themes.module`).then((m) => m.ThemesModule),
    },
    {
      path: 'problemvssolution',
      component: ProblemVsSolutionComponent
    },
    {
      path: 'callset',
      component: CallSetComponent
    },
    {
      path: 'work',
      component: WorkTypeComponent
    }
  ];
  
  @NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
  })
  export class CRMServiceroutingModule {}