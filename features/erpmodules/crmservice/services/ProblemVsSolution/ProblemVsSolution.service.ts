import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN,GBBaseDBDataService } from 'libs/gbdata/src';


import { IProblemVsSolution } from '../../models/IProblemVsSolution';

 
@Injectable({
  providedIn: 'root'
})
export class ProblemVsSolutionService extends GBBaseDataServiceWN<IProblemVsSolution> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IProblemVsSolution>) {
    super(dbDataService, 'Problem.Id');
  }
}
