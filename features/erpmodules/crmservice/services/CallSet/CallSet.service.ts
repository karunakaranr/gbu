import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';

import { ICallSet } from '../../models/ICallSet';
 
@Injectable({
  providedIn: 'root'
})
export class CallSetService extends GBBaseDataServiceWN<ICallSet> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<ICallSet>) {
    super(dbDataService, 'CallSetId');
  }
}


