import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { CallSetService } from '../../services/CallSet/CallSet.service';
import { ICallSet } from '../../models/ICallSet';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as CallSetJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/CallSet.json'
import { CRMServiceURLS } from './../../URLS/url'

@Component({
  selector: 'app-callset',
  templateUrl: './callset.component.html',
  styleUrls: ['./callset.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'CallSetId'},
    { provide: 'url', useValue: CRMServiceURLS.CallSet },
    { provide: 'DataService', useClass: CallSetService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class CallSetComponent extends GBBaseDataPageComponentWN<ICallSet> {
  title : string = "CallSet"
  CallSetJSON =CallSetJSON;
  form: GBDataFormGroupWN<ICallSet> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "CallSet", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if(arrayOfValues != null){
      this.CallSetFillData(arrayOfValues.CallSetId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
   }

  public CallSetFillData(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(FormVariable => {
          this.form.patchValue(FormVariable);
        })
    }
  }


  public CallSetPatchids(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<ICallSet> {
  const dbds: GBBaseDBDataService<ICallSet> = new GBBaseDBDataService<ICallSet>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<ICallSet>, dbDataService: GBBaseDBDataService<ICallSet>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<ICallSet> {
  return new GBDataPageService<ICallSet>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
