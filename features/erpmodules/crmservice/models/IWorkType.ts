export interface IWorkType {
    WorkTypeId: number
    WorkTypeCode: string
    WorkTypeName: string
    AllocationTypeId: number
    AllocationTypeName: string
    WorkTypeIsAllocationBased: string
    WorkTypeRemarks: string
    WorkTypeCreatedOn: string
    WorkTypeModifiedOn: string
    WorkTypeModifiedByName: string
    WorkTypeCreatedByName: string
    WorkTypeVersion: number
    WorkTypeDetailArray: WorkTypeDetailArray[]
  }
  
  export interface WorkTypeDetailArray {
    ParentWorkTypeDetailId: string
    WorkTypeDetailLevel: number
    WorkTypeDetailId: string
    WorkTypeId: string
    undefined: string
    ActivityId: string
    WorkTypeDetailDescription: string
    EmployeeId: string
    WorkTypeDetailEstimatedDuration: number
    WorkTypeDetailScheduleDays: string
    WorkTypeDetailAfterIds: string
    WorkTypeDetailRemarks: string
    WorkTypeDetailSlNo: number
  }
  