export interface ICallSet {
    CallSetId: number
    CallSetCode: string
    CallSetName: string
    CallSetRemarks: string
    CallSetDetailArray: CallSetDetailArray[]
    CallSetCreatedOn: string
    CallSetModifiedOn: string
    CallSetModifiedByName: string
    CallSetStatus: number
    CallSetVersion: number
  }
  
  export interface CallSetDetailArray {
    CallSetDetailSlNo: number
    CallSetDetailId: string
    undefined: string
    CallTypeId: string
    CallTypeName: string
    CallNatureId: string
    CallNatureName: string
    CallPriorityId: string
    CallPriorityName: string
    CallSetDetailNumber: string
    CallSetDetailPeriod: string
    CallSetDetailPeriodType: number
    CallSetId: number
  }
  