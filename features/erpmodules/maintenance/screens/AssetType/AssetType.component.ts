import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
// import { ServiceName } from ‘ServicePath’;
// import { ModelName } from ‘ModelPath’;
import { AssetTypeService } from '../../services/AssetType/AssetType.service';
import { IAssetType } from '../../models/IAssetType';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as AssetTypeJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/AssetType.json'
import { MaintenanceURLS } from '../../URLS/urls';


@Component({
  selector: 'app-AssetType',
  templateUrl: './AssetType.component.html',
  styleUrls: ['./AssetType.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'AssetTypeId'},
    { provide: 'url', useValue: MaintenanceURLS.AssetType },
    { provide: 'DataService', useClass: AssetTypeService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class AssetTypeComponent extends GBBaseDataPageComponentWN<IAssetType > {
  title: string = 'AssetType'
  AssetTypeJSON = AssetTypeJSON;


  form: GBDataFormGroupWN<IAssetType > = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'AssetType', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.AssetTypeRetrivalValue(arrayOfValues.AssetTypeId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }


  public AssetTypeRetrivalValue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(PackSet => {
        this.form.patchValue(PackSet);
      })
    }
  }


  public AssetTypePatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }

  public ChangeTab(event, TabName:string) {
    var i:number, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("FormTabLinks");
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(TabName).style.display = "block";
    console.log("Event:",event)
    event.currentTarget.className += " active";
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IAssetType > {
  const dbds: GBBaseDBDataService<IAssetType > = new GBBaseDBDataService<IAssetType >(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IAssetType >, dbDataService: GBBaseDBDataService<IAssetType >, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IAssetType > {
  return new GBDataPageService<IAssetType >(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
