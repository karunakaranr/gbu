import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ITypeComponent } from '../../models/ITypeComponent';
import { TypeComponentService } from '../../services/TypeComponent/TypeComponent.service';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as TypeComponentjson from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/TypeComponent.json'
import { MaintenanceURLS } from '../../URLS/urls';


@Component({
  selector: "app-TypeComponent",
  templateUrl: "./TypeComponent.component.html",
  styleUrls: ["./TypeComponent.component.scss"],
  providers: [
    { provide: 'IdField', useValue: 'TypeComponentId' },
    { provide: 'url', useValue: MaintenanceURLS.TypeComponentGet },
    { provide: 'saveurl', useValue: MaintenanceURLS.TypeComponent },
    { provide: 'deleteurl', useValue: MaintenanceURLS.TypeComponent },
    { provide: 'DataService', useClass: TypeComponentService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url','saveurl','deleteurl'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class TypeComponentComponent extends GBBaseDataPageComponentWN<ITypeComponent> {
  title: string = "TypeComponent"
  TypeComponentjson = TypeComponentjson;


  form: GBDataFormGroupWN<ITypeComponent> = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'TypeComponent', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.TypeComponentFillFunction(arrayOfValues.TypeComponentId,"")
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }


  public TypeComponentFillFunction(SelectedPicklistData: string,FieldName:string): void {

    if (SelectedPicklistData) {
      this.gbps.dataService.DynamicPostData(SelectedPicklistData,FieldName).subscribe(TypeComponent => {
        this.form.get('TypeComponentDetailArray').patchValue(TypeComponent);

      })

    }
 
  }

  public TypeComponentPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string,saveurl:string,deleteurl:string): GBBaseDBDataService<ITypeComponent> {
  const dbds: GBBaseDBDataService<ITypeComponent> = new GBBaseDBDataService<ITypeComponent>(http);
  dbds.endPoint = url;
  dbds.saveurl = saveurl;
  dbds.deleteurl = deleteurl
  return dbds;
}

export function getThisPageService(store: Store, dataService: GBBaseDataService<ITypeComponent>, dbDataService: GBBaseDBDataService<ITypeComponent>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<ITypeComponent> {
  return new GBDataPageService<ITypeComponent>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
