
import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs';
import { Select } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { SharedService } from 'features/commonreport/service/datapassing.service';



@Component({
    selector: 'app-Machinelist',
    templateUrl: './Machinelist.component.html',
    styleUrls: ['./Machinelist.component.scss'],
})
export class MachinelistComponent implements OnInit {
    @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
    public rowDatas: any;
    constructor(public sharedService: SharedService) { }
    ngOnInit(): void {
        this.getMachinelist();
    }

    public getMachinelist() {
        this.sharedService.getRowInfo().subscribe(data => {
            this.rowDatas = data;
            if (data.length > 0) {
            console.log("Machinelist Details:", this.rowDatas)

            if (data.MachineStatus === "1") {
                data.MachineStatus = "W";
            } else if (data.MachineStatus === "1") {
                data.MachineStatus = "W";
            }
        }
        });
    
    }
    
}
