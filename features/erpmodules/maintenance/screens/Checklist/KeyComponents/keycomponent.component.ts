
import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs';
import { Select } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { SharedService } from 'features/commonreport/service/datapassing.service';


@Component({
    selector: 'app-keycomponent',
    templateUrl: './keycomponent.component.html',
    styleUrls: ['./keycomponent.component.scss'],
})
export class keycomponentComponent implements OnInit {
    @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
    public rowData: any;
    constructor(public sharedService: SharedService) { }
    ngOnInit(): void {
        this.getkeycomponent();
    }

    public getkeycomponent() {
        this.sharedService.getRowInfo().subscribe(data => {
            this.rowData = data;
            if (data.length > 0) {
                console.log("Key Component Details :", this.rowData)


                let grouping = this.rowData;

                var finalizedGrp = [];

                grouping.map((inputdata, index) => {



                    finalizedGrp.push({
                        sno: index,
                        code: inputdata['DetailCode'],
                        name: inputdata['DetailName'],
                        serialno: inputdata['ReplaceOn'],
                        replace: inputdata['ReplaceOn'],
                        remarks: inputdata['DetailRemarks'],


                        keycode: inputdata['Code'] === "None" ? "" : inputdata['Code'],
                        keyname: inputdata['Name'] === "None" ? "" : inputdata['Name'],


                    });
                });

                const final = {};
                finalizedGrp.forEach((detailData) => {
                    final[detailData.keycode] = {
                        supplyGroups: {},
                        code: detailData.keycode,
                        name: detailData.keyname,
                        serialno: "",
                        replace: "",
                        remarks: "",


                        ...final[detailData.keycode]

                    };

                    final[detailData.keycode].supplyGroups[detailData.sno] = {
                        //sno:detailData.sno,
                        code: detailData.code,
                        name: detailData.name,
                        serialno: detailData.serialno,
                        replace: detailData.replace,
                        remarks: detailData.remarks,

                    }
                });
                const finalgrouping = Object.keys(final);
                const tableData = [];

                finalgrouping.forEach((codeData) => {
                    tableData.push({
                        code: final[codeData].code,
                        name: final[codeData].name,
                        serialno: "",
                        replace: "",
                        remarks: "",
                        bold: true,
                    });

                    const accounts = Object.keys(final[codeData].supplyGroups);
                    accounts.forEach((account) => {
                        tableData.push({
                            code: final[codeData].supplyGroups[account].code,
                            name: final[codeData].supplyGroups[account].name,
                            serialno: final[codeData].supplyGroups[account].serialno,
                            replace: final[codeData].supplyGroups[account].replace,
                            remarks: final[codeData].supplyGroups[account].remarks,


                        })
                    })

                })

                this.rowData = tableData;
                console.log("Final Data:", this.rowData[0])
            }
        });


    }
}
