import { Component, Inject, LOCALE_ID, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { SharedService } from 'features/commonreport/service/datapassing.service';
import { UOMService } from 'features/erpmodules/inventory/services/UOM/uom.service';
import { IScheduleDatewise } from 'features/erpmodules/maintenance/models/IScheduleDatewise';
import { DateFormaterPipe } from 'libs/gbcommon/src/lib/Date-Formater.pipe';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-ScheduleDatewise',
  templateUrl: './ScheduleDatewise.component.html',
  styleUrls: ['./ScheduleDatewise.component.scss'],
})
export class ScheduleDatewiseComponent implements OnInit {
  @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
  private dateFormatter: DateFormaterPipe = new DateFormaterPipe(this.store);

  public rowData: IScheduleDatewise[];

  constructor(public sharedService: SharedService,public store: Store, @Inject(LOCALE_ID) public locale: string){  }

  ngOnInit(): void {
    this.getScheduleDatewise();
  }

  public getScheduleDatewise(){
    this.sharedService.getRowInfo().subscribe(data => {
      if (data.length > 0) {
      this.rowData = data;
      console.log("Schedule Datewise Data :", this.rowData)

      let json = this.rowData;
      var finalizedArray = []
      json.map((row,index)=> {
        finalizedArray.push({
          sno: index,
          AssignedToEmployeeName: row['AssignedToEmployeeName'],
          TaskPlanStartOn: this.dateFormatter.transform(row['TaskPlanStartOn'], 'date'),
          TaskDetailTypeName: row['TaskDetailTypeName'],
         
          RecordStatus: row['RecordStatus'] == "Active" ? '' : '*',
          Overdue: row['Overdue'],

          TaskNumber: row['TaskNumber'],
          TaskTaskName: row['TaskTaskName'],
          OriginalPlannedDate: row['OriginalPlannedDate'],
          InChargeName: row['InChargeName'],
          AllocationShortName: row['AllocationShortName'],
          OriginalPlannedHours: row['OriginalPlannedHours'],
          TaskPlannedHours: row['TaskPlannedHours'],
          TaskActualHours: row['TaskActualHours'],
          TaskOriginalStatusName: row['TaskOriginalStatusName'] ==  "COMPLETED" ? 'C' : 'P',
          CompletedPercentage: row['CompletedPercentage'],
        });
      });
      const final = {};
      finalizedArray.forEach(detail => {
        final[detail.AssignedToEmployeeName] = {
          accounts: {},
          TaskNumber: detail.AssignedToEmployeeName,
          TaskTaskName: "",
          OriginalPlannedDate: "",
          InChargeName: "",
          AllocationShortName: "",
          OriginalPlannedHours: "",
          TaskPlannedHours: "",
          TaskOriginalStatusName: "",
          CompletedPercentage: "",
          Overdue: "",
          ...final[detail.AssignedToEmployeeName],
        }
            final[detail.AssignedToEmployeeName].accounts[detail.TaskPlanStartOn] = {
                accountGroups: {},
              TaskNumber: detail.TaskPlanStartOn,
              TaskTaskName: "",
              OriginalPlannedDate: "",
              InChargeName: "",
              AllocationShortName: "",
              OriginalPlannedHours: "",
              TaskPlannedHours: "",
              TaskOriginalStatusName: "",
              CompletedPercentage: "",
              Overdue: "",
              ...final[detail.AssignedToEmployeeName].accounts[detail.TaskPlanStartOn],
            }
        final[detail.AssignedToEmployeeName].accounts[detail.TaskPlanStartOn].accountGroups[detail.sno] = {
            TaskNumber: detail.TaskNumber + " (" + detail.TaskDetailTypeName + ")" + detail.RecordStatus ,
            TaskTaskName: detail.TaskTaskName,
            OriginalPlannedDate: detail.OriginalPlannedDate,
            InChargeName: detail.InChargeName,
            AllocationShortName: detail.AllocationShortName,
            OriginalPlannedHours: detail.OriginalPlannedHours,
            TaskPlannedHours: detail.TaskPlannedHours + " " + " " + detail.TaskActualHours,
            TaskOriginalStatusName: detail.TaskOriginalStatusName,
            CompletedPercentage: detail.CompletedPercentage,
            Overdue: detail.Overdue,
        }
      })
      const empcodes = Object.keys(final)
      const tableData = [];
      empcodes.forEach(code => {
        const accounts = Object.keys(final[code].accounts)

        tableData.push({
            TaskNumber: final[code].TaskNumber,
            TaskTaskName: "",
            OriginalPlannedDate: "",
            InChargeName: "",
            AllocationShortName: "",
            OriginalPlannedHours: "",
            TaskPlannedHours: "",
            TaskOriginalStatusName: "",
            CompletedPercentage: "",
            Overdue: "",
           bold: true,
        })
        accounts.forEach(ag => {
          tableData.push({
            TaskNumber: final[code].accounts[ag].TaskNumber,
            TaskTaskName: "",
            OriginalPlannedDate: "",
            InChargeName: "",
            AllocationShortName: "",
            OriginalPlannedHours: "",
            TaskPlannedHours: "",
            TaskOriginalStatusName: "",
            CompletedPercentage: "",
            Overdue: "",
            bold: true,
          })
       
         const accountGroups = Object.keys(final[code].accounts[ag].accountGroups);
         accountGroups.forEach((account) => {
            tableData.push({
                TaskNumber: final[code].accounts[ag].accountGroups[account].TaskNumber,
                TaskTaskName: final[code].accounts[ag].accountGroups[account].TaskTaskName,
                OriginalPlannedDate: final[code].accounts[ag].accountGroups[account].OriginalPlannedDate,
                InChargeName: final[code].accounts[ag].accountGroups[account].InChargeName,
                AllocationShortName: final[code].accounts[ag].accountGroups[account].AllocationShortName,
                OriginalPlannedHours: final[code].accounts[ag].accountGroups[account].OriginalPlannedHours,
                TaskPlannedHours: final[code].accounts[ag].accountGroups[account].TaskPlannedHours,
                TaskOriginalStatusName: final[code].accounts[ag].accountGroups[account].TaskOriginalStatusName,
                CompletedPercentage: final[code].accounts[ag].accountGroups[account].CompletedPercentage,
                Overdue: final[code].accounts[ag].accountGroups[account].Overdue,
            });
        });
    })

      })
      this.rowData = tableData;
      console.log("tableData",tableData)
    }
      });
    }
  }
  
  
