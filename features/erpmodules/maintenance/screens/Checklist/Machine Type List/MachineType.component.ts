
import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs';
import { Select } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';


@Component({
    selector: 'app-MachineType',
    templateUrl: './MachineType.component.html',
    styleUrls: ['./MachineType.component.scss'],
})
export class MachineTypeComponent implements OnInit {
    @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
    public rowData: any;
    constructor() { }
    ngOnInit(): void {
        this.getMachineType();
    }

    public getMachineType() {
        this.rowdatacommon$.subscribe(data => {
            this.rowData = data;
            console.log("Machine Type Details :", this.rowData)
            

            let grouping = this.rowData;

            var finalizedGrp = [];

            grouping.map((inputdata,index) =>{

                let nature ;

                if (inputdata['MachineTypeNature']==0){
                    nature = "Facility"
                } else {
                    nature = "Production"
                }

                finalizedGrp.push({
                    sno:index,
                    code:inputdata['MachineTypeCode'],
                    name:inputdata['MachineTypeName'],
                    para:inputdata['ParameterSetName'],

                    nature:nature ,//grouping heading
                });
            });

            const final = {};
            finalizedGrp.forEach((detailData) => {
                final[detailData.nature] ={
                    supplyGroups:{},       // create a variable name for group which we can store data
                    code:detailData.nature,
                    name:"",
                    para:"",
                    
                    ...final[detailData.nature]

                };

                final[detailData.nature].supplyGroups[detailData.sno] = {
                    //sno:detailData.sno,
                    code: detailData.code,
                    name:detailData.name,
                    para: detailData.para,
                
                }
            });
            const finalgrouping = Object.keys(final);
            const tableData = [];

            finalgrouping.forEach((codeData) => {
                tableData.push({
                    code:final[codeData].code,
                    name:"",
                    para:"",
                    bold: true,
                });

                const accounts = Object.keys(final[codeData].supplyGroups); //supply group is must given about in data storage variable 
                accounts.forEach((account) => {
                    tableData.push({
                        code:final[codeData].supplyGroups[account].code,
                        name:final[codeData].supplyGroups[account].name,
                        para:final[codeData].supplyGroups[account].para,

                    })
                })

            })

             this.rowData=tableData;
             console.log("Final Data:",this.rowData[0])

        });

       
    }
}
