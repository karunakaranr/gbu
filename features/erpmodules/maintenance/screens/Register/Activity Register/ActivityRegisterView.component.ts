import { HttpClient } from '@angular/common/http';
import { Component, Inject, LOCALE_ID, OnInit } from '@angular/core';

import { Observable } from 'rxjs';
import { Select, Store } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { IActivityRegister } from 'features/erpmodules/maintenance/models/IActivityRegister';
import { SharedService } from 'features/commonreport/service/datapassing.service';
import { DateFormaterPipe } from 'libs/gbcommon/src/lib/Date-Formater.pipe';


@Component({
  selector: 'app-ActivityRegisterView',
  templateUrl: './ActivityRegisterView.component.html',
  styleUrls: ['./ActivityRegisterView.component.scss'],
})
export class ActivityRegisterViewComponent implements OnInit {
  @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
  private dateFormatter: DateFormaterPipe = new DateFormaterPipe(this.store);

  public rowData : IActivityRegister[];
  constructor(public sharedService: SharedService,public store: Store, @Inject(LOCALE_ID) public locale: string) { }

  ngOnInit(): void {
    this.ActivityRegisterView();
  }
  
  public ActivityRegisterView() {
    this.sharedService.getRowInfo().subscribe(data => {
      if (data.length > 0) {
      this.rowData = data;
      console.log("Activity Register View :", this.rowData)

      let xx = [];
      for(let data of this.rowData){
        if(data.LeadId != 0){
          xx.push(data)
        }
      }
      let json = xx;
      var finalizedArray = []
      json.map((row,index)=> {
        finalizedArray.push({
          sno: index,
          Name: row['Name'],

          TActionTransactionDate: this.dateFormatter.transform(row['TActionTransactionDate'], 'date'),

          TActionDetailActionType: row['TActionDetailActionType'],

          Number: row['Number'],
          AllocationShortName: row['AllocationShortName'],
          Description: row['Description'],
          TActionDetailPeriodFrom: row['TActionDetailPeriodFrom'],
          TActionDetailPeriodTo: row['TActionDetailPeriodTo'],
          PlannedHours: row['PlannedHours'],
          ActualHours: row['ActualHours'],
          Duration: row['Duration'],
          MovementDuration: row['MovementDuration'],
          UtilizationPercent: row['UtilizationPercent'],
          EfficiencyPercent: row['EfficiencyPercent'],
          ProductivePercent: row['ProductivePercent'],
          CompletedPercentage: row['CompletedPercentage'],
          TActionDetailActionStatus: row['TActionDetailActionStatus'],
        });
      });
      const final = {};
      finalizedArray.forEach(detail => {
        final[detail.Number] = {
          accounts: {},
          Number: detail.Number,
          AllocationShortName: "",
          Description: "",
          TActionDetailPeriodFrom: "",
          TActionDetailPeriodTo: "",
          PlannedHours: "",
          ActualHours: "",
          Duration: "",
          MovementDuration: "",
          UtilizationPercent: "",
          EfficiencyPercent: "",
          ProductivePercent: "",
          CompletedPercentage: "",
          TActionDetailActionStatus: "",
          TActionDetailActionType: "",
          ...final[detail.Number],
        }
            final[detail.Number].accounts[detail.TActionTransactionDate] = {
                accountGroups: {},
                Number: detail.TActionTransactionDate,
                AllocationShortName: "",
                Description: "",
                TActionDetailPeriodFrom: "",
                TActionDetailPeriodTo: "",
                PlannedHours: "",
                ActualHours: "",
                Duration: "",
                MovementDuration: "",
                UtilizationPercent: "",
                EfficiencyPercent: "",
                ProductivePercent: "",
                CompletedPercentage: "",
                TActionDetailActionStatus: "",
                TActionDetailActionType: "",
              ...final[detail.Number].accounts[detail.TActionTransactionDate],
            }
        final[detail.Number].accounts[detail.TActionTransactionDate].accountGroups[detail.sno] = {
          Number: detail.Number,
          AllocationShortName: detail.AllocationShortName,
          Description: detail.Description,
          TActionDetailPeriodFrom: detail.TActionDetailPeriodFrom,
          TActionDetailPeriodTo: detail.TActionDetailPeriodTo,
          PlannedHours: detail.PlannedHours,
          ActualHours: detail.ActualHours,
          Duration: detail.Duration,
          MovementDuration: detail.MovementDuration,
          UtilizationPercent: detail.UtilizationPercent,
          EfficiencyPercent: detail.EfficiencyPercent,
          ProductivePercent: detail.ProductivePercent,
          CompletedPercentage: detail.CompletedPercentage,
          TActionDetailActionStatus: detail.TActionDetailActionStatus,
          TActionDetailActionType:"",
        }
      })
      const empcodes = Object.keys(final)
      const tableData = [];
      empcodes.forEach(code => {
        const accounts = Object.keys(final[code].accounts)

        tableData.push({
          Number: final[code].Number,
          AllocationShortName: "",
          Description: "",
          TActionDetailPeriodFrom: "",
          TActionDetailPeriodTo: "",
          PlannedHours: "",
          ActualHours: "",
          Duration: "",
          MovementDuration: "",
          UtilizationPercent: "",
          EfficiencyPercent: "",
          ProductivePercent: "",
          CompletedPercentage: "",
          TActionDetailActionStatus: "",
          TActionDetailActionType: "",
           bold: true,
        })
        accounts.forEach(ag => {
          tableData.push({
            Number: final[code].accounts[ag].Number,
          AllocationShortName: "",
          Description: "",
          TActionDetailPeriodFrom: "",
          TActionDetailPeriodTo: "",
          PlannedHours: "",
          ActualHours: "",
          Duration: "",
          MovementDuration: "",
          UtilizationPercent: "",
          EfficiencyPercent: "",
          ProductivePercent: "",
          CompletedPercentage: "",
          TActionDetailActionStatus: "",
          TActionDetailActionType: "",
            bold: true,
          })
       
         const accountGroups = Object.keys(final[code].accounts[ag].accountGroups);
         accountGroups.forEach((account) => {
            tableData.push({
              Number: final[code].accounts[ag].accountGroups[account].Number,
              AllocationShortName: final[code].accounts[ag].accountGroups[account].AllocationShortName,
              Description: final[code].accounts[ag].accountGroups[account].Description,
              TActionDetailPeriodFrom: final[code].accounts[ag].accountGroups[account].TActionDetailPeriodFrom,
              TActionDetailPeriodTo: final[code].accounts[ag].accountGroups[account].TActionDetailPeriodTo,
              PlannedHours: final[code].accounts[ag].accountGroups[account].PlannedHours,
              ActualHours: final[code].accounts[ag].accountGroups[account].ActualHours,
              Duration: final[code].accounts[ag].accountGroups[account].Duration,
              MovementDuration: final[code].accounts[ag].accountGroups[account].MovementDuration,
              UtilizationPercent: final[code].accounts[ag].accountGroups[account].UtilizationPercent,
              EfficiencyPercent: final[code].accounts[ag].accountGroups[account].EfficiencyPercent,
              ProductivePercent: final[code].accounts[ag].accountGroups[account].ProductivePercent,
              CompletedPercentage: final[code].accounts[ag].accountGroups[account].CompletedPercentage,
              TActionDetailActionStatus: final[code].accounts[ag].accountGroups[account].TActionDetailActionStatus,
              TActionDetailActionType: "",
            });
        });
    })

      })
      this.rowData = tableData;
      console.log("tableData",tableData)
    }
    });
  }
  

}
