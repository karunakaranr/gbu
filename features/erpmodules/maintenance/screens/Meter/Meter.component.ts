import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { MeterService } from '../../services/Meter/Meter.service';
import { IMeter } from '../../models/IMeter';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
// import * as JsonName from ‘JsonPath’ './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/PackSet.json';
import * as MeterJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/Meter.json'
import { MaintenanceURLS } from '../../URLS/urls';


@Component({
  selector: 'app-Meter',
  templateUrl: './Meter.component.html',
  styleUrls: ['./Meter.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'MeterId'},
    { provide: 'url', useValue: MaintenanceURLS.Meter },
    { provide: 'DataService', useClass: MeterService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class MeterComponent extends GBBaseDataPageComponentWN<IMeter > {
  title: string = 'Meter'
  MeterJSON = MeterJSON;


  form: GBDataFormGroupWN<IMeter > = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'Meter', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.MeterFillFunction(arrayOfValues.FormId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }


  public MeterFillFunction(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(PackSet => {
        this.form.patchValue(PackSet);
      })
    }
  }


  public MeterPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IMeter > {
  const dbds: GBBaseDBDataService<IMeter > = new GBBaseDBDataService<IMeter >(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IMeter >, dbDataService: GBBaseDBDataService<IMeter >, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IMeter > {
  return new GBDataPageService<IMeter >(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
