import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { SolutionService } from '../../services/Solution/Solution.service';
import { ISolution } from '../../models/ISolution';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as SolutionJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/Solution.json'
import { MaintenanceURLS } from '../../URLS/urls';


@Component({
  selector: 'app-Solution',
  templateUrl: './Solution.component.html',
  styleUrls: ['./Solution.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'SolutionId'},
    { provide: 'url', useValue: MaintenanceURLS.Solution },
    { provide: 'DataService', useClass: SolutionService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class SolutionComponent extends GBBaseDataPageComponentWN<ISolution > {
  title: string = 'Solution'
  SolutionJSON = SolutionJSON;


  form: GBDataFormGroupWN<ISolution > = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'Solution', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.SolutionFillFunction(arrayOfValues.SolutionId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }


  public SolutionFillFunction(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(PackSet => {
        this.form.patchValue(PackSet);
      })
    }
  }


  public SolutionPatchValue(SelectedPicklistDatas: any): void {
    console.log("SelectedPicklistDatas*: ",SelectedPicklistDatas) 
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<ISolution > {
  const dbds: GBBaseDBDataService<ISolution > = new GBBaseDBDataService<ISolution >(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<ISolution >, dbDataService: GBBaseDBDataService<ISolution >, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<ISolution > {
  return new GBDataPageService<ISolution >(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
