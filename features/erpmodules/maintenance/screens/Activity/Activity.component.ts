import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ActivityService } from '../../services/Activity/Activity.service';
import { IActivity } from '../../models/IActivity';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
// import * as JsonName from ‘JsonPath’ './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/PackSet.json';
import * as ActivityJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/Activity.json'
// import { url} from ‘UrlPath’;
import { MaintenanceURLS } from '../../URLS/urls';


@Component({
  selector: 'app-Activity',
  templateUrl: './Activity.component.html',
  styleUrls: ['./Activity.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'ActivityId'},
    { provide: 'url', useValue: MaintenanceURLS.ActivityRecruitmentProcess },
    { provide: 'DataService', useClass: ActivityService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class ActivityComponent extends GBBaseDataPageComponentWN<IActivity > {
  title: string = 'Activity'
  ActivityJSON = ActivityJSON;


  form: GBDataFormGroupWN<IActivity > = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'Activity', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.ActivityFillFunction(arrayOfValues.ActivityId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }


  public ActivityFillFunction(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(PackSet => {
        this.form.patchValue(PackSet);
      })
    }
  }


  public ActivityPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IActivity > {
  const dbds: GBBaseDBDataService<IActivity > = new GBBaseDBDataService<IActivity >(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IActivity >, dbDataService: GBBaseDBDataService<IActivity >, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IActivity > {
  return new GBDataPageService<IActivity >(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
