export interface IAssetTypeMaterial {
  ActivityMaterialSlNo: string
  ActivityMaterialId: string
  AssetTypeId: string
  AssetTypeName: string
  ResourceTypeId: string
  ResourceTypeName: string
  MachineTypeId: string
  MachineTypeName: string
  ActivityId: string
  ActivityName: string
  ActivityMaterialRemarks: string
  ActivityMaterialNature: string
  ActivityMaterialVersion: string
  undefined: string
  ItemId: string
  ItemName: string
  SkuId: string
  SKUName: string
  ActivityMaterialDuration: string
  ActivityMaterialQuantity: string
  ItemStockUOMName: string
  ActivityMaterialUsageType: string
}
