export interface IScheduleDatewise {
    TaskId: number
    TaskTaskType: number
    BizTransactionTypeId: number
    OuId: number
    PeriodId: number
    TaskNumber: string
    TaskDate: string
    TaskReferenceNumber: string
    TaskReferenceDate: string
    TaskTaskName: string
    TaskDescription: string
    ChildDetailType: number
    TaskDetailTypeName: string
    TaskTaskNature: number
    TaskNatureName: string
    PartyBranchId: number
    PartyBranchCode: string
    PartyBranchName: string
    ActivityId: number
    ActivityCode: string
    ActivityName: string
    AssignedToType: number
    DefaultImageId: string
    RequestedById: number
    RequestedByCode: string
    RequestedByName: string
    InChargeId: number
    EmployeeId: number
    InChargeCode: string
    InChargeName: string
    InchargePrimaryMailId: string
    AssignedToPrimaryMailId: string
    AssignedToEmployeeId: number
    AssignedToEmployeeCode: string
    AssignedToEmployeeName: string
    AllocationId: number
    AllocationName: string
    AllocationShortName: string
    ParentTaskId: number
    TaskOriginalStatus: number
    TaskOriginalStatusName: string
    TaskActualStartOn: string
    TaskActualEndOn: string
    TaskActualHours: number
    TaskPlannedHours: number
    TaskPlanStartOn: string
    TaskPlanEndOn: string
    OriginalPlannedDate: string
    OriginalPlannedHours: number
    TaskRemarks: string
    TaskDueOn: string
    ExpireOn: string
    GraceTime: number
    ParentName: string
    WBCCode: string
    ParentTaskNumber: string
    ParentDetailType: number
    CompletedPercentage: number
    RecordStatus: string
    Priority: string
    Overdue: string
    MileStoneId: number
    MileStoneNumber: string
    MileStoneName: string
    MileStoneDescription: string
    MileStoneStatus: number
    HeadingId: number
    HeadingNumber: string
    HeadingDescription: string
    HeadingName: string
    HeadingStatus: number
    WorkId: number
    WorkNumber: string
    WorkDescription: string
    WorkName: string
    WorkStatus: number
    RollUpId: number
    RollUpNumber: string
    RollUpDescription: string
    RollUpName: string
    RollUpStatus: number
    OuName: string
    LeadCode: string
    LeadName: string
    CreatedOn: string
    CreatedById: number
    CreatedByName: string
    ModifiedById: number
    ModifiedByName: string
    ModifiedOn: string
    ProjectName: string
    TaskVersion: number
    AssetId: number
    AssetCode: string
    AssetName: string
    ActionDetailDuration: number
    CallActionDetailDuration: string
    TotalOriginalPlannedHours: number
    TotalTaskActualHours: number
    TotalTaskPlannedHours: number
    CallTotalOriginalPlannedHours: string
    CallTotalTaskActualHours: string
    CallTotalTaskPlannedHours: string
    CallOriginalPlannedHours: string
    CallTaskActualHours: string
    CallTaskPlannedHours: string
    ProcessInstanceId: number
    DisplayValue: number
    InchargeMail: string
    EntityCode: string
    DocumentNumber: number
    ObjectId: number
    FromUserCode: string
    ViewImageUrls: string
    TaskDetailType: number
    SelectTypeId: number
    SelectTypeName: string
    AssignedTo: number
    CallAllotedTypeName: string
    CompletedById: number
    CompletedByName: string
    ClosedById: number
    ClosedByName: string
    CallTypeId: number
    CallTypeName: string
    WorkBenchType: number
    CallNatureId: number
    CallNatureName: string
    TaskReportMailDTOs: string
    ActivitiStatus: number
    TaskIsToDo: number
    CallPriorityId: number
    CallPriorityName: string
    NumberOfRecords: number
    Label: string
  }
  