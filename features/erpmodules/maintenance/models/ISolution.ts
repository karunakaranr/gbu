export class ISolution {
    SolutionId: number
    SolutionSolutionCode: string
    SolutionSolutionName: string
    SolutionEstimatedTime: number
    SolutionRemarks: string
    SolutionCreatedOn: string
    SolutionModifiedOn: string
    SolutionModifiedByName: string
    SolutionCreatedByName: string
    SolutionStatus: number
    SolutionVersion: number
}