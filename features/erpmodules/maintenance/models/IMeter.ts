export class IMeter {
    MeterId: number
    MeterName: string
    MeterNature: number
    MeterType: number
    MeterUtilityType: string
    MeterMeterNumber: string
    MeterRunType: number
    PartyId: number
    MeterAccountNumber: string
    MeterAttachedType: number
    TariffCategoryId: number
    MeterBillDate: string
    MeterBillCycle: number
    MeterPayByDate: string
    OrganizationUnitId: number
    PartyBranchId: number
    ParameterSetId: number
    ConsumptionParameterId: number
    AssetId: string
    MeterLastMeterReadingAt: string
    LastMetervalue: number
    MeterSetValue: string
    MeterIsIncludeInConsumption: number
    TypeOfMeterId: number
    MeterStatus: number
    MeterVersion: number
}