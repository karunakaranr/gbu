export class IActivityType {
    ActivityTypeId: number
    ActivityTypeCode: string
    ActivityTypeName: string
    ActivityTypeGenerateType: string
    ActivityTypeAutoType: string
    ActivityTypeAutoTypeProcess: string
    ActivityTypeClassId: number
    ActivityTypeSubClassId: number
    ActivityTypeIsActive: number
    ActivityTypeRemarks: string
    ActivityTypeCreatedById: number
    ActivityTypeCreatedByName: string
    ActivityTypeCreatedOn: string
    ActivityTypeModifiedById: number
    ActivityTypeModifiedByName: string
    ActivityTypeModifiedOn: string
    ActivityTypeSortOrder: number
    ActivityTypeStatus: number
    ActivityTypeVersion: number
}