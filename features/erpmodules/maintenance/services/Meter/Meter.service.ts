import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';


import { IMeter } from '../../models/IMeter';


 
@Injectable({
  providedIn: 'root'
})
export class MeterService extends GBBaseDataServiceWN<IMeter> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IMeter>) {
    super(dbDataService, 'MeterId');
  }
}
