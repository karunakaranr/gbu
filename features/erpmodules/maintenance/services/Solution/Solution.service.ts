import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';


// import { ModelName } from ‘ModelPath’;
import { ISolution } from '../../models/ISolution';


 
@Injectable({
  providedIn: 'root'
})
export class SolutionService extends GBBaseDataServiceWN<ISolution> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<ISolution>) {
    super(dbDataService, 'SolutionId');
  }
}
