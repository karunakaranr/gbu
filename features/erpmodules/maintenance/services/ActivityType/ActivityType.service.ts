import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';


import { IActivityType } from '../../models/IActivityType';


 
@Injectable({
  providedIn: 'root'
})
export class ActivityTypeService extends GBBaseDataServiceWN<IActivityType> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IActivityType>) {
    super(dbDataService, 'ActivityTypeId');
  }
}
