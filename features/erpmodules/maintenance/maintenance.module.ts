import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ShortNumberPipe } from 'libs/gbcommon/src/lib/short-number.pipe';

import { UicoreModule } from '@goodbooks/uicore';
import { UifwkUifwkmaterialModule } from '@goodbooks/uifwk/material';
import { NgxsModule } from '@ngxs/store';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


import { NgxsFormPluginModule } from '@ngxs/form-plugin';

import { TranslocoRootModule } from './../../transloco/transloco-root.module';
import { GbdataModule } from '@goodbooks/gbdata';
import { environment } from '../../environments/environment';
import { NgBreModule } from '@unisoft/ng-bre';

import { NgxChartsModule } from '@swimlane/ngx-charts';
import { GbchartModule } from '../../common/shared/screens/gbchart/gbchart.module';

import { EntitylookupModule } from './../../entitylookup/entitylookup.module';
import { geolocationModule } from './../../geolocation/geolocation.module'
import { NgSelectModule } from '@ng-select/ng-select';

import { AgGridModule } from 'ag-grid-angular';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule } from '@angular/material/button';
import { MatListModule } from '@angular/material/list'
import { MatIconModule } from '@angular/material/icon';
import { MatDividerModule } from '@angular/material/divider';
import { MatExpansionModule } from '@angular/material/expansion';

import { MatTabsModule } from '@angular/material/tabs'
import { MatCheckboxModule } from '@angular/material/checkbox';
import { AgmCoreModule } from '@agm/core';
import { AgmDirectionModule } from 'agm-direction';

import { GbgridModule } from 'features/gbgrid/components/gbgrid/gbgrid.module';
import { CommonReportModule } from './../../commonreport/commonreport.module';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MaterialModule } from 'features/common/material.module';
import { MatInputModule } from '@angular/material/input';
import { SamplescreensModule } from '../samplescreens/samplescreens.module';
import { MaintenanceComponent } from './maintenance.component';
import { MaintenanceRoutingModule } from './maintenance.routing.module';
import { MachinelistComponent } from './screens/Checklist/Machine List/Machinelist.component';
import { AssettypeComponent } from './screens/Checklist/Asset Type List/Assettype.component';
import { AssetactivityComponent } from './screens/Checklist/Asset Activity List/Assetactivity.component';
import { MachineTypeComponent } from './screens/Checklist/Machine Type List/MachineType.component';
import { EquipmentkeyComponent } from './screens/Checklist/Equipment Key Component List/Equipmentkey.component';
import { AssettypeactivityComponent } from './screens/Checklist/Asset Type Activity/Assettypeactivity.component';
import { keycomponentComponent } from './screens/Checklist/KeyComponents/keycomponent.component';
import { ActivityRegisterViewComponent } from './screens/Register/Activity Register/ActivityRegisterView.component';
import { ScheduleDatewiseComponent } from './screens/Checklist/Schedule Datewise/ScheduleDatewise.component';
import { ActivityComponent } from './screens/Activity/Activity.component';
import { SolutionComponent } from './screens/Solution/Solution.component';
import { ActivityTypeComponent } from './screens/ActivityType/ActivityType.component';
import { MeterComponent } from './screens/Meter/Meter.component';
import { ProblemComponent } from '../production/screens/Problem/Problem.component';
import { AssetTypeComponent } from './screens/AssetType/AssetType.component';
import { AssetTypeMaterialComponent } from './screens/AssetTypeMaterial/AssetTypeMaterial.component';
import { TypeComponentComponent } from './screens/TypeComponent/TypeComponent.component';
@NgModule({
  declarations: [
    MaintenanceComponent,
    MachinelistComponent,
    AssettypeactivityComponent,
    AssetTypeMaterialComponent,
    AssettypeComponent,
    TypeComponentComponent,
    AssetactivityComponent,
    MachineTypeComponent,
    EquipmentkeyComponent,
    AssettypeactivityComponent,
    keycomponentComponent,
    ActivityRegisterViewComponent,
    ScheduleDatewiseComponent,
    ActivityComponent,
    SolutionComponent,
    ActivityTypeComponent,
    MeterComponent,
    ProblemComponent,
    AssetTypeComponent
  ],
  imports: [
    SamplescreensModule,
    CommonModule,
    MaintenanceRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    UicoreModule,
    NgBreModule,
    UifwkUifwkmaterialModule,
    TranslocoRootModule,
    // NgxsModule.forFeature([
    //  EmployeeState]),
    // NgxsFormPluginModule.forRoot(),
    AgGridModule,
    GbgridModule,
    GbdataModule.forRoot(environment),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDvFSDxJXXR1nrdRDIzvp6DRZB9v649My8',
      libraries: ['places', 'geometry', 'drawing']
    }),
    AgmDirectionModule,
    NgxChartsModule,
    GbchartModule,
    EntitylookupModule,
    geolocationModule,
    NgSelectModule,
    MaterialModule,
    MatSortModule,
    MatFormFieldModule,
    MatSelectModule,
    MatButtonModule,
    MatListModule,
    MatTabsModule,
    MatCheckboxModule,
    MatTableModule,
    MatIconModule,
    MatExpansionModule,
    MatDividerModule,
    MatPaginatorModule,
    CommonReportModule,
    MatFormFieldModule,
    MatInputModule,

  ],
  exports: [ 
    MaintenanceComponent,
    MatSortModule,
    MatFormFieldModule,
    MatInputModule,
  ],
})
export class MaintenanceModule {


}
