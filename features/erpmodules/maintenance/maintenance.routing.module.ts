import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MaintenanceComponent } from './maintenance.component';
import { MachinelistComponent } from './screens/Checklist/Machine List/Machinelist.component';
import { AssettypeComponent } from './screens/Checklist/Asset Type List/Assettype.component';
import { AssetactivityComponent } from './screens/Checklist/Asset Activity List/Assetactivity.component';
import { MachineTypeComponent } from './screens/Checklist/Machine Type List/MachineType.component';
import { EquipmentkeyComponent } from './screens/Checklist/Equipment Key Component List/Equipmentkey.component';
import { AssettypeactivityComponent } from './screens/Checklist/Asset Type Activity/Assettypeactivity.component';
import { keycomponentComponent } from './screens/Checklist/KeyComponents/keycomponent.component';
import { ActivityRegisterViewComponent } from './screens/Register/Activity Register/ActivityRegisterView.component';
import { ScheduleDatewiseComponent } from './screens/Checklist/Schedule Datewise/ScheduleDatewise.component';
import { ActivityComponent } from './screens/Activity/Activity.component';
import { SolutionComponent } from './screens/Solution/Solution.component';
import { ActivityTypeComponent } from './screens/ActivityType/ActivityType.component';
import { MeterComponent } from './screens/Meter/Meter.component';
import { ProblemComponent } from '../production/screens/Problem/Problem.component';
import { AssetTypeComponent } from './screens/AssetType/AssetType.component';
import { AssetTypeMaterialComponent } from './screens/AssetTypeMaterial/AssetTypeMaterial.component';
import { TypeComponentComponent } from './screens/TypeComponent/TypeComponent.component';
const routes: Routes = [
  {
    path: 'assettype',
    component: AssetTypeComponent
  },
  {
    path:'typecomponent',
    component:TypeComponentComponent
  },
  {
    path: 'problem',
    component: ProblemComponent
  },

  {
    path: 'AssetTypeMaterial',
    component: AssetTypeMaterialComponent
  },

 
  {
    path: 'meter',
    component: MeterComponent
  },
  {
    path: 'activitytype',
    component: ActivityTypeComponent
  },
  {
    path: 'solution',
    component: SolutionComponent
  },
  {
    path: 'activity',
    component: ActivityComponent
  },
  {
    path: '',
    component: MaintenanceComponent
  },

  { path: 'themestest', loadChildren: () => import(`./../../themes/themes.module`).then(m => m.ThemesModule) },

  {
    path: 'Machinelist',
    children: [
      {
        path: 'list',
        component: MachinelistComponent
      },
    ]
  },

  {
    path: 'Assettype',
    children: [
      {
        path: 'list',
        component: AssettypeComponent
      },
    ]
  },

  {
    path: 'Assetactivity',
    children: [
      {
        path: 'list',
        component: AssetactivityComponent
      },
    ]
  },

  {
    path: 'MachineType',
    children: [
      {
        path: 'list',
        component: MachineTypeComponent
      },
    ]
  },

  {
    path: 'Equipmentkey',
    children: [
      {
        path: 'list',
        component: EquipmentkeyComponent
      },
    ]
  },

  {
    path: 'Assettypeactivity',
    children: [
      {
        path: 'list',
        component: AssettypeactivityComponent
      },
    ]
  },

  {
    path: 'keycomponent',
    children: [
      {
        path: 'list',
        component: keycomponentComponent
      },
    ]
  },

  {
    path: 'activityregister',
    children: [
      {
        path: 'list',
        component: ActivityRegisterViewComponent
      },
    ]
  },

  {
    path: 'scheduledatewise',
    children: [
      {
        path: 'list',
        component: ScheduleDatewiseComponent
      },
    ]
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MaintenanceRoutingModule { }
