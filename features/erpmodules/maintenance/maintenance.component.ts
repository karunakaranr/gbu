import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'goodbooks-maintenance',
  template: `
  `,
  styles: ['ul li a {margin-bottom: 0px; color: black}'],
})
export class MaintenanceComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
