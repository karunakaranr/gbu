import { Injectable } from '@angular/core';
import {GBHttpService} from '../../../../../../libs/gbcommon/src/lib/services/HTTPService/GBHttp.service';

const urls = require('./../../../URLS/urls.json');

@Injectable({
  providedIn: 'root',
})
export class StageDBService{
  endPoint: string = urls.Stage;
  useAPIPagination=true;
  idField:string = "?GcmId="
  constructor(public http: GBHttpService) {
  }

  getall(gcmid){
    const url = this.endPoint + 'SelectList/?firstNumber=1&maxResult=50';
    console.log("URL:",url);
    let criteria = {"SectionCriteriaList":[{"SectionId":0,"AttributesCriteriaList":[{"FieldName":"GcmType.Id","OperationType":5,"FieldValue":gcmid,"InArray":null,"JoinType":2},{"FieldName":"Code","OperationType":2,"FieldValue":"","InArray":null,"JoinType":0}]}]}
    return this.http.httppost(url,criteria);
  }


  getid(id){
    const url = this.endPoint + this.idField + id;
    return this.http.httpget(url);
  }
  
}
