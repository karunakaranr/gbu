import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HrmsComponent } from './hrms.component';
import { EmployeemasterlistComponent } from './screens/employee/employeelist/employeemasterlist.component';
import { EmployeemasterformComponent } from './screens/employee/employeemasterform/employeemasterform/employeemasterform';
import { EmployeeComponent } from './screens/employee/employeetable/employee.component';
import { EmployeeSkillListComponent } from './screens/employee/EmployeeSkill/EmployeeSkillList/EmployeeSkillList.component';
import { PayRevisionListComponent } from './screens/employee/PayRevisionList/PayRevision.component'
import { AdditionDeductionComponent } from './screens/AdditionDeduction/AdditionDeduction.component'
import { VillageListComponent } from './screens/VillageMaster/VillageList/VillageList.component';
import { AppraisalDashBoardComponent } from './screens/AppraisalDashBoard/AppraisalDashBoard.component';
import { leaverequestComponent } from './screens/LeaveRequest/LeaveRequest/leaverequest.component';
import { StageComponent } from './screens/TypeMaster/Stage/stage.component';
import { DailyAttendanceComponent } from './screens/Attendance/Daily Attendance List/DailyAttendance.component';
import { MonthlyAttendanceComponent } from './screens/Attendance/Monthly Attendance/MonthlyAttendance.component';
import { TimeslipComponent } from './screens/Attendance/Timeslip/Timeslip.component';
import { EmployeeVsShiftComponent } from './screens/EmployeeVsShift/EmployeeVsShiftView/EmployeeVsShift.component';
import { LateArrivalComponent } from './screens/Attendance/Late Arrival HTML View/LateArrivalView.component';
import { attendancepunchComponent } from './screens/Punches/AttendancePunch/attendancepunch.component';
import { EmployeedetailComponent } from './screens/employee/EmployeeDetail/employeedetail.component';
import { EarlyDepatureComponent } from './screens/Attendance/Early Depature/EarlyDepature.component';
//import { AppraisalSummarycomponent } from './screens/AppraisalSummary/AppraisalSummary.component';
import { PickupPlaceComponent } from './screens/TypeMaster/PickupPlace/pickupplace.component';
import { DAPointGroupComponent } from './screens/TypeMaster/DAPointGroup/dapointgroup.component';
import { TimeSlipComponent } from './screens/Timeslip/TimeSlip.component';
import { SkillMasterComponent } from './screens/EmployeeDetails/SkillMaster/skillmaster.component';

import { LeaveTypeComponent } from './screens/Leave/LeaveType/leavetype.component';
import { KeyResultAreaComponent } from './screens/KeyResultArea/keyresultarea.component';
import { ActivityRecruitmentProcessComponent } from './screens/Recruitment/ActivityRecruitmentProcess/activityrecruitmentprocess.component';
import { GcmComponent } from './screens/Recruitment/Gcm/gcm.component';

import { SubjectComponent } from './screens/Training/Subject/subject.component';
import { BankComponent } from '../finance/screens/Bank/Bank.component';
import { PunchDetailsComponent } from './screens/PunchDetails/PunchDetails.component';
import { PayGroupComponent } from './screens/TypeMaster/PayGroup/paygroup.component';
import { PayElementComponent } from './screens/PayElement/payelement.component';
import { SettingsComponent } from './screens/AdditionDeductions/Settings/settings.component';
import { GoalComponent } from './screens/Goal/goal.component';

import { NOCListComponent } from './screens/HumanResource/NOCList/noclist.component';
import { CasteCategoryComponent } from './screens/CasteCategory/castecategory.component';
import { ManPowerRequirementComponent } from './screens/ManPowerRequirement/manpowerrequirement.component';
import { EmployeeWorkingComponent } from './screens/EmployeeDetail/EmployeeWorking/EmployeeWorking.component';

import { GratuitySettingsComponent } from './screens/PayRollSettings/GratuitySettings/gratuitysettings.component';
import { SelectionRoundComponent } from './screens/Recruitment/SelectionRound/selectionround.component';
import { SelectionCycleComponent } from './screens/Recruitment/SelectionCycle/selectioncycle.component';
import { MeasureOptionsTypeComponent } from './screens/TypeMaster/MeasureOptionsType/measureoptionstype.component';
import { MeasureOptionsComponent } from './screens/MeasureOption/measureoption.component';
import { PayConfigurationComponent } from './screens/PayConfiguration/PayConfiguration.component';
import { RVLEmployeeMasterComponent } from './screens/RVLEmployeeMaster/RVLEmployeeMaster.component';
import { ShiftMasterComponent } from './screens/AttendanceSetting/ShiftMaster/shiftmaster.component';
import { PayPeriodComponent } from './screens/Punches/PayPeriod/payperiod.component';
import { ShiftPatterncomponent } from './screens/ShiftPattern/shiftpattern.component';
import { EmployeeQualificationComponent } from './screens/EmployeeQualification/EmployeeQualification.component';
import { PerformanceMeasureComponent } from './screens/PerformanceMeasure/PerformanceMeasure.component';
import { RecruitmentComponent } from './screens/Recruitment/RecruitmentWorkType/Recruitment.component';
import { OULevelSettingComponent } from './screens/OULevelSetting/oulevelsetting.component';
import { EmployeeExperienceComponent } from './screens/EmployeeExperience/EmployeeExperience.component';

import { GenerateEmptyDeclarationComponent } from './screens/GenerateEmptyDeclaration/generateemptydeclaration.component';
import { JobProfileLibraryComponent } from './screens/JobProfileLibrary/JobProfileLibrary.component';
import { EmployeeSalaryComponent } from './screens/EmployeeSalary/employeesalary.component';
import { EmployeeNomineeComponent } from './screens/EmployeeNominee/EmployeeNominee.component';
import { PayTaxSettingComponent } from './screens/PayTaxSetting/PayTaxSetting.component';
import { EmployeeReferenceComponent } from './screens/EmployeeReference/employeereference.component';
import { EmployeeSkillComponent } from './screens/EmployeeSkill/employeeskill.component';
import { PolicyComponent } from './screens/Policy/policy.component';
const routes: Routes = [
  {
    path: 'employeeskill',
    component: EmployeeSkillComponent
    
  },
  {
    path: 'jobprofile',
    component: JobProfileLibraryComponent

  },
  {
    path: 'policy',
    component: PolicyComponent

  },
  {
    path:'employeereference',
    component:EmployeeReferenceComponent
  },
  {
    path: 'employeesalary',
    component: EmployeeSalaryComponent
  },
  {
    path:'EmployeeNominee',
    component:EmployeeNomineeComponent
  },
  {
    path:'PayTaxSetting',
    component:PayTaxSettingComponent
  },
  {
    path: 'generateemptydeclaration',
    component: GenerateEmptyDeclarationComponent

  },
  {
    path: 'employeeskill',
    component: EmployeeSkillComponent

  },
  {
    path: 'employeeexperience',
    component: EmployeeExperienceComponent
  },
  {
    path: 'shiftpattern',
    component: ShiftPatterncomponent
  },
  {
    path: 'oulevelsetting',
    component: OULevelSettingComponent
  },
  {
    path: 'Recruitment',
    component: RecruitmentComponent
  },
  {
    path: 'PerformanceMeasure',
    component: PerformanceMeasureComponent
  },
  {
    path: 'EmployeeQualification',
    component: EmployeeQualificationComponent
  },
  {
    path: 'payperiod',
    component: PayPeriodComponent
  },
  {
    path: 'employeemaster',
    component: RVLEmployeeMasterComponent
  },
  {
    path: 'payconfiguration',
    component: PayConfigurationComponent
  },
  {
    path: 'SelectionRound',
    component: SelectionRoundComponent
  },
  {
    path: 'measureoption',
    component: MeasureOptionsComponent
  },
  {
    path: 'GratuitySettings',
    component: GratuitySettingsComponent
  },
  {
    path: 'employeeworking',
    component: EmployeeWorkingComponent
  },
  {
    path: 'ManPowerRequirement',
    component: ManPowerRequirementComponent
  },
  //   {
  //     path:'SelectionRound',
  //  component:SelectionRoundComponent
  //    },
  //    {
  //  path:'GratuitySettings',
  //  component:GratuitySettingsComponent
  //    },
  {
    path: 'NOCList',
    component: NOCListComponent
  },
  {
    path: 'castecategory',
    component: CasteCategoryComponent
  },
  {
    path: 'ShiftMaster',
    component: ShiftMasterComponent

  },
  {
    path: 'Settings',
    component: SettingsComponent
  },
  {
    path: 'PayElement',
    component: PayElementComponent
  },
  {
    path: 'PayGroup',
    component: PayGroupComponent
  },

  {
    path: 'Subject',
    component: SubjectComponent

  },

  {
    path: 'Goal',
    component: GoalComponent

  },

  {
    path: 'Gcm',
    component: GcmComponent
  },
  {
    path: 'ActivityRecruitmentProcess',
    component: ActivityRecruitmentProcessComponent
  },


  {
    path: 'KeyResultArea',
    component: KeyResultAreaComponent
  },
  {
    path: 'LeaveType',
    component: LeaveTypeComponent
  },

  {
    path: 'skillmaster',
    component: SkillMasterComponent

  },

  {
    path: 'dapointgroup',
    component: DAPointGroupComponent

  },
  {
    path: 'timeslip',
    component: TimeSlipComponent
  },
  {
    path: 'pickupplace',
    component: PickupPlaceComponent

  },
  {
    path: 'measureoptionstype',
    component: MeasureOptionsTypeComponent

  },
  {
    path: '',
    component: HrmsComponent
  },
  {
    path: 'stage/:id',
    component: GcmComponent
  },
  {
    path: 'Gcm/:id',
    component: GcmComponent
  },
  {
    path: 'EarlyDepature',
    children: [
      {
        path: 'list',
        component: EarlyDepatureComponent
      },
    ]
  },

  {
    path: 'emp',
    component: EmployeeComponent
  },
  {
    path: 'selectioncycle',
    component: SelectionCycleComponent
  },
  {
    path: 'employeedetail',
    component: EmployeedetailComponent
  },
  {
    path: 'attendancepunch',
    component: attendancepunchComponent
  },
  {
    path: 'appraisaldashboard',
    component: AppraisalDashBoardComponent
  },
  {
    path: 'payrevision',
    children: [
      {
        path: 'list',
        component: PayRevisionListComponent
      },
    ]
  },
  {
    path: 'additiondeduction',
    children: [
      {
        path: 'list',
        component: AdditionDeductionComponent
      },
    ]
  },
  {
    path: 'employee',
    children: [
      {
        path: 'list',
        component: EmployeemasterlistComponent
      },
      {
        path: 'id',
        component: EmployeemasterformComponent
      },
      {
        path: '',
        component: EmployeemasterformComponent
      },
      // {
      //   path: 'appraisalsummary',
      //   component: AppraisalSummarycomponent
      // },
      {
        path: 'skill',
        children: [
          {
            path: 'list',
            component: EmployeeSkillListComponent
          },
        ]
      },
      {
        path: 'leaverequest',
        children: [
          {
            path: 'list',
            component: leaverequestComponent
          },
        ]
      },
    ]
  },
  {
    path: 'villagelist',
    children: [
      {
        path: 'list',
        component: VillageListComponent
      },
    ]
  },

  {
    path: 'Dailyattendance',
    children: [
      {
        path: 'list',
        component: DailyAttendanceComponent
      },
    ]
  },

  {
    path: 'MonthlyAttendanc',
    children: [
      {
        path: 'list',
        component: MonthlyAttendanceComponent
      },
    ]
  },

  {
    path: 'Timeslip',
    children: [
      {
        path: 'list',
        component: TimeslipComponent
      },
    ]
  },
  {
    path: 'EmployeeVsShift',
    children: [
      {
        path: 'list',
        component: EmployeeVsShiftComponent
      },
    ]
  },
  {
    path: 'latearrivalview',
    children: [
      {
        path: 'list',
        component: LateArrivalComponent
      },
    ]
  }, {
    path: 'punchdetailsview',
    children: [
      {
        path: 'list',
        component: PunchDetailsComponent
      },
    ]
  },
  { path: 'themestest', loadChildren: () => import(`./../../themes/themes.module`).then(m => m.ThemesModule) },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HrmsRoutingModule { }
