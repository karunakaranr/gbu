


// // import { Component } from '@angular/core';
// // import { FormBuilder } from '@angular/forms';
// // import { ActivatedRoute, Router } from '@angular/router';
// // import { GBHttpService } from 'libs/gbcommon/src';
// // import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
// // import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
// // import { Store } from '@ngxs/store';
// // import { KeyResultAreaService } from '../../services/Appraisal/KeyResultArea/KeyResultArea.service';
// // import { IKeyResultArea } from '../../models/IKeyResultArea';
// // import { ButtonVisibility } from 'features/layout/store/layout.actions';
// // const urls = require('./../../URLS/urls.json');


// // @Component({
// //   selector: "app-KeySelectorArea",
// //   templateUrl: 'keyresultarea.component.html',
// //   styleUrls: ['keyresultarea.component.html'],
// //   providers: [
// //     { provide: 'IdField', useValue: 'KRAId'},
// //     { provide: 'url', useValue: urls.KRA },
// //     { provide: 'DataService', useClass: KeyResultAreaService },
// //     { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
// //     { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
// //   ]
// // })
// // export class KeyResultAreaComponent extends GBBaseDataPageComponentWN<IKeyResultArea> {
// //   title : string ="KeyResultArea"
// //   form: GBDataFormGroupWN<IKeyResultArea> = new GBDataFormGroupWN(this.gbps.gbhttp.http,"KeyResultArea", {}, this.gbps.dataService);
// //   thisConstructor() {
// //     let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
// //     if(arrayOfValues != null){
// //       this.Getselectedid(arrayOfValues.KRAId)
// //       this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
// //     } else {
// //       this.store.dispatch(new ButtonVisibility("Forms"))
// //     }
// //    }
 
// //   public Getselectedid(SelectedPicklistData: string): void {
// //     if (SelectedPicklistData) {
// //         this.gbps.dataService.getData(SelectedPicklistData).subscribe(KeyResultArea => {
// //             this.form.patchValue(KeyResultArea);
// //           })
// //       }
// //     }
  
  
// //     public Getselectedids(SelectedPicklistDatas: any): void {
// //       this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
// //     }
// //   }
  
  
// //   export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IKeyResultArea> {
// //     const dbds: GBBaseDBDataService<IKeyResultArea> = new GBBaseDBDataService<IKeyResultArea>(http);
// //     dbds.endPoint = url;
// //     return dbds;
// //   }
// //   export function getThisPageService(store: Store, dataService: GBBaseDataService<IKeyResultArea>, dbDataService: GBBaseDBDataService<IKeyResultArea>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IKeyResultArea> {
// //     return new GBDataPageService<IKeyResultArea>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
// //   }
import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { IKeyResultArea } from '../../models/IKeyResultArea';
import { KeyResultAreaService } from '../../services/Appraisal/KeyResultArea/KeyResultArea.service';
import { HRMSURLS } from './../../URLS/urls';

import * as KeyResultAreaJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/KeyResultArea.json'



import { ButtonVisibility } from 'features/layout/store/layout.actions';

@Component({
  selector: 'app-KeyResultArea',
  templateUrl:'./keyresultarea.component.html',
  styleUrls: ['./keyresultarea.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'KRAId' },
    { provide: 'url', useValue:HRMSURLS.KRA },
    { provide: 'DataService', useClass: KeyResultAreaService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class KeyResultAreaComponent extends GBBaseDataPageComponentWN<IKeyResultArea> {
  title: string = "KeyResultArea"
  KeyResultAreaJSON = KeyResultAreaJSON;

  form: GBDataFormGroupWN<IKeyResultArea> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "KeyResultArea", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.KeyResultAreaValue(arrayOfValues.KRAId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }
  public KeyResultAreaValue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(KeyResultArea => {
        this.form.patchValue(KeyResultArea);
      })
    }
  }
  public KeyResultAreaNot(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}
export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IKeyResultArea> {
  const dbds: GBBaseDBDataService<IKeyResultArea> = new GBBaseDBDataService<IKeyResultArea>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IKeyResultArea>, dbDataService: GBBaseDBDataService<IKeyResultArea>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IKeyResultArea> {
  return new GBDataPageService<IKeyResultArea>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
