import { Component, OnInit } from '@angular/core';
import { DashboardFunction } from 'libs/gbcommon/src/lib/services/DashboardFunction/dashboard.service';
import { IEmployeeHierarchy } from '../../models/IEmployeeHierarchy';
import { SharedService } from 'features/commonreport/service/datapassing.service';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { Observable } from 'rxjs';
import { Select } from '@ngxs/store';
import { ICriteriaDTODetail } from 'features/erpmodules/samplescreens/models/IFilterDetail';
import OrgChart from '@balkangraph/orgchart.js';

@Component({
  selector: 'app-employeehierarchy',
  templateUrl: './employeehierarchy.component.html',
  styleUrls: ['./employeehierarchy.component.scss'],
})
export class EmployeeHierarchyComponent implements OnInit {
  @Select(ReportState.Sourcecriteriadrilldown) filtercriteria$: Observable<any>;

  EmployeeHierarchyData = [];
  EmployeeHierarchyFlattenData: any;
  FilterData: ICriteriaDTODetail[];
  Orientation: any = OrgChart.orientation.left;
  OrientationBtn: boolean = true;

  constructor(
    public sharedService: SharedService,
    public commonfunction: DashboardFunction
  ) {}

  ngOnInit(): void {
    this.sharedService.getRowInfo().subscribe((data) => {
      this.EmployeeHierarchyData = data;

      if (
        this.EmployeeHierarchyData != undefined &&
        this.EmployeeHierarchyData.length > 0
      ) {
        const change = 0;
        this.drawHierarchy(change);
      }
    });
  }

  private drawHierarchy(change) {
    OrgChart.templates.olivia.field_2 =
      '<text class="olivia-f2" style="font-size: 12px;" x="240" y="20" text-anchor="end">{val}</text>';
    const tree = document.getElementById('tree');
    if (tree) {
      var chart = new OrgChart(tree, {
        //nodeMouseClick: OrgChart.action.edit, // edit mode
        //nodeMouseClick: OrgChart.action.details, // details mode
        nodeMouseClick: OrgChart.action.none, // prevent opening edit form
        template: 'olivia',
        layout: OrgChart.mixed,
        orientation: this.Orientation,
        enableSearch: false,
        showYScroll: OrgChart.scroll.visible,
        showXScroll: OrgChart.scroll.visible,
        mouseScrool: OrgChart.action.scroll,
        nodeBinding: {
          field_0: 'Name',
          field_1: 'Designation',
          field_2: 'Code',
          img_0: 'Image',
        },
        toolbar: {
          fullScreen: true,
          zoom: true,
          fit: true,
          expandAll: true,
        },
      });

      chart.on('click', function (sender, args) {
        let index = newData.indexOf(
          newData.find(function (el, index) {
            return el['id'] === args.node.id;
          })
        );

        alert(
          `Code : ${newData[index].Code}\nName : ${newData[index].Name}\nDesignation : ${newData[index].Designation}\nDepartment : ${newData[index].Department}\nReporting To Code : ${newData[index].ParentCode}\nReporting To Name : ${newData[index].ParentName}`
        );

        console.log('index', index);

        console.log(args);
        console.log(sender);
        // sender.editUI.show(args.node.id, false);
        // sender.editUI.show(args.node.id, true);  details mode
        return false; //to cancel the click event
      });

      if (change != 1) {
        this.flattenArrayExample(this.EmployeeHierarchyData);
      }
      const newData = this.EmployeeHierarchyFlattenData;

      chart.load(this.EmployeeHierarchyFlattenData);
    }
  }

  changeOrientation() {
    const change = 1;
    console.log(
      'EmployeeHierarchyFlattenData Data : ',
      this.EmployeeHierarchyFlattenData
    );
    this.Orientation =
      this.Orientation === OrgChart.orientation.left
        ? OrgChart.orientation.top
        : OrgChart.orientation.left;
    this.OrientationBtn = !this.OrientationBtn;
    this.drawHierarchy(change);
  }

  flattenArray(arr: any[]): any[] {
    return arr.reduce((acc, val) => {
      if (Array.isArray(val.children) && val.children.length > 0) {
        return acc.concat([val], this.flattenArray(val.children));
      } else {
        return acc.concat(val);
      }
    }, []);
  }

  // Hierarchy array to Flatten array for OrgChart

  flattenArrayExample(yourArray: any[]) {
    const flattenedArray = this.flattenArray(yourArray);
    this.EmployeeHierarchyFlattenData = flattenedArray;
    this.modifyArrayOfObjects(this.EmployeeHierarchyFlattenData);
  }

  // modify Array Of Objects for OrgChart

  private modifyArrayOfObjects(array: any[]) {
    return array.map((obj) => {
      // Deleting the 'children' key
      delete obj.children;

      // Renaming the 'Id' key to 'id'
      obj.id = obj.Id;

      obj.pid = obj.ParentId;
      // obj.EmployeePhoto = 'assets/EmployeeHierarchy/Head.svg';

      obj.Image = 'assets/EmployeeHierarchy/Head.svg';

      return obj;
    });
  }
}
