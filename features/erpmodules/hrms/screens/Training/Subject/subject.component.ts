import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { HRMSURLS } from './../../../URLS/urls';
import { ISubject } from 'features/erpmodules/hrms/models/ISubject';
import { SubjectService } from 'features/erpmodules/hrms/services/Training/Subject/Subject.service';
import * as SubjectJSON from './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/Subject.json'



import { ButtonVisibility } from 'features/layout/store/layout.actions';

@Component({
  selector: 'app-Subject',
  templateUrl:'./subject.component.html',
  styleUrls: ['./subject.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'SubjectId' },
    { provide: 'url', useValue:HRMSURLS.Subject },
    { provide: 'DataService', useClass: SubjectService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class SubjectComponent extends GBBaseDataPageComponentWN<ISubject> {
  title: string = "Subject"
  SubjectJSON = SubjectJSON;

  form: GBDataFormGroupWN<ISubject> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "Subject", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.SubjectValue(arrayOfValues.SubjectId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }
  public SubjectValue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(Subject => {
        this.form.patchValue(Subject);
      })
    }
  }
  public SubjectNot(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}
export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<ISubject> {
  const dbds: GBBaseDBDataService<ISubject> = new GBBaseDBDataService<ISubject>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<ISubject>, dbDataService: GBBaseDBDataService<ISubject>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<ISubject> {
  return new GBDataPageService<ISubject>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
