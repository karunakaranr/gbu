import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ShiftMasterService } from 'features/erpmodules/hrms/services/AttendanceSetting/ShiftMaster/ShiftMaster.service';
import { IShiftMaster } from 'features/erpmodules/hrms/models/IShiftMaster';
import { HRMSURLS } from './../../../URLS/urls';
import * as ShiftMasterJSON from './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/ShiftMaster.json'
import { ButtonVisibility } from 'features/layout/store/layout.actions';

@Component({
  selector: 'app-ShiftMaster',
  templateUrl: './shiftmaster.component.html',
  styleUrls: ['./shiftmaster.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'ShiftId' },
    { provide: 'url', useValue: HRMSURLS.ShiftMaster },
    { provide: 'DataService', useClass: ShiftMasterService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class ShiftMasterComponent extends GBBaseDataPageComponentWN<IShiftMaster> {
  title: string = "ShiftMaster"
  ShiftMasterJSON = ShiftMasterJSON;
  EndTime: any;
  firstTime: number;
  RemainTime: number;
  StartTime: number;
  ReplaceStartTime: number;

  form: GBDataFormGroupWN<IShiftMaster> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "ShiftMaster", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))

    if (arrayOfValues != null) {
      this.ShiftMasterValue(arrayOfValues.ShiftId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }
  public settingPatchValue(): void {

    if (this.form.get('ShiftIsBreakApplicable').value == "1") {
      this.form.get('ShiftBreakMinutes').patchValue('');
      this.form.get('ShiftWorkingMinutes').patchValue('');
    }
    if(this.form.get('ShiftIsBreakApplicable').value == "0") 
    {
      this.form.get('ShiftBreakMinutes').patchValue('0');
      this.form.get('ShiftWorkingMinutes').patchValue('0');
    }
  }
  public Changevalue(Input) {
    this.form.get("FirstHalfStartTimeExtraField").patchValue(this.form.get('TotalShiftStartTime').value)
  }

  public Valuechanging(Input) {
    this.form.get("FirstHalfEndTimeExtraField").patchValue(this.form.get('TotalShiftEndTime').value)
  }
  public Changevalueout(Input) {
    console.log("enttime:", Input)
    this.form.get("ShiftSecondHalfStartTimeSelfName").patchValue(this.form.get('FirstHalfEndTimeNewName').value)
    console.log("LastTime", this.form.get('ShiftSecondHalfStartTimeSelfName').value)
  }

  public TimeCalculator(SelectedPicklistDatas: any) {
    console.log("TimeFirst :", SelectedPicklistDatas)
    this.form.get('ShiftStartTime').patchValue(SelectedPicklistDatas.SelectedData)
    this.firstTime = SelectedPicklistDatas.SelectedData
    console.log("firstTime++++=", this.firstTime)
  }
  public EndTimeCalculator(SelectedPicklistDatas: any) {
    this.form.get('ShiftEndTime').patchValue(SelectedPicklistDatas.SelectedData)
    console.log("EndTime :", SelectedPicklistDatas)
    this.EndTime = SelectedPicklistDatas.SelectedData
    console.log("firstTime++++=", this.EndTime)
    this.RemainTime
    var ans = this.EndTime - this.firstTime
    console.log("Ans", ans)
    this.form.get('ShiftShiftMinutes').patchValue(ans)
    this.form.get('ShiftWorkingMinutes').patchValue(ans)
  }
  public FirsthalfEndTime(SelectedPicklistDatas: any) {
    this.form.get('ShiftFirstHalfEndTime').patchValue(SelectedPicklistDatas.SelectedData)
    this.form.get('ShiftSecondHalfStartTime').patchValue(SelectedPicklistDatas.SelectedData)
  }
  public ShiftMasterValue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(ShiftMaster => {
        this.form.patchValue(ShiftMaster);
      })
    }
  }

  public ShiftMasterNot(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IShiftMaster> {
  const dbds: GBBaseDBDataService<IShiftMaster> = new GBBaseDBDataService<IShiftMaster>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IShiftMaster>, dbDataService: GBBaseDBDataService<IShiftMaster>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IShiftMaster> {
  return new GBDataPageService<IShiftMaster>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}

