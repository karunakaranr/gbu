import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { IGoal } from '../../models/IGoal';
import { GoalService } from '../../services/Appraisal/KeyResultArea/Goal/Goal.service';
import { HRMSURLS } from './../../URLS/urls';
import * as GoalJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/Goal.json'
import { ButtonVisibility } from 'features/layout/store/layout.actions';

@Component({
  selector: 'app-Goal',
  templateUrl:'./goal.component.html',
  styleUrls: ['./goal.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'GoalId' },
    { provide: 'url', useValue:HRMSURLS.Goal }, 
    { provide: 'DataService', useClass: GoalService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class GoalComponent extends GBBaseDataPageComponentWN<IGoal> {
  title: string = "Goal"
  GoalJSON = GoalJSON;   

  form: GBDataFormGroupWN<IGoal> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "Goal", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.GoalValue(arrayOfValues.GoalId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }
  public GoalValue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(Goal => {
        this.form.patchValue(Goal);
      })
    }
  }
  public GoalNot(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}
export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IGoal> {
  const dbds: GBBaseDBDataService<IGoal> = new GBBaseDBDataService<IGoal>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IGoal>, dbDataService: GBBaseDBDataService<IGoal>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IGoal> {
  return new GBDataPageService<IGoal>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
