import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { EmployeeExperienceService } from '../../services/EmployeeExperience/EmployeeExperience.service';
import { IEmployeeExperience } from '../../models/IEmployeeExperience';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as EmployeeExperienceJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/EmployeeExperience.json'
import { HRMSURLS } from '../../URLS/urls';


@Component({
  selector: 'app-EmployeeExperience',
  templateUrl: './EmployeeExperience.component.html',
  styleUrls: ['./EmployeeExperience.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'EmployeeId'},
    { provide: 'url', useValue: HRMSURLS.EmployeeExperience },
    { provide: 'saveurl', useValue: HRMSURLS.EmployeeExperienceSave },
    { provide: 'deleteurl', useValue: HRMSURLS.EmployeeExperienceDelete },
    { provide: 'DataService', useClass: EmployeeExperienceService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url','saveurl','deleteurl','IdField'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class EmployeeExperienceComponent extends GBBaseDataPageComponentWN<IEmployeeExperience > {
  title: string = 'EmployeeExperience'
  EmployeeExperienceJSON = EmployeeExperienceJSON;


  form: GBDataFormGroupWN<IEmployeeExperience > = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'EmployeeExperience', {}, this.gbps.dataService);
  thisConstructor() {
    console.log("HRMSURLS.Experience:",HRMSURLS)
    console.log("HRMSURLS.EmployeeExperience",HRMSURLS.EmployeeExperience)
    console.log("HRMSURLS.EmployeeExperienceSave",HRMSURLS.EmployeeExperienceSave)
    console.log("HRMSURLS.EmployeeExperienceDelete ",HRMSURLS.EmployeeExperienceDelete )
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.EmployeeExperienceFillFunction(arrayOfValues.EmployeeId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }
 

  public EmployeeExperienceFillFunction(SelectedPicklistData: string): void {
    console.log("SelectedPicklistData",SelectedPicklistData)
    if (SelectedPicklistData) {
      this.gbps.dataService.postData(SelectedPicklistData).subscribe(EmployeeWorking => {
        console.log("EmployeeWorkingExperience**",EmployeeWorking)
        this.form.get('EmployeeId').patchValue(EmployeeWorking[0].EmployeeId)
        
        var FromMonthYear = EmployeeWorking[0].EmployeeExperienceFromMonthYear
        console.log("FromMonthYear",FromMonthYear)
        var FromSplit = FromMonthYear.split("-")
        
        var ToMonthYear = EmployeeWorking[0].EmployeeExperienceToMonthYear
        console.log("ToMonthYear",ToMonthYear)
        // this.form.get('EmployeeCode').patchValue(EmployeeWorking[0].EmployeeCode)
        // this.form.get('EmployeeName').patchValue(EmployeeWorking[0].EmployeeName)
        this.form.get('EmployeeExperienceDetailArray').patchValue(EmployeeWorking)
      })
    }
  }


  public PicklistPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string, saveurl: string, deleteurl: string, IdField: string): GBBaseDBDataService<IEmployeeExperience > {
  const dbds: GBBaseDBDataService<IEmployeeExperience > = new GBBaseDBDataService<IEmployeeExperience >(http);
  dbds.endPoint = url;
  dbds.saveurl = saveurl;
  dbds.deleteurl = deleteurl;
  dbds.IdField = IdField
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IEmployeeExperience >, dbDataService: GBBaseDBDataService<IEmployeeExperience >, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IEmployeeExperience > {
  return new GBDataPageService<IEmployeeExperience >(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
