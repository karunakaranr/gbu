import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ISelectionRound } from 'features/erpmodules/hrms/models/ISelectionRound';
import { SelectionRoundService } from 'features/erpmodules/hrms/services/Recruitment/SectionRound/sectionround.service';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { HRMSURLS } from 'features/erpmodules/hrms/URLS/urls';
import * as SelectionRoundJSON from './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/SelectionRound.json'

@Component({
  selector: 'app-SelectionRound',
  templateUrl: 'SelectionRound.component.html',
  styleUrls: ['SelectionRound.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'SelectionRoundId' },
    { provide: 'url', useValue:HRMSURLS.SelectionRound },
    { provide: 'DataService', useClass: SelectionRoundService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class SelectionRoundComponent extends GBBaseDataPageComponentWN<ISelectionRound> {
  title: string = "SelectionRound"
  SelectionRoundJSON = SelectionRoundJSON;

  form: GBDataFormGroupWN<ISelectionRound> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "SelectionRound", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.SelectionRoundValue(arrayOfValues.SelectionRoundId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }
  public SelectionRoundValue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(SelectionRound => {
        this.form.patchValue(SelectionRound);
      })
    }
  }
  public SelectionRoundNot(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}
export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<ISelectionRound> {
  const dbds: GBBaseDBDataService<ISelectionRound> = new GBBaseDBDataService<ISelectionRound>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<ISelectionRound>, dbDataService: GBBaseDBDataService<ISelectionRound>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<ISelectionRound> {
  return new GBDataPageService<ISelectionRound>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
