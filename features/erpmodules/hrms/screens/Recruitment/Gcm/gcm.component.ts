import { Component, EventEmitter, Output } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Select, Store } from '@ngxs/store';
import { IGcm } from 'features/erpmodules/hrms/models/IGcm';
import { GcmService } from 'features/erpmodules/hrms/services/Recruitment/Gcm/Gcm.service';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { LayoutState } from 'features/layout/store/layout.state';
import { Observable } from 'rxjs';
import * as GCMJSON from './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/Gcm.json'
const urls = require('./../../../URLS/urls.json');
@Component({
    selector: "app-Gcm",
    templateUrl:'gcm.component.html',
    styleUrls: ['gcm.component.scss'],
    providers: [
      { provide: 'IdField', useValue: 'GcmId'},
      { provide: 'url', useValue: urls.Stage },
      { provide: 'DataService', useClass: GcmService },
      { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
      { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
    ]
  })
  export class GcmComponent extends GBBaseDataPageComponentWN<IGcm> {
    title : string = "Gcm"


    @Select(LayoutState.GCMTypeId) GCMTypeId$: Observable<any>;
    GCMJSON = GCMJSON;  
    GCMTypeId:number
    @Output() Onselectdata: EventEmitter<any> = new EventEmitter<string>();


    form: GBDataFormGroupWN<IGcm> = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'Gcm', {}, this.gbps.dataService);
    ngOnInit(): void {
      this.GCMTypeId$.subscribe((gcmtypeid:number)=>{
        this.GCMTypeId = gcmtypeid;
    })
      this.thisConstructor();
      this.subscribeToFormChanges();
    }

    thisConstructor() {
      let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
      if (arrayOfValues != null) {
        this.GCMFillFunction(arrayOfValues.GcmId)
        this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
      } else {
        this.GCMFillFunction(-1)
        this.store.dispatch(new ButtonVisibility("Forms"))
      }
    }

    private subscribeToFormChanges(): void {
      this.form.valueChanges.subscribe(() => {
        this.Onselectdata.emit(this.form.value)
      });
    }

    public GCMFillFunction(SelectedPicklistData: number): void {
      if (SelectedPicklistData) {
        this.gbps.dataService.getData(SelectedPicklistData).subscribe(GCM => {          
          if(SelectedPicklistData == -1){
            if(this.GCMTypeId != -1){
              this.form.get('GcmTypeId').patchValue(this.GCMTypeId);
              this.form.get('GcmTypeCode').patchValue('None');
            }
          } else {
            this.form.patchValue(GCM);
          }
        })
      }
    }


    public GCMPatchValue(SelectedPicklistDatas: any): void {
      this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
      console.log("GCM:",this.form.value)
    }
  }


  export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IGcm> {
    const dbds: GBBaseDBDataService<IGcm> = new GBBaseDBDataService<IGcm>(http);
    dbds.endPoint = url;
    return dbds;
  }
  export function getThisPageService(store: Store, dataService: GBBaseDataService<IGcm>, dbDataService: GBBaseDBDataService<IGcm>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IGcm> {
    return new GBDataPageService<IGcm>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
  }