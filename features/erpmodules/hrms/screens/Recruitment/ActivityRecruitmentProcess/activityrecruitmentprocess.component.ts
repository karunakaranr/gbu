import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { IActivityRecruitmentProcess } from 'features/erpmodules/hrms/models/IActivityRecruitmentProcess';
import { HRMSURLS } from './../../../URLS/urls';
import { ActivityRecruitmentProcessService } from 'features/erpmodules/hrms/services/Recruitment/ActivityRecruitmentProcess.service';
import * as ActivityRecruitmentProcessJSON from './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/ActivityRecruitmentProcess.json'
import { ButtonVisibility } from 'features/layout/store/layout.actions';

@Component({
  selector: 'app-ActivityRecruitmentProcess',
  templateUrl:'./activityrecruitmentprocess.component.html',
  styleUrls: ['./activityrecruitmentprocess.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'ActivityId' },
    { provide: 'url', useValue:HRMSURLS.ActivityRecruitmentProcess },
    { provide: 'DataService', useClass: ActivityRecruitmentProcessService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class ActivityRecruitmentProcessComponent extends GBBaseDataPageComponentWN<IActivityRecruitmentProcess> {
  title: string = "ActivityRecruitmentProcess"
  ActivityRecruitmentProcessJSON = ActivityRecruitmentProcessJSON;

  form: GBDataFormGroupWN<IActivityRecruitmentProcess> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "ActivityRecruitmentProcess", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.ActivityRecruitmentProcessValue(arrayOfValues.ActivityId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }
  public ActivityRecruitmentProcessValue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(ActivityRecruitmentProcess => {
        this.form.patchValue(ActivityRecruitmentProcess);
      })
    }
  }
  public ActivityRecruitmentProcessNot(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}
export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IActivityRecruitmentProcess> {
  const dbds: GBBaseDBDataService<IActivityRecruitmentProcess> = new GBBaseDBDataService<IActivityRecruitmentProcess>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IActivityRecruitmentProcess>, dbDataService: GBBaseDBDataService<IActivityRecruitmentProcess>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IActivityRecruitmentProcess> {
  return new GBDataPageService<IActivityRecruitmentProcess>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
