import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Select } from '@ngxs/store';
import { Employees } from 'features/erpmodules/hrms/models/IEmployee';
import { EmployeeService } from 'features/erpmodules/hrms/services/employeemaster/employeemaster.service';
import { LayoutState } from 'features/layout/store/layout.state';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss']
})
export class EmployeeComponent implements OnInit {
  code: boolean = true;
  public displayedColumns: string[] = ['Code', 'Name', 'DepartmentName', 'PrimaryEMailId'];
  public dataSource = new MatTableDataSource<Employees>();
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  constructor(public service: EmployeeService) {
  }
  @Select(LayoutState.ThemeClass) themeClass$: Observable<string>;

  ngOnInit() {
    this.employeelist();
  }

  public employeelist = () => {
    this.service.getAll().
      subscribe(res => {
        this.dataSource.data = res as Employees[];
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      })
  }

  public doFilter = (value: string) => {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }

  public changeCheckevent(event) {
    this.code = event.target.checked;
  }
  
}

