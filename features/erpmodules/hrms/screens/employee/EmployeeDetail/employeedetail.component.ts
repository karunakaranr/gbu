import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Select, Store } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';


@Component({
  selector: 'app-employeedetail',
  templateUrl: './employeedetail.component.html',
  styleUrls: ['./employeedetail.component.scss'],
})
export class EmployeedetailComponent implements OnInit {
  @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
  public rowData = [];  
  
  constructor(public store: Store) { }
  ngOnInit(): void {
    this.employeedetailfun();
  }
  
  public employeedetailfun() {
    this.rowdatacommon$.subscribe(data => {
      this.rowData = data;
      console.log(" Employee Detail :", this.rowData)
    });
  }

}