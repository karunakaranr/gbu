import { Component, OnInit } from '@angular/core';
import { PayRevisionList } from '../../../services/PayRevision/PayRevision.service';
import { DatePipe } from '@angular/common';
import { NavigationExtras, Router } from '@angular/router'; 
import {Inject, LOCALE_ID } from '@angular/core';

@Component({
  selector: 'app-PayRevision',
  templateUrl: './PayRevision.component.html',
  styleUrls: ['./PayRevision.component.scss']
})

export class PayRevisionListComponent implements OnInit {
  title = 'Pay Revision List'
  public rowData = []
  public columnData = []
  public defaultColDef = []
  public TotalC2C;
  public TotalCostPerHour;
  public TotalRatePerHour;
  

  constructor(public serivce: PayRevisionList, private router: Router,  @Inject(LOCALE_ID) public locale: string) { }
  ngOnInit(): void {

    this.payrevisionlist();
  }
  selectedcell(data) {
    const queryParams: any = {};
    queryParams.myArray = JSON.stringify(data);
    const navigationExtras: NavigationExtras = {
      queryParams
    };
    this.router.navigate(["hrms/employee/id"], navigationExtras);

  }
  public payrevisionlist() {
    this.serivce.payrevisionlistview().subscribe(menudetails => {
      let servicejsondetail = menudetails
      this.serivce.Rowservice(servicejsondetail).subscribe(res => {
        this.rowData = res.ReportDetail;
        this.TotalC2C=0;
        this.TotalCostPerHour=0;
        this.TotalRatePerHour=0;
        let json = this.rowData;
        var finalizedArray = []
        for (let data of this.rowData) {
          this.TotalC2C=this.TotalC2C+data.C2C;
          this.TotalCostPerHour=this.TotalCostPerHour+data.CostPerHour;
          this.TotalRatePerHour=this.TotalRatePerHour+data.RatePerHour;
        }
        json.map((row,index) => {
          finalizedArray.push({
            sno:index,
            empid: row['EmployeeId'],
            empcode: row['EmployeeCode'],
            empname: row['EmployeeName'],
            revno: row['PayRevisionPayRevisionNumber'],
            revdate: row['PayRevisionRevisionDate'],
            refno: row['PayRevisionPayReferenceNumber'],
            refdate: row['PayRevisionReferenceDate'],
            natval: row['PayRevisionNatureValue'],
            c2c: row['C2C'],
            cph: row['CostPerHour'],
            rph: row['RatePerHour']
          })
        })
        const final = {};
        finalizedArray.forEach(detail => {
          final[detail.empname] = {
            accounts :{},
            empid:detail.empid,
            revno: detail.empcode,
            revdate: detail.empname,
            refno:0,
            refdate: "",
            natval: "",
            c2c:0,
            cph:0,
            rph:0,
            ...final[detail.empname], 
          }
          final[detail.empname].accounts[detail.sno] = {
            empid:detail.empid,
            revno: detail.revno,
            revdate: detail.revdate,
            refno: detail.refno,
            refdate:detail.refdate,
            natval:detail.natval,
            c2c:detail.c2c,
            cph:detail.cph,
            rph:detail.rph
          }
        })
        const empcodes = Object.keys(final)
        const tableData = [];
        empcodes.forEach(code => {
          const account = Object.keys(final[code].accounts)
          let sumc2c = 0;
          let sumcph = 0;
          let sumrph = 0;
          let refno = 0;
          account.forEach(account => {
            sumc2c = sumc2c + parseFloat(final[code].accounts[account].c2c);
            sumcph = sumcph + parseFloat(final[code].accounts[account].cph);
            sumrph = sumrph + parseFloat(final[code].accounts[account].rph);
            refno = refno + parseFloat(final[code].accounts[account].refno);
          })
            final[code].c2c = sumc2c;
            final[code].cph = sumcph;
            final[code].rph = sumrph;
            final[code].refno = refno;
          
            tableData.push({
             empid:final[code].empid,
              revno: final[code].revno,
              revdate: final[code].revdate,
              refno:final[code].refno,
              refdate:"",
              natval:"",
              c2c: final[code].c2c,
              cph: final[code].cph,
              rph: final[code].rph,
              bold: true,
            })
            account.forEach(ag => {
              tableData.push({
                empid:final[code].empid,
                revno: final[code].accounts[ag].revno,
                revdate: final[code].accounts[ag].revdate,
                refno: final[code].accounts[ag].refno,
                refdate: final[code].accounts[ag].refdate,
                natval:  final[code].accounts[ag].natval,
                c2c:final[code].accounts[ag].c2c,
                cph:final[code].accounts[ag].cph,
                rph:0,
              })
            })
        })
        this.rowData=tableData;
      })
    })

  }

}  