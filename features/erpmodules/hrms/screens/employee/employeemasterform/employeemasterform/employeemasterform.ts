import { Component, OnInit } from '@angular/core';
import { FormBuilder} from '@angular/forms';
import { IEmployee } from '../../../../models/IEmployee';
import {
  getThisGBPageService,
} from './employeemasterform.factory';
import {
  EmployeemasterFormgroupStore,
} from './employeemasterform.formgroup';
import { EmployeeService } from '../../../../services/employeemaster/employeemaster.service';
import { EmployeeDBService } from '../../../../dbservices/employeemaster/employeemaster.dbservice';
import { Select, Store } from '@ngxs/store';
import { GBHttpService } from './../../../../../../../libs/gbcommon/src/lib/services/HTTPService/GBHttp.service';
import {GBBaseDataPageStoreComponentWN} from './../../../../../../../libs/uicore/src/lib/components/Base/GBBasePage';

import { Observable } from 'rxjs';
import { EmployeeState } from '../../../../stores/employeemaster/employeemaster.state';
import { GetAll, GetData,SaveData } from '../../../../stores/employeemaster/employeemaster.action';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';

import { LayoutState } from '../../../../../../layout/store/layout.state';
@Component({
  selector: 'app-employeemasterform',
  templateUrl: './employeemasterform.html',
  styleUrls: ['./employeemasterform.scss'],
  providers: [
    { provide: 'IdField', useValue: 'EmployeeId' },
    { provide: 'url', useValue: 'url' },
    { provide: 'Store', useClass: Store },
    { provide: 'DBDataService', useClass: EmployeeDBService, deps: [GBHttpService], },
    { provide: 'DataService', useClass: EmployeeService, deps: ['DBDataService'] },
    {
      provide: 'PageService', useFactory: getThisGBPageService, deps: [Store, 'DataService', 'DBDataService',
        FormBuilder, GBHttpService, ActivatedRoute, Router,],
    },],
})
export class EmployeemasterformComponent extends GBBaseDataPageStoreComponentWN<IEmployee> implements OnInit {
  geoconfig
  afuConfig
  selection;
  dataLoading;
  uploadedFiles: Array < File > ;
  title = 'Employee Master';
  img = "";
  src = this.img ? true : false;
  imageUrl: any = 'assets/icons/Employee.png';
  editFile: boolean = true;
  removeUpload: boolean = false;

  uploadFile(event) {
    let reader = new FileReader();
    let file = event.target.files[0];
    if (event.target.files && event.target.files[0]) {
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.imageUrl = reader.result;
        this.editFile = false;
        this.removeUpload = true;
      }
    }
  } 
  form: EmployeemasterFormgroupStore = new EmployeemasterFormgroupStore(
    this.gbps.gbhttp.http,
    this.gbps.store
  );
  @Select(EmployeeState) currentData$;
  Picklistvalues;
  @Select(LayoutState.ThemeClass) themeClass$: Observable<string>;

  thisConstructor() { }

  loadScreen(recid?: string) {
    if (recid) {
      const id = parseInt(recid);
      this.gbps.store.dispatch(new GetData(id));
    }
    else {
      this.gbps.store.dispatch(new GetAll(null)).subscribe((res) => {
        this.Picklistvalues = res.EmployeeMaster.data
        console.log("this.Picklistvalues:"+JSON.stringify(this.Picklistvalues))
        this.gbps.store.dispatch(new SaveData(null));
      });
    }

  }
  ngOnInit(): void {

    this.geoconfig = {
      lat: 11.0282877, 
      long: 76.9551012,
      isdirection: true,
      address: ''
   };

   var rangen = Math.floor(Math.random() * 1499999999) + 1;
   let userObjectId = rangen;
   this.afuConfig = {
    multiple: true,
    maxSize: "1",
    isGBAPI: true,
    objecttypeid:-1399999744
  };

    let id = '';
    this.gbps.activeroute.params.subscribe((params: any) => {
      id = params['id'];
    });
    this.gbps.activeroute.paramMap.subscribe((params: ParamMap) => {
      id = params.get('id')
    });
    if (this.gbps.activeroute.firstChild) {
      this.gbps.activeroute.firstChild.params.subscribe((params: ParamMap) => {
        id = params['id'];
      });
    }
    console.log("routing2", id);
    this.loadScreen(id);
  }

  public getlocation(data) {
    console.log(data)
  }
  public changeFn(val) {
    for(let data of this.Picklistvalues){
      if(data.Code == val){
        console.log(data.Id)
        this.loadScreen(data.Id);
      }
    }
}

}


