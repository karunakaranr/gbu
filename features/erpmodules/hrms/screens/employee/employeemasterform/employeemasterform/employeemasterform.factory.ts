import { GBBaseDataService, GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { GBHttpService ,GbToasterService} from '@goodbooks/gbcommon';
import { IEmployee } from '../../../../models/IEmployee';
import { EmployeeService } from '../../../../services/employeemaster/employeemaster.service';
import { EmployeeDBService } from '../../../../dbservices/employeemaster/employeemaster.dbservice';
import { Store } from '@ngxs/store';
import { FormBuilder } from '@angular/forms';
import { GBDataPageService, GBDataPageServiceWN } from '@goodbooks/uicore';
import { ActivatedRoute, Router } from '@angular/router';
   

export function getThisDBDataService(http: GBHttpService): GBBaseDBDataService<IEmployee> {
  return new EmployeeDBService(http);
}
export function getThisDataServiceWN(dbDataService: GBBaseDBDataService<IEmployee>): GBBaseDataServiceWN<IEmployee> {
  return new EmployeeService(dbDataService);
}
export function getThisDataService(dbDataService: GBBaseDBDataService<IEmployee>): GBBaseDataService<IEmployee> {
  return new EmployeeService(dbDataService);
}
export function getThisGBPageService(store: Store, dataService: GBBaseDataService<IEmployee>, dbDataService: GBBaseDBDataService<IEmployee>, fb: FormBuilder, gbhttp:GBHttpService, activeroute:ActivatedRoute, router:Router,toaster:GbToasterService): GBDataPageService<IEmployee> {
  return new GBDataPageService<IEmployee>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
export function getThisGBPageServiceWN(store: Store, dataService: GBBaseDataServiceWN<IEmployee>, dbDataService: GBBaseDBDataService<IEmployee>, fb: FormBuilder, gbhttp:GBHttpService, activeroute:ActivatedRoute, router:Router,toaster:GbToasterService): GBDataPageServiceWN<IEmployee> {
  return new GBDataPageServiceWN<IEmployee>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
