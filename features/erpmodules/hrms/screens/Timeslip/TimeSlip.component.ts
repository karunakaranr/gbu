import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { TimeSlipService } from 'features/erpmodules/hrms/services/TimeSlip/TimeSlip.service';
import { ITimeSlip } from 'features/erpmodules/hrms/models/ITimeSlip';
import { IShift } from '../../models/IShift';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { HRMSURLS } from './../../URLS/urls'; 
import { DialogBoxComponent } from 'features/erpmodules/samplescreens/screens/DialogBox/DialogBox.component';
import * as TimeSlipJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/TimeSlip.json'

@Component({
    selector: 'app-TimeSlip',
    templateUrl: './TimeSlip.component.html',
    styleUrls: ['./TimeSlip.component.scss'],
    providers: [
        { provide: 'IdField', useValue: 'TimeSlipId'},
        { provide: 'url', useValue: HRMSURLS.TimeSlip },
        { provide: 'DataService', useClass: TimeSlipService },
        { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
        { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
    ]
})
export class TimeSlipComponent extends GBBaseDataPageComponentWN<ITimeSlip>{
  title : string = "TimeSlip"
  TimeSlipJSON = TimeSlipJSON;
  form: GBDataFormGroupWN<ITimeSlip> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "TimeSlip", {}, this.gbps.dataService);
  thisConstructor() {
      this.store.dispatch(new ButtonVisibility("Forms"))
   }

  public TimeSlipFillForm(TimeslipId: number): void {
    if (TimeslipId) {
      this.gbps.dataService.getData(TimeslipId).subscribe(TimeSlip => {
          this.form.patchValue(TimeSlip);
        })
    }
  }

  public PicklistPatchValue(SelectedPicklistDatas: any): void { // Picklist Emit Function
    if(SelectedPicklistDatas.id == 'ShiftId'){
      this.service.getService(HRMSURLS.Shift,SelectedPicklistDatas.SelectedData,'?ShiftId=').subscribe((Shift:IShift)=>{
        this.form.get('ShiftStartTime').patchValue(Shift.ShiftStartTime)
        this.form.get('ShiftEndTime').patchValue(Shift.ShiftEndTime)
      })
    }
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }

  public DurationCalculator(FromTimeToTime:any):void{
    if(this.form.get('TimeSlipTimeSlipEndTime').value != 0 && this.form.get('TimeSlipTimeSlipStartTime').value !=0){
      let duration = this.form.get('TimeSlipTimeSlipEndTime').value - this.form.get('TimeSlipTimeSlipStartTime').value;
      if(duration>0){
        this.form.get('TimeSlipDuration').patchValue(duration)
      } else{
          const dialogRef = this.dialog.open(DialogBoxComponent, {
          width: '400px',
          data: {
            message: "Please enter Valid Form and To Time",
            heading: 'Error',
          }
        });
        this.form.get('TimeSlipTimeSlipEndTime').patchValue(0)
        this.form.get('TimeSlipTimeSlipStartTime').patchValue(0)
        this.form.get('TimeSlipDuration').patchValue(0)
      }
    }
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<ITimeSlip> {
  const dbds: GBBaseDBDataService<ITimeSlip> = new GBBaseDBDataService<ITimeSlip>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<ITimeSlip>, dbDataService: GBBaseDBDataService<ITimeSlip>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<ITimeSlip> {
  return new GBDataPageService<ITimeSlip>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
