import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { PolicyService } from '../../services/Policy/policy.service';
import { IPolicy } from '../../models/IPolicy';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as PolicyJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/Policy.json'
import { HRMSURLS } from '../../URLS/urls';


@Component({
  selector: 'app-policy',
  templateUrl: './policy.component.html',
  styleUrls: ['./policy.component.scss'], 
  providers: [
    { provide: 'IdField', useValue: 'PolicyId'},
    { provide: 'url', useValue: HRMSURLS.Policy},
    { provide: 'DataService', useClass: PolicyService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class PolicyComponent extends GBBaseDataPageComponentWN<IPolicy> {
  title: string = 'Policy'
  PolicyJSON = PolicyJSON;


  form: GBDataFormGroupWN<IPolicy> = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'Policy', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.PolicyRetrivalValue(arrayOfValues.PolicyId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }

  public PolicyRetrivalValue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      console.log("Policyretrival",SelectedPicklistData)
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(Policy => {
        this.form.patchValue(Policy);
      })
    }
  }
  public PolicyPatchValue(SelectedPicklistDatas: any): void {
    console.log("SelectedPicklistDatas",SelectedPicklistDatas)
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}

export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IPolicy> {
  const dbds: GBBaseDBDataService<IPolicy> = new GBBaseDBDataService<IPolicy>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IPolicy>, dbDataService: GBBaseDBDataService<IPolicy>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IPolicy> {
  return new GBDataPageService<IPolicy>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
