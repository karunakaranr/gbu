import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
// import { ServiceName } from ‘ServicePath’;
// import { IPayTaxSetting } from ‘ModelPath’;
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { HRMSURLS } from '../../URLS/urls';
import { PayTaxTypeService } from 'features/erpmodules/framework/services/PayTaxType.Service';
import { IPayTaxSetting } from '../../models/IPayTaxSetting';
import * as PayTaxSettingJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/PayTaxSetting.json'
// import * as JsonName from ‘JsonPath’ './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/PackSet.json';
// import { url} from ‘UrlPath’;


@Component({
  selector: 'app-PayTaxSetting',
  templateUrl: './PayTaxSetting.component.html',
  styleUrls: ['./PayTaxSetting.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'PayTaxTypeDetailId'},
    { provide: 'url', useValue: HRMSURLS.PayTaxSetting },
    { provide: 'saveurl', useValue: HRMSURLS.PayTaxSettingSave },
    { provide: 'DataService', useClass: PayTaxTypeService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url','saveurl']},
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class PayTaxSettingComponent extends GBBaseDataPageComponentWN<IPayTaxSetting > {
  title: string = 'PayTaxSetting'
  PayTaxSettingJSON = PayTaxSettingJSON;


  form: GBDataFormGroupWN<IPayTaxSetting > = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'PayTaxSetting', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.FormFillFunction(arrayOfValues.PayTaxTypeDetailId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }


  public FormFillFunction(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(PackSet => {
        this.form.patchValue(PackSet);
      })
    }
  }


  public PicklistPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string,saveurl:string): GBBaseDBDataService<IPayTaxSetting[]> {
    const dbds: GBBaseDBDataService<IPayTaxSetting[]> = new GBBaseDBDataService<IPayTaxSetting[]>(http);
    dbds.endPoint = url;
    dbds.saveurl = saveurl;
    return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IPayTaxSetting >, dbDataService: GBBaseDBDataService<IPayTaxSetting >, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IPayTaxSetting > {
  return new GBDataPageService<IPayTaxSetting >(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}


