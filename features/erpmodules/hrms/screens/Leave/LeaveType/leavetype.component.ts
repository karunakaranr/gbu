import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { LeaveTypeService } from 'features/erpmodules/hrms/services/Leave/LeaveType.service';
import { ILeaveType } from 'features/erpmodules/hrms/models/ILeaveType';
import { HRMSURLS } from 'features/erpmodules/hrms/URLS/urls';
import * as LeaveTypeJSON from './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/LeaveType.json'


import { ButtonVisibility } from 'features/layout/store/layout.actions';

@Component({
  selector: 'app-LeaveType',
  templateUrl:'./leavetype.component.html',
  styleUrls: ['./leavetype.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'LeaveId' },
    { provide: 'url', useValue:HRMSURLS.LeaveType },
    { provide: 'DataService', useClass: LeaveTypeService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class LeaveTypeComponent extends GBBaseDataPageComponentWN<ILeaveType> {
  title: string = "LeaveType"
  LeaveTypeJSON = LeaveTypeJSON;

  form: GBDataFormGroupWN<ILeaveType> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "LeaveType", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.LeaveTypeValue(arrayOfValues.LeaveId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }
  public LeaveTypeValue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(LeaveType => {
        this.form.patchValue(LeaveType);
      })
    }
  }
  public LeaveTypeNot(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}
export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<ILeaveType> {
  const dbds: GBBaseDBDataService<ILeaveType> = new GBBaseDBDataService<ILeaveType>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<ILeaveType>, dbDataService: GBBaseDBDataService<ILeaveType>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<ILeaveType> {
  return new GBDataPageService<ILeaveType>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
