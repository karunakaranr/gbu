import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { IMeasureOptionsType } from 'features/erpmodules/hrms/models/IMeasureOptionsType';
import { MeasureOptionsTypeService } from 'features/erpmodules/hrms/services/TypeMaster/MeasureOptionsType/measureoptionstype.service';
import * as MeasureOptionsTypeJSON from './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/MeasureOptionsType.json'
import { HRMSURLS } from './../../../URLS/urls';

import { ButtonVisibility } from 'features/layout/store/layout.actions';

@Component({
  selector: 'app-MeasureOptionsType',
  templateUrl:'./measureoptionstype.component.html',
  styleUrls: ['./measureoptionstype.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'LovTypeId' },
    { provide: 'url', useValue:HRMSURLS.MeasureOptionsType },
    { provide: 'DataService', useClass: MeasureOptionsTypeService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class MeasureOptionsTypeComponent extends GBBaseDataPageComponentWN<IMeasureOptionsType> {
  title: string = "MeasureOptionsType"
  MeasureOptionsTypeJSON = MeasureOptionsTypeJSON;

  form: GBDataFormGroupWN<IMeasureOptionsType> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "MeasureOptionsType", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.MeasureOptionsTypeValue(arrayOfValues.LovTypeId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }
  public MeasureOptionsTypeValue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(MeasureOptionsType => {
        this.form.patchValue(MeasureOptionsType);
      })
    }
  }
  public MeasureOptionsTypeRetrival(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}
export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IMeasureOptionsType> {
  const dbds: GBBaseDBDataService<IMeasureOptionsType> = new GBBaseDBDataService<IMeasureOptionsType>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IMeasureOptionsType>, dbDataService: GBBaseDBDataService<IMeasureOptionsType>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IMeasureOptionsType> {
  return new GBDataPageService<IMeasureOptionsType>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
