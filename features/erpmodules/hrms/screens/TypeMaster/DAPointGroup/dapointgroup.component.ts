import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { IDAPointGroup } from 'features/erpmodules/hrms/models/IDAPointGroup';
import { DAPointGroupService } from 'features/erpmodules/hrms/services/TypeMaster/DAPointGroup/dapointgroup';
import { HRMSURLS } from './../../../URLS/urls';
import * as DAPointGroupJSON from './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/DAPointGroup.json'



import { ButtonVisibility } from 'features/layout/store/layout.actions';

@Component({
  selector: 'app-DAPointGroup',
  templateUrl:'./dapointgroup.component.html',
  styleUrls: ['./dapointgroup.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'DAPointGroupId' },
    { provide: 'url', useValue:HRMSURLS.DAP },
    { provide: 'DataService', useClass: DAPointGroupService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class DAPointGroupComponent extends GBBaseDataPageComponentWN<IDAPointGroup> {
  title: string = "DAPointGroup"
  DAPointGroupJSON = DAPointGroupJSON;

  form: GBDataFormGroupWN<IDAPointGroup> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "DAPointGroup", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.DAPointGroupValue(arrayOfValues.DAPointGroupId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }
  public DAPointGroupValue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(DAPointGroup => {
        this.form.patchValue(DAPointGroup);
      })
    }
  }
  public DAPointGroupNot(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}
export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IDAPointGroup> {
  const dbds: GBBaseDBDataService<IDAPointGroup> = new GBBaseDBDataService<IDAPointGroup>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IDAPointGroup>, dbDataService: GBBaseDBDataService<IDAPointGroup>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IDAPointGroup> {
  return new GBDataPageService<IDAPointGroup>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
