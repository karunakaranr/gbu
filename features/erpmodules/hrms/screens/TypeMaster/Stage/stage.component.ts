import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { StageService } from '../../../services/TypeMaster/Stage/stage.service';
import { Select, Store } from '@ngxs/store';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Observable } from 'rxjs/internal/Observable'
import { LayoutState } from 'features/layout/store/layout.state';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';



@Component({
  selector: 'app-Stage',
  templateUrl: './stage.component.html',
  styleUrls: ['./stage.component.scss'],
})
export class StageComponent implements OnInit {
  @Select(LayoutState.CurrentTheme) currenttheme: any;
  @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;  
  @Select(LayoutState.Formeditabe) formedit$: Observable<any>;
  @Select(LayoutState.Movenxtpreid) movenextpreviousid$: Observable<any>;
  @Select(LayoutState.FormEdit) edit$: Observable<boolean>;
  constructor(private store: Store,private router: Router , public activeroute:ActivatedRoute , public service:StageService , public fb:FormBuilder) { }

  title = 'Stage';
  selection:number;
  selectionname:string;
  selectioncode:string
  dataLoading; 
 
  isnoteditable=true;
  form : FormGroup 
  Listrowselectedindex : number                                                                          
  StagePicklistCode;
  StagePicklistName;
  GCMid : number
  
  ngOnInit(): void {
    this.activeroute.params.subscribe(res => {
      this.GCMid = res['id']
      })
    this.edit$.subscribe(res => {
      this.isnoteditable = res;
    })
    this.form = new FormGroup({
      GcmId: new FormControl() ,
      GcmCode: new FormControl(),
      GcmName: new FormControl(),
      GcmTypeId: new FormControl(),
      GcmTypeCode: new FormControl(),
      GcmTypeName: new FormControl(),
      GcmColor: new FormControl(),
      GcmRemarks: new FormControl(),
      GcmStatus: new FormControl(),
      GcmVersion: new FormControl(),
      GcmCreatedOn: new FormControl(),
      GcmModifiedOn: new FormControl(),
      GcmCreatedByName: new FormControl(),
      GcmModifiedByName: new FormControl(),
      GcmSourceType: new FormControl()
   });
    let id = '';
    const listid = this.activeroute.snapshot.queryParamMap.get('Id');
    const listrowindex = this.activeroute.snapshot.queryParamMap.get('RowIndexselected');
    let arrayOfValues = JSON.parse(listid);
    this.Listrowselectedindex = JSON.parse(listrowindex);
    id = arrayOfValues;
    this.loadScreen(id);
    this.movenextandpre()
    this.readOutputValueEmitted(id);
  }

  loadScreen(id){

    if(id){
      this.service.getid(id).subscribe(res => {
        console.log("KK",res)
        this.form = this.fb.group({
          GcmId: [res.GcmId] ,
          GcmCode: [res.GcmCode],
          GcmName: [res.GcmName],
          GcmTypeId: [res.GcmTypeId],
          GcmTypeCode: [res.GcmTypeCode],
          GcmTypeName: [res.GcmTypeName],
          GcmColor: [res.GcmColor],
          GcmRemarks: [res.GcmRemarks],
          GcmStatus: [res.GcmStatus],
          GcmVersion: [res.GcmVersion],
          GcmCreatedOn: [res.GcmCreatedOn],
          GcmModifiedOn: [res.GcmModifiedOn],
          GcmCreatedByName: [res.GcmCreatedByName],
          GcmModifiedByName: [res.GcmModifiedByName],
          GcmSourceType: [res.GcmSourceType]
        }) as FormGroup;
      })
      //  console.log("this.form",this.form.value.GcmRemarks);
    }
  }

  public changeFunCode(selecteddata) {
    let StageObj =  this.StagePicklistCode.find(t=>t.GcmCode ===selecteddata);
    if(StageObj == undefined){

      this.form = this.fb.group({
        GcmId: [0] ,
        GcmCode: [this.selectioncode],
        GcmName: [this.selectionname],
        GcmTypeId: [this.GCMid],
        GcmColor: [-1],
        GcmRemarks: [],
        GcmStatus: [1],
        GcmVersion: [1],
        GcmCreatedOn: ["/Date(1417996800000)/"],
        GcmModifiedOn: ["/Date(1417996800000)/"],
        GcmCreatedByName: ["ADMIN"],
        GcmModifiedByName: ["ADMIN"],
        GcmSourceType: [5]
      }) as FormGroup;

      this.form.removeControl('GcmTypeCode');
      this.form.removeControl('GcmTypeName');
    }
    else{
      this.selection = StageObj.GcmId;
      let id = StageObj.GcmId
      this.loadScreen(id);
    }
  }

  public changeFunName(selecteddata) {
    let StageObj =  this.StagePicklistName.find(t=>t.GcmName ===selecteddata);
    if(StageObj == undefined){
      this.form = this.fb.group({
        GcmId: [0] ,
        GcmCode: [this.selectioncode],
        GcmName: [this.selectionname],
        GcmTypeId: [this.GCMid],
        GcmColor: [-1],
        GcmRemarks: [],
        GcmStatus: [1],
        GcmVersion: [1],
        GcmCreatedOn: ["/Date(1417996800000)/"],
        GcmModifiedOn: ["/Date(1417996800000)/"],
        GcmCreatedByName: ["ADMIN"],
        GcmModifiedByName: ["ADMIN"],
        GcmSourceType: [5]
      }) as FormGroup;
    }
    else{
      this.selection = StageObj.GcmId;
      let id = StageObj.GcmId
      this.loadScreen(id);
    }
  }

  public callservice(){
      this.service.getall(this.GCMid).subscribe(res => {
        let data = res
        data = data.map(({ Id, Code,Name, }) => ({ GcmId: Id,GcmCode: Code, GcmName:Name}));
        console.log("JK",data)
        this.StagePicklistCode =data;
        this.StagePicklistName =data;
        
      })
  }

  public movenextandpre(){
    this.rowdatacommon$.subscribe(data => {
      console.log("Data", data)
      this.movenextpreviousid$.subscribe(res => {
        if(res){
          console.log("resid",res)
          let id = data[res].GcmId
          console.log("id",id)
          this.loadScreen(id)
        }
      })
    });

  }



  public readOutputValueEmitted(val){
    console.log("CHILDVALUE",val)
  }
}



