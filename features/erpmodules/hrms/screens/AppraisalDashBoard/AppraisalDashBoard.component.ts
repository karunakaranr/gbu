import { Component, Inject, LOCALE_ID, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { Observable } from 'rxjs';
import { FilterState } from 'features/gbfilter/store/gbfilter.state';
import { CommonReportdbservice } from 'features/commonreport/dbservice/commonreportdbservice';
import { IAppraisalDetail } from '../../models/IAppraisalDetail';
import { ICriteriaDTODetail } from 'features/erpmodules/sales/models/IFilterDetail';
import { SharedService } from 'features/commonreport/service/datapassing.service';

@Component({
  selector: 'app-AppraisalDashBoard',
  templateUrl: './AppraisalDashBoard.component.html',
  styleUrls: ['./AppraisalDashBoard.component.scss']
})

export class AppraisalDashBoardComponent implements OnInit {
  @Select(ReportState.Sourcecriteriadrilldown) filtercriteria$: Observable<any>;

@Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
AppraisalDetail: IAppraisalDetail;
ReportType : number=0;
filterData : ICriteriaDTODetail;
SummaryModel = { option: 0 }
SelectSummary : boolean =false;
Summary: string[] = ['Evaluator', 'Employee', 'Employee Department','Evaluator Department'];
  constructor(public sharedService: SharedService , public store: Store, @Inject(LOCALE_ID) public locale: string,public filterservice : CommonReportdbservice) { }
  ngOnInit(): void {
    this.ReceiveData();
  }

  public ReceiveData() {
    this.sharedService.getRowInfo().subscribe(data => {
      console.log("Appraisal Detail:", data)
      this.AppraisalDetail = data[0];
    });
  }

  public SummaryReportServiceCall() {
    this.filtercriteria$.subscribe(filterdata => {
      this.filterData = filterdata;
      console.log("Filter Data:",this.filterData);
    })
    console.log("Model:", this.SummaryModel)
    this.SelectSummary=true;
    this.ReportType=this.SummaryModel.option;
}  
}