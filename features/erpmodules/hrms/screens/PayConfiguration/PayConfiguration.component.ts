import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { PayConfigurationService } from '../../services/PayConfiguration/PayConfiguration.service';
import { IPayConfiguration } from '../../models/IPayConfiguration';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as PayConfigurationJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/PayConfiguration.json'
import { HRMSURLS } from '../../URLS/urls';
@Component({
  selector: 'app-PayConfiguration',
  templateUrl: './PayConfiguration.component.html',
  styleUrls: ['./PayConfiguration.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'PayConfigurationId'},
    { provide: 'url', useValue: HRMSURLS.PayConfiguration },
    { provide: 'DataService', useClass: PayConfigurationService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class PayConfigurationComponent extends GBBaseDataPageComponentWN<IPayConfiguration > {
  title: string = 'PayConfiguration'
  PayConfigurationJSON = PayConfigurationJSON;


  form: GBDataFormGroupWN<IPayConfiguration > = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'PayConfiguration', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.PayConfigurationFillFunction(arrayOfValues.PayConfigurationId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }


  public PayConfigurationFillFunction(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(PackSet => {
        this.form.patchValue(PackSet);
      })
    }
  }


  public PayConfigurationPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IPayConfiguration > {
  const dbds: GBBaseDBDataService<IPayConfiguration > = new GBBaseDBDataService<IPayConfiguration >(http);
  dbds.CopyFrom = ['PayConfigurationCode','PayConfigurationName'];
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IPayConfiguration >, dbDataService: GBBaseDBDataService<IPayConfiguration >, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IPayConfiguration > {
  return new GBDataPageService<IPayConfiguration >(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
