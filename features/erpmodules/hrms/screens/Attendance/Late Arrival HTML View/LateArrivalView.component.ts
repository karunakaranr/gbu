import { HttpClient } from '@angular/common/http';
import { Component, Inject, LOCALE_ID, OnInit } from '@angular/core';
// import { ItemcategoryService } from 'features/erpmodules/inventory/services/itemcategory/itemcategory.service';
import { Observable } from 'rxjs';
import { Select, Store } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { SharedService } from 'features/commonreport/service/datapassing.service';


@Component({
  selector: 'app-LateArrivalView',
  templateUrl: './LateArrivalView.component.html',
  styleUrls: ['./LateArrivalView.component.scss'],
})
export class LateArrivalComponent implements OnInit {
  @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
  public rowData = [];  
  public late: string = ''; 
  public later: string = ''; 
  public latest: string = '';

  constructor(public sharedService: SharedService,public store: Store, @Inject(LOCALE_ID) public locale: string) { }
  ngOnInit(): void {
    this.getLateArrivalView();
  }
  
  public getLateArrivalView() {
    this.sharedService.getRowInfo().subscribe(datas => {
      if (datas.length > 0) {
      this.rowData = datas;
      console.log("Late Arrival Data :", this.rowData)
    }
    });
  }

}
