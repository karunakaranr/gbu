
import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs';
import { Select } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { SharedService } from 'features/commonreport/service/datapassing.service';


@Component({
    selector: 'app-DailyAttendance',
    templateUrl: './DailyAttendance.component.html',
    styleUrls: ['./DailyAttendance.component.scss'],
})
export class DailyAttendanceComponent implements OnInit {
    @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
    public rowDatas: any;
    constructor(public sharedService: SharedService) { }
    ngOnInit(): void {
        this.getDailyAttendance();
    }

    public getDailyAttendance() {
        this.sharedService.getRowInfo().subscribe(data => {
            this.rowDatas = data;
            if (data.length > 0) {
                console.log("DailyAttendance Details :", this.rowDatas)

                let groupingData = this.rowDatas;
                var finalizedGroup = [];

                groupingData.map((inputData, index) => {

                    let assetNature;

                    if (inputData['DailyAttendanceDayType'] == null) {
                        assetNature = ''; // Set to an empty string when DailyAttendanceDayType is null or undefined
                    } else if (inputData['DailyAttendanceDayType'] === 0) {
                        assetNature = "F";
                    } else if (inputData['DailyAttendanceDayType'] === 1) {
                        assetNature = 'HW';
                    } else if (inputData['DailyAttendanceDayType'] === 2) {
                        assetNature = 'WOff';
                    } else if (inputData['DailyAttendanceDayType'] === 3) {
                        assetNature = '0';
                    } else if (inputData['DailyAttendanceDayType'] === 4) {
                        assetNature = 'L';
                    } else if (inputData['DailyAttendanceDayType'] === 5) {
                        assetNature = 'HL';
                    } else if (inputData['DailyAttendanceDayType'] === 6) {
                        assetNature = 'CW';
                    } else if (inputData['DailyAttendanceDayType'] === 7) {
                        assetNature = 'COff';
                    } else if (inputData['DailyAttendanceDayType'] === 8) {
                        assetNature = 'A';
                    } else if (inputData['DailyAttendanceDayType'] === 9) {
                        assetNature = 'H';
                    }


                    finalizedGroup.push({
                        sno: index,

                        code: inputData['EmployeeCode'],
                        name: inputData['EmployeeName'],
                        shift: inputData['ShiftCode'],
                        timein: inputData['DailyAttendanceCalInTime'],
                        timeout: inputData['DailyAttendanceCalOutTime'],
                        duration: inputData['DailyAttendanceCallDuration'],
                        daytype: assetNature,
                        type: inputData['LeaveCode'],

                        DailyAttDate: inputData['DailyAttendanceAttendanceDate'],
                        Depcode: inputData['DepartmentName'],

                    });
                });

                const final = {};
                finalizedGroup.forEach((detailData) => {
                    final[detailData.Depcode] = {
                        attendancegroup: {},
                        //sno:detailData.DailyAttDate,
                        code: detailData.DailyAttDate,
                        name: detailData.Depcode,
                        shift: "",
                        timein: "",
                        timeout: "",
                        duration: "",
                        daytype: "",
                        type: "",
                        ...final[detailData.Depcode]

                    };

                    final[detailData.Depcode].attendancegroup[detailData.sno] = {
                        //sno:detailData.sno,
                        code: detailData.code,
                        name: detailData.name,
                        shift: detailData.shift,
                        timein: detailData.timein,
                        timeout: detailData.timeout,
                        duration: detailData.duration,
                        daytype: detailData.daytype,
                        type: detailData.type,
                    }
                });

                const supGroups = Object.keys(final);

                const tableData = [];

                supGroups.forEach((codeData) => {
                    tableData.push({
                        //sno:final[codeData].sno,
                        code: final[codeData].code,
                        name: final[codeData].name,
                        shift: "",
                        timein: "",
                        timeout: "",
                        duration: "",
                        daytype: "",
                        type: "",
                        bold: true,
                    });

                    const accounts = Object.keys(final[codeData].attendancegroup);
                    accounts.forEach((account) => {
                        tableData.push({
                            //sno:final[codeData].attendancegroup[account].sno,
                            code: final[codeData].attendancegroup[account].code,
                            name: final[codeData].attendancegroup[account].name,
                            shift: final[codeData].attendancegroup[account].shift,
                            timein: final[codeData].attendancegroup[account].timein,
                            timeout: final[codeData].attendancegroup[account].timeout,
                            duration: final[codeData].attendancegroup[account].duration,
                            daytype: final[codeData].attendancegroup[account].daytype,
                            type: final[codeData].attendancegroup[account].type,
                        });

                    });

                });

                this.rowDatas = tableData;

                console.log("Final Data:", this.rowDatas[0])
            }
        });
    }
}

