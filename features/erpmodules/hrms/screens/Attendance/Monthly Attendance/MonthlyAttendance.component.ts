
import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs';
import { Select } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { SharedService } from 'features/commonreport/service/datapassing.service';



@Component({
    selector: 'app-MonthlyAttendance',
    templateUrl: './MonthlyAttendance.component.html',
    styleUrls: ['./MonthlyAttendance.component.scss'],
})
export class MonthlyAttendanceComponent implements OnInit {
    @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
    public rowDatas: any;
    constructor(public sharedService: SharedService) { }
    ngOnInit(): void {
        this.getMonthlyAttendance();
    }

    public getMonthlyAttendance() {
        this.sharedService.getRowInfo().subscribe(data => {
            this.rowDatas = data;
            if (data.length > 0) {
                console.log("MonthlyAttendance Details :", this.rowDatas)

                let groupingData = this.rowDatas;
                var finalizedGroup = [];

                groupingData.map((inputData, index) => {
                    finalizedGroup.push({
                        sno: index,

                        code: inputData['Employee'],
                        name: inputData['Employee'],
                        present: inputData['PresentDays'],
                        absent: inputData['Absent'],
                        leave: inputData['LeaveDays'],
                        woff: inputData['WeeklyOff'],
                        paid: inputData['PaidDays'],
                        days: inputData['PresentDays'],
                        pl: inputData['L_PL'],
                        sl: inputData['L_SL'],
                        cl: inputData['L_CL'],
                        lop: inputData['L_LOP'],

                        headoff: inputData['Employee'],
                        account: inputData['Employee'],

                    });
                });

                const final = {};
                finalizedGroup.forEach((detailData) => {
                    final[detailData.headoff.Location.Name] = {
                        attendancegroup: {},
                        //sno:detailData.DailyAttDate,
                        code: detailData.headoff.Location.Name,
                        name: detailData.account.Department.Name,
                        present: "",
                        absent: "",
                        leave: "",
                        woff: "",
                        paid: "",
                        days: "",
                        pl: "",
                        sl: "",
                        cl: "",
                        lop: "",
                        ...final[detailData.headoff.Location.Name]

                    };

                    final[detailData.headoff.Location.Name].attendancegroup[detailData.sno] = {
                        //sno:detailData.sno,
                        code: detailData.code,
                        name: detailData.name,
                        present: detailData.present,
                        absent: detailData.absent,
                        leave: detailData.leave,
                        woff: detailData.woff,
                        paid: detailData.paid,
                        days: detailData.days,
                        pl: detailData.pl,
                        sl: detailData.sl,
                        cl: detailData.cl,
                        lop: detailData.lop,
                    }
                });

                const supGroups = Object.keys(final);

                const tableData = [];

                supGroups.forEach((codeData) => {
                    tableData.push({
                        //sno:final[codeData].sno,
                        code: final[codeData].code,
                        name: final[codeData].name,
                        present: "",
                        absent: "",
                        leave: "",
                        woff: "",
                        paid: "",
                        days: "",
                        pl: "",
                        sl: "",
                        cl: "",
                        lop: "",
                        bold: true,
                    });

                    const accounts = Object.keys(final[codeData].attendancegroup);
                    accounts.forEach((account) => {
                        tableData.push({
                            //sno:final[codeData].attendancegroup[account].sno,
                            code: final[codeData].attendancegroup[account].code.Code,
                            name: final[codeData].attendancegroup[account].name.Name,
                            present: final[codeData].attendancegroup[account].present,
                            absent: final[codeData].attendancegroup[account].absent,
                            leave: final[codeData].attendancegroup[account].leave,
                            woff: final[codeData].attendancegroup[account].woff,
                            paid: final[codeData].attendancegroup[account].paid,
                            days: final[codeData].attendancegroup[account].days,
                            pl: final[codeData].attendancegroup[account].pl,
                            sl: final[codeData].attendancegroup[account].sl,
                            cl: final[codeData].attendancegroup[account].cl,
                            lop: final[codeData].attendancegroup[account].lop,
                        });

                    });

                });
                this.rowDatas = tableData;

                console.log("Final Data:", this.rowDatas[0])
            }
        });
    }
}

