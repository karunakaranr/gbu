
import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs';
import { Select } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';


@Component({
    selector: 'app-EarlyDepature',
    templateUrl: './EarlyDepature.component.html',
    styleUrls: ['./EarlyDepature.component.scss'],
})
export class EarlyDepatureComponent implements OnInit {
    @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
    public rowDatas: any;
    constructor() { }
    ngOnInit(): void {
        this.getEarlyDepature();
    }

    public getEarlyDepature() {
        this.rowdatacommon$.subscribe(data => {
            this.rowDatas = data;
            console.log("Early Depature Details :", this.rowDatas)

            let groupingData = this.rowDatas;
            var finalizedGroup = [];
            
            groupingData.map((inputData,index) => {

                let assetNature;

                if (inputData['DailyAttendanceDayType'] == null) {
                    assetNature = ''; // Set to an empty string when DailyAttendanceDayType is null or undefined
                } else if (inputData['DailyAttendanceDayType'] === 0) {
                    assetNature = "F";
                } else if (inputData['DailyAttendanceDayType'] === 1) {
                    assetNature = 'HW';
                } else if (inputData['DailyAttendanceDayType'] === 2) {
                    assetNature = 'WOff';
                } else if (inputData['DailyAttendanceDayType'] === 3) {
                    assetNature = '0';
                } else if (inputData['DailyAttendanceDayType'] === 4) {
                    assetNature = 'L';
                } else if (inputData['DailyAttendanceDayType'] === 5) {
                    assetNature = 'HL';
                } else if (inputData['DailyAttendanceDayType'] === 6) {
                    assetNature = 'CW';
                } else if (inputData['DailyAttendanceDayType'] === 7) {
                    assetNature = 'COff';
                } else if (inputData['DailyAttendanceDayType'] === 8) {
                    assetNature = 'A';
                } else if (inputData['DailyAttendanceDayType'] === 9) {
                    assetNature = 'H';
                }
                

                finalizedGroup.push({
                   sno:index,
                    
                    code: inputData['EmployeeCode'],
                    name:inputData['EmployeeName'],
                    shift: inputData['ShiftCode'],
                    timeend:inputData['ShiftCalEndTime'],
                    timeout:inputData['DailyAttendanceCalOutTime'],
                    early:inputData['DailyAttendanceCalEarlyOut'],
                    daytype: assetNature,
                    type:inputData['DailyAttendanceRemarks'],

                    DailyAttDate: inputData['DailyAttendanceAttendanceDate'],
                    // Depcode:inputData['DepartmentName'],

                });
            }); 
            
            const final = {};
            finalizedGroup.forEach((detailData) => {
                final[detailData.DailyAttDate] ={
                    attendancegroup:{},
                    //sno:detailData.DailyAttDate,
                    code:detailData.DailyAttDate,
                    name:"",
                    shift: "",
                    timeend:"",
                    timeout:"",
                    early:"",
                    daytype: "",
                    remarks:"",
                    ...final[detailData.DailyAttDate]

                };

                final[detailData.DailyAttDate].attendancegroup[detailData.sno] = {
                    //sno:detailData.sno,
                    code: detailData.code,
                    name:detailData.name,
                    shift: detailData.shift,
                    timeend:detailData.timeend,
                    timeout:detailData.timeout,
                    early:detailData.early,
                    daytype: detailData.daytype,
                    remarks:detailData.remarks,
                }
            });

            const supGroups = Object.keys(final);

            const tableData = [];

            supGroups.forEach((codeData) => {
                tableData.push({
                    //sno:final[codeData].sno,
                    code: final[codeData].code,
                    name:"",
                    shift: "",
                    timeend:"",
                    timeout:"",
                    early:"",
                    daytype: "",
                    remarks:"",
                    bold: true,
                });

                const accounts = Object.keys(final[codeData].attendancegroup);
                accounts.forEach((account) => {
                    tableData.push({
                        //sno:final[codeData].attendancegroup[account].sno,
                        code: final[codeData].attendancegroup[account].code,
                        name: final[codeData].attendancegroup[account].name,
                        shift:  final[codeData].attendancegroup[account].shift,
                        timeend: final[codeData].attendancegroup[account].timeend,
                        timeout: final[codeData].attendancegroup[account].timeout,
                        early: final[codeData].attendancegroup[account].early,
                        daytype:  final[codeData].attendancegroup[account].daytype,
                        remarks: final[codeData].attendancegroup[account].type,
                    });

            });

        });
        this.rowDatas=tableData;
      
        console.log("Final Data:",this.rowDatas[0])
      });
      }
}

