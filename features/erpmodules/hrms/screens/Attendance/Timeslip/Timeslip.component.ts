
import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs';
import { Select } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { SharedService } from 'features/commonreport/service/datapassing.service';


@Component({
    selector: 'app-Timeslip',
    templateUrl: './Timeslip.component.html',
    styleUrls: ['./Timeslip.component.scss'],
})
export class TimeslipComponent implements OnInit {
    @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
    public rowDatas: any;
    constructor(public sharedService: SharedService) { }
    ngOnInit(): void {
        this.getTimeslip();
    }

    public getTimeslip() {
        this.sharedService.getRowInfo().subscribe(data => {
            this.rowDatas = data;
            if (data.length > 0) {
                console.log("Timeslip Details :", this.rowDatas)

                let groupingData = this.rowDatas;
                var finalizedGroup = [];

                groupingData.map((inputData, index) => {
                    finalizedGroup.push({
                        sno: index,

                        slipdt: inputData['TimeSlipDate'],
                        slipdtno: inputData['TimeSlipNumber'],
                        attdt: inputData['TimeSlipAttendanceDate'],
                        from: inputData['TimeSlipTimeSlipCalStartTime'],
                        to: inputData['TimeSlipTimeSlipCalEndTime'],
                        official: inputData['TimeSlipDurationHour'],
                        personal: inputData['TimeSlipCalDuration'],
                        status: inputData['Status'],
                        remarks: inputData['TimeSlipReason'],


                        empname: inputData['EmployeeCode'],

                        empcode: inputData['EmployeeName'],

                    });
                });

                const final = {};
                finalizedGroup.forEach((detailData) => {
                    final[detailData.empname] = {
                        attendancegroup: {},
                        //sno:detailData.empname,
                        slipdt: detailData.empname,
                        slipdtno: detailData.empcode,
                        attdt: "",
                        from: "",
                        to: "",
                        official: "",
                        personal: "",
                        status: "",
                        remarks: "",
                        ...final[detailData.empname]

                    };

                    final[detailData.empname].attendancegroup[detailData.sno] = {
                        //sno:detailData.sno,
                        slipdt: detailData.slipdt,
                        slipdtno: detailData.slipdtno,
                        attdt: detailData.attdt,
                        from: detailData.from,
                        to: detailData.to,
                        official: detailData.official,
                        personal: detailData.personal,
                        status: detailData.status,
                        remarks: detailData.remarks,
                    }
                });

                const supGroups = Object.keys(final);

                const tableData = [];

                supGroups.forEach((slipdtData) => {
                    tableData.push({
                        //sno:final[slipdtData].sno,
                        slipdt: final[slipdtData].slipdt,
                        slipdtno: final[slipdtData].slipdtno,
                        attdt: "",
                        from: "",
                        to: "",
                        official: "",
                        personal: "",
                        status: "",
                        remarks: "",
                        bold: true,
                    });

                    const accounts = Object.keys(final[slipdtData].attendancegroup);
                    accounts.forEach((account) => {
                        tableData.push({
                            //sno:final[slipdtData].attendancegroup[account].sno,
                            slipdt: final[slipdtData].attendancegroup[account].slipdt,
                            slipdtno: final[slipdtData].attendancegroup[account].slipdtno,
                            attdt: final[slipdtData].attendancegroup[account].attdt,
                            from: final[slipdtData].attendancegroup[account].from,
                            to: final[slipdtData].attendancegroup[account].to,
                            official: final[slipdtData].attendancegroup[account].official,
                            personal: final[slipdtData].attendancegroup[account].personal,
                            status: final[slipdtData].attendancegroup[account].status,
                            remarks: final[slipdtData].attendancegroup[account].remarks,

                        });

                    });

                });
                this.rowDatas = tableData;

                console.log("Final Data:", this.rowDatas[0])
            }
        });
    }
}

