import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';

import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { HRMSURLS } from '../../URLS/urls';
import { MeasureOptionService } from '../../services/MeasureOption/measureoption.service';
import { IMeasureOption } from '../../models/IMeasureOption';
import * as MeasureOptionJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/MeasureOption.json'
import { DialogBoxComponent } from 'features/erpmodules/samplescreens/screens/DialogBox/DialogBox.component';

@Component({
  selector: 'app-MeasureOption',
  templateUrl: 'measureoption.component.html',
  styleUrls: ['measureoption.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'LovId' },
    { provide: 'url', useValue: HRMSURLS.MeasureOption },
    { provide: 'DataService', useClass: MeasureOptionService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class MeasureOptionsComponent extends GBBaseDataPageComponentWN<IMeasureOption> {
  title: string = "MeasureOption"
  MeasureOptionJSON = MeasureOptionJSON

  form: GBDataFormGroupWN<IMeasureOption> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "MeasureOption", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.MeasureOptionPicklistFillValue(arrayOfValues.LovId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }

// public  GetCodeDefineId(event:any){
//   console.log("Codedefine",event)
//   var codedefineId = event.SelectedData
//   console.log("codedefineId",codedefineId)
//   const codeDefineIdIndex = MeasureOptionJSON.ObjectFields.findIndex(field => field.Name === 'CodeDefineId');
//   console.log("codeDefineIdIndex",codeDefineIdIndex)
//   if (codeDefineIdIndex !== -1) {
//     MeasureOptionJSON.ObjectFields[codeDefineIdIndex].DefaultValue = codedefineId;

//     this.form.get('CodeDefineId').patchValue(codedefineId);
// } else {
//     console.error('Field "CodeDefineId" not found in JSON data.');
// }
// }

  public MeasureOptionPicklistFillValue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(MeasureOption => {
        this.form.patchValue(MeasureOption);
      })
    }
  }


  public MeasureOptionPicklistPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
  validatePercent(event: any): void {
    let value = event.target.value;

    value = Number(value);
    if (!/^\d+$/.test(value)||value > 200) {
      this.form.get('LovScore').setValue(0);
      const dialogRef = this.dialog.open(DialogBoxComponent, {
        width: '400px',
        data: {
          message: 'LOV score must be within 0-200',
          heading: 'Info',
        }
      });
    }
    else {
      this.form.get('LovScore').setValue(value);
    }
  }
  onFileSelected(event: any): void {
    console.log("eventevent",event)
    this.form.get(event.id).patchValue(event.SelectedData);
    console.log("selected code", event.Selected.Code);

    let criteria = {
      "SectionCriteriaList": [{
        "SectionId": 0,
        "AttributesCriteriaList": [
          { "FieldName": "EntityId", "OperationType": 1, "FieldValue": -2147481741, "InArray": null, "JoinType": 2 },
          { "FieldName": "CategoryCode", "OperationType": 5, "FieldValue": event.Selected.Code, "InArray": null, "JoinType": 2 }
        ],
        "OperationType": 0
      }]
    };

    console.log("criteria", criteria);

    this.http.httppost(HRMSURLS.codedefine, criteria).subscribe(
      (response: any) => {
        console.log("response", response);
    
        if (response.length == 0) {
          this.dialog.open(DialogBoxComponent, {
            width: '400px',
            data: {
              message: `Please create codedefine entry for lovtype ${event.Selected.Name}`,
              heading: 'Info',
            }
          });
          console.log('Upload :', response);
        } else {
          console.log('Upload successful:', response);
          const codedefineId = response[0].Id
          console.log("COdedeFineId",response[0].Id)
          const codeDefineIdIndex = MeasureOptionJSON.ObjectFields.findIndex(field => field.Name === 'CodeDefineId');
          console.log("codeDefineIdIndex",codeDefineIdIndex)
          if (codeDefineIdIndex !== -1) {
              MeasureOptionJSON.ObjectFields[codeDefineIdIndex].DefaultValue = codedefineId;
          
              this.form.get('CodeDefineId').patchValue(codedefineId);
          } else {
              console.error('Field "CodeDefineId" not found in JSON data.');
          }
        }
      },
      (error) => {
        console.error('Error:', error);
      }
    );
  }
}

export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IMeasureOption> {
  const dbds: GBBaseDBDataService<IMeasureOption> = new GBBaseDBDataService<IMeasureOption>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IMeasureOption>, dbDataService: GBBaseDBDataService<IMeasureOption>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IMeasureOption> {
  return new GBDataPageService<IMeasureOption>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
