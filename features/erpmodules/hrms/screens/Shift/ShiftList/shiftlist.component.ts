
import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs';
import { Select } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { SharedService } from 'features/commonreport/service/datapassing.service';


@Component({
    selector: 'app-shiftlist',
    templateUrl: './shiftlist.component.html',
    styleUrls: ['./shiftlist.component.scss'],
})
export class shiftlistComponent implements OnInit {
    @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
    public rowData: any;
    constructor(public sharedService: SharedService) { }
    ngOnInit(): void {
        this.shiftlist();
    }

    public shiftlist() {
        this.sharedService.getRowInfo().subscribe(data => {
            this.rowData = data;
            if (data.length > 0) {
            console.log("shiftlist Details :", this.rowData)
            }
            
            
        });
    }
}
