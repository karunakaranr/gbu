import { Component, Inject, LOCALE_ID, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { Observable } from 'rxjs';
import { SharedService } from 'features/commonreport/service/datapassing.service';
import { IPunchDetail } from '../../models/IPunchDetail';
// import { MapComponent } from 'features/erpmodules/samplescreens/screens/Map/map.component';
const mapdata= require("./../../../samplescreens/screens/Map/map.json")
@Component({
  selector: 'app-SitePunch',
  templateUrl: './SitePunch.component.html',
  styleUrls: ['./SitePunch.component.scss']
})

export class SitePunchComponent implements OnInit {
  RowData: IPunchDetail[];
  constructor(public sharedService: SharedService,@Inject(LOCALE_ID) public locale: string ) { }
  ngOnInit(): void {
      this.DataReceiver();
  }

  public DataReceiver() {
    console.log("mapdata:",mapdata)
    this.RowData = mapdata.tabledata;
    // this.sharedService.getRowInfo().subscribe(data => {
    //   this.RowData = data
    //   console.log("Site Punch Data:", data)
    // });
  }
  
}