import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ISettings } from 'features/erpmodules/hrms/models/ISettings';
import { SettingsService } from 'features/erpmodules/hrms/services/AdditionDeduction/Settings/settings.service';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { HRMSURLS } from 'features/erpmodules/hrms/URLS/urls';
import  * as SettingsJSON from './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/Settings.json'

@Component({
  selector: 'app-Settings',
  templateUrl: 'settings.component.html',
  styleUrls: ['settings.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'AdditionDeductionId' },
    { provide: 'url', useValue:HRMSURLS.Settings },
    { provide: 'DataService', useClass: SettingsService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class SettingsComponent extends GBBaseDataPageComponentWN<ISettings> {
  title: string = "Settings"
  SettingsJSON = SettingsJSON;

  form: GBDataFormGroupWN<ISettings> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "Settings", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.SettingsValue(arrayOfValues.AdditionDeductionId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }
  public settingPatchValue(): void {

    if (this.form.get('AdditionDeductionFieldDataType').value == "0") { 
        this.form.get('AdditionDeductionFieldDataSize').patchValue('0');

    }
    if (this.form.get('AdditionDeductionFieldDataType').value == "1") { 
        this.form.get('AdditionDeductionFieldDataSize').patchValue('100');
         this.form.get('AdditionDeductionDefaultValue').patchValue('0');
    }
    if (this.form.get('AdditionDeductionFieldDataType').value == "2") { 
        this.form.get('AdditionDeductionFieldDataSize').patchValue('18.2');
        this.form.get('AdditionDeductionDefaultValue').patchValue('');
    }

    if (this.form.get('AdditionDeductionFieldDataType').value == "3") { 
        this.form.get('AdditionDeductionFieldDataSize').patchValue('100');
       
    }
    }

  public SettingsValue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(Settings => {
        this.form.patchValue(Settings);
      })
    }
  }
  public SettingsNot(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}
export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<ISettings> {
  const dbds: GBBaseDBDataService<ISettings> = new GBBaseDBDataService<ISettings>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<ISettings>, dbDataService: GBBaseDBDataService<ISettings>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<ISettings> {
  return new GBDataPageService<ISettings>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
