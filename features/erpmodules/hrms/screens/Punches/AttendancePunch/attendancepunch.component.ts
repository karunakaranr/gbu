import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Select, Store } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';


@Component({
  selector: 'app-attendancepunch',
  templateUrl: './attendancepunch.component.html',
  styleUrls: ['./attendancepunch.component.scss'],
})
export class attendancepunchComponent implements OnInit {
  @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
  public rowData = [];  
 

  constructor(public store: Store) { }
  ngOnInit(): void {
    this.attendancepunchfun();
  }
  
  public attendancepunchfun() {
    this.rowdatacommon$.subscribe(data => {
      this.rowData = data;
      console.log("Attendance Punch:", this.rowData)
    });
  }

}
