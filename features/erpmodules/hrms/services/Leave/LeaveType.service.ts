import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';

import { ILeaveType } from '../../models/ILeaveType';



 
@Injectable({
  providedIn: 'root'
})
export class LeaveTypeService extends GBBaseDataServiceWN<ILeaveType> {

  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<ILeaveType>) {
    super(dbDataService, 'LeaveId');
  }
}
