import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';

import { IEmployeeQualification } from '../../models/IEmployeeQualification';
 
@Injectable({
  providedIn: 'root'
})
export class EmployeeQualificationService extends GBBaseDataServiceWN<IEmployeeQualification> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IEmployeeQualification>) {
    super(dbDataService, 'Employee.Id');
  }
}

