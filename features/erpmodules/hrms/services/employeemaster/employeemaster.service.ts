
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { GBBaseDataServiceWN} from './../../../../../libs/gbdata/src/lib/services/gbbasedata.service';

import { EmployeeDBService } from '../../dbservices/employeemaster/employeemaster.dbservice';
import { IEmployee } from '../../models/IEmployee';


@Injectable({
  providedIn: 'root'
})
export class EmployeeService extends GBBaseDataServiceWN<IEmployee> {
  previous:boolean;
  constructor(public dbDataService: EmployeeDBService) {
    super(dbDataService, 'EmployeeId');
  }

  saveData(data: IEmployee): Observable<any> {
    return super.saveData(data);
  }

  getData(id: string): Observable<IEmployee> {
    return super.getData(id);
  }
  deleteData(id: any): Observable<any> {
    return super.deleteData(id);
  }
  getAll(): Observable<any> {
    this.dbDataService.previous= this.previous;
    return super.getAll();
  }

  moveFirst(): Observable<IEmployee> {
    return super.moveFirst();
  }

  movePrev(): Observable<IEmployee> {
    return super.movePrev();
  }

  moveNext(): Observable<IEmployee> {
    return super.moveNext();
  }

  moveLast(): Observable<IEmployee> {
    return super.moveLast();
  }
}
