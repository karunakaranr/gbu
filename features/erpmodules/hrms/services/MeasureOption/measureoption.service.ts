import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { IMeasureOption } from '../../models/IMeasureOption';




 
@Injectable({
  providedIn: 'root'
})
export class MeasureOptionService extends GBBaseDataServiceWN<IMeasureOption> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IMeasureOption>) {
    super(dbDataService, 'LovId');
  }
}
