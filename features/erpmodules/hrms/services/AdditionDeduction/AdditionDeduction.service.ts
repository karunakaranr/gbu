import { Injectable } from '@angular/core';
import { AdditionDeductionDbService } from './../../dbservices/AdditionDeduction/AdditionDeductionDb.service';
@Injectable({
  providedIn: 'root'
})
export class AdditionDeduction {
  constructor(public dbservice: AdditionDeductionDbService) { }

  public additiondeductionview(){                    
    return this.dbservice.picklist();
  }

  public Rowservice(obj){                  
    return this.dbservice.reportData(obj);
  }
}