import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';


import { ITimeSlip } from '../../models/ITimeSlip';  


 
@Injectable({
  providedIn: 'root'
})
export class TimeSlipService extends GBBaseDataServiceWN<ITimeSlip> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<ITimeSlip>) {
    super(dbDataService, 'TimeSlipId');
  }
}
