import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';

import { IKeyResultArea } from 'features/erpmodules/hrms/models/IKeyResultArea';



 
@Injectable({
  providedIn: 'root'
})
export class KeyResultAreaService extends GBBaseDataServiceWN<IKeyResultArea> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IKeyResultArea>) {
    super(dbDataService, 'KRAId');
  }
}
