import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { IPolicy } from '../../models/IPolicy';



 
@Injectable({
  providedIn: 'root'
})
export class PolicyService extends GBBaseDataServiceWN<IPolicy> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IPolicy>) {
    super(dbDataService, 'PolicyId');
  }
}
