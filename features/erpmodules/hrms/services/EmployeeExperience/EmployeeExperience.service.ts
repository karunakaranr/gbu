import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';


import { IEmployeeExperience } from '../../models/IEmployeeExperience';


 
@Injectable({
  providedIn: 'root'
})
export class EmployeeExperienceService extends GBBaseDataServiceWN<IEmployeeExperience> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IEmployeeExperience>) {
    super(dbDataService, 'Employee.Id');
  }
}
