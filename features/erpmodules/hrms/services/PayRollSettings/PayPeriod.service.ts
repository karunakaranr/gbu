import { Inject, Injectable } from '@angular/core';

import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';

import { IPayPeriod } from '../../models/IPayPeriod';


@Injectable({
  providedIn: 'root'
})
export class PayPeriodService extends GBBaseDataServiceWN<IPayPeriod> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IPayPeriod>) {
    super(dbDataService, 'PayPeriodId');
  }
}