import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';

import { IGratuitySettings } from 'features/erpmodules/hrms/models/IGratuitySettings';



 
@Injectable({
  providedIn: 'root'
})
export class GratuitySettingsService extends GBBaseDataServiceWN<IGratuitySettings> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IGratuitySettings>) {
    super(dbDataService, 'GratuityGroupId');
  }
}
