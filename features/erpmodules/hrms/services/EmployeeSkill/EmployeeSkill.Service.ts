import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';

import { ISelectionRound } from 'features/erpmodules/hrms/models/ISelectionRound';

import { IEmployeeSkill } from '../../models/IEmployeeSkill';

 
@Injectable({
  providedIn: 'root'
})
export class EmployeeSkillService extends GBBaseDataServiceWN<IEmployeeSkill> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IEmployeeSkill>) {
    super(dbDataService, 'Employee.Id');
  }
}
