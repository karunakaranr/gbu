import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';


import { IShiftMaster } from 'features/erpmodules/hrms/models/IShiftMaster';

 
@Injectable({
  providedIn: 'root'
})
export class ShiftMasterService extends GBBaseDataServiceWN<IShiftMaster> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IShiftMaster>) {
    super(dbDataService, 'ShiftId');
  }
}
