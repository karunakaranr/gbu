import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';

import { IActivityRecruitmentProcess } from '../../models/IActivityRecruitmentProcess';



 
@Injectable({
  providedIn: 'root'
})
export class ActivityRecruitmentProcessService extends GBBaseDataServiceWN<IActivityRecruitmentProcess> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IActivityRecruitmentProcess>) {
    super(dbDataService, 'ActivityId');
  }
}
