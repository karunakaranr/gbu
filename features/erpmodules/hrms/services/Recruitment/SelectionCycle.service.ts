import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { ISelectionCycle } from '../../models/ISelectionCycle';


 
@Injectable({
  providedIn: 'root'
})
export class SelectionCycleService extends GBBaseDataServiceWN<ISelectionCycle> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<ISelectionCycle>) {
    super(dbDataService, 'SelectionCycleId');
  }
}
