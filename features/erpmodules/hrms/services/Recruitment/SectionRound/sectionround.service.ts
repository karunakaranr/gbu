import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';

import { ISelectionRound } from 'features/erpmodules/hrms/models/ISelectionRound';



 
@Injectable({
  providedIn: 'root'
})
export class SelectionRoundService extends GBBaseDataServiceWN<ISelectionRound> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<ISelectionRound>) {
    super(dbDataService, 'SelectionRoundId');
  }
}
