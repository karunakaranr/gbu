import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';

import { ISubject } from 'features/erpmodules/hrms/models/ISubject';




 
@Injectable({
  providedIn: 'root'
})
export class SubjectService extends GBBaseDataServiceWN<ISubject> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<ISubject>) {
    super(dbDataService, 'SubjectId');
  }
}
