import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';

import { IPayGroup } from 'features/erpmodules/hrms/models/IPayGroup';



 
@Injectable({
  providedIn: 'root'
})
export class PayGroupService extends GBBaseDataServiceWN<IPayGroup> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IPayGroup>) {
    super(dbDataService, 'PayGroupId');
  }
}
