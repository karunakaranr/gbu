import { Injectable } from '@angular/core';

import { StageDBService } from 'features/erpmodules/hrms/dbservices/TypeMaster/Stage/stagedb.service';


@Injectable({
  providedIn: 'root'    
})
export class StageService {
  constructor(public dbDataService: StageDBService) {
  }

  getall(gcmid){
    return this.dbDataService.getall(gcmid);
  }

  getid(id){
    return this.dbDataService.getid(id);
  }

}
