import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { IPickupPlace } from 'features/erpmodules/hrms/models/IPickupPlace ';
 
@Injectable({
  providedIn: 'root'
})
export class PickupPlaceService extends GBBaseDataServiceWN<IPickupPlace> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IPickupPlace>) {
    super(dbDataService, 'VillageId');
  }
}
