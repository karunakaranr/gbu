import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';

import { IDAPointGroup } from 'features/erpmodules/hrms/models/IDAPointGroup';



 
@Injectable({
  providedIn: 'root'
})
export class DAPointGroupService extends GBBaseDataServiceWN<IDAPointGroup> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IDAPointGroup>) {
    super(dbDataService, 'DAPointGroupId');
  }
}
