import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { IShiftPattern } from '../../models/IShiftPattern';

@Injectable({
  providedIn: 'root'
})

export class ShiftPatternService extends GBBaseDataServiceWN<IShiftPattern> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IShiftPattern>) {
    super(dbDataService, 'ShiftPatternId');
  }
}