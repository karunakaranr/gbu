import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { IEmployeeNominee } from '../../models/IEmployeeNominee';


 
@Injectable({
  providedIn: 'root'
})
export class EmployeeNomineeService extends GBBaseDataServiceWN<IEmployeeNominee> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IEmployeeNominee>) {
    super(dbDataService, 'EmployeeNomineeId',"EmployeeNomineeType");
  }
}
