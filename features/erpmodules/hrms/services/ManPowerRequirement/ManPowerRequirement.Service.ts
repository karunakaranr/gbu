import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';

import { IManPowerRequirement } from '../../models/IManPowerRequirement';



 
@Injectable({
  providedIn: 'root'
})
export class ManPowerRequirementService extends GBBaseDataServiceWN<IManPowerRequirement> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IManPowerRequirement>) {
    super(dbDataService, 'ManPowerId');
  }
}
