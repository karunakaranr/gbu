import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';

import { IPayElement } from '../../models/IPayElement';



 
@Injectable({
  providedIn: 'root'
})
export class PayElementService extends GBBaseDataServiceWN<IPayElement> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IPayElement>) {
    super(dbDataService, 'PayelementId');
  }
}
