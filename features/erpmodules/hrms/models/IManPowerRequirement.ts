export interface IManPowerRequirement {
  ManPowerId: number
  ManPowerVersionNo: number
  ManPowerFromDate: string
  ManPowerToDate: string
  ManPowerRemarks: string
  ManPowerDetailArray: ManPowerDetailArray[]
  OUId: number
  OUCode: string
  OUName: string
  ManPowerCreatedById: number
  ManPowerCreatedOn: string
  ManPowerModifiedById: number
  ManPowerModifiedOn: string
  ManPowerSortOrder: number
  ManPowerStatus: number
  ManPowerVersion: number
  ManPowerSourceType: number
  FunctionReportingToMailId: any
  FunctionReportingToMobileNumber: any
  AdminReportingToMailId: any
  AdminReportingToMobileNumber: any
  FunctionAndAdminReportingToMailId: any
  FunctionAndAdminReportingToMobileNumber: any
}

export interface ManPowerDetailArray {
  DepartmentId: number
  DepartmentCode: string
  DepartmentName: string
  ManPowerId: number
  ManPowerVersion: number
  ManPowerFromDate: string
  ManPowerToDate: string
  ManPowerRemarks: string
  ManPowerDetailId: number
  ManPowerDetailSlno: number
  ManPowerDetailRequired: number
  ShiftId: number
  ShiftCode: string
  ShiftDescription: string
  WorkTypeId: number
  WorkTypeCode: string
  WorkTypeName: string
  ManPowerDetailCreatedById: number
  ManPowerDetailCreatedOn: string
  ManPowerDetailModifiedById: number
  ManPowerDetailModifiedOn: string
  ManPowerDetailBaseClinetId: number
  ManPowerDetailSortOrder: number
  ManPowerDetailStatus: number
  ManPowerDetailVersion: number
  ManPowerDetailSourceType: number
}
