export interface IPerformanceMeasure {
    MeasureId: number
    MeasureName: string
    MeasureDescription: string
    LovTypeId: number
    LovTypeCode: string
    LovTypeName: string
    ScoreRequired: number
    KRAId: number
    KRAName: string
    KRADescription: string
    MeasureIsTextName: string
    MeasureIsText: number
    MeasureRemarks: string
    MeasureSourceType: number
    MeasureSortOrder: number
    MeasureStatus: number
    MeasureVersion: number
    MeasureCreatedById: number
    MeasureCreatedOn: string
    MeasureModifiedById: number
    MeasureModifiedOn: string
  }
  