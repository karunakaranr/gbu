export interface IPayGroup {

    PayGroupId: number
    PayGroupCode: string
    PayGroupName: string
    PayGroupSortOrder: number
    PayGroupStatus: number
    PayGroupVersion: number
    PayGroupCreatedById: number
    PayGroupCreatedOn: string
    PayGroupModifiedById: number
    PayGroupModifiedOn: string


}