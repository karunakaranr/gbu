export interface IDAPointGroup{
    DAPointGroupId: number
    DAPointGroupCode: string
    DAPointGroupName: string
    DAPointGroupDAPointGroupSet: string
    DAPointGroupBaseIndex: number
    DAPointGroupRatePerPoint: number
    DAPointGroupSortOrder: number
    DAPointGroupStatus: number
    DAPointGroupVersion: number
    DAPointGroupCreatedById: number
    DAPointGroupCreatedOn: string
    DAPointGroupModifiedById: number
    DAPointGroupModifiedOn: string

}