export interface IMeasureOption {
    LovId: number
    LovCode: string
    LovName: string
    LovTypeId: number
    LovTypeCode: string
    LovTypeName: string
    LovTypeScoreRequired: number
    LovScore: number
    LovCreatedById: number
    LovCreatedOn: string
    LovCreatedByName: string
    LovModifiedById: number
    LovModifiedOn: string
    LovModifiedByName: string
    LovSortOrder: number
    LovStatus: number
    LovVersion: number
    LovSourceType: number
    ParentId: number
    ParentCode: string
    ParentName: string
    CodeDefineId: number
  }
  