export interface ISkillMaster{
    SkillId: number
  SkillCode: string
  SkillName: string
  SkillTypeId: number
  SkillTypeCode: string
  SkillTypeName: string
  SkillRemarks: string
  ClientId: number
  ClientCode: string
  ClientName: string
  DomainId: number
  DomainCode: string
  DomainName: string
  SkillSortOrder: number
  SkillStatus: number
  SkillVersion: number
  SkillSourceType: number
  SkillCreatedById: number
  SkillCreatedOn: string
  SkillCreatedByName: any
  SkillModifiedById: number
  SkillModifiedOn: string
  SkillModifiedByName: any

}