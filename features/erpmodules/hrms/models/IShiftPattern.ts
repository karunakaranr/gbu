export interface IShiftPattern {
    ShiftPatternId: number
    ShiftPatternCode: string
    ShiftPatternName: string
    ShiftPatternType: number
    ShiftPatternDates: string
    ShiftPatternDays: number
    ShiftPatternSortOrder: number
    ShiftPatternStatus: number
    ShiftPatternVersion: number
    ShiftPatternCreatedById: number
    ShiftPatternCreatedOn: string
    ShiftPatternModifiedById: number
    ShiftPatternModifiedOn: string
    ShiftPatternDaysArray: ShiftPatternDaysArray[]
  }
  
  export interface ShiftPatternDaysArray {
    ShiftPatternDaysId: number
    ShiftPatternId: number
    ShiftPatternCode: string
    ShiftPatternName: string
    ShiftPatternDaysSlNo: number
    ShiftPatternDaysDayType: number
    ShiftId: number
    ShiftCode: string
    ShiftDescription: string
    ShiftPatternDaysNumberofDays: number
    ShiftPatternDetailArray: ShiftPatternDetailArray[]
  }
  
  export interface ShiftPatternDetailArray {
    ShiftPatternDetailId: number
    ShiftPatternDaysId: number
    ShiftPatternDetailSlNo: number
    ShiftPatternDetailDayType: number
    ShiftPatternDetailDaysType: any
    ShiftId: number
    ShiftCode: string
    ShiftDescription: string
  }