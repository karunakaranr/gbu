export interface IEmployeeReference {
    EmployeeReferenceId: number
    EmployeeId: number
    EmployeeCode: string
    EmployeeName: string
    EmployeeReferenceSlNo: number
    EmployeeReferenceReference: string
    EmployeeReferenceReferenceCompany: string
    EmployeeReferenceDesignation: string
    EmployeeReferenceRelationship: string
    EmployeeReferenceTimeKnown: string
    EmployeeReferenceIsVerified: number
    VerifiedById: number
    VerifiedByCode: string
    VerifiedByName: string
    EmployeeReferenceVerifiedOn: string
    EmployeeReferenceRemarks: string
  }
  