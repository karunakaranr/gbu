export interface IPayTaxSetting {
    PayTaxTypeId: number
    PayTaxTypeCode: string
    PayTaxTypeName: string
    PayTaxTypeRemarks: string
    PeriodId: number
    PeriodCode: string
    PeriodName: string
    PayTaxTypeDetailArray: PayTaxTypeDetailArray[]
    PayTaxTypeSourceType: number
    PayTaxTypeSortOrder: number
    PayTaxTypeStatus: number
    PayTaxTypeVersion: number
    PayTaxTypeCreatedById: number
    PayTaxTypeCreatedOn: string
    PayTaxTypeModifiedById: number
    PayTaxTypeModifiedOn: string
  }
  
  export interface PayTaxTypeDetailArray {
    PayTaxTypeDetailId: number
    PayTaxTypeDetailSlNo: number
    PayTaxTypeDetailFieldName: string
    PayTaxTypeDetailDisplayName: string
    PayTaxTypeDetailFieldRemarks: string
    PayTaxTypeDetailType: number
    PayTaxTypeDetailDataType: number
    PayTaxTypeDetailSize: number
    PayTaxTypeDetailIsDetail: number
    PayTaxTypeDetailIsAttachment: number
    PayTaxTypeDetailSection: string
    PayTaxTypeDetailDisplayOrder: number
    PayTaxTypeDetailEnumValues: string
    PayTaxTypeId: number
    PayTaxTypeCode: string
    PayTaxTypeName: string
    TempPayTaxTypeDetailSettingArray: any[]
  }
  