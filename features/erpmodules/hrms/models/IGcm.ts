export interface IGcm {

    SaveorUpdate: any
    GcmId: number
    GcmCode: string
    GcmName: string
    GcmTypeId: number
    GcmTypeCode: string
    GcmTypeName: any
    GcmTypeDescription: string
    GcmColor: number
    GcmRemarks: string
    GcmCreatedById: number
    GcmCreatedOn: string
    GcmModifiedById: number
    GcmModifiedOn: string
    GcmCreatedByName: any
    GcmModifiedByName: any
    GcmSortOrder: number
    GcmStatus: number
    GcmVersion: number
    GcmSourceType: number
    ModuleCode: any
    ModuleName: any

}