export interface IEmployeeNominee {
    EmployeeCode(EmployeeCode: any): unknown
    EmployeeName(EmployeeName: any): unknown
    EmployeeNomineeNomineeName: string
    EmployeeNomineeRelation: string
    EmployeeNomineeDOB: string
    EmployeeNomineePercentage: string
    EmployeeNomineeAadharNumber: string
    EmployeeNomineeAddress: string
    EmployeeNomineeGuardian: string
    EmployeeNomineeGuardianAddress: string
    EmployeeId: string
    EmployeeNomineeNomineeType: string
    CheckingCopyFromOption: number
  }
  