export interface IPayElement {
    PayelementId: number
    PayelementCode: string
    PayelementName: string
    PayelementPeriodType: number
    PayelementPeriodTypeName: string
    PayelementCostType: number
    PayelementCostTypeName: string
    PayelementSalaryType: number
    PayelementSalaryTypeName: string
    PayelementSortOrder: number
    PayelementStatus: number
    PayelementVersion: number
    PayelementCreatedById: number
    PayelementCreatedOn: string
    PayelementCreatedByName: any
    PayelementModifiedById: number
    PayelementModifiedOn: string
    PayelementModifiedByName: any

}