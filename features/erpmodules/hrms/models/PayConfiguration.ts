export interface Root {
    PayConfigurationId: number
    PayConfigurationCode: string
    PayConfigurationName: string
    SalaryAccountId: number
    SalaryAccountCode: string
    SalaryAccountName: string
    PayConfigurationSortOrder: number
    PayConfigurationStatus: number
    PayConfigurationVersion: number
    PayConfigurationSourceType: number
    PayConfigurationAddon: PayConfigurationAddon[]
    PayConfigurationCreatedById: number
    PayConfigurationCreatedByName: string
    PayConfigurationCreatedOn: string
    PayConfigurationModifiedById: number
    PayConfigurationModifiedByName: string
    PayConfigurationModifiedOn: string
    PayConfigurationDetailArray: PayConfigurationDetailArray[]
    StdSalaryGrossFormulaId: number
    StdSalaryGrossFormulaCode: string
    StdSalaryGrossFormulaName: string
    StdOneDaySalaryFormulaId: number
    StdOneDaySalaryFormulaCode: string
    StdOneDaySalaryFormulaName: string
    PayConfigurationIsPartialProcessRequired: number
    TotalEarningFormulaId: number
    TotalEarningFormulaCode: string
    TotalEarningFormulaName: string
    TotalDeductionFormulaId: number
    TotalDeductionFormulaCode: string
    TotalDeductionFormulaName: string
    NetSalaryFormulaId: number
    NetSalaryFormulaCode: string
    NetSalaryFormulaName: string
    C2CFormulaId: number
    C2CFormulaCode: string
    C2CFormulaName: string
    ReportFormatId: number
    ReportFormatName: string
  }
  
  export interface PayConfigurationAddon {
    Id: number
    FeAddon: string
    GetAddon: string
    Attributes: Attributes
  }
  
  export interface Attributes {}
  
  export interface PayConfigurationDetailArray {
    PayConfigurationDetailId: number
    PayConfigurationId: number
    PayConfigurationDetailSlNo: number
    PayConfigurationDetailFieldType: number
    PayelementId: number
    PayelementCode: string
    PayelementName: string
    PayConfigurationDetailFieldCode: string
    PayConfigurationDetailOperationType: number
    PayConfigurationDetailIsSeprateAccount: number
    SalaryAccountId: number
    SalaryAccountCode: string
    SalaryAccountName: string
    PayConfigurationDetailIsEmployeeAccount: number
    ContraAccountId: number
    ContraAccountCode: string
    ContraAccountName: string
    PayConfigurationDetailIsContraAccountMerge: number
    PayConfigurationDetailIsSalaryAccountMerge: number
    PayConfigurationDetailActualFieldType: number
    PayConfigurationDetailActualFieldName: string
    PayConfigurationDetailIsSalaryCostCenter: number
    PayConfigurationDetailIsContraCostCenter: number
    PayConfigurationDetailClaimType: number
    ClaimHeadId: number
    ClaimHeadCode: string
    ClaimHeadName: string
    PayConfigurationDetailPrintRequired: number
  }
  