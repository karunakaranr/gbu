export interface ICasteCategory {
  CasteCategoryId: number
  CasteCategoryCode: string
  CasteCategoryName: string
  CasteCategoryReservationPercentage: number
  CasteCategorySourceType: number
  CasteCategorySortOrder: number
  CasteCategoryStatus: number
  CasteCategoryVersion: number
  CasteCategoryCreatedById: number
  CasteCategoryCreatedOn: string
  CasteCategoryCreatedByName: string
  CasteCategoryModifiedById: number
  CasteCategoryModifiedOn: string
  CasteCategoryModifiedByName: string
  CasteCategoryDetailArray: CasteCategoryDetailArray[]
}

export interface CasteCategoryDetailArray {
  CasteCategoryDetailId: number
  CasteId: number
  CasteCode: string
  CasteName: string
  CasteCategoryId: number
  CasteCategoryCode: string
  CasteCategoryName: string
  CasteCategoryDetailSlNo: number
}

  