export interface ISelectionRound {
    SelectionRoundId: number
    SelectionRoundCode: string
    SelectionRoundName: string
    SelectionRoundRemarks: string
    SelectionRoundDetailArray: SelectionRoundDetailArray[]
    SelectionRoundCreatedById: number
    SelectionRoundCreatedOn: string
    SelectionRoundModifiedById: number
    SelectionRoundModifiedOn: string
    SelectionRoundSortOrder: number
    SelectionRoundStatus: number
    SelectionRoundVersion: number
    SelectionRoundSourceType: number
  } 
  export interface SelectionRoundDetailArray {
    SelectionRoundId: number
    SelectionRoundCode: string
    SelectionRoundName: string
    SelectionRoundRemarks: string
    SelectionRoundDetailId: number
    SelectionRoundDetailSlNo: number
    SelectionRoundDetailWeightage: number
    SkillId: number
    SkillCode: string
    SkillName: string
  }