export interface IKeyResultArea{

    KRAId: number
    KRAName: string
    KRADescription: string
    KRASection: string
    KRANatureName: string
    KRANature: number
    DepartmentId: number
    DepartmentCode: string
    DepartmentName: string
    KRARemarks: string
    KRASourceType: number
    KRASortOrder: number
    KRAStatus: number
    KRAVersion: number
    KRACreatedById: number
    KRACreatedOn: string
    KRACreatedByName: any
    KRAModifiedById: number
    KRAModifiedOn: string
    KRAModifiedByName: any


}