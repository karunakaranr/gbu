export interface IMeasureOptionsType {

    LovTypeId: number
    LovTypeCode: string
    LovTypeName: string
    LovTypeScoreRequired: number
    LovTypeCreatedById: number
    LovTypeCreatedOn: string
    LovTypeCreatedByName: any
    LovTypeModifiedById: number
    LovTypeModifiedOn: string
    LovTypeModifiedByName: any
    LovTypeSortOrder: number
    LovTypeStatus: number
    LovTypeVersion: number
    LovTypeSourceType: number
    LovTypeLovNature: number
    LovTypeGenerationType: number
    ParentLovTypeId: number
    ParentLovTypeCode: string
    ParentLovTypeName: string

}