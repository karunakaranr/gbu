export interface IPolicy {
    PolicyId: number
    PolicyCode: string
    PolicyName: string
    PolicyDescription: string
    PolicyPageId: number
    CurrencyId: number
    CurrencyCode: string
    CurrencyName: string
    PolicyVersionArray: PolicyVersionArray[]
    PolicyVersion: number
    IsUpdate: number
  }
  
  export interface PolicyVersionArray {
    PolicyVersionId: number
    PolicyId: number
    PolicyCode: string
    PolicyName: string
    PolicyVersionVersion: number
    PolicyVersionFromDate: string
    PolicyVersionToDate: string
    PolicyVersionRemarks: string
    PolicyVersionDetailArray: PolicyVersionDetailArray[]
  }
  
  export interface PolicyVersionDetailArray {
    PolicyVersionDetailId: number
    PolicyVersionParentId: number
    PolicyVersionDetailSlNo: number
    HeadId: number
    HeadCode: string
    HeadName: string
    CityClassId: number
    CityClassCode: string
    CityClassName: string
    SalaryGradeId: number
    SalaryGradeCode: string
    SalaryGradeName: string
    ModeId: number
    ModeCode: string
    ModeName: string
    VehicleTypeId: number
    VehicleTypeCode: string
    VehicleTypeName: string
    PolicyVersionDetailAmount: number
    PolicyVersionDetailRemarks: string
  }
  