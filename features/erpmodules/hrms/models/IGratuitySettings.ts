export interface IGratuitySettings
{
    GratuityGroupId: number
    GratuityGroupCode: string
    GratuityGroupName: string
    GratuityGroupEligibilityDays: number
    GratuityGroupEligibilityYears: number
    GratuityGroupSortOrder: number
    GratuityGroupStatus: number
    GratuityGroupVersion: number
    GratuityGroupCreatedById: number
    GratuityGroupCreatedOn: string
    GratuityGroupModifiedById: number
    GratuityGroupModifiedOn: string
    GratuityGroupDetailArray: GratuityGroupDetailArray[]
  }
  
  export interface GratuityGroupDetailArray {
    GratuityGroupDetailId: number
    GratuityGroupId: number
    GratuityGroupCode: string
    GratuityGroupName: string
    GratuityGroupDetailSlNo: number
    GratuityGroupDetailYearsFrom: number
    GratuityGroupDetailYearsTo: number
    GratuityGroupDetailNoOfDays: number
    BasedOnId: number
    BasedOnCode: string
    BasedOnName: string
  }