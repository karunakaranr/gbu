export type IEmployeeExperience = IEmployeeExperienceDetailArray[]

export interface IEmployeeExperienceDetailArray {
  EmployeeExperienceId: string
  EmployeeExperienceFromMonthYear: string
  EmployeeExperienceToMonthYear: string
  EmployeeId: string
  EmployeeExperienceSlno: number
  EmployeeExperienceEmployer: string
  EmployeeExperienceCity: string
  FromMonth: string
  FromMonthYear: number
  ToMonth: string
  ToMonthYear: number
  EmployeeExperienceNumberOfYear: string
  EmployeeExperienceDesignation: string
  EmployeeExperienceSalaryDrawn: string
  EmployeeExperienceReasonForLeaving: string
}