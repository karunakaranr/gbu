import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { UicoreModule } from '@goodbooks/uicore';
import { UifwkUifwkmaterialModule } from '@goodbooks/uifwk/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslocoRootModule } from './../../transloco/transloco-root.module';
import { GbdataModule } from '@goodbooks/gbdata';
import { environment } from '../../environments/environment';
import { NgBreModule } from '@unisoft/ng-bre';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { GbchartModule } from '../../common/shared/screens/gbchart/gbchart.module';
import { EntitylookupModule } from './../../entitylookup/entitylookup.module';
import { geolocationModule } from './../../geolocation/geolocation.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { AgGridModule } from 'ag-grid-angular';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule } from '@angular/material/button';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { MatDividerModule } from '@angular/material/divider';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatTabsModule } from '@angular/material/tabs';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { AgmCoreModule } from '@agm/core';
import { AgmDirectionModule } from 'agm-direction';
import { GbgridModule } from 'features/gbgrid/components/gbgrid/gbgrid.module';
import { CommonReportModule } from './../../commonreport/commonreport.module';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MaterialModule } from 'features/common/material.module';
import { MatInputModule } from '@angular/material/input';
import { SamplescreensModule } from '../samplescreens/samplescreens.module';

import { NgxsFormPluginModule } from '@ngxs/form-plugin';
import { EmployeemasterlistComponent } from './screens/employee/employeelist/employeemasterlist.component';
import { EmployeemasterformComponent } from './screens/employee/employeemasterform/employeemasterform/employeemasterform';

import { VillageListComponent } from './screens/VillageMaster/VillageList/VillageList.component';
import { HrmsRoutingModule } from './hrms-routing.module';
import { HrmsComponent } from './hrms.component';
import { EmployeeState } from './stores/employeemaster/employeemaster.state';
import { EmployeeComponent } from './screens/employee/employeetable/employee.component';
import { EmployeeSkillListComponent } from './screens/employee/EmployeeSkill/EmployeeSkillList/EmployeeSkillList.component';
import { PayRevisionListComponent } from './screens/employee/PayRevisionList/PayRevision.component';
import { AdditionDeductionComponent } from './screens/AdditionDeduction/AdditionDeduction.component';
import { AppraisalDashBoardComponent } from './screens/AppraisalDashBoard/AppraisalDashBoard.component';
import { AppraisalSummarycomponent } from './screens/AppraisalSummary/AppraisalSummary.component';
import { StageComponent } from './screens/TypeMaster/Stage/stage.component';

// import { MapComponent } from '../samplescreens/screens/Map/map.component';
import { SitePunchComponent } from './screens/SitePunch/SitePunch.component';
import { leaverequestComponent } from './screens/LeaveRequest/LeaveRequest/leaverequest.component';
import { shiftlistComponent } from './screens/Shift/ShiftList/shiftlist.component';
import { DailyAttendanceComponent } from './screens/Attendance/Daily Attendance List/DailyAttendance.component';
import { MonthlyAttendanceComponent } from './screens/Attendance/Monthly Attendance/MonthlyAttendance.component';
import { TimeslipComponent } from './screens/Attendance/Timeslip/Timeslip.component';
import { EmployeeVsShiftComponent } from './screens/EmployeeVsShift/EmployeeVsShiftView/EmployeeVsShift.component';
import { LateArrivalComponent } from './screens/Attendance/Late Arrival HTML View/LateArrivalView.component';
import { attendancepunchComponent } from './screens/Punches/AttendancePunch/attendancepunch.component';
import { EmployeedetailComponent } from './screens/employee/EmployeeDetail/employeedetail.component';

import { MeasureOptionsTypeComponent } from './screens/TypeMaster/MeasureOptionsType/measureoptionstype.component';
import { EarlyDepatureComponent } from './screens/Attendance/Early Depature/EarlyDepature.component';

import { PickupPlaceComponent } from './screens/TypeMaster/PickupPlace/pickupplace.component';
import { DAPointGroupComponent } from './screens/TypeMaster/DAPointGroup/dapointgroup.component';
import { TimeSlipComponent } from './screens/Timeslip/TimeSlip.component';
import { SkillMasterComponent } from './screens/EmployeeDetails/SkillMaster/skillmaster.component';
import { LeaveTypeComponent } from './screens/Leave/LeaveType/leavetype.component';
import { KeyResultAreaComponent } from './screens/KeyResultArea/keyresultarea.component';
import { ActivityRecruitmentProcessComponent } from './screens/Recruitment/ActivityRecruitmentProcess/activityrecruitmentprocess.component';

import { GcmComponent } from './screens/Recruitment/Gcm/gcm.component';

import { SubjectComponent } from './screens/Training/Subject/subject.component';
import { PunchDetailsComponent } from './screens/PunchDetails/PunchDetails.component';
import { PayElementComponent } from './screens/PayElement/payelement.component';


import { PayGroupComponent } from './screens/TypeMaster/PayGroup/paygroup.component';
import { SettingsComponent } from './screens/AdditionDeductions/Settings/settings.component';
 import { GoalComponent } from './screens/Goal/goal.component';
import { ShiftMasterComponent } from './screens/AttendanceSetting/ShiftMaster/shiftmaster.component';

import { NOCListComponent } from './screens/HumanResource/NOCList/noclist.component';
import { CasteCategoryComponent } from './screens/CasteCategory/castecategory.component';
import { ManPowerRequirementComponent } from './screens/ManPowerRequirement/manpowerrequirement.component';

import { EmployeeHierarchyComponent } from './screens/EmployeeHierarchy/employeehierarchy.component';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { SelectionRoundComponent } from './screens/Recruitment/SelectionRound/selectionround.component';
import { GratuitySettingsComponent } from './screens/PayRollSettings/GratuitySettings/gratuitysettings.component';
import { EmployeeWorkingComponent } from './screens/EmployeeDetail/EmployeeWorking/EmployeeWorking.component';
import { SelectionCycleComponent } from './screens/Recruitment/SelectionCycle/selectioncycle.component';
import { MeasureOptionsComponent } from './screens/MeasureOption/measureoption.component';
import { PayConfigurationComponent } from './screens/PayConfiguration/PayConfiguration.component';
import { RVLEmployeeMasterComponent } from './screens/RVLEmployeeMaster/RVLEmployeeMaster.component';
import { PayPeriodComponent } from './screens/Punches/PayPeriod/payperiod.component';
import { ShiftPatterncomponent } from './screens/ShiftPattern/shiftpattern.component';
import { EmployeeQualificationComponent } from './screens/EmployeeQualification/EmployeeQualification.component';
import { PerformanceMeasureComponent } from './screens/PerformanceMeasure/PerformanceMeasure.component';
import { RecruitmentComponent } from './screens/Recruitment/RecruitmentWorkType/Recruitment.component';
import { OULevelSettingComponent } from './screens/OULevelSetting/oulevelsetting.component';
import { EmployeeExperienceComponent } from './screens/EmployeeExperience/EmployeeExperience.component';
import { FinanceModule } from '../finance/finance.module';
import { AdminModule } from '../admin/admin.module';
import { GenerateEmptyDeclarationComponent } from './screens/GenerateEmptyDeclaration/generateemptydeclaration.component';
import { JobProfileLibraryComponent } from './screens/JobProfileLibrary/JobProfileLibrary.component';
import { EmployeeSalaryComponent } from './screens/EmployeeSalary/employeesalary.component';
import { EmployeeNomineeComponent } from './screens/EmployeeNominee/EmployeeNominee.component';
import { PayTaxSettingComponent } from './screens/PayTaxSetting/PayTaxSetting.component';
import { InventoryModule } from '../inventory/inventory.module';
import { EmployeeReferenceComponent } from './screens/EmployeeReference/employeereference.component';
import { EmployeeSkillComponent } from './screens/EmployeeSkill/employeeskill.component';
import { PolicyComponent } from './screens/Policy/policy.component';
@NgModule({
  declarations: [
    RVLEmployeeMasterComponent,
    EmployeeReferenceComponent,
    PolicyComponent,
    EmployeeSkillComponent,
    PayTaxSettingComponent,
    EmployeeNomineeComponent,
    JobProfileLibraryComponent,
    EmployeeSalaryComponent,
    OULevelSettingComponent,
    PerformanceMeasureComponent,
    EmployeeQualificationComponent,
    PayPeriodComponent,
    ShiftPatterncomponent,
    GenerateEmptyDeclarationComponent,
    EmployeeWorkingComponent,
    MeasureOptionsComponent,
    SelectionCycleComponent,
    TimeSlipComponent,
    RecruitmentComponent,
    EmployeeExperienceComponent,
    PayConfigurationComponent,
    EarlyDepatureComponent,
    CasteCategoryComponent,
    EmployeedetailComponent,
    attendancepunchComponent,
    StageComponent,
    EmployeemasterformComponent,
    HrmsComponent,
    EmployeemasterlistComponent,
    EmployeeComponent,
    EmployeeSkillListComponent,
    PayRevisionListComponent,
    AdditionDeductionComponent,
    VillageListComponent,
    AppraisalDashBoardComponent,
    AppraisalSummarycomponent,
    // MapComponent,
    SitePunchComponent,
    leaverequestComponent,
    shiftlistComponent,
    DailyAttendanceComponent,
    MonthlyAttendanceComponent,
    TimeslipComponent,
    EmployeeVsShiftComponent,
    LateArrivalComponent,
    MeasureOptionsTypeComponent,
    PickupPlaceComponent,
    DAPointGroupComponent,
    SkillMasterComponent,
    LeaveTypeComponent,
    KeyResultAreaComponent,
    ActivityRecruitmentProcessComponent,
    GcmComponent,
    SubjectComponent,
    PunchDetailsComponent,

    PayGroupComponent,
    PayElementComponent,
    SettingsComponent,
     GoalComponent,
     ShiftMasterComponent,
    NOCListComponent,
    
  GratuitySettingsComponent,
  SelectionRoundComponent,
  ManPowerRequirementComponent,
  EmployeeHierarchyComponent

  ],
  imports: [
    SamplescreensModule,
    CommonModule,
    HrmsRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    UicoreModule,
    NgBreModule,
    FinanceModule,
    AdminModule,
    UifwkUifwkmaterialModule,
    TranslocoRootModule,
    NgxsFormPluginModule.forRoot(),
    AgGridModule,
    GbgridModule,
    GbdataModule.forRoot(environment),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDvFSDxJXXR1nrdRDIzvp6DRZB9v649My8',
      libraries: ['places', 'geometry', 'drawing']
    }),
    AgmDirectionModule,
    NgxChartsModule,
    GbchartModule,
    EntitylookupModule,
    geolocationModule,
    NgSelectModule,
    MaterialModule,
    MatSortModule,
    MatFormFieldModule,
    MatSelectModule,
    MatButtonModule,
    MatListModule,
    MatTabsModule,
    MatCheckboxModule,
    MatTableModule,
    MatIconModule,
    MatExpansionModule,
    MatDividerModule,
    MatPaginatorModule,
    CommonReportModule,
    MatFormFieldModule,
    MatInputModule,
    HttpClientModule,

  ],
  exports: [
    HrmsComponent,
    MatSortModule,
    MatFormFieldModule,
    InventoryModule,
    MatInputModule,
  ],
})
export class HrmsModule {


}
