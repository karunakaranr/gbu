import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { COSTINGURLS } from 'features/erpmodules/costing/URLS/url';
import { CostHeadSetService } from '../../services/CostHeadSet.service';
import { ICostHeadSet } from '../../models/ICostHeadSet';
import * as CostHeadSetJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/CostHeadSet.json'
@Component({
    selector: 'app-CostHeadSet',
    templateUrl:'./costheadset.component.html',
    styleUrls: ['./costheadset.component.scss'],
    providers: [
        { provide: 'IdField', useValue: 'CostHeadSetId' },
        { provide: 'url', useValue: COSTINGURLS.CostHeadSet },
        { provide: 'DataService', useClass: CostHeadSetService },
        { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
        { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
    ]
})
export class CostHeadSetComponent extends GBBaseDataPageComponentWN<ICostHeadSet> {
    title: string = "CostHeadSet"
    CostHeadSetJSON = CostHeadSetJSON

    form: GBDataFormGroupWN<ICostHeadSet> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "CostHeadSet", {}, this.gbps.dataService);
    thisConstructor() {
        let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
        if (arrayOfValues != null) {
            this.CostHeadSetPicklistFillValue(arrayOfValues.CostHeadSetId)
            this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
        } else {
            this.store.dispatch(new ButtonVisibility("Forms"))
        }
    }

    public CostHeadSetPicklistFillValue(SelectedPicklistData: string): void {
        console.log("SelectedPicklistData",SelectedPicklistData)
        if (SelectedPicklistData) {
            this.gbps.dataService.getData(SelectedPicklistData).subscribe(CostHead => {
                console.log("CostHeadSet",CostHead)
                    this.form.patchValue(CostHead);
                 
            })
        }
    }



    public CostHeadSetPicklistPatchValue(SelectedPicklistDatas: any): void {
        this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
    }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<ICostHeadSet> {
    const dbds: GBBaseDBDataService<ICostHeadSet> = new GBBaseDBDataService<ICostHeadSet>(http);
    dbds.endPoint = url;
    return dbds;
}


export function getThisPageService(store: Store, dataService: GBBaseDataService<ICostHeadSet>, dbDataService: GBBaseDBDataService<ICostHeadSet>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<ICostHeadSet> {
    return new GBDataPageService<ICostHeadSet>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}