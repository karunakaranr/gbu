import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { COSTINGURLS } from 'features/erpmodules/costing/URLS/url';
import { BudgetService } from '../../services/Budget/Budget.service';
import { IBudget } from '../../models/IBudget';
import * as BudgetJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/Budget.json'
// import * as CostingBudgetMasterJSON from './CostingBudgetMaster.json'
const BudgetDummyJSON = require('./CostingBudgetMaster.json')
@Component({
    selector: 'app-Budget',
    templateUrl:'./Budget.component.html',
    styleUrls: ['./Budget.component.scss'],
    providers: [
        { provide: 'IdField', useValue: 'CostHeadSetId' },
        { provide: 'DataService', useClass: BudgetService },
        { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService] },
        { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
    ]
})
export class BudgetComponent extends GBBaseDataPageComponentWN<IBudget> {
    title: string = "Budget"
    BudgetJSON = BudgetJSON
    BudgetDummyData = BudgetDummyJSON
    form: GBDataFormGroupWN<IBudget> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "Budget", {}, this.gbps.dataService);
    Flag = true;
    thisConstructor() {
        this.store.dispatch(new ButtonVisibility("Forms"))
    } 

    public BudgetMasterPicklistFillValue(SelectedPicklistData: string): void {
        this.form.patchValue(this.BudgetDummyData)
        this.form.get('COSTINGMATERIALArray').patchValue(this.BudgetDummyData.CostingDetailArray[0].COSTINGMATERIALArray)
    }

    public BudgetMasterPicklistPatchValue(SelectedPicklistDatas: any): void {
        this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
    }

    public GridOutputPatch(GridOutput) : void{
        let VoucherDetailArray = JSON.stringify(this.form.get('CostingDetailArray').value)
        let VoucherDetailArray1 = JSON.parse(VoucherDetailArray)
        VoucherDetailArray1.forEach((field, fieldIndex:number) => {
          if(fieldIndex == 0){
            field.COSTINGMATERIALArray = this.form.get('COSTINGMATERIALArray').value
          }
        })
        this.form.get('CostingDetailArray').patchValue(VoucherDetailArray1)
    }

    public GridTotalOutput(GridOutput) : void{
        let VoucherDetailArray = JSON.stringify(this.form.get('CostingDetailArray').value)
        let VoucherDetailArray1 = JSON.parse(VoucherDetailArray)
        VoucherDetailArray1.forEach((field, fieldIndex:number) => {
          if(fieldIndex == 0){
            field.TOTALVALUE = GridOutput
          }
        })
        this.form.get('CostingDetailArray').patchValue(VoucherDetailArray1)
        console.log("Form Value:", this.form.value)
    }


}

export function getThisDBDataService(http: GBHttpService): GBBaseDBDataService<IBudget> {
    const dbds: GBBaseDBDataService<IBudget> = new GBBaseDBDataService<IBudget>(http);
    return dbds;
}

export function getThisPageService(store: Store, dataService: GBBaseDataService<IBudget>, dbDataService: GBBaseDBDataService<IBudget>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IBudget> {
    return new GBDataPageService<IBudget>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}