import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ISetting } from '../../models/ISetting';
import { SettingService } from '../../services/Setting/Setting.service';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as SettingJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/Setting.json'

import { COSTINGURLS } from '../../URLS/url';


@Component({
  selector: 'app-Setting',
  templateUrl: './Setting.component.html',
  styleUrls: ['./Setting.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'CostAddOnFieldsId'},
    { provide: 'url', useValue: COSTINGURLS.Adonfiled },
    { provide: 'DataService', useClass: SettingService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class SettingComponent extends GBBaseDataPageComponentWN<ISetting > {
  title: string = 'Setting'
  SettingJSON = SettingJSON;


  form: GBDataFormGroupWN<ISetting > = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'Setting', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.SettingFillFunction(arrayOfValues.CostAddOnFieldsId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }

  public GetDataSize(getData:any){
    console.log("getData",getData);
    if(getData == 0){
      console.log("Working==0")
      this.form.get('CostAddOnFieldsDataSize').patchValue(0)
    }
    else if(getData == 1){
      this.form.get('CostAddOnFieldsDataSize').patchValue(100)
    }
    else if(getData == 2){
      this.form.get('CostAddOnFieldsDataSize').patchValue(18.4)
    }
    else if(getData == 3){
      this.form.get('CostAddOnFieldsDataSize').patchValue(100)
    }
  }

  public SettingFillFunction(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(PackSet => {
        this.form.patchValue(PackSet);
      })
    }
  }


  public SettingPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<ISetting > {
  const dbds: GBBaseDBDataService<ISetting > = new GBBaseDBDataService<ISetting >(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<ISetting >, dbDataService: GBBaseDBDataService<ISetting >, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<ISetting > {
  return new GBDataPageService<ISetting >(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
