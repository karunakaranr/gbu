export class COSTINGURLS {
    public static  CostHead = '/cts/CostHead.svc/';
    public static CostDistribution = '/cts/CostDistribution.svc/'
    public static SaveURL = '/cts/CostDistribution.svc/SaveCostDistribution';
    public static CostHeadSet = '/cts/CostHeadSet.svc/';
    public static Resource = '/cts/Resource.svc/';
    public static OverHeadCost = '/cts/CostAnalysis.svc/';
    public static Adonfiled = '/cts/CostAddOnFields.svc/'
}
