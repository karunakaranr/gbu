import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { IBudget } from '../../models/IBudget';

@Injectable({
  providedIn: 'root'
})

export class BudgetService extends GBBaseDataServiceWN<IBudget> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IBudget>) {
    super(dbDataService, 'CostDistributionId');
  }
}
