import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { IResource } from '../models/IResource';



 
@Injectable({
  providedIn: 'root'
})

export class ResourceService extends GBBaseDataServiceWN<IResource> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IResource>) {
    super(dbDataService, 'ResourceId');
  }
}
