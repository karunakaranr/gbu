export interface ICostHeadSet {
    CostHeadSetId: number
    CostHeadSetCode: string
    CostHeadSetName: string
    CostHeadSetDetailArray: CostHeadSetDetailArray[]
    CostHeadSetCreatedById: number
    CostHeadSetCreatedOn: string
    CostHeadSetCreatedByName: string
    CostHeadSetModifiedById: number
    CostHeadSetModifiedOn: string
    CostHeadSetModifiedByName: string
    CostHeadSetBaseClinetId: number
    CostHeadSetSortOrder: number
    CostHeadSetStatus: number
    CostHeadSetVersion: number
    CostHeadSetSourceType: number
  }
  
  export interface CostHeadSetDetailArray {
    CostHeadSetDetailId: number
    CostHeadSetDetailSlNo: number
    CostHeadSetDetailParticulars: string
    CostHeadSetId: number
    CostHeadSetCode: string
    CostHeadSetName: string
    CostHeadId: number
    CostHeadCostHeadCode: string
    CostHeadCostHeadName: string
    CostHeadSetDetailCreatedById: number
    CostHeadSetDetailCreatedOn: string
    CostHeadSetDetailModifiedById: number
    CostHeadSetDetailModifiedOn: string
    CostHeadSetDetailBaseClinetId: number
    CostHeadSetDetailSortOrder: number
    CostHeadSetDetailStatus: number
    CostHeadSetDetailVersion: number
    CostHeadSetDetailSourceType: number
  }
  