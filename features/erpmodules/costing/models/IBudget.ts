export interface IBudget {
    COSTINGID: number
    COSTINGNUMBER: string
    COSTINGTYPE: number
    Selection: string
    Allocation: number
    COSTINGDATE: string
    COSTANALYSISTYPE: number
    REMARKS: string
    REFERENCENUMBER: string
    REFERENCEDATE: string
    CostingDetailArray: CostingDetailArray[]
  }
  
  export interface CostingDetailArray {
    SNo: number
    COSTINGDETAILID: number
    COSTINGID: number
    ITEMID: number
    ItemName: string
    ItemCode: string
    SKUID: number
    SKUName: string
    SKUCode: string
    QUANTITY: number
    UOMID: number
    UOMName: string
    UOMCode: string
    TOTALVALUE: number
    COSTINGMATERIALArray: Costingmaterialarray[]
  }
  
  export interface Costingmaterialarray {
    COSTINGMATERIALid: number
    COSTINGDETAILID: number
    SLNO: number
    STAGECODE: string
    ITEMID: number
    ItemName: string
    ItemCode: string
    SKUID: number
    SKUName: string
    SKUCode: string
    QUANTITY: number
    Level: number
    UOMID: number
    UOMNAME: string
    UOMCODE: string
    MakeorBuy: number
    ValueType: number
    SectionType: number
    CALCULATEDQUANTITY: number
    WASTEinputAGE: number
    NETQUANTITY: number
    ADJUSTQUANTITY: number
    FINALQUANTITY: number
    AVAILABLERATE: number
    PROJECTEDRATE: number
    RATEFC: number
    Value: number
    CURRENCYID: number
    CURRENCYNAME: string
    CURRENCYCODE: string
    CURRENCYCONVERSION: number
    TOTALCOST: number
    TOTALVALUE: number
    MATERIALCHARGESCOST: number
    MATERIALCHARGESVALUE: number
    COSTINGCHARGESArray: Costingchargesarray[]
    COSTINGPROCESSArray: Costingprocessarray[]
    COSTINGEXPENSEArray: Costingexpensearray[]
    MATERIALPROJECTEDPROCESSCOST: number
    MATERIALPROJECTEDPROCESSVALUE: number
    MATERIALEXPENSECOST: number
    MATERIALEXPENSEVALUE: number
    REMARKS: string
    Detail: string
  }
  
  export interface Costingchargesarray {
    COSTINGCHARGESid: number
    COSTINGMATERIALid: number
    SLNO: number
    Title: string
    CalcValue: number
    Input: number
  }
  
  export interface Costingprocessarray {
    COSTINGCHARGESid: number
    COSTINGMATERIALid: number
    SLNO: number
    Title: string
    CalcValue: number
    Input: number
  }
  
  export interface Costingexpensearray {
    COSTINGCHARGESid: number
    COSTINGMATERIALid: number
    SLNO: number
    Title: string
    CalcValue: number
    Input: number
  }
  