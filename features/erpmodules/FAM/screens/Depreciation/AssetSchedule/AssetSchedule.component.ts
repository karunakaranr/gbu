import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
// import { IAssetSchedule } from '../../models/IAssetSchedule';
// import { AssetScheduleService } from '../../services/AssetSchedule/AssetSchedule.service';
import { IAssetSchedule } from 'features/erpmodules/FAM/models/IAssetSchedule';
import { AssetScheduleService } from 'features/erpmodules/FAM/services/AssetSchedule.service';

import { ButtonVisibility } from 'features/layout/store/layout.actions';

// import * as AssetSchedulejson from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/AssetSchedule.json'

import * as AssetSchedulejson from './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/AssetSchedule.json'

import { FAMURLS } from 'features/erpmodules/FAM/urls/url';


@Component({
  selector: "app-AssetSchedule",
  templateUrl: "./AssetSchedule.component.html",
  styleUrls: ["./AssetSchedule.component.scss"],
  providers: [
    { provide: 'IdField', useValue: 'AssetScheduleId' },
    { provide: 'url', useValue: FAMURLS.AssetSchedule },
    { provide: 'DataService', useClass: AssetScheduleService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class AssetScheduleComponent extends GBBaseDataPageComponentWN<IAssetSchedule> {
  title: string = "AssetSchedule"
  AssetSchedulejson = AssetSchedulejson;




  form: GBDataFormGroupWN<IAssetSchedule> = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'AssetSchedule', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.AssetSchedulePicklist(arrayOfValues.AssetScheduleId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }




  public AssetSchedulePicklist(SelectedPicklistData: string): void {

   
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(AssetSchedule => {
        this.form.patchValue(AssetSchedule);
      })
    }
  }


  public AssetSchedulePicklists(SelectedPicklistDatas: any): void {
    
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
    console.log("iteem==>",SelectedPicklistDatas)
  }
}




export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IAssetSchedule> {
  const dbds: GBBaseDBDataService<IAssetSchedule> = new GBBaseDBDataService<IAssetSchedule>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IAssetSchedule>, dbDataService: GBBaseDBDataService<IAssetSchedule>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IAssetSchedule> {
  return new GBDataPageService<IAssetSchedule>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
