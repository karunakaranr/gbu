import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { IDepreciation } from '../../models/IDepreciation';
import { DepreciationService } from '../../services/Depreciation/Depreciation.service';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as Depreciationjson from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/Depreciation.json'
import { FAMURLS } from '../../urls/url';
import { DialogBoxComponent } from 'features/erpmodules/samplescreens/screens/DialogBox/DialogBox.component';

@Component({
  selector: "app-Depreciation",
  templateUrl: "./Depreciation.component.html",
  styleUrls: ["./Depreciation.component.scss"],
  providers: [
    { provide: 'IdField', useValue: 'DepreciationId' },
    { provide: 'url', useValue: FAMURLS.Depreciation },
    { provide: 'DataService', useClass: DepreciationService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class DepreciationComponent extends GBBaseDataPageComponentWN<IDepreciation> {
  title: string = "Depreciation"
  Depreciationjson = Depreciationjson;



  form: GBDataFormGroupWN<IDepreciation> = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'Depreciation', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.DepreciationFillFunction(arrayOfValues.DepreciationId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }


  validatePercent(event: any): void {
    let value = event.target.value;

    value = Number(value);

    if (!/^\d+$/.test(value)||value > 366) {
      this.form.get('DepreciationDaysperYear').setValue(0);
      const dialogRef = this.dialog.open(DialogBoxComponent, {
        width: '400px',
        data: {
          message: 'Please Enter Valid days.',
          heading: 'Info',
        }
      });
    }
    else {
      this.form.get('DepreciationDaysperYear').setValue(value);
    }
  }
  
  public setPatchValueToZero(): void {

    if (this.form.get('DepreciationPeriodType').value == 0 || this.form.get('DepreciationPeriodType').value == 1) {
      this.form.get('DepreciationPartialMonth').patchValue(1);
      this.form.get('DepreciationPartialPercentage').patchValue(0);
    }

    console.log("Form Detail:", this.form)
  }


  public DepreciationFillFunction(SelectedPicklistData: string): void {

    console.log("filling", SelectedPicklistData)
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(Depreciation => {
        this.form.patchValue(Depreciation);
      })
    }
  }


  public DepreciationPatchValue(SelectedPicklistDatas: any): void {

    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
    console.log("iteem==>", SelectedPicklistDatas)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IDepreciation> {
  const dbds: GBBaseDBDataService<IDepreciation> = new GBBaseDBDataService<IDepreciation>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IDepreciation>, dbDataService: GBBaseDBDataService<IDepreciation>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IDepreciation> {
  return new GBDataPageService<IDepreciation>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
