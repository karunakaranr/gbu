import { Inject, Injectable } from '@angular/core';
;
import { GBBaseDataServiceWN,GBBaseDBDataService } from 'libs/gbdata/src';


import { IAssetSchedule } from '../models/IAssetSchedule';


@Injectable({
  providedIn: 'root'
})
export class AssetScheduleService extends GBBaseDataServiceWN<IAssetSchedule> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IAssetSchedule>) {
    super(dbDataService, 'AssetScheduleId');
  }
}
