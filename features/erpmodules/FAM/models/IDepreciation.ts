export interface IDepreciation {
    DepreciationId: number
    DepreciationCode: string
    DepreciationName: string
    DepreciationCalculationMethod: number
    DepreciationPeriodType: number
    DepreciationPartialMonth: string
    DepreciationPartialPercentage: string
    DepreciationResidualType: number
    DepreciationResidual: string
    DepreciationDaysperYear: string
    DepreciationSortOrder: number
    DepreciationStatus: number
    DepreciationCreatedOn: string
    DepreciationModifiedOn: string
    DepreciationModifiedByName: string
    DepreciationCreatedByName: string
    DepreciationVersion: number
  }