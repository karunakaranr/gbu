import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DepreciationComponent } from './screens/Depreciation/Depreciation.component';
import { AssetScheduleComponent } from './screens/Depreciation/AssetSchedule/AssetSchedule.component';

const routes: Routes = [
  {
    path: 'themestest',
    loadChildren: () =>
      import(`../../themes/themes.module`).then((m) => m.ThemesModule),
  },
  {
    path:'depreciation',
    component:DepreciationComponent
  },
  {
    path:'assetschedule',
    component:AssetScheduleComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FAMroutingModule {}
