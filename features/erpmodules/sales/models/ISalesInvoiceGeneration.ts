export interface ISalesInvoiceGeneration {
    PartyId: number
    PartyName: string
    PartyBranchId: number
    PartyBranchName: string
    BizTransactionId: number
    BizTransactionName: string
    ToDate: string
    DoccumentDate: string
    ReferenceNumber: string
    Remarks: string
    SoBasedGeneration: number
    FromDate: string
  }