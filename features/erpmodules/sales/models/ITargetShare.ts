

export interface ITargetShare {
  TargetDataId: number
  TargetDataFromDate: string
  TargetDataToDate: string
  TargetDataCompareFromDate: string
  TargetDataCompareToDate: string
  TargetId: number
  TargetCode: string
  TargetName: string
  OUId: number
  OUCode: string
  OUName: string
  GBPeriodDetailId: number
  GBPeriodDetailSubPeriodName: string
  CompareGBPeriodDetailId: number
  CompareGBPeriodDetailSubPeriodName: string
  TargetDataDetailArray: TargetDataDetailArray[]
  TargetDataCreatedById: number
  TargetDataCreatedOn: string
  TargetDataModifiedById: number
  TargetDataModifiedOn: string
  TargetDataSortOrder: number
  TargetDataStatus: number
  TargetDataVersion: number
  TargetDataSourceType: number
}

export interface TargetDataDetailArray {
  TargetDataDetailId: number
  TargetDataDetailSlNo: number
  TargetDataDetailQuantity: number
  TargetDataDetailCost: number
  TargetDataDetailAmount: number
  TargetDataId: number
  ItemId: number
  ItemCode: string
  ItemName: string
  CostCenterId: number
  CostCenterCode: string
  CostCenterName: string
  ItemCategoryId: number
  ItemCategoryCode: string
  ItemCategoryName: string
  ItemSubCategoryId: number
  ItemSubCategoryCode: string
  ItemSubCategoryName: string
  BrandId: number
  BrandCode: string
  BrandName: string
  ItemGroupId: number
  ItemGroupCode: string
  ItemGroupName: string
  CounterId: number
  CounterCode: string
  CounterName: string
  DepartmentId: number
  DepartmentCode: string
  DepartmentName: string
  AllocationId: number
  AllocationShortName: string
  AllocationName: string
  PartyId: number
  PartyCode: string
  PartyName: string
  EmployeeId: number
  EmployeeCode: string
  EmployeeName: string
}
