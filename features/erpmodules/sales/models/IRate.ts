export interface IRate {
    RateId: number
    PeriodId: number
    PeriodCode: string
    PeriodName: string
    PeriodFromDate: string
    PeriodToDate: string
    OUId: number
    OUCode: string
    OUName: string
    BizTransactionTypeId: number
    BizTransactionTypeCode: string
    BizTransactionTypeName: string
    RateRateNumber: string
    RateRateDate: string
    RateReferenceNumber: string
    RateReferenceDate: string
    RateFromDate: string
    RateToDate: string
    CityId: number
    CityCode: string
    CityName: string
    RateCreatedById: number
    RateCreatedByName: string
    RateCreatedOn: string
    RateModifiedById: number
    RateModifiedByName: string
    RateModifiedOn: string
    RateBaseClinetId: number
    RateSortOrder: number
    RateStatus: number
    RateVersion: number
    RateSourceType: number
    RateDetailArray: RateDetailArray[]
  }
  
  export interface RateDetailArray {
    RateDetailId: number
    RateDetailSlNo: number
    ItemId: number
    ItemCode: string
    ItemName: string
    SKUId: number
    SKUName: string
    SKUCode: string
    RateDetailFCRate: number
    CurrencyId: number
    CurrencyCode: string
    CurrencyName: string
    RateDetailCreatedById: number
    RateDetailCreatedOn: string
    RateDetailModifiedById: number
    RateDetailModifiedOn: string
    RateDetailBaseClinetId: number
    RateDetailSortOrder: number
    RateDetailStatus: number
    RateDetailVersion: number
    RateDetailSourceType: number
  }
  