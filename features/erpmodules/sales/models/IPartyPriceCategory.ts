export interface IPartyPriceCategory {
  PartyPriceCategoryId: number
  PartyPriceCategoryCode: string
  PartyPriceCategoryName: string
  PartyPriceCategoryDetailArray: PartyPriceCategoryDetailArray[]
  PartyPriceCategoryCreatedById: number
  PartyPriceCategoryCreatedOn: string
  PartyPriceCategoryCreatedByName: any
  PartyPriceCategoryModifiedById: number
  PartyPriceCategoryModifiedOn: string
  PartyPriceCategoryModifiedByName: any
  PartyPriceCategoryStatus: number
  PartyPriceCategoryVersion: number
  PartyPriceCategorySortOrder: number
}

export interface PartyPriceCategoryDetailArray {
  PartyPriceCategoryDetailId: number
  PartyPriceCategoryId: number
  PartyPriceCategoryCode: string
  PartyPriceCategoryName: string
  PartyPriceCategoryDetailSlNo: number
  PriceCategoryId: number
  PriceCategoryCode: string
  PriceCategoryName: string
  PurchasePriceListTypeId: number
  PurchasePriceListTypeCode: string
  PurchasePriceListTypeName: string
  SalesPriceListTypeId: number
  SalesPriceListTypeCode: string
  SalesPriceListTypeName: string
  TransferPriceListTypeId: number
  TransferPriceListTypeCode: string
  TransferPriceListTypeName: string
  PaymentTermId: number
  PaymentTermCode: string
  PaymentTermName: string
  PartyPriceCategoryDetailCreditLimit: number
  PartyPriceCategoryDetailCreditDays: number
  PartyPriceCategoryDetailNoOfBills: number
}