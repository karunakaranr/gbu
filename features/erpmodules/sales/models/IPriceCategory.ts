export interface IPriceCategory {
    PriceCategoryId: number
    PriceCategoryCode: string
    PriceCategoryName: string
    PriceCategoryIsSelected: number
    PriceCategoryCreatedById: number
    PriceCategoryCreatedOn: string
    PriceCategoryCreatedByName: string
    PriceCategoryModifiedById: number
    PriceCategoryModifiedOn: string
    PriceCategoryModifiedByName: string
    PriceCategorySortOrder: number
    PriceCategoryStatus: number
    PriceCategoryVersion: number
  }