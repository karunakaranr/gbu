export interface ICashDiscount {
    CashDiscountId: number
    CashDiscountCode: string
    CashDiscountName: string
    BasedOnId: string
    BasedOnName: string
    CashDiscountRemarks: string
    CashDiscountStatus: number
    CashDiscountVersion: number
    CashDiscountDetailArray: CashDiscountDetailArray[]
    CashDiscountCreatedOn: string
    CashDiscountModifiedOn: string
    CashDiscountModifiedByName: string
    CashDiscountCreatedByName: string
  }
  
  export interface CashDiscountDetailArray {
    CashDiscountDetailSlNo: string
    CashDiscountDetailId: string
    CashDiscountDetailFromDays: string
    CashDiscountDetailToDays: string
    CashDiscountDetailPercentage: string
    CashDiscountDetailType: string
    CashDiscountId: number
  }