export interface ICounter {
    CounterId: number
    CounterCode: string
    CounterName: string
    CounterParentCounterIds: string
    OrganizationUnitId: number
    CounterTerminalId: string
    StoreId: number
    StoreName: string
    SupervisorId: number
    SupervisorName: string
    ParentCounterId: number
    ParentCounterName: string
    CounterLevels: string
    CounterRemarks: string
    CounterStatus: number
    CounterCreatedByName: string
    CounterCreatedOn: string
    CounterModifiedByName: string
    CounterModifiedOn: string
    CounterVersion: number
  }