import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { ItemWiseSummary } from './../../services/ItemWiseSummary/ItemWiseSummary.service';
import { Inject, LOCALE_ID } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ListReport } from '../../services/ListReport/ListReport.service';
import { ICriteriaDTODetail } from '../../models/IFilterDetail';
@Component({
  selector: 'app-ItemWiseSummary',
  templateUrl: './ItemWiseSummary.component.html',
  styleUrls: ['./ItemWiseSummary.component.scss']
})

export class ItemWiseSummaryComponent implements  OnChanges {
  @Input() BiztransactionClassId : number;
  @Input() BiztransactionClassName : string;
  @Input() ReportType : number;
  @Input() FilterData : ICriteriaDTODetail;
  RowData : any;
  constructor(public service: ItemWiseSummary,  public activeroute:ActivatedRoute,  private router: Router,public listreportservice: ListReport, @Inject(LOCALE_ID) public locale: string ) { }
  ngOnChanges(changes: SimpleChanges): void {
    if(changes.BiztransactionClassId || changes.ReportType){
      this.service.ItemWiseSummaryview(this.BiztransactionClassId,this.FilterData,this.ReportType).subscribe(menudetails => {
        this.RowData=menudetails;
        console.log("Item Wise Summary Data:",this.RowData)
      })
    }
  }
}