import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { CounterService } from '../../services/Counter/counter.service';
import { ICounter } from '../../models/ICounter';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as CounterJson from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/Counter.json';
import { SalesURLS } from 'features/erpmodules/sales/URLS/urls';


@Component({
  selector: 'app-counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'CounterId' },
    { provide: 'url', useValue: SalesURLS.Counter },
    { provide: 'DataService', useClass: CounterService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class CounterComponent extends GBBaseDataPageComponentWN<ICounter> {
  title: string = "Counter"
  CounterJson = CounterJson;


  form: GBDataFormGroupWN<ICounter> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "Counter", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.CounterRetrivalValue(arrayOfValues.CounterId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }

  public CounterRetrivalValue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(Counter => {
        this.form.patchValue(Counter);
      })
    }
  }


  public CounterPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<ICounter> {
  const dbds: GBBaseDBDataService<ICounter> = new GBBaseDBDataService<ICounter>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<ICounter>, dbDataService: GBBaseDBDataService<ICounter>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<ICounter> {
  return new GBDataPageService<ICounter>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
