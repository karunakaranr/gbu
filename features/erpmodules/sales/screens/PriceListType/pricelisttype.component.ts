import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { PriceListTypeService } from '../../services/PriceListType/Pricelisttype.service';
import { IPriceListType } from '../../models/IPriceListType';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { IBaseField, IPickListDetail } from 'libs/uicore/src/lib/interfaces/ibasefield';
import * as PricelisttypeJson from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/PriceListType.json';
import { SalesURLS } from '../../URLS/urls'; 

@Component({
  selector: 'app-pricelisttype',
  templateUrl: './pricelisttype.component.html',
  styleUrls: ['./pricelisttype.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'PriceListTypeId'},
    { provide: 'url', useValue: SalesURLS.PriceListType},
    { provide: 'DataService', useClass: PriceListTypeService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class PriceListTypeComponent extends GBBaseDataPageComponentWN<IPriceListType> {
  title: string = "PriceListType"
  PricelisttypeJson = PricelisttypeJson

  form: GBDataFormGroupWN<IPriceListType> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "PriceListType", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.PricelisttypeRetrivalValue(arrayOfValues.PriceListTypeId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {

      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }


  public PricelisttypeRetrivalValue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(PriceListType => {
        if (SelectedPicklistData != '0') {
          this.form.patchValue(PriceListType);
        }
      })
    }
  }


  public PricelisttypePatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IPriceListType> {
  const dbds: GBBaseDBDataService<IPriceListType> = new GBBaseDBDataService<IPriceListType>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IPriceListType>, dbDataService: GBBaseDBDataService<IPriceListType>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IPriceListType> {
  return new GBDataPageService<IPriceListType>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
