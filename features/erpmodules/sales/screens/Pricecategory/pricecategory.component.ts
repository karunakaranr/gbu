import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { PriceCategoryService } from 'features/erpmodules/sales/services/PriceCategory/pricecategory.service';
import { IPriceCategory } from 'features/erpmodules/sales/models/IPriceCategory';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as PriceCategoryJson from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/Pricecategory.json';
import { SalesURLS } from 'features/erpmodules/sales/URLS/urls';


@Component({
  selector: 'app-pricecategory',
  templateUrl: './Pricecategory.component.html',
  styleUrls: ['./Pricecategory.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'PriceCategoryId' },
    { provide: 'url', useValue: SalesURLS.PriceCategory },
    { provide: 'DataService', useClass: PriceCategoryService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class PriceCategoryComponent extends GBBaseDataPageComponentWN<IPriceCategory> {
  title: string = "Pricecategory"
  PriceCategoryJson = PriceCategoryJson;


  form: GBDataFormGroupWN<IPriceCategory> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "Pricecategory", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.PricecategoryRetrivalValue(arrayOfValues.PriceCategoryId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }


  public PricecategoryRetrivalValue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(Pricecategory => {
        this.form.patchValue(Pricecategory);
      })
    }
  }


  public PriceCategoryPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IPriceCategory> {
  const dbds: GBBaseDBDataService<IPriceCategory> = new GBBaseDBDataService<IPriceCategory>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IPriceCategory>, dbDataService: GBBaseDBDataService<IPriceCategory>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IPriceCategory> {
  return new GBDataPageService<IPriceCategory>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
