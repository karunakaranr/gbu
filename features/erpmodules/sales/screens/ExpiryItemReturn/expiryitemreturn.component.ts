import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { IExpiryItemReturn } from '../../models/ExpiryItemReturn';
import { ExpiryItemReturnService } from '../../services/ExpiryItemReturn/ExpiryItemReturn.Service';
import { SalesURLS } from '../../URLS/urls';
import * as ExpiryItemReturnJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/ExpiryItemReturn.json'
import { DialogBoxComponent } from 'features/erpmodules/samplescreens/screens/DialogBox/DialogBox.component';
import { DatePipe } from '@angular/common';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
@Component({
    selector: 'app-ExpiryItemReturn',
    templateUrl: 'expiryitemreturn.component.html',
    styleUrls: ['expiryitemreturn.component.scss'],
    providers: [
        { provide: 'IdField', useValue: 'PartyExpiryNameId' },
        { provide: 'url', useValue: SalesURLS.ExpiryItemReturning },
        { provide: 'DataService', useClass: ExpiryItemReturnService },
        { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
        { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
    ]
})
export class ExpiryItemReturnComponent extends GBBaseDataPageComponentWN<IExpiryItemReturn> {
    title: string = "ExpiryItemReturn"
    ExpiryItemReturnJSON = ExpiryItemReturnJSON;
    epochValue = 1714986375690;
    partyId: number = -1;
    Days :number =0;
   
    BillingDate:string="";
    bizValue : string = "";
    value: string;
    SelectedDate: string;
    public datenew(event:any)
    {
this.BillingDate=event
    }
    public GetRequestName(event :any){
        console.log ("nameeve",event)
       this.Days=event
    }
    public GetExpiryItemId(event: any) {
        console.log("event*8", event)
        this.partyId = event
    }
  
    datenewvalue(){
        console.log("hi")
    var datePipe = new DatePipe("en-US");
    console.log ("datePipe",datePipe)
    if(this.value != "" && this.value.toLowerCase() != "today"){
     
     
        this.SelectedDate = datePipe.transform(JSON.parse(this.value.replace("/Date(", "").replace(")/", "")), 'dd/MMM/yyyy');
        console.log ("SelectedDate",this.SelectedDate)
    }
}
    getGeneratealue() {
        
        let GenerateFun=SalesURLS.ExpiryItemReturning
      let  ExpiryItemReturnCriteria = [
            {
                "SectionCriteriaList": [
                    {
                        "SectionId": 0,
                        "AttributesCriteriaList": [
    
                            {
                                "FieldName": "biztransactiontypeid",
                                "OperationType": 1,
                                "FieldValue": -1499953436,
                                "InArray": null,
                                "JoinType": 2
                            },
    
                            {
                                "FieldName": "OUId",
                                "OperationType": 1,
                                "FieldValue": -1500000000,
                                "InArray": null,
                                "JoinType": 2
                            },
    
                            {
                                "FieldName": "Days",
                                "OperationType": 1,
                                "FieldValue":this.Days,
                                "InArray": null,
                                "JoinType": 2
                            },
    
                            {
                                "FieldName": "isproformaorsalesinvoice",
                                "OperationType": 1,
                                "FieldValue": 5,
                                "InArray": null,
                                "JoinType": 2
                            },
    
                            {
                                "FieldName": "BillingDate",
                                "OperationType": 10,
                                "FieldValue": this.BillingDate,
                                "InArray": null,
                                "JoinType": 2
                            },
    
                            {
                                "FieldName": "PartyId",
                                "OperationType": 5,
                                "FieldValue": this.partyId,
                                "InArray": null,
                                "JoinType": 2
                            }
    
                        ],
                        "OperationType": 0
                    }
    
                ]
            }
        ]
        if(this.bizValue==""){
            this.dialog.open(DialogBoxComponent, {
                width: '400px',
                data: {
                  message: "Please Select The Biztransaction Type",
                  heading: 'Info',
                }
              });
        }
        else{
            this.http.httppost(GenerateFun,ExpiryItemReturnCriteria).subscribe((response: any) => {
                console.log("response", response)
                this.dialog.open(DialogBoxComponent, {
                  width: '400px',
                  data: {
                    message: "Detail Saved Successfully",
                    heading: 'Info',
                  }
                });
                this.ClearValue()
              })
        }
       
     
        console.log("partyId*", this.partyId)
        let ans = JSON.stringify(ExpiryItemReturnCriteria)
        console.log("ans", ExpiryItemReturnCriteria)
    }
    
    ClearValue(){
        this.form.reset()

    }
   
    form: GBDataFormGroupWN<IExpiryItemReturn> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "ExpiryItemReturn", {}, this.gbps.dataService);
    thisConstructor() {
        let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
        if (arrayOfValues != null) {
            this.ExpiryItemReturnValue(arrayOfValues.PartyExpiryNameId)
            this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
        } else {
            this.store.dispatch(new ButtonVisibility("Forms"))
        }
    }

    public GetRequestFieldName(Event: any) {
        console.log("Event*",Event);
        this.bizValue = Event.Selected.Name
        console.log(":value",this.bizValue);
        
    }
    public ExpiryItemReturnValue(SelectedPicklistData: string): void {
        if (SelectedPicklistData) {
            this.gbps.dataService.getData(SelectedPicklistData).subscribe(ExpiryItemReturn => {
                this.form.patchValue(ExpiryItemReturn);
               
            })
        }
    }

    public ExpiryItemReturnNot(SelectedPicklistDatas: any): void {
        this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
    }
}
export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IExpiryItemReturn> {
    const dbds: GBBaseDBDataService<IExpiryItemReturn> = new GBBaseDBDataService<IExpiryItemReturn>(http);
    dbds.endPoint = url;
    return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IExpiryItemReturn>, dbDataService: GBBaseDBDataService<IExpiryItemReturn>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IExpiryItemReturn> {
    return new GBDataPageService<IExpiryItemReturn>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
