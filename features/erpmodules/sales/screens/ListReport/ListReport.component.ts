import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import  {Inject, LOCALE_ID } from '@angular/core';
import { ListReport } from '../../services/ListReport/ListReport.service';
import { MMLightBox } from 'features/erpmodules/samplescreens/services/LightBox/MMLightBox.service';
import { IListDetail } from '../../models/IListDetail';
import { ICriteriaDTODetail } from '../../models/IFilterDetail';
@Component({
  selector: 'app-ListReport',
  templateUrl: './ListReport.component.html',
  styleUrls: ['./ListReport.component.scss']
})

export class ListReportComponent implements OnChanges {
  @Input() BiztransactionClassId : number;
  @Input() BiztransactionClassName : string;
  @Input() ReportType : number;
  @Input() FilterData : ICriteriaDTODetail;
  RowData : IListDetail[]=[];
  DocumentId : number;
  showLightbox :boolean =false;
  constructor(public lightboxservice: MMLightBox, @Inject(LOCALE_ID) public locale: string,public service : ListReport ) { }
  ngOnChanges(changes: SimpleChanges): void {
    if(changes.BiztransactionClassId || changes.ReportType){
      this.service.ListReportview(this.BiztransactionClassId,this.FilterData,this.ReportType).subscribe(menudetails => {
        this.RowData=menudetails;
        console.log("List Data:",this.RowData)
      })
    }
  }

  public showlightbox(DocumentId : number){
    this.DocumentId=DocumentId;
    this.showLightbox=true;
  }

  closeLightBox(){
    this.showLightbox=false;
  }
}