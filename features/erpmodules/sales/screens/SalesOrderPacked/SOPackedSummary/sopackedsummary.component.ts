import { HttpClient } from '@angular/common/http';
import { Component, Inject, LOCALE_ID, OnInit } from '@angular/core';
// import { ItemcategoryService } from 'features/erpmodules/inventory/services/itemcategory/itemcategory.service';
import { Observable } from 'rxjs';
import { Select, Store } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';

// Import necessary modules and actions if not already imported
import { Drilldownsetting } from 'features/commonreport/datastore/commonreport.action';
import { GBHttpService } from 'libs/gbcommon/src/lib/services/HTTPService/GBHttp.service';
import { SharedService } from 'features/commonreport/service/datapassing.service';

@Component({
  selector: 'app-sopackedsummary',
  templateUrl: './sopackedsummary.component.html',
  styleUrls: ['./sopackedsummary.component.scss'],
})
export class sopackedsummaryComponent implements OnInit {
  @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
  @Select(ReportState.Sourcecriteriadrilldown) sourcerowcriteria$: Observable<any>;
  public rowData: any;
  selectedrowdatavalue: any;
  flag = 0;
  constructor(public sharedService: SharedService,public store: Store, @Inject(LOCALE_ID) public locale: string,public lib: GBHttpService) {}

  ngOnInit(): void {
    this.getsopackedsummary();
    //this.handleSourceRowCriteria(this.selectedrowdatavalue);
  }

  public getsopackedsummary() {
    this.sharedService.getRowInfo().subscribe(data => {
      this.rowData = data;
      console.log("Sales Order Packed & Not Invoice:", this.rowData);
    });
  }

  // Add the function to handle sourcerowcriteria$
  public handleSourceRowCriteria(selectedrowdatavalue) {
    console.log("L",selectedrowdatavalue)
    if(selectedrowdatavalue){
      let destinatopnfieldmapname = "DocumentId"
      let selectedrowdataid= selectedrowdatavalue.DocumentId;
      let drillmenuid = -1399979642;
      let drilldownsettingdata = [selectedrowdataid, drillmenuid,destinatopnfieldmapname];
      this.sharedService.setDrillInfo(drilldownsettingdata);
    }
    // if(selectedrowdatavalue){
    //   this.sourcerowcriteria$.subscribe(data => {
    //     console.log("selectedrowdatavalue,@#$%^&*((@@................",selectedrowdatavalue)
        // let destinatopnfieldmapname = "DocumentId"
        // let selectedrowdataid= selectedrowdatavalue.DocumentId;
        // let sourcecriteria = data.CriteriaDTO;
        // let drillmenuid = -1399979642;
    //     let drilldownsettingdata = [selectedrowdataid, sourcecriteria, drillmenuid,destinatopnfieldmapname];
  
        
    //     this.store.dispatch(new Drilldownsetting(drilldownsettingdata));
  
    //     console.log('Clicked on Document Number:', selectedrowdatavalue);
    //   });
    // }
  }
}
