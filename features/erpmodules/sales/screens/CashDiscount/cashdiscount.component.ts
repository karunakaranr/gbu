import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { CashDiscountService } from '../../services/CashDiscount/cashdiscount.service';
import { ICashDiscount } from '../../models/ICashDiscount';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as CashDiscountJson from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/CashDiscount.json';
import { SalesURLS } from '../../URLS/urls';


@Component({
  selector: 'app-cashdiscount',
  templateUrl: './cashdiscount.component.html', 
  styleUrls: ['./cashdiscount.component.scss'], 
  providers: [
    { provide: 'IdField', useValue: 'CashDiscountId'},
    { provide: 'url', useValue: SalesURLS.CashDisCount},
    { provide: 'DataService', useClass: CashDiscountService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] }, 
  ]
})
export class CashDiscountComponent extends GBBaseDataPageComponentWN<ICashDiscount> {
  title: string = 'CashDiscount'
  CashDiscountJson = CashDiscountJson;


  form: GBDataFormGroupWN<ICashDiscount> = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'CashDiscount', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.CashdiscountRetrivalValue(arrayOfValues.CashDiscountId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }
  public CashdiscountRetrivalValue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(CashDiscount => {
        this.form.patchValue(CashDiscount);
      })
    }
  }
  public CashdiscountPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<ICashDiscount> {
  const dbds: GBBaseDBDataService<ICashDiscount> = new GBBaseDBDataService<ICashDiscount>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<ICashDiscount>, dbDataService: GBBaseDBDataService<ICashDiscount>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<ICashDiscount> {
  return new GBDataPageService<ICashDiscount>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
