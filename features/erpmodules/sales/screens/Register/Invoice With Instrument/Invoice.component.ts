
import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs';
import { Select } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { SharedService } from 'features/commonreport/service/datapassing.service';


@Component({
    selector: 'app-Invoice',
    templateUrl: './Invoice.component.html',
    styleUrls: ['./Invoice.component.scss'],
})
export class InvoiceComponent implements OnInit {
    @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
    public rowData: any;

    public NetValue;
    public NetValueFc;
    public ItemValue;
    public ItemValueFc;
    public BillVal;


    constructor(public sharedService: SharedService) { }
    ngOnInit(): void {
        this.getInvoice();
    }

    public getInvoice() {
        this.sharedService.getRowInfo().subscribe(data => {
            this.rowData = data;
            if (data.length > 0) {
            console.log("Invoice Details :", this.rowData)

            this.NetValue = 0;
            this.NetValueFc = 0;
            this.ItemValue = 0;
            this.ItemValueFc=0;
            this.BillVal=0;
            

            for (let data of this.rowData) {
                this.NetValue = (parseFloat(this.NetValue) + parseFloat(data.TotalNetValue)).toFixed(2);
                this.NetValueFc = (parseFloat(this.NetValueFc) + parseFloat(data.TotalNetValueFc)).toFixed(2);
                this.ItemValue = (parseFloat(this.ItemValue) + parseFloat(data.ItemAmountPlus)).toFixed(2);
                this.ItemValueFc = (parseFloat(this.ItemValueFc) + parseFloat(data.ItemAmountPlusFC)).toFixed(2);
                this.BillVal = (parseFloat(this.BillVal) + parseFloat(data.BillValue)).toFixed(2);
              
            }

            

            let grouping = this.rowData;

            var finalizedGrp = [];

            grouping.map((inputdata, index) =>{

               
                finalizedGrp.push({
                    sno:index,

                    DocDt:inputdata['DocumentNumber'],
                    RefNo:inputdata['ReferenceNumber'],
                    RefDate:inputdata['ReferenceDate'],
                    Particulars:inputdata['PartyName'],
                    Person:inputdata['InchargeName'],
                    NetValue:inputdata['TotalNetValue'],
                    NetValueFc:inputdata['TotalNetValueFc'],
                    ItemValue:inputdata['ItemAmountPlus'],
                    ItemValueFc:inputdata['ItemAmountPlusFC'],
                    BillVal:inputdata['BillValue'],
                    Transaction:inputdata['TaxTransactionTypeName'],


                    dateDoc:inputdata['DocumentDate'] ,
                });
            });

            const final = {};
            finalizedGrp.forEach((detailData) => {
                final[detailData.dateDoc] ={
                    supplyGroups:{},       
                    DocDt:detailData.dateDoc,
                    RefNo:"",
                    RefDate:"",
                    Particulars:"",
                    Person:"",
                    NetValue:"",
                    NetValueFc:"",
                    ItemValue:"",
                    ItemValueFc:"",
                    BillVal:"",
                    Transaction:"",
                    
                    ...final[detailData.dateDoc]

                };

                final[detailData.dateDoc].supplyGroups[detailData.sno] = {
                    //sno:detailData.sno,
                    DocDt: detailData.DocDt,
                    RefNo:detailData.RefNo,
                    RefDate: detailData.RefDate,
                    Particulars:detailData.Particulars,
                    Person:detailData.Person,
                    NetValue:detailData.NetValue,
                    NetValueFc:detailData.NetValueFc,
                    ItemValue:detailData.ItemValue,
                    ItemValueFc:detailData.ItemValueFc,
                    BillVal:detailData.BillVal,
                    Transaction:detailData.Transaction,
                
                }
            });
            const finalgrouping = Object.keys(final);
            const tableData = [];

            finalgrouping.forEach((codeData) => {
                tableData.push({
                    DocDt:final[codeData].DocDt,
                    RefNo:"",
                    RefDate:"",
                    Particulars:"",
                    Person:"",
                    NetValue:"",
                    NetValueFc:"",
                    ItemValue:"",
                    ItemValueFc:"",
                    BillVal:"",
                    Transaction:"",
                    bold: true,
                });

                const accounts = Object.keys(final[codeData].supplyGroups); //supply group is must given about in data storage variable 
                accounts.forEach((account) => {
                    tableData.push({
                        DocDt:final[codeData].supplyGroups[account].DocDt,
                        RefNo:final[codeData].supplyGroups[account].RefNo,
                        RefDate:final[codeData].supplyGroups[account].RefDate,
                        Particulars:final[codeData].supplyGroups[account].Particulars,
                        Person:final[codeData].supplyGroups[account].Person,
                        NetValue:final[codeData].supplyGroups[account].NetValue,
                        NetValueFc:final[codeData].supplyGroups[account].NetValueFc,
                        ItemValue:final[codeData].supplyGroups[account].ItemValue,
                        ItemValueFc:final[codeData].supplyGroups[account].ItemValueFc,
                        BillVal:final[codeData].supplyGroups[account].BillVal,
                        Transaction:final[codeData].supplyGroups[account].Transaction,


                    })
                })

            })

             this.rowData=tableData;
             console.log("Final Data:",this.rowData[0])
        }
        });

       
    }
}
