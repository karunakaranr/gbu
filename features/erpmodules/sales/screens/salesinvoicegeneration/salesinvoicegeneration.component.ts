import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { SalesInvoiceGenerationService } from '../../services/SalesinvoiceGeneration/salesinvoicegenration.service';
import { ISalesInvoiceGeneration } from '../../models/ISalesInvoiceGeneration';
import { SalesURLS } from '../../URLS/urls';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as SalesInvoiceGenerationJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/SalesInvoiceGeneration.json'
import { DialogBoxComponent } from 'features/erpmodules/samplescreens/screens/DialogBox/DialogBox.component';
@Component({
  selector: 'app-SalesInvoiceGeneration',
  templateUrl: 'salesinvoicegeneration.component.html',
  styleUrls: ['salesinvoicegeneration.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'PartyBranchId' },
    { provide: 'url', useValue:SalesURLS.SalesInvoiceGeneration },
    { provide: 'DataService', useClass: SalesInvoiceGenerationService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
 
export class SalesInvoiceGenerationComponent extends GBBaseDataPageComponentWN<ISalesInvoiceGeneration> {
  title: string = "SalesInvoiceGeneration"
  SalesInvoiceGenerationJSON = SalesInvoiceGenerationJSON;

  remarks:String="";
  ToDate:String="";
  BillingDate:String="";
  referenceno:String="";
  isproformaorsalesinvoice:number=1;
  PartyId:number=-1;
  PartyBranchId:number=-1;

  public PartyIdsales(event :any){
    console.log ("nameeve",event)
   this.PartyId=event
}
  public PartyBranchIdName(event :any){
    console.log ("nameeve",event)
   this.PartyBranchId=event
}
  public proformaorsales(event :any){
    console.log ("nameeve",event)
   this.isproformaorsalesinvoice=event
}
  public GetRequestName(event :any){
    console.log ("nameeve",event)
   this.remarks=event
}
public GenerateNewBillingDate(event :any){
  console.log ("nameeve",event)
 this.BillingDate=event
}

public NewValueTodate(event :any){
  console.log ("nameeve",event)
 this.ToDate=event
}
public NewValueRefNum(event :any){
  console.log ("nameeve",event)
 this.referenceno=event
}

  GetSalesInvoiceGenerate() {
    console.log("hi")

    let Salesinvgen = SalesURLS.SalesInvoiceGeneration
    let  SalesInvoiceGenerationCriteria = [
      {
        "SectionCriteriaList": [{
                "SectionId": 0,
                "AttributesCriteriaList": [{
                        "FieldName": "biztransactiontypeid",
                        "OperationType": 1,
                        "FieldValue": -1499991279,
                        "InArray": null,
                        "JoinType": 2
                    }, {
                        "FieldName": "OUId",
                        "OperationType": 1,
                        "FieldValue": -1500000000,
                        "InArray": null,
                        "JoinType": 2
                    }, {
                        "FieldName": "referenceno",
                        "OperationType": 5,
                        "FieldValue":this.referenceno,
                        "InArray": null,
                        "JoinType": 2
                    }, {
                        "FieldName": "remarks",
                        "OperationType": 5,
                        "FieldValue": this.remarks,
                        "InArray": null,
                        "JoinType": 2
                    }, {
                        "FieldName": "isproformaorsalesinvoice",
                        "OperationType": 1,
                        "FieldValue":this.isproformaorsalesinvoice,
                        "InArray": null,
                        "JoinType": 2
                    },
                     {
                        "FieldName": "FromDate",
                        "OperationType": 11,
                        "FieldValue": "01/Jan/1990",
                        "InArray": null,
                        "JoinType": 2
                    }, {
                        "FieldName": "ToDate",
                        "OperationType": 10,
                        "FieldValue": this.ToDate,
                        "InArray": null,
                        "JoinType": 2
                    }, {
                        "FieldName": "BillingDate",
                        "OperationType": 10,
                        "FieldValue": this.BillingDate,
                        "InArray": null,
                        "JoinType": 2
                    }, {
                        "FieldName": "PartyId",
                        "OperationType": 5,
                        "FieldValue": this.PartyId,
                        "InArray": null,
                        "JoinType": 2
                    }, {
                        "FieldName": "PartyBranchId",
                        "OperationType": 5,
                        "FieldValue": this.PartyBranchId,
                        "InArray": null,
                        "JoinType": 2
                    }
                ],
                "OperationType": 0
            }
        ]
    }


    ]
    this.http.httppost(Salesinvgen, SalesInvoiceGenerationCriteria).subscribe((response: any) => {
      console.log("response", response)
      this.dialog.open(DialogBoxComponent, {
        width: '400px',
        data: {
          message: "Detail Saved Successfully",
          heading: 'Info',
        }
      });
      this.ClearValue()
    })
    let ans = JSON.stringify(SalesInvoiceGenerationCriteria)
  }
  ClearValue() {
this.form.reset()


  }

  form: GBDataFormGroupWN<ISalesInvoiceGeneration> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "SalesInvoiceGeneration", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.SalesInvoiceGenerationValue(arrayOfValues.PartyBranchId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }
  public SalesInvoiceGenerationValue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(SalesInvoiceGeneration => {
        this.form.patchValue(SalesInvoiceGeneration);
      })
    }
  }
  public SalesInvoiceGenerationNot(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}
export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<ISalesInvoiceGeneration> {
  const dbds: GBBaseDBDataService<ISalesInvoiceGeneration> = new GBBaseDBDataService<ISalesInvoiceGeneration>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<ISalesInvoiceGeneration>, dbDataService: GBBaseDBDataService<ISalesInvoiceGeneration>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<ISalesInvoiceGeneration> {
  return new GBDataPageService<ISalesInvoiceGeneration>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
