
import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs';
import { Select } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { Root2 } from 'features/erpmodules/sales/models/IScvendorStock';
import { SharedService } from 'features/commonreport/service/datapassing.service';


@Component({
    selector: 'app-ScVendorStock',
    templateUrl: './ScVendorStock.component.html',
    styleUrls: ['./ScVendorStock.component.scss'],
})
export class ScVendorStockComponent implements OnInit {
    @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
    public rowData: Root2[];
    constructor(public sharedService: SharedService) { }
    ngOnInit(): void {
        this.getScVendorStock();
    }

    public getScVendorStock() {
        this.sharedService.getRowInfo().subscribe(data => {
            this.rowData = data;
            if (data.length > 0) {
            console.log("Vendor Stock Details :", this.rowData); 

            let groupingData = this.rowData;
            var finalizedGroup = [];

            groupingData.map((inputData, index) => {

                finalizedGroup.push({
                    sno: index,
                    ItemCode:inputData['ItemCode'],
                    ItemName:inputData['ItemName'],
                    AdjDocumentNumber:inputData['AdjDocumentNumber'],
                    AdjDocumentDate:inputData['AdjDocumentDate'],
                    AdjQuantity:inputData['AdjQuantity'],
                    SecondAllocDocumentNumber:inputData['SecondAllocDocumentNumber'],
                    SecondAllocDocumentDate:inputData['SecondAllocDocumentDate'],
                    SecondAllocAdjQuantity:inputData['SecondAllocAdjQuantity'],

                    DocumentNumber:inputData['DocumentNumber'],
                    DocumentDate:inputData['DocumentDate'],
                    DocumentQuantity:inputData['DocumentQuantity']
                });

            });

            const final ={};
            finalizedGroup.forEach(detailData => {
                final[detailData.DocumentNumber] ={
                    Grouping: {},
                    ItemCode: detailData.DocumentNumber,
                    ItemName: detailData.DocumentDate,
                    AdjDocumentNumber: detailData.DocumentQuantity,
                    AdjDocumentDate:"",
                    AdjQuantity:"",
                    SecondAllocDocumentNumber:"",
                    SecondAllocDocumentDate:"",
                    SecondAllocAdjQuantity:"",

                    ...final[detailData.DocumentNumber]
                };
                final[detailData.DocumentNumber].Grouping[detailData.sno] ={
                    ItemCode: detailData.ItemCode,
                    ItemName: detailData.ItemName,
                    AdjDocumentNumber: detailData.AdjDocumentNumber,
                    AdjDocumentDate: detailData.AdjDocumentDate,
                    AdjQuantity: detailData.AdjQuantity,
                    SecondAllocDocumentNumber: detailData.SecondAllocDocumentNumber,
                    SecondAllocDocumentDate: detailData.SecondAllocDocumentDate,
                    SecondAllocAdjQuantity: detailData.SecondAllocAdjQuantity,
                }
            });

            const finalgrouping = Object.keys(final);
            const tableData = [];

            finalgrouping.forEach(codeData =>{
                tableData.push({
                    ItemCode: final[codeData].ItemCode,
                    ItemName: final[codeData].ItemName,
                    AdjDocumentNumber: final[codeData].AdjDocumentNumber,
                    AdjDocumentDate: "",
                    AdjQuantity: "",
                    SecondAllocDocumentNumber: "",
                    SecondAllocDocumentDate: "",
                    SecondAllocAdjQuantity: "",
                    bold: true,
                });

                const accounts = Object.keys(final[codeData].Grouping);
                accounts.forEach(account => {
                    tableData.push({
                        ItemCode: final[codeData].Grouping[account].ItemCode,
                        ItemName: final[codeData].Grouping[account].ItemName,
                        AdjDocumentNumber: final[codeData].Grouping[account].AdjDocumentNumber,
                        AdjDocumentDate: final[codeData].Grouping[account].AdjDocumentDate,
                        AdjQuantity: final[codeData].Grouping[account].AdjQuantity,
                        SecondAllocDocumentNumber: final[codeData].Grouping[account].SecondAllocDocumentNumber,
                        SecondAllocDocumentDate: final[codeData].Grouping[account].SecondAllocDocumentDate,
                        SecondAllocAdjQuantity: final[codeData].Grouping[account].SecondAllocAdjQuantity,
                    });
                });
            });
            this.rowData=tableData;
            console.log("Final Data:",this.rowData[0])
        }
        });
    }
}
