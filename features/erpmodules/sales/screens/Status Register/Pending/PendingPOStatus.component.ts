
import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs';
import { Select } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { SharedService } from 'features/commonreport/service/datapassing.service';


@Component({
    selector: 'app-pendingPOStatus',
    templateUrl: './pendingPOStatus.component.html',
    styleUrls: ['./pendingPOStatus.component.scss'],
})
export class pendingPOStatusComponent implements OnInit {
    @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
    public rowData: any;



    constructor(public sharedService: SharedService) { }
    ngOnInit(): void {
        this.getpendingPOStatus();
    }

    public getpendingPOStatus() {
        this.sharedService.getRowInfo().subscribe(data => {
            this.rowData = data;
            if (data.length > 0) {
            console.log("Pending Details :", this.rowData); 

            

            let groupingData = this.rowData;
            var finalizedGroup = [];


            groupingData.map((inputData, index) => {
                finalizedGroup.push({
                  sno: index,
                  ItemCode : inputData['ItemCode'],
                  ItemDescription: inputData['ItemDescription'],
                  UOMCode : inputData['UOMCode'],
                  ScheduleDate : inputData['ScheduleDate'],
                  Qty : inputData['OrderQuantity'],
                  PendQty : inputData['PendingQuantity'],
                  ScheduleQuantity : inputData['ScheduleQuantity'],
                  Currency : inputData['Currency'],
                  CurrencyConversion : inputData['CurrencyConversion'],
                  BaseRate : inputData['BaseRate'],
                  BasePlus : inputData['BasePlus'],
                  BaseGross : inputData['BaseGross'],
                  AllocationName : inputData['AllocationName'],
                  AllocationDescription : inputData['AllocationDescription'],
                  IDVal: inputData['ItemDocumentValue'],
                  IDCVal: inputData['ItemDocumentCostValue'],
                  IDRVal: inputData['ItemDocumentRealisationValue'],
                  IValFC: inputData['ItemValueFC'],


                  DocumentNumber: inputData['DocumentNumber'],
                  DocumentDate:inputData['DocumentDate'],
                  PreDocumentNumber:inputData['PreDocumentNumber'],
                  PreDocumentDate:inputData['PreDocumentDate'],
                  PartyCode:inputData['PartyCode'],
                  PartyName:inputData['PartyName'],
                  PartyReferenceNumber:inputData['PartyReferenceNumber'],
                  PartyReferenceDate:inputData['PartyReferenceDate'],
                  
                });
            });


            const final ={};
            finalizedGroup.forEach(detailData =>{
               final[detailData.DocumentNumber] ={
                firstgrup:{},
                ItemCode : detailData.DocumentNumber,
                ItemDescription : detailData.DocumentDate,
                UOMCode :detailData.PreDocumentNumber,
                ScheduleDate : detailData.PreDocumentDate,
                Qty : "",
                PendQty : "",
                ScheduleQuantity : "",
                Currency : "",
                CurrencyConversion : "",
                BaseRate : "",
                BasePlus : "",
                BaseGross : "",
                AllocationName : "",
                AllocationDescription : "",
                IDVal: "",
                IDCVal: "",
                IDRVal: "",
                IValFC:"",




                ...final[detailData.DocumentNumber]
               }; 

                final[detailData.DocumentNumber].firstgrup[detailData.PartyCode] = {
                      Secondgrup: {},
                      ItemCode : detailData.PartyCode,
                      ItemDescription : detailData.PartyName,
                      UOMCode :detailData.PartyReferenceNumber,
                      ScheduleDate :detailData.PartyReferenceDate,
                      Qty : "",
                      PendQty : "",
                      ScheduleQuantity : "",
                      Currency : "",
                      CurrencyConversion : "",
                      BaseRate : "",
                      BasePlus : "",
                      BaseGross : "",
                      AllocationName : "",
                      AllocationDescription : "",
                      IDVal: "",
                      IDCVal: "",
                      IDRVal: "",
                      IValFC: "",
    
    

                      ...final[detailData.DocumentNumber].firstgrup[detailData.PartyCode],

                };
                final[detailData.DocumentNumber].firstgrup[detailData.PartyCode].Secondgrup[detailData.sno] = {
                    ItemCode : detailData.ItemCode,
                    ItemDescription: detailData.ItemDescription,
                    UOMCode :detailData.Code,
                    ScheduleDate : detailData.Name,
                      Qty : detailData.Qty,
                      PendQty : detailData.PendQty,
                      ScheduleQuantity : detailData.ScheduleQuantity,
                      Currency : detailData.Currency,
                      CurrencyConversion : detailData.CurrencyConversion,
                      BaseRate : detailData.BaseRate,
                      BasePlus : detailData.BasePlus,
                      BaseGross : detailData.BaseGross,
                      AllocationName : detailData.AllocationName,
                      AllocationDescription : detailData.AllocationDescription,
                      IDVal: detailData.IDVal,
                      IDCVal: detailData.IDCVal,
                      IDRVal: detailData.IDRVal,
                      IValFC: detailData.IValFC,
    
    

                }


            }); 

            const dedGroups = Object.keys(final);

            const tableData = [];

            dedGroups.forEach((Details) => {
                const finalfirstgrup = Object.keys(final[Details].firstgrup);


                
                tableData.push({
                    ItemCode:final[Details].ItemCode,
                    ItemDescription : final[Details].ItemDescription,
                    UOMCode: final[Details].UOMCode,
                    ScheduleDate: final[Details].ScheduleDate,
                    Qty: "",
                    PendQty: "",
                    ScheduleQuantity: "",
                    Currency: "",
                    CurrencyConversion:"",
                    BaseRate: "",
                    BasePlus: "",
                    BaseGross:"",
                    AllocationName: "",
                    Remarks: "",
                    AllocationDescription:"",
                    IDCVal:"",
                    IDRVal:"",
                    IValFC:"",
                    bold: true,
                });

                finalfirstgrup.forEach(ag => {
                    const finalsecondgrup = Object.keys(final[Details].firstgrup[ag].Secondgrup)
                   

                    tableData.push({
                        ItemCode:final[Details].firstgrup[ag].ItemCode,
                        ItemDescription : final[Details].firstgrup[ag].ItemDescription,
                        UOMCode: final[Details].firstgrup[ag].UOMCode,
                        ScheduleDate: final[Details].firstgrup[ag].ScheduleDate,
                        Qty: "",
                        PendQty: "",
                        ScheduleQuantity: "",
                        Currency: "",
                        CurrencyConversion: "",
                        BaseRate:"",
                        BasePlus: "",
                        BaseGross:"",
                        AllocationName: "",
                        AllocationDescription: "",
                        IDVal:"",
                        IDCVal:"",
                        IDRVal:"",
                        IValFC:"",
                        bold: true,
                    });

                    finalsecondgrup.forEach(account =>{
                        tableData.push({
                            ItemCode:final[Details].firstgrup[ag].Secondgrup[account].ItemCode,
                            ItemDescription:final[Details].firstgrup[ag].Secondgrup[account].ItemDescription,
                            UOMCode:final[Details].firstgrup[ag].Secondgrup[account].UOMCode,
                            ScheduleDate:final[Details].firstgrup[ag].Secondgrup[account].ScheduleDate,
                            Qty:final[Details].firstgrup[ag].Secondgrup[account].Qty,
                            PendQty:final[Details].firstgrup[ag].Secondgrup[account].PendQty,
                            ScheduleQuantity:final[Details].firstgrup[ag].Secondgrup[account].ScheduleQuantity,
                            Currency:final[Details].firstgrup[ag].Secondgrup[account].Currency,
                            CurrencyConversion:final[Details].firstgrup[ag].Secondgrup[account].CurrencyConversion,
                            BaseRate:final[Details].firstgrup[ag].Secondgrup[account].BaseRate,
                            BasePlus:final[Details].firstgrup[ag].Secondgrup[account].BasePlus,
                            BaseGross:final[Details].firstgrup[ag].Secondgrup[account].BaseGross,
                            AllocationName:final[Details].firstgrup[ag].Secondgrup[account].AllocationName,
                            AllocationDescription:final[Details].firstgrup[ag].Secondgrup[account].AllocationDescription,
                            IDVal:final[Details].firstgrup[ag].Secondgrup[account].IDVal,
                            IDCVal:final[Details].firstgrup[ag].Secondgrup[account].IDCVal,
                            IDRVal:final[Details].firstgrup[ag].Secondgrup[account].IDRVal,
                            IValFC:final[Details].firstgrup[ag].Secondgrup[account].IValFC,

                        })
                    })
                })

            });

            this.rowData = tableData;

            console.log("Final Data:", this.rowData[0])
        }
        });
    }
}
