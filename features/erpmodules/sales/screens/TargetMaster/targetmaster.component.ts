import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { TargetMasterService } from '../../services/TargetMaster/Targetmaster.service';
import { ITargetMaster } from '../../models/ITargetMaster';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as TargetMasterJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/TargetMaster.json';
import { SalesURLS } from '../../URLS/urls';


@Component({
  selector: 'app-targetmaster',
  templateUrl: './targetmaster.component.html',
  styleUrls: ['./targetmaster.component.scss'],
  providers: [ 
    { provide: 'IdField', useValue: 'TargetId'},
    { provide: 'url', useValue: SalesURLS.TargetMaster },
    { provide: 'DataService', useClass: TargetMasterService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class TargetMasterComponent extends GBBaseDataPageComponentWN<ITargetMaster > {
  title: string = "TargetMaster"
  dateChange: boolean=true;
  TargetMasterJSON = TargetMasterJSON; 

  form: GBDataFormGroupWN<ITargetMaster> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "TargetMaster", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.TargetMasterRetrivalValue(arrayOfValues.TargetId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }
 
  public settingPatchValueToZero(value): void {
    console.log("form.get('TargetPeriodType').value:",this.form.get('TargetPeriodType').value)
    // console.log("New Date:",'/Date('+ new Date().getTime().toString() +')/')
    if (value == '0') { 
    
      this.form.get('GBPeriodId').patchValue('-1');
      this.form.get('GBPeriodName').patchValue('NONE');
      this.form.get('CompareGBPeriodId').patchValue('-1');
      this.form.get('CompareGBPeriodName').patchValue('NONE');
      this.form.get('TargetFromDate').patchValue("today");
      this.form.get('TargetToDate').patchValue("today");
      this.form.get('TargetCompareFromDate').patchValue("today");
      this.form.get('TargetCompareToDate').patchValue("today");
      this.dateChange=!this.dateChange
    }
    if (value == '1') {

      this.form.get('GBPeriodId').patchValue('-1'); 
      this.form.get('GBPeriodName').patchValue('NONE');
      this.form.get('CompareGBPeriodId').patchValue('-1');
      this.form.get('CompareGBPeriodName').patchValue('NONE');   
    }
  
    if (value == '2') {

      this.form.get('TargetFromDate').patchValue("today");
      this.form.get('TargetToDate').patchValue("today");
      this.form.get('TargetCompareFromDate').patchValue("today");
      this.form.get('TargetCompareToDate').patchValue("today");
      this.dateChange=!this.dateChange

    }
  }

  public TargetMasterRetrivalValue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(TargetMaster => {
        this.form.patchValue(TargetMaster);
        this.dateChange=!this.dateChange
      })
    }
  }


  public TargetMasterPatchValue(SelectedPicklistDatas: any): void {
    console.log("SelectedPicklistDatas",SelectedPicklistDatas)
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
} 


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<ITargetMaster> {
  const dbds: GBBaseDBDataService<ITargetMaster> = new GBBaseDBDataService<ITargetMaster >(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<ITargetMaster>, dbDataService: GBBaseDBDataService<ITargetMaster>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<ITargetMaster> {
  return new GBDataPageService<ITargetMaster >(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
