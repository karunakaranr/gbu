import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { PartyPriceCategoryService } from '../../services/PartyPriceCategory/PartyPriceCategory.service';
import { IPartyPriceCategory } from '../../models/IPartyPriceCategory';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as PartyPriceCategoryJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/PartyPriceCategory.json'
import { SalesURLS } from '../../URLS/urls';

@Component({
  selector: 'app-PartyPriceCategory',
  templateUrl: './PartyPriceCategory.component.html',
  styleUrls: ['./PartyPriceCategory.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'PartyPriceCategoryId'},
    { provide: 'url', useValue: SalesURLS.PartyPriceCategory },
    { provide: 'DataService', useClass: PartyPriceCategoryService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class PartyPriceCategoryComponent extends GBBaseDataPageComponentWN<IPartyPriceCategory > {
  title: string = 'PartyPriceCategory'
  PartyPriceCategoryJSON = PartyPriceCategoryJSON;


  form: GBDataFormGroupWN<IPartyPriceCategory > = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'PartyPriceCategory', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.PartyPriceCategoryFillFunction(arrayOfValues.PartyPriceCategoryId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }


  public PartyPriceCategoryFillFunction(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(PackSet => {
        this.form.patchValue(PackSet);
      })
    }
  }


  public PartyPriceCategoryPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IPartyPriceCategory > {
  const dbds: GBBaseDBDataService<IPartyPriceCategory > = new GBBaseDBDataService<IPartyPriceCategory >(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IPartyPriceCategory >, dbDataService: GBBaseDBDataService<IPartyPriceCategory >, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IPartyPriceCategory > {
  return new GBDataPageService<IPartyPriceCategory >(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
