import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { SummaryList } from './../../services/SummaryList/SummaryList.service';
import { Inject, LOCALE_ID } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ListReport } from '../../services/ListReport/ListReport.service';
import { IPartyWiseSummaryDetail } from '../../models/IPartyWiseSummaryDetail';
import { ICriteriaDTODetail } from '../../models/IFilterDetail';
@Component({
  selector: 'app-SummaryList',
  templateUrl: './SummaryList.component.html',
  styleUrls: ['./SummaryList.component.scss']
})

export class SummaryListComponent implements OnChanges {
  @Input() BiztransactionClassId : number;
  @Input() BiztransactionClassName : string;
  @Input() ReportType : number;
  @Input() FilterData : ICriteriaDTODetail;
  RowData : IPartyWiseSummaryDetail[]=[];
  constructor(public service: SummaryList,  public activeroute:ActivatedRoute,  private router: Router,public listreportservice: ListReport, @Inject(LOCALE_ID) public locale: string ) { }
  ngOnChanges(changes: SimpleChanges): void {
    if(changes.BiztransactionClassId || changes.ReportType){
      this.service.SummaryListview(this.BiztransactionClassId,this.FilterData,this.ReportType).subscribe(menudetails => {
        this.RowData=menudetails;
        console.log("Party Wise Summary Data:",this.RowData)
      })
    }
  }
}