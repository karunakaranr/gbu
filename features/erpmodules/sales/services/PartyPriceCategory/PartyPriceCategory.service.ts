import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';


import { IPartyTaxType } from 'features/erpmodules/admin/models/IPartyTaxType';


 
@Injectable({
  providedIn: 'root'
})
export class PartyPriceCategoryService extends GBBaseDataServiceWN<IPartyTaxType> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IPartyTaxType>) {
    super(dbDataService, 'PartyPriceCategoryId');
  }
}
