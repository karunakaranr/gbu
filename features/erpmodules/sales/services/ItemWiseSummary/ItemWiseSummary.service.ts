import { Injectable } from '@angular/core';
import { ItemWiseSummaryDbService } from './../../dbservices/ItemWiseSummary/ItemWiseSummaryDb.service';
import { ICriteriaDTODetail } from '../../models/IFilterDetail';
@Injectable({
  providedIn: 'root'
})
export class ItemWiseSummary {
  constructor(public dbservice: ItemWiseSummaryDbService) { }

  public ItemWiseSummaryview(BiztransactionClassId : number,FilterData : ICriteriaDTODetail,ReportType: number){               
    return this.dbservice.SummaryService(BiztransactionClassId,FilterData,ReportType);
  }
}