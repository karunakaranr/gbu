import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';


import { IPriceCategory } from '../../models/IPriceCategory';


 
@Injectable({
  providedIn: 'root'
})
export class PriceCategoryService extends GBBaseDataServiceWN<IPriceCategory> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IPriceCategory>) {
    super(dbDataService, 'PriceCategoryId');
  }
}