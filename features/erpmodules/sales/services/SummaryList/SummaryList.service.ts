import { Injectable } from '@angular/core';
import { SummaryListDbService } from './../../dbservices/SummaryList/SummaryListDb.service';
import { ICriteriaDTODetail } from '../../models/IFilterDetail';
@Injectable({
  providedIn: 'root'
})
export class SummaryList {
  constructor(public dbservice: SummaryListDbService) { }

  public SummaryListview(BiztransactionClassId : number,FilterData : ICriteriaDTODetail,ReportType: number){               
    return this.dbservice.picklist(BiztransactionClassId,FilterData,ReportType);
  }
}