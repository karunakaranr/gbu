import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';


import { IPriceListType } from '../../models/IPriceListType';


 
@Injectable({
  providedIn: 'root'
})
export class PriceListTypeService extends GBBaseDataServiceWN<IPriceListType> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IPriceListType>) {
    super(dbDataService, 'PriceListTypeId');
  }
}
