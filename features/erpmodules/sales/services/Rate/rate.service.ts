import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';


import { IRate } from '../../models/IRate';


 
@Injectable({
  providedIn: 'root'
})
export class RateService extends GBBaseDataServiceWN<IRate> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IRate>) {
    super(dbDataService, 'RateId');
  }
}
