import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';


import { ICashDiscount } from '../../models/ICashDiscount';

 
@Injectable({
  providedIn: 'root'
})
export class CashDiscountService extends GBBaseDataServiceWN<ICashDiscount> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<ICashDiscount>) {
    super(dbDataService, 'CashDiscountId');
  }
}
