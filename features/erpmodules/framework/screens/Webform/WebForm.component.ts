import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';

import { IWebForm } from '../../models/IWebform';
import { WebFormService } from '../../services/Webform/Webform.service';
import { ButtonVisibility } from 'features/layout/store/layout.actions';

import * as webformjson from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/WebForm.json'
import { FrameworkURLS } from '../../URLS/url';
@Component({
  selector: "app-WebForm",
  templateUrl: "./WebForm.component.html",
  styleUrls: ["./WebForm.component.scss"],
  providers: [
    { provide: 'IdField', useValue: 'WebFormId' },
    { provide: 'url', useValue: FrameworkURLS.WebForm },
    { provide: 'DataService', useClass: WebFormService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class WebFormComponent extends GBBaseDataPageComponentWN<IWebForm> {
  title: string = "WebForm"
  webformjson = webformjson;



  form: GBDataFormGroupWN<IWebForm> = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'WebForm', {}, this.gbps.dataService);
  thisConstructor() {
    console.log("webformjson:",webformjson)
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.WebFormFillFunction(arrayOfValues.WebFormId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }




  public WebFormFillFunction(SelectedPicklistData: string): void {

    console.log("filling", SelectedPicklistData)
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(WebForm => {
        this.form.patchValue(WebForm);
      })
    }
  }


  public WebFormPatchValue(SelectedPicklistDatas: any): void {

    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
    console.log("iteem==>", SelectedPicklistDatas)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IWebForm> {
  const dbds: GBBaseDBDataService<IWebForm> = new GBBaseDBDataService<IWebForm>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IWebForm>, dbDataService: GBBaseDBDataService<IWebForm>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IWebForm> {
  return new GBDataPageService<IWebForm>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
