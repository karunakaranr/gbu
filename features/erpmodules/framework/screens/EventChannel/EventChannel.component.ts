import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { IEventChannel } from '../../models/IEventChannel';
import { FrameworkURLS } from '../../URLS/url';
import { EventChannelService } from '../../services/EventChannel.service';
import *as EventChannelJSON from '././../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/EventChannel.json'


@Component({
  selector: 'app-EventChannel',
  templateUrl: './EventChannel.component.html',
  styleUrls: ['./EventChannel.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'EventChannelId'},
    { provide: 'url', useValue: FrameworkURLS.EventChannel },
    { provide: 'DataService', useClass: EventChannelService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class EventChannelComponent extends GBBaseDataPageComponentWN<IEventChannel > {
  title: string = 'EventChannel'
  EventChannelJSON = EventChannelJSON;


  form: GBDataFormGroupWN<IEventChannel > = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'EventChannel', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.FormFillFunction(arrayOfValues.EventChannelId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }


  public FormFillFunction(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(PackSet => {
        this.form.patchValue(PackSet);
      })
    }
  }


  public PicklistPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IEventChannel > {
  const dbds: GBBaseDBDataService<IEventChannel > = new GBBaseDBDataService<IEventChannel >(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IEventChannel >, dbDataService: GBBaseDBDataService<IEventChannel >, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IEventChannel > {
  return new GBDataPageService<IEventChannel >(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}


