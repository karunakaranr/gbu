import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { FrameworkURLS } from '../../URLS/url';
import { PayTaxTypeService } from '../../services/PayTaxType.Service';
import { IPayTaxType } from '../../models/IPayTaxType';
import * as PayTaxTypeJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/PayTaxType.json'



@Component({
  selector: 'app-PayTaxType',
  templateUrl: './PayTaxType.component.html',
  styleUrls: ['./PayTaxType.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'PayTaxTypeId'},
    { provide: 'url', useValue: FrameworkURLS.PayTaxType},
    { provide: 'DataService', useClass: PayTaxTypeService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class PayTaxTypeComponent extends GBBaseDataPageComponentWN<IPayTaxType> {
  title: string = 'PayTaxType'
  PayTaxTypeJSON = PayTaxTypeJSON;


  form: GBDataFormGroupWN<IPayTaxType > = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'PayTaxType', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.FormFillFunction(arrayOfValues.PayTaxTypeId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }


  public FormFillFunction(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(PackSet => {
        this.form.patchValue(PackSet);
      })
    }
  }


  public PicklistPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IPayTaxType > {
  const dbds: GBBaseDBDataService<IPayTaxType > = new GBBaseDBDataService<IPayTaxType >(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IPayTaxType >, dbDataService: GBBaseDBDataService<IPayTaxType >, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IPayTaxType > {
  return new GBDataPageService<IPayTaxType >(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
