import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { IPicklist } from '../../models/IPicklist';
import { PicklistService } from '../../services/Picklist.service';
import { ButtonVisibility } from 'features/layout/store/layout.actions';

import * as PicklistJson from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/Picklist.json'

import { FrameworkURLS } from '../../URLS/url';



@Component({
  selector: "app-Picklist",
  templateUrl: "./Picklist.component.html",
  styleUrls: ["./Picklist.component.scss"],
  providers: [
    { provide: 'IdField', useValue: 'PicklistId' },
    { provide: 'url', useValue: FrameworkURLS.Picklist },
    { provide: 'DataService', useClass: PicklistService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class PicklistComponent extends GBBaseDataPageComponentWN<IPicklist> {
  title: string = "Picklist"
  PicklistJson = PicklistJson;



  form: GBDataFormGroupWN<IPicklist> = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'Picklist', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.PicklistFillFunction(arrayOfValues.PicklistId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }



  public PicklistFillFunction(SelectedPicklistData: string): void {

   
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(Picklist => {
        this.form.patchValue(Picklist);
      })
    }
  }


  public PicklistPatchValue(SelectedPicklistDatas: any): void {
    
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
    // console.log("iteem==>",SelectedPicklistDatas)
  }
  
  public GridOutput(Value){
    console.log("Grid Output:",Value)
  }
}




export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IPicklist> {
  const dbds: GBBaseDBDataService<IPicklist> = new GBBaseDBDataService<IPicklist>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IPicklist>, dbDataService: GBBaseDBDataService<IPicklist>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IPicklist> {
  return new GBDataPageService<IPicklist>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
