import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { IProgramType } from '../../models/IProgramType';
import { FrameworkURLS } from '../../URLS/url';
import { ProgramTypeService } from '../../services/ProgramType.Service';
import * as  ProgramTypeJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/ProgramType.json';


@Component({
  selector: 'app-ProgramType',
  templateUrl: './ProgramType.component.html',
  styleUrls: ['./ProgramType.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'ProgramTypeId'},
    { provide: 'url', useValue: FrameworkURLS.ProgramType },
    { provide: 'DataService', useClass: ProgramTypeService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class ProgramTypeComponent extends GBBaseDataPageComponentWN<IProgramType > {
  title: string = 'ProgramType'
  ProgramTypeJSON =  ProgramTypeJSON;


  form: GBDataFormGroupWN<IProgramType > = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'ProgramType', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.FormFillFunction(arrayOfValues.ProgramTypeId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }


  public FormFillFunction(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(PackSet => {
        this.form.patchValue(PackSet);
      })
    }
  }


  public PicklistPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IProgramType > {
  const dbds: GBBaseDBDataService<IProgramType > = new GBBaseDBDataService<IProgramType >(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IProgramType >, dbDataService: GBBaseDBDataService<IProgramType >, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IProgramType > {
  return new GBDataPageService<IProgramType >(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}


