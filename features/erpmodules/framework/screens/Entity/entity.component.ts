import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import {IEntity} from 'features/erpmodules/FrameWork/models/IEntity'
import { EntityNewService } from '../../services/Entity.service';

import { FrameworkURLS } from './../../URLS/url';
import * as EntityJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/Entity.json'
import { ButtonVisibility } from 'features/layout/store/layout.actions';

@Component({
  selector: 'app-Entity',
  templateUrl: 'entity.component.html',
  styleUrls: ['entity.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'EntityId' },
    { provide: 'url', useValue:FrameworkURLS.Entity },
    { provide: 'DataService', useClass: EntityNewService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class EntityNewComponent extends GBBaseDataPageComponentWN<IEntity> {
  title: string = "Entity"
  EntityJSON = EntityJSON;

  form: GBDataFormGroupWN<IEntity> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "Entity", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.EntityValue(arrayOfValues.EntityId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }
  public ChangeTab(event, TabName:string) :void{
    var i:number, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("FormTabLinks");
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(TabName).style.display = "block";
    console.log("Event:",event)
    event.currentTarget.className += " active";
  }

  public EntityValue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(Entity => {
        this.form.patchValue(Entity);
      })
    }
  }
  public EntityNot(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}

export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IEntity> {
  const dbds: GBBaseDBDataService<IEntity> = new GBBaseDBDataService<IEntity>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IEntity>, dbDataService: GBBaseDBDataService<IEntity>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IEntity> {
  return new GBDataPageService<IEntity>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
