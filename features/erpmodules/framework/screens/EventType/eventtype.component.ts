import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { FrameworkURLS } from '../../URLS/url';
import { EventTypeService } from '../../services/eventtype.service';
import { IEventType } from '../../models/IEventType';
import * as EventTypeJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/EventType.json'
import { DialogBoxComponent } from 'features/erpmodules/samplescreens/screens/DialogBox/DialogBox.component';

@Component({
  selector: 'app-EventType',
  templateUrl: './eventtype.component.html',
  styleUrls: ['./eventtype.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'EventTypeId'},
    { provide: 'url', useValue: FrameworkURLS.EventType },
    { provide: 'DataService', useClass: EventTypeService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class EventTypeComponent extends GBBaseDataPageComponentWN<IEventType> {
  title: string = "EventType"
  EventTypeJSON = EventTypeJSON; 


  form: GBDataFormGroupWN<IEventType > = new GBDataFormGroupWN(this.gbps.gbhttp.http, "EventType", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.EventTypeRetrivalValue(arrayOfValues.EventTypeId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }
 

  public EventTypeRetrivalValue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(EvenType => {
        this.form.patchValue(EvenType);
        console.log("EvenType",EvenType)
        // this.form.get('EntityModuleName').patchValue(EvenType.EntityModuleName);
        
      })
    }
  }


  public EventTypePatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }

  Security(event: any): void {
    let value = event.target.value;

    value = Number(value);
    if (!/^\d+$/.test(value)||value > 9) {
      this.form.get('EventTypeSecurityLevel').setValue('0');
      const dialogRef = this.dialog.open(DialogBoxComponent, {
        width: '400px',
        data: {
          message: 'Security Value Should Be Between 1 & 9',
          heading: 'Info',
        }
      });
    }
    else {
      this.form.get('EventTypeSecurityLevel').setValue(value);
    }
  }

}

export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IEventType > {
  const dbds: GBBaseDBDataService<IEventType > = new GBBaseDBDataService<IEventType >(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IEventType >, dbDataService: GBBaseDBDataService<IEventType >, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IEventType > {
  return new GBDataPageService<IEventType >(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}


