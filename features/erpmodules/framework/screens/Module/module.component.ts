import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ModuleService } from '../../services/Module.Service';
import { FrameworkURLS } from '../../URLS/url';
import { IModule } from '../../models/IModule';
import * as ModuleNewJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/ModuleNew.json'
import { ButtonVisibility } from 'features/layout/store/layout.actions';

@Component({
  selector: 'app-ModuleNew',
  templateUrl:'module.component.html',
  styleUrls:['module.component.scss'],
  providers: [
    { provide: 'IdField', useValue:'ModuleId' },
    { provide: 'url', useValue: FrameworkURLS.ModuleNew},
    { provide: 'DataService', useClass: ModuleService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class ModuleComponent extends GBBaseDataPageComponentWN<IModule> {
  title: string = "ModuleNew"
  ModuleNewJSON = ModuleNewJSON;

  form: GBDataFormGroupWN<IModule> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "ModuleNew", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.ModuleFormVal(arrayOfValues.ModuleId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }
  public ModuleFormVal(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(Module => {
          this.form.patchValue(Module);
      })
    }
  }
  public Modulepatch(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}
export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IModule> {
  const dbds: GBBaseDBDataService<IModule> = new GBBaseDBDataService<IModule>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IModule>, dbDataService: GBBaseDBDataService<IModule>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IModule> {
  return new GBDataPageService<IModule>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
