import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { FrameworkURLS } from '../../URLS/url';
import { ClientSiteService } from '../../services/ClientSite.Service';
import { IClientSite } from '../../models/IClientSite';
import * as ClientSiteJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/ClientSite.json'



@Component({
  selector: 'app-ClientSite',
  templateUrl: './ClientSite.component.html',
  styleUrls: ['./ClientSite.component.scss'],
  providers: [
    { provide: 'IdField', useValue:'ClientSiteId'},
    { provide: 'url', useValue: FrameworkURLS.ClientSite },
    { provide: 'DataService', useClass: ClientSiteService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class ClientSiteComponent extends GBBaseDataPageComponentWN< IClientSite > {
  title: string = 'ClientSite'
  ClientSiteJSON = ClientSiteJSON;


  form: GBDataFormGroupWN<IClientSite > = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'ClientSite', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.FormFillFunction(arrayOfValues.ClientSiteId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }


  public FormFillFunction(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(PackSet => {
        this.form.patchValue(PackSet);
      })
    }
  }


  public PicklistPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IClientSite > {
  const dbds: GBBaseDBDataService<IClientSite > = new GBBaseDBDataService<IClientSite >(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IClientSite >, dbDataService: GBBaseDBDataService<IClientSite >, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IClientSite > {
  return new GBDataPageService<IClientSite >(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}


