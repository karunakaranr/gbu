import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
// import { IClient } from '../../models/IClient';
import { ClientService } from '../../services/client.service';

import {IClient } from './../../models/IClient'
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as ClientJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/Client.json'

import { FrameworkURLS } from '../../URLS/url';
@Component({
  selector: 'app-Client',
  templateUrl:'client.component.html',
  styleUrls:['client.component.scss'],
  providers: [
    { provide: 'IdField', useValue:'ClientId' },
    { provide: 'url', useValue: FrameworkURLS.Client},
    { provide: 'DataService', useClass: ClientService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class ClientComponent extends GBBaseDataPageComponentWN<IClient> {
  title: string = "Client"
  ClientJSON = ClientJSON;

  form: GBDataFormGroupWN<IClient> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "Client", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.ClientFormVal(arrayOfValues.ClientId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }
  public ClientFormVal(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(Client => {
          this.form.patchValue(Client);
      })
    }
  }
  public Clientpatch(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}
export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IClient> {
  const dbds: GBBaseDBDataService<IClient> = new GBBaseDBDataService<IClient>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IClient>, dbDataService: GBBaseDBDataService<IClient>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IClient> {
  return new GBDataPageService<IClient>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
