import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
// import { IReportFormat } from '../../models/IReportFormat';
// import { ReportFormatService } from '../../services/ReportFormat/ReportFormat.service';
import { ReportFormatService } from '../../services/ReportFormat/ReportFormat.service';
import { IReportFormat } from '../../models/IReportFormat';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
// import * as ReportFormatjson from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/ReportFormat.json'
import * as ReportFormatjson from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/ReportFormat.json'
import { FrameworkURLS } from '../../URLS/url';

@Component({
  selector: "app-ReportFormat",
  templateUrl: "./ReportFormat.component.html",
  styleUrls: ["./ReportFormat.component.scss"],
  providers: [
    { provide: 'IdField', useValue: 'ReportFormatId' },
    { provide: 'url', useValue: FrameworkURLS.ReportFormat },
    { provide: 'DataService', useClass: ReportFormatService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class ReportFormatComponent extends GBBaseDataPageComponentWN<IReportFormat> {
  title: string = "ReportFormat"
  ReportFormatjson = ReportFormatjson;



  form: GBDataFormGroupWN<IReportFormat> = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'ReportFormat', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.ReportFormatFillFunction(arrayOfValues.ReportFormatId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }


  public ReportFormatFillFunction(SelectedPicklistData: string): void {

   console.log("filling",SelectedPicklistData)
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(ReportFormat => {
        this.form.patchValue(ReportFormat);
      })
    }
  }


  public ReportFormatPatchValue(SelectedPicklistDatas: any): void {
    
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
    console.log("iteem==>",SelectedPicklistDatas)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IReportFormat> {
  const dbds: GBBaseDBDataService<IReportFormat> = new GBBaseDBDataService<IReportFormat>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IReportFormat>, dbDataService: GBBaseDBDataService<IReportFormat>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IReportFormat> {
  return new GBDataPageService<IReportFormat>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
