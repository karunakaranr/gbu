import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ProgramAssemblyService } from '../../services/ProgramAssembly/ProgramAssembly.service';
import { IProgramAssembly } from '../../models/IProgramAssembly';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as ProgramAssemblyJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/ProgramAssembly.json'
import { FrameworkURLS } from '../../URLS/url';


@Component({
  selector: 'app-ProgramAssembly',
  templateUrl: './ProgramAssembly.component.html',
  styleUrls: ['./ProgramAssembly.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'ProgramAssemblyId'},
    { provide: 'url', useValue: FrameworkURLS.ProgramAssembly },
    { provide: 'DataService', useClass: ProgramAssemblyService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class ProgramAssemblyComponent extends GBBaseDataPageComponentWN<IProgramAssembly > {
  title: string = 'ProgramAssembly'
  ProgramAssemblyJSON = ProgramAssemblyJSON;


  form: GBDataFormGroupWN<IProgramAssembly > = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'ProgramAssembly', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.ProgramAssemblyRetrivalValue(arrayOfValues.ProgramAssemblyId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }


  public ProgramAssemblyRetrivalValue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(PackSet => {
        this.form.patchValue(PackSet);
      })
    }
  }


  public ProgramAssemblyPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IProgramAssembly > {
  const dbds: GBBaseDBDataService<IProgramAssembly > = new GBBaseDBDataService<IProgramAssembly >(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IProgramAssembly >, dbDataService: GBBaseDBDataService<IProgramAssembly >, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IProgramAssembly > {
  return new GBDataPageService<IProgramAssembly >(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}


