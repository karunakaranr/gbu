import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { FrameworkURLS } from '../../URLS/url';
import { EventSourceService } from '../../services/EventSource.service';
import { FormBuilder } from '@angular/forms';
import * as EventSourceJSON from  './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/EventSource.json'
import { IEventSource } from '../../models/IEventSource';


@Component({
  selector: 'app-EventSource',
  templateUrl: './EventSource.component.html',
  styleUrls: ['./EventSource.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'EventSourceId'},
    { provide: 'url', useValue: FrameworkURLS.EventSource },
    { provide: 'DataService', useClass: EventSourceService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class EventSourceComponent extends GBBaseDataPageComponentWN<IEventSource > {
  title: string = 'EventSource'
  EventSourceJSON = EventSourceJSON;


  form: GBDataFormGroupWN<IEventSource > = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'EventSource', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.FormFillFunction(arrayOfValues.EventSourceId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }


  public FormFillFunction(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(PackSet => {
        this.form.patchValue(PackSet);
      })
    }
  }


  public PicklistPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IEventSource > {
  const dbds: GBBaseDBDataService<IEventSource > = new GBBaseDBDataService<IEventSource >(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IEventSource >, dbDataService: GBBaseDBDataService<IEventSource >, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IEventSource > {
  return new GBDataPageService<IEventSource >(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
