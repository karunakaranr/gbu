import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { FrameworkURLS } from '../../URLS/url';
import { ProgramClassService } from '../../services/ProgramClass.service';
import { IProgramClass } from '../../models/IProgramClass';
import * as ProgramClassJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/ProgramClass.json'


@Component({
  selector: 'app-ProgramClass',
  templateUrl: './ProgramClass.component.html',
  styleUrls: ['./ProgramClass.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'ProgramClassId'},
    { provide: 'url', useValue: FrameworkURLS.ProgramClass },
    { provide: 'DataService', useClass: ProgramClassService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class ProgramClassComponent extends GBBaseDataPageComponentWN<IProgramClass > {
  title: string = 'ProgramClass' 
  ProgramClassJSON = ProgramClassJSON;


  form: GBDataFormGroupWN<IProgramClass > = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'ProgramClass', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.FormFillFunction(arrayOfValues.ProgramClassId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }


  public FormFillFunction(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(PackSet => {
        this.form.patchValue(PackSet);
      })
    }
  }


  public PicklistPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IProgramClass > {
  const dbds: GBBaseDBDataService<IProgramClass > = new GBBaseDBDataService<IProgramClass >(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IProgramClass >, dbDataService: GBBaseDBDataService<IProgramClass >, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IProgramClass > {
  return new GBDataPageService<IProgramClass >(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
