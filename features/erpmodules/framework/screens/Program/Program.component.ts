import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { IProgram } from '../../models/IProgram';
import { ProgramService } from '../../services/Program/Program.service';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as Programjson from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/Program.json'
import { FrameworkURLS } from '../../URLS/url';

@Component({
  selector: "app-Program",
  templateUrl: "./Program.component.html",
  styleUrls: ["./Program.component.scss"],
  providers: [
    { provide: 'IdField', useValue: 'ProgramId' },
    { provide: 'url', useValue: FrameworkURLS.Program },
    { provide: 'DataService', useClass: ProgramService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class ProgramComponent extends GBBaseDataPageComponentWN<IProgram> {
  title: string = "Program"
  Programjson = Programjson;



  form: GBDataFormGroupWN<IProgram> = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'Program', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.ProgramFillFunction(arrayOfValues.ProgramId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }


  public ProgramFillFunction(SelectedPicklistData: string): void {

   console.log("filling",SelectedPicklistData)
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(Program => {
        this.form.patchValue(Program);
      })
    }
  }


  public ProgramPatchValue(SelectedPicklistDatas: any): void {
    
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
    console.log("iteem==>",SelectedPicklistDatas)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IProgram> {
  const dbds: GBBaseDBDataService<IProgram> = new GBBaseDBDataService<IProgram>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IProgram>, dbDataService: GBBaseDBDataService<IProgram>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IProgram> {
  return new GBDataPageService<IProgram>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
