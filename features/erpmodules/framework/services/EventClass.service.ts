import { Inject, Injectable } from '@angular/core';

import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';

import { IEventClass } from '../models/IEventClass';

@Injectable({
  providedIn: 'root'
})
export class EventClassService extends GBBaseDataServiceWN<IEventClass> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IEventClass>) {
    super(dbDataService, 'EventClassId');
  }
}