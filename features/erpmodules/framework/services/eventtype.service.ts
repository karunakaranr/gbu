import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';


import { IEventType } from '../models/IEventType';


 
@Injectable({
  providedIn: 'root'
})
export class EventTypeService extends GBBaseDataServiceWN<IEventType> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IEventType>) {
    super(dbDataService, 'EventTypeId');
  }
}
