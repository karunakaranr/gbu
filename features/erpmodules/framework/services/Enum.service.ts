import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { IEnum } from '../models/IEnum';

@Injectable({
  providedIn: 'root'
})
export class EnumService extends GBBaseDataServiceWN<IEnum> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IEnum>) {
    super(dbDataService, 'EnumarationId');
  }
}
