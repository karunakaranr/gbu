import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { IProgramClass } from '../models/IProgramClass';





 
@Injectable({
  providedIn: 'root'
})
export class ProgramClassService extends GBBaseDataServiceWN<IProgramClass> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IProgramClass>) {
    super(dbDataService, 'ProgramClassId');
  }
}
