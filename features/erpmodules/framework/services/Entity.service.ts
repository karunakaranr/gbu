import { Inject, Injectable } from '@angular/core';

import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { IEntity } from '../models/IEntity';


@Injectable({
  providedIn: 'root'
})
export class EntityNewService extends GBBaseDataServiceWN<IEntity> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IEntity>) {
    super(dbDataService, 'EntityId');
  }
}