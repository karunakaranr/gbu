import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';

import { IProgramAssembly } from '../../models/IProgramAssembly';


 
@Injectable({
  providedIn: 'root'
})
export class ProgramAssemblyService extends GBBaseDataServiceWN<IProgramAssembly> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IProgramAssembly>) {
    super(dbDataService, 'ProgramAssemblyId');
  }
}
