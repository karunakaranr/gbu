import { Inject, Injectable } from '@angular/core';

import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { IModule } from '../models/IModule';


@Injectable({
  providedIn: 'root'
})
export class ModuleService extends GBBaseDataServiceWN<IModule> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IModule>) {
    super(dbDataService, 'ModuleId');
  }
}