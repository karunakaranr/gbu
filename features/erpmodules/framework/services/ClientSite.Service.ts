import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { IClientSite } from '../models/IClientSite';





 
@Injectable({
  providedIn: 'root'
})
export class ClientSiteService extends GBBaseDataServiceWN<IClientSite> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IClientSite>) {
    super(dbDataService, 'ClientSiteId');
  }
}
