import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';

import { IPortletType } from '../models/IPortletType';


 
@Injectable({
  providedIn: 'root'
})
export class PortletTypeService extends GBBaseDataServiceWN<IPortletType> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IPortletType>) {
    super(dbDataService, 'PortletTypeId');
  }
}
