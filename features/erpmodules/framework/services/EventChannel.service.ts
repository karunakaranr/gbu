import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';


import { IEventChannel } from '../models/IEventChannel';


 
@Injectable({
  providedIn: 'root'
})
export class EventChannelService extends GBBaseDataServiceWN<IEventChannel> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IEventChannel>) {
    super(dbDataService, 'EventChannelId');
  }
}
