import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';


import { IEventSource } from '../models/IEventSource';


 
@Injectable({
  providedIn: 'root'
})
export class EventSourceService extends GBBaseDataServiceWN<IEventSource> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IEventSource>) {
    super(dbDataService, 'EventSourceId');
  }
}
