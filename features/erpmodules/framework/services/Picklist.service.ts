import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN,GBBaseDBDataService } from 'libs/gbdata/src';
import { IPicklist } from '../models/IPicklist';





 
@Injectable({
  providedIn: 'root'
})
export class PicklistService extends GBBaseDataServiceWN<IPicklist> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IPicklist>) {
    super(dbDataService, 'PicklistId');
  }
}
