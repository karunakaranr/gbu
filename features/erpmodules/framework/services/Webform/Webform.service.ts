import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN,GBBaseDBDataService } from 'libs/gbdata/src';


import { IWebForm } from '../../models/IWebform';
 
@Injectable({
  providedIn: 'root'
})
export class WebFormService extends GBBaseDataServiceWN<IWebForm> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IWebForm>) {
    super(dbDataService, 'WebFormId');
  }
}
