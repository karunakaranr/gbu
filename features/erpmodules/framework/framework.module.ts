import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { UicoreModule } from '@goodbooks/uicore';
import { UifwkUifwkmaterialModule } from '@goodbooks/uifwk/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslocoRootModule } from './../../transloco/transloco-root.module';
import { GbdataModule } from '@goodbooks/gbdata';
import { environment } from '../../environments/environment';
import { NgBreModule } from '@unisoft/ng-bre';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { GbchartModule } from '../../common/shared/screens/gbchart/gbchart.module';
import { EntitylookupModule } from './../../entitylookup/entitylookup.module';
import { geolocationModule } from './../../geolocation/geolocation.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { AgGridModule } from 'ag-grid-angular';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule } from '@angular/material/button';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { MatDividerModule } from '@angular/material/divider';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatTabsModule } from '@angular/material/tabs';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { AgmCoreModule } from '@agm/core';
import { AgmDirectionModule } from 'agm-direction';
import { GbgridModule } from 'features/gbgrid/components/gbgrid/gbgrid.module';
import { CommonReportModule } from './../../commonreport/commonreport.module';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MaterialModule } from 'features/common/material.module';
import { MatInputModule } from '@angular/material/input';
import { SamplescreensModule } from '../samplescreens/samplescreens.module';
import { PortletTypeComponent } from './screens/PortletType/PortletType.component';
import { FrameWorkroutingModule } from './framework.routing.module';
import { FrameWorkComponent } from './framework.component';
import { EventTypeComponent } from './screens/EventType/eventtype.component';
import { EventSourceComponent } from './screens/EventSource/EventSource.component';
import { ProgramClassComponent } from './screens/ProgramClass/ProgramClass.Component';
import { GcmTypeComponent } from './screens/GcmType/GcmType.Component';
import { EventClassComponent } from './screens/EventClass/eventclass.component';
import { WebFormComponent } from './screens/Webform/Webform.component';
import { ClientComponent } from './screens/Client/client.component';
import { EnumComponent } from './screens/Enum/enum.component';
import { PicklistComponent } from './screens/Picklist/Picklist.component';
import { EventChannelComponent } from './screens/EventChannel/EventChannel.component';
import { ProgramTypeComponent } from './screens/ProgramType/ProgramType.Component';
import { ProgramComponent } from './screens/Program/Program.component';
import { ProgramAssemblyComponent } from './screens/ProgramAssembly/ProgramAssembly.component';
import { ClientSiteComponent } from './screens/ClientSite/ClientSite.Component';
import { PayTaxTypeComponent } from './screens/PayTaxType/PayTaxType.component';
import { ReportFormatComponent } from './screens/ReportFormat/ReportFormat.component';
import { EntityNewComponent } from './screens/Entity/entity.component';
import { ModuleComponent } from './screens/Module/module.component';
@NgModule({
    declarations: [
      WebFormComponent,
      FrameWorkComponent,
      EventTypeComponent,
      EntityNewComponent,
      PayTaxTypeComponent,
      ReportFormatComponent,
      ProgramComponent,
      EventSourceComponent,
      ProgramAssemblyComponent,
      ProgramClassComponent,
      GcmTypeComponent,
      PortletTypeComponent,
      EventClassComponent,
      ClientComponent,
      EnumComponent,
      PicklistComponent,
      EventChannelComponent,
      ProgramTypeComponent,
      ClientSiteComponent,
      ModuleComponent
    
    ],
    imports: [
      SamplescreensModule,
      CommonModule,
      FrameWorkroutingModule, 
      ReactiveFormsModule,
      FormsModule,
      UicoreModule,
      NgBreModule,
      UifwkUifwkmaterialModule,
      TranslocoRootModule,
      AgGridModule,
      GbgridModule,
      GbdataModule.forRoot(environment),
      AgmCoreModule.forRoot({
        apiKey: 'AIzaSyDvFSDxJXXR1nrdRDIzvp6DRZB9v649My8',
        libraries: ['places', 'geometry', 'drawing'],
      }),
      AgmDirectionModule,
      NgxChartsModule,
      GbchartModule,
      EntitylookupModule,
      geolocationModule,
      NgSelectModule,
      MaterialModule,
      MatSortModule,
      MatFormFieldModule,
      MatSelectModule,
      MatButtonModule,
      MatListModule,
      MatTabsModule,
      MatCheckboxModule,
      MatTableModule,
      MatIconModule,
      MatExpansionModule,
      MatDividerModule,
      MatPaginatorModule,
      CommonReportModule,
      MatFormFieldModule,
      MatInputModule,
    ],
    exports: [
      FrameWorkComponent,
      MatSortModule,
      MatFormFieldModule,
      MatInputModule,
    ],
  })
  export class FrameWorkModule {
    constructor() {}
  }