export interface IProgramAssembly {
  ProgramAssemblyId: number
  ProgramAssemblyName: string
  ProgramAssemblySection: string
  ProgramAssemblyRemarks: string
  ModuleId: number
  ModuleCode: string
  ModuleName: string
  ProgramAssemblyCreatedById: number
  ProgramAssemblyCreatedByName: string
  ProgramAssemblyCreatedOn: string
  ProgramAssemblyModifiedById: number
  ProgramAssemblyModifiedByName: string
  ProgramAssemblyModifiedOn: string
  ProgramAssemblySortOrder: number
  ProgramAssemblyStatus: number
  ProgramAssemblyVersion: number
  ProgramAssemblySourceType: number
  ProgramAssemblyScreenOperationMode: number
}
