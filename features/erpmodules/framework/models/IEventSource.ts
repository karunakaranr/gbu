export interface IEventSource {
    EventSourceId: number,
    EventSourceCode: string,
    EventSourceName: string,
    EventSourceCreatedById: number,
    EventSourceCreatedOn: string,
    EventSourceModifiedById: number,
    EventSourceModifiedOn: string,
    EventSourceCreatedByName: any,
    EventSourceModifiedByName: any,
    EventSourceSortOrder: number,
    EventSourceStatus: number,
    EventSourceVersion: number,
    EventSourceSourceType: number
  }
  