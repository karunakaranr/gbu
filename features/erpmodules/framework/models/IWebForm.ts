export interface IWebForm {
    WebFormId: number
    WebFormName: string
    WebFormURL: string
    ModuleId: number
    ModuleName: string
    WebFormTemplate: string
    WebFormStatus: number
    WebFormVersion: number
    WebFormCreatedOn: string
    WebFormModifiedOn: string
    WebFormCreatedByName: string
    WebFormModifiedByName: string
    WebFormSourceType: number
    WebFormSecondURL: string
}