export interface IModule {
    ModuleId: number
    ModuleCode: string
    ModuleName: string
    ModuleRemarks: string
    ImageId: number
    ImageName: string
    ProjectId: number
    ProjectCode: string
    ProjectName: string
    MenuId: number
    MenuCode: string
    MenuName: string
    ModuleCreatedById: number
    ModuleCreatedOn: string
    ModuleModifiedById: number
    ModuleModifiedOn: string
    ModuleCreatedByName: any
    ModuleModifiedByName: any
    ModuleSortOrder: number
    ModuleStatus: number
    ModuleVersion: number
    ModuleSourceType: number
    ModuleScreenOperationMode: number
  }
  