export interface IEnum {
    EnumarationId: number
    EnumarationName: string
    EnumarationDescription: string
    EnumarationDetailArray: EnumarationDetailArray[]
    EnumarationCreatedById: number
    EnumarationCreatedByName: any
    EnumarationCreatedOn: string
    EnumarationModifiedById: number
    EnumarationModifiedByName: any
    EnumarationModifiedOn: string
    EnumarationStatus: number
    EnumarationVersion: number
    EnumarationSortOrder: number
    EnumarationSourceType: number
  }
  
  export interface EnumarationDetailArray {
    EnumarationDetailId: number
    EnumarationId: number
    EnumarationName: string
    EnumarationDetailSlNo: number
    EnumarationDetailEnumValue: number
    EnumarationDetailEnumValueDescription: string
  }
  