export interface IPicklist {
    PicklistCode: string
    PicklistName: string
    PicklistId: number
    PicklistTitle: string
    PicklistIsMultiselect: string
    PicklistUri: string
    PicklistSearchableFields: string
    PicklistSearchFields: string
    PicklistDisplayFieldName: string
    PicklistSelectionFieldName: string
    PicklistDisplayFieldTextBox: string
    PicklistWidth: string
    PicklistHeight: string
    PicklistNumberOfSelection: string
    PicklistMinimunNumber: string
    PicklistOutputDtos: PicklistOutputDto[]
    PicklistStatus: number
    PicklistVersion: number
  }
  
  export interface PicklistOutputDto {
    PicklistOutputId: string
    PicklistId: string
    PicklistOutputSlNo: number
    PicklistOutputFieldName: string
    PicklistOutputIsVisible: number
    PicklistOutputHeaderText: string
    PicklistOutputFieldSize: string
    PicklistOutputIsFreezingReq: number
    PicklistOutputIsHeaderWrappingReq: number
    PicklistOutputIsDataWrappingReq: number
  }
  