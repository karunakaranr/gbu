export interface IEventType {
    EventTypeId: number
    EventTypeCode: string
    EventTypeName: string
    EventTypeType: number
    EventType: number
    EventTypeNature: number
    EventTypeSecurityLevel: number
    EventTypeCriticality: number
    LinkedFormId: number
    LinkedFormName: string
    EventTypeNotification: number
    EventTypeAlter: number
    EventTypeSection: string
    EntityId: number
    EntityCode: string
    EntityName: string
    EntityModuleCode: string
    EntityModuleName: string
    EntityPocoToDTOLinkId: number
    EventTypeCreatedById: number
    EventTypeCreatedOn: string
    EventTypeModifiedById: number
    EventTypeModifiedOn: string
    EventTypeCreatedByName: string
    EventTypeModifiedByName: string
    EventTypeSortOrder: number
    EventTypeStatus: number
    EventTypeVersion: number
    EventTypeSourceType: number
    MailId: string
    NumberOfRecord: number
  }
  