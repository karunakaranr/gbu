export interface IProgram {
    ProgramId: number;
    ProgramName: string;
    ProgramSection: string;
    ProgramTypeId: string;
    ProgramTypeName: string;
    ModuleId: string;
    ModuleName: string;
    ProgramClassId: string;
    ProgramClassName: string;
    ProgramRemarks: string;
    ProgramSortOrder: string;
    ProgramCreatedOn: string;
    ProgramModifiedOn: string;
    ProgramModifiedByName: string;
    ProgramCreatedByName: string;
    ProgramStatus: number;
    ProgramVersion: number;
  }