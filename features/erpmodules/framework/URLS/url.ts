export class FrameworkURLS {

    public static EventType = '/fws/EventType.svc/';
    public static EventSource = '/fws/EventSource.svc/';
    public static ProgramClass = '/fws/ProgramClass.svc/';
    public static GcmType = '/fws/GcmType.svc/';
    public static PortletType = '/fws/PortletType.svc/';
    public static EventClass = '/fws/EventClass.svc/';
    public static Client = '/ds/Client.svc/';
    public static DataValidation = '/fws/DataValidation.svc/';
    public static Enum = '/fws/Enumaration.svc/';
    public static Picklist = '/fws/Picklist.svc/';
    public static EventChannel = '/fws/EventChannel.svc/';
    public static ProgramType ='/fws/ProgramType.svc/';
    public static ProgramAssembly = '/fws/ProgramAssembly.svc/';
    public static Module = '/fws/Module.svc/';
    public static Program = '/fws/Program.svc/';
    public static WebForm = '/fws/WebForm.svc/';
    public static ClientSite ='/ds/ClientSite.svc/';
    public static PayTaxType ='prs/PayTaxType.svc/';
    public static ReportFormat = '/ds/ReportFormat.svc/';
    public static Entity = '/fws/Entity.svc/';
    public static ModuleNew = '/fws/Module.svc/';
    
}


    