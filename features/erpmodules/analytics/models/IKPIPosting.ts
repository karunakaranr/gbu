export interface IKPIPosting {
  ModuleName: string
  ModuleId: number
  Section: string
  SectionId: number
  KPIName: string
  KPIId: number
  DateType: number
  FromDate: string
  ToDate: string
}
