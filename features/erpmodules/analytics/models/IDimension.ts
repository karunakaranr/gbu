export interface IDimension {
    DimensionId: number
    DimensionCode: string
    DimensionName: string
    DimensionSourceType: number
    DimensionSortOrder: number
    DimensionStatus: number
    DimensionVersion: number
    DimensionCreatedById: number
    DimensionCreatedOn: string
    DimensionModifiedById: number
    DimensionModifiedOn: string
    DimensionCreatedByName: any
    DimensionModifiedByName: any
    DbObjectId: number
    DbObjectName: string
  }
  