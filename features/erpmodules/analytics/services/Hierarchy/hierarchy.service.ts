import { Inject, Injectable } from '@angular/core';

import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';

import { IHierarchy } from 'features/erpmodules/analytics/models/IHierarchy';

@Injectable({
  providedIn: 'root'
})
export class HierarchyService extends GBBaseDataServiceWN<IHierarchy> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IHierarchy>) {
    super(dbDataService, 'HierarchyId');
  }
} 