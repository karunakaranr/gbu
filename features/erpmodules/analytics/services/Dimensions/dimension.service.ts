import { Inject, Injectable } from '@angular/core';

import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';

import { IDimension } from 'features/erpmodules/analytics/models/IDimension';


@Injectable({
  providedIn: 'root'
})
export class DimensionService extends GBBaseDataServiceWN<IDimension> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IDimension>) {
    super(dbDataService, 'DimensionId');
  }
}