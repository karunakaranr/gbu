import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';

import { IKPIPosting } from '../../models/IKPIPosting';
 
@Injectable({
  providedIn: 'root'
})
export class KPIPostingService extends GBBaseDataServiceWN<IKPIPosting> {

  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IKPIPosting>) {
    super(dbDataService, 'ModuleId');
  }
}
