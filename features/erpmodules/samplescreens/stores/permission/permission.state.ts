import { State, Action, StateContext } from '@ngxs/store';
import { permissiondetail } from './../../models/permission/ipermission.model';
import { GBDataState, GBDataStateModel } from '@goodbooks/gbdata';
import {permissionservice} from './../../services/permission/permission.service'
import { Inject, Injectable } from '@angular/core';
import { GetAll, MoveFirst, MoveNext, MovePrev, MoveLast, GetData, SaveData, GetAllAndMoveFirst, ClearData, DeleteData, AddData } from './permission.action';


  export class PermissionStateModel extends GBDataStateModel<permissiondetail> {
  }

  @State<PermissionStateModel>({
    name: 'Permission',
    defaults: {
      data: [],
      areDataLoaded: false,
      currentData: {
        model: undefined,
        dirty: false,
        errors: '',
        status: ''
      },
      currentIndex: 0,
    },
  })
  @Injectable()
  export class PermissionState extends GBDataState<permissiondetail> {
    constructor(@Inject(permissionservice) dataService: permissionservice) {
      super(dataService, null);
    }

    @Action(GetAll)
    GetAll(context: StateContext<GBDataStateModel<permissiondetail>>, {scl}: GetAll) {
      return super.Super_GetAll(context, {scl: scl});
    }
    @Action(GetAllAndMoveFirst)
    GetAllAndMoveFirst(context: StateContext<GBDataStateModel<permissiondetail>>, {scl}: GetAll) {

    }
    @Action(MoveFirst)
    MoveFirst(context: StateContext<GBDataStateModel<permissiondetail>>) {
      return super.Super_MoveFirst(context);
    }
    @Action(MovePrev)
    MovePrev(context: StateContext<GBDataStateModel<permissiondetail>>) {
      return super.Super_MovePrev(context);
    }
    @Action(MoveNext)
    MoveNext(context: StateContext<GBDataStateModel<permissiondetail>>) {
      return super.Super_MoveNext(context);
    }
    @Action(MoveLast)
    MoveLast(context: StateContext<GBDataStateModel<permissiondetail>>) {
      return super.Super_MoveLast(context);
    }
    @Action(GetData)
    GetData(context: StateContext<GBDataStateModel<permissiondetail>>,
      { id }: GetData
    ) {
      return super.Super_GetData(context, id.toString());
    }
    @Action(AddData)
    AddData(context: StateContext<GBDataStateModel<permissiondetail>>,
      { payload }: { payload: any }
    ) {
      return super.Super_AddData(context, {payload: payload});
    }

    @Action(ClearData)
    ClearData(context: StateContext<GBDataStateModel<permissiondetail>>) {
      return super.Super_ClearData(context);
    }
  
    @Action(DeleteData)
    DeleteData(context: StateContext<GBDataStateModel<permissiondetail>>) {
      return super.Super_DeleteData(context);
    }
    @Action(SaveData)
    SaveData(context: StateContext<GBDataStateModel<permissiondetail>>,
      { payload, id }: { payload: any; id: any }
    ) {
      return super.Super_SaveData(context, {payload: payload, id: id});
    }
  }
    
