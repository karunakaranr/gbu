import { permissiondetail } from './../../models/permission/ipermission.model';
import {
  GBDataStateAction,
  ISelectListCriteria,
} from '@goodbooks/gbdata';

const permissionEntityName = 'permission';
export class GetData extends GBDataStateAction<permissiondetail> {
  static readonly type = '[' + permissionEntityName + '] getData';
  constructor(public id: number) {
    super();
  }
}
export class SaveData extends GBDataStateAction<permissiondetail> {
  static readonly type = '[' + permissionEntityName + '] saveData';
  constructor(
    public payload: permissiondetail
  ) {
    super();
  }
}
export class AddData extends GBDataStateAction<permissiondetail> {
  static readonly type = '[' + permissionEntityName + '] addData';
  constructor(
    public payload: permissiondetail
  ) {
    super();
  }
}
export class GetAll extends GBDataStateAction<permissiondetail> {
  static readonly type = '[' + permissionEntityName + '] getAll';
  constructor(public scl: ISelectListCriteria) {
    super();
  }
}
export class GetAllAndMoveFirst extends GBDataStateAction<permissiondetail> {
  static readonly type = '[' + permissionEntityName + '] getAllAndMoveFirst';
  constructor(public scl: ISelectListCriteria) {
    super();
  }
}
export class MoveFirst extends GBDataStateAction<permissiondetail> {
  static readonly type = '[' + permissionEntityName + '] MoveFirst';
  constructor() {
    super();
  }
}
export class MovePrev extends GBDataStateAction<permissiondetail> {
  static readonly type = '[' + permissionEntityName + '] MovePrev';
  constructor() {
    super();
  }
}
export class MoveNext extends GBDataStateAction<permissiondetail> {
  static readonly type = '[' + permissionEntityName + '] MoveNext';
  constructor() {
    super();
  }
}
export class MoveLast extends GBDataStateAction<permissiondetail> {
  static readonly type = '[' + permissionEntityName + '] MoveLast';
  constructor() {
    super();
  }
}
export class ClearData extends GBDataStateAction<permissiondetail> {
  static readonly type = '[' + permissionEntityName + '] ClearData';
  constructor() {
    super();
  }
}
export class DeleteData extends GBDataStateAction<permissiondetail> {
  static readonly type = '[' + permissionEntityName + '] DeleteData';
  constructor() {
    super();
  }
}