import { State, Action, StateContext, Selector } from '@ngxs/store';
import { Injectable } from '@angular/core';
import { GbApexChartDTO } from './apexchart.action';
import { GbApexChartdataDTO } from './apexchart.action';
export class ApexChartStateModel {
  apexchartdata;
}
export class ApexChartdataStateModel {
  apexchartdata1;
}
@State<ApexChartStateModel>({
  name: 'ApexChart',
  defaults: {
    apexchartdata: null,
  },
})
@State<ApexChartdataStateModel>({
  name: 'ApexChart1',
  defaults: {
    apexchartdata1: null,
  },
})
@Injectable()
export class ApexChartState {
  @Selector() static ApexChart(state: ApexChartStateModel) {
    return state.apexchartdata;
  }
  @Selector() static ApexChart1(state: ApexChartdataStateModel) {
    return state.apexchartdata1;
  }

  @Action(GbApexChartDTO)
  GbApexChartDTO(context: StateContext<ApexChartStateModel>, IApexChart: GbApexChartDTO) {
    const state = context.getState();
    context.setState({ ...state, apexchartdata: IApexChart.DApexChart });
  }
  @Action(GbApexChartdataDTO)
  GbApexChartdataDTO(context: StateContext<ApexChartdataStateModel>, IApexChart: GbApexChartdataDTO) {
    const state = context.getState();
    context.setState({ ...state, apexchartdata1: IApexChart.DApexChartdata });
  }
}