
const storeSliceName = 'ApexChart';

export class GbApexChartDTO {
  static readonly type = '[' + storeSliceName + '] GbApexChartDTO';
  constructor(public DApexChart) {
  }
}
export class GbApexChartdataDTO {
    static readonly type = '[' + storeSliceName + '] GbApexChartdataDTO';
    constructor(public DApexChartdata) {
    }
  }
