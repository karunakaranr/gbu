import { Component, Input, OnInit } from '@angular/core';
import { VoucherLightBox } from './../../services/LightBox/VoucherLightBox.service';
import  {Inject, LOCALE_ID } from '@angular/core';
import { DatePipe } from '@angular/common';
@Component({
  selector: 'app-VoucherLightBox',
  templateUrl: './VoucherLightBox.component.html',
  styleUrls: ['./VoucherLightBox.component.scss']
})

export class VoucherLightBoxComponent implements OnInit {
  @Input() RowData;
  LightBoxData:any;
  constructor(public service: VoucherLightBox, @Inject(LOCALE_ID) public locale: string ) { }
  ngOnInit(): void {
    var datePipe = new DatePipe("en-US");
    if(this.RowData!=undefined){
      this.LightBoxData=this.RowData
      console.log("MM Light Box Data:",this.LightBoxData)    
    }
    // this.service.VoucherLightBoxView(-1499949381).subscribe(menudetail =>{
    //     this.LightBoxData=menudetail;
    //     this.LightBoxData.VoucherVoucherDate = datePipe.transform(JSON.parse(this.LightBoxData.VoucherVoucherDate.replace("/Date(", "").replace(")/", "")), 'dd/MMM/yyyy');
    //     this.LightBoxData.VoucherReferenceDate = datePipe.transform(JSON.parse(this.LightBoxData.VoucherReferenceDate.replace("/Date(", "").replace(")/", "")), 'dd/MMM/yyyy'); 
    //     console.log("Voucher Light Box Data:",this.LightBoxData)
    // })
  }
}