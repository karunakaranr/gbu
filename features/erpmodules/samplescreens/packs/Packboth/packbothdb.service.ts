import { Injectable } from '@angular/core';

import { GBHttpService } from '@goodbooks/gbcommon';
import { GBBaseDBDataService } from '@goodbooks/gbdata';

import { IPackboth } from './IPackboth';
const urls = require('./../../URLS/urls.json');

@Injectable({
  providedIn: 'root'
})
export class PackbothDBService extends GBBaseDBDataService<IPackboth> {
  endPoint: string = urls.Pack;

  constructor(http: GBHttpService) {
    super(http);
   }
}
