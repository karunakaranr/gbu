import { HttpClient } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService, GbToasterService } from '@goodbooks/gbcommon';

import { AbstractDS, GBBaseDataService, GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { GBBasePageComponent, GBBaseDataPageComponentWN, GBFormGroup, GBDataPageService, GBDataFormGroupWN } from '@goodbooks/uicore';
import { Store } from '@ngxs/store';

import { IPackboth } from './IPackboth';
import { PackbothService } from './packboth.service';
import { PackbothDBService } from './packbothdb.service';
const urls = require('./../../URLS/urls.json');

@Component({
  selector: 'app-packboth',
  templateUrl: './packboth.component.html',
  styleUrls: ['./packboth.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'PackId' },
    { provide: 'url', useValue: urls.Pack },
    { provide: 'DataService', useClass: PackbothService },
    { provide: 'DBDataService', useClass: PackbothDBService },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class PackbothComponent extends GBBaseDataPageComponentWN<IPackboth> implements OnInit {
  title = 'Packboth';
  form: GBDataFormGroupWN<IPackboth> = new GBDataFormGroupWN<IPackboth>(this.gbps.gbhttp.http, 'SC002', {}, this.gbps.dataService);

  thisConstructor() { }
  // constructor(protected fb: FormBuilder, protected http: HttpClient, @Inject('DataService') public dataService: GBBaseDataServiceWN<IPackboth>) {
  //   super(fb, http);
  // }
  // async ngOnInit() {
  //   this.dataService.getAll().subscribe(res => {
  //       this.dataService.moveFirst().subscribe(r => {
  //         const pk: IPackboth = r;
  //         this.form.patchValue(pk);
  //       });
  //     });

  //   }
  async ngOnInit() {
    this.gbps.dataService.getAll().subscribe(res => {
      this.gbps.dataService.moveFirst().subscribe(r => {
        this.form.patchValue(r);
      });
    });
  }
}

export function getThisPageService(store: Store, dataService: GBBaseDataService<IPackboth>, dbDataService: GBBaseDBDataService<IPackboth>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router, toaster: GbToasterService): GBDataPageService<IPackboth> {
  return new GBDataPageService<IPackboth>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
