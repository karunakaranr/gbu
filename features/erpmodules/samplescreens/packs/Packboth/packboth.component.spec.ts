import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PackbothComponent } from './packboth.component';

describe('PackbothComponent', () => {
  let component: PackbothComponent;
  let fixture: ComponentFixture<PackbothComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PackbothComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PackbothComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
