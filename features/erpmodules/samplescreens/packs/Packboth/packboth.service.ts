import { Injectable } from '@angular/core';
import { GBBaseDataServiceWN } from '@goodbooks/gbdata';

import { IPackboth } from './IPackboth';
import { PackbothDBService } from './packbothdb.service';


@Injectable({
  providedIn: 'root'
})
export class PackbothService extends GBBaseDataServiceWN<IPackboth> {
  constructor(public dbDataService: PackbothDBService) {
    super(dbDataService, 'PackId');
  }
}
