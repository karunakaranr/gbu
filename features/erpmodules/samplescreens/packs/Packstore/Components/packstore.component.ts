import { Component, OnInit } from '@angular/core';
import { Ipack } from './../Model/packstore.modal';
import { PackstoreService } from './../service/packsore.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngxs/store';
import { Deletepack, SavePack } from './../Store/pack.actions';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';

@Component({
  selector: 'app-pack',
  templateUrl: './packstore.component.html',
  styleUrls: ['./packstore.component.scss'],
})
export class PackstoreComponent implements OnInit {
  packForm: FormGroup;
  packdata: Ipack = new Ipack();
  constructor(public service: PackstoreService,
    private fb: FormBuilder, public store: Store,
    private route: ActivatedRoute, private router: Router) {
    this.packForm = this.fb.group({
      PackCode: ['', Validators.required],
      PackName: ['', Validators.required],
      PackConversionType: ['', Validators.required],
      PackConversionFactor: ['', Validators.required]
    });
  }
  ngOnInit() {
    this.route.paramMap.subscribe((params: ParamMap) => {
      const id = parseInt(params.get('id'));
      this.getlist(id);
    });
  }
  public getlist(id) {
    this.service.getlist(id).subscribe(res=>{
      this.packForm=this.fb.group({
        PackCode:[res.PackCode],
        PackName:[res.PackName],
        PackConversionFactor:[res.PackConversionFactor],
        PackConversionType:[res.PackConversionType]
      });
    });
  }
  public savepack() {
    const savedata: Ipack = this.packForm.value as Ipack;
    this.store.dispatch(new SavePack(savedata));
  }
  public clear() {
    this.packForm.reset();
  }
  public deletepack() { 
    const packid = '-123455'
    this.store.dispatch(new Deletepack(packid));
  }
  public updatepack() {
    const savedata: Ipack = this.packForm.value as Ipack;
    this.store.dispatch(new SavePack(savedata))
  }
}
