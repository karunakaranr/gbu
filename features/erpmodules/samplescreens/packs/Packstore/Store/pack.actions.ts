import { Ipack } from './../Model/packstore.modal'

export class SavePack {
  static readonly type = '[pack] Add';

  constructor(public payload: Ipack) {
  }
}

export class Deletepack
{
  static readonly type='[Pack] Delete';
  constructor(public Packid: string){}
}

