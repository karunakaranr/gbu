import { State, Action, StateContext, Selector } from '@ngxs/store';
import {SavePack,Deletepack} from './pack.actions';
import { PackstoreService } from './../service/packsore.service';
import {tap} from 'rxjs/operators';
import { Ipack } from '../Model/packstore.modal';

export class PackStateModel {
    data: any;
    selectedid: Ipack;
}

@State<PackStateModel>({
    name: 'Pack',
    defaults: {
        data: [],
        selectedid: null
    }
})


export class Pack
{
constructor(public Packservice: PackService){
}

@Selector()
static getsavepackdata(state: PackStateModel)
{
  return state.data;
}

    @Action(SavePack)
    add({ getState, patchState }: StateContext<PackStateModel>, { payload }: SavePack) {
        return this.Packservice.savepack(payload).pipe(tap((result) => {
            const state = getState();
            patchState({
                data: [...state.data, result]
            });
        }));
    }

    @Action(Deletepack)
    delete({getState, setState}: StateContext<PackStateModel>, {Packid}: Deletepack) {
        return this.Packservice.deletepack(Packid).pipe(tap(() => {
            const state = getState();
            setState({
                ...state,
                data: Packid,
            });
        }));
    }

}
