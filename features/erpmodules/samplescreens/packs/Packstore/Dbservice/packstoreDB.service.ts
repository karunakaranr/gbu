import { Injectable } from '@angular/core';
import { Ipack } from './../Model/packstore.modal';
import { GBHttpService } from '@goodbooks/gbcommon';
import { URLS } from './../../../URLS/urls';

@Injectable({
  providedIn: 'root'
})
export class PackstoredatabaseService  {
 
  constructor(public http: GBHttpService) {
   }
public getlistDB(id){
  return this.http.httpget(URLS.packfill+id)
}
savapack(pack: Ipack){
  return this.http.httppost(URLS.savepack,pack);
}

deletepack(packid){
  return this.http.httpdelete(URLS.deletepack+packid);
}

}
