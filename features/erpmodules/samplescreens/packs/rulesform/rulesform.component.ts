import { Component, ViewEncapsulation } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

import { AbstractDS } from '@goodbooks/gbdata';
import {
  GBBaseFormGroup,
  GBBasePageComponent,
  GBFormGroup,
  GBPageService,
} from '@goodbooks/uicore';
import { Store } from '@ngxs/store';
import { RuleEngine, EnumComparers, EnumValueType } from '@unisoft/ng-bre';
import { IActionSetError, IActionSetLock, IActionSetRequired, IActionSetValue, IFormula } from '@unisoft/ng-bre';

// custom validator to check that two fields match
export function MustMatch(controlName: string, matchingControlName: string) {
  return (formGroup: FormGroup) => {
    const control = formGroup.controls[controlName];
    const matchingControl = formGroup.controls[matchingControlName];

    if (matchingControl.errors && !matchingControl.errors.mustMatch) {
      // return if another validator has already found an error on the matchingControl
      return;
    }

    // set error on matchingControl if validation fails
    if (control.value !== matchingControl.value) {
      matchingControl.setErrors({ mustMatch: true });
    } else {
      matchingControl.setErrors(null);
    }
  };
}

function controlValidator(min: number, max: number) {      //factory function
  return (control: AbstractControl):{[key: string]: boolean} | null => {
    if( control.value !==null && (isNaN(control.value) || control.value <min  || control.value> max)){
      return {'ageValidator': true}
    }
    return null;
  };
}

@Component({
  selector: 'app-rulesform',
  templateUrl: './rulesform.component.html',
  styleUrls: ['./rulesform.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [
      // GBPageService,
    // { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class RulesFormComponent extends GBBasePageComponent {
  isDataPage = true;
  hasNavigation = false;
  title = 'Rules Form';
  dataService: AbstractDS;
  form: GBBaseFormGroup;
  allstatus = ['Single', 'Married', 'Divorced', 'Widowed'];
  submitted = false;
  re: RuleEngine;

  constructor(gbps: GBPageService,public activeroute: ActivatedRoute,  public store : Store) {
    super(gbps,activeroute,store);
  }
  thisConstructor() {
    this.form = new GBBaseFormGroup(
      {
        title: new FormControl('', Validators.required),
        firstName: new FormControl('', Validators.required),
        lastName: new FormControl('', Validators.required),
        dob: new FormControl('', Validators.required),
        age: new FormControl('', [Validators.required, controlValidator(20, 70)]),
        ms: new FormControl('', Validators.required),
        snm: new FormControl(''),
        dom: new FormControl(''),
        dod: new FormControl(''),
        dodt: new FormControl(''),
        ccnum: new FormControl(''),

        email: new FormControl('', [Validators.required, Validators.email]),
        password: new FormControl('', [
          Validators.required,
          Validators.minLength(6),
        ]),
        confirmPassword: new FormControl('', Validators.required),
        acceptTerms: new FormControl(false, Validators.requiredTrue),
      },
      // { validators: MustMatch("password", "confirmPassword")}
    );

    this.setRules();
    this.re.setContext(this.form);
   // this.dataCheck();
  }

/* meaning

     checi  if  dob is changed
         if true --
              1. age field is locked --Age is made readonly    -- SetLock
              2. Calculate age and set result  in age field    -- SetValue
                       formula based uisng diff years fiunctions between dob and today
              3. Age not proper -- set error 
*/
  setRules() {
    this.re = new RuleEngine(
      [
        {
          Condition: {
            Fact: 'dob',
            Comparer: EnumComparers.Changed,
            Truthy: [
              {
                Name: 'SetLock',
                Field: 'age',
                Locked: true,
              } as IActionSetLock,
              {
                Name: 'SetValue',
                Field: 'age',
                ValueType: EnumValueType.Formula,
                Value: {
                  Field: "dob",
                  Operator: "diffYears",
                  ValueType: EnumValueType.Function,
                  Value: "Today" // 432000000 //
                } as IFormula,
              } as IActionSetValue,
              {
                Name: 'SetError',
                Field: 'age',
                ErrorName: "ageappro",
                ErrorMessage: "Age not appro!"
              } as IActionSetError,
            ],
          },
        },
      ],
      null
    );

    this.re.addRule({
      Condition: {
        Fact: 'ms',
        Comparer: EnumComparers.AreEqual,
        ValueType: EnumValueType.Constant,
        Value: 'Single',
        Truthy: [
          {
            Name: 'SetRequired',
            Field: 'snm',
            Required: false,
          } as IActionSetRequired,
          {
            Name: 'SetLock',
            Field: 'dom',
            Locked: true,
          } as IActionSetLock,
          {
            Name: 'SetLock',
            Field: 'dod',
            Locked: true,
          } as IActionSetLock,
          {
            Name: 'SetLock',
            Field: 'dodt',
            Locked: true,
          } as IActionSetLock,
        ],
      },
    });
    this.re.addRule({
      Condition: {
        Fact: 'ms',
        Comparer: EnumComparers.AreEqual,
        ValueType: EnumValueType.Constant,
        Value: 'Married',
        Truthy: [
          {
            Name: 'SetRequired',
            Field: 'snm',
            Required: true,
          } as IActionSetRequired,
          {
            Name: 'SetLock',
            Field: 'dom',
            Locked: false,
          } as IActionSetLock,
          {
            Name: 'SetLock',
            Field: 'dod',
            Locked: true,
          } as IActionSetLock,
          {
            Name: 'SetLock',
            Field: 'dodt',
            Locked: true,
          } as IActionSetLock,
        ],
      },
    });
    this.re.addRule({
      Condition: {
        Fact: 'ms',
        Comparer: EnumComparers.AreEqual,
        ValueType: EnumValueType.Constant,
        Value: 'Divorced',
        Truthy: [
          {
            Name: 'SetRequired',
            Field: 'snm',
            Required: true,
          } as IActionSetRequired,
          {
            Name: 'SetLock',
            Field: 'dom',
            Locked: false,
          } as IActionSetLock,
          {
            Name: 'SetLock',
            Field: 'dod',
            Locked: false,
          } as IActionSetLock,
          {
            Name: 'SetLock',
            Field: 'dodt',
            Locked: true,
          } as IActionSetLock,
        ],
      },
    });
    this.re.addRule({
      Condition: {
        Fact: 'ms',
        Comparer: EnumComparers.AreEqual,
        ValueType: EnumValueType.Constant,
        Value: 'Widowed',
        Truthy: [
          {
            Name: 'SetRequired',
            Field: 'snm',
            Required: true,
          } as IActionSetRequired,
          {
            Name: 'SetLock',
            Field: 'dom',
            Locked: false,
          } as IActionSetLock,
          {
            Name: 'SetLock',
            Field: 'dod',
            Locked: true,
          } as IActionSetLock,
          {
            Name: 'SetLock',
            Field: 'dodt',
            Locked: false,
          } as IActionSetLock,
        ],
      },
    });
    this.re.addRule({
      Condition: {
        Fact: 'age',
        Comparer: EnumComparers.IsGreaterThanEqual,
        ValueType: EnumValueType.Constant,
        Value: 21,
        Truthy: [
          {
            Name: 'SetRequired',
            Field: 'ccnum',
            Required: true,
          } as IActionSetRequired,
        ],
        Falsy: [
          {
            Name: 'SetRequired',
            Field: 'ccnum',
            Required: false,
          } as IActionSetRequired,
        ],
      },
    });
    this.re.addRule({
      Condition: {
        Fact: 'confirmPassword',
        Comparer: EnumComparers.AreNotEqual,
        ValueType: EnumValueType.Field,
        Value: "password",
        Truthy: [
          {
            Name: 'SetError',
            Field: 'confirmPassword',
            Set: true,
            ErrorName: "mustMatch",
            ErrorMessage: "Password should match!"
          } as IActionSetError,
        ],
        Falsy: [
          {
            Name: 'SetError',
            Field: 'confirmPassword',
            Set: false,
            ErrorName: "mustMatch",
            ErrorMessage: "Password should match!"
          } as IActionSetError,
        ],
      },
    });
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.form.controls;
  }
  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.form.invalid) {
      return;
    }

    // display form values on success
    alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.form.value, null, 4));
    console.log(JSON.stringify(this.form.value, null, 4));
  }
  onReset() {
    this.submitted = false;
    this.form.reset();
  }

  dataCheck() {
    const data= {
      "title": "Mr",
      "firstName": "dfg",
      "lastName": "sdf",
      "dob": "1999-12-12",
      // "age": 20,
      "ms": "Single",
      "dom": "1999-12-12",
      "dod": "1999-12-12",
      "dodt": "1999-12-12",
      "snm": "",
      "ccnum": "",
      "email": "sfdfs@sdfs.cc",
      "password": "test1234",
      "confirmPassword": "test12345",
      "acceptTerms": true
    };
    this.re.run(data).subscribe((v) => {
      console.log (v);
    });
  }
}
