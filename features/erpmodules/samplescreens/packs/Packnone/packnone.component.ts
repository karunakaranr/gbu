import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN } from '@goodbooks/uicore';

import { IPacknone } from './IPacknone';
import { getThisDataServiceWN, getThisDBDataService, getThisDataService, getThisPageService } from './packnone.factory';
import { Store } from '@ngxs/store';
import { GBHttpService } from '@goodbooks/gbcommon';
const urls = require('./../../URLS/urls.json');

@Component({
  selector: 'app-packnone',
  templateUrl: './packnone.component.html',
  styleUrls: ['./packnone.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'PackId'},
    { provide: 'url', useValue: urls.Pack},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url']},
    { provide: 'DataService', useFactory: getThisDataServiceWN, deps: ['DBDataService', 'IdField']},
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class PacknoneComponent extends GBBaseDataPageComponentWN<IPacknone> implements OnInit {
  title = 'Packnone';
  form: GBDataFormGroupWN<IPacknone> = new GBDataFormGroupWN<IPacknone>(this.gbps.gbhttp.http, 'SC002', {}, this.gbps.dataService);
  thisConstructor() {  
  }
  ngOnInit(): void {
   
  }
}


