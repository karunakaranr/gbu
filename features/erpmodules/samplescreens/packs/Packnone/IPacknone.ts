export interface IPacknone {
  PackId: number;
  PackCode: string;
  PackName: string;
  PackConversionType: string;
  PackConversionFactor: number;
  PackStatus: number;
  PackVersion: number;
  sortorder: number;
  sourceType: string;
}




