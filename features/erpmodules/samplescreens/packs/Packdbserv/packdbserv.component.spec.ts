import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PackdbservComponent } from './packdbserv.component';

describe('PackdbservComponent', () => {
  let component: PackdbservComponent;
  let fixture: ComponentFixture<PackdbservComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PackdbservComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PackdbservComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
