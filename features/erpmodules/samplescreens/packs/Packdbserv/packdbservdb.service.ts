import { Injectable } from '@angular/core';

import { GBHttpService } from '@goodbooks/gbcommon';
import { GBBaseDBDataService } from '@goodbooks/gbdata';

import { IPackdbserv } from './IPackdbserv';
const urls = require('./../../URLS/urls.json');

@Injectable({
  providedIn: 'root'
})
export class PackdbservDBService extends GBBaseDBDataService<IPackdbserv> {
  endPoint: string = urls.Pack;

  constructor(http: GBHttpService) {
    super(http);
   }
}
