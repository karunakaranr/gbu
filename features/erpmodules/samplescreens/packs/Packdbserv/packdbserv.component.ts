import { HttpClient } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService,GbToasterService } from '@goodbooks/gbcommon';

import { AbstractDS, GBBaseDataService, GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import {  GBBaseDataPageComponentWN, GBDataFormGroup, GBDataFormGroupWN, GBDataPageService, GBFormGroup } from '@goodbooks/uicore';
import { Store } from '@ngxs/store';

import { IPackdbserv } from './IPackdbserv';
import { PackdbservDBService } from './packdbservdb.service';
const urls = require('./../../URLS/urls.json');

@Component({
  selector: 'app-packdbserv',
  templateUrl: './packdbserv.component.html',
  styleUrls: ['./packdbserv.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'PackId'},
    { provide: 'url', useValue: urls.Pack},
    { provide: 'DBDataService', useClass: PackdbservDBService},
    { provide: 'DataService', useFactory: getThisDataServiceWN, deps: ['DBDataService', 'IdField']},
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class PackdbservComponent extends GBBaseDataPageComponentWN<IPackdbserv> implements OnInit {
  title = 'Packdbserv';
  form: GBDataFormGroupWN<IPackdbserv> = new GBDataFormGroupWN<IPackdbserv>(this.gbps.gbhttp.http, 'SC002', {}, this.gbps.dataService);
  thisConstructor() {}

  // constructor(protected fb: FormBuilder, protected http: HttpClient, @Inject('DataService') public dataService: GBBaseDataServiceWN<IPackdbserv>) {
  //   super(fb, http);
  // }

  async ngOnInit() {
    this.gbps.dataService.getAll().subscribe(res => {
      this.gbps.dataService.moveFirst().subscribe(r => {
        this.form.patchValue(r);
      });
    });
  }
}

export function getThisDataServiceWN(dbds: PackdbservDBService, idField: string): GBBaseDataServiceWN<IPackdbserv> {
  return new GBBaseDataServiceWN<IPackdbserv>(dbds, idField);
}
export function getThisDataService(dbds: PackdbservDBService, idField: string): GBBaseDataService<IPackdbserv> {
  return new GBBaseDataServiceWN<IPackdbserv>(dbds, idField);
}
export function getThisDBDataService(http: GBHttpService, url: string): PackdbservDBService {
  const dbds: PackdbservDBService = new PackdbservDBService(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IPackdbserv>, dbDataService: GBBaseDBDataService<IPackdbserv>, fb: FormBuilder, gbhttp:GBHttpService, activeroute:ActivatedRoute, router:Router,toaster: GbToasterService): GBDataPageService<IPackdbserv> {
  return new GBDataPageService<IPackdbserv>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
