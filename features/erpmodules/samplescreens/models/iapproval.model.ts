export interface approval{
  DocumentNumber:string,
  BizTransactionTypeId:string,
 ObjectId:string,
 ProcessInstanceId:string,
 EntityCode:string,
 DisplayValue:string,
 FromUserCode:string,
 InchargeMail:string,
 Version:string,
 PartyBranchId:string,
 Remarks:string
  }

  export interface approvallist{
  map(arg0: (item: any) => any): any;
  filter: any;
  EntityId:string,
  TASKNO: string,
  TASKNAME: string,
  TaskHrs: string,
  TaskDate:string,
  Approver: string,
  TaskFromCode: string,
  TaskFromName: string,
  TaskFromDate: string,
  DocumentNumber: string,
  BizTransactionTypeId:string,
  ObjectId:string,
  ProcessInstanceId: string,
  EntityCode: string,
  DisplayValue: string,
  FromUserCode: string,
  InchargeMail: string,
  Version:string,
  PartyBranchId: string,
  Remarks: string,
  
  }

  /*
export interface approvallist{
  EntityId:string,
  TASKNO: string,
  TASKNAME: string,
  TaskHrs: string,
  TaskDate:string,
  Approver: string,
  TaskFromCode: string,
  TaskFromName: string,
  TaskFromDate: string,
  doc: string,
  bizid:string,
  objid:string,
  proinsid: string,
  ent: string,
  disval: string,
  usercode: string,
  incmail: string,
  version:string,
  getpartybranchidd: string,
  Approverclass: string,
  //isselected :boolean
  }
  */