export class Chartdata {
   name: string;
   value: any;
   // constructor(public name: string,public value: number){}
}

export class Chartsetting {
   show: boolean;
   chartType: string;
   chartTheme: string;
   view: any[];
   colorScheme: any[];
   schemeType: string;
   animations: boolean;
   showXAxis: boolean;
   showYAxis: boolean;
   showGridLines: boolean;
   roundDomains: boolean;
   rotateXAxisTicks: boolean;
   gradient: boolean;
   showLegend: boolean;
   legendTitle: string;
   legendPosition: string;
   showXAxisLabel: boolean;
   showYAxisLabel: boolean;
   xAxisLabel: string;
   yAxisLabel: string;
   autoScale: boolean;
   timeline: boolean;
   lineInterpolation: string;
   domain: any[];
   legend: boolean;
   showLabels: boolean;
   xAxis: boolean;
   yAxis: boolean;
   isDoughnut: boolean;
}

export enum ChartsType {
   areachart,
   piechart,
   linechart,
   gaugechart

}
export enum Chartsthemes {
   dark,
   light
}
