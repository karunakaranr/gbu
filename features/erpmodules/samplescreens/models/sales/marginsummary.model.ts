
export interface MarginSummary {
    FromDate: Date;
    ToDate: Date;
    Type: string;
    Id: number;
    Code: string;
    Name: string;
    Quantity: number;
    Value: number;
    Cost: number;
    Realisation: number;
    Margin: number;
    MarginPercentCost: number;
    MarginPercentSale: number;
    AvgRate: number;
    AvgCost: number;
    ItemBasicValue: number;
    ItemMinus: number;
    ItemPlus: number;
    ItemOtherMinus: number;
    ItemOtherPlus: number;
    ItemNetValue: number;
    ItemNetWeight: number;
    CurrencyId: number;
    CurrencyCode?: any;
    CurrencyName?: any;
    ValueFc: number;
    BasicValue: number;
    UomCode?: any;
    FinalQuantity: number;
    FinalNetValue: number;
    FinalBasicValue: number;
    FinalCost: number;
    FinalRealisation: number;
    FinalAverage: number;
    FinalAverageBasic: number;
    FinalMargin: number;
    FinalMarginPercentCost: number;
    FinalMarginPercentSale: number;
    NumberOfRecords: number;
    Label: string;
}

export interface AttributesCriteriaList {
    FieldName: string;
    OperationType: number;
    FieldValue: any;
    InArray?: any;
    JoinType: number;
    CriteriaAttributeName: string;
    CriteriaAttributeValue: string;
    IsCompulsory: number;
    IsHeader: number;
    CriteriaAttributeType: number;
    CriteriaAttributeId: number;
    FilterType: number;
    CriteriaFieldDisplayValue: string;
}

export interface SectionCriteriaList {
    SectionId: number;
    AttributesCriteriaList: AttributesCriteriaList[];
    OperationType: number;
}

export interface ReportCriteria {
    Id: number;
    SectionCriteriaList: SectionCriteriaList[];
}

export class ReportAttributeCriteria {
    FieldName: string;
    OperationType: number;
    FieldValue: any;
    InArray?: any;
    JoinType: number;
    CriteriaAttributeName: string;
    CriteriaAttributeValue: string;
    IsCompulsory: number;
    IsHeader: number;
    CriteriaAttributeType: number;
    CriteriaAttributeId: number;
    FilterType: number;
    CriteriaFieldDisplayValue: string;
}

export interface ReportPreferenceSetting {
    UserSettingsId: number;
    UserSettingsUserId: number;
    UserSettingsRoleId: number;
    UserSettingsDateFormat: string;
    UserSettingsCurrencyFormat: string;
    UserSettingsIsZeroSupressed: boolean;
    UserSettingsIsTruncate: boolean;
    UserSettingsQuantityFormat: string;
}

export interface ReportHeader {
    CompanyId: number;
    CompanyCode: string;
    CompanyName: string;
    BranchId: number;
    BranchCode: string;
    BranchName: string;
    DivisionId: number;
    DivisionCode: string;
    DivisionName: string;
    OuId: number;
    OuCode: string;
    OuName: string;
    OUDisplayName: string;
    AddressDetail?: any;
    CompanyAddressDetail?: any;
    ReportTitle: string;
    ReportTitleOptional: string;
    ReportPath: string;
    ReportFolder: string;
    CompanyLogo1: string;
    CompanyLogo2: string;
    IsPrintLogo: boolean;
}

export interface ReportFooter {
    IsoReferenceNumber?: any;
    ReportCode: string;
    ReportFormatCode: string;
    DataSourceFileName: string;
    ReportFile: string;
    ReportFormatVersion: number;
    ReportFormatModifiedOn: Date;
}

export interface BodyData {
    CompanyId: number;
    CompanyCode: string;
    CompanyName: string;
    CINNumber: string;
    BranchId: number;
    BranchCode: string;
    BranchName: string;
    DivisionId: number;
    DivisionCode: string;
    DivisionName: string;
    OuId: number;
    OuCode: string;
    OuName: string;
    AddressDetail?: any;
    CompanyAddressDetail?: any;
    ReportDetail: MarginSummary[];
    ReportCriteria: ReportCriteria;
    ReportAttributeCriteria: ReportAttributeCriteria[];
    ReportPreferenceSetting: ReportPreferenceSetting;
    ReportTitle: string;
    ReportTitleOptional: string;
    ReportPath: string;
    ReportFolder: string;
    IsoReferenceNumber?: any;
    IsPrintLogo: boolean;
    IsTimeStamp: boolean;
    ReportPrintMode: number;
    ReportChartType: number;
    CompanyLogo1: string;
    CompanyLogo2: string;
    OULogo1: string;
    OULogo2: string;
    OUDisplayName: string;
    OUShortName: string;
    ReportHeader: ReportHeader;
    ReportFooter: ReportFooter;
    ReportViewId: number;
    DatabaseName: string;
    ReportViewDynamicFooter?: any;
    TimeFormat: string;
    QuantityFormat: string;
    ServiceOffSet: number;
}

export interface RootObject {
    Id: number;
    Body: Body;
}

