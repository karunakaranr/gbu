export interface ReportCriteriaObject {
    SectionId: number;
    AttributesCriteriaList: AttributesCriteriaList[];
    OperationType: number;
}
    export class AttributesCriteriaList {
        FieldName: string;
        OperationType: number;
        FieldValue: any;
        JoinType: number;
        CriteriaAttributeName: string;
        CriteriaAttributeValue: string;
        IsHeader: number;
        IsCompulsory: number;
        CriteriaAttributeId: number;
        CriteriaAttributeType: number;
        FilterType: number;
    }

   


