

export interface permissiondetail {
  TIMESLIPID: string,
  BIZTRANSACTIONTYPEID: number,
  OUID: number,
  PERIODID: number,
  TIMESLIPNUMBER: number,
  TIMESLIPDATE: string,
  EMPLOYEEID: number,
  ATTENDANCEDATE: string,
  SHIFTID: number,
  TYPE: any,
  FROMTIME: number,
  TOTIME: number,
  DURATION: number,
  REASON: string,
  APPROVEDBYID: string,
  VERSION: number,
  STATUS: number,
  CREATEDBYID: number,
  CREATEDON: number,
  MODIFIEDBYID: number,
  MODIFIEDON: number,
  TIMESLIPSTARTTIME: number,
  TIMESLIPENDTIME: number,
  ONDUTYLOCATION: string,
  TIMESLIPSYNCID: number
}



export interface permissionlist {
  permissionNumber: string,
  permissionId: string,
  AttendanceDate: string,
  EmployeeCode: string,
  EmployeeName: string,
  permissionType: number,

}


export interface GridSetting {
  headerName: string;
  field: string;
  sortable: boolean;
  filter: boolean;
  checkboxSelection: boolean;
  resizable: boolean;
  editable: boolean;
  pinned: string;
  cellEditor: string;
  cellEditorParams: string;
}
export interface GridProperties {
  paginationAutoPageSize: boolean;
  paginationPageSize: number;
  pagination: boolean;
  undoRedoCellEditing: boolean;
  undoRedoCellEditingLimit: number;
  enableCellChangeFlash: boolean;
  enterMovesDown: boolean;
  enterMovesDownAfterEdit: boolean;
  rowSelection: string;
  enableRangeSelection: boolean;
  paginateChildRows: boolean;
}

