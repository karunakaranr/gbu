export interface IFilterDetail {
  ReportUri: string;
  Method: string;
  ReportTitle: string;
  CriteriaDTO: ICriteriaDTODetail[];
  MenuId: number;
  WebserviceId: number;
  PeriodFilter: number;
  RecordsPerPage: number;
}

export interface ICriteriaDTODetail {
  SectionCriteriaList: ISectionCriteriaDetail[];
}

export interface ISectionCriteriaDetail {
  SectionId: number;
  AttributesCriteriaList: IAttributeCriteriaDetail[];
  OperationType: number;
}

export interface IAttributeCriteriaDetail {
  FieldName: string;
  OperationType: number;
  FieldValue: number | string;
  JoinType: number;
  CriteriaAttributeName: string;
  CriteriaAttributeValue: string;
  IsHeader: number;
  IsCompulsory: number;
  CriteriaAttributeId: number | string;
  CriteriaAttributeType: number;
  InArray: unknown;
}
