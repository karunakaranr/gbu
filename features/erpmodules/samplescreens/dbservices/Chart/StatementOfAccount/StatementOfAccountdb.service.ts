import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { GBHttpService } from '@goodbooks/gbcommon';

@Injectable({
    providedIn: 'root',
})
export class StatementOfAccountDbService {
    rowcriteria;
    constructor(public http: GBHttpService) { }

    public picklist(): Observable<any> {
        const url = '/fws/Menu.svc/ReportMenuDetailsForMenu/?UserId=-1499998941&MenuId=-1399996346';
        console.log("url", url);
        return this.http.httpget(url);
    }


    public reportData(pickobj) {
        let criteria;
        let test = {

            "SectionCriteriaList": [
                {
                    "SectionId": -1497618628,
                    "AttributesCriteriaList": [
                        {
                            "FieldName": "OUId",
                            "OperationType": 5,
                            "FieldValue": "-1500000000",
                            "JoinType": 2,
                            "CriteriaAttributeName": "OU",
                            "CriteriaAttributeValue": "ABC ORGANIZATION",
                            "IsHeader": 1,
                            "IsCompulsory": 1,
                            "CriteriaAttributeId": -1899999999,
                            "CriteriaAttributeType": 0,
                            "FilterType": 1
                        },
                        {
                            "FieldName": "VoucherDetailAccountId",
                            "OperationType": 5,
                            "FieldValue": "-1499998875",
                            "JoinType": 2,
                            "CriteriaAttributeName": "Voucher Account",
                            "CriteriaAttributeValue": "CashOnHand",
                            "IsHeader": 0,
                            "IsCompulsory": 1,
                            "CriteriaAttributeId": -1399999947,
                            "CriteriaAttributeType": 0,
                            "FilterType": 1
                        },
                        {
                            "FieldName": "Type",
                            "OperationType": 5,
                            "FieldValue": "0",
                            "JoinType": 2,
                            "CriteriaAttributeName": "Type",
                            "CriteriaAttributeValue": "Normal",
                            "IsHeader": 1,
                            "IsCompulsory": 1,
                            "CriteriaAttributeId": -1399999995,
                            "CriteriaAttributeType": 3,
                            "FilterType": 1
                        },
                        {
                            "FieldName": "Provision",
                            "OperationType": 5,
                            "FieldValue": "1",
                            "JoinType": 2,
                            "CriteriaAttributeName": "Provision",
                            "CriteriaAttributeValue": "No",
                            "IsHeader": 1,
                            "IsCompulsory": 1,
                            "CriteriaAttributeId": -1399999663,
                            "CriteriaAttributeType": 3,
                            "FilterType": 1
                        },
                        {
                            "FieldName": "BatchAllocation",
                            "OperationType": 5,
                            "FieldValue": "1",
                            "JoinType": 2,
                            "CriteriaAttributeName": "BatchAllocation",
                            "CriteriaAttributeValue": "No",
                            "IsHeader": 1,
                            "IsCompulsory": 1,
                            "CriteriaAttributeId": -1399999127,
                            "CriteriaAttributeType": 3,
                            "FilterType": 1
                        },
                        {
                            "FieldName": "PeriodFromDate",
                            "OperationType": 10,
                            "FieldValue": "1617235200",
                            "JoinType": 0,
                            "CriteriaAttributeName": "From Date",
                            "CriteriaAttributeValue": "01/Apr/2021",
                            "IsHeader": 0,
                            "IsCompulsory": 0,
                            "CriteriaAttributeId": -2147483643,
                            "CriteriaAttributeType": 4,
                            "FilterType": 1
                        },
                        {
                            "FieldName": "PeriodToDate",
                            "OperationType": 11,
                            "FieldValue": "1680220800",
                            "JoinType": 0,
                            "CriteriaAttributeName": "To Date",
                            "CriteriaAttributeValue": "31/Mar/2023",
                            "IsHeader": 0,
                            "IsCompulsory": 0,
                            "CriteriaAttributeId": -2147483642,
                            "CriteriaAttributeType": 4,
                            "FilterType": 1
                        }
                    ],
                    "OperationType": 0
                }
            ],
            
        }
        let xx = pickobj[0].WebServiceSettingArray[0].WebServiceSettingDetailArray
        console.log("xxxx",xx)
        
        criteria = {
            "ReportUri": pickobj[0].WebServiceUriTemplate,
            "Method": pickobj[0].MethodType,
            "ReportTitle": "",
            "CriteriaDTO": test,
            "MenuId": pickobj[0].MenuId,
            "WebserviceId": pickobj[0].WebServiceId,
            "PeriodFilter": 0,
            "RecordsPerPage": 0
        }
        this.rowcriteria = criteria
        let url;
        console.log("pickobj", criteria)
        let fullurl = 'http://169.56.148.10:82/gb4/cos/Common.svc/Report';
        let str = fullurl.split('/gb4');
        let urlstr = str[str.length - 1];
        url = urlstr;
        console.log("api", url);
        return this.http.httppost(url, criteria);
    }
}