import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { GBHttpService } from '@goodbooks/gbcommon';
@Injectable({
    providedIn: 'root',
})
export class VoucherLightBoxDbService {
    rowcriteria ;
    constructor(public http: GBHttpService) { }

    public picklist(data): Observable<any> {
        const url = '/as/Voucher.svc/?VoucherId='+data;
        return this.http.httpget(url);
    }
}