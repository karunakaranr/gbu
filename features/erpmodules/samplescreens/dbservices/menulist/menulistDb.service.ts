import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { GBHttpService } from '@goodbooks/gbcommon';
@Injectable({
    providedIn: 'root',
})
export class MenuListDBService {
    rowcriteria ;
    constructor(public http: GBHttpService) { }

    public RolesAndRights(menuid:number,BizTransactionClassId:number): Observable<any> {
        const url = '/ads/BizTransactionType.svc/FormSettings/?MenuId='+menuid+'&BizTransactionTypeId='+BizTransactionClassId
        return this.http.httpget(url);
    }
}