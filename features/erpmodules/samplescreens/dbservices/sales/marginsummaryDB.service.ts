import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { URLS } from '../../URLS/urls';
import { MarginSummary } from '../../models/sales/marginsummary.model';
import { GBHttpService } from '@goodbooks/gbcommon';
@Injectable({
	providedIn: 'root'
})
export class MarginSummaryDBService {
	constructor(public HTTP: GBHttpService) { }
	marginsummarydb(): Observable<MarginSummary[]> {
		const criteria =
	{
			"SectionCriteriaList": [
				{
					"SectionId": -1499490868,
					"AttributesCriteriaList": [
						{
							"FieldName": "OUId",
							"OperationType": 1,
							"FieldValue": "-1500000000",
							"JoinType": 2,
							"CriteriaAttributeName": "OU",
							"CriteriaAttributeValue": "TESTING COMPANY",
							"IsHeader": 1,
							"IsCompulsory": 0,
							"CriteriaAttributeId": -1899999999,
							"CriteriaAttributeType": 0,
							"FilterType": 1
						},
						{
							"FieldName": "BizTransactionClassId",
							"OperationType": 1,
							"FieldValue": "-1799999904",
							"JoinType": 2,
							"CriteriaAttributeName": "Class",
							"CriteriaAttributeValue": "",
							"IsHeader": 1,
							"IsCompulsory": 1,
							"CriteriaAttributeId": -1899999970,
							"CriteriaAttributeType": 0,
							"FilterType": 1
						},
						{
							"FieldName": "PeriodFromDate",
							"OperationType": 10,
							"FieldValue": "-224052427",
							"JoinType": 0,
							"CriteriaAttributeName": "From",
							"CriteriaAttributeValue": "01/Jan/1899",
							"IsHeader": 0,
							"IsCompulsory": 0,
							"CriteriaAttributeId": -2147483643,
							"CriteriaAttributeType": 4,
							"FilterType": 1
						},
						{
							"FieldName": "PeriodToDate",
							"OperationType": 11,
							"FieldValue": "2534022144",
							"JoinType": 0,
							"CriteriaAttributeName": "To",
							"CriteriaAttributeValue": "31/Dec/9999",
							"IsHeader": 0,
							"IsCompulsory": 0,
							"CriteriaAttributeId": -2147483642,
							"CriteriaAttributeType": 4,
							"FilterType": 1
						},
						{
							"FieldName": "BIZTransactionClassId",
							"OperationType": 1,
							"FieldValue": -1799999904,
							"JoinType": 0,
							"CriteriaAttributeName": null,
							"CriteriaAttributeValue": "TESTING COMPANY",
							"IsHeader": 1,
							"IsCompulsory": 1,
							"CriteriaAttributeId": -1899999970,
							"CriteriaAttributeType": 0
						}
					],
					"OperationType": 0
				}
			]
		}

		return this.HTTP.httppost(URLS.MarginSummary, criteria);
	}

}
