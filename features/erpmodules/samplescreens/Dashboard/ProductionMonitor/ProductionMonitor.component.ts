import { Component, OnInit } from '@angular/core';
const jsonvalue = require('./ProductionMonitor.json')
@Component({
  selector: 'app-ProductionMonitor',
  templateUrl: './ProductionMonitor.component.html',
  styleUrls: ['./ProductionMonitor.component.scss']
})

export class ProductionMonitorComponent implements OnInit {
  title = 'ProductionMonitor'
  rowdata=[];
  tabledata=[];
  Heading;
  HeadingKeys;
  dummyHeaading;
  constructor() { }
  ngOnInit(): void {
    this.rowdata=jsonvalue;
    this.Heading=this.rowdata['DetailArray'][0]
    this.dummyHeaading= JSON.parse(JSON.stringify(this.Heading));
    this.HeadingKeys=Object.keys(this.Heading)
    for(let data of this.HeadingKeys){
      if(this.Heading[data]>=1000){
        this.dummyHeaading[data]= (this.Heading[data] / 1000).toFixed(1) + 'K';
      }
    }
    console.log("Heading:",this.Heading)
    console.log("Dummy Heading:",this.dummyHeaading)
  }
  public viewtable(data){
    if(data=="WorkOrderDetailArray"){
      document.getElementById("worktable").style.display = "block";
      document.getElementById("itemtable").style.display = "none";
      document.getElementById("processtable").style.display = "none";
    }
    if(data=="ItemDetailArray"){
      document.getElementById("worktable").style.display = "none";
      document.getElementById("itemtable").style.display = "block";
      document.getElementById("processtable").style.display = "none";
    }
    if(data=="ProcessDetailAray"){
      document.getElementById("worktable").style.display = "none";
      document.getElementById("itemtable").style.display = "none";
      document.getElementById("processtable").style.display = "block";
    }
    this.tabledata=this.rowdata[data];
  }
}