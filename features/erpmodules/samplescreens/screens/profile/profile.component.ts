import { Component} from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { AbstractDS } from '@goodbooks/gbdata';
import { GBBasePageComponent, GBFormGroup, GBPageService } from '@goodbooks/uicore';
import { Store } from '@ngxs/store';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent extends GBBasePageComponent {
  isDataPage = true;
  hasNavigation = false;
  title = 'Profile';
  dataService: AbstractDS;
  form: GBFormGroup = new GBFormGroup(this.gbps.gbhttp.http, 'SC001', {});

  constructor(gbps: GBPageService,public activeroute: ActivatedRoute,  public store : Store) {
    super(gbps,activeroute,store)
  }
  thisConstructor() {}

}
