import { MarkerLabel } from '@agm/core/services/google-maps-types';
import { Component, ElementRef, EventEmitter, Input, NgZone, OnChanges, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';

declare var google: any; // Declare the global 'google' variable
const mapjson = require('./map.json');

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnChanges  {
  @Input() ParentData;
  @ViewChild('mapContainer') mapContainer: ElementRef;
  @Output() MarkerAddress: EventEmitter<any> = new EventEmitter<string>();
  @Output() MarkerGeoCode: EventEmitter<any> = new EventEmitter<string>();

  map: google.maps.Map;
  lat: number;
  lng: number;
  markers: google.maps.Marker[] = []; // Array to store markers
  marker: any;

  constructor(private ngZone: NgZone) { }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.ParentData && changes.ParentData.currentValue) {
      console.log("PARENRTDTAT",this.ParentData)
      this.loadGoogleMaps(() => {
        this.initMap();
      });
    }
  }

  ngAfterViewInit() {
    this.loadGoogleMaps(() => {
      this.initMap();
    });
  }

  loadGoogleMaps(callback: () => void) {
    if (typeof google === 'undefined') {
      const script = document.createElement('script');
      script.src = `https://maps.googleapis.com/maps/api/js?key=AIzaSyA1jUd_lZjCY578l5sYUcigk4HKhXonR28&libraries=places`;
      script.async = true;
      script.defer = true;
      script.onload = callback;
      document.body.appendChild(script);
    } else {
      callback();
    }
  }

  initMap() {
    if (this.mapContainer) {
      // GeoCode provided
      console.log("Parent Data:", this.ParentData);
      if (this.ParentData && this.ParentData.length > 0 && this.ParentData[0].GeoCode) {
        let maplocations = this.ParentData[0].GeoCode.split(',');
        console.log("Map Locations:", maplocations);
        const mapOptions: google.maps.MapOptions = {
          center: { lat: parseFloat(maplocations[0]), lng: parseFloat(maplocations[1]) },
          zoom: 7,
          minZoom: 5,
          maxZoom: 17,
        };
  
        this.map = new google.maps.Map(this.mapContainer.nativeElement, mapOptions);
  
        // Add markers and road routes using the provided coordinates
        this.displayMarkersAndRoadRoutes(this.ParentData);
  
        // Set restricted bounds to limit scrolling
        const allowedBounds = new google.maps.LatLngBounds(
          new google.maps.LatLng(-85, -180), // South West
          new google.maps.LatLng(85, 180) // North East
        );
  
        google.maps.event.addListener(this.map, 'drag', () => {
          if (allowedBounds.contains(this.map.getCenter())) return;
  
          // Move map back within bounds
          const c = this.map.getCenter(),
            x = c.lng(),
            y = c.lat(),
            maxX = allowedBounds.getNorthEast().lng(),
            maxY = allowedBounds.getNorthEast().lat(),
            minX = allowedBounds.getSouthWest().lng(),
            minY = allowedBounds.getSouthWest().lat();
  
          let newX = x,
            newY = y;
  
          if (x < minX) {
            newX = minX;
          } else if (x > maxX) {
            newX = maxX;
          }
  
          if (y < minY) {
            newY = minY;
          } else if (y > maxY) {
            newY = maxY;
          }
  
          this.map.setCenter(new google.maps.LatLng(newY, newX));
        });
  
        // Add single click event listener to the map
        google.maps.event.addListener(this.map, 'click', (event) => {
          this.addMarker(event.latLng);
        });
  
        // Add double click event listener to the map
        google.maps.event.addListener(this.map, 'dblclick', (event) => {
          this.addMarker(event.latLng, false);
        });
  
      } else if(this.ParentData == "" || this.ParentData[0].GeoCode == "") {
        // GeoCode not provided, display map with content
        const defaultMapOptions: google.maps.MapOptions = {
          zoom: 3,
        };
  
        this.map = new google.maps.Map(this.mapContainer.nativeElement, defaultMapOptions);
        console.log("No GeoCodes in this data.")
        // Display content on the map container
        this.mapContainer.nativeElement.innerHTML = "<div style='text-align:center;'>No GeoCodes provided.</div>";
      }
    }
  }
  
  // Function to add a marker to the map
  addMarker(location: google.maps.LatLng, draggable: boolean = true) {
    // If there's an existing marker, remove it
    if (this.marker) {
      this.marker.setMap(null);
    }
    // Create a marker at the specified location
    this.marker = new google.maps.Marker({
      position: location,
      map: this.map,
      draggable: draggable
    });
  
    // Add dragend event listener to the marker
    google.maps.event.addListener(this.marker, 'dragend', () => {
      console.log('Marker dragged to:', this.marker.getPosition().lat(), this.marker.getPosition().lng());
      // Retrieve geocode and geolocation of the marker
      this.getGeoAddress(this.marker.getPosition());
      // Perform any other actions you want when the marker is dragged
    });
  
    // Retrieve geocode and geolocation of the marker
    this.getGeoAddress(location);
  }
  
  // Function to retrieve geocode of the marker
  getGeoAddress(location: google.maps.LatLng) {
    const geocoder = new google.maps.Geocoder();
    geocoder.geocode({ 'location': location }, (results, status) => {
      if (status === google.maps.GeocoderStatus.OK) {
        if (results[0]) {
          console.log('Geolocation of the marker:', location.lat(), location.lng());
          console.log('Address of the marker:', results[0].formatted_address);
          this.MarkerAddress.emit(results[0].formatted_address);
          this.MarkerGeoCode.emit({ lat: location.lat(), lng: location.lng() });
        } else {
          console.log('No geocode found for this location');
        }
      } else {
        console.log('Geocoder failed due to:', status);
      }
    });
  }
  
  // Function to retrieve geolocation of the marker

  

  displayMarkersAndRoadRoutes(coordinates) {
    const bounds = new google.maps.LatLngBounds();
    const directionsService = new google.maps.DirectionsService();

    // Create an array to hold all coordinates with type 'In'
    const filteredCoordinates = coordinates.filter(coord => coord.Type === 'In');


    let markersAdded = false; // Flag to check if markers are added


    for (let i = 0; i < filteredCoordinates.length; i++) {
      const coords = filteredCoordinates[i].GeoCode.split(',');
      const latLng = new google.maps.LatLng(parseFloat(coords[0]), parseFloat(coords[1]));
      const label = {
        text: ' ' + (i + 1),
        color: '#fff',
        fontSize: '14px',
        backgroundColor: '#FF5733',
        fontWeight: 'bold',
        padding: '8px',
        borderRadius: '5px',
        border: '2px solid #fff'
      } as unknown as MarkerLabel;

      const marker = new google.maps.Marker({
        position: latLng,
        label: label,
        map: this.map
      });

      bounds.extend(latLng);

      if (i < filteredCoordinates.length - 1) {
        const nextCoords = filteredCoordinates[i + 1].GeoCode.split(',');
        const nextLatLng = new google.maps.LatLng(parseFloat(nextCoords[0]), parseFloat(nextCoords[1]));

        const request = {
          origin: latLng,
          destination: nextLatLng,
          travelMode: google.maps.TravelMode.DRIVING
        };

        directionsService.route(request, (response, status) => {
          if (status === google.maps.DirectionsStatus.OK) {
            const directionsRenderer = new google.maps.DirectionsRenderer({
              map: this.map,
              polylineOptions: {
                strokeColor: '#FF5733',
                strokeOpacity: 1.0,
                strokeWeight: 4
              },
              suppressMarkers: true // Disable default markers
            });
            directionsRenderer.setDirections(response);
          }
        });
      }
    }
    if (!markersAdded) {
      // If no markers are added, fit the map to bounds (if available)
      if (!bounds.isEmpty()) {
        this.map.fitBounds(bounds);
      }
    }
  }
}