
import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewChildren, QueryList, ElementRef } from '@angular/core';
import { Observable } from 'rxjs';
import { GridProperties, GridSetting } from '../../../inventory/models/IPack';
import { approvalservice } from './../../services/approval/approval.service';
import { approvallist, approval } from './../../models/iapproval.model'
import { Store } from '@ngxs/store';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
@Component({
  selector: 'app-approvallist',
  templateUrl: './approvallist.component.html',
  styleUrls: ['./approvallist.component.scss'],
})
export class approvallistComponent implements OnInit {
  title = 'Approval List'
  gridApi
  public rowData = [];
  public columnData = [];
  public defaultColDef = [];
  activeTab = 0;
  typ = 1
  index
  approvallistdata: Array<object>;
  public requestedtome: Array<object>;
  approvaljson:approval[]=[];
  //approvaljson= [];
  @ViewChildren("checkboxes") checkboxes: QueryList<ElementRef>;

  // checked = false;
  form: FormGroup;


  isMasterSel: boolean;



  constructor(public service: approvalservice, private http: HttpClient, public store: Store, private fb: FormBuilder) {

  }

  ngOnInit(): void {
    this.onTabChanged(0)
  }

  onTabChanged(args) {
    this.approvallist(args);
  /*  if (args == 0)  //Request To Me
    {

      this.approvallist(0);
    }
    else if (args == 1)   //Request By Me
    {

      this.approvallist(1);
    }*/
  }


  uncheckAll() {
    this.checkboxes.forEach((element) => {
      element.nativeElement.checked = false;
    });
  }
  checkAll() {
    this.checkboxes.forEach((element) => {
      element.nativeElement.checked = true;
    });
  }
  approve() {
   console.log("approve")
   console.log(this.approvaljson)
    this.service.approvalservice(this.approvaljson).subscribe((data) => {


    this.approvallistdata.splice(this.index , 1);
    //this.approvallistdata = data;

    });

  }
  reject() {
    this.service.rejectservice(this.approvaljson).subscribe((data) => {
      console.log(this.approvaljson)

      this.approvallistdata.splice(this.index , 1);
      //this.approvallistdata = data;

      });
   // this.service.rejectservice(this.approvaljson)

  }
  resend() {
    this.service.resendservice(this.approvaljson).subscribe((data) => {
      console.log(this.approvaljson)

      this.approvallistdata.splice(this.index , 1);
      //this.approvallistdata = data;

      });
  //  this.service.resendservice(this.approvaljson)

  }
  selectall(approvejson,id) {
    this.approvaljson.push(approvejson)
    //this.approvaljson = approvejson
    this.index=id
   // alert(this.index)
    console.log("args from selected checkbox")
    // console.log(args)
    console.log(JSON.stringify(this.approvaljson))

    /*

     var FinalWorkflowArrayObj = [];
   FinalWorkflowArrayObj.push(approvaljson)

   var workflowacceptobj = {};
   for (var i = 0; i < FinalWorkflowArrayObj.length; i++) {


   workflowacceptobj["DocumentNumber"] =FinalWorkflowArrayObj[i].doc ;
   workflowacceptobj["BizTransactionTypeId"] = FinalWorkflowArrayObj[i].bizid;
   workflowacceptobj["ObjectId"] = FinalWorkflowArrayObj[i].objid;
   workflowacceptobj["ProcessInstanceId"] = FinalWorkflowArrayObj[i].proinsid;
   workflowacceptobj["EntityCode"] = FinalWorkflowArrayObj[i].ent;
   workflowacceptobj["DisplayValue"] = FinalWorkflowArrayObj[i].disval;
   workflowacceptobj["FromUserCode"] = FinalWorkflowArrayObj[i].usercode;
   workflowacceptobj["InchargeMail"] = FinalWorkflowArrayObj[i].incmail;
   workflowacceptobj["Version"] = FinalWorkflowArrayObj[i].version;
   workflowacceptobj["PartyBranchId"] = FinalWorkflowArrayObj[i].getpartybranchidd;
   workflowacceptobj["Remarks"] = "";
   }
   var Finalworkflowacceptobj = [];
   Finalworkflowacceptobj.push(workflowacceptobj);
   console.log("finalapprojson")
   console.log(JSON.stringify(Finalworkflowacceptobj))

*/
  }


  public approvallist(args) {

    if (args == 0) {
      console.log("approvallist"+args)
      this.service.approvareqtomeservice().subscribe((data) => {
        this.approvallistformat(data)
        // this.approvallistdata = data;


      });
    }
    else {
      this.service.approvalreqbymeservice().subscribe((data) => {
        this.approvallistformat(data)
        // this.approvallistdata = data;


      });

    }

  }
  approvallistformat(data) {
    // this.approvallistdata = data;
    console.log("datalenght")
    console.log(data.length)
    this.approvallistdata = [];
    let mmheadids = ""
    let payments = ""
    for (var i = 0; i < data.length; i++) {


      /////////////////////////////////////////////////////////////////////////////////////////

      //  var fromDate = JSONDateToFormat(data[i].Date, "dd/mmm/yyyy/ HH:MM");


      var bizname = data[i].BizTransactionTypeName;
      if (bizname == null || data[i].BizTransactionTypeId == -1 || data[i].BizTransactionTypeId == 0) {
        bizname = "";
      }

      if (bizname.toLowerCase() == "task") {
        data[i].UserName1 = data[i].InchargeName;
      } else {
        data[i].UserName1 = data[i].UserName;
      }
      try {
        if (data[i].EntityId == -1899997954) {
          if (mmheadids == "") {
            mmheadids = data[i].ObjectId;
          } else {
            mmheadids = mmheadids + "," + data[i].ObjectId;
          }

        } else if (data[i].EntityId != -1899997954) {
          payments = "";
        }
      } catch (e) {
        console.log(e)
      }

      var mail = data[i].InchargeMail;

      if (mail == "null") {
        mail = "mail@myunisoft.com";
      }

      var approve = data[i].ApproveStatus;
      if (this.typ == 1) {
        if (approve == null || approve == "null") {
          approve = "Waiting For Approval";
        } else {
          approve = "Waiting For Approval by" + " " + " " + data[i].ApproverName;
        }

      }
      else if (this.typ == 2) {
        if (approve == null || approve == "null") {
          approve = "Waiting For Approval by" + " " + data[i].ApproverName;
        } else {
          approve = approve + " " + "by" + " " + data[i].ApproverName;

        }
      }

      else {
        if (approve == null || approve == "null") {
          approve = "Waiting For Approval by" + " " + data[i].ApproverName;
        } else {
          approve = approve + " " + "by" + " " + data[i].ApproverName;

        }
      }



      /*if (typ == 2) {
          document.getElementById("enable_accept").disabled = true;
          document.getElementById("enable_resend").disabled = true;
          document.getElementById("enable_request").disabled = true;
      }*/

      /* if (data[i].ApproveStatus == "Rejected") {
           chkcls = "Reject";
       }

       if (data[i].ApproveStatus == "ReSend") {
           chkcls = "Resend";

       }

       if (data[i].ApproveStatus == null || data[i].ApproveStatus == "null") {
           chkcls = "Waiting";

       }*/

      if (data[i].UserCode == null) {
        data[i].UserCode = "";

      }
      var json_object = "";
      if (data[i].JsonObject != "NONE") {
        json_object = eval("(" + data[i].JsonObject + ")");
        json_object = JSON.stringify(json_object);
      }
      var remark = "";
      if (data[i].Remarks != null) {
        remark = "Remarks" + ":" + " " + data[i].Remarks;
      }

      if (data[i].EntityId == -1899997952 || data[i].EntityCode == "MMHead" || data[i].EntityCode == "MMHEADDTO") {
        // var fromDate = JSONDateToFormat(data[i].Date, "dd/mmm/yyyy");
        var fromDate = data[i].Date
        if (data[i].PartyName == null || data[i].PartyName == "null") {
          data[i].PartyName = "";
        }
        data[i].DisplayValue = data[i].BizTransactionTypeName + " " + data[i].DocumentNumber + " " + fromDate + " " + data[i].PartyName + "-- " + data[i].TotalValue;
      }
      //added as per VV sir and Manivannan sir on 22 Oct 2017
      else {
        data[i].DisplayValue = data[i].DisplayValue + "-- " + data[i].TotalValue;
      }
      data[i].DisplayValue = data[i].DisplayValue.replace("'", " ");
      data[i].DisplayValue = data[i].DisplayValue.replace(/["']/g, " "); //added as per parthiban sir as one task was not getting approved

      //Added the below condition as per VV sir on 15 Feb 2018
      if (data[i].EntityId == -1899999161) //Voucher EntityId
      {
        data[i].DisplayValue = data[i].DisplayValue + "-- " + data[i].BizTransactionTypeName;
      }

      /*$(highlight).ready(function () {
      $("li").hover(function () {
      $(this).css("border-color", "#525252");
      }, function () {
      $(this).css("border-color", "#FFF");
      });
      });*/
      var displayoucode = "";

      if (data[i].OuCode != null && data[i].OuName != null) {
        displayoucode = " OU- " + data[i].OuCode;
      }

      //<img src='"+data[i].ImageUrl+"'  class='task_pro_image'/> <span>"+bizname+"</span><span> "+data[i].Particulars+"</span><p></p><p><span>"+'  '+"</span></p>

      //Added for redmine 20285
      var PartyNamedisplay = "";
      if (data[i].PartyName != null && data[i].PartyName != "") {
        PartyNamedisplay = data[i].PartyName;
      }


      data[i].Date = data[i].Date.replace('/Date(', '');
      data[i].Date = data[i].Date.replace(')/', '');
      // var jsDate = new Date(parseInt(parsedresponse[i].Date));
      //18/11/2017, 11:28:50 am
      let datefromservice = new Date(parseInt(data[i].Date))
      let localstring = datefromservice.toLocaleString()
      let splitlocalstring = localstring.split(",")
      let splitdate = splitlocalstring[0]
      let gettime = splitlocalstring[1].split(" ")
      let gettime2 = gettime[1].split(":")
      let splittime = gettime2[0] + ":" + gettime2[1]
      let fromdate = splitdate + " " + splittime

      let formdate1 = data[i].Date

      this.approvallistdata.push(
        {

          EntityId: data[i].EntityId,
          TASKNO: data[i].DocumentNumber,
          TASKNAME: data[i].DisplayValue,
          //TaskHrs: TaskHrs,
          // TaskDate: TaskDate,
          Approver: approve,
          TaskFromCode: data[i].UserCode,
          TaskFromName: data[i].UserName1,
          TaskFromDate: fromdate,//fromDate,
          displayoucode: displayoucode,
          partyname: PartyNamedisplay,
          Remarks: remark,
          DocumentNumber: data[i].DocumentNumber,
          BizTransactionTypeId: data[i].BizTransactionTypeId,
          ObjectId: data[i].ObjectId,
          ProcessInstanceId: data[i].ProcessInstanceId,
          EntityCode: data[i].EntityCode,
          DisplayValue: data[i].DisplayValue,
          FromUserCode: data[i].FromUserCode,
          InchargeMail: data[i].InchargeMail,
          Version: data[i].Version,
          PartyBranchId: data[i].PartyBranchId,
          // Approverclass: approverclass,
        }
      )
    }
    this.gridApi = this.approvallistdata;
    //console.log(this.approvallistdata)
  }
  public getgridsetting() {
    this.gridSetting().subscribe((res) => {
      this.columnData = res;
      this.getgridproperties();
    });
  }

  public getgridproperties() {
    this.gridproperties().subscribe((res) => {
      this.defaultColDef = res;
    });
  }

  private grid_setting = "assets/data/packlistseting.json";
  private grid_properties = "assets/data/grid-properties.json";
  gridSetting(): Observable<GridSetting[]> {
    return this.http.get<GridSetting[]>(this.grid_setting);
  }
  gridproperties(): Observable<GridProperties[]> {
    return this.http.get<GridProperties[]>(this.grid_properties);
  }
  public quicksearch(event: any) {
   // this.gridApi.setQuickFilter(event);
  // let allRowData: any[] = [];
  // this.gridApi.forEachNode(node => allRowData.push(node.data));
   this.gridApi.setQuickFilter(event);
   console.log("quicksearch")
  }
}
