import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewChildren, QueryList, ElementRef } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngxs/store';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
@Component({
    selector: 'app-geolocationtest',
    templateUrl: './geolocationtest.component.html',
    styleUrls: ['./geolocationtest.component.scss'],
})
export class geolocationtestComponent implements OnInit {
    title = 'Geolocation test'
    form: FormGroup;
    geoconfig
    constructor(private http: HttpClient, public store: Store, private fb: FormBuilder) {

    }

    ngOnInit(): void {
        this.geoconfig = {
            lat: 11.0282877, 
            long: 76.9551012,
            isdirection: true,
            address: ''
         };
    }
    public getlocation(data) {
        console.log("getgeo data")
     console.log(data)
      }

}
