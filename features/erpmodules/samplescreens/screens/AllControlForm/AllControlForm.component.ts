import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

import { Select, Store } from '@ngxs/store';

import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/internal/Observable'
import { LayoutState } from 'features/layout/store/layout.state';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { FormUnEditable } from 'features/layout/store/layout.actions';
import { InstrumentService } from 'features/erpmodules/finance/services/Instrument/Instrument.service';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { DateAdapter } from '@angular/material/core';

@Component({
  selector: 'app- AllControlForm',
  templateUrl: './AllControlForm.component.html',
  styleUrls: ['./AllControlForm.component.scss'],
})
export class AllControlFormComponent implements OnInit {
    @Select(LayoutState.CurrentTheme) currenttheme: any;
    @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
    @Select(LayoutState.Formeditabe) formedit$: Observable<any>;
    @Select(LayoutState.Movenxtpreid) movenextpreviousid$: Observable<any>;
    @Select(LayoutState.FormEdit) edit$: Observable<boolean>;
    dataLoading;
    SelectedCriteriaCheck;
    dateTime: Date;
    mytime: Date;
    selectedDate: Date;
    Createdate: string;
    isnoteditable: boolean;
    form: FormGroup;
    selectionName: string;
    OUselection: string;
    InstrumentNamePicklist;
    OUPicklist;
    DataType;
    TypeofInfo;
    Fromdate:Date;
    myTime: Date = new Date();
    TypeofInfoPicklist = [
      {
        key: 0,
        value: "Immediate",
      },
      {
        key: 1,
        value: "On Realisation"
      },
      {
        key: 2,
        value: "On Deposit"
      },
    ];
    DataTypePicklists = [
      {
        key: 0,
        value: "Check",
      },
      {
        key: 1,
        value: "Cheque"
      },
      {
        key: 2,
        value: "Demand Draft"
      },
      {
        key: 3,
        value: "EFT (Electronic Transfer)"
      },
      {
        key: 4,
        value: "Gift Voucher"
      },
      {
        key: 5,
        value: "Credit Card"
      },
      {
        key: 6,
        value: "Debit Card"
      },
    ];
    constructor(private dateAdapter: DateAdapter<Date>,public service: InstrumentService,public activeroute: ActivatedRoute, public fb: FormBuilder,public store: Store) {
      this.dateAdapter.setLocale('en-GB');
     }

    ngOnInit(): void {
    //     this.store.dispatch(new FormUnEditable);
    //     this.dateTime = new Date();
    //     this.Createdate = "/Date("+this.dateTime.getTime()+")/"
    //     this.edit$.subscribe(res => {
    //       console.log("Instrument Edit:",res);
    //       this.isnoteditable = res;
    //     })
    //     this.form = this.fb.group({
    //       InstrumentId: new FormControl(0),
    //       InstrumentName: new FormControl(),
    //       InstrumentType: new FormControl(),
    //       InstrumentDate: new FormControl(new Date()),
    //       InstrumentTime: new FormControl(),
    //       InstrumentAccountPostType: new FormControl(),
    //       InstrumentPostDatedType: new FormControl(),
    //       InstrumentValidityPeriodInDays: new FormControl(0),
    //       InstrumentStatusOfInstrument: new FormControl(),
    //       InstrumentIsApplicableForBank: new FormControl(),
    //       InstrumentIsApplicableforCounterTransaction: new FormControl(),
    //       InstrumentVersion: new FormControl(1),
    //       InstrumentStatus: new FormControl(1),
    //       AccountId: new FormControl(),
    //       AccountName: new FormControl(),
    //       InstrumentCreatedByName: new FormControl('ADMIN'),
    //       InstrumentCreatedOn: new FormControl(this.Createdate),
    //       InstrumentModifiedByName: new FormControl('ADMIN'),
    //       InstrumentModifiedOn: new FormControl(this.Createdate),
    //       InstrumentOuIds: new FormControl(),
    //       InstrumentOuNames: new FormControl(),
    //       InstrumentOuGroupIds: new FormControl(),
    //       InstrumentOuGroupNames: new FormControl()
    //     });
    //     let id;
    //     this.loadScreen(id)
    // }

    // loadScreen(id : number) {
    //   this.service.getall().subscribe(res => {
    //     let data = res
    //     data = data.map(({ Id, Name }) => ({ InstrumentId: Id, InstrumentName: Name }));
    //     this.InstrumentNamePicklist = data;
    //   })
    //   this.service.getOU().subscribe(res => {
    //     let data = res
    //     data = data.map(({ Id, Name }) => ({ InstrumentOuIds: Id, InstrumentOuNames: Name }));
    //     this.OUPicklist = data;
    //   })
    //   if (id) {
    //     this.service.getid(id).subscribe(res => {
    //       console.log("Instrument Selected Data:", res.InstrumentId)
    //       if(res.InstrumentId != undefined){
    //         this.form = this.fb.group({
    //           InstrumentId: [res.InstrumentId],
    //           InstrumentName: [res.InstrumentName],
    //           InstrumentType: [res.InstrumentType],
    //           InstrumentDate: [new Date()],
    //           InstrumentTime: [],
    //           InstrumentAccountPostType: [res.InstrumentAccountPostType],
    //           InstrumentPostDatedType: [res.InstrumentPostDatedType],
    //           InstrumentValidityPeriodInDays: [res.InstrumentValidityPeriodInDays],
    //           InstrumentStatusOfInstrument: [res.InstrumentStatusOfInstrument],
    //           InstrumentIsApplicableForBank: [res.InstrumentIsApplicableForBank],
    //           InstrumentIsApplicableforCounterTransaction: [res.InstrumentIsApplicableforCounterTransaction],
    //           InstrumentVersion: [res.InstrumentVersion],
    //           InstrumentStatus: [res.InstrumentStatus],
    //           AccountId: [res.AccountId],
    //           AccountName: [res.AccountName],
    //           InstrumentCreatedByName: [res.InstrumentCreatedByName],
    //           InstrumentCreatedOn: [res.InstrumentCreatedOn],
    //           InstrumentModifiedByName: [res.InstrumentModifiedByName],
    //           InstrumentModifiedOn: [res.InstrumentModifiedOn],
    //           InstrumentOuIds: [res.InstrumentOuIds],
    //           InstrumentOuNames: [res.InstrumentOuNames],
    //           InstrumentOuGroupIds: [res.InstrumentOuGroupIds],
    //           InstrumentOuGroupNames: [res.InstrumentOuGroupNames]
    //         }) as FormGroup;
    //         this.form.get('InstrumentName').valueChanges.subscribe(value => {
    //           if (value !== null && value != undefined) {
    //             if(value.length > 25){
    //               alert("Instrment Name length Should not be more than 25 ")
    //               const newValue = value.slice(0, 25); // Remove Extra character
    //               this.form.get('InstrumentName').setValue(newValue, { emitEvent: false });
    //             }
    //           }
    //         });
    //         this.form.get('InstrumentValidityPeriodInDays').valueChanges.subscribe(value => {
    //           if (value !== null) {
    //             const newValue = value.replace(/[^0-9]/g, ''); // Remove non-numeric characters
    //             this.form.get('InstrumentValidityPeriodInDays').setValue(newValue, { emitEvent: false });
    //           }
    //         });
    //         let ousplit = res.InstrumentOuNames.split(','); // For Multiple Picklist
    //         let oulist = [];
    //         for (let ou of ousplit) {
    //           if (this.OUPicklist.find(t => t.InstrumentOuNames == ou)) {
    //             oulist.push(ou)
    //           }
  
    //         }
    //         this.form.get('InstrumentOuNames').patchValue(oulist);
    //       }
    //       else{
    //         alert(JSON.stringify(res))
    //         // this.store.dispatch(new FormReset);
    //         // this.formreset()
    //       }
    //     })
  
    //   }
    // }

    // addEvent(type: string, event: MatDatepickerInputEvent<Date>) {
    //   this.selectedDate = event.value;
    // }

    // public changeInstrumentNameFn(selecteddata) {
    //   let InstrumentObj = this.InstrumentNamePicklist.find(t => t.InstrumentName === selecteddata);
    //   let id = InstrumentObj.InstrumentId
    //   this.loadScreen(id);
    // }

    // public changeOUNameFn(selecteddata) {
    //   this.form.get('InstrumentOuIds').patchValue("");
    //   for(let data of selecteddata){
    //     let InstrumentObj = this.OUPicklist.find(t => t.InstrumentOuNames === data);
    //     this.form.get('InstrumentOuIds').patchValue(this.form.get('InstrumentOuIds').value+InstrumentObj.InstrumentOuIds+",");
    //   }
    //   this.form.get('InstrumentOuIds').patchValue(this.form.get('InstrumentOuIds').value.slice(0, -1));
    }
}