import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Chartdata } from '../../../models/sales/chart.model';
import { Chartsetting } from '../../../models/sales/chart.model';
import { MarginSummaryService } from '../../../services/sales/marginsummary.service';
import { MarginSummary } from '../../../models/sales/marginsummary.model';

@Component({
  selector: 'goodbooks-marginsummary',
  templateUrl: './marginsummary.component.html',
  styleUrls: ['./marginsummary.component.scss']
})
export class MarginsummaryComponent implements OnInit {
  title = "Margin Summary";
  public chartdata = [];
  public chartsetting = [];
  private _chartsetting  = "/assets/data/chartsetting.json";
  
  constructor(private marginsummaryservice: MarginSummaryService,private http : HttpClient) { }
  ngOnInit() {
    this.GetMarginChartSetting();
  }

  marginsettingservice(): Observable<Chartsetting[]>{
    return this.http.get<Chartsetting[]>(this._chartsetting);
 }

  public GetMarginChartSetting() {
    this.marginsettingservice().subscribe((data)=>{
      this.chartsetting = data;
      console.log(this.chartsetting)
    })
    this.GetMarginChartData();
  }

  public GetMarginChartData() {
    this.margincolumndata().subscribe((data)=>{
      this.chartdata=data;
      console.log(this.chartdata)
    })
  }

  public margincolumndata(): Observable<Chartdata[]> {
    return this.marginsummaryservice.getmarginsummary().pipe(
      map((metadata: MarginSummary[]) => {
        const reportfields = metadata;
        console.log(reportfields)
        const columnfields = Array<Chartdata>();
        for (let i = 0; i < reportfields.length; i++) {
             const columnrow = new Chartdata();
             columnrow.name = reportfields[i].Code;
             columnrow.value = reportfields[i].Cost;
             columnfields.push(columnrow);
        }
        return columnfields;
      })
    );
  }


}


