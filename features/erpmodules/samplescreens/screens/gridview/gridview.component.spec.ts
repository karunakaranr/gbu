import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GridViewComponent } from './gridview.component';

describe('EntitygridComponent', () => {
  let component: GridViewComponent;
  let fixture: ComponentFixture<EntitygridComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GridViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GridViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
