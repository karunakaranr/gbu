import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';

import {
  GBBaseDataPageStoreComponentWN,
} from '@goodbooks/uicore';


import {
  getThisGBPageService,
} from './permission.factory';
import {
  PermissionFormgroup,
  PermissionFormgroupStore,
} from './permission.formgroup';
import { permissionservice } from './../../services/permission/permission.service'
//import { Time } from "@angular/common";
import { permissiondetail } from './../../models/permission/ipermission.model';
import {permissiondbservice} from './../../dbservices/permission/permissiondb.service'
//import { PackDBService } from './../../../dbservices/Pack/packdb.service';
import { Select, Store } from '@ngxs/store';
import { GBHttpService } from '@goodbooks/gbcommon';
import { Observable, Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';
import {PermissionState} from './../../stores/permission/permission.state'
//import { PackState } from './../../../stores/Pack/pack.state';
import {  GetAll, GetData, MoveFirst  } from './../../stores/permission/permission.action';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';

@Component({
  selector: 'app-permissionform',
  templateUrl: './permissionform.component.html',
  styleUrls: ['./permissionform.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'idfield' },
    { provide: 'url', useValue: 'url' },
    { provide: 'Store', useClass: Store },
    { provide: 'DBDataService',useClass: permissiondbservice,deps: [GBHttpService],},
    { provide: 'DataService', useClass: permissionservice, deps: ['DBDataService'] },
    { provide: 'PageService',useFactory: getThisGBPageService,deps: [Store,
        'DataService',
        'DBDataService',
        FormBuilder,
        GBHttpService,
        ActivatedRoute,
        Router,
      ],
    },
  ],
})
export class permissionformComponent extends GBBaseDataPageStoreComponentWN<permissiondetail>
  implements OnInit {
  title = 'Permission';

  form: PermissionFormgroupStore = new PermissionFormgroupStore(
    this.gbps.gbhttp.http,
    this.gbps.store
  );
  @Select(PermissionState) currentData$;

  thisConstructor() { }

  loadScreen(recid?: string) {
    if (recid) {
      const id = parseInt(recid);
      this.gbps.store.dispatch(new GetData(id));
    }
    else {
      this.gbps.store.dispatch(new GetAll(null)).subscribe(() => {
        this.gbps.store.dispatch(new MoveFirst());
      });
    }
  }

  ngOnInit(): void {
    let id = '';
    this.gbps.activeroute.params.subscribe((params: any) => {
      id = params['id'];
    });
    this.gbps.activeroute.paramMap.subscribe((params: ParamMap) => {
      id = params.get('id')
    });
    if (this.gbps.activeroute.firstChild) {
      this.gbps.activeroute.firstChild.params.subscribe((params: ParamMap) => {
        id = params['id'];
      });
    }
    this.loadScreen(id);
  }
}


/*import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { GBBasePageComponentNG, GBBasePageComponent, GBDataPageServiceNG, GBBaseDataPageStoreComponentWN } from '@goodbooks/uicore';
import { GBBaseFormGroup } from '@goodbooks/uicore';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService, GBFormGroup } from '@goodbooks/uicore';
import { AbstractDS } from '@goodbooks/gbdata';
import { GBPageService } from '@goodbooks/uicore';
import { permissionservice } from './../../services/permission/permission.service'
//import { Time } from "@angular/common";
import { permissiondetail } from './../../models/permission/ipermission.model';

import { Store } from '@ngxs/store';
import { Savepermission } from './../../stores/permission/permission.action';
@Component({
  selector: 'app-permissionform',
  templateUrl: './permissionform.component.html',
  styleUrls: ['./permissionform.component.scss'],
  providers: [
    { provide: 'PageService', useClass: GBDataPageServiceNG },
    { provide: 'DataService', useClass: permissionservice },
]
})
export class permissionformComponent {
  isDataPage = true;
  hasNavigation = false;
  title = 'Permission';
  dataService: AbstractDS;
  form: FormGroup;
  public Permissiondetails: permissiondetail;
 // form: GBFormGroup = new GBFormGroup(this.gbps.gbhttp.http, 'SC005', {});
  constructor(private store: Store,private fb: FormBuilder) {
    this.form = this.fb.group({
      type: ['', Validators.required],
      date:  ['', Validators.required],
      fromtime: ['', Validators.required],
      totime:  ['', Validators.required],
      duration:  ['', Validators.required],
      remarks:  ['', Validators.required],
      location:  ['', Validators.required],
 
  
  });

  }

  save(){
    alert("date")
console.log("inside save")
    this.Permissiondetails = {
      TIMESLIPID: "0",
      BIZTRANSACTIONTYPEID: 0,
      OUID: 0,
      PERIODID: 0,
      TIMESLIPNUMBER: 0,
      TIMESLIPDATE: getEPOCDate(new Date),
      EMPLOYEEID: 0,
      ATTENDANCEDATE: getEPOCDate(this.form.value.date),
      SHIFTID: 0,
      TYPE: this.form.value.type,
      FROMTIME: this.form.value.fromtime,
      TOTIME: this.form.value.totime,
      DURATION:this.form.value.duration,
      REASON:this.form.value.remarks,
      APPROVEDBYID: "-1",
      VERSION: 1,
      STATUS: 1,
      CREATEDBYID: 0,
      CREATEDON: 0,
      MODIFIEDBYID: 0,
      MODIFIEDON: 0,
      TIMESLIPSTARTTIME: this.form.value.fromtime,
      TIMESLIPENDTIME: this.form.value.totime,
      ONDUTYLOCATION: this.form.value.location,
      TIMESLIPSYNCID: -1
  }

  console.log( this.Permissiondetails )
    const savedata: permissiondetail = this.form.value as permissiondetail;
    this.store.dispatch(new Savepermission(savedata));
    console.log(this.form.value)


    //this.permission.permissionservice(this.form.value):permissiondetail
    // const permissionjson: permissiondetail = this.form.value as permissiondetail;
    // (this.gbps.dataService as permissionservice).permissionservice(permissionjson);
  }

  /*public savemodel() {
    const savedata: permissiondetail = this.form.value as permissiondetail;
    this.store.dispatch(new Savemodel(savedata));
  }*/
/*  public clear() {
    this.form.reset();
  }

}

function getEPOCDate(date) {
  if (date == "") {
      date = new Date();
  }
  if (date == undefined) {
      date = new Date();
  }
  var DDt = new Date(date);
  var DDtYear = DDt.getFullYear();
  var DDtMonth = DDt.getMonth();
  var DDtDay = DDt.getDate();
  return ("/Date(" + Date.UTC(DDtYear, DDtMonth, DDtDay) + ")/");
}

*/




