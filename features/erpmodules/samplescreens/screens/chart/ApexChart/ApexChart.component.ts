import { Component, OnInit, ViewChild, Input} from '@angular/core';
import { StatementOfAccount } from './../../../services/chart/StatementOfAccount/StatementOfAccount.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { Select } from '@ngxs/store';
import { ApexChartState } from 'features/erpmodules/samplescreens/stores/apexchart/apexchart.state';

import {
  ChartComponent,
  ApexAxisChartSeries,
  ApexChart,
  ApexXAxis,
  ApexDataLabels,
  ApexTitleSubtitle,
  ApexStroke,
  ApexGrid,
  ApexAnnotations,
} from 'ng-apexcharts';

export type ChartOptions = {
  series: ApexAxisChartSeries;
  annotations: ApexAnnotations;
  chart: ApexChart;
  xaxis: ApexXAxis;
  dataLabels: ApexDataLabels;
  grid: ApexGrid;
  labels: string[];
  stroke: ApexStroke;
  title: ApexTitleSubtitle;
};
const jsonvalue = require('./../StatementOfAccount/StatementOfAccount.json')
@Component({
  selector: 'app-ApexChart',
  templateUrl: './ApexChart.component.html',
  styleUrls: ['./ApexChart.component.scss']
})

export class ApexChartComponent implements OnInit {
  @Select(ApexChartState.ApexChart) apexchart$: Observable<any>;
  @Input() chartdata
  rowData;
  @ViewChild('chart') chart: ChartComponent;
  public chartOptions: Partial<ChartOptions>;
  public chartOptions1: Partial<ChartOptions>;
  public chartOptions2: Partial<ChartOptions>;
  title = 'Apex Chart '
  TypeList = ['Line','Bar','Pie']
  lightBoxForm: FormGroup;
  value;
  chartseries=[];
  keyValue=[];
  constructor(public service: StatementOfAccount, private fb: FormBuilder) { }
  ngOnInit(): void {
    this.apexchart$.subscribe(id => {
      this.rowData=id
      if(this.rowData!=undefined){
      this.keyValue=Object.keys(this.rowData[0])
        for (let item of this.chartdata[0].series) {
          let a1 = item.name
          item.data = []
          for (let data of this.rowData) {
            item.data.push(data[a1])
          }
        }
        let b1 = this.chartdata[0].label
        this.chartdata[0].labels = []
        for (let data of this.rowData) {
          this.chartdata[0].labels.push(data[b1])
        }

        if(this.chartdata[0].chart.type == "line" || this.chartdata[0].chart.type == "bar"){
          this.chartlist(this.chartdata)
        }
        else {
          this.chartlist1(this.chartdata)
        }
      }
    });
    // this.apexchart1$.subscribe(id => {
    //   this.chartdata=id
    //   console.log("IDDDDDDDD:"+JSON.stringify(this.rowData[0]))
    //   for (let item of this.chartdata[0].series) {
    //     console.log("Charttttt1111:"+JSON.stringify(item))
    //     let a1 = item.name
    //     item.data = []
    //     for (let data of this.rowData) {
    //       item.data.push(data[a1])
    //     }
    //   }
    //   let b1 = this.chartdata[0].label
    //   this.chartdata[0].labels = []
    //   for (let data of this.rowData) {
    //     this.chartdata[0].labels.push(data[b1])
    //   }
    //   this.keyValue=Object.keys(this.rowData[0])
    //   this.chartdata[0].chart.type = "line"
    //   console.log("Charttttt:"+this.chartdata)
    //   this.chartlist(this.chartdata)
    // });
    this.lightBoxForm = this.fb.group({
      label: '',
      series: '',
      charttype: '',
      charttype1:'line'
    });
    this.ApexChart();
    
  }
  public ApexChart() { 
    console.log("Report Detail:",this.rowData);
    console.log("Chart Detail:", JSON.stringify(this.chartdata))
        this.keyValue=Object.keys(this.rowData[0])
        for (let item of this.chartdata[0].series) {
          let a1 = item.name
          item.data = []
          for (let data of this.rowData) {
            item.data.push(data[a1])
          }
        }
        let b1 = this.chartdata[0].label
        this.chartdata[0].labels = []
        for (let data of this.rowData) {
          this.chartdata[0].labels.push(data[b1])
        }

        if(this.chartdata[0].chart.type == "pie"){
          this.chartlist1(this.chartdata)
        }
        else {
          this.chartlist(this.chartdata)
        }
  }
  public chartlist(chartdata) {

    this.chartOptions = {
      series: chartdata[0].series,

      chart: chartdata[0].chart,

      dataLabels: {
        enabled: false,
      },
      stroke: {
        curve: 'straight',
        width: 3,
      },
      grid: {
        padding: {
          right: 30,
          left: 20,
        },
      },
      title: chartdata[0].title,

      labels: chartdata[0].labels,
    };
    console.log("Chart:" + JSON.stringify(this.chartOptions));
  }
  public chartlist1(chartdata) {

    this.chartOptions = {
      series: chartdata[0].series[0].data,

      chart: chartdata[0].chart,
      dataLabels: {
        enabled: false,
      },
      stroke: {
        curve: 'straight',
        width: 3,
      },
      grid: {
        padding: {
          right: 30,
          left: 20,
        },
      },
      title: chartdata[0].title,

      labels: chartdata[0].labels,
    };
    console.log("Chart:" + JSON.stringify(this.chartOptions));
  }
  changeChart(value) {
    if (value == "Line") {
      this.chartdata[0].chart.type = "line"
      this.chartlist(this.chartdata)
    }
    if (value == "Pie") {
      this.chartdata[0].chart.type = "pie"
      this.chartlist1(this.chartdata)
    }
    if (value == "Bar") {
      this.chartdata[0].chart.type = "bar"
      this.chartlist(this.chartdata)
    }
  }

  public btnfilter() {
    document.getElementById("myModal1").style.display = "block";
  }

  public close() {
    document.getElementById("myModal1").style.display = "none";
  }

  public applybutton() {
    let chartdata1=this.chartdata
    let a1 = this.lightBoxForm.get('series').value
    chartdata1[0].series = []
    for( let item of a1){
     let temp=[]
     for (let data of this.rowData){
      temp.push(data[item])
     }
     chartdata1[0].series.push({name:item,data:temp})
    }
    let b1 = this.lightBoxForm.get("label").value
    chartdata1[0].labels = []
    for (let data of this.rowData) {
      chartdata1[0].labels.push(data[b1])
    }
    let value=this.lightBoxForm.get("charttype").value
    if (value == "Line") {
      this.chartdata[0].chart.type = "line"
      this.chartlist(this.chartdata)
    }
    if (value == "Pie") {
      this.chartdata[0].chart.type = "pie"
      this.chartlist1(this.chartdata)
    }
    if (value == "Bar") {
      this.chartdata[0].chart.type = "bar"
      this.chartlist(this.chartdata)
    }
    if (this.lightBoxForm.get("charttype").value == 'Pie') {
      this.chartlist1(chartdata1)
    } else {
      this.chartlist(chartdata1)
    }
    document.getElementById("myModal1").style.display = "none";
  }

  get charttype() {
    return this.lightBoxForm.get('charttype1')
  };

  public clearbutton(){
    this.lightBoxForm.reset();
    this.charttype.setValue(this.chartdata[0].chart.type)
  } 
}