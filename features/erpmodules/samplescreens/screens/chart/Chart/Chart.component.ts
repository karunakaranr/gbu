import { Component, OnInit, ViewChild, Input} from '@angular/core';
import { StatementOfAccount } from './../../../services/chart/StatementOfAccount/StatementOfAccount.service';
import { FormBuilder, FormGroup } from '@angular/forms';

import {
  ChartComponent,
  ApexAxisChartSeries,
  ApexChart,
  ApexXAxis,
  ApexDataLabels,
  ApexTitleSubtitle,
  ApexStroke,
  ApexGrid,
  ApexAnnotations,
} from 'ng-apexcharts';

export type ChartOptions = {
  series: ApexAxisChartSeries;
  annotations: ApexAnnotations;
  chart: ApexChart;
  xaxis: ApexXAxis;
  dataLabels: ApexDataLabels;
  grid: ApexGrid;
  labels: string[];
  stroke: ApexStroke;
  title: ApexTitleSubtitle;
};
const jsonvalue = require('./../StatementOfAccount/StatementOfAccount.json')
@Component({
  selector: 'app-ApexChart',
  templateUrl: './ApexChart.component.html',
  styleUrls: ['./ApexChart.component.scss']
})

export class ApexChartComponent implements OnInit {
  @Input() rowData;
  @Input() chartdata;
  @ViewChild('chart') chart: ChartComponent;
  public chartOptions: Partial<ChartOptions>;
  public chartOptions1: Partial<ChartOptions>;
  public chartOptions2: Partial<ChartOptions>;
  title = 'Apex Chart '
  TypeList = ['line','bar','pie']
  lightBoxForm: FormGroup;
  value;
  chartseries=[];
  keyValue=[];
  constructor(public service: StatementOfAccount, private fb: FormBuilder) { }
  ngOnInit(): void {
    this.lightBoxForm = this.fb.group({
      label: '',
      series: '',
      charttype: '',
      charttype1:'line'
    });
    this.insurancelist();
  }
  public insurancelist() {
    console.log("Report Detail:", this.rowData);
    console.log("Chart Detail:", JSON.stringify(this.chartdata))
    let date: Date
    this.service.StatementOfAccountview().subscribe(menudetails => {
      let servicejsondetail = menudetails
      this.service.Rowservice(servicejsondetail, '').subscribe(res => {
        this.rowData = res.ReportDetail;
        this.keyValue=Object.keys(this.rowData[0])
        for (let item of this.chartdata[0].series) {
          let a1 = item.name
          item.data = []
          for (let data of this.rowData) {
            item.data.push(data[a1])
          }
        }
        let b1 = this.chartdata[0].label
        this.chartdata[0].labels = []
        for (let data of this.rowData) {
          this.chartdata[0].labels.push(data[b1])
        }

        this.chartdata[0].chart.type = "line"
        this.chartlist(this.chartdata)
      });

    })
  }
  public chartlist(chartdata) {

    this.chartOptions = {
      series: chartdata[0].series,

      chart: chartdata[0].chart,

      dataLabels: {
        enabled: false,
      },
      stroke: {
        curve: 'straight',
        width: 3,
      },
      grid: {
        padding: {
          right: 30,
          left: 20,
        },
      },
      title: chartdata[0].title,

      labels: chartdata[0].labels,
    };
    console.log("Chart:" + JSON.stringify(this.chartOptions));
  }
  public chartlist1(chartdata) {

    this.chartOptions = {
      series: chartdata[0].series[0].data,

      chart: chartdata[0].chart,
      dataLabels: {
        enabled: false,
      },
      stroke: {
        curve: 'straight',
        width: 3,
      },
      grid: {
        padding: {
          right: 30,
          left: 20,
        },
      },
      title: chartdata[0].title,

      labels: chartdata[0].labels,
    };
    console.log("Chart:" + JSON.stringify(this.chartOptions));
  }
}