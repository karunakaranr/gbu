export class URLS {
  public static savepack = '/mms/Pack.svc';
  public static deletepack = '/mms/Pack.svc/?PackId=';
  public static packlist = '/mms/Pack.svc/SelectList';
  public static packfill = '/mms/Pack.svc/?PackId=';
  public static savemodel = '/prox/mms/ItemModel.svc';
  public static deletemodel = '/prox/mms/ItemModel.svc/?ModelId=';
  public static modellist  = '/prox/mms/ItemModel.svc/SelectList';
  public static modelfill  = '/prox/mms/ItemModel.svc/?ModelId=';
  public static brandlist = '/prox/mms/ItemBrand.svc/SelectList';
  public static MarginSummary  = '/mms/Register.svc/MarginSummary/?Type=4&FirstNumber=1&MaxResult=10';
  public static savepermission = '/ads/OfflineSync.svc/OffLineSynchRequest/';
  public static packsetlist = '/mms/PackSet.svc/SelectList';
  public static packsetfill = '/mms/PackSet.svc/?PackSetId=';
  public static packsetsave = '/mms/PackSet.svc/';
  public static packsetdelete='/mms/PackSet.svc/?PackSetId='
  public static saveemployee = '/prs/Employee.svc';


}
export class GbUIURLS {
  public static USERSETTING = '/fws/User.svc/UserSettingDetail/?UserId=';
  //public static USERSETTING = '{baseurl}/fws/User.svc/UserSettingDetail/?UserId=';
  public static MODULELIST = '/fws/Module.svc/ModuleList';
 
  public static MENULIST = '/fws/Menu.svc/UserModuleMenuTree/?UserId=';
  public static savepermission = '/prs/TimeSlip.svc/';
}
