import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { permissiondbservice } from './../../dbservices/permission/permissiondb.service';
import { permissiondetail } from './../../models/permission/ipermission.model';

import { GBBaseDataServiceWN } from '@goodbooks/gbdata';
@Injectable({
  providedIn: 'root',
})
export class permissionservice extends GBBaseDataServiceWN<permissiondetail>{

  constructor(public dbDataService: permissiondbservice) {
    super(dbDataService,'TimeSlipId')
   }

 /* public permissionservice(permissionjson): Observable<permissiondetail> {
    
    return this.permissiondbservice.permissionsave(permissionjson);
  }*/

  saveData(data: permissiondetail): Observable<any> {
    return super.saveData(data);
  }

  getData(id: string): Observable<permissiondetail> {
    return super.getData(id);
  }
  deleteData(id: any): Observable<any> {
    return super.deleteData(id);
  }
  getAll(): Observable<any> {
    return super.getAll();
  }

  moveFirst(): Observable<permissiondetail> {
    return super.moveFirst();
  }

  movePrev(): Observable<permissiondetail> {
    return super.movePrev();
  }

  moveNext(): Observable<permissiondetail> {
    return super.moveNext();
  }

  moveLast(): Observable<permissiondetail> {
    return super.moveLast();
  }
}  
