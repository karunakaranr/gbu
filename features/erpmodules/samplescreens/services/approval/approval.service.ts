import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { approvaldbservice } from './../../dbservices/approval/approvaldb.service';
import { approvallist,approval } from './../../models/iapproval.model';
@Injectable({
  providedIn: 'root',
})
export class approvalservice {

  constructor(public approvaldbservice: approvaldbservice) { }

  public approvareqtomeservice(): Observable<approvallist> {
    return this.approvaldbservice.approvaltomeservice();
  }
  public approvalreqbymeservice(): Observable<approvallist> {
    return this.approvaldbservice.approvalbymeservice();
  }

  public approvalservice(approvaljson): Observable<approval> {
     return this.approvaldbservice.approvalservice(approvaljson);
  }
  public rejectservice(approvaljson): Observable<approval> {
    return this.approvaldbservice.rejectservice(approvaljson);
 }
 public resendservice(approvaljson): Observable<approval> {
  return this.approvaldbservice.resendservice(approvaljson);
}

}












