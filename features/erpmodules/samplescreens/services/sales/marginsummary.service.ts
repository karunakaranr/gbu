import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { MarginSummaryDBService } from '../../dbservices/sales/marginsummaryDB.service';
import { MarginSummary } from '../../models/sales/marginsummary.model';

@Injectable({
  providedIn: 'root'
})
export class MarginSummaryService {
  constructor(public marginDBservice: MarginSummaryDBService) { }
  public getmarginsummary() {
  return this.marginDBservice.marginsummarydb();
  }

}
