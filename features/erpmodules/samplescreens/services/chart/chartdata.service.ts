import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Chartdata } from '../../models/chart.model';

@Injectable({
  providedIn: 'root'
})
export class ChartdataService {
  private _chartdata   = "assets/data/chartdata.json";
 
  private _url  = "assets/data/populationdata.json";
  
  private _bubble  = "assets/data/bubbledata.json";
 
  constructor(private http : HttpClient) { }
  chartdata(): Observable<Chartdata[]>{
  return this.http.get<Chartdata[]>(this._chartdata);
   }

  getchart(): Observable<Chartdata[]>{
  return this.http.get<Chartdata[]>(this._url);
   }
   getbubble(): Observable<Chartdata[]>{
    return this.http.get<Chartdata[]>(this._bubble);
     }
}
