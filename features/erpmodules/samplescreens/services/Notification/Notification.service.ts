import { Injectable } from '@angular/core';
import { NotificationDbService } from '../../dbservices/Notification/NotificationDb.service';
@Injectable({
  providedIn: 'root'
})
export class Notification {
  constructor(public dbservice: NotificationDbService) { }

  public NotificationView(){               
    return this.dbservice.notification();
  }
}