import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProductionComponent } from './production.component';
import { WorkOrderDashboardComponent } from './screens/WorkOrderDashboard/WorkOrderDashboard.component';
import { WorkOrderSummaryComponent } from './screens/WorkOrderSummary/WorkOrderSummary.component';
import { WorkOrderListComponent } from './screens/WorkOrderList/WorkOrderList.component';
import { ProductionSummaryComponent } from './screens/Production/Summary/ProductionSummary.component';
import { StoppageDetailComponent } from './screens/Stoppage/StoppageDetail.component';
// import { ConsolidatedShortageComponent } from './screens/Shortage/ConsolidatedShortage.component';
import { ProdRegisterReportComponent } from './screens/Register/ProdRegisterReport.component';
import { WIPStockViewComponent } from './screens/Production/WIP Stock/WIPStockView.component';
import { MachineWiseProdComponent } from './screens/Production/Machine Wise Production/MachineWiseProd.component';
import { WorkCenterComponent } from './screens/WorkCenter/WorkCenter.component';
import { ResourceTypeComponent } from './screens/ResourceType/ResourceType.component';
import { ProcessMasterComponent } from './screens/ProcessMaster/ProcessMaster.component';
import { DefectComponent } from './screens/Defect/Defect.component';
import { CountMasterComponent } from './screens/CountMaster/CountMaster.component';
import { MachineTypeComponent } from './screens/MachineType/MachineType.component';
import { ProcessTypeComponent } from './screens/ProcessType/ProcessType.component';
import { ParameterSetComponent } from './screens/ParameterSet/ParameterSet.component';
import { DieMasterComponent } from './screens/DieMaster/DieMaster.component';
import { NestingPlanComponent } from './screens/NestingPlan/NestingPlan.component';
import { FloorPlanComponent } from './screens/FloorPlan/FloorPlan.component';
const routes: Routes = [
  {
    path: 'diemaster',
    component: DieMasterComponent
  },
  {
    path: 'parameterset',
    component: ParameterSetComponent
  },
  {
    path: 'processtype',
    component: ProcessTypeComponent
  },
  {
    path: 'nestingplan',
    component: NestingPlanComponent
  },
  {
    path: 'processmaster',
    component: ProcessMasterComponent
  },
  {
    path: 'resourcetype',
    component: ResourceTypeComponent
  },
  {
    path: '',
    component: ProductionComponent
  },
  {
    path: 'workcenter',
    component: WorkCenterComponent
  },
  {
    path: 'Stoppagedetail',
    component: StoppageDetailComponent
  },
  {
    path: 'productionsummary',
    component: ProductionSummaryComponent
  },
  {
    path: 'workorder',
    component: WorkOrderDashboardComponent
  },
  {
    path: 'workordersummary',
    component: WorkOrderSummaryComponent
  },
  {
    path: 'workorderlist',
    component: WorkOrderListComponent
  },
  // {
  //   path: 'consolidatedshortage',
  //   component: ConsolidatedShortageComponent
  // },
  {
    path: 'productionregister',
    component: ProdRegisterReportComponent
  },
  {
    path: 'wipstock',
    component: WIPStockViewComponent
  },
  {
    path: 'machinewiseproduction',
    component: MachineWiseProdComponent
  },
  { path: 'themestest', loadChildren: () => import(`./../../themes/themes.module`).then(m => m.ThemesModule) },
  {
    path: 'defect',
    component: DefectComponent
  },
  {
    path: 'countmaster',
    component: CountMasterComponent
  },
  {
    path: 'machinetype',
    component: MachineTypeComponent
  },
  {
    path: 'FloorPlan',
    component: FloorPlanComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductionRoutingModule { }
