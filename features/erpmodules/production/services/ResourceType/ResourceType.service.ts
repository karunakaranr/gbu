import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';


// import { ModelName } from ‘ModelPath’;
import { IResourceType } from '../../models/IResourceType';


 
@Injectable({
  providedIn: 'root'
})
export class ResourceTypeService extends GBBaseDataServiceWN<IResourceType> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IResourceType>) {
    super(dbDataService, 'ResourceTypeId');
  }
}
