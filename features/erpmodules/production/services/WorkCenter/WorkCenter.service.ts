import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';


import { IWorkCenter } from '../../models/IWorkCenter';


 
@Injectable({
  providedIn: 'root'
})
export class WorkCenterService extends GBBaseDataServiceWN<IWorkCenter> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IWorkCenter>) {
    super(dbDataService, 'WorkCenterId');
  }
}
