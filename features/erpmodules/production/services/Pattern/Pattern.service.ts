import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';


// import { ModelName } from ‘ModelPath’;
import { IPattern } from '../../models/IPattern';

 
@Injectable({
  providedIn: 'root'
})
export class PatternService extends GBBaseDataServiceWN<IPattern> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IPattern>) {
    super(dbDataService, 'ItemId');
  }
}
