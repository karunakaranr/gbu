import { Injectable } from '@angular/core';
import { WorkOrderListDbService } from './../../dbservices/WorkOrderList/WorkOrderListDb.service';
import { ICriteriaDTODetail } from '../../models/IFilterDetail';
@Injectable({
  providedIn: 'root'
})
export class WorkOrderList {
  constructor(public dbservice: WorkOrderListDbService) { }

  public WorkOrderListview(FilterData : ICriteriaDTODetail){               
    return this.dbservice.picklist(FilterData);
  }
}