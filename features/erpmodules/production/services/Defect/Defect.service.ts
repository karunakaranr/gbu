import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';


import { IDefect } from '../../models/IDefect';


 
@Injectable({
  providedIn: 'root'
})
export class DefectService extends GBBaseDataServiceWN<IDefect> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IDefect>) {
    super(dbDataService, 'DefectId');
  }
}
