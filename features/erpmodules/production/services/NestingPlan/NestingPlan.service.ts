import { Inject, Injectable } from '@angular/core';

import { GBBaseDataServiceWN,GBBaseDBDataService } from 'libs/gbdata/src';


import { INestingPlan } from '../../models/INestingPlan';



 
@Injectable({
  providedIn: 'root'
})
export class NestingPlanService extends GBBaseDataServiceWN<INestingPlan> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<INestingPlan>) {
    super(dbDataService, 'NestingPlanId');
  }
}
