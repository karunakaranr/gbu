import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';

import { IProcessType } from '../../models/IProcessType';


 
@Injectable({
  providedIn: 'root'
})
export class ProcessTypeService extends GBBaseDataServiceWN<IProcessType> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IProcessType>) {
    super(dbDataService, 'ProcessTypeId');
  }
}
