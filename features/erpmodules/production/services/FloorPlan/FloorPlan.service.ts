import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';


// import { ModelName } from ‘ModelPath’;
import { IFloorPlan } from '../../models/IFloorPlan';


 
@Injectable({
  providedIn: 'root'
})
export class FloorPlanService extends GBBaseDataServiceWN<IFloorPlan> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IFloorPlan>) {
    super(dbDataService, 'RoleId');
  }
}
