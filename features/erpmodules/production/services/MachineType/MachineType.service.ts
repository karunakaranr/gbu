import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';


// import { ModelName } from ‘ModelPath’;
import { IMachineType } from '../../models/IMachineType';


 
@Injectable({
  providedIn: 'root'
})
export class MachineTypeService extends GBBaseDataServiceWN<IMachineType> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IMachineType>) {
    super(dbDataService, 'MachineTypeId');
  }
}
