import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';


// import { ModelName } from ‘ModelPath’;
import { ICountMaster } from '../../models/ICountMaster';

 
@Injectable({
  providedIn: 'root'
})
export class CountMasterService extends GBBaseDataServiceWN<ICountMaster> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<ICountMaster>) {
    super(dbDataService, 'CountId');
  }
}
