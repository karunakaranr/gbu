import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';


// import { ModelName } from ‘ModelPath’;
import { IDieMaster } from '../../models/IDieMaster';


 
@Injectable({
  providedIn: 'root'
})
export class DieMasterService extends GBBaseDataServiceWN<IDieMaster> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IDieMaster>) {
    super(dbDataService, 'PatternId');
  }
}
