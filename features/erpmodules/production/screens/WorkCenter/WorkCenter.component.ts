import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { WorkCenterService } from '../../services/WorkCenter/WorkCenter.service';
import { IWorkCenter } from '../../models/IWorkCenter';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as WorkCenterJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/WorkCenter.json'
import {URLS} from 'features/erpmodules/production/URLS/urls'
import { getValue } from '@ngneat/transloco';



@Component({
  selector: 'app-WorkCenter',
  templateUrl: './WorkCenter.component.html',
  styleUrls: ['./WorkCenter.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'WorkCenterId'},
    { provide: 'url', useValue: URLS.WorkCenter },
    { provide: 'DataService', useClass: WorkCenterService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class WorkCenterComponent extends GBBaseDataPageComponentWN<IWorkCenter > {
  title: string = 'WorkCenter'
  WorkCenterJSON = WorkCenterJSON;


  form: GBDataFormGroupWN<IWorkCenter > = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'WorkCenter', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.WorkCenterFillFunction(arrayOfValues.WorkCenterId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }

  public WorkCenterGetMinute(GetValue:any){
    console.log("GetValue: ",GetValue);
    this.form.get('WorkCenterStartTime').patchValue(GetValue.SelectedData)
  }

public GetDecimalValue(GetValue:any){
console.log("GetValue",GetValue);
this.form.get('WorkCenterAvailableCapacity').patchValue(GetValue)
}

  public WorkCenterFillFunction(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(PackSet => {
        this.form.patchValue(PackSet);
      })
    }
  }


  public WorkCenterPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IWorkCenter > {
  const dbds: GBBaseDBDataService<IWorkCenter > = new GBBaseDBDataService<IWorkCenter >(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IWorkCenter >, dbDataService: GBBaseDBDataService<IWorkCenter >, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IWorkCenter > {
  return new GBDataPageService<IWorkCenter >(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
