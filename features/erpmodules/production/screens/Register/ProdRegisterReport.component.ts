import { HttpClient } from '@angular/common/http';
import { Component, Inject, LOCALE_ID, OnInit } from '@angular/core';

import { Observable } from 'rxjs';
import { Select, Store } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';


@Component({
  selector: 'app-ProdRegisterReport',
  templateUrl: './ProdRegisterReport.component.html',
  styleUrls: ['./ProdRegisterReport.component.scss'],
})
export class ProdRegisterReportComponent implements OnInit {
  @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
  public rowData = [];
  constructor(public store: Store) { }

  ngOnInit(): void {
    this.ProdRegisterReport();
  }
  
  public ProdRegisterReport() {
    this.rowdatacommon$.subscribe(data => {
      this.rowData = data;
      console.log("Production Register Data :", this.rowData)
    });
  }

}
