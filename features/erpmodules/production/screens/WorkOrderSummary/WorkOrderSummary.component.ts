import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { WorkOrderSummary } from './../../services/WorkOrderSummary/WorkOrderSummary.service';
import { Inject, LOCALE_ID } from '@angular/core';
import { ICriteriaDTODetail } from '../../models/IFilterDetail';
import { IWorkOrderSummary } from '../../models/IWorkOrderSummary';
@Component({
  selector: 'app-WorkOrderSummary',
  templateUrl: './WorkOrderSummary.component.html',
  styleUrls: ['./WorkOrderSummary.component.scss']
})

export class WorkOrderSummaryComponent implements  OnChanges {
  @Input() ReportType : number;
  @Input() FilterData : ICriteriaDTODetail;
  RowData : IWorkOrderSummary[]=[];
  constructor(public service: WorkOrderSummary, @Inject(LOCALE_ID) public locale: string ) { }
  ngOnChanges(changes: SimpleChanges): void {
    if(changes.ReportType){
      this.service.WorkOrderSummaryview(this.FilterData,this.ReportType).subscribe(menudetails => {
        this.RowData=menudetails;
        console.log("Work Order Summary Data:",this.RowData)
      })
    }
  }

}