import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { CountMasterService } from '../../services/CountMaster/CountMaster.service';
import { ICountMaster } from '../../models/ICountMaster';
import { ButtonVisibility } from 'features/layout/store/layout.actions';

import * as CountMasterJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/CountMaster.json'
import {URLS} from 'features/erpmodules/production/URLS/urls'



@Component({
  selector: 'app-CountMaster',
  templateUrl: './CountMaster.component.html',
  styleUrls: ['./CountMaster.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'CountId'},
    { provide: 'url', useValue: URLS.Count },
    { provide: 'DataService', useClass: CountMasterService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class CountMasterComponent extends GBBaseDataPageComponentWN<ICountMaster > {
  title: string = 'CountMaster'
  CountMasterJSON = CountMasterJSON;


  form: GBDataFormGroupWN<ICountMaster > = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'CountMaster', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.CountMasterFillFunction(arrayOfValues.CountId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }
  public MCountTypePatch(MCountType:any){
    console.log("MCountType",MCountType)
    if (this.form.get('MCountType').value==3) {
      this.form.get('MCountType').patchValue("")
    }
 }

  public CountMasterFillFunction(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(PackSet => {
        this.form.patchValue(PackSet);
      })
    }
  }


  public CountMasterPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<ICountMaster > {
  const dbds: GBBaseDBDataService<ICountMaster > = new GBBaseDBDataService<ICountMaster >(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<ICountMaster >, dbDataService: GBBaseDBDataService<ICountMaster >, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<ICountMaster > {
  return new GBDataPageService<ICountMaster >(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
