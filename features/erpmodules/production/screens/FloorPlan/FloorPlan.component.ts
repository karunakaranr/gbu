import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { IFloorPlan } from '../../models/IFloorPlan';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import {URLS} from 'features/erpmodules/production/URLS/urls'
import { Component, OnInit } from '@angular/core';
import * as FloorPlanJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/FloorPlan.json'

//Fabric
import { fabric } from 'fabric';
import { FloorPlanService } from '../../services/FloorPlan/FloorPlan.service';


@Component({
  selector: 'app-FloorPlan',
  templateUrl: './FloorPlan.component.html',
  styleUrls: ['./FloorPlan.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'RoleId'},
    { provide: 'url', useValue: URLS.FloorPlan },
    { provide: 'DataService', useClass: FloorPlanService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class FloorPlanComponent extends GBBaseDataPageComponentWN<IFloorPlan> {
  title: string = 'Role';
  FloorPlanJSON = FloorPlanJSON;


  //Fabric
  textInput: string = ''; // Variable to store the text input
  output: string[] = [];
  canvas: fabric.Canvas;
  selectedColor: string;
  colorOptions = [
    { label: 'Red', value: 'red' },
    { label: 'Green', value: 'green' },
    { label: 'Blue', value: 'blue' },
    // Add more color options as needed
  ];
  public props = {
    canvasFill: '#ffffff',
    canvasImage: '',
    id: null,
    opacity: null,
    fill: null,
    fontSize: null,
    lineHeight: null,
    charSpacing: null,
    fontWeight: null,
    fontStyle: null,
    textAlign: null,
    fontFamily: null,
    TextDecoration: ''
  };
  json:any;
  savedjson: any;
  pausePanning = false;
  zoomStartScale = 0;
  currentX: number;
  currentY: number;
  xChange: number;
  yChange: number;
  lastX: number;
  lastY: number;
  //----------------------------------------


  form: GBDataFormGroupWN<IFloorPlan > = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'Role', {}, this.gbps.dataService);
  thisConstructor() {

    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.RoleRetrivalValue(arrayOfValues.RoleId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }


  }


// FloorPlan Code

ngOnInit(): void {
    
  this.canvas = new fabric.Canvas('c', {

    backgroundColor: '#fff',
    selection: false,
    width: 800,
    height: 400
  });
  this.canvas.setHeight(400);
  this.canvas.setWidth(800);

  // Listen to touch events on the canvas
this.canvas.on('touch:gesture', (e: any) => {
if (e.self.touches && e.self.touches.length == 2) {
    this.pausePanning = true;
    const point = new fabric.Point(e.self.x, e.self.y);
    if (e.self.state == "start") {
        this.zoomStartScale = this.canvas.getZoom();
    }
    const delta = this.zoomStartScale * e.self.scale;
    this.canvas.zoomToPoint(point, delta);
    this.output.push(`zoom`);
    this.pausePanning = false;
}
});

  this.canvas.on('selection:created', (e: any) => {
    this.output.push(`selection:created`);
    this.pausePanning = true;
  });

  this.canvas.on('selection:cleared', (e: any) => {
    this.output.push(`selection:cleared`);
    this.pausePanning = false;
  });

  this.canvas.on('touch:drag', (e: fabric.IEvent) => {
    const touchEvent = e.e as TouchEvent; // Extract native touch event
  
    if (!this.pausePanning && touchEvent.touches.length > 0) {
      const touch = touchEvent.touches[0];
      const touchX = touch.clientX;
      const touchY = touch.clientY;
  
      if (touchX !== undefined && touchY !== undefined) {
        this.currentX = touchX;
        this.currentY = touchY;
        this.xChange = this.currentX - this.lastX;
        this.yChange = this.currentY - this.lastY;
  
        if (Math.abs(this.xChange) <= 50 && Math.abs(this.yChange) <= 50) {
          const delta = new fabric.Point(this.xChange, this.yChange);
          this.canvas.relativePan(delta);
          this.output.push('pan');
        }
  
        this.lastX = touchX;
        this.lastY = touchY;
      }
    }
  });
  
  

  this.canvas.on('mouse:wheel', (opt: { e: { deltaY: any; offsetX: any; offsetY: any; preventDefault: () => void; stopPropagation: () => void; }; }) => {

    if (this.canvas.getObjects().length === 0) {
      console.log("no objs")
      return; // Exit the function if there are no objects inside the canvas
    }

    this.output.push(`ss`);
    const delta = opt.e.deltaY;
    let zoom = this.canvas.getZoom();
    zoom *= 0.999 ** delta;
    if (zoom > 20) zoom = 20;
    if (zoom < 0.01) zoom = 0.01;
    this.canvas.zoomToPoint({ x: opt.e.offsetX, y: opt.e.offsetY }, zoom);
    opt.e.preventDefault();
    opt.e.stopPropagation();
    this.canvas.requestRenderAll();
  });

  document.addEventListener('keydown', (event) => {
    if (event.key === 'Delete') {
      const activeObject = this.canvas.getActiveObject();
      if (activeObject) {
        this.canvas.remove(activeObject);
        this.canvas.requestRenderAll();
        this.output.push('Object removed');
      }
    }
  });

   // Listen to keyboard events
document.addEventListener('keydown', (event) => {
  const activeObject = this.canvas.getActiveObject();
  if (activeObject) {
    if (event.key === '[') { // Rotate counterclockwise
      activeObject.rotate(activeObject.angle - 5);
      this.canvas.requestRenderAll();
      this.output.push('Object rotated counterclockwise');
    } else if (event.key === ']') { // Rotate clockwise
      activeObject.rotate(activeObject.angle + 5);
      this.canvas.requestRenderAll();
      this.output.push('Object rotated clockwise');
    }
  }
});

// Listen to keyboard events
document.addEventListener('keydown', (event) => {
  const activeObject = this.canvas.getActiveObject();
  if (activeObject) {
    if (event.key === 'r') { // Rotate clockwise
      const rotationAngle = 90; // Change this value to rotate by a different angle
      activeObject.rotate(activeObject.angle + rotationAngle);
      this.canvas.requestRenderAll();
      this.output.push(`Object rotated clockwise by ${rotationAngle} degrees`);
    }
  }
});

document.addEventListener('keydown', (event) => {
  const key = event.keyCode;
  const distance = 5;

  const activeObject = this.canvas.getActiveObject();

  if (activeObject) {
    switch (key) {
      case 37: // Left arrow key
        activeObject.left -= distance;
        break;
      case 38: // Up arrow key
        activeObject.top -= distance;
        break;
      case 39: // Right arrow key
        activeObject.left += distance;
        break;
      case 40: // Down arrow key
        activeObject.top += distance;
        break;
      default:
        return;
    }

    this.canvas.renderAll();
    event.preventDefault();
  }
});
}





convertToJSON() {
  var json = JSON.stringify(this.canvas.toJSON());
  console.log(json);
  this.savedjson = json;
  this.form.get('RoleRemarks').patchValue(json)
  console.log("KARUNAKARAN",this.form.value)
  // You can do something with the JSON, such as send it to a server or display it in an alert
}

clearCanvas() {
  this.canvas.clear();
}

loadFromJSON() {
  console.log("saved json",this.savedjson)
  this.canvas.loadFromJSON(this.savedjson, () => {
    this.canvas.renderAll();
  });
}

addSVGToCanvas() {
  fabric.Image.fromURL('assets/Sample_Floorplan.jpg', (image: fabric.Image) => {
    image.scale(0.5).set({
      left: 100,
      top: 50
    });
    this.canvas.add(image);
    this.canvas.setActiveObject(image);
  });
}



onFileSelected(event: any) {
  const file: File = event.target.files[0];
  const reader = new FileReader();

  reader.onload = (e: any) => {
    this.loadSVG(e.target.result);
  };

  reader.readAsText(file);
}

loadSVG(svgText: string) {
  fabric.loadSVGFromString(svgText, (objects, options) => {
    const obj = fabric.util.groupSVGElements(objects, options);
    this.canvas.add(obj).renderAll();
  });
}

removeSelectedObject() {
  const selectedObject = this.canvas.getActiveObject();
  if (selectedObject) {
    this.canvas.remove(selectedObject);
    this.canvas.discardActiveObject();
  }
}

addText() {
  const text = new fabric.Textbox(this.textInput, {
    left: 300,
    top: 300,
    fill: '#000', // Text color
    fontSize: 24, // Font size in pixels
    fontFamily: 'Arial', // Font family
  });

  this.canvas.add(text);
}

getImgPolaroid(event: any) {
  const el = event.target;
  fabric.loadSVGFromURL(el.src, (objects, options) => {
    const image = fabric.util.groupSVGElements(objects, options);
    image.set({
      left: 10,
      top: 10,
      angle: 0,
      padding: 10,
      cornerSize: 10,
      hasRotatingPoint: true,
    });
    this.extend(image, this.randomId());
    this.canvas.add(image);
    this.selectItemAfterAdded(image);
  });
}

extend(obj, id) {
  obj.toObject = ((toObject) => {
    return function () {
      return fabric.util.object.extend(toObject.call(this), {
        id
      });
    };
  })(obj.toObject);
}

randomId() {
  return Math.floor(Math.random() * 999999) + 1;
}
selectItemAfterAdded(obj) {
  this.canvas.discardActiveObject().renderAll();
  this.canvas.setActiveObject(obj);
}
addFigure(figure) {
  let add: any;
  switch (figure) {
    case 'rectangle':
      add = new fabric.Rect({
        width: 200, height: 100, left: 10, top: 10, angle: 0,
        fill: '#3f51b5'
      });
      break;
    case 'square':
      add = new fabric.Rect({
        width: 100, height: 100, left: 10, top: 10, angle: 0,
        fill: '#4caf50'
      });
      break;
    case 'triangle':
      add = new fabric.Triangle({
        width: 100, height: 100, left: 10, top: 10, fill: '#2196f3'
      });
      break;
    case 'circle':
      add = new fabric.Circle({
        radius: 50, left: 10, top: 10, fill: '#ff5722'
      });
      break;
  }
  this.extend(add, this.randomId());
  this.canvas.add(add);
  this.selectItemAfterAdded(add);
}

setCanvasFill() {
  this.canvas.backgroundColor = this.selectedColor;
  this.canvas.renderAll();

}
rasterize() {
  const image = new Image();
  image.src = this.canvas.toDataURL({ format: 'png' });
  const w = window.open('');
  w.document.write(image.outerHTML);
  this.downLoadImage();
}
downLoadImage() {
  const c = this.canvas.toDataURL({ format: 'png' });
  const downloadLink = document.createElement('a');
  document.body.appendChild(downloadLink);
  downloadLink.href = c;
  downloadLink.target = '_self';
  downloadLink.download = Date.now() + '.png';
  downloadLink.click();
}

drawMode() {
  this.canvas.isDrawingMode = !this.canvas.isDrawingMode;
}

copySelectedObjectManually() {
  const activeObject = this.canvas.getActiveObject();
  if (activeObject) {
    const clone = fabric.util.object.clone(activeObject);
    clone.set({
      left: activeObject.left + 10,
      top: activeObject.top + 10
    });
    this.canvas.add(clone);
    this.canvas.renderAll();
  }
}

rasterizeJSON() {
  this.json = JSON.stringify(this.canvas, null, 2);
}
public RoleRetrivalValue(SelectedPicklistData: string): void {
  if (SelectedPicklistData) {
    this.gbps.dataService.getData(SelectedPicklistData).subscribe(PackSet => {
      this.form.patchValue(PackSet);
      console.log("saved json",PackSet.RoleRemarks)
      this.canvas.loadFromJSON(PackSet.RoleRemarks, () => {
        this.canvas.renderAll();
      });
    })
  }
}


}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IFloorPlan > {
  const dbds: GBBaseDBDataService<IFloorPlan > = new GBBaseDBDataService<IFloorPlan >(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IFloorPlan >, dbDataService: GBBaseDBDataService<IFloorPlan >, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IFloorPlan > {
  return new GBDataPageService<IFloorPlan >(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
