import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
// import { ServiceName } from ‘ServicePath’;
// import { ModelName } from ‘ModelPath’;
import { ResourceTypeService } from '../../services/ResourceType/ResourceType.service';
import { IResourceType } from '../../models/IResourceType';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
// import * as JsonName from ‘JsonPath’ './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/PackSet.json';
import * as ResourceTypeJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/ResourceType.json'
// import { url} from ‘UrlPath’;
import { URLS } from 'features/erpmodules/production/URLS/urls'
import { Console } from 'console';





@Component({
  selector: 'app-ResourceType',
  templateUrl: './ResourceType.component.html',
  styleUrls: ['./ResourceType.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'ResourceTypeId' },
    { provide: 'url', useValue: URLS.ResourceType },
    { provide: 'DataService', useClass: ResourceTypeService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class ResourceTypeComponent extends GBBaseDataPageComponentWN<IResourceType> {
  title: string = 'ResourceType'
  ResourceTypeJSON = ResourceTypeJSON;


  form: GBDataFormGroupWN<IResourceType> = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'ResourceType', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.ResourceTypeRetrivalValue(arrayOfValues.ResourceTypeId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }

  public ChangePicklistValue(CompoValue: Number) {
    console.log("CompoValue", CompoValue);
    if (CompoValue == 0) {
      console.log("CompoValue: ", 0);
      this.form.get('EmployeeTypeName').patchValue("NONE");
      this.form.get('MachineTypeName').patchValue("");
      this.form.get('AssetTypeName').patchValue("NONE")
    }
    else if (CompoValue == 1) {
      console.log("CompoValue: ", 1);
      this.form.get('MachineTypeName').patchValue("NONE");
      this.form.get('EmployeeTypeName').patchValue("");
      this.form.get('AssetTypeName').patchValue("NONE")
    }
    else if (CompoValue == 2) {
      console.log("CompoValue: ", 2);
      this.form.get('MachineTypeName').patchValue("NONE");
      this.form.get('AssetTypeName').patchValue("NONE");
      this.form.get('EmployeeTypeName').patchValue("NONE")
    }
    else if (CompoValue == 3) {
      console.log("CompoValue: ", 3);
      this.form.get('MachineTypeName').patchValue("NONE");
      this.form.get('EmployeeTypeName').patchValue("NONE");
      this.form.get('AssetTypeName').patchValue("")
    }
  }

  public ResourceTypeRetrivalValue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(PackSet => {
        this.form.patchValue(PackSet);
      })
    }
  }


  public ResourceTypePatchValue(SelectedPicklistDatas: any): void {
    console.log("SelectedPicklistDatasRetrivalValue", SelectedPicklistDatas)
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IResourceType> {
  const dbds: GBBaseDBDataService<IResourceType> = new GBBaseDBDataService<IResourceType>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IResourceType>, dbDataService: GBBaseDBDataService<IResourceType>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IResourceType> {
  return new GBDataPageService<IResourceType>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
