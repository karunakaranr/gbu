import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ProblemService } from '../../services/Problem/Problem.service';
import { IProblem } from '../../models/IProblem';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
// import * as JsonName from ‘JsonPath’ './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/PackSet.json';
import * as ProblemJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/Problem.json'
import {URLS} from 'features/erpmodules/production/URLS/urls'


@Component({
  selector: 'app-Problem',
  templateUrl: './Problem.component.html',
  styleUrls: ['./Problem.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'ProblemId'},
    { provide: 'url', useValue: URLS.Problem },
    { provide: 'DataService', useClass: ProblemService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class ProblemComponent extends GBBaseDataPageComponentWN<IProblem > {
  title: string = 'Problem'
  ProblemJSON = ProblemJSON;


  form: GBDataFormGroupWN<IProblem > = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'Problem', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.ProblemFillFunction(arrayOfValues.ProblemId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }


  public ProblemFillFunction(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(PackSet => {
        this.form.patchValue(PackSet);
      })
    }
  }


  public ProblemPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IProblem > {
  const dbds: GBBaseDBDataService<IProblem > = new GBBaseDBDataService<IProblem >(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IProblem >, dbDataService: GBBaseDBDataService<IProblem >, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IProblem > {
  return new GBDataPageService<IProblem >(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
