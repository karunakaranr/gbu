import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { MachineTypeService } from '../../services/MachineType/MachineType.service';
import { IMachineType } from '../../models/IMachineType';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as MachineTypeJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/MachineType.json'
import {URLS} from 'features/erpmodules/production/URLS/urls'


@Component({
  selector: 'app-MachineType',
  templateUrl: './MachineType.component.html',
  styleUrls: ['./MachineType.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'MachineTypeId'},
    { provide: 'url', useValue: URLS.Machine },
    { provide: 'DataService', useClass: MachineTypeService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class MachineTypeComponent extends GBBaseDataPageComponentWN<IMachineType > {
  title: string = 'MachineType'
  MachineTypeJSON = MachineTypeJSON;


  form: GBDataFormGroupWN<IMachineType > = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'MachineType', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.MachineTypeFillFunction(arrayOfValues.MachineTypeId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }


  public MachineTypeFillFunction(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(PackSet => {
        this.form.patchValue(PackSet);
      })
    }
  }


  public MachineTypePatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IMachineType > {
  const dbds: GBBaseDBDataService<IMachineType > = new GBBaseDBDataService<IMachineType >(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IMachineType >, dbDataService: GBBaseDBDataService<IMachineType >, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IMachineType > {
  return new GBDataPageService<IMachineType >(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
