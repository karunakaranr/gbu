import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ParameterSetService } from '../../services/ParameterSet/ParameterSet.service';
import { IParameterSet } from '../../models/IParameterSet';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as ParameterSetJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/ParameterSet.json'
import {URLS} from 'features/erpmodules/production/URLS/urls'

@Component({
  selector: 'app-ParameterSet',
  templateUrl: './ParameterSet.component.html',
  styleUrls: ['./ParameterSet.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'ParameterSetId'},
    { provide: 'url', useValue: URLS.Parameterset },
    { provide: 'DataService', useClass: ParameterSetService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class ParameterSetComponent extends GBBaseDataPageComponentWN<IParameterSet > {
  title: string = 'ParameterSet'
  ParameterSetJSON = ParameterSetJSON;


  form: GBDataFormGroupWN<IParameterSet > = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'ParameterSet', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.ParameterSetFillFunction(arrayOfValues.ParameterSetId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }


  public ParameterSetFillFunction(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(PackSet => {
        this.form.patchValue(PackSet);
      })
    }
  }


  public PicklistPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IParameterSet > {
  const dbds: GBBaseDBDataService<IParameterSet > = new GBBaseDBDataService<IParameterSet >(http);
  dbds.CopyFrom = ['ParameterSetCode','ParameterSetName'];
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IParameterSet >, dbDataService: GBBaseDBDataService<IParameterSet >, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IParameterSet > {
  return new GBDataPageService<IParameterSet >(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
