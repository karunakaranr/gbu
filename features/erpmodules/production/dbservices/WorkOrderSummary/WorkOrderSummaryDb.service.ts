import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { GBHttpService } from '@goodbooks/gbcommon';
import { AuthState } from 'features/common/shared/stores/auth.state';
import { Select } from '@ngxs/store';
import { ICriteriaDTODetail } from '../../models/IFilterDetail';
@Injectable({
    providedIn: 'root',
})
export class WorkOrderSummaryDbService {
    rowcriteria;
    @Select(AuthState.AddDTO) LOGINDTO$: Observable<any>;
    constructor(public http: GBHttpService) { }

    public picklist(FilterData: ICriteriaDTODetail, ReportType: number): Observable<any> {
        let criteria = {

            "SectionCriteriaList": [
                {
                    "SectionId": 0,
                    "AttributesCriteriaList": [{
                        "FieldName": "OUId",
                        "OperationType": 5,
                        "FieldValue": FilterData.SectionCriteriaList[0].AttributesCriteriaList[0].FieldValue,
                        "JoinType": 2,
                        "CriteriaAttributeName": FilterData.SectionCriteriaList[0].AttributesCriteriaList[0].CriteriaAttributeName,
                        "CriteriaAttributeValue": FilterData.SectionCriteriaList[0].AttributesCriteriaList[0].CriteriaAttributeValue,
                        "IsHeader": 0,
                        "IsCompulsory": 1,
                        "CriteriaAttributeId": FilterData.SectionCriteriaList[0].AttributesCriteriaList[0].CriteriaAttributeId,
                        "CriteriaAttributeType": 0,
                        "FilterType": 1
                     }, 
                    ],
                    "OperationType": 0
                }
            ]

        }

        const url = '/mms/MMReports.svc/WorkOrderSummaryReport/?Type=' + ReportType;
        return this.http.httppost(url, criteria);
    }
}