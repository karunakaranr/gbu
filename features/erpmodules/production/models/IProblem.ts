export class IProblem {
    ProblemId: number
    ProblemCode: string
    ProblemName: string
    ProblemCategoryId: number
    ProblemCategoryCode: string
    ProblemCategoryName: string
    ApplicableCategoryId: number
    ApplicableCategoryCode: string
    ApplicableCategoryName: string
    ProblemRemarks: string
    ProblemSortOrder: number
    ProblemStatus: number
    ProblemVersion: number
    ProblemCreatedById: number
    ProblemCreatedOn: string
    ProblemCreatedByName: string
    ProblemModifiedById: number
    ProblemModifiedOn: string
    ProblemModifiedByName: string
}