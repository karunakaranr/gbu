export class IFloorPlan {
  RoleId: number
  RoleCode: string
  RoleName: string
  RoleRemarks: string
  RoleCreatedById: number
  RoleCreatedOn: string
  RoleModifiedById: number
  RoleModifiedOn: string
  RoleSortOrder: number
  RoleStatus: number
  RoleVersion: number
  RoleSourceType: number
  RoleScreenOperationMode: number
}