export interface INestingPlan {
    NestingPlanId: number
    NestingPlanCode: string
    NestingPlanPlanDate: string
    NestingPlanPlanType: number
    SheetItemId: string
    SheetItemName: string
    SheetItemCode: string
    SheetSKUId: string
    SheetSKUName: string
    SheetSKUCode: string
    LotId: string
    LotLotNumber: string
    PackNumberId: string
    PackNumberNumber: string
    NestingPlanSheetDimension: string
    NestingPlanReferenceNumber: string
    NestingPlanReferenceDate: string
    NestingPlanRunLength: string
    NestingPlanWastage: string
    NestingPlanFileReference: number
    NestingPlanCMSID: number
    GuId: string
    PlanPreparedById: string
    PlanPreparedByName: string
    PlanApprovedById: string
    PlanApprovedByName: string
    NestingPlanNestingTime: string
    NestingPlanSetUpTime: string
    NestingPlanCycleTime: string
    NestingPlanRemarks: string
    NestingPlanVersion: number
    NestingPlanSheetWeight: string
    NestingPlanTotalComponentWeight: string
    UpdateNestingPlan: number
    NestingPlanStatus: string
    NestingPlanDetailArray: NestingPlanDetailArray[]
    NestingPlanCreatedOn: string
    NestingPlanModifiedOn: string
    NestingPlanModifiedByName: string
    NestingPlanCreatedByName: string
    NestingPlanSourceType: number
  }
  
  export interface NestingPlanDetailArray {
    NestingPlanDetailSlNo: number
    NestingPlanDetailType: number
    undefined: string
    "Item.Id": string
    ItemCode: string
    ItemId: string
    ItemName: string
    SKUId: string
    SKUCode: string
    SKUName: string
    NestingPlanDetailQuantity: string
    NestingPlanDetailWeight: string
    NestingPlanDetailTotalWeight: string
    NestingPlanDetailRunLength: string
    NestingPlanDetailTotalRunLength: string
    NestingPlanDetailDuration: string
    NestingPlanDetailTotalDuration: string
    NestingPlanDetailRemarks: string
    NestingPlanDetailDimension: string
  }
  