export class IDefect {
  DefectId: number
  DefectCode: string
  DefectName: string
  DefectPointPerDefect: string
  DefectRemarks: string
  DefectCreatedById: number
  DefectCreatedOn: string
  DefectModifiedById: number
  DefectModifiedOn: string
  DefectCreatedByName: string
  DefectModifiedByName: string
  DefectSortOrder: number
  DefectStatus: number
  DefectVersion: number
}