import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { BinService } from '../../services/Bin/bin.service';
import { IBin } from '../../models/IBin';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as BinJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/Bin.json';
import { WMSURLS } from 'features/erpmodules/WMS/urls/url';


@Component({
  selector: 'app-bin', 
  templateUrl: './bin.component.html',
  styleUrls: ['./bin.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'BinId'},
    { provide: 'url', useValue: WMSURLS.bin},
    { provide: 'DataService', useClass: BinService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class BinComponent extends GBBaseDataPageComponentWN<IBin> {
  title: string = "Bin"
  BinJSON = BinJSON; 


  form: GBDataFormGroupWN<IBin > = new GBDataFormGroupWN(this.gbps.gbhttp.http, "Bin", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.BinRetrivalValue(arrayOfValues.BinId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }

  public BinRetrivalValue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(Bin => {
        this.form.patchValue(Bin);
      })
    }
  }


  public BinPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IBin> {
  const dbds: GBBaseDBDataService<IBin> = new GBBaseDBDataService<IBin>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IBin >, dbDataService: GBBaseDBDataService<IBin>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IBin> {
  return new GBDataPageService<IBin>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}


