import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { WarehouseService } from '../../services/Warehouse/warehouse.service';
import { IWarehouse } from '../../models/IWarehouse';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as WarehouseJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/Warehouse.json';
import { WMSURLS } from '../../urls/url';


@Component({
  selector: 'app-warehouse',
  templateUrl: './warehouse.component.html',
  styleUrls: ['./warehouse.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'WareHouseId'},
    { provide: 'url', useValue: WMSURLS.warehouse },
    { provide: 'DataService', useClass: WarehouseService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class WarehouseComponent extends GBBaseDataPageComponentWN<IWarehouse> {
  title: string = "Warehouse"
  WarehouseJSON = WarehouseJSON;

  form: GBDataFormGroupWN<IWarehouse> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "Warehouse", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.WarehouseRetrivalValue(arrayOfValues.WareHouseId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }
  
    public WarehouseDefaultValue(value): void {
    console.log("OU",value)
    if (value == "0") {
      this.form.get('OuGroupName').patchValue('');
      this.form.get('OuId').patchValue('-1');
      this.form.get('OuName').patchValue('NONE');
      
    }
    if (value == "1") {
     
        this.form.get('OuGroupId').patchValue('-1');
        this.form.get('OuGroupName').patchValue('NONE');
        this.form.get('OuName').patchValue('');
       
      }
    }
    public StoreDefaultValue(value): void {
      console.log("Store",value)
      if (value == "0") {
        this.form.get('WareHouseStoreNames').patchValue('');
        this.form.get('StoreId').patchValue('-1');
        this.form.get('StoreName').patchValue('NONE');
       
      }
      if (value == "1") {
       
          this.form.get('WareHouseStoreIds').patchValue('-1');
          this.form.get('WareHouseStoreNames').patchValue('NONE');
          this.form.get('StoreName').patchValue('');
         
        }
      }

  public WarehouseRetrivalValue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(warehouse => {
        this.form.patchValue(warehouse);
      })
    }
  }

  public WarehousePatchValue(SelectedPicklistDatas: any): void {
    console.log("SelectedPicklistDatas.id",this.form.get(SelectedPicklistDatas.id))
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}

export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IWarehouse> {
  const dbds: GBBaseDBDataService<IWarehouse> = new GBBaseDBDataService<IWarehouse>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IWarehouse>, dbDataService: GBBaseDBDataService<IWarehouse>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IWarehouse> {
  return new GBDataPageService<IWarehouse>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}


