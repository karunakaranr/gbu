import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BinTypeComponent } from './screens/Bintype/bintype.component';
import { WarehouseComponent } from './screens/Warehouse/warehouse.component';
import { BinComponent } from './screens/Bin/bin.component';
import { CaptureimagevideoComponent } from 'features/gbaudiovideo/component/audiovideo/audiovideo.component';
 

const routes: Routes = [

      {
        path: 'bintype',
        component: BinTypeComponent
      },
      {
        path: 'imagevideo',
        component: CaptureimagevideoComponent
    
      },
      {
        path: 'warehouse',
        component: WarehouseComponent
      },
      {
        path: 'bin',
        component: BinComponent
      },

    {
      path: 'themestest',
      loadChildren: () =>
        import(`../../themes/themes.module`).then((m) => m.ThemesModule),
    }
  ];
  
  @NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
  })
  export class WMSroutingModule {}