export interface IBintype {
    BinTypeId: number
    BinTypeCode: string
    BinTypeName: string
    BinTypeMaxWeight: number
    BinTypeTotalCapacity: number
    BinTypeRemarks: string
    BinTypeSortOrder: number
    BinTypeStatus: number
    BinTypeVersion: number
    BinTypeCreatedById: number
    BinTypeCreatedOn: string
    BinTypeCreatedByName: string
    BinTypeModifiedById: number
    BinTypeModifiedByName: string
    BinTypeModifiedOn: string
  }
  