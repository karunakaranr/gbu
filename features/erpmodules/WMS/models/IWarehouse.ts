export interface IWarehouse {
    WareHouseStoreIds: string
    WareHouseStoreNames: string
    OuId: number
    OuCode: string
    OuName: string
    OuGroupId: number
    OuGroupCode: string
    OuGroupName: string
    StoreId: number
    StoreCode: string
    StoreName: string
    WareHouseId: number
    WareHouseCode: string
    WareHouseName: string
    WareHouseIsMultipleOu: number
    WareHouseIsMultipleStore: number
    WareHouseRemarks: string
    WareHouseWHType: number
    WareHouseCreatedById: number
    WareHouseModifiedById: number
    WareHouseCreatedByName: string
    WareHouseModifiedByName: string
    WareHouseCreatedOn: string
    WareHouseModifiedOn: string
    WareHouseStatus: number
    WareHouseVersion: number
    WareHouseSortOrder: number
  }
  