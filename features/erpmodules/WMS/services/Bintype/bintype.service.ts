import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';


import { IBintype } from '../../models/IBintype';


 
@Injectable({
  providedIn: 'root'
})
export class BintypeService extends GBBaseDataServiceWN<IBintype> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IBintype>) {
    super(dbDataService, 'BinTypeId');
  }
}
