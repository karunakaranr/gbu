export interface IDeviceType {
    DeviceTypeId: number,
    DeviceTypeCode: string,
    DeviceTypeName: string,
    DeviceTypeCreatedById: number,
    DeviceTypeCreatedOn: string,
    DeviceTypeModifiedById: number,
    DeviceTypeModifiedOn: string,
    DeviceTypeSortOrder: number,
    DeviceTypeStatus: number,
    DeviceTypeVersion: number
  }
  