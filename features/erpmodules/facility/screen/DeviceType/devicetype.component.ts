import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { DeviceTypeService } from '../../service/devicetype.service';
import { IDeviceType } from '../../models/IDeviceType';
// import * as JsonName from ‘JsonPath’ './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/PackSet.json';
import { FacilityURLS } from '../../URLS/url';
import * as DeviceTypeJSONName from './../../../../../apps/Goodbooks/Goodbooks/src/assets//FormJSONS/DeviceType.json'


@Component({
  selector: 'app-DeviceType',
  templateUrl: './devicetype.component.html',
  styleUrls: ['./devicetype.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'DeviceTypeId'},
    { provide: 'url', useValue: FacilityURLS.DeviceType },
    { provide: 'DataService', useClass: DeviceTypeService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class DeviceTypeComponent extends GBBaseDataPageComponentWN<IDeviceType > {
  title: string = 'DeviceType'
  DeviceTypeJSONName = DeviceTypeJSONName;


  form: GBDataFormGroupWN<IDeviceType > = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'DeviceType', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.FormFillFunction(arrayOfValues.DeviceTypeId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }


  public FormFillFunction(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(DeviceType => {
        this.form.patchValue(DeviceType);
      })
    }
  }


  public PicklistPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IDeviceType > {
  const dbds: GBBaseDBDataService<IDeviceType > = new GBBaseDBDataService<IDeviceType >(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IDeviceType >, dbDataService: GBBaseDBDataService<IDeviceType >, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IDeviceType > {
  return new GBDataPageService<IDeviceType >(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}


