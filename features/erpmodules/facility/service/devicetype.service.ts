import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';


import { IDeviceType } from '../models/IDeviceType';


 
@Injectable({
  providedIn: 'root'
})
export class DeviceTypeService extends GBBaseDataServiceWN<IDeviceType> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IDeviceType>) {
    super(dbDataService, 'DeviceTypeId');
  }
}
