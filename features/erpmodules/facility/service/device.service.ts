import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { IDevice } from '../models/IDevice';




 
@Injectable({
  providedIn: 'root'
})
export class DeviceService extends GBBaseDataServiceWN<IDevice> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IDevice>) {
    super(dbDataService, 'DeviceId');
  }
}
