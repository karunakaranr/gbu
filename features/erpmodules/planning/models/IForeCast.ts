export interface IForeCast {
    ForeCastId: number
    ForeCastCode: string
    ForeCastName: string
    ForeCastSetId: number
    ForeCastSetCode: string
    ForeCastSetName: string
    ForeCastPeriodType: number
    ForeCastItemLevel: number
    ForeCastIsForeCastConsume: number
    ForeCastMaxPercentPerDocument: number
    ForeCastForwardDays: number
    ForeCastBackwardDays: number
    InchargeId: number
    InchargeCode: string
    InchargeName: string
    ForeCastSortOrder: number
    ForeCastStatus: number
    ForeCastVersion: number
    ForeCastCreatedById: number
    ForeCastCreatedOn: string
    ForeCastCreatedByName: string
    ForeCastModifiedById: number
    ForeCastModifiedByName: string
    ForeCastModifiedOn: string
  }
  