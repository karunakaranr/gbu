export interface IForeCastSet {
    ForeCastSetId: number
    ForeCastSetCode: string
    ForeCastSetName: string
    ForeCastSetPeriodType: number
    ForeCastSetLevel: number
    ForeCastSetIsForeCastConsume: number
    ForeCastSetMaxPercentPerDocument: number
    ForeCastSetForwardDays: number
    ForeCastSetBackwardDays: number
    InchargeId: number
    InchargeCode: string
    InchargeName: string
    ForeCastSetSortOrder: number
    ForeCastSetStatus: number
    ForeCastSetVersion: number
    ForeCastSetCreatedById: number
    ForeCastSetCreatedByName: any
    ForeCastSetCreatedOn: string
    ForeCastSetModifiedById: number
    ForeCastSetModifiedByName: any
    ForeCastSetModifiedOn: string
  }