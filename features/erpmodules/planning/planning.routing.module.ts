import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ForeCastSetComponent } from './screens/MRPMaster/ForecastSet/forecastset.component';
import { MRPMasterComponent } from './screens/MRPMaster/MRPMaster.component';
import { ForecastComponent } from './screens/Forecast/forecast.component';
const routes: Routes = [
  {
    path: 'forecastset',
    component: ForeCastSetComponent
  },
  {
    path: 'mrpmaster',
    component: MRPMasterComponent
  },
  {
    path: 'forecast',
    component: ForecastComponent 
  },
  {
    path: 'themestest',
    loadChildren: () =>
      import(`../../themes/themes.module`).then((m) => m.ThemesModule),
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PlanningroutingModule { }