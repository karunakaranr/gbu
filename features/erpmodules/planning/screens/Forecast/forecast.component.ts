import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ForecastService } from '../../services/Forecast/forecast.service';
import { IForeCast } from '../../models/IForeCast';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as ForecastJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/Forecast.json';
import { WMSURLS } from 'features/erpmodules/WMS/urls/url';
import { PLANNINGURLS } from '../../URLS/url';


@Component({
  selector: 'app-forecast', 
  templateUrl: './forecast.component.html',
  styleUrls: ['./forecast.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'ForeCastId'},
    { provide: 'url', useValue: PLANNINGURLS.Forecast},
    { provide: 'DataService', useClass: ForecastService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class ForecastComponent extends GBBaseDataPageComponentWN<IForeCast> {
  title: string = "Forecast"
  ForecastJSON = ForecastJSON; 


  form: GBDataFormGroupWN<IForeCast > = new GBDataFormGroupWN(this.gbps.gbhttp.http, "Forecast", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.ForecastRetrivalValue(arrayOfValues.ForeCastId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }
// 
  public ConsumptionFunction(value): void {
    console.log("form.get('ForeCastMaxPercentPerDocument').value:",this.form.get('ForeCastMaxPercentPerDocument').value)

    if (value == '0') { 
    
      this.form.get('ForeCastMaxPercentPerDocument').patchValue(0);
      this.form.get('ForeCastForwardDays').patchValue(0);
      this.form.get('ForeCastBackwardDays').patchValue(0);
    }
    if (value == '1') { 
    
      this.form.get('ForeCastMaxPercentPerDocument').patchValue(0);
      this.form.get('ForeCastForwardDays').patchValue(0);
      this.form.get('ForeCastBackwardDays').patchValue(0);
    }
   
  }
  public ForecastRetrivalValue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(ForeCast => {
        this.form.patchValue(ForeCast);
      })
    }
  }
  
// patching
// public PatchingValues(forecast){
//   console.log("forecast",forecast)
//       this.form.get('InchargeName').patchValue(forecast)
//     }

  public ForecastPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IForeCast> {
  const dbds: GBBaseDBDataService<IForeCast> = new GBBaseDBDataService<IForeCast>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IForeCast >, dbDataService: GBBaseDBDataService<IForeCast>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IForeCast> {
  return new GBDataPageService<IForeCast>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}


