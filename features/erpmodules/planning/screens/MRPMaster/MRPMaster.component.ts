import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { MRPMasterService } from '../../services/MRPMaster.service';
import { IMRPMaster } from '../../models/IMRPMaster';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { PLANNINGURLS } from '../../URLS/url';
// import * as MRPMasterJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/AccountSchedule.json';
import * as MRPMasterJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/MRPMaster.json'
@Component({
  selector: 'app-MRPMaster',
  templateUrl:'MRPMaster.component.html',
  styleUrls:['MRPMaster.component.scss'],
  providers: [
    { provide: 'IdField', useValue:'MRPId' },
    { provide: 'url', useValue: PLANNINGURLS.MRPMaster},
    { provide: 'DataService', useClass: MRPMasterService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class MRPMasterComponent extends GBBaseDataPageComponentWN<IMRPMaster> {
  title: string = "MRPMaster"
  MRPMasterJSON = MRPMasterJSON;

  form: GBDataFormGroupWN<IMRPMaster> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "MRPMaster", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.MRPMasterFormVal(arrayOfValues.MRPId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }
  public MRPMasterFormVal(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(MRPMaster => {
          this.form.patchValue(MRPMaster);
      })
    }
  }
  public MRPMasterpatch(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}
export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IMRPMaster> {
  const dbds: GBBaseDBDataService<IMRPMaster> = new GBBaseDBDataService<IMRPMaster>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IMRPMaster>, dbDataService: GBBaseDBDataService<IMRPMaster>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IMRPMaster> {
  return new GBDataPageService<IMRPMaster>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
