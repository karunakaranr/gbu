import { Inject, Injectable } from '@angular/core';

import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';

import { IMRPMaster } from '../models/IMRPMaster';

@Injectable({
  providedIn: 'root'
})
export class MRPMasterService extends GBBaseDataServiceWN<IMRPMaster> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IMRPMaster>) {
    super(dbDataService, 'MRPId');
  }
}