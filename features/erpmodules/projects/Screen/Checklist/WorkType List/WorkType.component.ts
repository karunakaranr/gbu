
import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs';
import { Select } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { SharedService } from 'features/commonreport/service/datapassing.service';


@Component({
    selector: 'app-WorkType',
    templateUrl: './WorkType.component.html',
    styleUrls: ['./WorkType.component.scss'],
})
export class WorkTypeComponent implements OnInit {
    @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
    public rowData: any;
  

    constructor(public sharedService: SharedService) { }
    ngOnInit(): void {
        this.getWorkType();
    }

    public getWorkType() {
        this.sharedService.getRowInfo().subscribe(data => {
            this.rowData = data;
            if (data.length > 0) {
            console.log("WorkType Details :", this.rowData);

          
            let groupingData = this.rowData;
            var finalizedGroup = [];

            groupingData.map((inputData, index) => {

                for (let i = 0; i < inputData['WorkTypeDetailArray'].length; i++) {
                    let temp;

                    if (inputData['WorkTypeDetailArray'][i]['WorkTypeDetailType'] == 0.0) {
                        temp = "Activity";
                    } else {
                        temp = "WorkType";
                    }

                    let remarks = inputData['WorkTypeDetailArray'][i]['WorkTypeDetailRemarks'];
                    let updatedRemarks = remarks.replace(/u0026/g, "&");

                    finalizedGroup.push({
                        sno: index,
                        Type: temp,
                        Level: inputData['WorkTypeDetailArray'][i]['WorkTypeDetailLevel'],
                        Activity: inputData['WorkTypeDetailArray'][i]['ActivityName'],
                        Parent: inputData['WorkTypeDetailArray'][i]['ParentWorkTypeIdName'],
                        Group: inputData['WorkTypeDetailArray'][i]['UserGroupName'],
                        Employee: inputData['WorkTypeDetailArray'][i]['EmployeeName'],
                        Role: inputData['WorkTypeDetailArray'][i]['RoleName'],
                        Remarks: updatedRemarks,

                        tdscatname: inputData['AllocationTypeCode'],
                        allocationname: inputData['AllocationTypeName'],
                        branchprtyname: inputData['WorkTypeCode'],
                        demonew: inputData['WorkTypeName'],
            
                    });
                }
            });
        

            const final = {};
            finalizedGroup.forEach((detailData) => {
                final[detailData.tdscatname] = {
                    singlegrup: {},
                    // sno: detailData.tdscatname,
                    Type: detailData.tdscatname,
                    Level:  detailData.allocationname,
                    Activity: "",
                    Parent: "",
                    Group: "",
                    Employee: "",
                    Role: "",
                    Remarks: "",
                  
                    ...final[detailData.tdscatname]

                };

                final[detailData.tdscatname].singlegrup[detailData.branchprtyname] = {
                    deductedmultigrup: {},
                    // sno: detailData.branchprtyname,
                    Type: detailData.branchprtyname,
                    Level: detailData.demonew,
                    Activity: "",
                    Parent: "",
                    Group: "",
                    Employee: "",
                    Role: "",
                    Remarks: "",
                   

                    ...final[detailData.tdscatname].singlegrup[detailData.branchprtyname],
                }

                final[detailData.tdscatname].singlegrup[detailData.branchprtyname].deductedmultigrup[detailData.sno] = {
                    // sno: detailData.sno,
                    Type: detailData.Type,
                    Level: detailData.Level,
                    Activity: detailData.Activity,
                    Parent: detailData.Parent,
                    Group: detailData.Group,
                    Employee: detailData.Employee,
                    Role: detailData.Role,
                    Remarks: detailData.Remarks,
                  

                }
            });

            const dedGroups = Object.keys(final);

            const tableData = [];

            dedGroups.forEach((Details) => {
                const finalsiglegrup = Object.keys(final[Details].singlegrup);
               

                tableData.push({
                    // sno: final[Details].sno,
                    Type: final[Details].Type,
                    Level:final[Details].Level,
                    Activity: "",
                    Parent: "",
                    Group:"",
                    Employee: "",
                    Role:"",
                    Remarks: "",
                    certdate: "",
                    bold: true,
                });



                finalsiglegrup.forEach(ag => {
                    const finalmultigrup = Object.keys(final[Details].singlegrup[ag].deductedmultigrup)
                   

                    tableData.push({
                        // sno: final[Details].deductedgroup[ag].sno,
                        Type: final[Details].singlegrup[ag].Type,
                        Level:final[Details].singlegrup[ag].Level,
                        Activity: "",
                        Parent: "",
                        Group: "",
                        Employee: "",
                        Role: "",
                        Remarks: "",
                        certdate: "",
                        bold: true,
                    })


                    finalmultigrup.forEach(account => {
                        tableData.push({
                            // sno: final[Details].deductedgroup[ag].deductedmultigrup[account].sno,
                            Type: final[Details].singlegrup[ag].deductedmultigrup[account].Type,
                            Level: final[Details].singlegrup[ag].deductedmultigrup[account].Level,
                            Activity: final[Details].singlegrup[ag].deductedmultigrup[account].Activity,
                            Parent: final[Details].singlegrup[ag].deductedmultigrup[account].Parent,
                            Group: final[Details].singlegrup[ag].deductedmultigrup[account].Group,
                            Employee: final[Details].singlegrup[ag].deductedmultigrup[account].Employee,
                            Role: final[Details].singlegrup[ag].deductedmultigrup[account].Role,
                            Remarks: final[Details].singlegrup[ag].deductedmultigrup[account].Remarks,
                           

                        })
                    })
                })

            });
            this.rowData = tableData;

            console.log("Final Data:", this.rowData[0])


        }
        });
    }
}
