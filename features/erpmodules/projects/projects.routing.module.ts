import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProjectComponent } from './projects.component';
import { WorkTypeComponent } from './Screen/Checklist/WorkType List/WorkType.component';
const routes: Routes = [
  {
    path: '',
    component: ProjectComponent
  },

  
  
  { path: 'themestest', loadChildren: () => import(`./../../themes/themes.module`).then(m => m.ThemesModule) },

  
  {
    path: 'WorkType',
    children: [
      {
        path: 'list',
        component: WorkTypeComponent
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProjectRoutingModule { }
