import { Component, OnInit } from '@angular/core';
import { TDSContact } from './../../services/TDSContact/TDSContact.service';
const col = require('./../../../../../apps/Goodbooks/Goodbooks/src/assets/data/tdscontact.json')


@Component({
  selector: 'app-TDSContact',
  templateUrl: './TDSContact.component.html',
  styleUrls: ['./TDSContact.component.scss']
})

export class TDSContactComponent implements OnInit {
  title = 'TDSContact '
  public rowData = []
  public columnData = []

  constructor(public service: TDSContact) { }
  ngOnInit(): void {
    this.insurancelist();
  }
  public insurancelist() {
    this.service.tdscontactview().subscribe(menudetails => {
      this.columnData = col;
      let servicejsondetail = menudetails
      this.service.Rowservice(servicejsondetail).subscribe(res => {
        this.rowData = res.ReportDetail;
        for (let data of this.rowData) {
            if (data.TDSContactContactType==0){
                data.TDSContactContactType="LandLord"
            }
            if (data.TDSContactContactType==1){
                data.TDSContactContactType="Lender"
            }
            if (data.TDSContactContactType==2){
                data.TDSContactContactType="Previous Employer"
            }
            if (data.TDSContactContactType==3){
                data.TDSContactContactType="Others"
            }

        }
  });
})

}

}