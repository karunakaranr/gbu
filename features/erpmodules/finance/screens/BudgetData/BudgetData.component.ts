import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { BudgetDataService } from '../../services/BudgetData/BudgetData.service';
import { IBudgetData } from '../../models/IBudgetData';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as BudgetDataJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/BudgetData.json'
import { URLS } from '../../URLS/urls';



@Component({
  selector: 'app-BudgetData',
  templateUrl: './BudgetData.component.html',
  styleUrls: ['./BudgetData.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'BudgetDataId'},
    { provide: 'url', useValue: URLS.BudgetData },
    { provide: 'saveurl', useValue: URLS.BudgetSave },
    { provide: 'DataService', useClass: BudgetDataService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url','saveurl'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class BudgetDataComponent extends GBBaseDataPageComponentWN<IBudgetData > {
  title: string = 'BudgetData'
  BudgetDataJSON = BudgetDataJSON;
  AccountLevelStatus:number = 0;
  PeriodTypeStatus: number = 0;
  ngOnInit(): void {
    this.AccountLevelStatus
    this.PeriodTypeStatus
  }

  form: GBDataFormGroupWN<IBudgetData > = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'BudgetData', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.BudgetDataFillFunction(arrayOfValues.BudgetDataId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }

  public FindAccountLevel(event:any){
    console.log("event",event)
    this.form.get('BudgetDataFromDate').patchValue(event.Selected.FromDate)
    this.form.get('BudgetDataToDate').patchValue(event.Selected.ToDate)
    console.log("eventAccountLevel",event.Selected.AccountLevel)
    var AccountLevel = event.Selected.AccountLevel
    var PeriodType = event.Selected.PeriodType
    console.log("eventPeriodType",event.Selected.PeriodType)
    if(AccountLevel ==0){
     this.AccountLevelStatus=0
     console.log('AccountLevelStatus0',this.AccountLevelStatus)
    }
    else if(AccountLevel == 1){
      this.AccountLevelStatus=1
      console.log('AccountLevelStatus1',this.AccountLevelStatus)
     }
     else if(AccountLevel == 2){
      this.AccountLevelStatus=2
      console.log('AccountLevelStatus2',this.AccountLevelStatus)
     }
     else if(AccountLevel == 3){
      this.AccountLevelStatus=3
      console.log('AccountLevelStatus3',this.AccountLevelStatus)
     }
     else if(AccountLevel == 4){
      this.AccountLevelStatus=4
      console.log('AccountLevelStatus4',this.AccountLevelStatus)
     }
     if(PeriodType == 0){
      this.PeriodTypeStatus=0
      console.log('PeriodTypeStatus0',this.PeriodTypeStatus)
     }
     else if(PeriodType == 1){
      this.PeriodTypeStatus=1
      console.log('PeriodTypeStatus1',this.PeriodTypeStatus)
     }
     else if(PeriodType == 2){
      this.PeriodTypeStatus=2
      console.log('PeriodTypeStatus2',this.PeriodTypeStatus)
     }
     this.BudgetDataPatchValue(event)
  }

  public BudgetDataFillFunction(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.postData(SelectedPicklistData).subscribe(Budget => {

        this.form.patchValue(Budget);
        this.form.get('OUId').patchValue(Budget[0].OUId)
        this.form.get('OUName').patchValue(Budget[0].OUName)
        this.form.get('BudgetDataFromDate').patchValue(Budget[0].BudgetDataFromDate)
        this.form.get('BudgetDataToDate').patchValue(Budget[0].BudgetDataToDate)
        this.form.get('CostCenterName').patchValue(Budget[0].BudgetDataDetailArray[0].CostCenterName)
        this.form.get('BudgetDataDetailArray').patchValue(Budget[0].BudgetDataDetailArray)
        
      })
    }
  }

//   public GetDateformat(PicklistData:any){
//     console.log("PicklistData",PicklistData)
//     let FromDateEpoc = PicklistData[0].BudgetDataFromDate
//     let ToDateEpoc = PicklistData[0].BudgetDataFromDate

//     const timestamp = Number(FromDateEpoc.match(/\d+/)[0]);
//     const ToMilSec = Number(ToDateEpoc.match(/\d+/)[0]);
//     console.log("timestamp",timestamp)

//     const date = new Date(timestamp);
//     const date1 = new Date(ToMilSec);

//     const formattedDate = date.toLocaleDateString(); 
//     const ToDate = date1.toLocaleDateString()

//     console.log("formattedDate",formattedDate);
//     this.form.get('BudgetDataFromDate').patchValue(formattedDate)
//     this.form.get('BudgetDataToDate').patchValue(ToDate)
//     console.log("From", this.form.get('BudgetDataFromDate').patchValue(formattedDate))
//     return formattedDate
// //     let FromDateMil:any = FromDateEpoc.replace("/Date(", "").replace(")/", "");
// //     let ToDateMil: any = ToDateEpoc.replace("/Date(", "").replace(")/", "");
// //     let Today = new Date(FromDateMil)
// //     console.log("FromDateMil: ",FromDateMil,"ToDateMil: ",ToDateMil)
// //     console.log("Today",Today)
// //     const formattedDate = Today.toLocaleDateString();
// // console.log("formattedDate",formattedDate)
    
   
//   }


  public BudgetDataPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}  


export function getThisDBDataService(http: GBHttpService, url: string, saveurl:string, IdField:string): GBBaseDBDataService<IBudgetData > {
  const dbds: GBBaseDBDataService<IBudgetData > = new GBBaseDBDataService<IBudgetData >(http);
  dbds.endPoint = url;
  dbds.saveurl = saveurl;
  dbds.IdField = IdField
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IBudgetData >, dbDataService: GBBaseDBDataService<IBudgetData >, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IBudgetData > {
  return new GBDataPageService<IBudgetData >(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
