
import { Component, Inject, LOCALE_ID, OnInit } from '@angular/core';

import { Observable } from 'rxjs';
import { Select, Store } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { SharedService } from 'features/commonreport/service/datapassing.service';


@Component({
    selector: 'app-tbaccount',
    templateUrl: './tbaccount.component.html',
    styleUrls: ['./tbaccount.component.scss'],
})
export class tbaccountComponent implements OnInit {
    @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
    public rowData: any;
    tabledata;
    public TotalDebit;
    public TotalCredit;

    constructor(public store: Store, @Inject(LOCALE_ID) public locale: string,public sharedService: SharedService) { }
    ngOnInit(): void {
        this.gettbaccount();
    }

    public gettbaccount() {
        this.sharedService.getRowInfo().subscribe(data => {
            this.rowData = data;
            console.log("Trial Balance Account:", this.rowData);
            this.TotalDebit = 0;
            this.TotalCredit = 0;
            let json = this.rowData;
            var finalizedArray = [];
    
            for (let data of this.rowData) {
                this.TotalDebit = this.TotalDebit + data.Debit;
                this.TotalCredit = this.TotalCredit + data.Credit;
            }
    
            json.map((row) => {
                finalizedArray.push({
                    ascode: row['AccountScheduleCode'],
                    asname: row['AccountScheduleName'],
                    agcode: row['AccountGroupCode'],
                    agname: row['AccountGroupName'],
                    acccode: row['AccountCode'],
                    accname: row['AccountName'],
                    debit: row['Debit'],
                    credit: row['Credit']
                });
            });
    
            const final = {};
            finalizedArray.forEach((detail) => {
                final[detail.ascode] = {
                    accountGroups: {},
                    acccode: detail.ascode,
                    accname: detail.asname,
                    debit: detail.debit,
                    credit: detail.credit,
                    ...final[detail.ascode],
                };
    
                final[detail.ascode].accountGroups[detail.agcode] = {
                    accounts: {},
                    acccode: detail.agcode,
                    accname: detail.agname,
                    debit: detail.debit,
                    credit: detail.credit,
                    ...final[detail.ascode].accountGroups[detail.agcode],
                };
    
                final[detail.ascode].accountGroups[detail.agcode].accounts[detail.acccode] = {
                    acccode: detail.acccode,
                    accname: detail.accname,
                    debit: detail.debit,
                    credit: detail.credit,
                };
            });
    
            const grpcodes = Object.keys(final);
            const tableData = [];
    
            grpcodes.forEach((acccode) => {
                const accountGroups = Object.keys(final[acccode].accountGroups);
    
                // Initialize totals for the top-level account schedule
                let acccodeTotalDebit = 0;
                let acccodeTotalCredit = 0;
    
                accountGroups.forEach(ag => {
                    let sumdebit = 0;
                    let sumcredit = 0;
    
                    const accounts = Object.keys(final[acccode].accountGroups[ag].accounts);
                    accounts.forEach(account => {
                        sumdebit = sumdebit + parseFloat(final[acccode].accountGroups[ag].accounts[account].debit);
                        sumcredit = sumcredit + parseFloat(final[acccode].accountGroups[ag].accounts[account].credit);
                    });
    
                    final[acccode].accountGroups[ag].debit = sumdebit;
                    final[acccode].accountGroups[ag].credit = sumcredit;
    
                    // Update totals for the current account group
                    acccodeTotalDebit += sumdebit;
                    acccodeTotalCredit += sumcredit;
    
                    tableData.push({
                        acccode: final[acccode].accountGroups[ag].acccode,
                        accname: final[acccode].accountGroups[ag].accname,
                        debit: final[acccode].accountGroups[ag].debit,
                        credit: final[acccode].accountGroups[ag].credit,
                        bold: true,
                    });
    
    
                // Update totals for the current account schedule
                final[acccode].debit = acccodeTotalDebit;
                final[acccode].credit = acccodeTotalCredit;
    
                tableData.push({
                    acccode: final[acccode].acccode,
                    accname: final[acccode].accname,
                    debit: final[acccode].debit,
                    credit: final[acccode].credit,
                    bold: true,
                });

                accounts.forEach(account => {
                    tableData.push({
                        acccode: final[acccode].accountGroups[ag].accounts[account].acccode,
                        accname: final[acccode].accountGroups[ag].accounts[account].accname,
                        debit: final[acccode].accountGroups[ag].accounts[account].debit,
                        credit: final[acccode].accountGroups[ag].accounts[account].credit,
                    });
                });
            });
            });

            
    
            this.tabledata = tableData;
            console.log("Final Data:", this.rowData);
        });
    }    
}    