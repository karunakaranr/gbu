import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Select, Store } from '@ngxs/store';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { URLS } from 'features/erpmodules/finance/URLS/urls';
import { ChequeBookService } from '../../services/ChequeBook/ChequeBook.service';
import { IChequeBook } from '../../models/IChequeBook';
import * as ChequeBookJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/ChequeBook.json'
import { LayoutState } from 'features/layout/store/layout.state';
import { Observable } from 'rxjs';
@Component({
    selector: 'app-ChequeBook',
    templateUrl: './chequebook.component.html',
    styleUrls: ['./chequebook.component.scss'],
    providers: [
        { provide: 'IdField', useValue: 'ChequeBookId' },
        { provide: 'url', useValue: URLS.ChequeBook },
        { provide: 'DataService', useClass: ChequeBookService },
        { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
        { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
    ]
})
export class ChequeBookComponent extends GBBaseDataPageComponentWN<IChequeBook> {
    title: string = "ChequeBook"
    ChequeBookJSON = ChequeBookJSON
    ChequeBookDetailArray = [];

    form: GBDataFormGroupWN<IChequeBook> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "ChequeBook", {}, this.gbps.dataService);
    thisConstructor() {
        let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
        if (arrayOfValues != null) {
            this.ChequeBookPicklistFillValue(arrayOfValues.ChequeBookId)
            this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
        } else {
            this.store.dispatch(new ButtonVisibility("Forms"))
        }

    }

    generateChequeBookDetailArray(startNo: number, numOfLeaves: number, prefix: string): any[] {
        const initialArray = [];
        for (let i = 0; i < numOfLeaves; i++) {
            initialArray.push({
                ChequeBookDetailId: "0",
                ChequeBookSlNo: i + 1, // Sequential numbering from 1
                ChequeBookDetailChequeNo: prefix + (startNo + i), // Starting from 'startNo'
                ChequeBookDetailStatus: 0,
                ChequeBookDetailCancelReason: ""
            });
        }
        return initialArray;
    }

    loadChequeBookDetailArray() {
        const numOfLeaves = (this.form.get('ChequeBookNoOfLeaves').value);
        const startNo = (this.form.get('ChequeBookStartNo').value);
        const prefix = (this.form.get('ChequeBookChequePrefix').value)
        if (!isNaN(numOfLeaves) && numOfLeaves > 0 && !isNaN(startNo)) {
            this.ChequeBookDetailArray = this.generateChequeBookDetailArray(startNo, numOfLeaves, prefix);
            this.form.get('ChequeBookDetailArray').patchValue(this.ChequeBookDetailArray);
        } else {
            console.error("Invalid input.");
        }
    }
    public ChequeBookPicklistFillValue(SelectedPicklistData: string): void {

        if (SelectedPicklistData) {
            console.log("id", SelectedPicklistData)

            this.gbps.dataService.getData(SelectedPicklistData).subscribe(ChequeBook => {
                console.log("ChequeBook", ChequeBook)

                if (SelectedPicklistData == "-1") {
                    this.form.get('ChequeBookDetailArray').patchValue(this.ChequeBookDetailArray)
                } else {
                    this.form.patchValue(ChequeBook);
                }

            })
        }
    }

    public ChequeBookPicklistPatchValue(SelectedPicklistDatas: any): void {
        // console.log("id",SelectedPicklistDatas)
        this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
        // this.form.get('ChequeBookDetailArray').patchValue(this.ChequeBookDetailArray)

    }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IChequeBook> {
    const dbds: GBBaseDBDataService<IChequeBook> = new GBBaseDBDataService<IChequeBook>(http);
    dbds.endPoint = url;
    return dbds;
}


export function getThisPageService(store: Store, dataService: GBBaseDataService<IChequeBook>, dbDataService: GBBaseDBDataService<IChequeBook>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IChequeBook> {
    return new GBDataPageService<IChequeBook>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
