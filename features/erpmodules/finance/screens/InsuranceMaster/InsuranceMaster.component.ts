import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { IInsuranceMaster } from '../../models/IInsuranceMaster';
import { ButtonVisibility } from 'features/layout/store/layout.actions';

import * as InsuranceMasterjson from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/InsuranceMaster.json'
import { URLS } from '../../URLS/urls'
import { InsuranceMasterService } from '../../services/InsuranceMaster/InsuranceMaster.service';
import { DialogBoxComponent } from 'features/erpmodules/samplescreens/screens/DialogBox/DialogBox.component';

@Component({
  selector: "app-InsuranceMaster",
  templateUrl: "./InsuranceMaster.component.html",
  styleUrls: ["./InsuranceMaster.component.scss"],
  providers: [
    { provide: 'IdField', useValue: 'InsuranceId' },
    { provide: 'url', useValue: URLS.InsuranceMaster },
    { provide: 'DataService', useClass: InsuranceMasterService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class InsuranceMasterComponent extends GBBaseDataPageComponentWN<IInsuranceMaster> {
  title: string = "InsuranceMaster"
  InsuranceMasterjson = InsuranceMasterjson;



  form: GBDataFormGroupWN<IInsuranceMaster> = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'InsuranceMaster', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      console.log("arraysmass", arrayOfValues)
      this.InsuranceMasterFillFunction(arrayOfValues.InsuranceId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }



  public getDateFromEpochString(epochString: string): Date {

    const epochMilliseconds = Number(epochString.match(/\d+/)[0]);
    console.log("getDateFromEpochStringFunction", epochMilliseconds)
    return new Date(epochMilliseconds);

  }

  public calculateDateDifferences() {

    let Cfrom = this.form.get('InsuranceStartDate').value;
    let CToDate = this.form.get('InsuranceEndDate').value;

    const dateObject1 = this.getDateFromEpochString(Cfrom);
    const dateObject2 = this.getDateFromEpochString(CToDate);


    console.log(dateObject1, dateObject2, "dates");

    if (dateObject1 > dateObject2) {
      this.form.get('InsuranceStartDate').patchValue('today');
      console.log(this.form.get('InsuranceEndDate').value, "today")
      const dialogRef = this.dialog.open(DialogBoxComponent, {
        width: '400px',
        data: {
          message: 'Please Check the End days.',
          heading: 'Info',
        }
      });
    }
  }


  public setPatchValueToZero(value): void {
    console.log("this.form.get('InsuranceOUType').value:", this.form.get('InsuranceOUType').value)

    if (value == 0) {
      console.log("this.form.get('InsuranceOUType').value:", this.form.get('InsuranceOUType').value)
      this.form.get('OUGroupId').patchValue('-1');
      this.form.get('OUGroupName').patchValue('NONE');
      this.form.get('OUId').patchValue('-1');
      this.form.get('OUName').patchValue('NONE');
    }

    else if (value == 1) {
      this.form.get('OUId').patchValue('-1');
      this.form.get('OUName').patchValue('NONE');
    }
    else if (value == 2) {
      this.form.get('OUGroupId').patchValue('-1');
      this.form.get('OUGroupName').patchValue('NONE');
    }

  }

  public setPatchValueToZero2(value): void {

    if (value == 0 || value == 1) {

      this.form.get('AssetId').patchValue('-1');
      this.form.get('AssetName').patchValue('NONE');

      this.form.get('MachineId').patchValue('-1');
      this.form.get('MachineName').patchValue('NONE');

      this.form.get('EmployeeId').patchValue('-1');
      this.form.get('EmployeeName').patchValue('NONE');

      this.form.get('VehicleId').patchValue('-1');
      this.form.get('VehicleNumber').patchValue('NONE');
    }

    else if (value == 2) {
      this.form.get('AssetId').patchValue('-1');
      this.form.get('AssetName').patchValue('NONE');

      this.form.get('MachineId').patchValue('-1');
      this.form.get('MachineName').patchValue('NONE');

      this.form.get('EmployeeId').patchValue('-1');
      this.form.get('EmployeeName').patchValue('NONE');

      this.form.get('AssetId').patchValue('-1');
      this.form.get('AssetName').patchValue('NONE');
    }
    else if (value == 3) {
      this.form.get('EmployeeId').patchValue('-1');
      this.form.get('EmployeeName').patchValue('NONE');

      this.form.get('AssetId').patchValue('-1');
      this.form.get('AssetName').patchValue('NONE');

      this.form.get('VehicleId').patchValue('-1');
      this.form.get('VehicleNumber').patchValue('NONE');

      this.form.get('PartyId').patchValue('-1');
      this.form.get('PartyName').patchValue('NONE');
    }

    else if (value == 4) {
      this.form.get('EmployeeId').patchValue('-1');
      this.form.get('EmployeeName').patchValue('NONE');

      this.form.get('MachineId').patchValue('-1');
      this.form.get('MachineName').patchValue('NONE');

      this.form.get('VehicleId').patchValue('-1');
      this.form.get('VehicleNumber').patchValue('NONE');

      this.form.get('PartyId').patchValue('-1');
      this.form.get('PartyName').patchValue('NONE');
    }

    else if (value == 5) {
      this.form.get('AssetId').patchValue('-1');
      this.form.get('AssetName').patchValue('NONE');

      this.form.get('MachineId').patchValue('-1');
      this.form.get('MachineName').patchValue('NONE');

      this.form.get('VehicleId').patchValue('-1');
      this.form.get('VehicleNumber').patchValue('NONE');

      this.form.get('PartyId').patchValue('-1');
      this.form.get('PartyName').patchValue('NONE');
    }

  }


  public InsuranceMasterFillFunction(SelectedPicklistData: string): void {

    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(InsuranceMaster => {
        this.form.patchValue(InsuranceMaster);
      })
    }
  }

  public InsuranceMasterPatchValue(SelectedPicklistDatas: any): void {

    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
    console.log("iteem==>", SelectedPicklistDatas)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IInsuranceMaster> {
  const dbds: GBBaseDBDataService<IInsuranceMaster> = new GBBaseDBDataService<IInsuranceMaster>(http);
  dbds.endPoint = url;
  console.log("utry", url)
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IInsuranceMaster>, dbDataService: GBBaseDBDataService<IInsuranceMaster>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IInsuranceMaster> {
  return new GBDataPageService<IInsuranceMaster>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
