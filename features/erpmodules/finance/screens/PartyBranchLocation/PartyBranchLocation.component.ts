import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
// import { IPartyBranchLocation } from '../../models/IPartyBranchLocation';
// import { PartyBranchLocationService } from '../../services/PartyBranchLocation/PartyBranchLocation.service';
import { IPartyBranchLocation } from '../../models/IPartyBranchLocation';
import { PartyBranchLocationService } from '../../services/PartyBranchLocation/PartyBranchLocation.service';
import { ButtonVisibility } from 'features/layout/store/layout.actions';

// import * as PartyBranchLocationjson from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/PartyBranchLocation.json'
import * as PartyBranchLocationjson from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/PartyBranchLocation.json'

import { URLS } from './../../URLS/urls'

@Component({
  selector: "app-PartyBranchLocation",
  templateUrl: "./PartyBranchLocation.component.html",
  styleUrls: ["./PartyBranchLocation.component.scss"],
  providers: [
    { provide: 'IdField', useValue: 'PartyBranchLocationId' },
    { provide: 'url', useValue: URLS.PartyBranchLocation },
    { provide: 'DataService', useClass: PartyBranchLocationService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class PartyBranchLocationComponent extends GBBaseDataPageComponentWN<IPartyBranchLocation> {
  title: string = "PartyBranchLocation"
  PartyBranchLocationjson = PartyBranchLocationjson;



  form: GBDataFormGroupWN<IPartyBranchLocation> = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'PartyBranchLocation', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.PartyBranchLocationFillFunction(arrayOfValues.PartyBranchLocationId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }




  public PartyBranchLocationFillFunction(SelectedPicklistData: string): void {
   console.log("please show the data",SelectedPicklistData)
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(PartyBranchLocation => {
        this.form.patchValue(PartyBranchLocation);
      })
    }
    console.log(" show the data",SelectedPicklistData)
  }


  public PartyBranchLocationPatchValue(SelectedPicklistDatas: any): void {
    
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
    console.log("iteem==>",SelectedPicklistDatas)
  }
}




export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IPartyBranchLocation> {
  const dbds: GBBaseDBDataService<IPartyBranchLocation> = new GBBaseDBDataService<IPartyBranchLocation>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IPartyBranchLocation>, dbDataService: GBBaseDBDataService<IPartyBranchLocation>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IPartyBranchLocation> {
  return new GBDataPageService<IPartyBranchLocation>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
