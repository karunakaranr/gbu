import { HttpClient } from '@angular/common/http';
import { Component, Inject, LOCALE_ID, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Select, Store } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { SharedService } from 'features/commonreport/service/datapassing.service';


@Component({
  selector: 'app-brsregister',
  templateUrl: './brsregister.component.html',
  styleUrls: ['./brsregister.component.scss'],
})
export class brsregisterComponent implements OnInit {
  @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
  public rowData = [];
  public TotalAmount;
  constructor(public sharedService: SharedService,public store: Store) { }

  ngOnInit(): void {
    this.getbrsregister();
  }
  
  public getbrsregister() {
    this.sharedService.getRowInfo().subscribe(data => {
      if (data.length > 0) {
      this.rowData = data;
      console.log("BRS Register Data :", this.rowData)

      this.TotalAmount = 0;
      for (let data of this.rowData) {
        this.TotalAmount = this.TotalAmount + data.InstrumnetAmount;
    }
  }
    });
  }

}
