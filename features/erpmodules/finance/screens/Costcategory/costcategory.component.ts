import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
// import { ICostCategory } from '../../models/ICostCategory';
import { ICostCategory } from '../../models/ICostCategory';
import { CostCategoryService } from '../../services/Cost Category/CostCategory.service';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { IBaseField, IPickListDetail } from 'libs/uicore/src/lib/interfaces/ibasefield';

import { URLS } from './../../URLS/urls'
// const urls = require('./../../URLS/urls.json');

@Component({
  selector: 'app-CostCategory',
  templateUrl: './CostCategory.component.html',
  styleUrls: ['./CostCategory.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'CostCategoryId' },
    { provide: 'url', useValue: URLS.CostCategory },
    { provide: 'DataService', useClass: CostCategoryService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class CostCategoryComponent extends GBBaseDataPageComponentWN<ICostCategory> {
  title: string = "CostCategory"
  CostCategoryJSON: IBaseField
  JSONCalling = 0;
  form: GBDataFormGroupWN<ICostCategory> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "CostCategory", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.Getselectedid(arrayOfValues.CostCategoryId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.Getselectedid('0')
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }


  public FindPicklist(FieldName: string): IPickListDetail {
    if (this.CostCategoryJSON != undefined) {
      return this.CostCategoryJSON.ObjectFields.find((field: any) => field.Name === FieldName).FormPicklist
    } else {
      setTimeout(() => {
        this.FindPicklist(FieldName)
      }, 500)

    }
  }


  public Getselectedid(SelectedPicklistData: string): void {
    console.log("picklistersss",SelectedPicklistData)
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(CostCategory => {
       
        if (SelectedPicklistData != '0') {
          this.form.patchValue(CostCategory);
        }
        if (this.JSONCalling == 0) {
          this.localhttp.get('assets/FormJSONS/CostCategory.json').subscribe((CostCategoryJson: IBaseField) => {
            this.CostCategoryJSON = CostCategoryJson
          })
          this.JSONCalling = 1;
        }
      })
    }
    console.log("afterpicklistersss",SelectedPicklistData)
  }


  public Getselectedids(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<ICostCategory> {
  const dbds: GBBaseDBDataService<ICostCategory> = new GBBaseDBDataService<ICostCategory>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<ICostCategory>, dbDataService: GBBaseDBDataService<ICostCategory>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<ICostCategory> {
  return new GBDataPageService<ICostCategory>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
