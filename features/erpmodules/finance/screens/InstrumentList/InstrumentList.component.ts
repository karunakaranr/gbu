import { Component, OnInit } from '@angular/core';
import { InstrumentList } from '../../services/InstrumentList/InstrumentList.service';

@Component({
  selector: 'app-InstrumentList',
  templateUrl: './InstrumentList.component.html',
  styleUrls: ['./InstrumentList.component.scss']
})

export class InstrumentListComponent implements OnInit {
  title = 'Instrument List'
  public rowData = []

  constructor(public service: InstrumentList) { }
  ngOnInit(): void {
    this.instrumentlist();
  }
  public instrumentlist() {
    this.service.instrumentlistview().subscribe(menudetails => {
      let servicejsondetail = menudetails
      this.service.Rowservice(servicejsondetail).subscribe(res => {
        this.rowData = res.ReportDetail;
        for(let data of this.rowData){
            if (data.InstrumentType == 0) {
                data.InstrumentType = "CASH";
            }
            if (data.InstrumentType == 1) {
                data.InstrumentType = "CHEQUE";
            }
            if (data.InstrumentType == 2) {
                data.InstrumentType = "DD";
            }
            if (data.InstrumentType == 3) {
                data.InstrumentType = "EFT";
            }
            if (data.InstrumentType == 4) {
                data.InstrumentType = "GV";
            }
            if (data.InstrumentType == 5) {
                data.InstrumentType = "CC";
            }
            if (data.InstrumentType == 6) {
                data.InstrumentType = "DC";
            }
            if (data.InstrumentAccountPostType == 0) {
                data.InstrumentAccountPostType = "Immediate";
            }
            if (data.InstrumentAccountPostType == 1) {
                data.InstrumentAccountPostType = "OnRealisation";
            }
            if (data.InstrumentAccountPostType == 2) {
                data.InstrumentAccountPostType = "OnDepo";
            }
            if (data.InstrumentPostDatedType == 0) {
                data.InstrumentPostDatedType = "NotApplicable";
            }
            if (data.InstrumentPostDatedType == 1) {
                data.InstrumentPostDatedType = "Immediate";
            }
            if (data.InstrumentPostDatedType == 2) {
                data.InstrumentPostDatedType = "OnDepo";
            }
            if (data.InstrumentPostDatedType == 3) {
                data.InstrumentPostDatedType = "OnRealisation";
            }
            if (data.InstrumentStatusOfInstrument == 0) {
                data.InstrumentStatusOfInstrument = "Pending";
            }
            if (data.InstrumentStatusOfInstrument == 1) {
                data.InstrumentStatusOfInstrument = "Realisation";
            }
            if (data.InstrumentIsApplicableForBank == 0) {
                data.InstrumentIsApplicableForBank = "Yes";
            }
            if (data.InstrumentIsApplicableForBank == 1) {
                data.InstrumentIsApplicableForBank = "No";
            }
            if (data.InstrumentIsApplicableforCounterTransaction == 0) {
                data.InstrumentIsApplicableforCounterTransaction = "Yes";
            }
            if (data.InstrumentIsApplicableforCounterTransaction == 1) {
                data.InstrumentIsApplicableforCounterTransaction = "No";
            }
        }
        let json = this.rowData;
        console.log("dkd", res);
        var finalizedArray = []
        json.map(row => {
          finalizedArray.push({
            type: row['InstrumentType'],
            name: row['InstrumentName'],
            account: row['AccountName'],
            post: row['InstrumentAccountPostType'],
            postdate: row['InstrumentPostDatedType'],
            status: row['InstrumentStatusOfInstrument'],
            bank: row['InstrumentIsApplicableForBank'],
            counter: row['InstrumentIsApplicableforCounterTransaction'],
            day: row['InstrumentValidityPeriodInDays']
          })
        })

        const final = {};
        finalizedArray.forEach(detail => {
          final[detail.type] = {
            accountGroups :{},
            name:detail.type,
            account:"",
            post:"",
            postdate:"",
            status:"",
            bank:"",
            counter:"",
            day:"",
            ...final[detail.type], 
          }

          final[detail.type].accountGroups[detail.name] =  {
            name:detail.name,
            account:detail.account,
            post:detail.post,
            postdate:detail.postdate,
            status:detail.status,
            bank:detail.bank,
            counter:detail.counter,
            day:detail.day,
            ...final[detail.type].accountGroups[detail.name],
          }
        })


        const grpcodes = Object.keys(final)

        const tableData = []
        grpcodes.forEach(code => {
          const accountGroups = Object.keys(final[code].accountGroups)

          tableData.push({
            name: final[code].name,
            account:"",
            post:"",
            postdate:"",
            status:"",
            bank:"",
            counter:"",
            day:"",
            bold: true,
          })

          accountGroups.forEach(ag => {
            tableData.push({
              code: ag,
              name: final[code].accountGroups[ag].name,
              account:final[code].accountGroups[ag].account,
              post:final[code].accountGroups[ag].post,
              postdate:final[code].accountGroups[ag].postdate,
              status:final[code].accountGroups[ag].status,
              bank:final[code].accountGroups[ag].bank,
              counter:final[code].accountGroups[ag].counter,
              day:final[code].accountGroups[ag].day,
            })
          })
        })
        this.rowData = tableData;
  });
})

}

}