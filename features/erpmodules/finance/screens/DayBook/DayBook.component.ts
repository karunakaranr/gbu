
import { Component, Inject, LOCALE_ID, OnInit } from '@angular/core';

import { Observable } from 'rxjs';
import { Select, Store } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';


@Component({
    selector: 'app-daybook',
    templateUrl: './daybook.component.html',
    styleUrls: ['./daybook.component.scss'],
})
export class daybookComponent implements OnInit {
    @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
    public rowData: any;
    total: any[];
    showNarration: boolean = false; // Declare the showNarration property and initialize it
    constructor(public store: Store, @Inject(LOCALE_ID) public locale: string) { }
    ngOnInit(): void {
        this.getdaybook();
    }

    onShowNarrationChange() {
        console.log("showNarration changed:", this.showNarration);
        this.getdaybook(); // Ensure that getdaybook is called when showNarration changes
    }

    public getdaybook() {
        this.rowdatacommon$.subscribe(data => {
            this.rowData = data;
            console.log("Day Book Details :", this.rowData)


            let json = this.rowData;
            var finalizedArray = [];
            json.map((row, index) => {
                finalizedArray.push({
                    sno: index,
                    vdate: row['VoucherDate'],

                    vnarration: row['VoucherNarration'],

                    vno: row['VoucherNumber'],
                    bizcode: row['BizTransactionTypeCode'],
                    refno: row['ReferenceNumber'],
                    refdate: row['ReferenceDate'],
                    vaccno: row['VoucherDetailAccountName'],
                    debit: row['Debit'],
                    credit: row['Credit']
                });
                console.log("finalizedArray:",finalizedArray)
            });



            const final = {};
            finalizedArray.forEach((detail) => {
                final[detail.vdate] = {
                    accountGroups: {},
                    vno: detail.vdate,
                    bizcode: "",
                    refno: "",
                    refdate: "",
                    vaccno: "",
                    vnarration: detail.vnarration,
                    debit: 0,
                    credit: 0,
                    ...final[detail.vdate],
                };

            
                final[detail.vdate].accountGroups[detail.sno] = {
                    vno: detail.vno,
                    bizcode: detail.bizcode,
                    refno: detail.refno,
                    refdate: detail.refdate,
                    vaccno: detail.vaccno,
                    vnarration: detail.vnarration,
                    debit: detail.debit,
                    credit: detail.credit,
                };
            });
            console.log("Final:", final)
            const grpcodes = Object.keys(final);

            const tableData = [];
            grpcodes.forEach((vno) => {
                const accountGroups = Object.keys(final[vno].accountGroups);
                let sumdebit = 0;
                let sumcredit = 0;
                accountGroups.forEach(account => {
                    sumdebit = sumdebit + parseFloat(final[vno].accountGroups[account].debit);
                    sumcredit = sumcredit + parseFloat(final[vno].accountGroups[account].credit);
                })
                final[vno].debit = sumdebit;
                final[vno].credit = sumcredit;


                tableData.push({
                    vno: final[vno].vno,
                    bizcode: "",
                    refno: "",
                    refdate: "",
                    vaccno: "",
                    vnarration:"",
                    debit: final[vno].debit,
                    credit: final[vno].credit,
                    bold: true,
                });

        
                const accounts = Object.keys(final[vno].accountGroups);
                accounts.forEach((account) => {
                    tableData.push({
                        vno: final[vno].accountGroups[account].vno,
                        bizcode: final[vno].accountGroups[account].bizcode,
                        refno: final[vno].accountGroups[account].refno,
                        refdate: final[vno].accountGroups[account].refdate,
                        vaccno: final[vno].accountGroups[account].vaccno,
                        vnarration: final[vno].accountGroups[account].vnarration,
                        debit: final[vno].accountGroups[account].debit,
                        credit: final[vno].accountGroups[account].credit,
                    });
                });
            });
            this.total = tableData;

            console.log("Final Data:", this.total)
        });
    }
}


