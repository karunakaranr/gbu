import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { PartyOUService } from '../../services/PartyOU/partyou.service';
import { IPartyOU } from '../../models/IPartyOU';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as PartyOUJson from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/PartyOU.json'
import { URLS} from '../../URLS/urls';

@Component({
  selector: 'app-PartyOU',
  templateUrl: './partyou.component.html',
  styleUrls: ['./partyou.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'ItemOUId'},
    { provide: 'url', useValue: URLS.PartyOU},
    { provide: 'saveurl', useValue: URLS.PartyOUsave },
    { provide: 'deleteurl', useValue: URLS.PartyOUsave },
    { provide: 'DataService', useClass: PartyOUService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url','saveurl', 'deleteurl'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class PartyOUComponent extends GBBaseDataPageComponentWN<IPartyOU> {
  title: string = "PartyOU"
  PartyOUJson = PartyOUJson;


  form: GBDataFormGroupWN<IPartyOU> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "PartyOU", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.PartyOURetrivalValue(arrayOfValues.ItemOUId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }

 
  public PartyOURetrivalValue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.postData(SelectedPicklistData).subscribe(PartyOU => {
        this.form.patchValue(PartyOU[0]);
      })
    }
  }

  public PartyOUPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }

  public ChangeTab(event, TabName:string) :void{
    var i:number, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("FormTabLinks");
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(TabName).style.display = "block";
    console.log("Event:",event)
    event.currentTarget.className += " active";
  }
  
}

export function getThisDBDataService(http: GBHttpService, url: string,saveurl: string, deleteurl:string): GBBaseDBDataService<IPartyOU> {
  const dbds: GBBaseDBDataService<IPartyOU> = new GBBaseDBDataService<IPartyOU >(http);
  dbds.endPoint = url;
  dbds.saveurl = saveurl;
  dbds.deleteurl = deleteurl;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IPartyOU >, dbDataService: GBBaseDBDataService<IPartyOU>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IPartyOU> {
  return new GBDataPageService<IPartyOU>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
