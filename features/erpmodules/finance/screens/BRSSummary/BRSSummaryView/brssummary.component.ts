import { HttpClient } from '@angular/common/http';
import { Component, Inject, LOCALE_ID, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Select, Store } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';


@Component({
  selector: 'app-brssummary',
  templateUrl: './brssummary.component.html',
  styleUrls: ['./brssummary.component.scss'],
})
export class brssummaryComponent implements OnInit {
  @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
  public rowData = [];
  public TotalBalance;
  public TotalLessNotCr;
  public TotalAddNotDr;
  public TotalAcNotCr;
  public TotalAcNotDr;
  public TotalCalBalance;
  constructor(public store: Store) { }

  ngOnInit(): void {
    this.brssummary();
  }
  
  public brssummary() {
    this.rowdatacommon$.subscribe(data => {
      this.rowData = data;
      console.log("BRS Summary Data :", this.rowData)


      this.TotalBalance = 0;
      this.TotalLessNotCr = 0;
      this.TotalAddNotDr = 0;
      this.TotalAcNotCr = 0;
      this.TotalAcNotDr = 0;
      this.TotalCalBalance = 0;
    //   let json = this.rowData;
    //   var finalizedArray = [];
      for (let data of this.rowData) {
          this.TotalBalance = this.TotalBalance + data.BookBalance;
          this.TotalLessNotCr = this.TotalLessNotCr + data.LessNotcr;
          this.TotalAddNotDr = this.TotalAddNotDr + data.AddNotDr;
          this.TotalAcNotCr = this.TotalAcNotCr + data.NotAcNotCr;
          this.TotalAcNotDr = this.TotalAcNotDr + data.NotAcNotDr;
          this.TotalCalBalance = this.TotalCalBalance + data.CalculatedBalance;
      }
    });
  }

}
