import { HttpClient } from '@angular/common/http';
import { Component, Inject, LOCALE_ID, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Observable } from 'rxjs';
import { ISchedule } from 'features/erpmodules/finance/models/IScheduleList';
import { Select, Store } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { SharedService } from 'features/commonreport/service/datapassing.service';


@Component({
  selector: 'app-schedulelist',
  templateUrl: './schedulelist.component.html',
  styleUrls: ['./schedulelist.component.scss'],
})
export class schedulelistComponent implements OnInit {
  @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
  public rowData : any;  
  
  constructor(public sharedService: SharedService,public store: Store, @Inject(LOCALE_ID) public locale: string) { }
  ngOnInit(): void {
    this.getScheduleList();
  }
  
  public getScheduleList() {
    this.sharedService.getRowInfo().subscribe(data => {
      if (data.length > 0) {
      this.rowData = data;
      console.log("Schedule List :", this.rowData)

       let json = this.rowData;
      var finalizedArray = [];
      json.map((row) => {
        finalizedArray.push({
          accschname: row['AccountScheduleNatureName'],
          ascode: row['AccountScheduleCode'],
          asname: row['AccountScheduleName'],
        });
      });


      const final = {};
      finalizedArray.forEach((detail) => {
        final[detail.accschname] = {
          accountGroups: {},
          ascode: detail.accschname,
          asname: "",
          ...final[detail.accschname],
        };

        // final[detail.parname].accountGroups[detail.ouname] = {
        //   accounts: {},
        //   ascode: detail.parname,
        //   asname:  detail.ouname,
        //   igremarks: "",
        //   igselect: "",
        
        //   ...final[detail.parname].accountGroups[detail.ouname],
        // };
        final[detail.accschname].accountGroups[detail.ascode] = {
          ascode: detail.ascode,
          asname: detail.asname,
        };
      });

      const grpcodes = Object.keys(final);

      const tableData = [];
      grpcodes.forEach((ascode) => {
        const accountGroups = Object.keys(final[ascode].accountGroups);

        tableData.push({
          ascode: final[ascode].ascode,
          asname: "",
          bold: true,
        });

        // accountGroups.forEach((ag) => {
        //   tableData.push({
        //     ascode: final[ascode].ascode,
        //     // asname: final[ascode].accountGroups[ag].asname,
        //     bold: true,
        //   });

          const accounts = Object.keys(final[ascode].accountGroups);
          accounts.forEach((account) => {
            tableData.push({
              ascode: final[ascode].accountGroups[account].ascode,
              asname: final[ascode].accountGroups[account].asname,
            });
          });
        });
        this.rowData=tableData;
      
      console.log("Final Data:",this.rowData[0])
    }
    });
    }
  }


