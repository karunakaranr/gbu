import { Component, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { EbankService } from 'features/erpmodules/finance/services/E-Bank/ebank.service';
import { IEBank } from 'features/erpmodules/finance/models/IEbank';
import { ButtonVisibility, FormEditable } from 'features/layout/store/layout.actions';
import * as EBankJSON from './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/EBank.json';
import { DatePipe } from '@angular/common';
import { URLS } from 'features/erpmodules/finance/URLS/urls';
import { DialogBoxComponent } from 'features/erpmodules/samplescreens/screens/DialogBox/DialogBox.component';
import { stringify } from '@angular/compiler/src/util';




@Component({
  selector: 'app-ebank',
  templateUrl: './ebank.component.html',
  styleUrls: ['./ebank.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'AccountId' },
    { provide: 'url', useValue: URLS.Account },
    { provide: 'DataService', useClass: EbankService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService ,'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class ebankComponent extends GBBaseDataPageComponentWN<IEBank> {

  @ViewChild('input1') input1: ElementRef;
  @ViewChild('input2') input2: ElementRef;
  @ViewChild('input3') input3: ElementRef;
  @ViewChild('input4') input4: ElementRef;
  @ViewChild('input5') input5: ElementRef;
  @ViewChild('input6') input6: ElementRef;

  showAccountDetails: boolean = false; // New property to control the display of AccountDetails
  showDiv: boolean = false; // Initial state of the div (hidden)
  showBalance: boolean = false;
  title: string = "E-Bank"
  EBankJSON = EBankJSON
  accountbalance: number;
  form: GBDataFormGroupWN<IEBank> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "EBank", {}, this.gbps.dataService);
  public dateandtime: string;
  currentDate: Date;
  bankdetails: IEBank[];

  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    this.store.dispatch(new FormEditable);
    if (arrayOfValues != null) {
      this.EBankFillData(arrayOfValues.FormId)
      this.store.dispatch(new ButtonVisibility(" "))
    } else {
      this.store.dispatch(new ButtonVisibility(" "))
    }
  }


  ngDoCheck() {

    let accountCodeFilled;
    if(this.form.get('AccountCode')?.value.lookupdata==null){
      accountCodeFilled = this.form.get('AccountCode')?.value.lookupdata;
    } else if(this.form.get('AccountCode')?.value.lookupdata==undefined ){
      accountCodeFilled = this.form.get('AccountCode')?.value;
    } else {
      accountCodeFilled = this.form.get('AccountCode')?.value.lookupdata;
    }

    let secretKeyFilled = this.form.get('SecretKey')?.value;

    // Update showAccountDetails based on some condition, e.g., both fields are filled
    console.log("Condition:",accountCodeFilled)
    // this.showAccountDetails = accountCodeFilled && secretKeyFilled;

    console.log("Account Code Fill:",accountCodeFilled)

    if (( accountCodeFilled == null || accountCodeFilled == "" ) || (secretKeyFilled == "")) {
      this.showDiv = false; // Set showDiv to true to display the balance table
      console.log("Show Div", this.showDiv);
    } else {
      this.showDiv = true; // Set showDiv to true to display the balance table
      console.log("Show Div", this.showDiv);
    }
  }



public getfillcheck(event: Event, template: any) {
 
  let accountCodeFilled;
  console.log("AccountCode:",this.form.get('AccountCode')?.value.lookupdata==null)
  if(this.form.get('AccountCode')?.value.lookupdata==null){
    accountCodeFilled = this.form.get('AccountCode')?.value.lookupdata;
  } else if(this.form.get('AccountCode')?.value.lookupdata==undefined ){
    accountCodeFilled = this.form.get('AccountCode')?.value;
  } else {
    accountCodeFilled = this.form.get('AccountCode')?.value.lookupdata;
  }
  
  console.log("Account Code:",accountCodeFilled)

  let secretKeyFilled = this.form.get('SecretKey')?.value;

  console.log("Secret Key:",secretKeyFilled)
  console.log("Condition1:",(accountCodeFilled == null) && (secretKeyFilled == ""))
    // Check if both fields are filled before allowing other operations
    if (( accountCodeFilled == null || accountCodeFilled == "" ) && (secretKeyFilled == "")) {
      const dialogRef = this.dialog.open(DialogBoxComponent, {
        width: '400px',
        data: {
          message: "Please Fill Account Number and Secret Key",
          heading: 'Error'
        }
      })
      
    } else if(accountCodeFilled  == null) {
      const dialogRef = this.dialog.open(DialogBoxComponent, {
        width: '400px',
        data: {
          message: "Please Fill Account Number",
          heading: 'Error'
        }
      })
      console.log("2nd condition")
    } else if(secretKeyFilled === ""){
      const dialogRef = this.dialog.open(DialogBoxComponent, {
        width: '400px',
        data: {
          message: "Please Fill Secret Key",
          heading: 'Error'
        }
      })
      console.log("3rd condition")
    } else if (!!(accountCodeFilled!=null && secretKeyFilled)){
      
      this.openotpdailogbox(template); 
    }
}



getCurrentDateTime() {
  this.currentDate = new Date();
  console.log("Current Date and Time:", this.currentDate);
}


moveToNext(event: any, index: number) {
  const inputLength = event.target.value.length;
  if (inputLength === 1) {
    switch (index) {
      case 1:
        this.input2.nativeElement.focus();
        break;
      case 2:
        this.input3.nativeElement.focus();
        break;
      case 3:
        this.input4.nativeElement.focus();
        break;
      case 4:
        this.input5.nativeElement.focus();
        break;
      case 5:
        this.input6.nativeElement.focus();
        break;
      default:
        // Handle the last input here
        break;
    }
    setTimeout(() => {
      event.target.value = '•';
    }, 500);
  }
}


handleBackspace(event: any, index: number) {
  if (event.keyCode === 8 && event.target.value === '') {
    switch (index) {
      case 2:
        this.input1.nativeElement.focus();
        this.input1.nativeElement.value = '';
        break;
      case 3:
        this.input2.nativeElement.focus();
        this.input2.nativeElement.value = '';
        break;
      case 4:
        this.input3.nativeElement.focus();
        this.input3.nativeElement.value = '';
        break;
      case 5:
        this.input4.nativeElement.focus();
        this.input4.nativeElement.value = '';
        break;
      case 6:
        this.input5.nativeElement.focus();
        this.input5.nativeElement.value = '';
        break;
      default:
        // Handle the first input here
        break;
    }
  }
}
  public EBankFillData(SelectedPicklistData): void {
    console.log("nf jn vvjkj ",SelectedPicklistData , )
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(FormVariable => {
        this.bankdetails = [FormVariable];
        console.log("Bank Details",this.bankdetails[0])
        this.form.patchValue(FormVariable[0]);
        console.log("Form Value:",this.form.value)
      })
    }
  }


  public openotpdailogbox(templateRef){
    let dialogRef = this.dialog.open(templateRef, {
      width: '450px',
      height: '220px',
    });
  }

  public closeotpdailogbox(templateRef){
    this.dialog.closeAll();
  }

  clearInputs() {
    this.input1.nativeElement.value = '';
    this.input2.nativeElement.value = '';
    this.input3.nativeElement.value = '';
    this.input4.nativeElement.value = '';
    this.input5.nativeElement.value = '';
    this.input6.nativeElement.value = '';
  }
  
  validateInput(event: any) {
    const keyCode = event.keyCode;
    if ((keyCode < 48 || keyCode > 57) && keyCode !== 8) {
      event.preventDefault();
    }
  }

  public ebankservice() {
    this.service.getcheckbalance().subscribe(res =>{
      this.accountbalance = res.GetAccountBalanceResponse.GetAccountBalanceResponseBody.data.Balance;
      console.log("Check Balance:",res.GetAccountBalanceResponse.GetAccountBalanceResponseBody.data.Balance)
      this.showBalance = true;
      console.log("Show Balance:",this.showBalance)
      this.dialog.closeAll();
      this.getCurrentDateTime();
    })
  }

  public Getselectedids(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IEBank> {
  const dbds: GBBaseDBDataService<IEBank> = new GBBaseDBDataService<IEBank>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IEBank>, dbDataService: GBBaseDBDataService<IEBank>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IEBank> {
  return new GBDataPageService<IEBank>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
