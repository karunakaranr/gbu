import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';

import {GBBaseDataPageStoreComponentWN} from './../../../../../libs/uicore/src/lib/components/Base/GBBasePage';

import { PartyWithBranch } from './../../models/IPartyWithBranch';
import {
  getThisGBPageService,
} from './PartyWithBranch.factory';
import {
  PartyWithBranchFormgroupStore,
} from './PartyWithBranch.formgroups';
import { PartyWithBranchService } from './../../services/PartyWithBranch/PartyWithBranch.service';
import { PartyWithBranchDBService } from './../../dbservices/PartyWithBranch/PartyWithBranchDB.service';
import { Select, Store } from '@ngxs/store';
import { GBHttpService } from './../../../../../libs/gbcommon/src/lib/services/HTTPService/GBHttp.service';
import { PartyWithBranchState } from './../../stores/PartyWithBranch/PartyWithBranch.state';
import { GetAll, GetData, SaveData } from '../../stores/PartyWithBranch/PartyWithBranch.action';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { UOMState } from 'features/erpmodules/inventory/stores/UOM/UOM.state';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-PartyWithBranch',
  templateUrl: './PartyWithBranch.component.html',
  styleUrls: ['./PartyWithBranch.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'idfield' },
    { provide: 'url', useValue: 'url' },
    { provide: 'Store', useClass: Store },
    {provide: 'DBDataService',useClass: PartyWithBranchDBService,deps: [GBHttpService],},
    { provide: 'DataService', useClass: PartyWithBranchService, deps: ['DBDataService'] },
    {provide: 'PageService',useFactory: getThisGBPageService,deps: [Store,'DataService','DBDataService',
    FormBuilder,GBHttpService,ActivatedRoute,Router,],},],
})
export class PartyWithBranchComponent extends GBBaseDataPageStoreComponentWN<PartyWithBranch> implements OnInit {
  title = 'PartyWithBranch';
  selection;
  dataLoading;
  isnoteditable=false;
  form: PartyWithBranchFormgroupStore = new PartyWithBranchFormgroupStore(                                
    this.gbps.gbhttp.http,                                                        
    this.gbps.store                                                              
  );     
  @Select(UOMState.FormEditable) formedit$: Observable<any>;                                
  @Select(PartyWithBranchState) currentData$;
  Picklistvalues;
  Picklistvalues1;
  thisConstructor() { }
  loadScreen(recid?: string) {
    if (recid) {
      const id = parseInt(recid);
      this.gbps.store.dispatch(new GetData(id));
    }
    else {
      this.gbps.store.dispatch(new GetAll(null)).subscribe((res) => {
        console.log(" this.Picklistvalues:"+ JSON.stringify(res))
        this.Picklistvalues = res.PartyWithBranch.data;
        this.Picklistvalues1 = res.PartyWithBranch.data;
        this.gbps.store.dispatch(new SaveData(null));
      });
    }
  }

  ngOnInit(): void {
    let id = '';
    this.gbps.activeroute.params.subscribe((params: any) => {
      id = params['id'];
    });
    this.gbps.activeroute.paramMap.subscribe((params: ParamMap) => {
      id = params.get('id')
    });
    if (this.gbps.activeroute.firstChild) {
      this.gbps.activeroute.firstChild.params.subscribe((params: ParamMap) => {
        id = params['id'];
      });
    }
    this.loadScreen(id);
    this.formedit$.subscribe(id => {
      if(id){
        console.log("Yukesh",id)
        this.ChangeEdit()
      }
    })
  }
  public changeFn(val) {
    console.log("Dropdown selection:", val);
    for(let data of this.Picklistvalues){
      if(data.Code == val){
        console.log(data.Id)
        this.loadScreen(data.Id);
      }
    }
}
public changeFn1(val) {
  console.log("Dropdown selection:", val);
  for (let data of this.Picklistvalues1) {
    if (data.Name == val) {
      console.log(data.Id)
      this.loadScreen(data.Id);
    }
  }
}

public ChangeEdit(){
  this.isnoteditable=!this.isnoteditable;
}

}


