import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { IBudgetMaster } from '../../models/IBudgetMaster';
import { BudgetMasterService } from '../../services/BudgetMaster/BudgetMaster.service';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as BudgetMasterJson from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/BudgetMaster.json'
import { URLS } from '../../URLS/urls';
import { DialogBoxComponent } from 'features/erpmodules/samplescreens/screens/DialogBox/DialogBox.component';
// import { today } from 'libs/ng-bre/src/lib/functions/customfunctions';


@Component({
  selector: "app-BudgetMaster",
  templateUrl: "./BudgetMaster.component.html",
  styleUrls: ["./BudgetMaster.component.scss"],
  providers: [
    { provide: 'IdField', useValue: 'BudgetMasterId' },
    { provide: 'url', useValue: URLS.BudgetMaster },
    { provide: 'DataService', useClass: BudgetMasterService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class BudgetMasterComponent extends GBBaseDataPageComponentWN<IBudgetMaster> {
  title: string = "BudgetMaster"
  BudgetMasterJson = BudgetMasterJson;



  form: GBDataFormGroupWN<IBudgetMaster> = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'BudgetMaster', {}, this.gbps.dataService);
  Enddate: any;
  Startdate: any;
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.BudgetMasterFillFunction(arrayOfValues.BudgetMasterId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }
  public setPatchValueToZero(value): void {
    console.log("this.form.get('BudgetAccountLevel').value:", this.form.get('BudgetAccountLevel').value)

    if (value == 0 || value == 2) {
      console.log('BudgetAccountLevel value set to 0');

      this.form.get('CostCategoryId').patchValue('-1');
      this.form.get('CostCategoryName').patchValue('NONE');

    }

    console.log("Form Detail:", this.form)
  }



  public getDateFromEpochString(epochString: string): Date {

    const epochMilliseconds = Number(epochString.match(/\d+/)[0]);
    console.log("getDateFromEpochStringFunction", epochMilliseconds)
    return new Date(epochMilliseconds);

  }


  public calculateDateDifference() {

    let Tfrom = this.form.get('BudgetFromDate').value;
    let TToDate = this.form.get('BudgetToDate').value;

    const dateObject1 = this.getDateFromEpochString(Tfrom);
    const dateObject2 = this.getDateFromEpochString(TToDate);



    if (dateObject1 > dateObject2) {
      this.form.get('BudgetFromDate').patchValue('today');

      const dialogRef = this.dialog.open(DialogBoxComponent, {
        width: '400px',
        data: {
          message: 'Please Check the End days.',
          heading: 'Info',
        }
      });
    }
  }


  public calculateDateDifferences() {

    let Cfrom = this.form.get('BudgetCompareFromDate').value;
    let CToDate = this.form.get('BudgetCompareToDate').value;

    const dateObject1 = this.getDateFromEpochString(Cfrom);
    const dateObject2 = this.getDateFromEpochString(CToDate);


    console.log(dateObject1, dateObject2, "dates");

    if (dateObject1 > dateObject2) {
      this.form.get('BudgetCompareFromDate').patchValue('today');

      const dialogRef = this.dialog.open(DialogBoxComponent, {
        width: '400px',
        data: {
          message: 'Please Check the End days.',
          heading: 'Info',
        }
      });
    }
  }



  public setPatchValueToZero2(): void {
    console.log("this.form.get('BudgetPeriodType').value:", this.form.get('BudgetPeriodType').value)

    // var date = new
    if (this.form.get('BudgetPeriodType').value == 0 || this.form.get('BudgetPeriodType').value == 1) {
      console.log('BudgetPeriodType value set to 1');

      this.form.get('GBPeriodId').patchValue('-1');
      this.form.get('GBPeriodName').patchValue('NONE');
      this.form.get('CompareGBPeriodId').patchValue('-1');
      this.form.get('CompareGBPeriodName').patchValue('NONE');

    }
    else if (this.form.get('BudgetPeriodType').value == 0 || this.form.get('BudgetPeriodType').value == 2) {
      console.log('BudgetPeriodType value set to 0 or 2');

      this.form.get('BudgetFromDate').patchValue('today');
      this.form.get('BudgetToDate').patchValue('today');
      this.form.get('BudgetCompareFromDate').patchValue('today');
      this.form.get('BudgetCompareToDate').patchValue('today');

    }

    console.log("Form Detail:", this.form)
  }



  public BudgetMasterFillFunction(SelectedPicklistData: string): void {

    console.log("filling", SelectedPicklistData)
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(BudgetMaster => {
        this.form.patchValue(BudgetMaster);
      })
    }
  }


  public BudgetMasterPatchValue(SelectedPicklistDatas: any): void {
    console.log("patching", SelectedPicklistDatas)
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)

  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IBudgetMaster> {
  const dbds: GBBaseDBDataService<IBudgetMaster> = new GBBaseDBDataService<IBudgetMaster>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IBudgetMaster>, dbDataService: GBBaseDBDataService<IBudgetMaster>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IBudgetMaster> {
  return new GBDataPageService<IBudgetMaster>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
