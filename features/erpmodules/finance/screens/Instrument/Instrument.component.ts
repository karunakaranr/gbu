import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Select, Store } from '@ngxs/store';
import { InstrumentService } from './../../services/Instrument/Instrument.service';
import { IInstrument } from '../../models/IInstrument';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { Observable } from 'rxjs';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { URLS } from '../../URLS/urls'
import * as InstrumentJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/Instrument.json';
import { DialogBoxComponent } from 'features/erpmodules/samplescreens/screens/DialogBox/DialogBox.component';
@Component({
  selector: 'app-Instrument',
  templateUrl: './Instrument.component.html',
  styleUrls: ['./Instrument.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'InstrumentId' },
    { provide: 'url', useValue: URLS.Instrument },
    { provide: 'DataService', useClass: InstrumentService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class InstrumentComponent extends GBBaseDataPageComponentWN<IInstrument> {
  title : string = 'Instrument'
  InstrumentJSON = InstrumentJSON
  form: GBDataFormGroupWN<IInstrument> = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'Instrument', {}, this.gbps.dataService);
  @Select(ReportState.SelectedformViewType) selectedview$: Observable<any>;

  thisConstructor() {    
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))

    this.selectedview$.subscribe(data => {
      console.log("Change:",data)
      if(data != null){
        if(data == "Instrumentmasterform1"){
          document.getElementById("Instrumentform1").style.display = "block";
          document.getElementById("Instrumentform2").style.display = "none";
        }
        else if(data == "Instrumentmasterform2"){
          document.getElementById("Instrumentform1").style.display = "none";
          document.getElementById("Instrumentform2").style.display = "block"
        }
      }
    })

    if(arrayOfValues != null){
      this.InstrumentFillData(arrayOfValues.InstrumentId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
   }

  public InstrumentFillData(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(Instrument => {
          this.form.patchValue(Instrument);
          if(typeof Instrument == 'string'){
            this.dialog.open(DialogBoxComponent, {
              width: '600px',
              data: {
                message: Instrument,
                heading: 'Error',
              }
            })
          }
      })
    }
  }

  public PicklistPatchValue(SelectedPicklistDatas: any): void { // Picklist Emit Function
    console.log("SelectedPicklistDatas:",SelectedPicklistDatas)
    let idsplit= SelectedPicklistDatas.id.split(',')
    if(idsplit.length==1){
      this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
    } else {
      this.form.get(idsplit[0]).value[idsplit[1]] = SelectedPicklistDatas.SelectedData
      this.form.patchValue(this.form.value)
    }
  }
  public Checkboxoutput(Checkbox){
    console.log("Checkbox:",Checkbox)
  }

  public RadioButtonOutPut(Radiobutton){
    console.log("Radiobutton:",Radiobutton)
  }

  public ComboBoxOutPut(ComboBox){
    console.log("ComboBox:",ComboBox)
  }

  public InputOutPut(Input){
    console.log("Input:",Input)
  }
}

export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IInstrument> {
  const dbds: GBBaseDBDataService<IInstrument> = new GBBaseDBDataService<IInstrument>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IInstrument>, dbDataService: GBBaseDBDataService<IInstrument>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IInstrument> {
  return new GBDataPageService<IInstrument>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}



// import { Component, OnInit } from '@angular/core';
// import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

// import { InstrumentService } from './../../services/Instrument/Instrument.service';

// import { Select, Store } from '@ngxs/store';

// import { ActivatedRoute } from '@angular/router';
// import { Observable } from 'rxjs/internal/Observable'
// import { LayoutState } from 'features/layout/store/layout.state';
// import { ReportState } from 'features/commonreport/datastore/commonreport.state';
// import { FormValidationvalue } from 'features/layout/store/layout.actions';
// import { MatDialog } from '@angular/material/dialog';
// import { DialogBoxComponent } from 'features/erpmodules/samplescreens/screens/DialogBox/DialogBox.component';
// @Component({
//   selector: 'app- Instrument',
//   templateUrl: './Instrument.component.html',
//   styleUrls: ['./Instrument.component.scss'],
// })
// export class InstrumentComponent implements OnInit {
//   @Select(LayoutState.CurrentTheme) currenttheme: any;
//   @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
//   @Select(LayoutState.Formeditabe) formedit$: Observable<any>;
//   @Select(LayoutState.Movenxtpreid) movenextpreviousid$: Observable<any>;
//   @Select(LayoutState.FormEdit) edit$: Observable<boolean>;
//   @Select(LayoutState.Resetformvalue) restvalue$: Observable<any>;
//   title = ' Instrument';
//   searchTerm: string = '';
//   searchOnEnter: boolean = false;
//   selection: number;
//   selectionCode: string;
//   selectionName: any = [];
//   OUselection: string;
//   OUGroupselection: string;
//   selection1;
//   DataType;
//   TypeofInfo;
//   PostDatedType;
//   StatusOfInstrument;
//   SelectedCriteriaCheck;
//   dataLoading;
//   isnoteditable : boolean = true;
//   form: FormGroup
//   Listrowselectedindex: number
//   InstrumentNamePicklist;
//   AccountPicklistvalue;
//   OUPicklist;
//   OUGroupPicklist;
//   resetdata:any;
//   TypeofInfoPicklist = [
//     {
//       key: 0,
//       value: "Immediate",
//     },
//     {
//       key: 1,
//       value: "On Realisation"
//     },
//     {
//       key: 2,
//       value: "On Deposit"
//     },
//   ];
//   PostDatedTypePicklist = [
//     {
//       key: 0,
//       value: "Not Applicable",
//     },
//     {
//       key: 1,
//       value: "Immediate"
//     },
//     {
//       key: 2,
//       value: "On Deposit"
//     },
//     {
//       key: 3,
//       value: "On Realisation"
//     },
//   ];
//   StatusOfInstrumentPicklist = [
//     {
//       key: 0,
//       value: "Pending",
//     },
//     {
//       key: 1,
//       value: "Realisation"
//     },

//   ];
//   DataTypePicklists = [
//     {
//       key: 0,
//       value: "Cash",
//     },
//     {
//       key: 1,
//       value: "Cheque"
//     },
//     {
//       key: 2,
//       value: "Demand Draft"
//     },
//     {
//       key: 3,
//       value: "EFT (Electronic Transfer)"
//     },
//     {
//       key: 4,
//       value: "Gift Voucher"
//     },
//     {
//       key: 5,
//       value: "Credit Card"
//     },
//     {
//       key: 6,
//       value: "Debit Card"
//     },
//   ];
//   dateTime: Date;
//   Createdate: string;
//   constructor(private dialog: MatDialog, public activeroute: ActivatedRoute, public service: InstrumentService, public fb: FormBuilder,public store: Store) { }

//   ngOnInit(): void {
//     this.dateTime = new Date();
//     this.Createdate = "/Date("+this.dateTime.getTime()+")/"
//     this.edit$.subscribe(res => {
//       this.isnoteditable = res;
//     })

//     this.form = this.fb.group({
//       InstrumentId: new FormControl(0),
//       InstrumentName: new FormControl(),
//       InstrumentType: new FormControl(0),
//       InstrumentAccountPostType: new FormControl(0),
//       InstrumentPostDatedType: new FormControl(0),
//       InstrumentValidityPeriodInDays: new FormControl(0),
//       InstrumentStatusOfInstrument: new FormControl(0),
//       InstrumentIsApplicableForBank: new FormControl(1),
//       InstrumentIsApplicableforCounterTransaction: new FormControl(1),
//       InstrumentVersion: new FormControl(1),
//       InstrumentStatus: new FormControl(1),
//       AccountId: new FormControl(),
//       AccountName: new FormControl(),
//       InstrumentCreatedByName: new FormControl('ADMIN'),
//       InstrumentCreatedOn: new FormControl(this.Createdate),
//       InstrumentModifiedByName: new FormControl('ADMIN'),
//       InstrumentModifiedOn: new FormControl(this.Createdate),
//       InstrumentOuIds: new FormControl(),
//       InstrumentOuNames: new FormControl(),
//       InstrumentOuGroupIds: new FormControl(),
//       InstrumentOuGroupNames: new FormControl()
//     });
//     let Validation = [
//       {
//       InstrumentName : "InstrumentName", 
//       AccountName: "AccountName", 
//       InstrumentValidityPeriodInDays : "ValidityDays"
//     }
//   ]
//   this.resetdata = this.form.value
//     let FormActionParameter = [];
//     FormActionParameter.push({"Validation" : Validation})
//     FormActionParameter.push({"Reset" : this.form.value})
//     console.log("FormActionParameter:",FormActionParameter)
//     let id = '';
//     const listid = this.activeroute.snapshot.queryParamMap.get('Id');
//     const listrowindex = this.activeroute.snapshot.queryParamMap.get('RowIndexselected');
//     let arrayOfValues = JSON.parse(listid);
//     this.Listrowselectedindex = JSON.parse(listrowindex);
//     id = arrayOfValues;
//     this.store.dispatch(new FormValidationvalue(FormActionParameter));
//     this.loadScreen(id);
//     this.movenextandpre()
//     this.readOutputValueEmitted(id);
//   }

  
//   loadScreen(id) {
//     this.service.getall().subscribe(res => {
//       let data = res
//       data = data.map(({ Id, Name }) => ({ InstrumentId: Id, InstrumentName: Name }));
//       this.InstrumentNamePicklist = data;
//     })
//     this.service.getaccount().subscribe(res => {
//       console.log("Instrument:",res)
//       let data = res
//       data = data.map(({ Id, Code, Name }) => ({ InstrumentAccountId: Id, InstrumentAccountCode: Code, InstrumentAccountName: Name }));
//       console.log("Instrument:",data)
//       data.unshift({ InstrumentAccountId : '', InstrumentAccountCode: '', InstrumentAccountName: ''})
//       this.AccountPicklistvalue = data;
//       console.log("Instrument:",this.AccountPicklistvalue)
//     })
//     this.service.getOU().subscribe(res => {
//       let data = res
//       data = data.map(({ Id, Name }) => ({ InstrumentOuIds: Id, InstrumentOuNames: Name }));
//       this.OUPicklist = data;
//     })
//     this.service.getOUGroup().subscribe(res => {
//       let data = res
//       data = data.map(({ Id, Name }) => ({ InstrumentOuGroupIds: Id, InstrumentOuGroupNames: Name }));
//       this.OUGroupPicklist = data;
//     })
//     if (id) {
//       this.service.getid(id).subscribe(res => {
//         console.log("Instrument Selected Data:", res.InstrumentId)
//         if(res.InstrumentId != undefined){
//           this.form = this.fb.group({
//             InstrumentId: [res.InstrumentId],
//             InstrumentName: [res.InstrumentName],
//             InstrumentType: [res.InstrumentType],
//             InstrumentAccountPostType: [res.InstrumentAccountPostType],
//             InstrumentPostDatedType: [res.InstrumentPostDatedType],
//             InstrumentValidityPeriodInDays: [res.InstrumentValidityPeriodInDays],
//             InstrumentStatusOfInstrument: [res.InstrumentStatusOfInstrument],
//             InstrumentIsApplicableForBank: [res.InstrumentIsApplicableForBank],
//             InstrumentIsApplicableforCounterTransaction: [res.InstrumentIsApplicableforCounterTransaction],
//             InstrumentVersion: [res.InstrumentVersion],
//             InstrumentStatus: [res.InstrumentStatus],
//             AccountId: [res.AccountId],
//             AccountName: [res.AccountName],
//             InstrumentCreatedByName: [res.InstrumentCreatedByName],
//             InstrumentCreatedOn: [res.InstrumentCreatedOn],
//             InstrumentModifiedByName: [res.InstrumentModifiedByName],
//             InstrumentModifiedOn: [res.InstrumentModifiedOn],
//             InstrumentOuIds: [res.InstrumentOuIds],
//             InstrumentOuNames: [res.InstrumentOuNames],
//             InstrumentOuGroupIds: [res.InstrumentOuGroupIds],
//             InstrumentOuGroupNames: [res.InstrumentOuGroupNames]
//           }) as FormGroup;
//           let ousplit = res.InstrumentOuNames.split(','); // For Multiple Picklist
//           let ougroupsplit = res.InstrumentOuGroupNames.split(',');
//           let oulist = [];
//           let ougrouplist = [];
//           for (let ou of ousplit) {
//             if (this.OUPicklist.find(t => t.InstrumentOuNames == ou)) {
//               oulist.push(ou)
//             }

//           }
//           for (let ougroup of ougroupsplit) {
//             if (this.OUGroupPicklist.find(t => t.InstrumentOuGroupNames == ougroup)) {
//               ougrouplist.push(ougroup)
//             }
//           }
//           this.form.get('InstrumentOuNames').patchValue(oulist);
//           this.form.get('InstrumentOuGroupNames').patchValue(ougrouplist);
//         }
//         else{
//           const dialogRef = this.dialog.open(DialogBoxComponent, {
//             data: {
//               message: res,
//               heading: 'Error',
//               // button:'Ok'
//               // Add any other properties you want to pass
//             }
//           });
//           // alert(JSON.stringify(res))
//           this.form.reset(this.resetdata);
//         }
//       })

//     }

//   }

//   inputvalidation(){
//     this.form.get('InstrumentValidityPeriodInDays').valueChanges.subscribe(value => {
//       if (value !== null) {
//         const newValue = value.replace(/[^0-9]/g, ''); // Remove non-numeric characters
//         this.form.get('InstrumentValidityPeriodInDays').setValue(newValue, { emitEvent: false });
//       }
//     });
//   }

//   onSearch(term: string): void {
//     this.searchTerm = term;
//     // Call your service here with the search term
//     if (this.searchOnEnter) {
//       console.log('Term:',term)
//     }
//   }

//   onKeyDown(event: KeyboardEvent): void {
//     if (event.key === 'Enter') {
//       this.searchOnEnter = true;
//       this.onSearch(this.searchTerm);
//       this.searchOnEnter = false;
//     }
//   }

//   customSearch(term: string, item: any): boolean {
//     // Customize the search logic based on your requirements
//     return (
//       (item.InstrumentAccountCode && item.InstrumentAccountCode.toLowerCase().includes(term.toLowerCase())) ||
//       (item.InstrumentAccountName && item.InstrumentAccountName.toLowerCase().includes(term.toLowerCase()))
//     );
//   }

//   callservice(){
//     this.service.getall().subscribe(res => {
//       let data = res
//       data = data.map(({ Id, Name }) => ({ InstrumentId: Id, InstrumentName: Name }));
//       this.InstrumentNamePicklist = data;
//     })
//     this.form.get('InstrumentName').valueChanges.subscribe(value => {
//       if (value !== null && value != undefined) {
//         if(value.length > 25){
//           alert("Instrment Name length Should not be more than 25 ")
//           const newValue = value.slice(0, 25); // Remove Extra character
//           this.form.get('InstrumentName').setValue(newValue, { emitEvent: false });
//         }
//       }
//     });
//   }

//   public changeInstrumentNameFn(selecteddata) {
//     let InstrumentObj = this.InstrumentNamePicklist.find(t => t.InstrumentName === selecteddata);
//     let id = InstrumentObj.InstrumentId
//     this.loadScreen(id);
//   }

//   public changeAccountNameFn(selecteddata) {
//     let InstrumentObj = this.AccountPicklistvalue.find(t => t.InstrumentAccountName === selecteddata);
//     let id = InstrumentObj.InstrumentAccountId
//     this.form.get('AccountId').patchValue(id);
//   }

//   public changeOUNameFn(selecteddata) {
//     this.form.get('InstrumentOuIds').patchValue("");
//     for(let data of selecteddata){
//       let InstrumentObj = this.OUPicklist.find(t => t.InstrumentOuNames === data);
//       this.form.get('InstrumentOuIds').patchValue(this.form.get('InstrumentOuIds').value+InstrumentObj.InstrumentOuIds+",");
//     }
//     this.form.get('InstrumentOuIds').patchValue(this.form.get('InstrumentOuIds').value.slice(0, -1));
//   }

//   public changeOUGroupNameFn(selecteddata) {
//     this.form.get('InstrumentOuGroupIds').patchValue("");
//     for(let data of selecteddata){
//       let InstrumentObj = this.OUGroupPicklist.find(t => t.InstrumentOuGroupNames === data);
//       this.form.get('InstrumentOuGroupIds').patchValue(this.form.get('InstrumentOuGroupIds').value+InstrumentObj.InstrumentOuGroupIds+",");
//     }
//     this.form.get('InstrumentOuGroupIds').patchValue(this.form.get('InstrumentOuGroupIds').value.slice(0, -1));
//   }

//   public movenextandpre() {
//     this.rowdatacommon$.subscribe(data => {
//       console.log("Data", data)
//       this.movenextpreviousid$.subscribe(res => {
//         if (res) {
//           console.log("resid", res)
//           let id = data[res].InstrumentId
//           this.loadScreen(id)
//         }
//       })
//     });

//   }

//   public readOutputValueEmitted(val) {
//     console.log("CHILDVALUE", val)
//   }

// }