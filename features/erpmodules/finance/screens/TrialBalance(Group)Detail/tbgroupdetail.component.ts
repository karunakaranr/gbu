
import { Component, Inject, LOCALE_ID, OnInit } from '@angular/core';

import { Observable } from 'rxjs';
import { Select, Store } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { SharedService } from 'features/commonreport/service/datapassing.service';


@Component({
    selector: 'app-tbgroupdetail',
    templateUrl: './tbgroupdetail.component.html',
    styleUrls: ['./tbgroupdetail.component.scss'],
})
export class tbgroupdetailComponent implements OnInit {
    @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
    public rowData: any;
    public TotalOpening;
    public TotalDebit;
    public TotalCredit;
    public TotalClosing;
    tabledata: any[];
    constructor(public sharedService: SharedService,public store: Store,  @Inject(LOCALE_ID) public locale: string) { }
    ngOnInit(): void {
        this.gettbgroupdetail();
    }

    public gettbgroupdetail() {
        this.sharedService.getRowInfo().subscribe(data => {
            if (data.length > 0) {
            this.rowData = data;
            console.log("Trial Balance Group Detail :", this.rowData)

            this.TotalOpening = 0;
            this.TotalDebit = 0;
            this.TotalCredit = 0;
            this.TotalClosing = 0;
            let json = this.rowData;
            var finalizedArray = [];
            for (let data of this.rowData) {
                this.TotalOpening = this.TotalOpening + data.Opening;
                this.TotalDebit = this.TotalDebit + data.Debit;
                this.TotalCredit = this.TotalCredit + data.Credit;
                this.TotalClosing = this.TotalClosing + data.Closing;
            }
            json.map((row) => {
                finalizedArray.push({
                    accschcode: row['AccountScheduleCode'],
                    accschname: row['AccountScheduleName'],

                    accgrpcode: row['AccountGroupCode'],
                    accgrpname: row['AccountGroupName'],
                    opg: row['Opening'],
                    debit: row['Debit'],
                    credit: row['Credit'],
                    cls: row['Closing'],
                });
            });

            const final = {};
            finalizedArray.forEach((detail) => {
                final[detail.accschcode] = {
                    accountGroups: {},
                    accgrpcode: detail.accschcode,
                    accgrpname: detail.accschname,
                    opg: detail.opg,
                    debit: detail.debit,
                    credit: detail.credit,
                    cls: detail.cls,
                    
                    ...final[detail.accschcode],
                };

                final[detail.accschcode].accountGroups[detail.accgrpcode] = {
                    accgrpcode: detail.accgrpcode,
                    accgrpname: detail.accgrpname,
                    opg: detail.opg,
                    debit: detail.debit,
                    credit: detail.credit,
                    cls: detail.cls,
                };
            });

            const grpcodes = Object.keys(final);

            const tableData = [];
            grpcodes.forEach((accgrpcode) => {
                const accountGroups = Object.keys(final[accgrpcode].accountGroups);
                let sumopening = 0;
                let sumdebit = 0;
                let sumcredit = 0;
                let sumclosing = 0;

                accountGroups.forEach((ag) => {
                    sumopening = sumopening + parseFloat(final[accgrpcode].accountGroups[ag].Opening);
                    sumdebit = sumdebit + parseFloat(final[accgrpcode].accountGroups[ag].debit);
                    sumcredit = sumcredit + parseFloat(final[accgrpcode].accountGroups[ag].credit);
                    sumclosing = sumclosing + parseFloat(final[accgrpcode].accountGroups[ag].Closing);
                })
                final[accgrpcode].Opening = sumopening;
                final[accgrpcode].debit = sumdebit;
                final[accgrpcode].credit = sumcredit;
                final[accgrpcode].Closing = sumclosing;

                tableData.push({
                    accgrpcode: final[accgrpcode].accgrpcode,
                    accgrpname: final[accgrpcode].accgrpname,
                    opg: final[accgrpcode].opg,
                    debit: final[accgrpcode].debit,
                    credit: final[accgrpcode].credit,
                    cls: final[accgrpcode].cls,
                    bold: true,
                });

                const accounts = Object.keys(final[accgrpcode].accountGroups);
                accounts.forEach((account) => {
                    tableData.push({
                        accgrpcode: final[accgrpcode].accountGroups[account].accgrpcode,
                        accgrpname: final[accgrpcode].accountGroups[account].accgrpname,
                        opg: final[accgrpcode].accountGroups[account].opg,
                        debit: final[accgrpcode].accountGroups[account].debit,
                        credit: final[accgrpcode].accountGroups[account].credit,
                        cls: final[accgrpcode].accountGroups[account].cls,
                    });
                });
            });
            this.tabledata = tableData;
            console.log("Summary Register Data:", tableData)
        }
        });
    }
}


