import { Component, OnInit } from '@angular/core';
import { PartyBranchList } from '../../../services/AccountList/PartyBranch/PartyBranch.service';
const col = require('./../../../../../../apps/Goodbooks/Goodbooks/src/assets/data/partybranch.json')


@Component({
  selector: 'app-PartyBranch',
  templateUrl: './PartyBranch.component.html',
  styleUrls: ['./PartyBranch.component.scss']
})

export class PartyBranchListComponent implements OnInit {
  title = 'Party Branch List'
  public rowData = []
  public columnData = []

  constructor(public service: PartyBranchList) { }
  ngOnInit(): void {
    this.partybranchlist();
  }
  public partybranchlist() {
    this.service.partylistview().subscribe(menudetails => {
      this.columnData = col;
      let servicejsondetail = menudetails
      this.service.Rowservice(servicejsondetail).subscribe(res => {
        this.rowData = res.ReportDetail;

        for (let data of this.rowData) {
            if (data.StateCode=='NONE'){
                data.StateCode=""
            }
            if (data.StateName=='NONE'){
                data.StateName=""
            }
        }
  });
})

}

}