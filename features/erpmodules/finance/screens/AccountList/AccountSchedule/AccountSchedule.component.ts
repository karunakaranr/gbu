import { Component, OnInit } from '@angular/core';
import { AccountScheduleList } from '../../../services/AccountList/AccountSchedule/AccountSchedule.service';
import { Router } from '@angular/router'; 
import {Inject, LOCALE_ID } from '@angular/core';

@Component({
  selector: 'app-AccountSchedule',
  templateUrl: './AccountSchedule.component.html',
  styleUrls: ['./AccountSchedule.component.scss']
})

export class AccountScheduleComponent implements OnInit {
  title = 'Account Schedule'
  public rowData = []
  constructor(public serivce: AccountScheduleList, private router: Router,  @Inject(LOCALE_ID) public locale: string) { }
  ngOnInit(): void {
    this.accountschedule();
  }
  public accountschedule() {
    this.serivce.accountscheduleview().subscribe(menudetails => {
      let servicejsondetail = menudetails
      this.serivce.Rowservice(servicejsondetail).subscribe(res => {
        this.rowData = res.ReportDetail;
        console.log("dkd", res);
        let json = this.rowData;
        var finalizedArray = []
        json.map(row => {
          finalizedArray.push({
            nature: row['AccountScheduleNatureName'],
            code: row['AccountScheduleCode'],
            name: row['AccountScheduleName'],
          })
        })
        const final = {};
        finalizedArray.forEach(detail => {
          final[detail.nature] = {
            accounts :{},
            code: detail.nature,
            name: "",
            ...final[detail.nature], 
          }
          final[detail.nature].accounts[detail.code] = {
            code: detail.code,
            name: detail.name,
          }
        })
        const empcodes = Object.keys(final)
        const tableData = [];
        empcodes.forEach(code => {
          const account = Object.keys(final[code].accounts)
            tableData.push({
              code: final[code].code,
              name: "",
              bold: true,
            })
            account.forEach(ag => {
              tableData.push({
                code: final[code].accounts[ag].code,
                name: final[code].accounts[ag].name,
              })
            })
        })
        this.rowData=tableData;
      })
    })

  }

}  