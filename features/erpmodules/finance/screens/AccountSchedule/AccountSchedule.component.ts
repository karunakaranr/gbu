import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';

import { AccountScheduleService } from '../../services/AccountSchedule/AccountSchedule.service'; 
import { IAccountSchedule } from '../../models/IAccountSchedule';
import { ButtonVisibility } from 'features/layout/store/layout.actions';


import * as AccountSchedulejson from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/AccountSchedule.json'

import { URLS } from '../../URLS/urls';


@Component({
  selector: "app-AccountSchedule",
  templateUrl: "./AccountSchedule.component.html",
  styleUrls: ["./AccountSchedule.component.scss"],
  providers: [
    { provide: 'IdField', useValue: 'AccountScheduleId' },
    { provide: 'url', useValue: URLS.AccountSchedule },
    { provide: 'DataService', useClass: AccountScheduleService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class AccountScheduleFormComponent extends GBBaseDataPageComponentWN<IAccountSchedule> {
  title: string = "AccountSchedule"
  AccountSchedulejson = AccountSchedulejson;


  

  form: GBDataFormGroupWN<IAccountSchedule> = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'AccountSchedule', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.AccountSchedulePicklist(arrayOfValues.AccountScheduleId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }




  public AccountSchedulePicklist(SelectedPicklistData: string): void {

    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(AccountSchedule => {
        this.form.patchValue(AccountSchedule);
      })
    }
  }


  public AccountSchedulePicklists(SelectedPicklistDatas: any): void {
    
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
    console.log("iteem==>",SelectedPicklistDatas)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IAccountSchedule> {
  const dbds: GBBaseDBDataService<IAccountSchedule> = new GBBaseDBDataService<IAccountSchedule>(http);
  dbds.endPoint = url;
  return dbds;
}

export function getThisPageService(store: Store, dataService: GBBaseDataService<IAccountSchedule>, dbDataService: GBBaseDBDataService<IAccountSchedule>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IAccountSchedule> {
  return new GBDataPageService<IAccountSchedule>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
