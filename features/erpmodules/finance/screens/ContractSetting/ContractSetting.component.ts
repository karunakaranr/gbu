import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';

import { ContractSettingService } from '../../services/ContractSetting/ContractSetting.service';
import { IContractSetting } from '../../models/IContractSetting';
import { ButtonVisibility } from 'features/layout/store/layout.actions';

import * as ContractSettingjson from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/ContractSetting.json'

import { URLS } from '../../URLS/urls';


@Component({
  selector: "app-ContractSetting",
  templateUrl: "./ContractSetting.component.html",
  styleUrls: ["./ContractSetting.component.scss"],
  providers: [
    { provide: 'IdField', useValue: 'ContractSettingId' },
    { provide: 'url', useValue: URLS.ContractSetting },
    { provide: 'DataService', useClass: ContractSettingService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class ContractSettingComponent extends GBBaseDataPageComponentWN<IContractSetting> {
  title: string = "ContractSetting"
  ContractSettingjson = ContractSettingjson;




  form: GBDataFormGroupWN<IContractSetting> = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'ContractSetting', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.ContractSettingPicklist(arrayOfValues.ContractSettingId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }




public ContractSettingPicklist(SelectedPicklistData: string): void {
console.log("patchig",SelectedPicklistData)
  if (SelectedPicklistData) {
    this.gbps.dataService.getData(SelectedPicklistData).subscribe(ContractSetting => {
      this.form.patchValue(ContractSetting);
    })
  }
}



  public ContractSettingPicklists(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
    console.log("iteem==>",SelectedPicklistDatas)
  }
}




export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IContractSetting> {
  const dbds: GBBaseDBDataService<IContractSetting> = new GBBaseDBDataService<IContractSetting>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IContractSetting>, dbDataService: GBBaseDBDataService<IContractSetting>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IContractSetting> {
  return new GBDataPageService<IContractSetting>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
