import { PartyWithBranch } from '../../models/IPartyWithBranch';
import {
  GBDataStateAction,
 
} from './../../../../../libs/gbdata/src/lib/store/gbdata.actions';
import {  ISelectListCriteria, } from './../../../../../libs/gbdata/src/lib/interfaces/ISectionCriteriaList';

  const PartyWithBranchEntityName = 'PartyWithBranch';
  export class GetData extends GBDataStateAction<PartyWithBranch> {
    static readonly type = '[' + PartyWithBranchEntityName + '] getData';
    constructor(public id: number) {
      super();
    }
  }
  export class SaveData extends GBDataStateAction<PartyWithBranch> {
    static readonly type = '[' + PartyWithBranchEntityName + '] saveData';
    constructor(
      public payload: PartyWithBranch
    ) {
      super();
    }
  }
  export class AddData extends GBDataStateAction<PartyWithBranch> {
    static readonly type = '[' + PartyWithBranchEntityName + '] addData';
    constructor(
      public payload: PartyWithBranch
    ) {
      super();
    }
  }
  export class GetAll extends GBDataStateAction<PartyWithBranch> {
    static readonly type = '[' + PartyWithBranchEntityName + '] getAll';
    constructor(public scl: ISelectListCriteria) {
      super();
    }
  }
  export class GetAllAndMoveFirst extends GBDataStateAction<PartyWithBranch> {
    static readonly type = '[' + PartyWithBranchEntityName + '] getAllAndMoveFirst';
    constructor(public scl: ISelectListCriteria) {
      super();
    }
  }
  export class MoveFirst extends GBDataStateAction<PartyWithBranch> {
    static readonly type = '[' + PartyWithBranchEntityName + '] MoveFirst';
    constructor() {
      super();
    }
  }
  export class MovePrev extends GBDataStateAction<PartyWithBranch> {
    static readonly type = '[' + PartyWithBranchEntityName + '] MovePrev';
    constructor() {
      super();
    }
  }
  export class MoveNext extends GBDataStateAction<PartyWithBranch> {
    static readonly type = '[' + PartyWithBranchEntityName + '] MoveNext';
    constructor() {
      super();
    }
  }
  export class MoveLast extends GBDataStateAction<PartyWithBranch> {
    static readonly type = '[' + PartyWithBranchEntityName + '] MoveLast';
    constructor() {
      super();
    }
  }
  export class ClearData extends GBDataStateAction<PartyWithBranch> {
    static readonly type = '[' + PartyWithBranchEntityName + '] ClearData';
    constructor() {
      super();
    }
  }
  export class DeleteData extends GBDataStateAction<PartyWithBranch> {
    static readonly type = '[' + PartyWithBranchEntityName + '] DeleteData';
    constructor() {
      super();
    }
  }


