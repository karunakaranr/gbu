import {
    GBDataStateAction,
    GBDataStateActionFactoryWN,
   
  } from './../../../../../libs/gbdata/src/lib/store/gbdata.actions';
  import {  ISelectListCriteria, } from './../../../../../libs/gbdata/src/lib/interfaces/ISectionCriteriaList';
  import { PartyWithBranch } from '../../models/IPartyWithBranch';
  import { GetAll, GetData, MoveFirst, MoveLast, MoveNext, MovePrev, SaveData, ClearData, DeleteData, AddData } from './PartyWithBranch.action';
  
  export class PartyWithBranchStateActionFactory extends GBDataStateActionFactoryWN<PartyWithBranch> {
      GetData(id: number): GBDataStateAction<PartyWithBranch> {
        return new GetData(id);
      }
      GetAll(scl: ISelectListCriteria): GBDataStateAction<PartyWithBranch> {
        return new GetAll(scl);
      }
      MoveFirst(): GBDataStateAction<PartyWithBranch> {
        return new MoveFirst();
      }
      MoveNext(): GBDataStateAction<PartyWithBranch> {
        return new MoveNext();
      }
      MovePrev(): GBDataStateAction<PartyWithBranch> {
        return new MovePrev();
      }
      MoveLast(): GBDataStateAction<PartyWithBranch> {
        return new MoveLast();
      }
      AddData(payload: PartyWithBranch): GBDataStateAction<PartyWithBranch> {
        return new AddData(payload);
      }
      SaveData(payload: PartyWithBranch): GBDataStateAction<PartyWithBranch> {
        return new SaveData(payload);
      }
      ClearData(): GBDataStateAction<PartyWithBranch> {
        return new ClearData();
      }
      DeleteData(): GBDataStateAction<PartyWithBranch> {
        return new DeleteData();
      }
    }
  