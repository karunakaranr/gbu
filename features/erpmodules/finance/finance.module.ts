import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';


import { UicoreModule } from '@goodbooks/uicore';
import { UifwkUifwkmaterialModule } from '@goodbooks/uifwk/material';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';

import { TranslocoRootModule } from './../../transloco/transloco-root.module';
import { GbdataModule } from '@goodbooks/gbdata';
import { environment } from '../../environments/environment';
import { NgBreModule } from '@unisoft/ng-bre';

import { NgxChartsModule } from '@swimlane/ngx-charts';
import { GbchartModule } from '../../common/shared/screens/gbchart/gbchart.module';

import { EntitylookupModule } from './../../entitylookup/entitylookup.module';
import { geolocationModule} from './../../geolocation/geolocation.module'
import { NgSelectModule } from '@ng-select/ng-select';

import { AgGridModule } from 'ag-grid-angular';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule} from '@angular/material/button';
import { MatListModule } from '@angular/material/list'
import { MatIconModule} from '@angular/material/icon';
import { MatDividerModule} from '@angular/material/divider';
import {MatExpansionModule} from '@angular/material/expansion';

import { MatTabsModule} from '@angular/material/tabs'
import { MatCheckboxModule} from '@angular/material/checkbox';
import { AgmCoreModule } from '@agm/core';
import { AgmDirectionModule } from 'agm-direction';

import { GbgridModule } from 'features/gbgrid/components/gbgrid/gbgrid.module';
import { StatementOfAccountReportComponent} from './screens/StatementofAccount/StatementofAccountReport/StatementofAccountReport.component'
import { CommonReportModule } from './../../commonreport/commonreport.module';
import { FinanceRoutingModule } from './finance.routing.module';
import { FinanceComponent } from './finance.component';
import { MatTableModule } from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MaterialModule } from 'features/common/material.module';
import { MatInputModule } from '@angular/material/input';
import { PartyBranchListComponent } from './screens/AccountList/PartyBranchList/PartyBranch.component'
import { AccountListComponent } from './screens/AccountList/AccountList/AccountList.component'
import { AccountScheduleComponent } from './screens/AccountList/AccountSchedule/AccountSchedule.component';
import { AccountScheduleFormComponent } from './screens/AccountSchedule/AccountSchedule.component';
import { AccountGroupComponent } from './screens/AccountList/AccountGroup/AccountGroup.component'
import { ContractSettingComponent } from './screens/ContractSetting/ContractSetting.component';
import { CompanyVsLedgerComponent } from './screens/CompnayVsLedger/CompanyVsLedger.component'
import { InstrumentListComponent } from './screens/InstrumentList/InstrumentList.component'
import { InsuranceListComponent } from './screens/InsuranceList/InsuranceList.component'
import { TDSContactComponent } from './screens/TDSContact/TDSContact.component';
import { PartyWithBranchComponent } from './screens/PartyWithBranch/PartyWithBranch.component';
import { CashBookComponent } from './screens/Books/CashBook/CashBook.component';
import { NgxsModule } from '@ngxs/store';
import { PartyWithBranchState } from './stores/PartyWithBranch/PartyWithBranch.state';
import { NgxsFormPluginModule } from '@ngxs/form-plugin';
import { SamplescreensModule } from '../samplescreens/samplescreens.module';

import { schedulelistComponent } from './screens/Schedule/ScheduleList/schedulelist.component';
import { partybranchlistComponent } from './screens/PartyBranch/PartyBranchList/partybranchlist.component';
import { paymentsummaryComponent } from './screens/Payment/PaymentSummary/paymentsummary.component';
import { tbscheduleComponent } from './screens/TrialBalanceSchedule/tbschedule.component';
import { tbgroupComponent } from './screens/TrialBalanceGroup/tbgroup.component';
import { tbaccountComponent } from './screens/TrialBalanceAccount/tbaccount.component';
import { tbscheduledetailComponent } from './screens/TrialBalance(Schedule)Detail/tbscheduledetail.component';
import { BankComponent } from './screens/Bank/Bank.component';
import { CostCenterComponent } from './screens/CostCenter/CostCenter.component';
import { tbgroupdetailComponent } from './screens/TrialBalance(Group)Detail/tbgroupdetail.component';
import { tbaccountdetailComponent } from './screens/TrialBalance(Account)Detail/tbaccountdetail.component';
import { InstrumentComponent } from './screens/Instrument/Instrument.component';
import { receivablebillwiseComponent } from './screens/ReceivablesBillwise/receivablebillwise.component';
import { receivablesdetailComponent } from './screens/ReceivablesDetail/receivablesdetail.component';
import { receivablessummaryComponent } from './screens/ReceivablesSummary/receivablessummary.component';
import { bankbookComponent } from './screens/BankBook/bankbook.component';
import { soasingleComponent } from './screens/StatementofAccount(Single)/soasingle.component';
import { daybookComponent } from './screens/DayBook/DayBook.component';
import { deductedcategoryComponent } from './screens/TDS/DeductedCategory/deductedcategory.component';
import { registerdetailComponent } from './screens/RegisterDetail/RegisterDetailSummary/registerdetail.component';
import { PartyBranchLocationComponent } from './screens/PartyBranchLocation/PartyBranchLocation.component';
import { brssummaryComponent } from './screens/BRSSummary/BRSSummaryView/brssummary.component';
import { brsdetailComponent } from './screens/BRSDetail/BRSDetailView/brsdetail.component';
import { brsregisterComponent } from './screens/BRSRegister/BRSRegisterView/brsregister.component';
import { PartyComponent } from './screens/Party/Party.component';
import { TDSCategoryMasterComponent } from './screens/TDSCategoryMaster/TDSCategoryMaster.component';
import { BudgetMasterComponent } from './screens/BudgetMaster/BudgetMaster.component';
import { InsuranceMasterComponent } from './screens/InsuranceMaster/InsuranceMaster.component';
import { DistributionListComponent } from './screens/DistributionList/DistributionList.component';
import { CostCategoryComponent } from './screens/CostCategory/costcategory.component';
import { CostCenterPatternComponent } from './screens/CostCenterPattern/CostCenterPattern.component';
import { ebankComponent } from './screens/E-Bank/E-Bank/ebank.component';
import { AccountVoucherComponent } from './Transaction/AccountVoucher/AccountVoucher.component';
import { AccountMasterComponent } from './screens/AccountMaster/AccountMaster.component';
import { DeferralPlanComponent } from './screens/DeferralPlan/DeferralPlan.Component';
import { ChequeBookComponent } from './screens/ChequeBook/chequebook.component';
import { PartyOUComponent } from './screens/PartyOU/partyou.component';
import { NarrationMasterComponent } from './screens/NarrationMaster/NarrationMaster.component';
import { QuillModule } from 'ngx-quill'
import { BudgetDataComponent } from './screens/BudgetData/BudgetData.component';
@NgModule({
  declarations: [
    AccountVoucherComponent,
    PartyOUComponent,
    ChequeBookComponent,
    InstrumentComponent,
    FinanceComponent,
    AccountMasterComponent,
    NarrationMasterComponent,
    BankComponent,
    PartyComponent,
    DistributionListComponent,
    InsuranceMasterComponent,
    TDSCategoryMasterComponent,
    CostCenterPatternComponent,
    CostCategoryComponent,
    BudgetMasterComponent,
    PartyBranchListComponent,
    BudgetDataComponent,
    PartyBranchLocationComponent,
    CostCenterComponent,
    AccountListComponent,
    AccountScheduleComponent,
    AccountScheduleFormComponent,
    AccountGroupComponent,
    CompanyVsLedgerComponent,
    ContractSettingComponent,
    InstrumentListComponent,
    StatementOfAccountReportComponent,
    InsuranceListComponent,
    TDSContactComponent,
    PartyWithBranchComponent, 
    CashBookComponent, 

    schedulelistComponent,
    partybranchlistComponent,
    paymentsummaryComponent,
    tbscheduleComponent,
    tbgroupComponent,
    tbaccountComponent,
    tbscheduledetailComponent,
    tbgroupdetailComponent,
    tbaccountdetailComponent,
    receivablebillwiseComponent,
    receivablesdetailComponent,
    receivablessummaryComponent,
    bankbookComponent,
    soasingleComponent,
    daybookComponent,
    deductedcategoryComponent,
    registerdetailComponent,
    brssummaryComponent,
    brsdetailComponent,
    brsregisterComponent,
    ebankComponent,
    DeferralPlanComponent
  ],
  imports: [
    SamplescreensModule,
    CommonModule,
    FinanceRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    UicoreModule,
    NgBreModule,
    UifwkUifwkmaterialModule,
    TranslocoRootModule,
    QuillModule.forRoot(),
    AgGridModule,
    GbgridModule,
    GbdataModule.forRoot(environment),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDvFSDxJXXR1nrdRDIzvp6DRZB9v649My8' ,
      libraries: ['places' ,'geometry','drawing']
    }),
    NgxsModule.forFeature([]),
    NgxsFormPluginModule.forRoot(),
    AgmDirectionModule,
    NgxChartsModule,
    GbchartModule,
    EntitylookupModule,
    geolocationModule,
    NgSelectModule,
    MaterialModule,
    MatSortModule,
    MatFormFieldModule,
    MatSelectModule,
    MatButtonModule,
    MatListModule,
    MatTabsModule,
    MatCheckboxModule,
    MatTableModule,
    MatIconModule,
    MatExpansionModule,
    MatDividerModule,
    MatPaginatorModule,
    CommonReportModule,
    MatFormFieldModule,
    MatInputModule,

  ],
   exports: [
    FinanceComponent,
     MatSortModule,
     MatFormFieldModule,
     MatInputModule,
     BankComponent
   ],
})
export class FinanceModule {
  
 
}