import { Injectable } from '@angular/core';
import { TDSContactDbService } from './../../dbservices/TDSContact/TDSContactDb.service';
@Injectable({
  providedIn: 'root'
})
export class TDSContact {
  constructor(public dbservice: TDSContactDbService) { }

  public tdscontactview(){             
    return this.dbservice.picklist();
  }

  public Rowservice(obj){
    return this.dbservice.reportData(obj);
  }
}