import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN,GBBaseDBDataService } from 'libs/gbdata/src';

import { IBudgetMaster } from '../../models/IBudgetMaster';

 
@Injectable({
  providedIn: 'root'
})
export class BudgetMasterService extends GBBaseDataServiceWN<IBudgetMaster> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IBudgetMaster>) {
    super(dbDataService, 'BudgetId');
  }
}
