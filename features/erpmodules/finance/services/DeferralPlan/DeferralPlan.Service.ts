import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { IDeferralPlan } from '../../models/IDeferralPlan';


// import { ModelName } from ‘ModelPath’;


 
@Injectable({
  providedIn: 'root'
})
export class DeferalPlanService extends GBBaseDataServiceWN<IDeferralPlan> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IDeferralPlan>) {
    super(dbDataService, 'DeferalPlanId');
  }
}
