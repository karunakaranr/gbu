import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN,GBBaseDBDataService } from 'libs/gbdata/src';

import { IParty } from '../../models/IParty';

 
@Injectable({
  providedIn: 'root'
})
export class PartyService extends GBBaseDataServiceWN<IParty> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IParty>) {
    super(dbDataService, 'PartyId');
  }
}
