import { Injectable } from '@angular/core';
import { InstrumentListDbService } from './../../dbservices/InstrumentList/InstrumentListDb.service';
@Injectable({
  providedIn: 'root'
})
export class InstrumentList {
  constructor(public dbservice: InstrumentListDbService) { }

  public instrumentlistview(){                    
    return this.dbservice.picklist();
  }

  public Rowservice(obj){
    return this.dbservice.reportData(obj);
  }
}