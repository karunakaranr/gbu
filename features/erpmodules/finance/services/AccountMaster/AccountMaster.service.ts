import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN,GBBaseDBDataService } from 'libs/gbdata/src';



import { IAccountMaster } from '../../models/IAccountMaster';
 
@Injectable({
  providedIn: 'root'
})
export class AccountMasterService extends GBBaseDataServiceWN<IAccountMaster> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IAccountMaster>) {
    super(dbDataService,'AccountId');
  }
}
