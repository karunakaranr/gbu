import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { GBBaseDataServiceWN} from './../../../../../libs/gbdata/src/lib/services/gbbasedata.service';

import { PartyWithBranch } from './../../models/IPartyWithBranch';
import { PartyWithBranchDBService } from './../../dbservices/PartyWithBranch/PartyWithBranchDB.service';


@Injectable({
  providedIn: 'root'
})
export class PartyWithBranchService extends GBBaseDataServiceWN<PartyWithBranch> {
  constructor(public dbDataService: PartyWithBranchDBService) {
    super(dbDataService, 'PartyId');
  }

  saveData(data: PartyWithBranch): Observable<any> {
    return super.saveData(data);
  }

  getData(id: string): Observable<PartyWithBranch> {
    return super.getData(id);
  }
  deleteData(id: any): Observable<any> {
    return super.deleteData(id);
  }
  getAll(): Observable<any> {
    return super.getAll();
  }

  moveFirst(): Observable<PartyWithBranch> {
    return super.moveFirst();
  }

  movePrev(): Observable<PartyWithBranch> {
    return super.movePrev();
  }

  moveNext(): Observable<PartyWithBranch> {
    return super.moveNext();
  }

  moveLast(): Observable<PartyWithBranch> {
    return super.moveLast();
  }
}
