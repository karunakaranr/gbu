import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';

import { IInstrument } from '../../models/IInstrument';

 
@Injectable({
  providedIn: 'root'
})
export class InstrumentService extends GBBaseDataServiceWN<IInstrument> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IInstrument>) {
    super(dbDataService, 'InstrumentId');
  }
}