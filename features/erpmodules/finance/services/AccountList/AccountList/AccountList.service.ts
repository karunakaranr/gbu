import { Injectable } from '@angular/core';
import { AccountListDbService } from './../../../dbservices/AccountList/AccountList/AccountListDb.service';
@Injectable({
  providedIn: 'root'
})
export class AccountList {
  constructor(public dbservice: AccountListDbService) { }

  public accountlistview(){        
    console.log("aaaa",this.dbservice.picklist())               //col
    return this.dbservice.picklist();
  }

  public Rowservice(obj){ 
    return this.dbservice.reportData(obj);
  }
}