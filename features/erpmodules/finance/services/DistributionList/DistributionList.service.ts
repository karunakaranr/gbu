import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN,GBBaseDBDataService } from 'libs/gbdata/src';

import { IDistributionList } from '../../models/IDistributionList';

 
@Injectable({
  providedIn: 'root'
})
export class DistributionListService extends GBBaseDataServiceWN<IDistributionList> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IDistributionList>) {
    super(dbDataService, 'DistributionListId');
  }
}
