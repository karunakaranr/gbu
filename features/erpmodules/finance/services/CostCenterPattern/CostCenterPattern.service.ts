import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN,GBBaseDBDataService } from 'libs/gbdata/src';


import { ICostCenterPattern } from '../../models/ICostCenterPattern';

 
@Injectable({
  providedIn: 'root'
})
export class CostCenterPatternService extends GBBaseDataServiceWN<ICostCenterPattern> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<ICostCenterPattern>) {
    super(dbDataService, 'CostCenterPatternId');
  }
}
