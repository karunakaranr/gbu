import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';

import { IBudgetData } from '../../models/IBudgetData';


 
@Injectable({
  providedIn: 'root'
})
export class BudgetDataService extends GBBaseDataServiceWN<IBudgetData> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IBudgetData>) {
    super(dbDataService, 'BudgetDataId',"BudgetData");
  }
}
