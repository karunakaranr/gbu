import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { IEBank } from '../../models/IEbank';
 
@Injectable({
  providedIn: 'root'
})
export class EbankService extends GBBaseDataServiceWN<IEBank> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IEBank>) {
    super(dbDataService, 'AccountId');
  }
}
