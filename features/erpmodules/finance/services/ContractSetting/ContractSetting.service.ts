import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN,GBBaseDBDataService } from 'libs/gbdata/src';


import { IContractSetting } from '../../models/IContractSetting';


 
@Injectable({
  providedIn: 'root'
})
export class ContractSettingService extends GBBaseDataServiceWN<IContractSetting> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IContractSetting>) {
    super(dbDataService, 'ContractSettingId');
  }
}
