import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN,GBBaseDBDataService } from 'libs/gbdata/src';


import { IInsuranceMaster } from '../../models/IInsuranceMaster';


 
@Injectable({
  providedIn: 'root'
})
export class InsuranceMasterService extends GBBaseDataServiceWN<IInsuranceMaster> {
  
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IInsuranceMaster>) {
    super(dbDataService,'InsuranceId');
    console.log("dbdseries",dbDataService)
  }
}
