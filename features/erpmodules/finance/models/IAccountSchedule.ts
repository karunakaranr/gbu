export interface IAccountSchedule {
    AccountScheduleId: number
    AccountScheduleCode: string
    AccountScheduleName: string
    AccountScheduleShortName: string
    AccountScheduleNature: number
    AccountScheduleNatureName: string
    ParentId: number
    ParentCode: string
    ParentName: string
    AccountScheduleCreatedById: number
    AccountScheduleModifiedById: number
    AccountScheduleCreatedByName: any
    AccountScheduleCreatedOn: string
    AccountScheduleModifiedByName: any
    AccountScheduleModifiedOn: string
    AccountScheduleSortOrder: number
    AccountScheduleStatus: number
    AccountScheduleVersion: number
    AccountScheduleSourceType: number
}