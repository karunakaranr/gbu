export interface IBank {
    GcmId: number
    GcmCode: string
    GcmName: string
    GcmTypeId: string
    GcmTypeCode: string
    GcmTypeName: string
    GcmColor: number
    GcmRemarks: string
    GcmStatus: number
    GcmVersion: number
    GcmCreatedOn: string
    GcmModifiedOn: string
    GcmCreatedByName: string
    GcmModifiedByName: string
    GcmSourceType: number
}
