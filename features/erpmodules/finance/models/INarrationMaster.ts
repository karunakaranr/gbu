export interface INarrationMaster {
    NarrationId: number
    NarrationName: string
    ParameterSetId: number
    ParameterSetCode: string
    ParameterSetName: string
    NarrationType: number
    NarrationNarrations: string
    NarrationCreatedById: number
    NarrationCreatedOn: string
    NarrationModifiedById: number
    NarrationModifiedOn: string
    NarrationSortOrder: number
    NarrationStatus: number
    NarrationVersion: number
  }
  