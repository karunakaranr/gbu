export type Root = IBudgetData[]

export interface IBudgetData {
  BudgetDataId: number
  BudgetDataFromDate: string
  BudgetDataToDate: string
  BudgetDataCompareFromDate: string
  BudgetDataCompareToDate: string
  OUId: number
  OUCode: string
  OUName: string
  BudgetId: number
  BudgetCode: string
  BudgetName: string
  GBPeriodDetailId: number
  GBPeriodDetailSubPeriodName: string
  CompareGBPeriodDetailId: number
  CompareGBPeriodDetailSubPeriodName: string
  BudgetDataDetailArray: BudgetDataDetailArray[]
  BudgetDataCreatedById: number
  BudgetDataCreatedOn: string
  BudgetDataCreatedByName: string
  BudgetDataModifiedById: number
  BudgetDataModifiedOn: string
  BudgetDataModifiedByName: string
  BudgetDataSortOrder: number
  BudgetDataStatus: number
  BudgetDataVersion: number
  BudgetDataSourceType: number
}

export interface BudgetDataDetailArray {
  BudgetDataDetailId: number
  BudgetDataDetailSlNo: number
  BudgetDataDetailType: number
  BudgetDataDetailAmount: number
  BudgetDataId: number
  AccountId: number
  AccountCode: string
  AccountName: string
  AccountGroupId: number
  AccountGroupCode: string
  AccountGroupName: string
  CostCenterId: number
  CostCenterCode: string
  CostCenterName: string
}
