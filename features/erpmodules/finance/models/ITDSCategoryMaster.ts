export interface ITDSCategoryMaster {
    TDSCategoryId: number
    TDSCategoryCode: string
    TDSCategoryName: string
    TDSCategorySection: string
    TDSCategoryReferenceSection: string
    TDSCategoryForm: string
    TDSCategoryTDSPercent: string
    TDSCategorySCPercent: string
    TDSCategoryCessPercent: string
    TDSCategoryAmountLimit: string
    TDSCategoryTransactionLimit: string
    DeducationAccountId: string
    DeducationAccountName: string
    PaidAccountId: string
    PaidAccountName: string
    ClaimAccountId: string
    ClaimAccountName: string
    SufferedAccountId: string
    SufferedAccountName: string
    TDSCategoryRemarks: string
    TDSCategoryCreatedOn: string
    TDSCategoryModifiedOn: string
    TDSCategoryIsCompany: number
    TDSCategoryModifiedByName: string
    TDSCategoryCreatedByName: string
    TDSCategoryStatus: number
    TDSCategoryVersion: number
  }
  