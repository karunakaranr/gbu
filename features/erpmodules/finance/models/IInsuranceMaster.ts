export interface IInsuranceMaster {
    InsuranceId: number
    InsuranceOUType: number
    OUGroupId: string
    OUGroupName: string
    OUId: string
    OUName: string
    InsuranceInsuranceType: number
    InsurancePolicyNumber: string
    VehicleId: string
    VehicleNumber: string
    PartyId: string
    PartyName: string
    AssetId: string
    AssetName: string
    EmployeeId: string
    EmployeeName: string
    MachineId: string
    MachineName: string
    InsuranceStartDate: string
    InsuranceEndDate: string
    InsuranceCoverageAmount: string
    InsurancePremiumAmount: string
    InsurancePeriodType: string
    InsuranceLastPaidOn: string
    InsuranceLastPaidTill: string
    InsurancePremiumDueOn: string
    InsuranceInsuranceOStatus: string
    InsuranceRemarks: string
    InsuranceStatus: number
    InsuranceCreatedByName: string
    InsuranceCreatedOn: string
    InsuranceModifiedByName: string
    InsuranceModifiedOn: string
    InsuranceVersion: number
  }
  