export interface IBudgetMaster {
    BudgetId: number
    BudgetAccountLevel: number
    BudgetCode: string
    BudgetName: string
    CostCategoryId: string
    CostCategoryName: string
    CriteriaConfigId: string
    CriteriaConfigName: string
    BudgetPeriodType: number
    BudgetFromDate: string
    BudgetToDate: string
    BudgetCompareFromDate: string
    BudgetCompareToDate: string
    GBPeriodId: string
    GBPeriodName: string
    CompareGBPeriodId: string
    CompareGBPeriodName: string
    BudgetVersion: number
    BudgetStatus: number
    BudgetRemarks: string
    BudgetCreatedByName: string
    BudgetCreatedOn: string
    BudgetModifiedByName: string
    BudgetModifiedOn: string
  }
  