export interface IDeferralPlan {
    DeferalPlanId: number
    DeferalPlanCode: string
    DeferalPlanName: string
    DeferalPlanRemarks: string
    DeferalAccountId: number
    DeferalAccountCode: string
    DeferalAccountName: string
    DeferalPlanDeferalPercent: number
    DeferalPlanPeriodType: number
    DeferalPlanPeriods: number
    DeferalPlanCalculationMethod: number
    DeferalPlanStartDateType: number
    DeferalPlanPeriodRemarksTemplate: string
    DeferalPlanCreatedById: number
    DeferalPlanCreatedOn: string
    DeferalPlanModifiedById: number
    DeferalPlanModifiedOn: string
    DeferalPlanSortOrder: number
    DeferalPlanStatus: number
    DeferalPlanVersion: number
    DeferalPlanSourceType: number
    DeferalPlanDays: number
    DeferalPlanMonths: number
  }
  