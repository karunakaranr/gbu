import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { GBHttpService } from '@goodbooks/gbcommon';
import { ItemCategorylistComponent } from 'features/erpmodules/inventory/screens/itemcategory/itemcategorylist/itemcategorylist.component';

@Injectable({
    providedIn: 'root',
})
export class CompanyVsLedgerDbService {
    rowcriteria;
    constructor(public http: GBHttpService) { }

    public picklist(): Observable<any> {
        const url = '/fws/Menu.svc/ReportMenuDetailsForMenu/?UserId=-1499998941&MenuId=-1399989008';
        return this.http.httpget(url);
    }


    public reportData(pickobj) {
        let criteria;
        let test = {

            "SectionCriteriaList": [
                {
                    "SectionId": 0,
                    "AttributesCriteriaList": [
                    ],
                    "OperationType": 1
                }
            ],
            
        }
        let xx = pickobj[0].WebServiceSettingArray[0].WebServiceSettingDetailArray
        console.log("xxxx",xx)
        for (let item of xx) {
            if(item.WebServiceSettingDetailCriteriaFieldValue!="NONE"){
            test.SectionCriteriaList[0].AttributesCriteriaList.push(
                {
                    FieldName: item.WebServiceCriteriaFieldName,
                    OperationType: "1",
                    FieldValue: item.CriteriaAttributeContextValueFieldName,
                    JoinType: "2",
                    CriteriaAttributeName: item.CriteriaAttributeName,
                    CriteriaAttributeValue:item.CriteriaAttributeName,
                    IsHeader:item.WebServiceSettingDetailIsHeader,
                    IsCompulsory:item.WebServiceSettingDetailIsCompulsory,
                    CriteriaAttributeId: item.CriteriaAttributeId,
                    CriteriaAttributeType:item.CriteriaAttributeType,
                    FilterType:"1"
                }
            )
            }
        }
        criteria = {
            "ReportUri": pickobj[0].WebServiceUriTemplate,
            "Method": pickobj[0].MethodType,
            "ReportTitle": "",
            "CriteriaDTO": test,
            "MenuId": pickobj[0].MenuId,
            "WebserviceId": pickobj[0].WebServiceId,
            "PeriodFilter": 0,
            "RecordsPerPage": 0
        }
        this.rowcriteria = criteria
        let url;
        console.log("pickobj", criteria)
        let fullurl = 'http://169.56.148.10:82/gb4/cos/Common.svc/Report';
        let str = fullurl.split('/gb4');
        let urlstr = str[str.length - 1];
        url = urlstr;
        console.log("api", url);
        return this.http.httppost(url, criteria);
    }
}