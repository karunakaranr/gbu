import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { GBHttpService } from '@goodbooks/gbcommon';

@Injectable({
    providedIn: 'root',
})
export class StatementOfAccountbservice {
    rowcriteria;
    constructor(public http: GBHttpService) { }

    public picklist(): Observable<any> {
        const url = '/fws/Menu.svc/ReportMenuDetailsForMenu/?UserId=-1499998941&MenuId=-1399996346';
        return this.http.httpget(url);
    }

    public reportData(pickobj) {

        let criteria;
        let test =
        {
            SectionCriteriaList: [
                {
                    "SectionId": 0,
                    "AttributesCriteriaList": [
                        {
                            "FieldName": "OUId",
                            "OperationType": "5",
                            "FieldValue": "-1500000000",
                            "JoinType": "2",
                            "CriteriaAttributeName": "OU",
                            "CriteriaAttributeValue": "TESTING COMPANY",
                            "IsHeader": "1",
                            "IsCompulsory": "1",
                            "CriteriaAttributeId": "-1899999999",
                            "CriteriaAttributeType": 0
                        },
                        {
                            "FieldName": "AccountType",
                            "OperationType": "5",
                            "FieldValue": "3",
                            "JoinType": "2",
                            "CriteriaAttributeName": "AccountType",
                            "CriteriaAttributeValue": "Sub Ledger",
                            "IsHeader": "1",
                            "IsCompulsory": "1",
                            "CriteriaAttributeId": "-1399999952",
                            "CriteriaAttributeType": 0
                        },
                        {
                            "FieldName": "VoucherDetailAccountId",
                            "OperationType": "5",
                            "FieldValue": "-1499993427",
                            "JoinType": "2",
                            "CriteriaAttributeName": "Voucher Account",
                            "CriteriaAttributeValue": "VJKA00002",
                            "IsHeader": "0",
                            "IsCompulsory": "1",
                            "CriteriaAttributeId": "-1399999947",
                            "CriteriaAttributeType": 0
                        },
                        {
                            "FieldName": "Type",
                            "OperationType": "5",
                            "FieldValue": "0",
                            "JoinType": "2",
                            "CriteriaAttributeName": "Type",
                            "CriteriaAttributeValue": "Normal",
                            "IsHeader": "1",
                            "IsCompulsory": "1",
                            "CriteriaAttributeId": "-1399999995",
                            "CriteriaAttributeType": 0
                        },
                        {
                            "FieldName": "Provision",
                            "OperationType": "5",
                            "FieldValue": "0",
                            "JoinType": "2",
                            "CriteriaAttributeName": "Provision",
                            "CriteriaAttributeValue": "Yes",
                            "IsHeader": "1",
                            "IsCompulsory": "1",
                            "CriteriaAttributeId": "-1399999663",
                            "CriteriaAttributeType": 0
                        },
                        {
                            "FieldName": "InfoClass",
                            "OperationType": "5",
                            "FieldValue": "0",
                            "JoinType": "2",
                            "CriteriaAttributeName": "Information",
                            "CriteriaAttributeValue": "Yes",
                            "IsHeader": "1",
                            "IsCompulsory": "1",
                            "CriteriaAttributeId": "-1399999200",
                            "CriteriaAttributeType": 0
                        },
                        {
                            "FieldName": "IsSingleAccountSelect",
                            "OperationType": "5",
                            "FieldValue": "0",
                            "JoinType": "2",
                            "CriteriaAttributeName": "IsAccountSelection",
                            "CriteriaAttributeValue": "Yes",
                            "IsHeader": "1",
                            "IsCompulsory": "1",
                            "CriteriaAttributeId": "-1399998995",
                            "CriteriaAttributeType": 0
                        },
                        {
                            "FieldName": "InstrumentStatus",
                            "OperationType": "5",
                            "FieldValue": "1",
                            "JoinType": "2",
                            "CriteriaAttributeName": "All/Realised",
                            "CriteriaAttributeValue": "Realised",
                            "IsHeader": "1",
                            "IsCompulsory": "1",
                            "CriteriaAttributeId": "-1399998946",
                            "CriteriaAttributeType": 0
                        },
                        {
                            "FieldName": "PeriodFromDate",
                            "OperationType": 10,
                            "FieldValue": "1648771200",
                            "JoinType": 0,
                            "CriteriaAttributeName": "From Date",
                            "CriteriaAttributeValue": "01/Apr/2022",
                            "IsHeader": 0,
                            "IsCompulsory": 0,
                            "CriteriaAttributeId": -2147483643,
                            "CriteriaAttributeType": 4
                        },
                        {
                            "FieldName": "PeriodToDate",
                            "OperationType": 11,
                            "FieldValue": "1680220800",
                            "JoinType": 0,
                            "CriteriaAttributeName": "To Date",
                            "CriteriaAttributeValue": "31/Mar/2023",
                            "IsHeader": 0,
                            "IsCompulsory": 0,
                            "CriteriaAttributeId": -2147483642,
                            "CriteriaAttributeType": 4
                        }
                    ],
                    "OperationType": 0
                }
            ]
        }

        criteria = {
            "ReportUri": pickobj[0].WebServiceUriTemplate,
            "Method": pickobj[0].MethodType,
            "ReportTitle": "Statement of Account",
            "CriteriaDTO": test,
            "MenuId": pickobj[0].MenuId,
            "WebserviceId": pickobj[0].WebServiceId,
            "PeriodFilter": 0,
            "RecordsPerPage": 0
        }
        this.rowcriteria = criteria
        let url;
        console.log("pickobj", criteria)
        let fullurl = 'http://169.56.148.10:82/gb4/cos/Common.svc/Report';
        let str = fullurl.split('/gb4');
        let urlstr = str[str.length - 1];
        url = urlstr;
        console.log("api", url);
        return this.http.httppost(url, criteria);
    }
    public CriteriaConfigWithFieldValue(id) {
        let urls = '/fws/CriteriaConfig.svc/CriteriaConfigWithFieldValue/?CriteriaConfigId=' + id;
        return this.http.httpget(urls);
    }
    public CriteriaConfigId(id) {
        let urls = '/fws/CriteriaConfig.svc/?CriteriaConfigId=' + id;
        return this.http.httpget(urls);
    }
    public reportDatacriteria(criterias) {
        this.rowcriteria.CriteriaDTO = criterias
        let url;
        let fullurl = 'http://169.56.148.10:82/gb4/cos/Common.svc/Report';
        let str = fullurl.split('/gb4');
        let urlstr = str[str.length - 1];
        url = urlstr;
        console.log("api", url);
        return this.http.httppost(url, this.rowcriteria);
    }
}
