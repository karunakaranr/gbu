import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { IReport } from './../../../../../commonreport/model/report.model';
import { Store } from '@ngxs/store';

@Component({
  selector: 'app-packlist',
  templateUrl: './packlist.component.html',
  styleUrls: ['./packlist.component.scss'],
})
export class PacklistComponent implements OnInit {
  title = 'Packlist'
  public dataArray: IReport;
  public rowData = [];
  public columnData = [];
  public defaultColDef = [];
  public type;

  constructor(private http: HttpClient, public store: Store) { }

  ngOnInit(): void {
    this.type = 'GridView';

  }
  public onOptionsSelected(e) {
    this.type = e;
  }

}


