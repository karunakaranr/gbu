
import { IPack } from '../../../models/IPack';
import { PackService } from '../../../services/Pack/pack.service';
import { PackDBService } from '../../../dbservices/Pack/packdb.service';
import { Store } from '@ngxs/store';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { GBBaseDataService, GBBaseDataServiceWN} from './../../../../../../libs/gbdata/src/lib/services/gbbasedata.service';
import {GBBaseDBDataService} from './../../../../../../libs/gbdata/src/lib/services/gbbasedbdata.service'
import {GBHttpService} from './../../../../../../libs/gbcommon/src/lib/services/HTTPService/GBHttp.service';
import {GbToasterService} from './../../../../../../libs/gbcommon/src/lib/services/toaster/gbtoaster.service';
import {GBDataPageService, GBDataPageServiceWN} from './../../../../../../libs/uicore/src/lib/services/gbpage.service';
export function getThisDBDataService(http: GBHttpService): GBBaseDBDataService<IPack> {
  return new PackDBService(http);
}
export function getThisDataServiceWN(dbDataService: GBBaseDBDataService<IPack>): GBBaseDataServiceWN<IPack> {
  return new PackService(dbDataService);
}
export function getThisDataService(dbDataService: GBBaseDBDataService<IPack>): GBBaseDataService<IPack> {
  return new PackService(dbDataService);
}
export function getThisGBPageService(store: Store, dataService: GBBaseDataService<IPack>, dbDataService: GBBaseDBDataService<IPack>, fb: FormBuilder, gbhttp:GBHttpService, activeroute:ActivatedRoute, router:Router,toaster:GbToasterService): GBDataPageService<IPack> {
  return new GBDataPageService<IPack>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
export function getThisGBPageServiceWN(store: Store, dataService: GBBaseDataServiceWN<IPack>, dbDataService: GBBaseDBDataService<IPack>, fb: FormBuilder, gbhttp:GBHttpService, activeroute:ActivatedRoute, router:Router,toaster:GbToasterService): GBDataPageServiceWN<IPack> {
  return new GBDataPageServiceWN<IPack>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
