import { IPack } from '../../../models/IPack';
import { HttpClient } from '@angular/common/http';
import { PackService } from '../../../services/Pack/pack.service';
import { Store } from '@ngxs/store';
import { PackStateActionFactory } from '../../../stores/Pack/pack.actionfactory';
import { GBDataFormGroupStoreWN, GBDataFormGroupWN } from './../../../../../../libs/uicore/src/lib/classes/GBFormGroup';
export class PackFormgroup extends GBDataFormGroupWN<IPack> {
  constructor(http: HttpClient, dataService: PackService) {
    super(http, 'SC002', {}, dataService)
  }

  public clear() {
    super.clear();
  }

  public delete() {
    super.delete();
  }

  public save() {
    super.save();
  }

  public moveFirst() {
    super.moveFirst();
  }

  public movePrev() {
    super.movePrev();
  }

  public moveNext() {
    super.moveNext();
  }

  public moveLast() {
    super.moveLast();
  }
}

export class PackFormgroupStore extends GBDataFormGroupStoreWN<IPack> {
  constructor(http: HttpClient, store: Store) {
    super(http, 'SC002', {}, store, new PackStateActionFactory())
  }

  public clear() {
    super.clear();
  }

  public delete() {
    super.delete();
  }

  public save() {
    super.save();
  }

  public moveFirst() {
    super.moveFirst();
  }

  public movePrev() {
    super.movePrev();
  }

  public moveNext() {
    super.moveNext();
  }

  public moveLast() {
    super.moveLast();
  }
}

