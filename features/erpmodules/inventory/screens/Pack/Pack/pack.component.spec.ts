import { FormBuilder } from '@angular/forms';
import {GBDataPageServiceWN} from './../../../../../../libs/uicore/src/lib/services/gbpage.service';
import { Store } from '@ngxs/store';
import {GBBaseDataServiceWN} from './../../../../../../libs/gbdata/src/lib/services/gbbasedata.service';
import {GBHttpService} from './../../../../../../libs/gbcommon/src/lib/services/HTTPService/GBHttp.service';
import {GBBaseDBDataService} from './../../../../../../libs/gbdata/src/lib/services/gbbasedbdata.service'
import { IPack } from './../../../models/IPack';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { PackComponent } from './pack.component';

const datajson = require('./pack.json'); // for Testing

describe('PackComponent', () => {
  let fixture: PackComponent;
  let formBuilderMock: GBDataPageServiceWN<IPack>;
  let store:Store;
  let dataService:GBBaseDataServiceWN<IPack>;
  let dbDataService:GBBaseDBDataService<IPack>;
  let fb = new FormBuilder();
  let gbhttp:GBHttpService;
  let activeroute=new ActivatedRoute();
  let router: Router;
  beforeEach(() => {
    formBuilderMock = new GBDataPageServiceWN(store,dataService,dbDataService,fb,gbhttp,activeroute,router);
    fixture = new PackComponent(
      formBuilderMock,
    );
    // fixture.ngOnInit();
  });

  describe('Test: ngOnInit', () => {
    it('check service reponse', () => {
      expect(fixture.loadScreen()).toEqual(datajson);
    });
  });

});