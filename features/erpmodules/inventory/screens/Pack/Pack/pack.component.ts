
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import {FormlyFieldConfig,FormlyFormOptions,} from '@ngx-formly/core';

import {GBBaseDataPageStoreComponentWN} from './../../../../../../libs/uicore/src/lib/components/Base/GBBasePage';

import { IPack } from './../../../models/IPack';
import {
  getThisGBPageService,
} from './pack.factory';
import {
  PackFormgroupStore,
} from './pack.formgroup';
import { PackService } from '../../../services/Pack/pack.service';
import { PackDBService } from '../../../dbservices/Pack/packdb.service';
import { Select, Store } from '@ngxs/store';
import {GBHttpService} from './../../../../../../libs/gbcommon/src/lib/services/HTTPService/GBHttp.service';
import { PackState } from '../../../stores/Pack/pack.state';
import { GetData } from '../../../stores/Pack/pack.actions';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
const datajson = require('./pack.json'); // for Testing

@Component({
  selector: 'app-pack',
  templateUrl: './pack.component.html',
  styleUrls: ['./pack.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'idfield' },
    { provide: 'url', useValue: 'url' },
    { provide: 'Store', useClass: Store },
    { provide: 'DBDataService', useClass: PackDBService, deps: [GBHttpService], },
    { provide: 'DataService', useClass: PackService, deps: ['DBDataService'] },
    {
      provide: 'PageService', useFactory: getThisGBPageService, deps: [Store,
        'DataService',
        'DBDataService',
        FormBuilder,
        GBHttpService,
        ActivatedRoute,
        Router,
      ],
    },
  ],
})
export class PackComponent extends GBBaseDataPageStoreComponentWN<IPack>
  implements OnInit {

  title = 'Pack';
  options: FormlyFormOptions = {};
  form: PackFormgroupStore;
  @Select(PackState) currentData$;

  thisConstructor() { }

  loadScreen(recid?: string) {
    if (recid) {
      const id = parseInt(recid);
      this.gbps.store.dispatch(new GetData(id));
    }
    let json = datajson; 
    console.log(datajson[0])
    return json;
  }

  ngOnInit(): void {
    let id = '';
    this.gbps.activeroute.params.subscribe((params: any) => {
      id = params['id'];

    });
    this.gbps.activeroute.paramMap.subscribe((params: ParamMap) => {
      id = params.get('id')

    });
    if (this.gbps.activeroute.firstChild) {
      this.gbps.activeroute.firstChild.params.subscribe((params: ParamMap) => {
        id = params['id'];


      });
    }
    this.loadScreen(id);
  }


  packformly = new FormGroup({});
  model = {
    code: '',
    Name: '',
    ConversionType: '',
    Factor: ''
  };
  fields: FormlyFieldConfig[] = [
    {
      key: 'code',
      type: 'input',
      templateOptions: {
        type: 'input',
        label: 'PackCode',
        placeholder: 'Code'
      }
    },
    {
      key: 'Name',
      type: 'input',
      templateOptions: {
        type: 'input',
        label: 'PackName',
        placeholder: 'Name'
      }
    },
    {
      key: 'ConversionType',
      type: 'select',
      templateOptions: {
        label: 'ConversionType',
        options:[
          {
            value:'0',
            label:'NoConversion'
          },
          {
            value:'1',
            label:'Fixed'
          },
          {
            value:'2',
            label:'varying'
          }
        ]
      }
    },
      {
        key: 'Factor',
        type: 'input',
        templateOptions: {
          label: 'ConversionFactor',
          placeholder: 'ConversionFactor'
        },
     
    },
  ];
}
