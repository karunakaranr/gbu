import { Component, Inject, LOCALE_ID, OnInit } from '@angular/core';
import { ItemGroupService } from './../../../services/ItemGroup/ItemGroup.service';
import { Select, Store } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { Observable } from 'rxjs';
import { SharedService } from 'features/commonreport/service/datapassing.service';

@Component({
  selector: 'app-ItemGroupList.component',
  templateUrl: './ItemGroupList.component.html',
  styleUrls: ['./ItemGroupList.component.scss']
})

export class ItemGroupListComponent implements OnInit {
  @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
  public rowData = [];
  itemgrouplist: any[] = [];

  constructor(public sharedService: SharedService,public store: Store, @Inject(LOCALE_ID) public locale: string) { }

  ngOnInit() {
    this.Getitemgrouplist();
  }


  Getitemgrouplist(): void {
    this.sharedService.getRowInfo().subscribe(data => {
      if (data.length > 0) {
      this.rowData = data;
      console.log("Item Group :", this.rowData)
      
      let json = this.rowData;
      var finalizedArray = [];
      // json.map((row) => {
      //   finalizedArray.push({
      //     parname: row['ParentName'],
      //     oucode: row['ItemGroupCode'],
      //     ouname: row['ItemGroupName'],
      //     strcode: row['ItemGroupRemarks'],
      //     strname: row['ItemGroupIsSelected'],
      //   });
      // });


      json.map((row) => {
        finalizedArray.push({
          parname: row['ParentName'],
          igcode: row['ItemGroupCode'],
          igname: row['ItemGroupName'],
          igremarks: row['ItemGroupRemarks'],
          igselect: row['ItemGroupIsSelected'],
        });
      });


      const final = {};
      finalizedArray.forEach((detail) => {
        final[detail.parname] = {
          accountGroups: {},
          igcode: detail.parname,
          igname: "",
          igremarks: "",
          igselect: "",
          ...final[detail.parname],
        };

        final[detail.parname].accountGroups[detail.ouname] = {
          accounts: {},
          igcode: detail.parname,
          igname:  detail.ouname,
          igremarks: "",
          igselect: "",
        
          ...final[detail.parname].accountGroups[detail.ouname],
        };
        final[detail.parname].accountGroups[detail.ouname].accounts[detail.igremarks] = {
          igcode: detail.igcode,
          igname: detail.igname,
          igremarks: detail.igremarks,
          igselect: detail.igselect,
        
        };
      });

      const grpcodes = Object.keys(final);

      const tableData = [];
      grpcodes.forEach((igcode) => {
        const accountGroups = Object.keys(final[igcode].accountGroups);

        // tableData.push({
        //   strcode: final[strcode].strcode,
        //   strname: "",
        //   bold: true,
        // });

        accountGroups.forEach((ag) => {
          tableData.push({
            igcode: final[igcode].accountGroups[ag].igcode,
            // igname: final[igcode].accountGroups[ag].igname,
            bold: true,
          });

          const accounts = Object.keys(final[igcode].accountGroups[ag].accounts);
          accounts.forEach((account) => {
            tableData.push({
              igcode: final[igcode].accountGroups[ag].accounts[account].igcode,
              igname: final[igcode].accountGroups[ag].accounts[account].igname,
              igremarks: final[igcode].accountGroups[ag].accounts[account].igremarks,
              igselect: final[igcode].accountGroups[ag].accounts[account].igselect,
              
            });
          });
        });
      });

        for (let data of tableData) {
          
          if (data.igselect == "0") {
            data.igselect = "Yes"
          }
          else
          {
            data.igselect = "No"
          }

        }
        this.itemgrouplist = tableData;
      }
    });
  }

}

