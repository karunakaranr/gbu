import { Component, OnInit } from '@angular/core';
import { PartyListService } from './../../../services/PartyList/PartyList.service'

const col = require('./../../../../../../apps/Goodbooks/Goodbooks/src/assets/data/PartyList.json')

@Component({
  selector: 'app-PartyListView.component',
  templateUrl: './PartyListView.component.html',
  styleUrls: ['./PartyListView.component.scss']
})

export class PartyListViewComponent implements OnInit {
  title = 'Party List'

  public rowData = []
  public columnData = []


  constructor(
    private partylistservice: PartyListService) { }

  ngOnInit() {
    this.Getpartylistview();
  }


  Getpartylistview(): void {
    this.partylistservice.Getpartylistview().subscribe(menuDetails => {
      const menuDet = menuDetails;
      this.columnData = col;
      this.partylistservice.Rowservice(menuDet).subscribe(res => {
        this.rowData = res.ReportDetail;

        for (let data of this.rowData) {
          if (data) {
        if(data.PartyIsServiceProvider == 0)
            {
            data.PartyIsServiceProvider = "Yes";
            }
            else if(data.PartyIsServiceProvider == 1)
            {
            data.PartyIsServiceProvider = "No";
            }
       if(data.PartyIsVendor == 0)
            {
            data.PartyIsVendor = "Yes";
            }
            else if(data.PartyIsVendor == 1)
            {
            data.PartyIsVendor = "No";
            }
       if(data.PartyIsAccount == 0)
            {
            data.PartyIsAccount = "Yes";
            }
            else if(data.PartyIsAccount == 1)
            {
            data.PartyIsAccount = "No";
            }
        if(data.PartyIsCustomer == 0)
            {
            data.PartyIsCustomer = "Yes";
            }
            else if(data.PartyIsCustomer == 1)
            {
            data.PartyIsCustomer = "No";
            }
        if(data.PartyIsProspect == 0)
            {
            data.PartyIsProspect = "Yes";
            }
            else if(data.PartyIsProspect == 1)
            {
            data.PartyIsProspect = "No";
            }
            



      }
    }
  });
})

}

}
