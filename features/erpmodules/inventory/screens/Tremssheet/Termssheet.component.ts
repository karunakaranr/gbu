import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ITermssheet } from '../../models/ITermssheet';
import { TermssheetService } from '../../services/Termssheet/Termssheet.service';
import { ButtonVisibility } from 'features/layout/store/layout.actions';

import * as Termssheetjson from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/Termssheet.json'
import { InventoryURLS } from '../../URLS/urls';

@Component({
  selector: "app-Termssheet",
  templateUrl: "./Termssheet.component.html",
  styleUrls: ["./Termssheet.component.scss"],
  providers: [
    { provide: 'IdField', useValue: 'TermsSetId' },
    { provide: 'url', useValue: InventoryURLS.Termssheet },
    { provide: 'DataService', useClass: TermssheetService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class TermssheetComponent extends GBBaseDataPageComponentWN<ITermssheet> {
  title: string = "Termssheet"
  Termssheetjson = Termssheetjson;



  form: GBDataFormGroupWN<ITermssheet> = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'Termssheet', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.TermssheetFillFunction(arrayOfValues.TermsSetId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }


  public TermssheetFillFunction(SelectedPicklistData: string): void {

   console.log("filling",SelectedPicklistData)
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(Termssheet => {
        this.form.patchValue(Termssheet);
      })
    }
  }


  public TermssheetPatchValue(SelectedPicklistDatas: any): void {
    
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
    console.log("iteem==>",SelectedPicklistDatas)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<ITermssheet> {
  const dbds: GBBaseDBDataService<ITermssheet> = new GBBaseDBDataService<ITermssheet>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<ITermssheet>, dbDataService: GBBaseDBDataService<ITermssheet>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<ITermssheet> {
  return new GBDataPageService<ITermssheet>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
