import { HttpClient } from '@angular/common/http';
import { Component, Inject, LOCALE_ID, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { GridProperties, GridSetting } from '../../../models/Iitemcategory';
import { Select, Store } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { SharedService } from 'features/commonreport/service/datapassing.service';


@Component({
  selector: 'app-StockLedgerDetail',
  templateUrl: './StockLedgerDetail.component.html',
  styleUrls: ['./StockLedgerDetail.component.scss'],
})
export class StockLedgerDetailComponent implements OnInit {
  @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
  public rowData = [];
  public TotalValue;
  constructor(public sharedService: SharedService,public store: Store) { }

  ngOnInit(): void {
    this.getstockledgerdetail();
  }

  public getstockledgerdetail() {
    this.sharedService.getRowInfo().subscribe(data => {
      if (data.length > 0) {
      this.rowData = data;
      console.log("Stock Ledger Detail :", this.rowData)



      // this.sumquantity = 0;
      // this.sumgoodquantity = 0;
      let json = this.rowData;
      var finalizedArray = [];
      // for (let data of this.rowData) {
      //   this.sumquantity = this.sumquantity + data.Quantity;
      //   this.sumgoodquantity = this.sumgoodquantity + data.GoodQuantity;
      // }
      json.map((row, index) => {
        finalizedArray.push({
          sno: index,
          code: row['Code'],
          name: row['Name'],

          icode: row['ItemCode'],
          iname: row['ItemName'],
          uom: row['UOM'],
          openstock: row['OpeningStock'],
          openvalue: row['OpeningValue'],
          inqty: row['InwardQuantity'],
          inptval: row['InwardPostedValue'],
          outqty: row['OutwardQuantity'],
          outptval: row['OutwardPostedValue'],
          clstock: row['ClosingStock'],
          clval: row['ClosingValue']
        });
      });


      const final = {};
      finalizedArray.forEach((detail) => {
        final[detail.code] = {
          accountGroups: {},
          
          icode: detail.code,
          iname: detail.name,
          uom: "",
          openstock: 0,
          openvalue: 0,
          inqty: 0,
          inptval: 0,
          outqty: 0,
          outptval: 0,
          clstock: 0,
          clval: 0,
          ...final[detail.code],
        };


        final[detail.code].accountGroups[detail.sno] = {
          icode: detail.icode,
          iname: detail.iname,
          uom: detail.uom,
          openstock: detail.openstock,
          openvalue: detail.openvalue,
          inqty: detail.inqty,
          inptval: detail.inptval,
          outqty: detail.outqty,
          outptval: detail.outptval,
          clstock: detail.clstock,
          clval: detail.clval
        };
      });

      const grpcodes = Object.keys(final);

      const tableData = [];
      grpcodes.forEach((code) => {
        const accountGroups = Object.keys(final[code].accountGroups);
        let sumopenquantity = 0;
        let sumopenvalue = 0;
        let suminquantity = 0;
        let suminvalue = 0;
        let sumoutquantity = 0;
        let sumoutvalue = 0;
        let sumclosequantity = 0;
        let sumclosevalue = 0;

        accountGroups.forEach((ag) => {
          sumopenquantity = sumopenquantity + parseFloat(final[code].accountGroups[ag].openstock);
          sumopenvalue = sumopenvalue + parseFloat(final[code].accountGroups[ag].openvalue);
          suminquantity = suminquantity + parseFloat(final[code].accountGroups[ag].inqty);
          suminvalue = suminvalue + parseFloat(final[code].accountGroups[ag].inptval);
          sumoutquantity = sumoutquantity + parseFloat(final[code].accountGroups[ag].outqty);
          sumoutvalue = sumoutvalue + parseFloat(final[code].accountGroups[ag].outptval);
          sumclosequantity = sumclosequantity + parseFloat(final[code].accountGroups[ag].clstock);
          sumclosevalue = sumclosevalue + parseFloat(final[code].accountGroups[ag].clval);
        })
        final[code].openstock = sumopenquantity;
        final[code].openvalue = sumopenvalue;
        final[code].inqty = suminquantity;
        final[code].inptval = suminvalue;
        final[code].outqty = sumoutquantity;
        final[code].outptval = sumoutvalue;
        final[code].clstock = sumclosequantity;
        final[code].clval = sumclosevalue;

        tableData.push({
          icode: final[code].icode,
          iname: final[code].iname,
          uom: "",
          openstock: final[code].openstock,
          openvalue: final[code].openvalue,
          inqty: final[code].inqty,
          inptval: final[code].inptval,
          outqty: final[code].outqty,
          outptval: final[code].outptval,
          clstock: final[code].clstock,
          clval: final[code].clval,
          bold: true,
        });

        const accounts = Object.keys(final[code].accountGroups);
        accounts.forEach((account) => {
          tableData.push({
            icode: final[code].accountGroups[account].icode,
            iname: final[code].accountGroups[account].iname,
            uom: final[code].accountGroups[account].uom,
            openstock: final[code].accountGroups[account].openstock,
            openvalue: final[code].accountGroups[account].openvalue,
            inqty: final[code].accountGroups[account].inqty,
            inptval: final[code].accountGroups[account].inptval,
            outqty: final[code].accountGroups[account].outqty,
            outptval: final[code].accountGroups[account].outptval,
            clstock: final[code].accountGroups[account].clstock,
            clval: final[code].accountGroups[account].clval

          });
        });
      });
      this.rowData = tableData;
      console.log("Stock Ledger Detail Data:", this.rowData)
    }
    });
  }

}
