import { HttpClient } from '@angular/common/http';
import { Component, Inject, LOCALE_ID, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { GridProperties, GridSetting } from '../../../models/Iitemcategory';
import { Select, Store } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { SharedService } from 'features/commonreport/service/datapassing.service';


@Component({
  selector: 'app-StockPositionSummary',
  templateUrl: './StockPositionSummary.component.html',
  styleUrls: ['./StockPositionSummary.component.scss'],
})
export class stockpositionComponent implements OnInit {
  @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
  public rowData = [];
  public TotalValue;
  constructor(public sharedService: SharedService,public store: Store) { }

  ngOnInit(): void {
    this.stockposition();
  }
  
  public stockposition() {
    this.sharedService.getRowInfo().subscribe(data => {
      if (data.length > 0) {
      this.rowData = data;
      console.log("Stock Position Summary :", this.rowData)

      this.TotalValue = 0;
      for (let data of this.rowData) {
        this.TotalValue = this.TotalValue + data.PostedValue;
    }
  }
    });
  }

}
