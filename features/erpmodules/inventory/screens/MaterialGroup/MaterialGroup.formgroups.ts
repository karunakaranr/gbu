import { MaterialGroup } from '../../models/IMaterialGroup';
import { GBDataFormGroupStoreWN, GBDataFormGroupWN } from './../../../../../libs/uicore/src/lib/classes/GBFormGroup';
import { HttpClient } from '@angular/common/http';
import { MaterialGroupService } from '../../services/MaterialGroup/MaterialGroup.service';
import { Store } from '@ngxs/store';
import { MaterialGroupStateActionFactory } from '../../stores/MaterialGroup/MaterialGroup.actionfactory';

export class MaterialGroupFormgroup extends GBDataFormGroupWN<MaterialGroup> {
  constructor(http: HttpClient, dataService: MaterialGroupService) {
    super(http, 'SC0017', {}, dataService)
  }

  public clear() {
    super.clear();
  }

  public delete() {
    super.delete();
  }

  public save() {
    super.save();
  }

  public moveFirst() {
    super.moveFirst();
  }

  public movePrev() {
    super.movePrev();
  }

  public moveNext() {
    super.moveNext();
  }

  public moveLast() {
    super.moveLast();
  }
}
export class MaterialGroupFormgroupStore extends GBDataFormGroupStoreWN<MaterialGroup> {
  constructor(http: HttpClient, store: Store) {
    super(http, 'SC0017', {}, store, new MaterialGroupStateActionFactory())
  }

  public clear() {
    super.clear();
  }

  public delete() {
    super.delete();
  }

  public save() {
    super.save();
  }

  public moveFirst() {
    super.moveFirst();
  }

  public movePrev() {
    super.movePrev();
  }

  public moveNext() {
    super.moveNext();
  }

  public moveLast() {
    super.moveLast();
  }
}

