import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { MaterialGroupService } from '../../services/MaterialGroup/MaterialGroup.service';
import { IMaterialGroup } from '../../models/IMaterialGroup';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { IBaseField, IPickListDetail } from 'libs/uicore/src/lib/interfaces/ibasefield';

import { InventoryURLS } from 'features/erpmodules/inventory/URLS/urls';

import * as MaterialGroupJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/MaterialGroup.json';




@Component({
  selector: 'app-materialgroup',
  templateUrl: './MaterialGroup.component.html',
  styleUrls: ['./MaterialGroup.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'MaterialGroupId' },
    { provide: 'url', useValue: InventoryURLS.MaterialGroup },
    { provide: 'DataService', useClass: MaterialGroupService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class MaterialGroupComponent extends GBBaseDataPageComponentWN<IMaterialGroup> {
  title: string = "MaterialGroup"
  MaterialGroupJSON = MaterialGroupJSON

  form: GBDataFormGroupWN<IMaterialGroup> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "MaterialGroup", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.MaterialGroupRetrivalValue(arrayOfValues.MaterialGroupId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {

      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }





  public MaterialGroupRetrivalValue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(materialgroup => {
        if (SelectedPicklistData != '0') {
          this.form.patchValue(materialgroup);
        }
      })
    }
  }


  public MaterialGroupPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IMaterialGroup> {
  const dbds: GBBaseDBDataService<IMaterialGroup> = new GBBaseDBDataService<IMaterialGroup>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IMaterialGroup>, dbDataService: GBBaseDBDataService<IMaterialGroup>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IMaterialGroup> {
  return new GBDataPageService<IMaterialGroup>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
