import { HttpClient } from '@angular/common/http';
import { Component, Inject, LOCALE_ID, OnInit } from '@angular/core';
// import { ItemcategoryService } from 'features/erpmodules/inventory/services/itemcategory/itemcategory.service';
import { Observable } from 'rxjs';
import { GridProperties, GridSetting } from '../../../models/Iitemcategory';
import { Select, Store } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { SharedService } from 'features/commonreport/service/datapassing.service';


@Component({
  selector: 'app-itemcategorylist',
  templateUrl: './itemcategorylist.component.html',
  styleUrls: ['./itemcategorylist.component.scss'],
})
export class ItemCategorylistComponent implements OnInit {
  @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
  public rowData = [];  
  
  constructor(public sharedService: SharedService,public store: Store, @Inject(LOCALE_ID) public locale: string) { }
  ngOnInit(): void {
    this.brandlist();
  }
  
  public brandlist() {
    this.sharedService.getRowInfo().subscribe(data => {
      if (data.length > 0) {
      this.rowData = data;
      console.log("Item Category :", this.rowData)
      }
    });
  }
}
