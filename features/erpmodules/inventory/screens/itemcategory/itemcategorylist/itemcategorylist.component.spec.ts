import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ItemCategorylistComponent } from './itemcategorylist.component';

describe('ItemCategorylistComponent', () => {
  let component: ItemCategorylistComponent;
  let fixture: ComponentFixture<ItemCategorylistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemCategorylistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemCategorylistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
