import { Component, OnInit } from "@angular/core";
import { CostcenterService } from "features/erpmodules/inventory/services/costcenter/costcenter.service";

@Component({
  selector: 'app-costcenterlist',
  templateUrl: './costcenterlist.component.html',
  styleUrls: ['./costcenterlist.component.scss']
})
export class CostcenterlistComponent implements OnInit {

    ccList: any[] = [];

    constructor(
        private costcenterService: CostcenterService
    ) { }

    ngOnInit() {
        this.getCostCenterList();
    }

    getCostCenterList(): void{
        this.costcenterService.getCostcenterList().subscribe(menuDetails => {
            const menuDet = menuDetails;
            this.costcenterService.rowService(menuDet).subscribe(res => {
                this.prepareTableData(res.ReportDetail);
            });
        });
    }

    prepareTableData(data: any[]): void {
        const final = {};
        const tableData = [];
        let ccTypeCodes = [];

        data.forEach((detail) =>{
            final[detail.CostCenterTypeCode] = {
                ccGroups: [],
                code: detail.CostCenterTypeName,
                name: '',
                shortName: '',
                ...final[detail.CostCenterTypeCode]
            };
        
            final[detail.CostCenterTypeCode].ccGroups.push({
                code: detail.CostCenterCode,
                name: detail.CostCenterName,
                shortName: detail.CostCenterShortName
            });
        });

        ccTypeCodes = Object.keys(final);

        ccTypeCodes.forEach((ccTypeCode) => {
            tableData.push({
                code: final[ccTypeCode].code,
                name: final[ccTypeCode].name,
                shortName: final[ccTypeCode].shortName,
                bold: true
            });
            final[ccTypeCode].ccGroups.forEach((ccGroup) => {
                tableData.push({
                    code: ccGroup.code,
                    name: ccGroup.name,
                    shortName: ccGroup.shortName,
                    bold: false
                });
            });  
        });

        this.ccList = tableData;
    }
    
}





  