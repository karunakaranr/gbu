import { Component, OnInit } from '@angular/core';
import { DashboardFunction } from 'libs/gbcommon/src/lib/services/DashboardFunction/dashboard.service';
import { IBomTree } from '../../models/IBomTree';
import { Swiper } from 'swiper';
import { SharedService } from 'features/commonreport/service/datapassing.service';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { Observable } from 'rxjs';
import { Select } from '@ngxs/store';
import { ICriteriaDTODetail } from 'features/erpmodules/samplescreens/models/IFilterDetail';
import OrgChart from '@balkangraph/orgchart.js';

@Component({
  selector: 'app-BomTree',
  templateUrl: './BomTree.component.html',
  styleUrls: ['./BomTree.component.scss'],
})
export class BomTreeComponent implements OnInit {
  @Select(ReportState.Sourcecriteriadrilldown) filtercriteria$: Observable<any>;
  BomTreeData: any;
  BomTreeModifiedData: any;
  Orientation: any = OrgChart.orientation.top;
  OrientationBtn: boolean = false;

  constructor(
    public sharedService: SharedService,
    public commonfunction: DashboardFunction
  ) {}

  ngOnInit(): void {
    this.sharedService.getRowInfo().subscribe((data) => {
   
      // console.log('BomTree Data : ', this.BomTreeData);
      if (data != undefined) {
        if (data.length > 0) {
          console.log('ngOnInit data : ', data);

          this.BomTreeData = data;
          this.DuplicateObject(this.BomTreeData);

          console.log('Duplicate : ', this.BomTreeData);

          this.drawHierarchy();
        }
      }
    });
  }

  private drawHierarchy() {
    console.log('draw hie', this.BomTreeModifiedData);
    const newData = this.BomTreeModifiedData;
    const tree = document.getElementById('tree1');
    if (tree) {
      var chart = new OrgChart(tree, {
        //nodeMouseClick: OrgChart.action.edit, // edit mode
        //nodeMouseClick: OrgChart.action.details, // details mode
        nodeMouseClick: OrgChart.action.none, // prevent opening edit form
        template: 'dynamic',
        layout: OrgChart.mixed,
        orientation: this.Orientation,
        enableSearch: false,
        showYScroll: OrgChart.scroll.visible,
        showXScroll: OrgChart.scroll.visible,
        mouseScrool: OrgChart.action.scroll,
        nodeBinding: {
          field_0: 'DetailItemName',
          field_1: 'RequiredQuantity',
          img_0: 'ItemPhoto',
        },
        toolbar: {
          fullScreen: true,
          zoom: true,
          fit: true,
          expandAll: true,
        },
      });

      OrgChart.templates.dynamic = Object.assign(
        {},
        OrgChart.templates.deborah
      );
      OrgChart.templates.dynamic.size = [300, 150];

      OrgChart.templates.dynamic.node =
        '<rect x="0" y="0" height="{h}" width="{w}" fill="#039BE5" stroke-width="1" stroke="#aeaeae" rx="5" ry="5"></rect>';

      OrgChart.templates.dynamic.img_0 =
        '<clipPath id="ulaImg">' +
        '<circle cx="150" cy="75" r="40"></circle>' +
        '</clipPath>' +
        '<image preserveAspectRatio="xMidYMid slice" clip-path="url(#ulaImg)" xlink:href="{val}" x="110" y="35" width="80" height="80">' +
        '</image>';

      OrgChart.templates.dynamic.field_0 =
        '<text data-width="270" style="font-size: 18px;" fill="#6e3ab6" x="150" y="25" text-anchor="middle">{val}</text>';

      OrgChart.templates.dynamic.field_1 =
        '<text data-width="270" style="font-size: 11px;" fill="#6e3ab6" x="30" y="135" text-anchor="middle">{val}</text>';

      chart.on('click', function (sender, args) {
        let index = newData.indexOf(
          newData.find(function (el, index) {
            return el['id'] === args.node.id;
          })
        );

        alert(
          `Code : ${newData[index].DetailItemCode}\nName : ${newData[index].DetailItemName}\nParent Item Code : ${newData[index].ParentItemCode}\nParent Item Name : ${newData[index].ParentItemName}\nRequired Quantity : ${newData[index].RequiredQuantity}`
        );

        console.log('index', index);

        console.log(args);
        console.log(sender);
        // sender.editUI.show(args.node.id, false);
        // sender.editUI.show(args.node.id, true);  details mode
        return false; //to cancel the click event
      });

      chart.load(this.BomTreeModifiedData);
    }
  }

  changeOrientation() {
    this.Orientation =
      this.Orientation === OrgChart.orientation.left
        ? OrgChart.orientation.top
        : OrgChart.orientation.left;
    this.OrientationBtn = !this.OrientationBtn;
    this.drawHierarchy();
  }

  private DuplicateObject(array: any[]) {
    if (array.length > 0) {
      // Retrieve the first object from the array
      let firstObject = array[0];

      // Clone the first object
      let duplicatedObject = Object.assign({}, firstObject);

      // Optionally, you can modify properties of the duplicated object here if needed

      // Add the duplicated object to the beginning of the array
      array.unshift(duplicatedObject);
    }

    this.BomTreeModifiedData = this.modifyArrayOfObjects(this.BomTreeData);
    console.log('BomTreeModified Data : ', this.BomTreeModifiedData);
  }

  // modify Array Of Objects for OrgChart

  private modifyArrayOfObjects(array: any[]) {
    return array.map((obj, index) => {
      console.log(index);

      if (index == 0) {
        obj.id = obj.ItemId;

        obj.DetailItemName = obj.ParentItemName;
        obj.DetailItemCode = obj.ParentItemCode;
        obj.ParentItemName = '';
        obj.ParentItemCode = '';

        obj.pid = -1;
      } else {
        obj.id = obj.DetailItemId;

        obj.pid = obj.ParentItemId;
      }

      obj.ItemPhoto = 'assets/BomTree/Bearing.jpg';
      return obj;
    });
  }
}
