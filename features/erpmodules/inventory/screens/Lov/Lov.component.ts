import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ILov } from '../../models/ILov';
import { LovService } from '../../services/Lov/Lov.service';
import { InventoryURLS } from '../../URLS/urls';
import * as LovJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/Lov.json'
import { ButtonVisibility } from 'features/layout/store/layout.actions';
@Component({
  selector: 'app-Lov',
  templateUrl: 'Lov.component.html',
  styleUrls:['Lov.component.scss'],
  providers: [
    { provide: 'IdField', useValue:'LovId' },
    { provide: 'url', useValue:InventoryURLS.Lov},
    { provide: 'DataService', useClass: LovService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class LovComponent extends GBBaseDataPageComponentWN<ILov> {
  title: string = "Lov"
  LovJSON= LovJSON;

  form: GBDataFormGroupWN<ILov> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "Lov", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.Lovvalue(arrayOfValues.LovId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }
  public Lovvalue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(Lov => {
          this.form.patchValue(Lov);
      })
    }
  }
  public LovNot(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}
export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<ILov> {
  const dbds: GBBaseDBDataService<ILov> = new GBBaseDBDataService<ILov>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<ILov>, dbDataService: GBBaseDBDataService<ILov>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<ILov> {
  return new GBDataPageService<ILov>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
