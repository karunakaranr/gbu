import { GBBaseDataService, GBBaseDataServiceWN} from './../../../../../../libs/gbdata/src/lib/services/gbbasedata.service';
import {GBBaseDBDataService} from './../../../../../../libs/gbdata/src/lib/services/gbbasedbdata.service'
import {GBHttpService} from './../../../../../../libs/gbcommon/src/lib/services/HTTPService/GBHttp.service';
import {GbToasterService} from './../../../../../../libs/gbcommon/src/lib/services/toaster/gbtoaster.service';
import { IUOM } from '../../../models/IUOM';
import { UOMService } from '../../../services/UOM/uom.service';
import { UOMDBService } from '../../../dbservices/UOM/uomdb.service';
import { Store } from '@ngxs/store';
import { FormBuilder } from '@angular/forms';
import {GBDataPageService, GBDataPageServiceWN} from './../../../../../../libs/uicore/src/lib/services/gbpage.service';
import { ActivatedRoute, Router } from '@angular/router';

// export function getThisDBDataService(http: GBHttpService): GBBaseDBDataService<IUOM> {
//   return new UOMDBService(http);
// }
// export function getThisDataServiceWN(dbDataService: GBBaseDBDataService<IUOM>): GBBaseDataServiceWN<IUOM> {
//   return new UOMService(dbDataService);
// }
// export function getThisDataService(dbDataService: GBBaseDBDataService<IUOM>): GBBaseDataService<IUOM> {
//   return new UOMService(dbDataService);
// }
export function getThisGBPageService(store: Store, dataService: GBBaseDataService<IUOM>, dbDataService: GBBaseDBDataService<IUOM>, fb: FormBuilder, gbhttp:GBHttpService, activeroute:ActivatedRoute, router:Router,toaster:GbToasterService): GBDataPageService<IUOM> {
  return new GBDataPageService<IUOM>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
export function getThisGBPageServiceWN(store: Store, dataService: GBBaseDataServiceWN<IUOM>, dbDataService: GBBaseDBDataService<IUOM>, fb: FormBuilder, gbhttp:GBHttpService, activeroute:ActivatedRoute, router:Router, toaster:GbToasterService): GBDataPageServiceWN<IUOM> {
  return new GBDataPageServiceWN<IUOM>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
