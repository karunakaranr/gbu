import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UOMlistComponent } from './uomlist.component';

describe('PacklistComponent', () => {
  let component: UOMlistComponent;
  let fixture: ComponentFixture<UOMlistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UOMlistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UOMlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
