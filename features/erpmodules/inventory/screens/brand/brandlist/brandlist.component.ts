import { Component, OnInit } from '@angular/core';
import { BrandService } from 'features/erpmodules/inventory/services/brand/brand.service';
import { Observable } from 'rxjs';
import {ReportState} from './../../../../../commonreport/datastore/commonreport.state';
import {CommonReportService} from  './../../../../../commonreport/service/commonreport.service'
import { Select } from '@ngxs/store';
import { ActivatedRoute} from '@angular/router';
import { SharedService } from 'features/commonreport/service/datapassing.service';

const col = require('./../../../../../../apps/Goodbooks/Goodbooks/src/assets/data/brandlist.json')


@Component({
  selector: 'app-brandlist',
  templateUrl: './brandlist.component.html',
  styleUrls: ['./brandlist.component.scss'],
})
export class BrandlistComponent implements OnInit {
  title = 'Brandlist'
  public rowData = [];
  public columnData = [];
  public defaultColDef = [];
  constructor(public sharedService: SharedService,public service: BrandService , public report:CommonReportService ,private route: ActivatedRoute,) { }
  @Select(ReportState.CriteriaConfigArray) rowcerteria: Observable<any>;
  arrayOfValues: Array<string>;
  public state = '';
  ngOnInit(): void {
    
    this.brandlist();
  }
  
  public brandlist() {
    this.sharedService.getRowInfo().subscribe(data => {
      if (data.length > 0) {
      this.rowData = data;
      console.log("Brand List :", this.rowData)
      }
  })
  }
  }
