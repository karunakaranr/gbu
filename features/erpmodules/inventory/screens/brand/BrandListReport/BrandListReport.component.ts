import { Component, Inject, LOCALE_ID, OnInit } from '@angular/core';
import { ItemBrandListService } from './../../../services/brand/BrandListReport.service'
import { Select, Store } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { Observable } from 'rxjs';

const col = require('./../../../../../../apps/Goodbooks/Goodbooks/src/assets/data/BrandListReport.json')

@Component({
  selector: 'app-BrandListReport.component',
  templateUrl: './BrandListReport.component.html',
  styleUrls: ['./BrandListReport.component.scss']
})

export class ItemBrandReportComponent implements OnInit {
  @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
  public rowData = []

  constructor(public store: Store, @Inject(LOCALE_ID) public locale: string) { }
  ngOnInit(): void {
    this.itembrandlist();
  }
  public itembrandlist() {
    this.rowdatacommon$.subscribe(data => {
      this.rowData = data;
      console.log("Brand :", this.rowData);
    

        // Create a new array to store the updated data
        const updatedRowData = [];

        for (let data of this.rowData) {
          let updatedData = Object.assign({}, data); // Create a copy of the original object

          if (data.ItemBrandIsSelected === 0) {
            updatedData.ItemBrandIsSelected = "Yes";
          } else if (data.ItemBrandIsSelected === 1) {
            updatedData.ItemBrandIsSelected = "No";
          }

          updatedRowData.push(updatedData);
        }

        // Replace the original rowData with the updated array
        this.rowData = updatedRowData;

        console.log("Brand :", this.rowData);

      });
    }
}