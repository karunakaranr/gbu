import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ItemOuService } from '../../services/ItemOU/Itemou.service';
import { IItemOU } from '../../models/IItemOU';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as ItemOUJson from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/ItemOU.json';
import { InventoryURLS } from '../../URLS/urls';


@Component({
  selector: 'app-ItemOU',
  templateUrl: './itemou.component.html',
  styleUrls: ['./itemou.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'ItemOUId'},
    { provide: 'url', useValue: InventoryURLS.ItemOU},
    { provide: 'saveurl', useValue: InventoryURLS.ItemOUsave },
    { provide: 'deleteurl', useValue: InventoryURLS.ItemOUsave },
    { provide: 'DataService', useClass: ItemOuService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url','saveurl', 'deleteurl'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class ItemOUComponent extends GBBaseDataPageComponentWN<IItemOU > {
  title: string = "ItemOU"
  ItemOUJson = ItemOUJson;


  form: GBDataFormGroupWN<IItemOU > = new GBDataFormGroupWN(this.gbps.gbhttp.http, "ItemOU", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.ItemOURetrivalValue(arrayOfValues.ItemOUId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }

  public leadtimePatch(itemou){
    console.log("itemou",itemou)
        this.form.get('ItemOULeadTimeOffSet').patchValue(itemou)
      }

  public ItemOURetrivalValue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.postData(SelectedPicklistData).subscribe(ItemOU => {
        this.form.patchValue(ItemOU[0]);
      })
    }
  }


  public ItemOUPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }

  public ChangeTab(event, TabName:string) :void{
    var i:number, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("FormTabLinks");
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(TabName).style.display = "block";
    console.log("Event:",event)
    event.currentTarget.className += " active";
  }

  
}


export function getThisDBDataService(http: GBHttpService, url: string,saveurl: string, deleteurl:string): GBBaseDBDataService<IItemOU> {
  const dbds: GBBaseDBDataService<IItemOU > = new GBBaseDBDataService<IItemOU >(http);
  dbds.endPoint = url;
  dbds.saveurl = saveurl;
  dbds.deleteurl = deleteurl;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IItemOU >, dbDataService: GBBaseDBDataService<IItemOU >, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IItemOU > {
  return new GBDataPageService<IItemOU >(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
