import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { InventoryURLS } from '../../URLS/urls';
import { SimpleItemService } from '../../services/SimpleITem/simpleitem.service';
import { ISimpleItem } from '../../models/ISimpleItem';
import * as SimpleItemJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/SimpleItem.json';
@Component({
    selector: 'app-SimpleItem',
    templateUrl:'./simpleitem.component.html',
    styleUrls: ['./simpleitem.component.scss'],
    providers: [
        { provide: 'IdField', useValue: 'ItemId' },
        { provide: 'url', useValue: InventoryURLS.SimpleItem },
        { provide: 'DataService', useClass: SimpleItemService },
        { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
        { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
    ]
})
export class SimpleItemComponent extends GBBaseDataPageComponentWN<ISimpleItem> {
    title: string = "SimpleItem"
    SimpleItemJSON = SimpleItemJSON

    form: GBDataFormGroupWN<ISimpleItem> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "SimpleItem", {}, this.gbps.dataService);
    thisConstructor() {
        let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
        if (arrayOfValues != null) {
            this.SimpleItemPicklistFillValue(arrayOfValues.SimpleItemId)
            this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
        } else {
            this.store.dispatch(new ButtonVisibility("Forms"))
        }
    }
  
    public SimpleItemPicklistFillValue(SelectedPicklistData: string): void {
        // console.log("id",SelectedPicklistData)
        if (SelectedPicklistData) {
            // console.log("id",SelectedPicklistData)

            this.gbps.dataService.getData(SelectedPicklistData).subscribe(SimpleItem => {
                // console.log("SimpleItem",SimpleItem)
                    this.form.patchValue(SimpleItem);                
               
            })
        }
    }

    public SimpleItemPicklistPatchValue(SelectedPicklistDatas: any): void {
        // console.log("id",SelectedPicklistDatas)
        console.log("SelectedPicklistDatasRetrival",SelectedPicklistDatas)
        this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
    }

    public ChangeTab(event, TabName:string) :void{
        var i:number, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
          tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("FormTabLinks");
        for (i = 0; i < tablinks.length; i++) {
          tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(TabName).style.display = "block";
        console.log("Event:",event)
        event.currentTarget.className += " active";
      }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<ISimpleItem> {
    const dbds: GBBaseDBDataService<ISimpleItem> = new GBBaseDBDataService<ISimpleItem>(http);
    dbds.endPoint = url;
    return dbds;
}


export function getThisPageService(store: Store, dataService: GBBaseDataService<ISimpleItem>, dbDataService: GBBaseDBDataService<ISimpleItem>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<ISimpleItem> {
    return new GBDataPageService<ISimpleItem>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
