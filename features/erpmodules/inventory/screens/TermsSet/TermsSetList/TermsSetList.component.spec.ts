import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TermssetlistComponent } from './TermsSetList.component';

describe('TermssetlistComponent', () => {
  let component: TermssetlistComponent;
  let fixture: ComponentFixture<TermssetlistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TermssetlistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TermssetlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
