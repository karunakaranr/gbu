import { Component, Inject, LOCALE_ID, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Select, Store } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { CommonModule } from '@angular/common';
import { SharedService } from 'features/commonreport/service/datapassing.service';

@Component({
  selector: 'app-storelist',
  templateUrl: './storelist.component.html',
  styleUrls: ['./storelist.component.scss'],
})
export class StorelistComponent implements OnInit {
  @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
  public rowData = [];
 
  constructor(public sharedService: SharedService,public store: Store, @Inject(LOCALE_ID) public locale: string) {}
 
  ngOnInit(): void {
    this.getstorelist();
  }

  public getstorelist() {
    this.sharedService.getRowInfo().subscribe((data) => {
      if (data.length > 0) {
      this.rowData = data; // Store the original data
      console.log("Store List:", this.rowData);

      // Create a new array to store the updated data
      const updatedRowData = [];

      for (let data of this.rowData) {
        let updatedData = Object.assign({}, data); // Create a copy of the original object

        if (data.StoreType === 0) {
          updatedData.StoreType = "OWN STOCK";
        } else if (data.StoreType === 1) {
          updatedData.StoreType = "CUSTOMER STOCK";
        }

        if (data.StoreIsPlanning === 0) {
          updatedData.StoreIsPlanning = "Yes";
        } else if (data.StoreIsPlanning === 1) {
          updatedData.StoreIsPlanning = "No";
        }

        if (data.StoreIsStockValue === 0) {
          updatedData.StoreIsStockValue = "Yes";
        } else if (data.StoreIsStockValue === 1) {
          updatedData.StoreIsStockValue = "No";
        }

        updatedRowData.push(updatedData);
      }

      // Replace the original rowData with the updated array
      this.rowData = updatedRowData;
      console.log("Store List:", this.rowData);

      let json = this.rowData;
      var finalizedArray = [];
      json.map((row) => {
        finalizedArray.push({
          oucode: row['OrganizationUnitCode'],
          ouname: row['OrganizationUnitName'],
          strcode: row['StoreCode'],
          strname: row['StoreName'],
          strtype: row['StoreType'],
          strplan: row['StoreIsPlanning'],
          strstockval: row['StoreIsStockValue'],
        });
      });

      const final = {};
      finalizedArray.forEach((detail) => {
        final[detail.oucode] = {
          accountGroups: {},
          strcode: detail.oucode,
          strname: "",
          strtype: "",
          strplan: "",
          strstockval: "",
          ...final[detail.oucode],
        };

        final[detail.oucode].accountGroups[detail.ouname] = {
          accounts: {},
          strcode: detail.oucode,
          strname:  detail.ouname,
          strtype: "",
          strplan: "",
          strstockval: "",
          ...final[detail.oucode].accountGroups[detail.ouname],
        };
        final[detail.oucode].accountGroups[detail.ouname].accounts[detail.strtype] = {
          strcode: detail.strcode,
          strname: detail.strname,
          strtype: detail.strtype,
          strplan: detail.strplan,
          strstockval: detail.strstockval,
        };
      });

      const grpcodes = Object.keys(final);

      const tableData = [];
      grpcodes.forEach((strcode) => {
        const accountGroups = Object.keys(final[strcode].accountGroups);

        // tableData.push({
        //   strcode: final[strcode].strcode,
        //   strname: "",
        //   bold: true,
        // });

        accountGroups.forEach((ag) => {
          tableData.push({
            strcode: final[strcode].accountGroups[ag].strcode,
            strname: final[strcode].accountGroups[ag].strname,
            bold: true,
          });

          const accounts = Object.keys(final[strcode].accountGroups[ag].accounts);
          accounts.forEach((account) => {
            tableData.push({
              strcode: final[strcode].accountGroups[ag].accounts[account].strcode,
              strname: final[strcode].accountGroups[ag].accounts[account].strname,
              strtype: final[strcode].accountGroups[ag].accounts[account].strtype,
              strplan: final[strcode].accountGroups[ag].accounts[account].strplan,
              strstockval: final[strcode].accountGroups[ag].accounts[account].strstockval,
            });
          });
        });
      });
      this.rowData = tableData;
      console.log("Table Data:",tableData)
    }
    });
  }
}
