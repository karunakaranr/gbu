import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Select, Store } from '@ngxs/store';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { InventoryURLS } from '../../URLS/urls';
import { MRPPartyTypeService } from '../../services/MRPPartyType/MRPPartyType.service';
import { IMRPPartyType } from '../../models/IMRPPartyType';
import * as MRPPartyTypeJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/MRPPartyType.json'
import { LayoutState } from 'features/layout/store/layout.state';
import { Observable } from 'rxjs';
@Component({
    selector: 'app-MRPPartyType',
    templateUrl:'./mrppartytype.component.html',
    styleUrls: ['./mrppartytype.component.scss'],
    providers: [
        { provide: 'IdField', useValue: 'MRPPartyTypeId' },
        { provide: 'url', useValue: InventoryURLS.MRPPartyType },
        { provide: 'DataService', useClass: MRPPartyTypeService },
        { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
        { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
    ]
})
export class MRPPartyTypeComponent extends GBBaseDataPageComponentWN<IMRPPartyType> {
    title: string = "MRPPartyType"
    MRPPartyTypeJSON = MRPPartyTypeJSON;
	@Select(LayoutState.Resetformvalue) resetform$: Observable<any>;
    MRPPartyTypeDetailArray =[
		{
			"MRPPartyTypeDetailId": 0,
			"MRPPartyTypeDetailSlNo": 1,
			"MRPItemTypeId": -1,
			"MRPItemTypeCode": "",
			"MRPItemTypeName": "",
			"MRPPartyTypeDetailNature": 1,
			"BIZTransactionId": -1,
			"BIZTransactionCode": "",
			"BIZTransactionName": "",
			"MRPPartyTypeId": -1500000000,
			"MRPPartyTypeCode": "",
			"MRPPartyTypeName": ""
		},
        {
			"MRPPartyTypeDetailId": 0,
			"MRPPartyTypeDetailSlNo": 2,
			"MRPItemTypeId": -1,
			"MRPItemTypeCode": "",
			"MRPItemTypeName": "",
			"MRPPartyTypeDetailNature": 2,
			"BIZTransactionId":  -1,
			"BIZTransactionCode": "",
			"BIZTransactionName": "",
			"MRPPartyTypeId": -1500000000,
			"MRPPartyTypeCode": "",
			"MRPPartyTypeName": ""
		},
        {
			"MRPPartyTypeDetailId": 0,
			"MRPPartyTypeDetailSlNo": 3,
			"MRPItemTypeId": -1,
			"MRPItemTypeCode": "",
			"MRPItemTypeName": "",
			"MRPPartyTypeDetailNature": 3,
			"BIZTransactionId": -1,
			"BIZTransactionCode": "",
			"BIZTransactionName": "",
			"MRPPartyTypeId": -1500000000,
			"MRPPartyTypeCode": "",
			"MRPPartyTypeName": ""
		},
        {
			"MRPPartyTypeDetailId": 0,
			"MRPPartyTypeDetailSlNo": 4,
			"MRPItemTypeId": -1,
			"MRPItemTypeCode": "",
			"MRPItemTypeName": "",
			"MRPPartyTypeDetailNature": 4,
			"BIZTransactionId": -1,
			"BIZTransactionCode": "",
			"BIZTransactionName": "",
			"MRPPartyTypeId": -1500000000,
			"MRPPartyTypeCode": "",
			"MRPPartyTypeName": ""
		},
        {
			"MRPPartyTypeDetailId": 0,
			"MRPPartyTypeDetailSlNo": 5,
			"MRPItemTypeId": -1,
			"MRPItemTypeCode": "",
			"MRPItemTypeName": "",
			"MRPPartyTypeDetailNature": 8,
			"BIZTransactionId": -1,
			"BIZTransactionCode": "",
			"BIZTransactionName": "",
			"MRPPartyTypeId": -1500000000,
			"MRPPartyTypeCode": "",
			"MRPPartyTypeName": ""
		},
        {
			"MRPPartyTypeDetailId": 0,
			"MRPPartyTypeDetailSlNo": 6,
			"MRPItemTypeId": -1,
			"MRPItemTypeCode": "",
			"MRPItemTypeName": "",
			"MRPPartyTypeDetailNature": 9,
			"BIZTransactionId": -1,
			"BIZTransactionCode": "",
			"BIZTransactionName": "",
			"MRPPartyTypeId": -1500000000,
			"MRPPartyTypeCode": "",
			"MRPPartyTypeName": ""
		},
        {
			"MRPPartyTypeDetailId": 0,
			"MRPPartyTypeDetailSlNo": 7,
			"MRPItemTypeId": -1,
			"MRPItemTypeCode": "",
			"MRPItemTypeName": "",
			"MRPPartyTypeDetailNature": 10,
			"BIZTransactionId": -1,
			"BIZTransactionCode": "",
			"BIZTransactionName": "",
			"MRPPartyTypeId": -1500000000,
			"MRPPartyTypeCode": "",
			"MRPPartyTypeName": ""
		},
        {
			"MRPPartyTypeDetailId": 0,
			"MRPPartyTypeDetailSlNo": 8,
			"MRPItemTypeId": -1,
			"MRPItemTypeCode": "",
			"MRPItemTypeName": "",
			"MRPPartyTypeDetailNature": 11,
			"BIZTransactionId": -1,
			"BIZTransactionCode": "",
			"BIZTransactionName": "",
			"MRPPartyTypeId": -1500000000,
			"MRPPartyTypeCode": "",
			"MRPPartyTypeName": ""
		}
	];

    form: GBDataFormGroupWN<IMRPPartyType> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "MRPPartyType", {}, this.gbps.dataService);
    thisConstructor() {
        let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
        if (arrayOfValues != null) {
            this.MRPPartyTypePicklistFillValue(arrayOfValues.MRPPartyTypeId)
            this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
            
        } else {
            this.store.dispatch(new ButtonVisibility("Forms"))
        }
		this.resetform$.subscribe((res:boolean)=>{
			this.MRPPartyTypePicklistFillValue("-1")
		})
    }
	
    public MRPPartyTypePicklistFillValue(SelectedPicklistData: string): void {
        // console.log("id",SelectedPicklistData)
        if (SelectedPicklistData) {
            console.log("id",SelectedPicklistData)

            this.gbps.dataService.getData(SelectedPicklistData).subscribe(MRPPartyType => {
                console.log("MRPPartyType",MRPPartyType)
                    
                    if(SelectedPicklistData == "-1"){
                        this.form.get('MRPPartyTypeDetailArray').patchValue(this.MRPPartyTypeDetailArray)
                    } else{
                        this.form.patchValue(MRPPartyType);  
                    }
   
               
            })
        }
    }

    public MRPPartyTypePicklistPatchValue(SelectedPicklistDatas: any): void {
        // console.log("id",SelectedPicklistDatas)
        console.log("SelectedPicklistDatasRetrival",SelectedPicklistDatas)
        this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
        this.form.get('MRPPartyTypeDetailArray').patchValue(this.MRPPartyTypeDetailArray)
    } 
}
export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IMRPPartyType> {
    const dbds: GBBaseDBDataService<IMRPPartyType> = new GBBaseDBDataService<IMRPPartyType>(http);
    dbds.endPoint = url;
    return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IMRPPartyType>, dbDataService: GBBaseDBDataService<IMRPPartyType>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IMRPPartyType> {
    return new GBDataPageService<IMRPPartyType>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
