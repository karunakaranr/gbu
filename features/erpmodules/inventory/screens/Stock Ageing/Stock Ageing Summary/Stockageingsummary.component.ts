import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Observable } from 'rxjs';
import { Select } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { SharedService } from 'features/commonreport/service/datapassing.service';

@Component({
    selector: 'app-Stockageingsummary',
    templateUrl: './Stockageingsummary.component.html',
    styleUrls: ['./Stockageingsummary.component.scss'],
})
export class StockageingsummaryComponent implements OnInit {
    @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
    public rowData: any;
    public selectedOption: string = 'Qty'; // Default to 'Qty'
    public totalage;
    public Cost;
    public Realisation;
    public Margin;
    public MarginCost;
    public MarginVal;
    public Marginnum;

    constructor(public sharedService: SharedService, private cdr: ChangeDetectorRef) { }

    ngOnInit(): void {
        this.getStockageingsummary();
    }

    public getStockageingsummary() {
        this.sharedService.getRowInfo().subscribe(data => {
            this.rowData = data;
            if (data.length > 0) {
            let total = 0;

            console.log("Stock Ageing Summary Details:", this.rowData);

            this.totalage = 0;
            this.Cost = 0;
            this.Realisation = 0;
            this.Margin=0;
            this.MarginCost=0;
            this.MarginVal=0;
            this.Marginnum=0;

            
            for (let data of this.rowData) {
                this.totalage = this.totalage + data.TotalAgeValue;
                this.Cost = this.Cost + data.Age1Value;
                this.Realisation = this.Realisation + data.Age2Value;
                this.Margin = this.Margin + data.Age3Value;
                this.MarginCost =this.MarginCost + data.Age4Value;
                this.MarginVal = this.MarginVal + data.Age5Value;
                this.Marginnum = this.Marginnum + data.Age6Value;

            }

            let groupingData = this.rowData;
            const selectedOption = this.selectedOption; // Store the selected option

            var tableData = [];

            groupingData.forEach((detailData) => {
                
                let total;
                let Quantity;
                total = detailData['Age6Value'] + detailData['Age2Value'] + detailData['Age3Value'] +
                        detailData['Age4Value'] + detailData['Age5Value'] + detailData['Age1Value'] +
                        detailData['Age7Value'];

                Quantity = detailData['Age1GoodQuantity'] + detailData['Age2GoodQuantity'] + detailData['Age3GoodQuantity'] +detailData['Age4GoodQuantity'] + detailData['Age5GoodQuantity'] + detailData['Age6GoodQuantity'] + detailData['Age7GoodQuantity']; 

                tableData.push({
                    Code: detailData['Code'],
                    Name: detailData['Name'],
                    total: total,
                    displayValue: selectedOption === 'Qty' ? detailData['Age1GoodQuantity'] : detailData['Age1Value'],
                    displayValue1: selectedOption === 'Qty' ? detailData['Age2GoodQuantity'] : detailData['Age2Value'],
                    displayValue2: selectedOption === 'Qty' ? detailData['Age3GoodQuantity'] : detailData['Age3Value'],
                    displayValue3: selectedOption === 'Qty' ? detailData['Age4GoodQuantity'] : detailData['Age4Value'],
                    displayValue4: selectedOption === 'Qty' ? detailData['Age5GoodQuantity'] : detailData['Age5Value'],
                    displayValue5: selectedOption === 'Qty' ? Quantity : total
                  
                });
            });

            this.rowData = tableData;

            // Manually trigger change detection
            this.cdr.detectChanges();
        }
        });
    }

    public toggleOption(option: string) {
        this.selectedOption = option;
        this.getStockageingsummary(); // Reload data when the option changes
    }
}
