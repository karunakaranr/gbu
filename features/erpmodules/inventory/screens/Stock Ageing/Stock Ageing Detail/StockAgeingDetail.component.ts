import { HttpClient } from '@angular/common/http';
import { Component, Inject, LOCALE_ID, OnInit } from '@angular/core';
// import { ItemcategoryService } from 'features/erpmodules/inventory/services/itemcategory/itemcategory.service';
import { Observable } from 'rxjs';
import { Select, Store } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';

// Import necessary modules and actions if not already imported
import { Drilldownsetting } from 'features/commonreport/datastore/commonreport.action';
import { GBHttpService } from 'libs/gbcommon/src/lib/services/HTTPService/GBHttp.service';
import { IStockAgeingDetail, IStockAgeingDetailTable } from 'features/erpmodules/inventory/models/IStockAgeingDetail';
import { SharedService } from 'features/commonreport/service/datapassing.service';

@Component({
  selector: 'app-StockAgeingDetail',
  templateUrl: './StockAgeingDetail.component.html',
  styleUrls: ['./StockAgeingDetail.component.scss'],
})
export class StockAgeingDetailComponent implements OnInit {
  @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
  @Select(ReportState.Sourcecriteriadrilldown) sourcerowcriteria$: Observable<any>;
  public rowData: IStockAgeingDetail;
  selectedrowdatavalue: any;
  tabledata: IStockAgeingDetailTable;
  updatedtabledata: any[];

  constructor(public sharedService: SharedService,public store: Store, @Inject(LOCALE_ID) public locale: string,public lib: GBHttpService) {}

  ngOnInit(): void {
    this.getStockAgeingDetail();
  }

  public getStockAgeingDetail() {
    this.sharedService.getRowInfo().subscribe(data => {
      if (data.length > 0) {
      this.rowData = data;
      console.log("Stock Ageing Detail Data:", this.rowData);

      let json = this.rowData;
      var finalizedArray = []
      json.map((row,index)=> {
        finalizedArray.push({
          sno: index,
          code: row['Code'],
          name: row['Name'],

          icode: row['ItemCode'],
          iname: row['ItemName'],
          agevalue: row['Age1Value']+row['Age2Value']+row['Age3Value']+row['Age4Value']+row['Age5Value']+row['Age6Value'],
          age1value: row['Age1Value'],
          age2value: row['Age2Value'],
          age3value: row['Age3Value'],
          age4value: row['Age4Value'],
          age5value: row['Age5Value'],
          age6value: row['Age6Value'],

        });
      });
      const final = {};
      finalizedArray.forEach(detail => {
        final[detail.code] = {
          accounts: {},
          icode: detail.code,
          iname: detail.name,
          agevalue: 0,
          age1value: 0,
          age2value: 0,
          age3value: 0,
          age4value: 0,
          age5value: 0,
          age6value: 0,

          ...final[detail.code],
        }
        final[detail.code].accounts[detail.sno] = {
          icode: detail.icode,
          iname: detail.iname,
          agevalue: detail.agevalue,
          age1value: detail.age1value,
          age2value: detail.age2value,
          age3value: detail.age3value,
          age4value: detail.age4value,
          age5value: detail.age5value,
          age6value: detail.age6value,
        }
      })
      const empcodes = Object.keys(final)
      const tableData = [];
      empcodes.forEach(code => {
        const accounts = Object.keys(final[code].accounts)

        let sumagevalue = 0;
        let sumage1value = 0;
        let sumage2value = 0;
        let sumage3value = 0;
        let sumage4value = 0;
        let sumage5value = 0;
        let sumage6value = 0;

        accounts.forEach((ag) => {
          sumagevalue = sumagevalue + parseFloat(final[code].accounts[ag].agevalue);
          sumage1value = sumage1value + parseFloat(final[code].accounts[ag].age1value);
          sumage2value = sumage2value + parseFloat(final[code].accounts[ag].age2value);
          sumage3value = sumage3value + parseFloat(final[code].accounts[ag].age3value);
          sumage4value = sumage4value + parseFloat(final[code].accounts[ag].age4value);
          sumage5value = sumage5value + parseFloat(final[code].accounts[ag].age5value);
          sumage6value = sumage6value + parseFloat(final[code].accounts[ag].age6value);
        })
        final[code].agevalue = sumagevalue;
        final[code].age1value = sumage1value;
        final[code].age2value = sumage2value;
        final[code].age3value = sumage3value;
        final[code].age4value = sumage4value;
        final[code].age5value = sumage5value;
        final[code].age6value = sumage6value;

        tableData.push({
          icode: final[code].icode,
          iname: final[code].iname,
          agevalue: final[code].agevalue,
          age1value: final[code].age1value,
          age2value: final[code].age2value,
          age3value: final[code].age3value,
          age4value: final[code].age4value,
          age5value: final[code].age5value,
          age6value: final[code].age6value,
          bold: true,
        })

        const accountGroups = Object.keys(final[code].accounts)
        accountGroups.forEach(account => {
          tableData.push({
            icode: final[code].accounts[account].icode,
            iname: final[code].accounts[account].iname,
            agevalue: final[code].accounts[account].agevalue,
            age1value: final[code].accounts[account].age1value,
            age2value: final[code].accounts[account].age2value,
            age3value: final[code].accounts[account].age3value,
            age4value: final[code].accounts[account].age4value,
            age5value: final[code].accounts[account].age5value,
            age6value: final[code].accounts[account].age6value,
          })
        })
      })
      this.updatedtabledata = tableData;
      console.log("tableData",tableData)
    }
    });
  }
}
