import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { GridProperties,GridSetting} from './../../../models/Ipackset';
import { PacksetService } from '../../../services/packset/packset.service';
import { Store } from '@ngxs/store';

@Component({
  selector: 'app-packsetlist',
  templateUrl: './packsetlist.component.html',
  styleUrls: ['./packsetlist.component.scss'],
})
export class PacksetlistComponent implements OnInit {
  title = 'PackSetList'
  public rowData = [];
  public columnData = [];
  public defaultColDef = [];

  constructor(public service: PacksetService, private http: HttpClient, public store: Store) { }

  ngOnInit(): void {
    this.Packlist();

  }

  public Packlist() {
    this.service.getAll().subscribe((res) => {
      this.rowData = res;
      this.getgridsetting();
    });
  }
  public getgridsetting() {
    this.gridSetting().subscribe((res) => {
      this.columnData = res;
      this.getgridproperties();
    });
  }
  public getgridproperties() {
    this.gridproperties().subscribe((res) => {
      this.defaultColDef = res;
    });
  }

  private grid_setting = "assets/data/packsetlistsetting.json";
  private grid_properties = "assets/data/grid-properties.json";
  gridSetting(): Observable<GridSetting[]> {
    return this.http.get<GridSetting[]>(this.grid_setting);
  }
  gridproperties(): Observable<GridProperties[]> {
    return this.http.get<GridProperties[]>(this.grid_properties);
  }

}
