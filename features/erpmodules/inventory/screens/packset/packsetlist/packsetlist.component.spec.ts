import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PacksetlistComponent } from './packsetlist.component';

describe('PacklistComponent', () => {
  let component: PacksetlistComponent;
  let fixture: ComponentFixture<PacksetlistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PacksetlistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PacksetlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
