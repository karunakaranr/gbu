import { Component, OnInit } from "@angular/core";
import { BankbranchService } from "features/erpmodules/inventory/services/bankbranch/bankbranch.service";

const col = require('./../../../../../../apps/Goodbooks/Goodbooks/src/assets/data/bankbranchlist.json')

@Component({
  selector: 'app-bankbranchlist',
  templateUrl: './bankbranchlist.component.html',
  styleUrls: ['./bankbranchlist.component.scss']
})
export class BankbranchlistComponent implements OnInit {
  public rowData = [];
  public columnData = [];

  constructor(private bankbranchService: BankbranchService){  }

  ngOnInit(): void {
    this.getBankbranchlist();
  }

  getBankbranchlist() {
    this.bankbranchService.getBankBranchList().subscribe(menuDetails => {
      const menuDet = menuDetails;
      this.bankbranchService.rowService(menuDet).subscribe((res: { ReportDetail: any[]; }) => {
        this.columnData = col;
        this.rowData = res.ReportDetail;
      });
    });
    




  }
  
  
}
