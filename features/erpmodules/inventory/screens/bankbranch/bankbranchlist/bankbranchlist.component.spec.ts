import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BankbranchlistComponent } from './bankbranchlist.component';

describe('BankbranchlistComponent', () => {
  let component: BankbranchlistComponent;
  let fixture: ComponentFixture<BankbranchlistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BankbranchlistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BankbranchlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
