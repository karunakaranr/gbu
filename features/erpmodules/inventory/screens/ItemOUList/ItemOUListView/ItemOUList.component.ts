import { Component, OnInit } from '@angular/core';
import { ItemOUListView } from '../../../services/ItemOUList/ItemOUListView.service';
const col = require('./../../../../../../apps/Goodbooks/Goodbooks/src/assets/data/ItemOUList.json')


@Component({
  selector: 'app-ItemOUList.component',
  templateUrl: './ItemOUListView.component.html',
  styleUrls: ['./ItemOUListView.component.scss']
})

export class ItemOUListViewComponent implements OnInit {
  title = 'ItemOU List'
  public rowData = []
  public columnData = []

  constructor(public serivce: ItemOUListView) { }
  ngOnInit(): void {
    this.itemoulistview();
  }
  public itemoulistview() {
    this.serivce.itemoulistview().subscribe(menudetails => {
      this.columnData = col;
      let servicejsondetail = menudetails
      this.serivce.Rowservice(servicejsondetail).subscribe(res => {
        this.rowData = res.ReportDetail;

        for (let data of this.rowData) {
          if (data) {
            console.log(data.IsMakeOrBuy);
        if(data.IsMakeOrBuy == 0)
            {
            data.IsMakeOrBuy = "Make";
            }
            else if(data.IsMakeOrBuy == 1)
            {
            data.IsMakeOrBuy = "Buy";
            }
            else if(data.IsMakeOrBuy == 2)
            {
            data.IsMakeOrBuy = "Buy-Specific DWG";
            }
       if(data.MRPPlanMethod == 0)
            {
            data.MRPPlanMethod = "MPS PLANNING";
            }
            else if(data.MRPPlanMethod == 1)
            {
            data.MRPPlanMethod = "MRP PLANNING";
            }
            else if(data.MRPPlanMethod == 2)
            {
            data.MRPPlanMethod = "DRP PLANNING";
            }
            else if(data.MRPPlanMethod == 3)
            {
            data.MRPPlanMethod = "MPS AND DRP";
            }
            else if(data.MRPPlanMethod == 4)
            {
            data.MRPPlanMethod = "MRP AND DRP";
            }
       if(data.MasterPlanMethod == 0)
            {
            data.MasterPlanMethod = "MIN/MAX";
            }
            else if(data.MasterPlanMethod == 1)
            {
            data.MasterPlanMethod = "ROL";
            }
        if(data.LotMethod == 0)
            {
            data.LotMethod = "LOT-FOR-LOT";
            }
            else if(data.LotMethod == 1)
            {
            data.LotMethod = "";
            }
        if(data.InventoryMethod == 0)
            {
            data.InventoryMethod = "Mean Absolute Deviation";
            }
            else if(data.InventoryMethod == 1)
            {
            data.InventoryMethod = "";
            }
        if(data.SafetyStockMethod == 0)
            {
            data.SafetyStockMethod = "Fixed Qty";
            }
            else if(data.SafetyStockMethod == 1)
            {
            data.SafetyStockMethod = "Fixed Days";
            }
        if(data.IsSaleableItem == 0)
            {
            data.IsSaleableItem = "Yes";
            }
            else if(data.IsSaleableItem == 1)
            {
            data.IsSaleableItem = "No";
            }
        
        if(data.BomType == 0)
            {
            data.BomType = "NOT APPLICABLE";
            }
            else if(data.BomType == 1)
            {
            data.BomType = "STANDARD";
            }
            else if(data.BomType == 2)
            {
            data.BomType = "NON STANDARD";
            }
            else if(data.BomType == 3)
            {
            data.BomType = "STANDARD PROCESSWISE";
            }
            else if(data.BomType == 4)
            {
            data.BomType = "RAW MATERIAL";
            }
            else if(data.BomType == 5)
            {
            data.BomType = "NESTING PLAN";
            }
        if(data.IsPurchaseable == 0)
            {
            data.IsPurchaseable = "Yes";
            }
            else if(data.IsPurchaseable == 1)
            {
            data.IsPurchaseable = "No";
            }
        if(data.ItemOrTemplate == 0)
            {
            data.ItemOrTemplate = "Yes";
            }
            else if(data.ItemOrTemplate == 1)
            {
            data.ItemOrTemplate = "No";
            }
        if(data.IsStockPosting == 0)
            {
            data.IsStockPosting = "Yes";
            }
            else if(data.IsStockPosting == 1)
            {
            data.IsStockPosting = "No";
            }
            console.log('Table data ', this.rowData);

      }
    }
  });
})

}

}