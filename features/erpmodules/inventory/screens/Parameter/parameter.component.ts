import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ParameterService } from '../../services/Parameter/parameter.service';
import { IParameter } from '../../models/IParameter';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { IBaseField, IPickListDetail } from 'libs/uicore/src/lib/interfaces/ibasefield';

import { InventoryURLS } from 'features/erpmodules/inventory/URLS/urls';

import * as ParameterJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/Parameter.json';



@Component({
  selector: 'app-parameter',
  templateUrl: './parameter.component.html',
  styleUrls: ['./parameter.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'ParameterId' },
    { provide: 'url', useValue: InventoryURLS.Parameter },
    { provide: 'DataService', useClass: ParameterService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class ParameterComponent extends GBBaseDataPageComponentWN<IParameter> {
  title: string = "Parameter"
  ParameterJSON = ParameterJSON

  form: GBDataFormGroupWN<IParameter> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "Parameter", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.ParameterRetrivalValue(arrayOfValues.ParameterId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {

      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }

  public LovTypeValues(): void {

    if (this.form.get('ParameterTypeOfInfo').value == "0") {
      this.form.get('LovTypeId').patchValue('-1');
      this.form.get('LovTypeName').patchValue('NONE');


    }
    if (this.form.get('ParameterTypeOfInfo').value == "1") {
      this.form.get('LovTypeId').patchValue('-1');
      this.form.get('LovTypeName').patchValue('NONE');


    }
    if (this.form.get('ParameterTypeOfInfo').value == "5") {
      this.form.get('LovTypeId').patchValue('-1');
      this.form.get('LovTypeName').patchValue('NONE');

    }
  }



  public ParameterRetrivalValue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(Parameter => {
        if (SelectedPicklistData != '0') {
          this.form.patchValue(Parameter);
        }
      })
    }
  }


  public ParameterPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }

  public addnew(templateRef) {
    let dialogRef = this.dialog.open(templateRef, {
      width: '600px'
    });
  }

  savedata

  public UOMRetrivalValue(formdata): void {
    if (formdata) {
      this.savedata = formdata
      console.log("PARAMTERUOM",this.savedata)
    }
  }

  public newsave(){
    console.log("SAVEEE",this.savedata)
    let title = 'Uom';
    let url = InventoryURLS.UOM
    this.formaction.savemodel(this.savedata,title,url)
    this.dialog.closeAll()
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IParameter> {
  const dbds: GBBaseDBDataService<IParameter> = new GBBaseDBDataService<IParameter>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IParameter>, dbDataService: GBBaseDBDataService<IParameter>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IParameter> {
  return new GBDataPageService<IParameter>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
