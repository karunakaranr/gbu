import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ModelService } from '../../services/model/model.service';
import { IModel } from '../../models/IModel';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
// import * as JsonName from ‘JsonPath’ './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/PackSet.json';
import * as ModelJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/Model.json'
import { InventoryURLS } from '../../URLS/urls';


@Component({
  selector: 'app-Model',
  templateUrl: './Model.component.html',
  styleUrls: ['./Model.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'ModelId'},
    { provide: 'url', useValue: InventoryURLS.MODEL },
    { provide: 'DataService', useClass: ModelService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class ModelComponent extends GBBaseDataPageComponentWN<IModel > {
  title: string = 'Model'
  ModelJSON = ModelJSON;


  form: GBDataFormGroupWN<IModel > = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'Model', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.ModelFillFunction(arrayOfValues.ModelId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }


  public ModelFillFunction(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(PackSet => {
        this.form.patchValue(PackSet);
      })
    }
  }


  public ModelPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IModel > {
  const dbds: GBBaseDBDataService<IModel > = new GBBaseDBDataService<IModel >(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IModel >, dbDataService: GBBaseDBDataService<IModel >, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IModel > {
  return new GBDataPageService<IModel >(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
