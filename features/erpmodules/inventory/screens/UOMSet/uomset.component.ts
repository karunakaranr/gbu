import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { UOMSetService } from '../../services/UOMSet/uomset.service';
import { IUOMSet } from '../../models/IUOMSet';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as UOMSetJson from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/UOMSet.json';
import { InventoryURLS } from '../../URLS/urls';

@Component({
  selector: 'app-uomset',
  templateUrl: './uomset.component.html',
  styleUrls: ['./uomset.component.scss'], 
  providers: [
    { provide: 'IdField', useValue: 'UOMSetId'},
    { provide: 'url', useValue: InventoryURLS.UOMSet},
    { provide: 'DataService', useClass: UOMSetService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class UOMSetComponent extends GBBaseDataPageComponentWN<IUOMSet> {
  title: string = 'UOMSet'
  UOMSetJson = UOMSetJson;

  form: GBDataFormGroupWN<IUOMSet> = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'UOMSet', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.UOMSetRetrivalValue(arrayOfValues.UOMSetId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }
  public UOMSetRetrivalValue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(UOMSet => {
        this.form.patchValue(UOMSet);
      })
    }
  }
  public UOMSetPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IUOMSet> {
  const dbds: GBBaseDBDataService<IUOMSet> = new GBBaseDBDataService<IUOMSet>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IUOMSet>, dbDataService: GBBaseDBDataService<IUOMSet>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IUOMSet> {
  return new GBDataPageService<IUOMSet>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
