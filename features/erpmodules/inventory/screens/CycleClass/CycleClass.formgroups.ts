import { CycleClass } from '../../models/ICycleClass';
import { GBDataFormGroupStoreWN, GBDataFormGroupWN } from './../../../../../libs/uicore/src/lib/classes/GBFormGroup';
import { HttpClient } from '@angular/common/http';
import { CycleClassService } from '../../services/CycleClass/CycleClass.service';
import { Select, Store } from '@ngxs/store';
import { CycleClassStateActionFactory } from '../../stores/CycleClass/CycleClass.actionfactory';

export class CycleClassFormgroup extends GBDataFormGroupWN<CycleClass> {
  constructor(http: HttpClient, dataService: CycleClassService) {
    super(http, 'SC0018', {}, dataService)
  }

  public clear() {
    super.clear();
  }

  public delete() {
    super.delete();
  }

  public save() {
    super.save();
  }

  public moveFirst() {
    super.moveFirst();
  }

  public movePrev() {
    super.movePrev();
  }

  public moveNext() {
    super.moveNext();
  }

  public moveLast() {
    super.moveLast();
  }
}
export class CycleClassFormgroupStore extends GBDataFormGroupStoreWN<CycleClass> {
  constructor(http: HttpClient, store: Store) {
    super(http, 'SC0018', {}, store, new CycleClassStateActionFactory())
  }

  public clear() {
    super.clear();
  }

  public delete() {
    super.delete();
  }

  public save() {
    super.save();
  }

  public moveFirst() {
    super.moveFirst();
  }

  public movePrev() {
    super.movePrev();
  }

  public moveNext() {
    super.moveNext();
  }

  public moveLast() {
    super.moveLast();
  }
}

