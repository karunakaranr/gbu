import { HttpClient } from '@angular/common/http';
import { GBBaseDataService, GBBaseDataServiceWN} from './../../../../../libs/gbdata/src/lib/services/gbbasedata.service';
import {GBBaseDBDataService} from './../../../../../libs/gbdata/src/lib/services/gbbasedbdata.service'
import {GBHttpService} from './../../../../../libs/gbcommon/src/lib/services/HTTPService/GBHttp.service';
import {GbToasterService} from './../../../../../libs/gbcommon/src/lib/services/toaster/gbtoaster.service';
import { CycleClass } from '../../models/ICycleClass';
import { CycleClassService } from '../../services/CycleClass/CycleClass.service';
import { CycleClassDBService } from '../../dbservices/CycleClass/CycleClassDB.service';
import { Store } from '@ngxs/store';
import { FormBuilder } from '@angular/forms';
import {GBDataPageService, GBDataPageServiceWN} from './../../../../../libs/uicore/src/lib/services/gbpage.service';
import { ActivatedRoute, Router } from '@angular/router';

export function getThisDBDataService(http: GBHttpService): GBBaseDBDataService<CycleClass> {
  return new CycleClassDBService(http);
}
export function getThisDataServiceWN(dbDataService: GBBaseDBDataService<CycleClass>): GBBaseDataServiceWN<CycleClass> {
  return new CycleClassService(dbDataService);
}
export function getThisDataService(dbDataService: GBBaseDBDataService<CycleClass>): GBBaseDataService<CycleClass> {
  return new CycleClassService(dbDataService);
}
export function getThisGBPageService(store: Store, dataService: GBBaseDataService<CycleClass>, dbDataService: GBBaseDBDataService<CycleClass>, fb: FormBuilder, gbhttp:GBHttpService, activeroute:ActivatedRoute, router:Router,toaster:GbToasterService): GBDataPageService<CycleClass> {
  return new GBDataPageService<CycleClass>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
export function getThisGBPageServiceWN(store: Store, dataService: GBBaseDataServiceWN<CycleClass>, dbDataService: GBBaseDBDataService<CycleClass>, fb: FormBuilder, gbhttp:GBHttpService, activeroute:ActivatedRoute, router:Router, toaster:GbToasterService): GBDataPageServiceWN<CycleClass> {
  return new GBDataPageServiceWN<CycleClass>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
