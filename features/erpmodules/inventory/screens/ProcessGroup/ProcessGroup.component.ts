import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ProcessGroupService } from '../../services/ProcessGroup/processgroup.service';
import { IProcessGroup } from '../../models/IProcessGroup';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { IBaseField, IPickListDetail } from 'libs/uicore/src/lib/interfaces/ibasefield';
import { InventoryURLS } from 'features/erpmodules/inventory/URLS/urls';
import * as ProcessGroupJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/ProcessGroup.json';


@Component({
  selector: 'app-processgroup',
  templateUrl: './ProcessGroup.component.html',
  styleUrls: ['./ProcessGroup.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'ProcessGroupId'},
    { provide: 'url', useValue: InventoryURLS.ProcessGroup},
    { provide: 'DataService', useClass: ProcessGroupService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class ProcessGroupComponent extends GBBaseDataPageComponentWN<IProcessGroup> {
  title: string = "ProcessGroup"
  ProcessGroupJSON = ProcessGroupJSON

  form: GBDataFormGroupWN<IProcessGroup> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "ProcessGroup", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.ProcessGroupRetrivalValue(arrayOfValues.ProcessGroupId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {

      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }





  public ProcessGroupRetrivalValue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(ProcessGroup => {
        if (SelectedPicklistData != '0') {
          this.form.patchValue(ProcessGroup);
        }
      })
    }
  }


  public ProcessGroupPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IProcessGroup> {
  const dbds: GBBaseDBDataService<IProcessGroup> = new GBBaseDBDataService<IProcessGroup>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IProcessGroup>, dbDataService: GBBaseDBDataService<IProcessGroup>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IProcessGroup> {
  return new GBDataPageService<IProcessGroup>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
