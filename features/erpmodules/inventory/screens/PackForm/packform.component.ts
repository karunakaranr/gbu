import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { InventoryURLS } from '../../URLS/urls';
import { PackFormService } from '../../services/PackForm/packform.service';
import { IPackForm } from '../../models/IPackForm';
import * as PackFormJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/PackForm.json'
@Component({
    selector: 'app-PackForm',
    templateUrl:'./packform.component.html',
    styleUrls: ['./packform.component.scss'],
    providers: [
        { provide: 'IdField', useValue: 'PackId' },
        { provide: 'url', useValue: InventoryURLS.Pack },
        { provide: 'DataService', useClass: PackFormService },
        { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
        { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
    ]
})
export class PackFormComponent extends GBBaseDataPageComponentWN<IPackForm> {
    title: string = "PackForm"
    PackFormJSON = PackFormJSON

    form: GBDataFormGroupWN<IPackForm> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "PackForm", {}, this.gbps.dataService);
    thisConstructor() {
        let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
        if (arrayOfValues != null) {
            this.PackFormPicklistFillValue(arrayOfValues.PackId)
            this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
        } else {
            this.store.dispatch(new ButtonVisibility("Forms"))
        }
    }
  
    public PackFormPicklistFillValue(SelectedPicklistData: string): void {
        // console.log("id",SelectedPicklistData)
        if (SelectedPicklistData) {
            // console.log("id",SelectedPicklistData)

            this.gbps.dataService.getData(SelectedPicklistData).subscribe(PackForm => {
                // console.log("PackForm",PackForm)
                    this.form.patchValue(PackForm);                
               
            })
        }
    }

    public PackFormPicklistPatchValue(SelectedPicklistDatas: any): void {
        // console.log("id",SelectedPicklistDatas)
        console.log("SelectedPicklistDatasRetrival",SelectedPicklistDatas)
        this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
    }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IPackForm> {
    const dbds: GBBaseDBDataService<IPackForm> = new GBBaseDBDataService<IPackForm>(http);
    dbds.endPoint = url;
    return dbds;
}


export function getThisPageService(store: Store, dataService: GBBaseDataService<IPackForm>, dbDataService: GBBaseDBDataService<IPackForm>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IPackForm> {
    return new GBDataPageService<IPackForm>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
