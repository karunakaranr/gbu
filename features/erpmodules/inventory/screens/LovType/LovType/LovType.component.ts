import { Component, EventEmitter, Output } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { LovTypeService } from 'features/erpmodules/inventory/services/LovType/LovType/LovType.service';
import { ILovType } from 'features/erpmodules/inventory/models/ILovType';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { IBaseField, IPickListDetail } from 'libs/uicore/src/lib/interfaces/ibasefield';

import { InventoryURLS } from 'features/erpmodules/inventory/URLS/urls';

import * as LovTypeJSON from './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/LovType.json'




@Component({
  selector: 'app-LovType',
  templateUrl: './LovType.component.html',
  styleUrls: ['./LovType.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'LovTypeId' },
    { provide: 'url', useValue: InventoryURLS.LovType },
    { provide: 'DataService', useClass: LovTypeService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class LovTypeComponent extends GBBaseDataPageComponentWN<ILovType> {
  title: string = "LovType"
  LovTypeJSON = LovTypeJSON
  @Output() Onselectdata: EventEmitter<any> = new EventEmitter<string>();


  form: GBDataFormGroupWN<ILovType> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "LovType", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.LovTypeRetrival(arrayOfValues.LovTypeId)
      // this.LovTypeRetrival(arrayOfValues.GcmId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.LovTypeRetrival('0')
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }




  public LovTypeRetrival(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(LovType => {
        if (SelectedPicklistData != '0') {
          this.form.patchValue(LovType);
        }
      })
    }
  }


  public LovTypePatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<ILovType> {
  const dbds: GBBaseDBDataService<ILovType> = new GBBaseDBDataService<ILovType>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<ILovType>, dbDataService: GBBaseDBDataService<ILovType>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<ILovType> {
  return new GBDataPageService<ILovType>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
