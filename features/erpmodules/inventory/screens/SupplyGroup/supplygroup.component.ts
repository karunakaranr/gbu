import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { SupplyGroupService } from '../../services/SupplyGroup/supplygroup.service';
import { ISupplyGroup } from '../../models/ISupplyGroup';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as SupplygroupJson from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/SupplyGroup.json';
import { InventoryURLS } from '../../URLS/urls';


@Component({
  selector: 'app-supplygroup',
  templateUrl: './supplygroup.component.html',
  styleUrls: ['./supplygroup.component.scss'], 
  providers: [
    { provide: 'IdField', useValue: 'SupplyGroupId'},
    { provide: 'url', useValue: InventoryURLS.SupplyGroup},
    { provide: 'DataService', useClass: SupplyGroupService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class SupplyGroupComponent extends GBBaseDataPageComponentWN<ISupplyGroup> {
  title: string = 'SupplyGroup'
  SupplygroupJson = SupplygroupJson;


  form: GBDataFormGroupWN<ISupplyGroup> = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'SupplyGroup', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.SupplygroupRetrivalValue(arrayOfValues.SupplyGroupId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }
  public SupplygroupRetrivalValue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(Supplygroup => {
        this.form.patchValue(Supplygroup);
      })
    }
  }
  public SupplygroupPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<ISupplyGroup> {
  const dbds: GBBaseDBDataService<ISupplyGroup> = new GBBaseDBDataService<ISupplyGroup>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<ISupplyGroup>, dbDataService: GBBaseDBDataService<ISupplyGroup>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<ISupplyGroup> {
  return new GBDataPageService<ISupplyGroup>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
