import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { PartyBranchService } from './../../../services/PartyBranch/PartyBranch.service'

const col = require('./../../../../../../apps/Goodbooks/Goodbooks/src/assets/data/partybranchlist.json')

@Component({
  selector: 'app-partybranchlist.component',
  templateUrl: './partybranchlist.component.html',
  styleUrls: ['./partybranchlist.component.scss']
})

export class PartyBranchListComponent implements OnInit {
  title = 'Party Branch'

  public rowData = []
  public columnData = []
  stringifiedData: any;
  stringifiedData1: any;
  stringifiedData2: any;

  constructor(public serivce: PartyBranchService) { }
  ngOnInit(): void {
    this.partybranch();
  }
  public partybranch() {
    this.serivce.Reportdetailservice().subscribe(menudetails => {
      this.columnData = col;
      let servicejsondetail = menudetails
      this.serivce.Rowservice(servicejsondetail).subscribe(res => {
        this.rowData = res.ReportDetail;

        for (let data of this.rowData) {
          if (data) {
            this.stringifiedData = JSON.stringify(data.NumberofBills);
            this.stringifiedData1 = JSON.stringify(data.CreditDays);
            this.stringifiedData2 = JSON.stringify(data.CreditLimit);
            if (data.Mobile == "0") {
              data.Mobile = "";
            }
            if (data.Phone == "0") {
              data.Phone = "";
            }
            if (this.stringifiedData == "0") {
              data.NumberofBills = "";
            }
            if (this.stringifiedData1 == "0") {
              data.CreditDays = "";
            }
            if (this.stringifiedData2 == "0") {
              data.CreditLimit = "";
            }
            data.PurchasePriceCategoryName = data.PurchasePriceCategoryName.replace("NONE", "");
            data.SalesPriceCategoryName = data.SalesPriceCategoryName.replace("NONE", "");
            data.PartyTaxTypeName = data.PartyTaxTypeName.replace("NONE", "");
          }
        }
      });
    })

  }

}
