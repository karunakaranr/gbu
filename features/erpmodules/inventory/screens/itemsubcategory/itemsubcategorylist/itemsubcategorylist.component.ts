import { HttpClient } from '@angular/common/http';
import { Component, Inject, LOCALE_ID, OnInit } from '@angular/core';
import { ItemsubcategoryService } from 'features/erpmodules/inventory/services/itemsubcategory/itemsubcategory.service';
import { Observable } from 'rxjs';
import { GridProperties, GridSetting } from '../../../models/Iitemcategory';
import { Select, Store } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
@Component({
  selector: 'app-itemsubcategorylist',
  templateUrl: './itemsubcategorylist.component.html',
  styleUrls: ['./itemsubcategorylist.component.scss'],
})
export class ItemSubCategorylistComponent implements OnInit {
  @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
  public rowData = [];
  constructor(public store: Store) { }

  ngOnInit(): void {
    this.itemsubcategorylist();
  }
  
  public itemsubcategorylist() {
    this.rowdatacommon$.subscribe(data => {
      this.rowData = data;
      console.log("Item Sub Category :", this.rowData)
    });
  }

}
