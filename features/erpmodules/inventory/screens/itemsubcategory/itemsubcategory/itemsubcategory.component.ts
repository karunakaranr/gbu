import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { IItemSubCategory } from 'features/erpmodules/inventory/models/Iitemsubcategory';
import { ItemsubcategoryService } from 'features/erpmodules/inventory/services/itemsubcategory/itemsubcategory.service';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { InventoryURLS } from './../../../URLS/urls';
import * as ItemSubCategoryJSON from './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/ItemSubCategory.json'
@Component({
  selector: 'app-ItemSubCategory',
  templateUrl:  'itemsubcategory.component.html',
  styleUrls:['itemsubcategory.component.scss'],
  providers: [
    { provide: 'IdField', useValue:'ItemSubCategoryId' },
    { provide: 'url', useValue: InventoryURLS.ItemSubCategory},
    { provide: 'DataService', useClass: ItemsubcategoryService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class ItemSubCategoryComponent extends GBBaseDataPageComponentWN<IItemSubCategory> {
  title: string = "ItemSubCategory"
  ItemSubCategoryJSON = ItemSubCategoryJSON;

  form: GBDataFormGroupWN<IItemSubCategory> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "ItemSubCategory", {}, this.gbps.dataService);
  thisConstructor() {
    console.log("URLS:",InventoryURLS)
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.ItemSubCategoryValue(arrayOfValues.ItemSubCategoryId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }
  public ItemSubCategoryValue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(ItemSubCategory => {
          this.form.patchValue(ItemSubCategory);
      })
    }
  }
  public ItemsubCategoryNew(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}
export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IItemSubCategory> {
  const dbds: GBBaseDBDataService<IItemSubCategory> = new GBBaseDBDataService<IItemSubCategory>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IItemSubCategory>, dbDataService: GBBaseDBDataService<IItemSubCategory>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IItemSubCategory> {
  return new GBDataPageService<IItemSubCategory>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
