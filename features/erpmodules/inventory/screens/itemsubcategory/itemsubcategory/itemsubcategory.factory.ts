
import { GBBaseDataService, GBBaseDataServiceWN} from './../../../../../../libs/gbdata/src/lib/services/gbbasedata.service';
import {GBBaseDBDataService} from './../../../../../../libs/gbdata/src/lib/services/gbbasedbdata.service'
import {GBHttpService} from './../../../../../../libs/gbcommon/src/lib/services/HTTPService/GBHttp.service';
import {GbToasterService} from './../../../../../../libs/gbcommon/src/lib/services/toaster/gbtoaster.service';
import { IItemSubCategory } from '../../../models/Iitemsubcategory';
import { ItemsubcategoryService } from '../../../services/itemsubcategory/itemsubcategory.service';
import { ItemsubcategoryDBService } from '../../../dbservices/itemsubcategory/itemsubcategorydb.service';
import { Store } from '@ngxs/store';
import { FormBuilder } from '@angular/forms';
import {GBDataPageService, GBDataPageServiceWN} from './../../../../../../libs/uicore/src/lib/services/gbpage.service';
import { ActivatedRoute, Router } from '@angular/router';

export function getThisDBDataService(http: GBHttpService): GBBaseDBDataService<IItemSubCategory> {
  return new ItemsubcategoryDBService(http);
}
export function getThisDataServiceWN(dbDataService: GBBaseDBDataService<IItemSubCategory>): GBBaseDataServiceWN<IItemSubCategory> {
  return new ItemsubcategoryService(dbDataService);
}
export function getThisDataService(dbDataService: GBBaseDBDataService<IItemSubCategory>): GBBaseDataService<IItemSubCategory> {
  return new ItemsubcategoryService(dbDataService);
}
export function getThisGBPageService(store: Store, dataService: GBBaseDataService<IItemSubCategory>, dbDataService: GBBaseDBDataService<IItemSubCategory>, fb: FormBuilder, gbhttp:GBHttpService, activeroute:ActivatedRoute, router:Router,toaster:GbToasterService): GBDataPageService<IItemSubCategory> {
  return new GBDataPageService<IItemSubCategory>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
export function getThisGBPageServiceWN(store: Store, dataService: GBBaseDataServiceWN<IItemSubCategory>, dbDataService: GBBaseDBDataService<IItemSubCategory>, fb: FormBuilder, gbhttp:GBHttpService, activeroute:ActivatedRoute, router:Router,toaster:GbToasterService): GBDataPageServiceWN<IItemSubCategory> {
  return new GBDataPageServiceWN<IItemSubCategory>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
