import { ItemsubcategoryComponent } from './itemsubcategory.component';
import { FormBuilder } from '@angular/forms';
import {GBDataPageServiceWN} from './../../../../../../libs/uicore/src/lib/services/gbpage.service';
import { Store } from '@ngxs/store';
import {GBBaseDataServiceWN} from './../../../../../../libs/gbdata/src/lib/services/gbbasedata.service';
import {GBHttpService} from './../../../../../../libs/gbcommon/src/lib/services/HTTPService/GBHttp.service';
import {GBBaseDBDataService} from './../../../../../../libs/gbdata/src/lib/services/gbbasedbdata.service'
import { IItemSubCategory } from '../../../models/Iitemsubcategory';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
const datajson = require('./../itemsubapiresponse.json'); // for Testing

describe('ItemsubcategoryComponent', () => {
  let fixture: ItemsubcategoryComponent;
  let formBuilderMock: GBDataPageServiceWN<IItemSubCategory>;
	let store:Store;
	let dataService:GBBaseDataServiceWN<IItemSubCategory>;
  let dbDataService:GBBaseDBDataService<IItemSubCategory>;
	let fb = new FormBuilder();
	let gbhttp:GBHttpService;
	let activeroute=new ActivatedRoute();
	let router: Router;
  beforeEach(() => {
    formBuilderMock = new GBDataPageServiceWN(store,dataService,dbDataService,fb,gbhttp,activeroute,router);
    fixture = new ItemsubcategoryComponent(
      formBuilderMock,
    );
    fixture.loaddata();
  });

  describe('Test: load', () => {
    it('check service reponse', () => {
      expect(fixture.loaddata()).toEqual(datajson);
    });
  });

  describe('Test: currentdata', () => {
    it('check service currentdata', () => {
      expect(fixture.currentdatatest).toEqual(datajson[0]);
    });
  });

  describe('Save: without mandatory id', () => {
    it('check  mandatory id', () => {
      expect(fixture.withoutid).toEqual(datajson[0]);
    });
  });

  describe('Save: without mandatory id & code', () => {
    it('check mandatory id & code', () => {
      expect(fixture.withoutidcode).toEqual(datajson[0]);
    });
  });

  describe('Save: with null data', () => {
    it('check null datas', () => {
      expect(fixture.nodata).toEqual(datajson[0]);
    });
  });

});
