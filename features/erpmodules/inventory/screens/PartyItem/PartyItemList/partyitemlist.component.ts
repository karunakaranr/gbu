import { Component, Inject, LOCALE_ID, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Select, Store } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { CommonModule, DatePipe } from '@angular/common';

@Component({
    selector: 'app-partyitemlist',
    templateUrl: './partyitemlist.component.html',
    styleUrls: ['./partyitemlist.component.scss'],
})
export class partyitemlistComponent implements OnInit {
    @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
    public rowData = [];
    public TotalAdd;
    public TotalLess;
    public TotalCost;


    constructor(public store: Store, @Inject(LOCALE_ID) public locale: string) { }

    ngOnInit(): void {
        this.getpartyitemlist();
    }

    public getpartyitemlist() {
        this.rowdatacommon$.subscribe((data) => {
            this.rowData = data; // Store the original data
            console.log("Party Item List:", this.rowData);


            // var datePipe = new DatePipe("en-US");
            // for (let data of this.rowData) {
            //     console.log("Before:",data.EffectiveFrom);
            //     data.EffectiveFrom = datePipe.transform(JSON.parse(data.EffectiveFrom.replace("/Date(", "").replace(")/", "")), 'dd/MMM/yyyy');
            //     console.log("After:",data.EffectiveFrom);
            // }


            this.TotalAdd = 0;
            this.TotalLess = 0;
            this.TotalCost = 0;
            let json = this.rowData;
            var finalizedArray = [];
            for (let data of this.rowData) {
                this.TotalAdd = this.TotalAdd + data.AmountPlus;
                this.TotalLess = this.TotalLess + data.AmountMinus;
                this.TotalCost = this.TotalCost + data.NetCost;
            }
            json.map((row) => {
                finalizedArray.push({
                    iid: row['ItemId'],
                    icode: row['ItemCode'],
                    iname: row['ItemName'],
                    //   strcode: row['StoreCode'],
                    //   strname: row['StoreName'],
                    //   strtype: row['StoreType'],


                    //   strplan: row['StoreIsPlanning'],
                    //   strstockval: row['StoreIsStockValue'],
                    ptycode: row['PartyCode'],
                    ptyname: row['PartyName'],
                    pcsname: row['ProcessName'],
                    rate: row['Rate'],
                    curcode: row['CurrencyCode'],

                    conrate: row['ConversionRate'],
                    plus: row['AmountPlus'],
                    minus: row['AmountMinus'],
                    cost: row['NetCost'],
                    days: row['LeadDays'],

                    parsts: row['PartyStatus'],
                    ival: row['InputValue'],
                    iparcode: row['ItemPartyCode'],
                    iparname: row['ItemPartyName'],
                    date: row['EffectiveFrom'],
                });
            });

            const final = {};
            finalizedArray.forEach((detail) => {
                final[detail.ptycode] = {
                    accountGroups: {},
                    ptycode: detail.ptycode,
                    ptyname: detail.ptyname,
                    pcsname: "",
                    rate: "",
                    curcode: "",

                    conrate: "",
                    plus: 0,
                    minus: 0,
                    cost: 0,
                    days: "",

                    parsts: "",
                    ival: "",
                    iparcode: "",
                    iparname: "",
                    date: "",
                    ...final[detail.ptycode],
                };

                final[detail.ptycode].accountGroups[detail.ptyname] = {
                    ptycode: detail.icode,
                    ptyname: detail.iname,
                    pcsname: detail.pcsname,
                    rate: detail.rate,
                    curcode: detail.curcode,

                    conrate: detail.conrate,
                    plus: detail.plus,
                    minus: detail.minus,
                    cost: detail.cost,
                    days: detail.days,

                    parsts: detail.parsts,
                    ival: detail.ival,
                    iparcode: detail.iparcode,
                    iparname: detail.iparname,
                    date: detail.date
                };
                // final[detail.icode].accountGroups[detail.iname].accounts[detail.ptycode] = {
                //     ptycode: detail.ptycode,
                //     ptyname: detail.ptyname,
                //     pcsname: detail.pcsname,
                //     rate: detail.rate,
                //     curcode: detail.curcode,

                //     conrate: detail.conrate,
                //     plus: detail.plus,
                //     minus: detail.minus,
                //     cost: detail.cost,
                //     days: detail.days,

                //     parsts: detail.parsts,
                //     ival: detail.ival,
                //     iparcode: detail.iparcode,
                //     iparname: detail.iparname,
                //     date: detail.date,
                // };
            });

            const grpcodes = Object.keys(final);

            const tableData = [];
            grpcodes.forEach((ptycode) => {
                const accountGroups = Object.keys(final[ptycode].accountGroups);
                let sumadd = 0;
                let sumless = 0;
                let sumcost = 0;
                // tableData.push({
                //   ptycode: final[pcsname].ptycode,
                //   ptyname: "",
                //   bold: true,
                // });

                accountGroups.forEach((ag) => {
                    sumadd = sumadd + parseFloat(final[ptycode].accountGroups[ag].plus);
                    sumless = sumless + parseFloat(final[ptycode].accountGroups[ag].minus);
                    sumcost = sumcost + parseFloat(final[ptycode].accountGroups[ag].cost);
                })
                final[ptycode].plus = sumadd;
                final[ptycode].minus = sumless;
                final[ptycode].cost = sumcost;

                tableData.push({
                    ptycode: final[ptycode].ptycode,
                    ptyname: final[ptycode].ptyname,
                    pcsname: "",
                    rate: "",
                    curcode: "",

                    conrate: "",
                    plus: final[ptycode].plus,
                    minus: final[ptycode].minus,
                    cost: final[ptycode].cost,
                    days: "",

                    parsts: "",
                    ival: "",
                    iparcode: "",
                    iparname: "",
                    date: "",
                    bold: true,
                });

                const accounts = Object.keys(final[ptycode].accountGroups);
                accounts.forEach((account) => {
                    tableData.push({
                        ptycode: final[ptycode].accountGroups[account].ptycode,
                        ptyname: final[ptycode].accountGroups[account].ptyname,
                        //   pcsname: final[pcsname].accounts[account].pcsname,
                        //   strplan: final[pcsname].accounts[account].strplan,
                        //   strstockval: final[pcsname].accounts[account].strstockval,
                        pcsname: final[ptycode].accountGroups[account].pcsname,
                        rate: final[ptycode].accountGroups[account].rate,
                        curcode: final[ptycode].accountGroups[account].curcode,

                        conrate: final[ptycode].accountGroups[account].conrate,
                        plus: final[ptycode].accountGroups[account].plus,
                        minus: final[ptycode].accountGroups[account].minus,
                        cost: final[ptycode].accountGroups[account].cost,
                        days: final[ptycode].accountGroups[account].days,

                        parsts: final[ptycode].accountGroups[account].parsts,
                        ival: final[ptycode].accountGroups[account].ival,
                        iparcode: final[ptycode].accountGroups[account].iparcode,
                        iparname: final[ptycode].accountGroups[account].iparname,
                        date: final[ptycode].accountGroups[account].date,
                    });
                });
            });
            this.rowData = tableData;
            console.log("Party Item Data:", this.rowData)
        });
    }
}
