import { HttpClient } from '@angular/common/http';
import { Component, Inject, LOCALE_ID, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Select, Store } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { SharedService } from 'features/commonreport/service/datapassing.service';
import { DateFormaterPipe } from 'libs/gbcommon/src/lib/Date-Formater.pipe';


@Component({
  selector: 'app-WOReport',
  templateUrl: './WOReport.component.html',
  styleUrls: ['./WOReport.component.scss'],
})
export class WOReportComponent implements OnInit {
  @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
  private dateFormatter: DateFormaterPipe = new DateFormaterPipe(this.store);

  public rowData = [];
  public TotalNetValue;
  public TotalNetValueFc;
  public TotalBasicValue;
  public TotalBasicValueFc;
  public TotalBillValue;
  constructor(public sharedService: SharedService,public store: Store) { }

  ngOnInit(): void {
    this.WOReport();
  }
  
  public WOReport() {
    this.sharedService.getRowInfo().subscribe(data => {
      if (data.length > 0) {
      this.rowData = data;
      console.log("Register W/O Item :", this.rowData)

      this.TotalNetValue = 0;
      this.TotalNetValueFc = 0;
      this.TotalBasicValue = 0;
      this.TotalBasicValueFc = 0;
      this.TotalBillValue = 0;
      let json = this.rowData;
      var finalizedArray = [];
      for (let data of this.rowData) {
          this.TotalNetValue = this.TotalNetValue + data.TotalItemBasicValue;
          this.TotalNetValueFc = this.TotalNetValueFc + data.TotalItemBasicValueFC;
          this.TotalBasicValue = this.TotalBasicValue + data.TotalNetValue;
          this.TotalBasicValueFc = this.TotalBasicValueFc + data.TotalNetValueFC;
          this.TotalBillValue = this.TotalBillValue + data.BillValue;
      }
      json.map((row,index) => {
          finalizedArray.push({
              sno:index,
              docdate: this.dateFormatter.transform(row['DocumentDate'], 'date'),
              biztypename: row['BIZTransactionType'],
              partyrefno: row['PartyReferenceNumber'],

              docno: row['DocumentNumber'],
              refno: row['ReferenceNumber'],
              refdate: row['ReferenceDate'],
              partyname: row['Party'],
              inchargename: row['Incharge'],
              basicvalue: row['TotalItemBasicValue'],
              basicvaluefc: row['TotalItemBasicValueFC'],
              netvalue: row['TotalNetValue'],
              netvaluefc: row['TotalNetValueFC'],
              billvalue: row['BillValue'],
              taxtypename: row['TaxTransactionType'],
          });
      });

      const final = {};
      finalizedArray.forEach((detail) => {
          final[detail.docdate] = {
              accountGroups: {},
              docno: detail.docdate,
              refno: detail.biztypename.Name,
              refdate: "",
              partyname: "",
              inchargename: detail.partyrefno,
              basicvalue: 0,
              basicvaluefc: 0,
              netvalue: 0,
              netvaluefc: 0,
              billvalue: 0,
              taxtypename: "",
              ...final[detail.docdate],
          };

          final[detail.docdate].accountGroups[detail.docno] = {
            accountGroups: {},
            docno: detail.docno,
            refno: detail.refno,
            refdate: detail.refdate,
            partyname: detail.partyname.Name,
            inchargename: detail.inchargename.Name,
            basicvalue: detail.basicvalue,
            basicvaluefc: detail.basicvaluefc,
            netvalue: detail.netvalue,
            netvaluefc: detail.netvaluefc,
            billvalue: detail.billvalue,
            taxtypename: detail.taxtypename.Name,
          };
          // final[detail.icode].accountGroups[detail.iname].accounts[detail.docdate] = {
          //     docdate: detail.docdate,
          //     docno: detail.docno,
          //     pcsname: detail.pcsname,
          //     rate: detail.rate,
          //     curcode: detail.curcode,

          //     conrate: detail.conrate,
          //     plus: detail.plus,
          //     minus: detail.minus,
          //     cost: detail.cost,
          //     days: detail.days,

          //     parsts: detail.parsts,
          //     ival: detail.ival,
          //     iparcode: detail.iparcode,
          //     iparname: detail.iparname,
          //     date: detail.date,
          // };
      });

      const grpcodes = Object.keys(final);

      const tableData = [];
      grpcodes.forEach((docdate) => {
          const accountGroups = Object.keys(final[docdate].accountGroups);
          let sumnetvalue = 0;
          let sumnetvaluefc = 0;
          let sumbasicvalue = 0;
          let sumbasicvaluefc = 0;
          let sumbillvalue = 0;

          accountGroups.forEach((ag) => {
              sumnetvalue = sumnetvalue + parseFloat(final[docdate].accountGroups[ag].basicvalue);
              sumnetvaluefc = sumnetvaluefc + parseFloat(final[docdate].accountGroups[ag].basicvaluefc);
              sumbasicvalue = sumbasicvalue + parseFloat(final[docdate].accountGroups[ag].netvalue);
              sumbasicvaluefc = sumbasicvaluefc + parseFloat(final[docdate].accountGroups[ag].netvaluefc);
              sumbillvalue = sumbillvalue + parseFloat(final[docdate].accountGroups[ag].billvalue);
          })
          final[docdate].basicvalue = sumnetvalue;
          final[docdate].basicvaluefc = sumnetvaluefc;
          final[docdate].netvalue = sumbasicvalue;
          final[docdate].netvaluefc = sumbasicvaluefc;
          final[docdate].billvalue = sumbillvalue;

          tableData.push({
              docno: final[docdate].docno,
              refno: final[docdate].refno,
              refdate: "",
              partyname: "",
              inchargename: final[docdate].inchargename,
              basicvalue: final[docdate].basicvalue,
              basicvaluefc: final[docdate].basicvaluefc,
              netvalue: final[docdate].netvalue,
              netvaluefc: final[docdate].netvaluefc,
              billvalue: final[docdate].billvalue,
              taxtypename: "",
              bold: true,
          });

          const accounts = Object.keys(final[docdate].accountGroups);
          accounts.forEach((account) => {
              tableData.push({

              docno: final[docdate].accountGroups[account].docno,
              refno: final[docdate].accountGroups[account].refno,
              refdate: final[docdate].accountGroups[account].refdate,
              partyname: final[docdate].accountGroups[account].partyname,
              inchargename: final[docdate].accountGroups[account].inchargename,
              basicvalue: final[docdate].accountGroups[account].basicvalue,
              basicvaluefc: final[docdate].accountGroups[account].basicvaluefc,
              netvalue: final[docdate].accountGroups[account].netvalue,
              netvaluefc: final[docdate].accountGroups[account].netvaluefc,
              billvalue: final[docdate].accountGroups[account].billvalue,
              taxtypename: final[docdate].accountGroups[account].taxtypename,
              });
          });
      });
      this.rowData = tableData;
      console.log("Register W/O Report Data:", this.rowData)
    }
    });
  }

}
