
import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs';
import { Select } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { SharedService } from 'features/commonreport/service/datapassing.service';


@Component({
    selector: 'app-Datewisemin',
    templateUrl: './Datewisemin.component.html',
    styleUrls: ['./Datewisemin.component.scss'],
})
export class DatewiseminComponent implements OnInit {
    @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
    public rowData: any;


    public ToatlVal; // genad total
    public ToatlCost;
    public ToatlRealisation;
    public Toatlpadding;
    public ToatlpaddingCost;
  

    constructor(public sharedService: SharedService) { }
    ngOnInit(): void {
        this.getDatewisemine();
    }

    public getDatewisemine() {
        this.sharedService.getRowInfo().subscribe(data => {
            if (data.length > 0) {
            this.rowData = data;
            console.log("Datewisemin Details :", this.rowData);

            
            this.ToatlVal=0; // genad total
            this.ToatlCost=0; 
            this.ToatlRealisation=0; 
            this.Toatlpadding=0; 
            this.ToatlpaddingCost=0; 
            

            for (let data of this.rowData) {
             this.ToatlVal = this.ToatlVal + data.ItemBasicValue;  
             this.ToatlCost = this.ToatlCost + data.ItemPlus; 
             this.ToatlRealisation = this.ToatlRealisation + data.ItemMinus; 
             this.Toatlpadding = this.Toatlpadding + data.BillValue; 
             this.ToatlpaddingCost = this.ToatlpaddingCost + data.BasicAfterDisc; 
            
         }

          
            let groupingData = this.rowData;
            var finalizedGroup = [];

            groupingData.map((inputData, index) => {

              
                var tax
                if (inputData["Itemplus"] !== 0.0) {
                    tax = inputData["Itemplus"];
                } else {
                    tax = "0.00";
                }
                finalizedGroup.push({
                    sno: index,
                    Type: inputData['ItemCode'],
                    Level: inputData['ItemName'],
                    Activity: inputData['TransactionQuantity'],
                    Parent: inputData['TransactionUOM'],
                    Group: inputData['Rate'],
                    Employee: inputData['ItemBasicValue'],
                    Role: tax,
                    Remarks: inputData['ItemMinus'],
                    billval:inputData['BillValue'],
                    netval:inputData['NetValue'],

                    tdscatname: inputData['DocumentDate'],
                   
                    branchprtyname: inputData['DocumentNumber'],
                    demonew: inputData['AccountName'],
                    CreatedByName:inputData['CreatedByName'],
                    ModifiedByName:inputData['ModifiedByName'],
                    ReferenceDate:inputData['ReferenceDate'],
                    CurrencyCode:inputData['CurrencyCode'],
                    PartyReferenceNumber:inputData['PartyReferenceNumber'],
                    AdvanceAmount:inputData['AdvanceAmount'],
                    BillValue:inputData['BillValue'],
                    TotalNetValueFC:inputData['TotalNetValueFC'],


                });

            });
        

            const final = {};
            finalizedGroup.forEach((detailData) => {
                final[detailData.tdscatname] = {
                    singlegrup: {},
                    // sno: detailData.tdscatname,
                    Type: detailData.tdscatname,
                    Level:  "",
                    Activity: "",
                    Parent: "",
                    Group: "",
                    Employee: "",
                    Role: "",
                    Remarks: "",
                    billval:"",
                    netval:"",

                  
                    ...final[detailData.tdscatname]

                };

                final[detailData.tdscatname].singlegrup[detailData.branchprtyname] = {
                    deductedmultigrup: {},
                    // sno: detailData.branchprtyname,
                    Type: detailData.branchprtyname,
                    Level: detailData.demonew,
                    Activity: detailData.CreatedByName,
                    Parent: detailData.ModifiedByName,
                    Group: detailData.ReferenceDate,
                    Employee:detailData. CurrencyCode,
                    Role: detailData.PartyReferenceNumber,
                    Remarks: detailData.AdvanceAmount,
                    billval:detailData.BillValue,
                    netval:detailData.TotalNetValueFC,
                   

                    ...final[detailData.tdscatname].singlegrup[detailData.branchprtyname],
                }

                final[detailData.tdscatname].singlegrup[detailData.branchprtyname].deductedmultigrup[detailData.sno] = {
                    // sno: detailData.sno,
                    Type: detailData.Type,
                    Level: detailData.Level,
                    Activity: detailData.Activity,
                    Parent: detailData.Parent,
                    Group: detailData.Group,
                    Employee: detailData.Employee,
                    Role: detailData.Role,
                    Remarks: detailData.Remarks,
                    billval:detailData.billval,
                    netval:detailData.netval,
                  

                }
            });

            const dedGroups = Object.keys(final);

            const tableData = [];

            dedGroups.forEach((Details) => {
                const finalsiglegrup = Object.keys(final[Details].singlegrup);
               

                tableData.push({
                    // sno: final[Details].sno,
                    Type: final[Details].Type,
                    Level:"",
                    Activity: "",
                    Parent: "",
                    Group:"",
                    Employee: "",
                    Role:"",
                    Remarks: 0,
                    billval:0,
                    netval:0,
                    bold: true,
                });



                finalsiglegrup.forEach(ag => {
                    const finalmultigrup = Object.keys(final[Details].singlegrup[ag].deductedmultigrup)
                    let actualquan = 0; 
                    let finaltotal = 0;
                    let finalnetval = 0;

                    finalmultigrup.forEach(account =>{
                        actualquan = actualquan + parseFloat(final[Details].singlegrup[ag].deductedmultigrup[account].Remarks);
                        finaltotal = finaltotal + parseFloat(final[Details].singlegrup[ag].deductedmultigrup[account].billval);
                        finalnetval = finalnetval + parseFloat(final[Details].singlegrup[ag].deductedmultigrup[account].netval);

                    })
                    final[Details].Remarks = actualquan;
                    final[Details].billval = finaltotal;
                    final[Details].netval = finalnetval;
                    tableData.push({
                        // sno: final[Details].deductedgroup[ag].sno,
                        Type: final[Details].singlegrup[ag].Type,
                        Level:final[Details].singlegrup[ag].Level,
                        Activity: final[Details].singlegrup[ag].Activity,
                        Parent: final[Details].singlegrup[ag].Parent,
                        Group: final[Details].singlegrup[ag].Group,
                        Employee:final[Details].singlegrup[ag].Employee,
                        Role: final[Details].singlegrup[ag].Role,
                        Remarks: "Adv:"+final[Details].Remarks,
                        billval:final[Details].billval,
                        netval:final[Details].netval,
                        bold: true,
                    })


                    finalmultigrup.forEach(account => {
                        tableData.push({
                            // sno: final[Details].deductedgroup[ag].deductedmultigrup[account].sno,
                            Type: final[Details].singlegrup[ag].deductedmultigrup[account].Type,
                            Level: final[Details].singlegrup[ag].deductedmultigrup[account].Level,
                            Activity: final[Details].singlegrup[ag].deductedmultigrup[account].Activity,
                            Parent: final[Details].singlegrup[ag].deductedmultigrup[account].Parent,
                            Group: final[Details].singlegrup[ag].deductedmultigrup[account].Group,
                            Employee: final[Details].singlegrup[ag].deductedmultigrup[account].Employee,
                            Role: final[Details].singlegrup[ag].deductedmultigrup[account].Role,
                            Remarks: final[Details].singlegrup[ag].deductedmultigrup[account].Remarks,
                            billval: final[Details].singlegrup[ag].deductedmultigrup[account].billval,
                            netval: final[Details].singlegrup[ag].deductedmultigrup[account].netval,
                           

                        })
                    })
                })

            });
            this.rowData = tableData;

            console.log("Final Data:", this.rowData[0])


        }
        });
    }
}
