import { Injectable } from '@angular/core';
import { ItemReportdbservice } from './../../dbservices/ItemReport/ItemReportdb.service';
@Injectable({
  providedIn: 'root'
})
export class ItemReportService {
  constructor(public dbservice: ItemReportdbservice) { }

  public GetitemreportList(){                 
    return this.dbservice.picklist();
  }

  public Rowservice(obj){                  
    return this.dbservice.reportData(obj);
  }
}