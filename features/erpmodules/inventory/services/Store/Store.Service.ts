import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';


import { IStore } from '../../models/IStore';


 
@Injectable({
  providedIn: 'root'
})
export class StoreService extends GBBaseDataServiceWN<IStore> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IStore>) {
    super(dbDataService, 'StoreId');
  }
}
