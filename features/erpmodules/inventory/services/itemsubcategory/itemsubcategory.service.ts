import { Inject, Injectable } from '@angular/core';

import { GBBaseDataServiceWN,GBBaseDBDataService,GBBaseDBService } from 'libs/gbdata/src';
import { IItemSubCategory } from '../../models/Iitemsubcategory';

// import { ModelName } from ‘ModelPath’;
// import { IItemSubCategory } from '../../models/Iitemsubcategory';
@Injectable({
  providedIn: 'root'
})
export class ItemsubcategoryService extends GBBaseDataServiceWN<IItemSubCategory> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IItemSubCategory>) {
    super(dbDataService, 'itemsubcategoryId');
  }
}
