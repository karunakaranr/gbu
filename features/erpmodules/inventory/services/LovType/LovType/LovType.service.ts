import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';


// import { ILovType } from 'features/erpmodules/inventory/models/ILovType';

import { ILovType } from 'features/erpmodules/inventory/models/ILovType';
 
@Injectable({
  providedIn: 'root'
})
export class LovTypeService extends GBBaseDataServiceWN<ILovType> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<ILovType>) {
    super(dbDataService, 'LovTypeId');
  }
}
