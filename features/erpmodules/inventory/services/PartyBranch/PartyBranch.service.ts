import { Injectable } from '@angular/core';
import { PartyBranchdbservice } from './../../dbservices/PartyBranch/PartyBranchdb.service';
@Injectable({
  providedIn: 'root'
})
export class PartyBranchService {
  constructor(public dbservice: PartyBranchdbservice) { }

  public Reportdetailservice(){                      
    return this.dbservice.picklist();
  }

  public Rowservice(obj){                    
    return this.dbservice.reportData(obj);
  }
}