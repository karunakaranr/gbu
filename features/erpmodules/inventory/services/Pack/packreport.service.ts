import { Injectable } from '@angular/core';
import { packReportDBService } from './../../dbservices/Pack/packreportdb.service';


@Injectable({
  providedIn: 'root'
})
export class packReportService {
  constructor(public dbservice: packReportDBService) { }
  public reportservice() {
    return this.dbservice.Reportservice();
  }

  public dataservice(cri){
  return this.dbservice.dataservice(cri);
  }
}
