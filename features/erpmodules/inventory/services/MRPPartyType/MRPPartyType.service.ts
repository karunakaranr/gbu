import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN,GBBaseDBDataService } from 'libs/gbdata/src';
import { IMRPPartyType } from '../../models/IMRPPartyType';




 
@Injectable({
  providedIn: 'root'
})
export class MRPPartyTypeService extends GBBaseDataServiceWN<IMRPPartyType> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IMRPPartyType>) {
    super(dbDataService, 'MRPPartyTypeId');
  }
}
