import { Inject, Injectable } from '@angular/core';
// import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { GBBaseDataServiceWN,GBBaseDBDataService } from 'libs/gbdata/src';

// import { LotType } from ‘ModelPath’;
// import { LovType } from '../../models/ILovType';
import { ILotType } from '../../models/ILotType';



 
@Injectable({
  providedIn: 'root'
})
export class LotTypeService extends GBBaseDataServiceWN<ILotType> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<ILotType>) {
    super(dbDataService, 'LotTypeId');
  }
}
