import { Inject, Injectable } from '@angular/core';
// import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { GBBaseDataServiceWN ,GBBaseDBDataService} from 'libs/gbdata/src'; 

import { IProcessGroup } from "../../models/IProcessGroup";


 
@Injectable({
  providedIn: 'root'
})
export class ProcessGroupService extends GBBaseDataServiceWN<IProcessGroup> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IProcessGroup>) {
    super(dbDataService, 'ProcessGroupId');
  }
}



