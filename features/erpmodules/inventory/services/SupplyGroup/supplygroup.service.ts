import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';


import { ISupplyGroup } from '../../models/ISupplyGroup';


 
@Injectable({
  providedIn: 'root'
})
export class SupplyGroupService extends GBBaseDataServiceWN<ISupplyGroup> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<ISupplyGroup>) {
    super(dbDataService, 'SupplyGroupId');
  }
}
