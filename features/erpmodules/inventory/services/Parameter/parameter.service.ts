import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';


import { IParameter } from '../../models/IParameter'; 


 
@Injectable({
  providedIn: 'root'
})
export class ParameterService extends GBBaseDataServiceWN<IParameter> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IParameter>) {
    super(dbDataService, 'ParameterId');
  }
}
