import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { IPackForm } from '../../models/IPackForm';





 
@Injectable({
  providedIn: 'root'
})
export class PackFormService extends GBBaseDataServiceWN<IPackForm> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IPackForm>) {
    super(dbDataService, 'PackId');
  }
}
