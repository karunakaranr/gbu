import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';


import { IItemOU } from '../../models/IItemOU';


 
@Injectable({
  providedIn: 'root'
})
export class ItemOuService extends GBBaseDataServiceWN<IItemOU> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IItemOU>) {
    super(dbDataService, 'ItemOUId',"ItemOU");
  }
}
