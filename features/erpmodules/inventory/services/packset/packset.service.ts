import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN} from './../../../../../libs/gbdata/src/lib/services/gbbasedata.service';
import { Ipackset } from './../../models/Ipackset';
import { GBBaseDBDataService } from 'libs/gbdata/src';

@Injectable({
  providedIn: 'root'
})
export class PacksetService extends GBBaseDataServiceWN<Ipackset> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<Ipackset>) {
    super(dbDataService, 'PackSetId');
  }
}


