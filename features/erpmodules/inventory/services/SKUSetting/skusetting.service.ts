import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';


import { ISKUSetting } from '../../models/ISKUSetting';


 
@Injectable({
  providedIn: 'root'
})
export class SKUSettingService extends GBBaseDataServiceWN<ISKUSetting> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<ISKUSetting>) {
    super(dbDataService, 'SKUSettingId');
  }
}
