import { Injectable } from "@angular/core";
import { JobdefinedbService } from "../../dbservices/jobdefine/jobdefinedb.service";

@Injectable({
    providedIn: 'root'
})
export class JobdefineService {

    constructor(
        private jobdefinedbService: JobdefinedbService
    ){ }

    public getJobDefineList() {                      
        return this.jobdefinedbService.picklist();
    }

    public rowService(obj){                    
        return this.jobdefinedbService.reportData(obj);
    }
}