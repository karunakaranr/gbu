import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';


// import { ModelName } from ‘ModelPath’;
import { IItemCategory } from '../../models/Iitemcategory';


 
@Injectable({
  providedIn: 'root'
})
export class ItemCategoryService extends GBBaseDataServiceWN<IItemCategory> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IItemCategory>) {
    super(dbDataService, 'ItemCategoryId');
  }
}
