import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { ITerm } from '../../models/ITerm';




 
@Injectable({
  providedIn: 'root'
})
export class TermService extends GBBaseDataServiceWN<ITerm> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<ITerm>) {
    super(dbDataService, 'TermsId');
  }
}
