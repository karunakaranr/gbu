import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';


import { IUOMSet } from '../../models/IUOMSet';


 
@Injectable({
  providedIn: 'root'
})
export class UOMSetService extends GBBaseDataServiceWN<IUOMSet> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IUOMSet>) {
    super(dbDataService, 'UOMSetId');
  }
}
