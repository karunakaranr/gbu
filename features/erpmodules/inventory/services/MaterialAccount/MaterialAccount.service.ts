import { Injectable } from "@angular/core";
import { MaterialAccountDbService } from "../../dbservices/MaterialAccount/MaterialAccountDb.service";

@Injectable({
    providedIn: 'root'
})
export class MaterialAccount {

    constructor(
        private accountScheduledbService: MaterialAccountDbService
    ){ }

    public materialaccountview() {                      
        return this.accountScheduledbService.picklist();
    }

    public Rowservice(obj){                    
        return this.accountScheduledbService.reportData(obj);
    }
}
