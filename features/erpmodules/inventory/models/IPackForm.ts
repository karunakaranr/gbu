export interface IPackForm {
    PackId: number
    PackCode: string
    PackName: string
    PackConversionType: number
    PackConversionFactor: number
    PackCreatedById: number
    PackCreatedOn: string
    PackCreatedByName: string
    PackModifiedById: number
    PackModifiedOn: string
    PackModifiedByName: string
    PackSortOrder: number
    PackStatus: number
    PackVersion: number
  }
  