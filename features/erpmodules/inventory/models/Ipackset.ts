export interface Igridlist {
    PackSetDetailId: number;
    PackSetId: number;
    PackSetDetailSlNo: number;
    PackId: number;
    PackSetDetailConversionType: number;
    PackSetDetailConversionFactor: number;
    PackSetCode: string;
    PackSetName: string;
    PackCode: string;
    PackName: string;
}

export class Ipackset {
    PackSetId: number;
    PackSetCode: string;
    PackSetName: string;
    PackSetSortOrder: number;
    PackSetStatus: number;
    PackSetCreatedById: number;
    PackSetCreatedOn: Date;
    PackSetModifiedById: number;
    PackSetModifiedOn: Date;
    PackSetSourceType: number;
    BaseTenantId: number;
    PackSetDetailArray: Igridlist[];
    PackSetCreatedByName?: any;
    PackSetModifiedByName?: any;
    PackSetVersion: number;
}
export interface GridSetting{
    headerName : string;
    field : string;
    sortable : boolean;
    filter : boolean;
    checkboxSelection : boolean;
    resizable : boolean;
    editable : boolean;
    pinned : string;
    cellEditor : string;
    cellEditorParams : string;
  }
  export interface GridProperties{
    paginationAutoPageSize: boolean;
    paginationPageSize : number;
    pagination : boolean;
    undoRedoCellEditing : boolean;
    undoRedoCellEditingLimit : number;
    enableCellChangeFlash : boolean;
    enterMovesDown : boolean;
    enterMovesDownAfterEdit : boolean;
    rowSelection : string;
    enableRangeSelection : boolean;
    paginateChildRows : boolean;
  }
  


