export interface IProcessOperation {
    ProcessOperationId: number
    ProcessOperationCode: string
    ProcessOperationName: string
    ProcessId: number
    ProcessCode: string
    ProcessName: string
    ProcessOperationCreatedById: number
    ProcessOperationCreatedOn: string
    ProcessOperationModifiedById: number
    ProcessOperationModifiedOn: string
    ProcessOperationCreatedByName: string
    ProcessOperationModifiedByName: string
    ProcessOperationSortOrder: number
    ProcessOperationStatus: number
    ProcessOperationVersion: number
}