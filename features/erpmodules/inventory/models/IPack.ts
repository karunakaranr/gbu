export interface IPack {
  PackId: number;
  PackCode: string;
  PackName: string;
  PackConversionType: string;
  PackConversionFactor: number;

  PackStatus: number;
  PackVersion: number;
  sortorder: number;
  sourceType: string;

  EntityId: number;
  
}

export interface GridSetting{
  headerName : string;
  field : string;
  sortable : boolean;
  filter : boolean;
  checkboxSelection : boolean;
  resizable : boolean;
  editable : boolean;
  pinned : string;
  cellEditor : string;
  cellEditorParams : string;
}
export interface GridProperties{
  paginationAutoPageSize: boolean;
  paginationPageSize : number;
  pagination : boolean;
  undoRedoCellEditing : boolean;
  undoRedoCellEditingLimit : number;
  enableCellChangeFlash : boolean;
  enterMovesDown : boolean;
  enterMovesDownAfterEdit : boolean;
  rowSelection : string;
  enableRangeSelection : boolean;
  paginateChildRows : boolean;
}
