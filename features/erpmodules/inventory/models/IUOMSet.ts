export interface IUOMSet {
    UOMSetId: number
    UOMSetCode: string
    UOMSetName: string
    BaseUOMId: number
    BaseUOMCode: string
    BaseUOMName: string
    UOMSetDetailArray: UomsetDetailArray[]
    UOMSetCreatedById: number
    UOMSetCreatedOn: string
    UOMSetCreatedByName: string
    UOMSetModifiedById: number
    UOMSetModifiedOn: string
    UOMSetModifiedByName: string
    UOMSetStatus: number
    UOMSetVersion: number
    UOMSetSortOrder: number
  }
  
  export interface UomsetDetailArray {
    UOMSetDetailId: number
    UOMSetId: number
    UOMSetCode: string
    UOMSetName: string
    UOMSetDetailSlNo: number
    FromUOMId: number
    FromUOMCode: string
    FromUOMName: string
    ToUOMId: number
    ToUOMCode: string
    ToUOMName: string
    UOMSetDetailConversionType: number
    UOMSetDetailConversionValue: number
    ConversionFormulaId: number
    ConversionFormulaCode: string
    ConversionFormulaName: string
  }
  