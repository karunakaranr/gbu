export interface IItemTemplatePart {
    ItemTemplatePartId: number
    ItemTemplatePartCode: string
    ItemTemplatePartName: string
    ParentId: string
    ParentName: string
    ParameterSetId: string
    ParameterSetName: string
    ItemTemplatePartPartDescription: string
    UOMId: string
    UOMName: string
    ItemTemplatePartItemLinkType: string
    ItemTemplatePartItemLinkTypeName: string
    ItemTemplatePartLinkItemId: string
    ItemTemplatePartCalculationMethod: string
    ItemTemplatePartSortOrder: string
    ItemTemplatePartCreatedOn: string
    ItemTemplatePartModifiedOn: string
    ItemTemplatePartModifiedByName: string
    ItemTemplatePartCreatedByName: string
    ItemTemplatePartVersion: number
    ItemTemplatePartStatus: number
    ItemTemplatePartVsParameterArray: ItemTemplatePartVsParameterArray[]
  }
  
  export interface ItemTemplatePartVsParameterArray {
    ItemTemplatePartVsParameterSlNo: string
    undefined: string
    ParameterId: string
    ParameterName: string
    ItemTemplatePartVsParameterParameterType: number
    ItemTemplatePartVsParameterParameterInputType: number
    ItemTemplatePartVsParameterParameterValue: string
    ItemTemplatePartVsParameterSpreadSheetName: string
    ItemTemplatePartVsParameterCellNumber: number
    ItemTemplatePartVsParameterParameterTextValue: string
    ItemTemplatePartSlNo: number
  }
  