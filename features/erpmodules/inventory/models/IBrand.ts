export class IBrand {
  ItemBrandId: number
  ItemBrandCode: string
  ItemBrandName: string
  ItemBrandIsSelected: number
  ItemBrandRemarks: string
  ItemBrandIsAsset: number
  ItemBrandIsItem: number
  ItemBrandCreatedById: number
  ItemBrandCreatedOn: string
  ItemBrandCreatedByName: string
  ItemBrandModifiedById: number
  ItemBrandModifiedOn: string
  ItemBrandModifiedByName: string
  ItemBrandSortOrder: number
  ItemBrandStatus: number
  ItemBrandVersion: number
  ItemBrandScreenOperationMode: number
}

export interface GridSetting {
  headerName: string;
  field: string;
  sortable: boolean;
  filter: boolean;
  checkboxSelection: boolean;
  resizable: boolean;
  editable: boolean;
  pinned: string;
  cellEditor: string;
  cellEditorParams: string;
}
export interface GridProperties {
  paginationAutoPageSize: boolean;
  paginationPageSize: number;
  pagination: boolean;
  undoRedoCellEditing: boolean;
  undoRedoCellEditingLimit: number;
  enableCellChangeFlash: boolean;
  enterMovesDown: boolean;
  enterMovesDownAfterEdit: boolean;
  rowSelection: string;
  enableRangeSelection: boolean;
  paginateChildRows: boolean;
}
