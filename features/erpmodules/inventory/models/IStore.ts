export interface IStore {
    StoreId: number
    StoreCode: string
    StoreName: string
    OrganizationUnitId: number
    OrganizationUnitName: string
    PartyBranchId: number
    PartyBranchName: string
    StoreType: string
    StoreSortOrder: string
    StoreIsReservation: number
    StoreIsPlanning: number
    StoreIsStockValue: number
    StoreStatus: number
    StoreVersion: number
    StoreCreatedOn: string
    StoreModifiedOn: string
    StoreModifiedByName: string
    StoreCreatedByName: string
    StoreSourceType: number
  }
  