export interface ITerm {
    TermsId: number
    TermsSection: string
    TermsDescription: string
    TermsType: number
    TermsTypeName: string
    TermsSortOrder: number
    TermsStatus: number
    TermsVersion: number
    TermsCreatedById: number
    TermsCreatedOn: string
    TermsCreatedByName: string
    TermsModifiedById: number
    TermsModifiedOn: string
    TermsModifiedByName: string
  }
  