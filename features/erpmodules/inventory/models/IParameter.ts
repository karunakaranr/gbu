export interface IParameter {
    ParameterId: number;
    ParameterCode: string;
    ParameterName: string;
    ParameterDataType: number;
    ParameterDataSize: number;
    ParameterUOMId: number;
    ParameterUOMCode: string;
    ParameterUOMName: string;
    ParameterMinValue: number;
    ParameterMaxValue: number;
    ParameterTypeOfInfo: number;
    LovTypeId: number;
    LovTypeName: string;
    ParameterIsFormulaField: number;
    ParameterRemarks: string;
    ParameterFieldMin: number;
    ParameterFieldMax: number;
    ParameterIsCritical: number;
    ParameterConsolidationType: number;
    ParameterCreatedById: number;
    ParameterCreatedOn: string;
    ParameterCreatedByName: string;
    ParameterModifiedById: number;
    ParameterModifiedOn: string;
    ParameterModifiedByName: string;
    ParameterSortOrder: number;
    ParameterStatus: number;
    ParameterVersion: number;
    ParameterSourceType: number;
    ParameterScreenOperationMode: number;
  }
  