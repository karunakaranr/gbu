export interface ILov
 {
    LovId: string;
    LovCode: string;
    LovName: string;
    LovScore: number;
    LovTypeId: string;
    LovTypeName: string;
    ParentId: string;
    LovStatus: string;
    LovTypeVersion: number;
    LovTypeStatus: number;

}