export interface ILotType {
    LotTypeId: number
    LotTypeName: string
    OUId: number
    LotTypeType: number
    LotTypeLotNumberingType: number
    ProcessId: string
    ItemId: string
    ItemName: string
    SKUId: string
    SKUName: string
    LotTypeLotNumberSize: number
    LotTypePrefix: string
    LotTypeSuffix: string
    LotTypeFromNumber: string
    LotTypeCreatedOn: string
    LotTypeModifiedOn: string
    LotTypeModifiedByName: string
    LotTypeCreatedByName: string
    LotTypeRemarks: string
    LotTypeStatus: number
    LotTypeVersion: number
}


