export interface IBomTree {
  StandardStageId: number;
  StageName: string;
  StageCount: number;
  StageValue: number;
  NumberOfRecords: number;
  Label: string;
}
