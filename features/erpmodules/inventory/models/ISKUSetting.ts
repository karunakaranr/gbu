export interface ISKUSetting {
    SKUSettingId: number;
    SKUSettingCode: string;
    SKUSettingName: string;
    SKUSettingSortOrder: number;
    SKUSettingStatus: number;
    SKUSettingVersion: number;
    SKUSettingSourceType: number;
    SKUSettingCreatedById: number;
    SKUSettingCreatedOn: string;
    SKUSettingModifiedById: number;
    SKUSettingModifiedOn: string;
    SKUSettingDetailArray: SkusettingDetailArray[]
  }
  
  export interface SkusettingDetailArray {
    SKUSettingDetailSlNo: string;
    SKUSettingId: string;
    SKUSettingDetailId: string;
    SKUSettingDetailParticulars: string;
    SKUSettingDetailSKUCodeExpression: string;
    SKUSettingDetailSKUNameExpression: string;
    undefined: string;
    ProcessId: string;
    ProcessName: string;
    SKUSettingDetailSKUMatchFilter: string;
    SKUSettingDetailFilterType: string;
  }
  