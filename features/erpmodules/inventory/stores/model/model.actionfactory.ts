import {
  GBDataStateAction,
  GBDataStateActionFactoryWN,
 
} from './../../../../../libs/gbdata/src/lib/store/gbdata.actions';
import {  ISelectListCriteria, } from './../../../../../libs/gbdata/src/lib/interfaces/ISectionCriteriaList';
import { IModel } from '../../models/IModel';
import { GetAll, GetData, MoveFirst, MoveLast, MoveNext, MovePrev, GetAllAndMoveFirst, SaveData, ClearData, DeleteData, AddData } from './model.actions';

export class ModelStateActionFactory extends GBDataStateActionFactoryWN<IModel> {
    GetData(id: number): GBDataStateAction<IModel> {
      return new GetData(id);
    }
    GetAll(scl: ISelectListCriteria): GBDataStateAction<IModel> {
      return new GetAll(scl);
    }
    MoveFirst(): GBDataStateAction<IModel> {
      return new MoveFirst();
    }
    MoveNext(): GBDataStateAction<IModel> {
      return new MoveNext();
    }
    MovePrev(): GBDataStateAction<IModel> {
      return new MovePrev();
    }
    MoveLast(): GBDataStateAction<IModel> {
      return new MoveLast();
    }
    AddData(payload: IModel): GBDataStateAction<IModel> {
      return new AddData(payload);
    }
    SaveData(payload: IModel): GBDataStateAction<IModel> {
      return new SaveData(payload);
    }
    ClearData(): GBDataStateAction<IModel> {
      return new ClearData();
    }
    DeleteData(): GBDataStateAction<IModel> {
      return new DeleteData();
    }
  }
