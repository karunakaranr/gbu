import { GBDataStateAction, GBDataStateActionFactoryWN, ISelectListCriteria,} from '@goodbooks/gbdata';
import { Ipackset } from '../../models/Ipackset';
import { GetAll, GetData, MoveFirst, MoveLast, MoveNext, MovePrev, SaveData, ClearData, DeleteData, AddData } from './packset.action';

export class PackSetStateActionFactory extends GBDataStateActionFactoryWN<Ipackset> {
    GetData(id: number): GBDataStateAction<Ipackset> {
      return new GetData(id);
    }
    GetAll(scl: ISelectListCriteria): GBDataStateAction<Ipackset> {
      return new GetAll(scl);
    }
    MoveFirst(): GBDataStateAction<Ipackset> {
      return new MoveFirst();
    }
    MoveNext(): GBDataStateAction<Ipackset> {
      return new MoveNext();
    }
    MovePrev(): GBDataStateAction<Ipackset> {
      return new MovePrev();
    }
    MoveLast(): GBDataStateAction<Ipackset> {
      return new MoveLast();
    }
    AddData(payload: Ipackset): GBDataStateAction<Ipackset> {
      return new AddData(payload);
    }
    SaveData(payload: Ipackset): GBDataStateAction<Ipackset> {
      return new SaveData(payload);
    }
    ClearData(): GBDataStateAction<Ipackset> {
      return new ClearData();
    }
    DeleteData(): GBDataStateAction<Ipackset> {
      return new DeleteData();
    }
  }
