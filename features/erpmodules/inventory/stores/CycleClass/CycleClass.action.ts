import { CycleClass } from '../../models/ICycleClass';
import {
  GBDataStateAction,
  GBDataStateActionFactory,
  GBDataStateActionFactoryWN,
 
} from './../../../../../libs/gbdata/src/lib/store/gbdata.actions';
import {  ISelectListCriteria, } from './../../../../../libs/gbdata/src/lib/interfaces/ISectionCriteriaList';

  const CycleClassEntityName = 'CycleClass';
  export class GetData extends GBDataStateAction<CycleClass> {
    static readonly type = '[' + CycleClassEntityName + '] getData';
    constructor(public id: number) {
      super();
    }
  }
  export class SaveData extends GBDataStateAction<CycleClass> {
    static readonly type = '[' + CycleClassEntityName + '] saveData';
    constructor(
      public payload: CycleClass
    ) {
      super();
    }
  }
  export class AddData extends GBDataStateAction<CycleClass> {
    static readonly type = '[' + CycleClassEntityName + '] addData';
    constructor(
      public payload: CycleClass
    ) {
      super();
    }
  }
  export class GetAll extends GBDataStateAction<CycleClass> {
    static readonly type = '[' + CycleClassEntityName + '] getAll';
    constructor(public scl: ISelectListCriteria) {
      super();
    }
  }
  export class GetAllAndMoveFirst extends GBDataStateAction<CycleClass> {
    static readonly type = '[' + CycleClassEntityName + '] getAllAndMoveFirst';
    constructor(public scl: ISelectListCriteria) {
      super();
    }
  }
  export class MoveFirst extends GBDataStateAction<CycleClass> {
    static readonly type = '[' + CycleClassEntityName + '] MoveFirst';
    constructor() {
      super();
    }
  }
  export class MovePrev extends GBDataStateAction<CycleClass> {
    static readonly type = '[' + CycleClassEntityName + '] MovePrev';
    constructor() {
      super();
    }
  }
  export class MoveNext extends GBDataStateAction<CycleClass> {
    static readonly type = '[' + CycleClassEntityName + '] MoveNext';
    constructor() {
      super();
    }
  }
  export class MoveLast extends GBDataStateAction<CycleClass> {
    static readonly type = '[' + CycleClassEntityName + '] MoveLast';
    constructor() {
      super();
    }
  }
  export class ClearData extends GBDataStateAction<CycleClass> {
    static readonly type = '[' + CycleClassEntityName + '] ClearData';
    constructor() {
      super();
    }
  }
  export class DeleteData extends GBDataStateAction<CycleClass> {
    static readonly type = '[' + CycleClassEntityName + '] DeleteData';
    constructor() {
      super();
    }
  }


