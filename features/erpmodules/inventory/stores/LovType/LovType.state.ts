// import { State, Action, StateContext, Selector } from '@ngxs/store';
// import { LovType } from '../../models/ILovType';
// import { GBDataState, GBDataStateModel } from './../../../../../libs/gbdata/src/lib/store/gbdata.state';
// import { LovTypeService } from '../../services/LovType/LovType/LovType.service';
// import { Inject, Injectable } from '@angular/core';
// import { GetAll, MoveFirst, MoveNext, MovePrev, MoveLast, GetData, SaveData, GetAllAndMoveFirst,ClearData, DeleteData, AddData  } from './LovType.action';


//   export class LovTypeStateModel extends GBDataStateModel<LovType> {
//   }

//   @State<LovTypeStateModel>({
//     name: 'LovType',
//     defaults: {
//       data: [],
//       areDataLoaded: false,
//       currentData: {
//         model: undefined,
//         dirty: false,
//         errors: '',
//         status: ''
//       },
//       currentIndex: 0,
//     },
//   })
//   @Injectable()
//   export class LovTypeState extends GBDataState<LovType> {
//     constructor(@Inject(LovTypeService) dataService: LovTypeService) {
//       super(dataService, null);
//     }

//     @Action(GetData)
//     GetData(context: StateContext<GBDataStateModel<LovType>>,
//       { id }: GetData
//     ) {
//       return super.Super_GetData(context, id.toString());
//     }
//     @Action(ClearData)
//     ClearData(context: StateContext<GBDataStateModel<LovType>>) {
//       return super.Super_ClearData(context);
//     }
//     @Action(GetAll)
//     GetAll(context: StateContext<GBDataStateModel<LovType>>, {scl}: GetAll) {
//       return super.Super_GetAll(context, {scl: scl});

//     }
//     @Action(GetAllAndMoveFirst)
//     GetAllAndMoveFirst(context: StateContext<GBDataStateModel<LovType>>, {scl}: GetAll) {

//     }
//     @Action(MoveFirst)
//     MoveFirst(context: StateContext<GBDataStateModel<LovType>>) {
//       return super.Super_MoveFirst(context);
//     }
//     @Action(MovePrev)
//     MovePrev(context: StateContext<GBDataStateModel<LovType>>) {
//       return super.Super_MovePrev(context);
//     }
//     @Action(MoveNext)
//     MoveNext(context: StateContext<GBDataStateModel<LovType>>) {
//       return super.Super_MoveNext(context);
//     }
//     @Action(MoveLast)
//     MoveLast(context: StateContext<GBDataStateModel<LovType>>) {
//       return super.Super_MoveLast(context);
//     }
//     @Action(DeleteData)
//     DeleteData(context: StateContext<GBDataStateModel<LovType>>) {
//       console.log("context",context);
//       console.log("super.Super_DeleteData(context)",super.Super_DeleteData(context))
//       return super.Super_DeleteData(context);
//     }
//     @Action(SaveData)
//     SaveData(context: StateContext<GBDataStateModel<LovType>>,
//       { payload, id }: { payload: any; id: any }
//     ) {
//       return super.Super_SaveData(context, {payload: payload, id: id});
//     }
//     @Action(AddData)
//     AddData(context: StateContext<GBDataStateModel<LovType>>,
//       { payload }: { payload: any }
//     ) {
//       return super.Super_AddData(context, {payload: payload});
//     }
//   }
