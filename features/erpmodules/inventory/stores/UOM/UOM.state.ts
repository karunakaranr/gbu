import { State, Action, StateContext, Selector } from '@ngxs/store';
import { IUOM } from '../../models/IUOM';
import { GBDataState, GBDataStateModel } from './../../../../../libs/gbdata/src/lib/store/gbdata.state';
import { UOMService } from '../../services/UOM/uom.service';
import { Inject, Injectable } from '@angular/core';
import { GetAll, MoveFirst, MoveNext, MovePrev, MoveLast, GetData, SaveData, GetAllAndMoveFirst,ClearData, DeleteData, AddData, FormEditOption   } from './UOM.actions';


  export class UOMStateModel extends GBDataStateModel<IUOM> {
  }
  export class FormEditStateModel {
    formedit;
  }

  @State<UOMStateModel>({
    name: 'UOM',
    defaults: {
      data: [],
      areDataLoaded: false,
      currentData: {
        model: undefined,
        dirty: false,
        errors: '',
        status: ''
      },
      currentIndex: 0,
    },
  })

  @State<FormEditStateModel>({
    name: 'Form',
    defaults: {
      formedit: null,
    },
  })
  @Injectable()
  
  export class UOMState extends GBDataState<IUOM> {
    // constructor(@Inject(UOMService) dataService: UOMService) {
    //   super(dataService, null);
    // }
    @Selector() static FormEditable(state: FormEditStateModel) {
      return state.formedit;
    }
    @Action(GetData)
    GetData(context: StateContext<GBDataStateModel<IUOM>>,
      { id }: GetData
    ) {
      return super.Super_GetData(context, id.toString());
    }
    @Action(ClearData)
    ClearData(context: StateContext<GBDataStateModel<IUOM>>) {
      return super.Super_ClearData(context);
    }
    @Action(GetAll)
    GetAll(context: StateContext<GBDataStateModel<IUOM>>, {scl}: GetAll) {
      return super.Super_GetAll(context, {scl: scl});
    }
    @Action(GetAllAndMoveFirst)
    GetAllAndMoveFirst(context: StateContext<GBDataStateModel<IUOM>>, {scl}: GetAll) {

    }
    @Action(MoveFirst)
    MoveFirst(context: StateContext<GBDataStateModel<IUOM>>) {
      return super.Super_MoveFirst(context);
    }
    @Action(MovePrev)
    MovePrev(context: StateContext<GBDataStateModel<IUOM>>) {
      return super.Super_MovePrev(context);
    }
    @Action(MoveNext)
    MoveNext(context: StateContext<GBDataStateModel<IUOM>>) {
      return super.Super_MoveNext(context);
    }
    @Action(MoveLast)
    MoveLast(context: StateContext<GBDataStateModel<IUOM>>) {
      return super.Super_MoveLast(context);
    }
    @Action(DeleteData)
    DeleteData(context: StateContext<GBDataStateModel<IUOM>>) {
      return super.Super_DeleteData(context);
    }
    @Action(SaveData)
    SaveData(context: StateContext<GBDataStateModel<IUOM>>,
      { payload, id }: { payload: any; id: any }
    ) {
      return super.Super_SaveData(context, {payload: payload, id: id});
    }
    @Action(AddData)
    AddData(context: StateContext<GBDataStateModel<IUOM>>,
      { payload }: { payload: any }
    ) {
      return super.Super_AddData(context, {payload: payload});
    }

    @Action(FormEditOption)
    FormEditOption(context: StateContext<FormEditStateModel>, FormEdit1: FormEditOption) {
      const state = context.getState();
      context.setState({ ...state, formedit: FormEdit1.FormEdit});
    }
  }
