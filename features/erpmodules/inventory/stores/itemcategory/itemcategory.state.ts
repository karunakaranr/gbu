import { State, Action, StateContext, Selector } from '@ngxs/store';
import { IItemCategory } from '../../models/Iitemcategory';
import { GBDataState, GBDataStateModel } from './../../../../../libs/gbdata/src/lib/store/gbdata.state';
import { ItemCategoryService } from '../../services/itemcategory/itemcategory.service';
import { Inject, Injectable } from '@angular/core';
import { GetAll, MoveFirst, MoveNext, MovePrev, MoveLast, GetData, SaveData, GetAllAndMoveFirst,ClearData, DeleteData, AddData  } from './itemcategory.actions';


  export class ItemcategoryStateModel extends GBDataStateModel<IItemCategory> {
  }

  @State<ItemcategoryStateModel>({
    name: 'Itemcategory',
    defaults: {
      data: [],
      areDataLoaded: false,
      currentData: {
        model: undefined,
        dirty: false,
        errors: '',
        status: ''
      },
      currentIndex: 0,
    },
  })
  @Injectable()
  export class ItemcategoryState extends GBDataState<IItemCategory> {
    // constructor(@Inject(ItemcategoryService) dataService: ItemcategoryService) {
    //   super(dataService, null);
    // }

    @Action(GetData)
    GetData(context: StateContext<GBDataStateModel<IItemCategory>>,
      { id }: GetData
    ) {
      return super.Super_GetData(context, id.toString());
    }
    @Action(ClearData)
    ClearData(context: StateContext<GBDataStateModel<IItemCategory>>) {
      return super.Super_ClearData(context);
    }
    @Action(GetAll)
    GetAll(context: StateContext<GBDataStateModel<IItemCategory>>, {scl}: GetAll) {
      return super.Super_GetAll(context, {scl: scl});
    }
    @Action(GetAllAndMoveFirst)
    GetAllAndMoveFirst(context: StateContext<GBDataStateModel<IItemCategory>>, {scl}: GetAll) {

    }
    @Action(MoveFirst)
    MoveFirst(context: StateContext<GBDataStateModel<IItemCategory>>) {
      return super.Super_MoveFirst(context);
    }
    @Action(MovePrev)
    MovePrev(context: StateContext<GBDataStateModel<IItemCategory>>) {
      return super.Super_MovePrev(context);
    }
    @Action(MoveNext)
    MoveNext(context: StateContext<GBDataStateModel<IItemCategory>>) {
      return super.Super_MoveNext(context);
    }
    @Action(MoveLast)
    MoveLast(context: StateContext<GBDataStateModel<IItemCategory>>) {
      return super.Super_MoveLast(context);
    }
    @Action(DeleteData)
    DeleteData(context: StateContext<GBDataStateModel<IItemCategory>>) {
      return super.Super_DeleteData(context);
    }
    @Action(SaveData)
    SaveData(context: StateContext<GBDataStateModel<IItemCategory>>,
      { payload, id }: { payload: any; id: any }
    ) {
      return super.Super_SaveData(context, {payload: payload, id: id});
    }
    @Action(AddData)
    AddData(context: StateContext<GBDataStateModel<IItemCategory>>,
      { payload }: { payload: any }
    ) {
      return super.Super_AddData(context, {payload: payload});
    }
  }
