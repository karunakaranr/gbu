import {
  GBDataStateAction,
  GBDataStateActionFactoryWN,
 
} from './../../../../../libs/gbdata/src/lib/store/gbdata.actions';
import {  ISelectListCriteria, } from './../../../../../libs/gbdata/src/lib/interfaces/ISectionCriteriaList';
import { IItemCategory } from '../../models/Iitemcategory';
import { GetAll, GetData, MoveFirst, MoveLast, MoveNext, MovePrev, SaveData, ClearData, DeleteData, AddData } from './itemcategory.actions';

export class ItemcategoryStateActionFactory extends GBDataStateActionFactoryWN<IItemCategory> {
  GetData(id: number): GBDataStateAction<IItemCategory> {
    return new GetData(id);
  }
  GetAll(scl: ISelectListCriteria): GBDataStateAction<IItemCategory> {
    return new GetAll(scl);
  }
  MoveFirst(): GBDataStateAction<IItemCategory> {
    return new MoveFirst();
  }
  MoveNext(): GBDataStateAction<IItemCategory> {
    return new MoveNext();
  }
  MovePrev(): GBDataStateAction<IItemCategory> {
    return new MovePrev();
  }
  MoveLast(): GBDataStateAction<IItemCategory> {
    return new MoveLast();
  }
  AddData(payload: IItemCategory): GBDataStateAction<IItemCategory> {
    return new AddData(payload);
  }
  SaveData(payload: IItemCategory): GBDataStateAction<IItemCategory> {
    return new SaveData(payload);
  }
  ClearData(): GBDataStateAction<IItemCategory> {
    return new ClearData();
  }
  DeleteData(): GBDataStateAction<IItemCategory> {
    return new DeleteData();
  }
}
