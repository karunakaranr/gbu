import { ILov } from '../../models/ILov';
import {
  GBDataStateAction,

} from './../../../../../libs/gbdata/src/lib/store/gbdata.actions';
import { ISelectListCriteria, } from './../../../../../libs/gbdata/src/lib/interfaces/ISectionCriteriaList';

const LovEntityName = 'Lov';
export class GetData extends GBDataStateAction<ILov> {
  static readonly type = '[' + LovEntityName + '] getData';
  constructor(public id: number) {
    super();
  }
}
export class SaveData extends GBDataStateAction<ILov> {
  static readonly type = '[' + LovEntityName + '] saveData';
  constructor(
    public payload: ILov
  ) {
    super();
  }
}
export class AddData extends GBDataStateAction<ILov> {
  static readonly type = '[' + LovEntityName + '] addData';
  constructor(
    public payload: ILov
  ) {
    super();
  }
}
export class GetAll extends GBDataStateAction<ILov> {
  static readonly type = '[' + LovEntityName + '] getAll';
  constructor(public scl: ISelectListCriteria) {
    super();
  }
}
export class GetAllAndMoveFirst extends GBDataStateAction<ILov> {
  static readonly type = '[' + LovEntityName + '] getAllAndMoveFirst';
  constructor(public scl: ISelectListCriteria) {
    super();
  }
}
export class MoveFirst extends GBDataStateAction<ILov> {
  static readonly type = '[' + LovEntityName + '] MoveFirst';
  constructor() {
    super();
  }
}
export class MovePrev extends GBDataStateAction<ILov> {
  static readonly type = '[' + LovEntityName + '] MovePrev';
  constructor() {
    super();
  }
}
export class MoveNext extends GBDataStateAction<ILov> {
  static readonly type = '[' + LovEntityName + '] MoveNext';
  constructor() {
    super();
  }
}
export class MoveLast extends GBDataStateAction<ILov> {
  static readonly type = '[' + LovEntityName + '] MoveLast';
  constructor() {
    super();
  }
}
export class ClearData extends GBDataStateAction<ILov> {
  static readonly type = '[' + LovEntityName + '] ClearData';
  constructor() {
    super();
  }
}
export class DeleteData extends GBDataStateAction<ILov> {
  static readonly type = '[' + LovEntityName + '] DeleteData';
  constructor() {
    super();
  }
}


