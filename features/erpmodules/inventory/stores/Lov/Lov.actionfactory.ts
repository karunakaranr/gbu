import {
    GBDataStateAction,
    GBDataStateActionFactoryWN,
   
  } from './../../../../../libs/gbdata/src/lib/store/gbdata.actions';
  import {  ISelectListCriteria, } from './../../../../../libs/gbdata/src/lib/interfaces/ISectionCriteriaList';
  import { Lov } from '../../models/ILov';
  import { GetAll, GetData, MoveFirst, MoveLast, MoveNext, MovePrev, SaveData, ClearData, DeleteData, AddData } from './Lov.action';
  
  export class LovStateActionFactory extends GBDataStateActionFactoryWN<Lov> {
      GetData(id: number): GBDataStateAction<Lov> {
        return new GetData(id);
      }
      GetAll(scl: ISelectListCriteria): GBDataStateAction<Lov> {
        return new GetAll(scl);
      }
      MoveFirst(): GBDataStateAction<Lov> {
        return new MoveFirst();
      }
      MoveNext(): GBDataStateAction<Lov> {
        return new MoveNext();
      }
      MovePrev(): GBDataStateAction<Lov> {
        return new MovePrev();
      }
      MoveLast(): GBDataStateAction<Lov> {
        return new MoveLast();
      }
      AddData(payload: Lov): GBDataStateAction<Lov> {
        return new AddData(payload);
      }
      SaveData(payload: Lov): GBDataStateAction<Lov> {
        return new SaveData(payload);
      }
      ClearData(): GBDataStateAction<Lov> {
        return new ClearData();
      }
      DeleteData(): GBDataStateAction<Lov> {
        return new DeleteData();
      }
    }
  