import { Injectable } from '@angular/core';
import {GBHttpService} from './../../../../../libs/gbcommon/src/lib/services/HTTPService/GBHttp.service';
import {ISelectListCriteria } from './../../../../../libs/gbdata/src/lib/interfaces/ISectionCriteriaList';
import {GBBaseDBDataService} from './../../../../../libs/gbdata/src/lib/services/gbbasedbdata.service';
import { Observable } from 'rxjs';

import { IModel} from '../../models/IModel';
const urls = require('../../URLS/urls.json');

@Injectable({
  providedIn: 'root',
})
export class ModelDBService extends GBBaseDBDataService<IModel> {
  endPoint: string = urls.MODEL;

  constructor(http: GBHttpService) {
    super(http);
  }

  putData(data: IModel) {
    return super.putData(data);
  }

  postData(data: IModel) {
    return super.postData(data);
  }

  getData(idField: string, id: string): Observable<IModel> {
    return super.getData(idField, id);
  }

  deleteData(idField: string, id: string) {
    return super.deleteData(idField, id)
  }

  getList(scl: ISelectListCriteria): Observable<any> {
    return super.getList(scl);
  }
}
