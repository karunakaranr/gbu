import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { GBHttpService } from '@goodbooks/gbcommon';

@Injectable({
    providedIn: 'root',
})
export class UserListdbservice {
    rowcriteria ;
    constructor(public http: GBHttpService) { }

    public picklist(): Observable<any> {
        const url = '/fws/Menu.svc/ReportMenuDetailsForMenu/?UserId=-1499998941&MenuId=-1399989015';
        return this.http.httpget(url);
    }
    
    public reportData(pickobj) {
        let  criteria;
        criteria = {
            "ReportUri": pickobj[0].WebServiceUriTemplate,
            "Method": pickobj[0].MethodType,
            "ReportTitle": "",
            "CriteriaDTO": null,
            "MenuId": pickobj[0].MenuId,
            "WebserviceId": pickobj[0].WebServiceId,
            "PeriodFilter": 0,
            "RecordsPerPage": 0
        }
        this.rowcriteria = criteria
        let url;
        console.log("pickobj",criteria)
        let fullurl = 'http://169.56.148.10:82/gb4/cos/Common.svc/Report';
        let str =  fullurl.split('/gb4');
        let urlstr = str[str.length - 1];
        url = urlstr;
        console.log("api",url);
        return this.http.httppost(url, criteria);
    }
}
