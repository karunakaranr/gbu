import { Injectable } from '@angular/core';
import { GBHttpService } from '@goodbooks/gbcommon';
const urls = require('../../URLS/urls.json');

@Injectable({
  providedIn: 'root'
})
export class packReportDBService {
  constructor(public http: GBHttpService) { }
  endPoint: string = urls.packreport;
  public Reportservice() {
    return this.http.httpget(this.endPoint);
  }

  public dataservice(cri){
    let reporturl = urls.commonreport;
    return this.http.httppost(reporturl,cri);
  }

}
