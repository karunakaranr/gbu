import { Injectable } from '@angular/core';
import {GBHttpService} from './../../../../../libs/gbcommon/src/lib/services/HTTPService/GBHttp.service';
import {ISelectListCriteria } from './../../../../../libs/gbdata/src/lib/interfaces/ISectionCriteriaList';
import {GBBaseDBDataService} from './../../../../../libs/gbdata/src/lib/services/gbbasedbdata.service';
import { Observable } from 'rxjs';
import { IPack } from '../../models/IPack';


const urls = require('../../URLS/urls.json');

@Injectable({
  providedIn: 'root',
})
export class PackDBService extends GBBaseDBDataService<IPack> {
  endPoint: string = urls.Pack;


  constructor(http: GBHttpService) {
    super(http);
  }

  putData(data: IPack) {
    return super.putData(data);
  }

  postData(data: IPack) {
    return super.postData(data);
  }

  getData(idField: string, id: string): Observable<IPack> {
    return super.getData(idField, id);
  }

  deleteData(idField: string, id: string) {
    return super.deleteData(idField, id)
  }

  getList(scl: ISelectListCriteria): Observable<any> {
    return super.getList(scl);
  }
}
