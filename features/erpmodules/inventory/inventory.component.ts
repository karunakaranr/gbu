import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'goodbooks-inventory',
  template: `
  `,
  styles: ['ul li a {margin-bottom: 0px; color: black}'],
})
export class InventoryComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
