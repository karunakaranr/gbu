import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { IDriver } from '../../models/IDriver';




 
@Injectable({
  providedIn: 'root'
})
export class DriverService extends GBBaseDataServiceWN<IDriver> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IDriver>) {
    super(dbDataService, 'DriverId');
  }
}
