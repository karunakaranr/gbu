import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { LogisticsURLS } from '../../URLS/urls';
import { DriverService } from '../../service/Driver/Driver.Service';
import { IDriver } from '../../models/IDriver';
// import * as JsonName from ‘JsonPath’ './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/PackSet.json';
import * as DriverJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/Driver.json'



@Component({
  selector: 'app-Driver',
  templateUrl: './Driver.component.html',
  styleUrls: ['./Driver.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'DriverId'},
    { provide: 'url', useValue: LogisticsURLS.Driver },
    { provide: 'DataService', useClass: DriverService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class DriverComponent extends GBBaseDataPageComponentWN<IDriver> {
  title: string = 'Driver'
  DriverJSON = DriverJSON;


  form: GBDataFormGroupWN<IDriver> = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'Driver', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.FormFillFunction(arrayOfValues.DriverId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }


  public FormFillFunction(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(PackSet => {
        this.form.patchValue(PackSet);
      })
    }
  }


  public PicklistPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IDriver> {
  const dbds: GBBaseDBDataService<IDriver> = new GBBaseDBDataService<IDriver>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IDriver>, dbDataService: GBBaseDBDataService<IDriver>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IDriver> {
  return new GBDataPageService<IDriver>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}


