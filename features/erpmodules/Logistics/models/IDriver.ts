export interface IDriver {
  DriverId: number
  DriverDriverName: string
  DriverSex: number
  DriverDob: string
  DriverLicenseNumber: string
  DriverLicenseType: number
  DriverBatchNumber: string
  DriverValidUpto: string
  DriverIssueBy: string
  DriverIsEmployee: number
  EmployeeId: number
  AddressId: number
  ContactId: number
  ContactMobileNo: string
  EmployeeName: string
  ContactName: string
  ContactVersion: number
  AddressLine1: string
  AddressMobile: string
  AddressLine2: string
  AddressLine3: string
  AddressLine4: string
  AddressLine5: string
  AddressLocationId: number
  AddressLocationName: string
  AddressCityId: number
  AddressCityName: string
  AddressStandardRegionId: number
  AddressStandardRegionName: string
  AddressCountryId: number
  AddressCountryName: string
  AddressZipCode: string
  AddressMail: string
  AddressStateId: number
  AddressStateName: string
  AddressPhone: string
  AddressGeoCode: string
  AddressWeb: string
  AddressGSTIn: string
  AddressFax: string
  AddressVersion: number
  DriverCreatedById: number
  DriverCreatedOn: string
  DriverModifiedById: number
  DriverModifiedOn: string
  DriverBaseClinetId: number
  DriverSortOrder: number
  DriverStatus: number
  DriverVersion: number
  DriverSourceType: number
  AddressDTO: any
}
