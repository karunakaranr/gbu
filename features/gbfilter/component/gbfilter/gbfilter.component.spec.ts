import { GbFilterComponent } from './gbfilter.component';
import { FormBuilder } from '@angular/forms';
import { ChangeDetectorRef,Component, Inject, ViewEncapsulation } from '@angular/core';
// import {GBBasePageComponentNG, GBBasePageComponent} from './../../../../../libs/uicore/src/lib/components/Base/GBBasePage';
// import { GBDataPageServiceNG} from './../../../../../libs/uicore/src/lib/services/gbpage.service';
// import { GBBaseService } from './../../../../../libs/gbdata/src/lib/services/gbbasedata.service';
import { Store } from '@ngxs/store';
// import {GBHttpService} from './../../../../../libs/gbcommon/src/lib/services/HTTPService/GBHttp.service';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { GbFilterService } from './../../services/gbfilter/gbfilter.service';
const datajson = require('./../../filterapiresponse.json');
const criteriajsondata = require('./../../crietriajson.json');


describe('NewComponentComponent', () => {
	let fixture: GbFilterComponent;
  let formBuilderMock: FormBuilder;
	let store:Store;
	let dataservice : GbFilterService;
	let fb= new FormBuilder();
  let ref : ChangeDetectorRef
	beforeEach(() => {
    formBuilderMock = new FormBuilder();
    // fixture = new GbFilterComponent();
	fixture = new GbFilterComponent(formBuilderMock,dataservice,store,ref);
	fixture.ngOnInit();
	fixture.apply()
	});

	describe('Test: ngOnInit function', () => {
		it('check the service response', () => {
			const filterresponse = datajson[0].ReportCriter
			expect(fixture.ngOnInit()).toEqual(filterresponse);
		});
	});

	describe('Test: apply function', () => {
		it('check the criteria json with CriteriaAttributeValue (id)', () => {
			const jsoncriteria = criteriajsondata.AttributesCriteriaList
			expect(fixture.testid).toEqual(jsoncriteria);
		});
	});

	describe('Test: apply function', () => {
		it('check the criteria json with CriteriaAttributeValue (id&value)', () => {
			const jsoncriteria = criteriajsondata.AttributesCriteriaList
			expect(fixture.testidcode).toEqual(jsoncriteria);
		});
	});

	describe('Test: apply function', () => {
		it('check the criteria json with CriteriaAttributeValue (null)', () => {
			const jsoncriteria = criteriajsondata.AttributesCriteriaList
			expect(fixture.criteria.get('AttributesCriteriaList').value).toEqual(jsoncriteria);
		});
	});

	// describe('Test: apply function', () => {
	// 	it('check the criteria json', () => {
	// 		const jsoncriteria = criteriajsondata
	// 		expect(fixture.apply()).toEqual(jsoncriteria);
	// 	});
	// });
});
