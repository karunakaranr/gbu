export class GbFilterURL {
    public static filter = '/fws/Menu.svc/ReportMenuDetailsForMenu/?UserId=-1499998941&MenuId=-1399985911'
    public static useracess = '/ads/UserAccessRights.svc/OUPicklist';
    public static branch = '/ads/Branch.svc/SelectList';
    public static division = '/ads/Division.svc/SelectList';
    public static department = '/ads/Department.svc/SelectList';
    public static employeetype = '/fws/Gcm.svc/SelectList';
    public static stafftype = '/fws/Gcm.svc/SelectList';
    public static worktype = '/fws/Gcm.svc/SelectList';
    public static employee = '/prs/Employee.svc/GetEmployeePicklistSelect2';
    public static payroll = '/prs/PayGroup.svc/SelectList/?';
    public static village = '/prs/Village.svc/SelectList';
    public static apply = '/prs/Employee.svc/EmployeesDetailFormReport/?'
}