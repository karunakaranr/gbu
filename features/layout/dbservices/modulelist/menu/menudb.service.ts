import { Injectable } from '@angular/core';
import { Select, Store } from '@ngxs/store';

import { Observable, forkJoin } from 'rxjs';
import { IMenulist } from './../../../models/Imenu.model';
import { GBHttpService } from '@goodbooks/gbcommon';
import { GbUIURLS } from './../../../URLS/urls';
import { HttpClient, HttpParams } from '@angular/common/http';
import { FilterState } from './../../../../gbfilter/store/gbfilter.state'
import { AuthState } from './../../../../common/shared/stores/auth.state';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
@Injectable({
  providedIn: 'root'
})
export class MenudbService {

  constructor(private http: GBHttpService, private httpp: HttpClient) { }
  @Select(FilterState.Filter) filtercriteria$: Observable<any>;
  @Select(AuthState.AddDTO) LOGINDTO$: Observable<any>;
  @Select(ReportState.Sourcecriteriadrilldown) sourcerowcriteria$: Observable<any>;
  public MenulistService(ModuleId: any) {
    let userid;
    this.LOGINDTO$.subscribe(dto => {
      let Logindto = dto;
      userid = dto.UserId;
    })
    const param = new HttpParams({ fromObject: { UserId: userid, ModuleId: ModuleId } });
    let params = {
      UserId: userid,
      ModuleId: ModuleId
    }
    return this.http.httpgetparams(GbUIURLS.MENULIST, params)
    // const testurl = '/fws/Menu.svc/UserModuleMenuTree/?UserId=-1499998941&ModuleId=-1899999989'
    // return this.http.httpget(GbUIURLS.MENULIST+ModuleId);
    //  return this.http.httpget(testurl);

  }


  public exceldbservice(exceldata, menudata, type) {
    let criterias;
    this.sourcerowcriteria$.subscribe(data => {
      if (data) {
        if (data != "Empty") {
          criterias = data.CriteriaDTO
        }
      }
    })
    let weburl = menudata[0].WebServiceUriTemplate
    let re = /FirstNumber=1/gi;
    var newstr = weburl.replace(re, 'FirstNumber=-1');
    let x = /MaxResult=10/gi
    var Finalweburl = newstr.replace(x, 'MaxResult=-1');

    let criteria = {
      "ReportUri": Finalweburl,
      "Method": menudata[0].MethodType,
      "ReportTitle": exceldata.ReportViewName,
      "CriteriaDTO": criterias,
      "EntityId": menudata[0].EntityId,
      "IsExcelImport": type,
      "ReportFormatId": exceldata.TemplateId,
      "ReportId": exceldata.TemplateId,
      "ReportViewId": exceldata.ReportViewId
    }
    let url;
    let fullurl = 'http://169.56.148.10:82/gb4/cos/Common.svc/Report';
    let str = fullurl.split('/gb4');
    let urlstr = str[str.length - 1];
    url = urlstr;
    return this.http.httppost(url, criteria);
  }


  public savedata(url, criteria) {
    return this.http.httppost(url, criteria);
  }

  public savedraft(criteria) {
    let url = '/ads/Draft.svc/'
    return this.http.httppost(url, criteria);
  }

  public deletedata(url, idfied, selectedid) {
    let finalurl = url + idfied + selectedid
    return this.http.httpdelete(finalurl)
  }


  public mailgenrationdbservice(menudetails, pdfcsv, mailid) {
    let criterias;
    this.sourcerowcriteria$.subscribe(data => {
      if (data) {
        if (data != "Empty") {
          criterias = data.CriteriaDTO
        }
      }
    })
    let url;
    let fullurl = 'http://169.56.148.10:82/gb4/cos/Common.svc/AsynchReportMail/';
    let str = fullurl.split('/gb4');
    let urlstr = str[str.length - 1];
    url = urlstr;

    let criteria = {
      "ReportUri": menudetails[0].WebServiceUriTemplate,
      "Method": menudetails[0].MethodType,
      "ReportTitle": menudetails[0].ReportFormatName,
      "ReportFormatId": menudetails[0].DefaultReportFormatId,
      "CriteriaDTO": criterias,
      "ReportId": menudetails[0].DefaultReportViewId,
      "ReportViewId": 2,
      "MailIds": mailid,
      "EntityId": menudetails[0].EntityId,
      "AttachmentType": pdfcsv
    }

    return this.http.httppost(url, criteria);
  }

  public reportviewreportsettingdbservice(menudetails) {
    let url = '/fws/ReportView.svc/ReportViewDetails/?ReportId=' + menudetails[0].MenuReportId + '&ApplicableMenuId=' + menudetails[0].MenuId

    return this.http.httpget(url)
  }

  public reportviewaddnewsetting(menudetails) {
    let url = "/fws/Report.svc/?ReportId=" + menudetails[0].MenuReportId

    return this.http.httpget(url);
  }

  public reportviewformsettingdbservice(menudetails, reportid) {
    let url = '/fws/ReportView.svc/ReportViewFields/?ReportId=' + menudetails[0].MenuReportId + '&ReportViewId=' + reportid;

    let urls = '/cs/Criteria.svc/List/?ObjectCode=REPORTVIEW'
    let crteira = { "SectionCriteriaList": [{ "SectionId": 0, "AttributesCriteriaList": [{ "FieldName": "Report.Id", "OperationType": 1, "FieldValue": menudetails[0].MenuReportId, "InArray": null, "JoinType": 2 }, { "FieldName": "Id", "OperationType": 1, "FieldValue": reportid, "InArray": null, "JoinType": 0 }], "OperationType": 0 }] }
    const api1Call = this.http.httpget(url);
    const api2Call = this.http.httppost(urls, crteira);

    return forkJoin([api1Call, api2Call]);
  }

  public reportviewsavesettinggdbservice(savedata) {
    let url = "/fws/ReportView.svc/ReportViewWithFields/"

    return this.http.httppost(url, savedata)
  }

  public reportschedularreportdbservice(menudetails) {
    let url = "fws/Scheduler.svc/AutoScheduling/Get"

    let criteria = { "MenuId": menudetails[0].MenuId, "IsReportMenu": 0 }
    return this.http.httppost(url, criteria)
  }

  public reportschedularsavedbservice(savedetails) {
    let url = "/fws/Scheduler.svc/AutoScheduling"
    return this.http.httppost(url, savedetails)
  }

  public commentlistdbservice(id) {
    let url = "/cs/Criteria.svc/List/?ObjectCode=COMMENT"

    let criteria = {
      "SectionCriteriaList": [
        {
          "SectionId": 0,
          "AttributesCriteriaList": [
            {
              "FieldName": "ObjectId",
              "OperationType": 5,
              "FieldValue": "-1499999915",
              "InArray": null,
              "JoinType": 2
            },
            {
              "FieldName": "ObjectTypeId",
              "OperationType": 5,
              "FieldValue": "-1899999870",
              "InArray": null,
              "JoinType": 0
            }
          ]
        }
      ]
    }
    return this.http.httppost(url, criteria)
  }

  public commentnewsavedbservice(adddnewcriteria) {
    let url = "/fws/Comment.svc/"
    return this.http.httppost(url, adddnewcriteria)
  }
}





