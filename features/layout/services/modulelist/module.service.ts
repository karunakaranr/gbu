import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { Moduledbservice } from './../../dbservices/modulelist/moduleDB.service';
import { IModulelist } from './../../models/Imodule.model';
@Injectable({
  providedIn: 'root',
})
export class Moduleservice {

  constructor(public moduleDBservice: Moduledbservice) { }

  public modulelistservice(): Observable<IModulelist> {
    return this.moduleDBservice.ModulelisetService();
  }

}
