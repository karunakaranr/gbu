import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GbmenutreeComponent } from './gbmenutree.component';

describe('GbmenutreeComponent', () => {
  let component: GbmenutreeComponent;
  let fixture: ComponentFixture<GbmenutreeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GbmenutreeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GbmenutreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
