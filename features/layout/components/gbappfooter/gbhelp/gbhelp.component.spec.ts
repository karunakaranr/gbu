import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GbhelpComponent } from './gbhelp.component';

describe('GbhelpComponent', () => {
  let component: GbhelpComponent;
  let fixture: ComponentFixture<GbhelpComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GbhelpComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GbhelpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
