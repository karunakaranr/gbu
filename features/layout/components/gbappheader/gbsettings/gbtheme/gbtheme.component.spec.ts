import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GbThemeComponent } from './gbtheme.component';

describe('GbThemeComponent', () => {
  let component: GbThemeComponent;
  let fixture: ComponentFixture<GbThemeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GbThemeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GbThemeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
