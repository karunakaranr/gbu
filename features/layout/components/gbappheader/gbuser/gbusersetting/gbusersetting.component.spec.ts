import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GbusersettingComponent } from './gbusersetting.component';

describe('GbusersettingComponent', () => {
  let component: GbusersettingComponent;
  let fixture: ComponentFixture<GbusersettingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GbusersettingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GbusersettingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
