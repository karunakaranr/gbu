import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GbclientComponent } from './gbclient.component';

describe('GbclientComponent', () => {
  let component: GbclientComponent;
  let fixture: ComponentFixture<GbclientComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GbclientComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GbclientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
