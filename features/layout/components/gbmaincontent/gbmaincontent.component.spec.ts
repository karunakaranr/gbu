import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GbmaincontentComponent } from './gbmaincontent.component';

describe('GbmaincontentComponent', () => {
  let component: GbmaincontentComponent;
  let fixture: ComponentFixture<GbmaincontentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GbmaincontentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GbmaincontentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
