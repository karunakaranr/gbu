import { Component, OnInit, ViewChild} from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { GBBasePageComponentNG } from '@goodbooks/uicore';
import { GbformactionsComponent } from './gbformheader/gbformactions/gbformactions.component';
import { GbformheaderComponent } from './gbformheader/gbformheader.component';
import {GbformtoolbarComponent} from './gbformtoolbar/gbformtoolbar.component';
import { Select } from '@ngxs/store';
import { LayoutState } from 'features/layout/store/layout.state';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-gbmaincontent',
  templateUrl: './gbmaincontent.component.html',
  styleUrls: ['./gbmaincontent.component.scss']
})
export class GbmaincontentComponent implements OnInit {
  
  public color;
  public curcomp:any;
  change:boolean;
  @Select(LayoutState.OpenMainBar) OpenMenuBar$: Observable<boolean>;
  @ViewChild(RouterOutlet) page: RouterOutlet;
  @ViewChild(GbformheaderComponent) formheader: GbformheaderComponent;
  @ViewChild(GbformtoolbarComponent) formtoolbar: GbformtoolbarComponent;
  @ViewChild(GbformactionsComponent) formdata: GbformactionsComponent;

  ngOnInit(): void {
    this.OpenMenuBar$.subscribe(res => {
      this.change = res;
      if(this.change){
        this.setStyle('--menubarheight', '0px');
      } else {
        this.setStyle('--menubarheight', '66px');
      }
      console.log("Menu:",this.change)
    })
  }

  setStyle(name,value) {
    document.documentElement.style.setProperty(name, value);
  }

  public onActivate(){
    if (this.page){
      if (this.page.component){
        this.curcomp=this.page.component as GBBasePageComponentNG;
          //this.formtoolbar.form.formComponent= this.curcomp;
          this.formheader.forms.formComponent = this.curcomp;
          
      }
    }
  }
}
