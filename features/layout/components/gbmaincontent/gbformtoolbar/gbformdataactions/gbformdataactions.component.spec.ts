import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GbformdataactionsComponent } from './gbformdataactions.component';

describe('GbformdataactionsComponent', () => {
  let component: GbformdataactionsComponent;
  let fixture: ComponentFixture<GbformdataactionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GbformdataactionsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GbformdataactionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
