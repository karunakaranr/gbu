import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GbformdatatoolbarComponent } from './gbformdatatoolbar.component';

describe('GbformdatatoolbarComponent', () => {
  let component: GbformdatatoolbarComponent;
  let fixture: ComponentFixture<GbformdatatoolbarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GbformdatatoolbarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GbformdatatoolbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
