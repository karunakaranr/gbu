import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GbformheaderComponent } from './gbformheader.component';

describe('GbformheaderComponent', () => {
  let component: GbformheaderComponent;
  let fixture: ComponentFixture<GbformheaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GbformheaderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GbformheaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
