import { Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { LayoutState } from './../../../../store/layout.state'
import { Observable } from 'rxjs';
import { FilterState } from 'features/gbfilter/store/gbfilter.state';


@Component({
  selector: 'app-gbformtitle',
  templateUrl: './gbformtitle.component.html',
  styleUrls: ['./gbformtitle.component.scss']
})
export class GbformtitleComponent implements OnInit {
  viewvalue : string;
  viewmode :string;
  @Select(LayoutState.Title) view$: Observable<any>;
  @Select(FilterState.Filter) filtercriteria$: Observable<any>;
  @Select(LayoutState.Visibilebuttons) visiblebuttons$: Observable<string>;
  MenuPath: string;
  FormTitle: string;
  constructor() { }

  ngOnInit(): void {
    this.visiblebuttons$.subscribe(res => {
      if(res){
        this.viewmode = res;
        console.log("Modulemnupath:",this.viewmode)
      }
    })
    this.view$.subscribe(data => {
      if (data){
        const splitTitle = data.split('-'); // Splitting the data
        this.MenuPath = splitTitle[0]; // Storing the first part
        this.FormTitle = splitTitle[1]; // Storing the second part
        console.log("First part:", this.MenuPath);
        console.log("Second part:", this.FormTitle);
      }
    })
  }
}
