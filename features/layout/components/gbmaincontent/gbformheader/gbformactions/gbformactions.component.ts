import { Component, ElementRef, Injectable, Input, OnInit, ViewChild, isDevMode } from '@angular/core';
import { GBBasePageComponentNG } from '@goodbooks/uicore';
import { Observable } from 'rxjs';
import { Select, Store } from '@ngxs/store';
import { LayoutState } from './../../../../store/layout.state';
import { ReportState } from './../../../../../commonreport/datastore/commonreport.state'
import { FilterState } from './../../../../../gbfilter/store/gbfilter.state'
import { AuthState } from './../../../../../common/shared/stores/auth.state'
import { MenuserviceService } from './../../../../services/menu/menuservice.service'
import { GBHttpService } from '@goodbooks/gbcommon';
import { MatDialog } from '@angular/material/dialog';
import * as utf8 from "utf8";
import { Router } from '@angular/router';
import { GblogComponent } from 'features/gblog/components/gblog/gblog.component';
import { GbaboutComponent } from 'features/gbabout/components/gbabout/gbabout.component';
import { FormEditable, FormUnEditable, FormControlValue, ResetForm, Movenextpreviousid, InnerExceptionError } from 'features/layout/store/layout.actions';
import { RowDataPassing, SelectedViewtype, SelectedformViewtype } from 'features/commonreport/datastore/commonreport.action';
import { take } from 'rxjs/operators';
import { DialogBoxComponent } from 'features/erpmodules/samplescreens/screens/DialogBox/DialogBox.component';
import { formactionservice } from 'features/layout/services/formaction/formaction.sevice';
import { IBizTransaction } from 'features/layout/models/IBizTransaction';
import { HttpClient } from '@angular/common/http';
import { Reporviewtsettingreport } from 'features/layout/models/reportviewsetting.model';
import { ReportviewSettngaddnew } from 'features/layout/models/reportviewsettingaddnew.model';
import { ControlValueAccessor, FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { reportscheduler } from 'features/layout/models/reportscheduler.model';
import { ILoginDTO } from 'features/common/shared/stores/auth.model';
const globalbase64js = require("base-64");
const globalpako = require('pako');
@Injectable({
  providedIn: 'root'
})
@Component({
  selector: 'app-gbformactions',
  templateUrl: './gbformactions.component.html',
  styleUrls: ['./gbformactions.component.scss']
})
export class GbformactionsComponent implements OnInit {
  @Select(LayoutState.ThemeClass) themeClass$: Observable<string>;
  @Select(ReportState.SelectedViewtype) selectedview$: Observable<any>;
  @Select(LayoutState.Visibilebuttons) visiblebuttons$: Observable<string>;
  @Select(LayoutState.FormValidationvalues) formvaldiation$: Observable<string>;
  @Select(ReportState.CriteriaConfigArray) rowcerteria$: Observable<any>;
  @Select(ReportState.Sourcecriteriadrilldown) sourcerowcriteria$: Observable<any>;
  @Select(FilterState.Reportformatselection) reportformatselection$: Observable<any>;
  @Select(FilterState.Filter) filtercriteria$: Observable<any>;
  @Select(AuthState.AddDTO) LOGINDTO$: Observable<any>;
  @Select(LayoutState.Path) path$: Observable<any>;
  @Select(ReportState.Reportid) Reportid$: Observable<any>;
  @Select(LayoutState.FormEdit) edit$: Observable<boolean>;
  @Select(LayoutState.TotalReportPages) totalreportpages$ : Observable<any>;
  @Select(ReportState.ListofViewtypes) listofview$: Observable<any>;
  @Select(LayoutState.Versionservicurl) jsaperdetails$: Observable<any>
  @Select(LayoutState.FormControlValue) formcontrol$ : Observable<any>
  @Select(LayoutState.InnerExceptionError) InnerExceptionError$ : Observable<any>
  @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
  @Select(LayoutState.RolesAndRights) rolesandrights$: Observable<any>;
  @Select(LayoutState.Movenxtpreid) movenextpreviousid$: Observable<any>;
  @Select(AuthState.AddDTO) loginDTOs$: Observable<ILoginDTO>;
  @Input() public _formComponent: GBBasePageComponentNG;
  @Input() public FormGroup: GBBasePageComponentNG;
  @ViewChild('emailid', { static: false }) emailid: ElementRef;
  @ViewChild('pdfinputmail', { static: false }) pdfinputmail: ElementRef;
  @ViewChild('csvinputmail', { static: false }) csvinputmail: ElementRef;
  LOGINDTO:ILoginDTO;
  public Infoformid : string
  public Inforformname : string
  public Infostatus :string
  public Infolastcreatedon : string
  public Infolastmodifiedon : string
  public Infoversion : string
  public viewmode:string;
  public exceldata;
  public menudetails;
  public movenextpreid
  public firstnumber:number;
  public maxresult:number;
  public MaxResult:string;
  public maxnum:number;
  public RolesAndRights:IBizTransaction
  public CopyFromJSON;
  public CopyFromId:number;
  public CopyFrom:boolean = false
  showEvalCondition: boolean = false;
  AdvancedSchdulerModels:boolean = false;
  dummy = 1;
  TotalRecord: number;
  public disableButton = true;
  editDisableButton: boolean = false;
  buttonOpacity: number = 1;
  editButtonOpacity: number = 1;
  rowData: any;
  color:any;
  isReadOnly: boolean = true;
  Save: boolean = false;
  Reportviewsetttingtype:Boolean = false;
  CommentListviewData:any;
  SelectedCommentData:any;
  InnerExceptionError:string;
  public viewtypes = [];
  public formviewtypes = ['Instrumentmasterform1','Instrumentmasterform2' ]
  reportsettingform:FormGroup
  reportScheduler:FormGroup;
  ReportViewFieldsDTOs: FormArray
  ReportId:number
  form = this.fb.group({
    CommentId: 0,
    UserId: '',
    CommentTimeStamp: '',
    CommentCommentStatuss: 0,
    CommentAccessType: "0",
    PrivateUserGroupId: -1,
    CommentObjectTypeId: '',
    CommentObjectId: '',
    CommentCommentText: '',
    CommentSecurityLevel: '0',
    ParentCommentId: '-1',
    CommentVersion: 1
  });

  public ReporttableScheduler: reportscheduler;
  public ReportSchedularActiontypes = [
    {key:0, value:'Simple Mail'},
    {key:1, value:'SMS'},
    {key:2, value:'ReportMail'}
  ];
  public Reportviewreportsettings: Reporviewtsettingreport
  public Reportviewaddnewsettings;
  dependentPicklistEmployee=[{
    "DependPicklistId": "Report.Id",
    "DependPicklistName": "Report.Id",
    "Operationtpyevalue":5,
    "values": -1899998881,
  },{
    "DependPicklistId": "ApplicableMenuIds",
    "DependPicklistName": "ApplicableMenuIds",
    "Operationtpyevalue":2,
    "values": -1499998992,
  }]
  commemtlistviewcontentvisible :boolean =true;
  ActionTypes = ['Simple Mail','SMS','Report Mail']
  DeliveryTypes = ['Please-Select','Employee','Role','Content','Others','Party','User','Contact','Usergroup','Subcription']
  ContentTypes = ['Please-Select','Direct','Employee','Party','User','Contact','Usergroup','Role']
  ReportTypes = ['Please-Select','HTML','XML','Text','Report','DataFile','Template']
  AttachTypes = ['Please-Select','JAsperReport','PDF','Excel','Doc','Text']



  SchedulerPicklistTemplate=[{
    "DependPicklistId": "EventTypeId",
    "DependPicklistName": "EventTypeId",
    "Operationtpyevalue":5,
    "values": "-1399999739",
  }]

  SchedulerPicklistCriteriaConfig=[{
    "DependPicklistId": "CriteriaServiceId",
    "DependPicklistName": "CriteriaServiceId",
    "Operationtpyevalue":5,
    "values": -1899999981,
  }]

  SchedulerPicklistReportFormat=[
    {
      "DependPicklistId": "Report.Id",
      "DependPicklistName": "Report.Id",
      "Operationtpyevalue":5,
      "values": '',
    },{
      "DependPicklistId": "ApplicableMenuIds",
      "DependPicklistName": "ApplicableMenuIds",
      "Operationtpyevalue":2,
      "values": '',
    }
  ]

  SchedulerPicklistFile = [{
    "DependPicklistId": "FileType",
    "DependPicklistName": "FileType",
    "Operationtpyevalue":5,
    "values": 2,
  }]




  ViewtypePickLists=[{key:0 , value:"GRID"},{key:1 , value:"Template"},{key:2 , value:"Pivot"},{key:3 , value:"HTML"},{key:4 , value:"Page"}]
  CopyFromName: any;
  CopyFromDisplay: boolean;
  constructor(public fb:FormBuilder, private https: HttpClient,public formaction: formactionservice,public http: GBHttpService,public store: Store, public dialog: MatDialog, private router: Router, public service: MenuserviceService) {
    this.loginDTOs$.subscribe(dto => {
      console.log("LoginDTO:",dto)
      this.LOGINDTO = dto;
    })
  }

  get formComponent(): GBBasePageComponentNG {
    return this._formComponent;
  }

  get formgrp(): GBBasePageComponentNG {
    return this.FormGroup;
  }
  @Input()
  set formComponent(val: GBBasePageComponentNG) {
    this._formComponent = val;
  }
  public cc = [];
  public rr = [];

  ngOnInit(): void {
    this.getdata();
    this.store.dispatch(new FormUnEditable);
    this.edit$.subscribe(res => {
      this.disableButton = !res;
    })
   this.totalreportpages$.subscribe(data => {
        
    this.TotalRecord = data
    if (this.maxresult > this.TotalRecord && this.TotalRecord != 0) {
      this.maxresult = this.TotalRecord
    }
  })  
    this.rolesandrights$.subscribe(roles=>{
      this.RolesAndRights = roles
    })

    this.InnerExceptionError$.subscribe(error=>{
      this.InnerExceptionError = error
    })

    this.sourcerowcriteria$.subscribe(data => {
      if (data) {
        console.log("Data:",data)
        let criteriaurl = data.ReportUri
        for(let i=0; i<criteriaurl.split('?')[1].split('&').length; i++){
          if(criteriaurl.split('?')[1].split('&')[i].includes('FirstNumber')){
            this.firstnumber = parseInt(criteriaurl.split('?')[1].split('&')[i].split('=')[1])
          }
          if(criteriaurl.split('?')[1].split('&')[i].includes('MaxResult')){
            this.maxresult = parseInt(criteriaurl.split('?')[1].split('&')[i].split('=')[1])
          }
        }
        this.MaxResult = "&MaxResult="+ this.maxresult;
        this.maxnum = this.maxresult
        if (this.maxresult > this.TotalRecord && this.TotalRecord != 0) {
          this.maxresult = this.TotalRecord
          console.log("Maximum Result:",this.maxresult)
        }
      }
    })
    this.listofview$.subscribe(res => {
      if (res) {
        this.viewtypes = [];
        for(let data of res){
          this.viewtypes.push(data)
        }
        this.viewtypes.unshift({ReportViewId:'', ReportViewType:'STD-HTML-VIEW' ,SecondTemplateLocation:'Standard Html View' ,ReportViewName:'Standard Html View',TemplateLocation:'STDHTMLVIEW'})
        console.log("View Types:", this.viewtypes)
      }
    })
    this.visiblebuttons$.subscribe(res => {
      if (res) {
        this.viewmode = res;
      }
    })

    this.rowcerteria$.subscribe(res => {
      if (res) {
        this.menudetails = res;
        this.SchedulerPicklistReportFormat[0].values = res[0].MenuReportId
        this.SchedulerPicklistReportFormat[1].values = res[0].MenuId
        this.exceldata = res[0].ReportVsViewsArray;
      }
    })
  }


  prepreviousset(){
    // this.TotalRecord = parseInt(localStorage.getItem('TotalRecord'))
    if(this.firstnumber > 1){
      this.firstnumber = 1;
      this.maxresult = this.maxnum;
      this.sourcerowcriteria$.subscribe(data => {
        let criteriaurl = data.ReportUri
          let ReportURI = criteriaurl.split('?');
          let urlparameter = ReportURI[1].split('&')
          let FirstNumber = "FirstNumber="+ this.firstnumber;
          criteriaurl = ReportURI[0] + '?'
          for(let i=0;i < urlparameter.length ;i++){
            if(urlparameter[i].includes('FirstNumber')){
              criteriaurl = criteriaurl + FirstNumber + '&'
            }
            else if(urlparameter[i].includes('MaxResult')){
              criteriaurl = criteriaurl + this.MaxResult +'&'
            }
            else{
              criteriaurl = criteriaurl + urlparameter[i] + '&';
            }
          }
          criteriaurl = criteriaurl.slice(0, -1);
          let criteria = { ...data, ReportUri: criteriaurl };
          let url = '/cos/Common.svc/Report';
          this.formaction.formactionservice(url,criteria).subscribe( res => {
            let data = res.ReportDetail;
            if(data){
              this.store.dispatch(new RowDataPassing(data));
            }
          })
      })
    }
  }

  previousset(){
    // this.TotalRecord = parseInt(localStorage.getItem('TotalRecord'))
    if(this.firstnumber > 1){
      if(this.firstnumber > this.maxnum){
        this.firstnumber -= this.maxnum;
        this.maxresult -= this.maxnum;
      }
      else {
        this.firstnumber = 1
      }
      if(this.maxresult % this.maxnum != 0){
        this.maxresult = Math.ceil(this.maxresult / this.maxnum) * this.maxnum;
      }
     
      this.sourcerowcriteria$.subscribe(data => {
        let criteriaurl = data.ReportUri
          let ReportURI = criteriaurl.split('?');
          let urlparameter = ReportURI[1].split('&')
          let FirstNumber = "FirstNumber="+ this.firstnumber;
          criteriaurl = ReportURI[0] + '?'
          for(let i=0;i < urlparameter.length ;i++){
            if(urlparameter[i].includes('FirstNumber')){
              criteriaurl = criteriaurl + FirstNumber + '&'
            }
            else if(urlparameter[i].includes('MaxResult')){
              criteriaurl = criteriaurl + this.MaxResult +'&'
            }
            else{
              criteriaurl = criteriaurl + urlparameter[i] + '&';
            }
          }
          let criteria = { ...data, ReportUri: criteriaurl };
          let url = '/cos/Common.svc/Report';
          this.formaction.formactionservice(url,criteria).subscribe( res => {
            let data = res.ReportDetail;
            if(data){
              this.store.dispatch(new RowDataPassing(data));
            }
          })
      })
    }
  }

  nextset(){
    // this.TotalRecord = parseInt(localStorage.getItem('TotalRecord'))
    if(this.maxresult < this.TotalRecord || this.TotalRecord == 0){
      if(this.maxresult < this.maxnum){
        this.firstnumber += this.maxresult;
      }
      else{
        this.firstnumber += this.maxnum;
      }
      this.maxresult += this.maxnum;
      if(this.maxresult > this.TotalRecord && this.TotalRecord != 0){
        this.maxresult = this.TotalRecord
      }
      this.sourcerowcriteria$.subscribe(data => {
        let criteriaurl = data.ReportUri
          let ReportURI = criteriaurl.split('?');
          let urlparameter = ReportURI[1].split('&')
          let FirstNumber = "FirstNumber="+ this.firstnumber;
          criteriaurl = ReportURI[0] + '?'
          for(let i=0;i < urlparameter.length ;i++){
            if(urlparameter[i].includes('FirstNumber')){
              criteriaurl = criteriaurl + FirstNumber + '&'
            }
            else if(urlparameter[i].includes('MaxResult')){
              criteriaurl = criteriaurl + this.MaxResult +'&'
            }
            else{
              criteriaurl = criteriaurl + urlparameter[i] + '&';
            }
          }
          criteriaurl = criteriaurl.slice(0, -1);
          let criteria = { ...data, ReportUri: criteriaurl };
          let url = '/cos/Common.svc/Report';
          this.formaction.formactionservice(url,criteria).subscribe( res => {
            let data = res.ReportDetail;
            if(data){
              this.store.dispatch(new RowDataPassing(data));
            }
          })
      })
    }
  }

  nextnextset(){
    // this.TotalRecord = parseInt(localStorage.getItem('TotalRecord'))
    if(this.maxresult < this.TotalRecord){
      this.maxresult = this.TotalRecord
      this.firstnumber = Math.floor(this.maxresult / this.maxnum) * this.maxnum + 1;
      this.sourcerowcriteria$.subscribe(data => {
        let criteriaurl = data.ReportUri
          let ReportURI = criteriaurl.split('?');
          let urlparameter = ReportURI[1].split('&')
          let FirstNumber = "FirstNumber="+ this.firstnumber;
          criteriaurl = ReportURI[0] + '?'
          for(let i=0;i < urlparameter.length ;i++){
            if(urlparameter[i].includes('FirstNumber')){
              criteriaurl = criteriaurl + FirstNumber + '&'
            }
            else if(urlparameter[i].includes('MaxResult')){
              criteriaurl = criteriaurl + this.MaxResult +'&'
            }
            else{
              criteriaurl = criteriaurl + urlparameter[i] + '&';
            }
          }
          let criteria = { ...data, ReportUri: criteriaurl };
          let url = '/cos/Common.svc/Report';
          this.formaction.formactionservice(url,criteria).subscribe( res => {
            let data = res.ReportDetail;
            if(data){
              this.store.dispatch(new RowDataPassing(data));
            }
          })
      })
    }
  }
 
  public selectedview(view) {
    this.store.dispatch(new SelectedViewtype(view))
  }

  public selectedformview(formview){
    let selectedformview = formview.View
    this.store.dispatch(new SelectedformViewtype(selectedformview))
  }

  public addRecord() {
    const add = this.formComponent.form as any;
    add.clear();
  }

  public path() {
    this.path$.subscribe(res => {
      this.store.dispatch(new FormEditable);
      if (res) {
        let testpath = res
        this.router.navigate([testpath]);
      }
    })
    // this.store.dispatch(new ButtonVisibility("Forms"))
  }

  public Excel(data) {
    let type = 2
    this.service.excelservice(data, this.menudetails, type).subscribe(res => {
      if (res) {
        let name = this.menudetails[0].ReportName;
        this.convertBase64ToExcel(res, name);
      }
    });
  }
  convertBase64ToExcel = function (bssix, fname) {
    var data = bssix;

    var contentType = 'application/vnd.ms-excel';
    var blob1 = this.b64toBlobCommon(data, contentType);
    var blobUrl1 = URL.createObjectURL(blob1);

    //window.open(blobUrl1);
    var downloadLink = document.createElement('a');
    downloadLink.href = blobUrl1;
    downloadLink.download = fname + '.xlsx';
    document.body.appendChild(downloadLink);
    downloadLink.click();
    document.body.removeChild(downloadLink);
  };

  b64toBlobCommon = function (b64Data, contentType, sliceSize) {
    contentType = contentType || '';
    sliceSize = sliceSize || 512;

    var byteCharacters = atob(b64Data);
    var byteArrays = [];

    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      var slice = byteCharacters.slice(offset, offset + sliceSize);

      var byteNumbers = new Array(slice.length);
      for (var i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      var byteArray = new Uint8Array(byteNumbers);

      byteArrays.push(byteArray);
    }

    var blob = new Blob(byteArrays, { type: contentType });
    return blob;
  };



  public CSV(data) {
    let type = 0
    this.service.excelservice(data, this.menudetails, type).subscribe(res => {
      if (res) {
        let name = 'Report';
        this.convertBase64ToCSV(res, name);
      }
    });
  }
  convertBase64ToCSV = function (bssix, fname) {
    var data = bssix;

    var uri = 'data:text/csv;charset=utf-8,' + encodeURIComponent(data);
    var downloadLink = document.createElement("a");
    downloadLink.href = uri;
    downloadLink.download = fname + ".csv";
    document.body.appendChild(downloadLink);
    downloadLink.click();
    document.body.removeChild(downloadLink);
  };


  LoginDTO;
  ReportviewID;
  public refresh(){
    this.sourcerowcriteria$.subscribe(data => {
      let url = '/cos/Common.svc/Report';
      this.formaction.formactionservice(url,data).subscribe(res =>{
        console.log("Data:",data)

      })
  })
  }

  public pdf() {
    let filterdata;
    this.sourcerowcriteria$.subscribe(data => {
      let finaldata = JSON.stringify(data.CriteriaDTO);
      filterdata = finaldata
    })
    let configurationjasperdetail = localStorage.getItem("JasperDetailUrl");
    this.LOGINDTO$.subscribe(dto => {
      let Logindto = dto;
      this.LoginDTO = JSON.stringify(Logindto);
    })
    this.selectedview$.subscribe(data => {
      this.ReportviewID = data.Id
    })
    let DefaultReportFormatId;
    this.reportformatselection$.subscribe(data => {
      DefaultReportFormatId = data;
    })
    this.rowcerteria$.subscribe(res => {
      if (res) {
        let btoafilter = btoa(filterdata);
        let Reportname = res[0].MenuName
        let ReportFilename = res[0].ReportFile
        let weburl = res[0].WebServiceUriTemplate
        let re = /FirstNumber=1/gi;
        var newstr = weburl.replace(re, 'FirstNumber=-1');
        let x = /MaxResult=10/gi
        var Finalweburl = newstr.replace(x, 'MaxResult=-1');
        let symbol = /&/gi
        var Finalweburls = Finalweburl.replace(symbol, '@');
        let WebServiceUriTemplate = Finalweburls
        let MenuReportId = res[0].MenuReportId
       let Base64LoginDTO = btoa(this.LoginDTO) // string to Base64 Converter (btoa) || (atob) to convert Base64 to string
        let pdfdata = configurationjasperdetail + "/jasperserver/flow.html?_flowId=viewReportFlow&j_username=jasperadmin&j_password=jasperadmin&param=1&standAlone=true&_flowId=viewReportFlow&ParentFolderUri=Freports&reportUnit=" + ReportFilename + "&output=pdf&Criteria=" + btoafilter + "&JasperReportTitle=" + Reportname + "&Method=POST&URL=" + WebServiceUriTemplate + "&Login=" + Base64LoginDTO + "&ReportFormatId=" + DefaultReportFormatId + "&ReportId=" + MenuReportId + "&ReportViewId=" + this.ReportviewID + "&IsDynamicJasperRequired=1";
        window.open(pdfdata, "_blank");
      }
      else {
        const dialogRef = this.dialog.open(DialogBoxComponent, {
          data: {
            message: ['Please Select Any Report'],
            heading: 'Error',
            // button:'Ok'
            // Add any other properties you want to pass
          }
        });
      }
    })
    // const add = this.formComponent as any;
    // var doc = new jsPDF('p', 'pt');

    // add.columnData.forEach(element => {
    //   var temp = element.headerName;
    //   this.cc.push(temp);
    // });

    // add.rowData.forEach(element => {
    //   var temp = [element.Code, element.Name];
    //   this.rr.push(temp);
    // });
  }

  public getdata(){
    let data =  this.formComponent as any
    if(data!=undefined && data.form!=undefined && data.form.value!=undefined){
      this.store.dispatch(new FormControlValue(data.form.value))
      if(data.form.ds.dbDataService.CopyFrom!=undefined){
        this.CopyFromDisplay = true
      } else {
        this.CopyFromDisplay = false
      }
      if(data.form.ds.IdField.includes('Id') && (data.form.ds.selectedid == 0 || data.form.ds.selectedid == -1 || data.form.ds.selectedid == undefined)){
        this.Save = true;
        this.editButtonOpacity = 1;
        this.editDisableButton = false;
      } else {
        this.Save = false;
        // this.editButtonOpacity = 0.2;
        // this.editDisableButton = true;
      }
    }
    setTimeout(() => {
      this.getdata()
    }, 500)
  }

  copyFrom(){
    const data = this.formComponent as any;
    const jsonFile = `assets/FormJSONS/`+data.title+`.json`;
    this.https.get(jsonFile).subscribe(res=>{
      this.CopyFromJSON = res
      console.log("Copy From JSON Res:",this.CopyFromJSON)
      this.CopyFromName = data.form.ds.dbDataService.CopyFrom[0]
      this.CopyFrom = true;
      document.getElementById("copyfrommodal").style.display = "block";
    })    
  }

  close(){
    this.CopyFrom = false
    document.getElementById("copyfrommodal").style.display = "none";
  }

  CopyFromPatchValue(CopyFromId){
    this.CopyFromId = CopyFromId;
  }

  public copyfromsave(){
    const data = this.formComponent as any;
    let url = data.form.ds.dbDataService.endPoint + '?' + data.form.ds.IdField + '=' + this.CopyFromId;
    this.http.httpget(url).subscribe(res=>{
      data.form.patchValue(res);
      data.form.get(data.form.ds.IdField).patchValue(0)
      for(let copyfromdata of data.form.ds.dbDataService.CopyFrom){
        data.form.get(copyfromdata).patchValue("")
      }
    })
    document.getElementById("copyfrommodal").style.display = "none";
  }
  public saveRecord() {
    const data = this.formComponent as any;
    const jsonFile = `assets/FormJSONS/`+data.title+`.json`;
    let jsondata;
    let flag = true;
    let alertdata = [];
    let IsArrayNameRequired:boolean = false;
    let RemoveArrayName: string = ""
    if(data.form.ds.dbDataService.PostingParameterFieldName == undefined){
      this.https.get(jsonFile).subscribe(res=>{
        jsondata = res['ObjectFields']
        for(let jsonvalue of jsondata){
          if(jsonvalue.IsFieldRequired==false){
            IsArrayNameRequired = true;
            RemoveArrayName = jsonvalue.Name;
          }
          if(jsonvalue.Required){
            if(typeof data.form.value[jsonvalue.Name] == 'object'){
              if((data.form.value[jsonvalue.Name] == undefined || data.form.value[jsonvalue.Name] == null || data.form.value[jsonvalue.Name].length == 0) && jsonvalue.Grid != undefined){
                flag = false
                alertdata.push("Please give the Entry in Grid")
              }
              else if((data.form.value[jsonvalue.Name] == null || data.form.value[jsonvalue.Name].lookupdata == undefined) && jsonvalue.Grid != undefined){
                for(let i=0; i < data.form.value[jsonvalue.Name].length; i++){
                  let griddata = data.form.value[jsonvalue.Name][i]
                  for(let grid of jsonvalue.Grid){
                    if(grid.Required!=undefined && grid.Required == true){
                      if(typeof grid.value == 'object'){
                        if(griddata[grid.value[griddata[grid.DependendField]]] === "" || griddata[grid.value[griddata[grid.DependendField]]] == null ||griddata[grid.value[griddata[grid.DependendField]]] == undefined){
                          flag = false
                          alertdata.push(grid.Label+ " - "+ (i+1) +" Should not be Empty")
                        }
                      } else if(griddata[grid.value] === "" || griddata[grid.value] == null || griddata[grid.value] == undefined){
                        flag = false
                        alertdata.push(grid.Label+ " - "+ (i+1) +" Should not be Empty")
                      } else if(grid.Type.toLowerCase() == 'mail'){
                        let emailPattern  = /^[\w-]+(?:\.[\w-]+)*@[a-zA-Z\d-]+\.[a-zA-Z]{2,}$/
                        if (!(emailPattern.test(griddata[grid.value]))) {
                          alertdata.push("Please Provide Valid Email - "+ (i+1) + " - " + grid.Label)
                          flag = false
                        }
                      }
                    } else if(grid.Type.toLowerCase() == 'mail' && griddata[grid.value]!=''){
                      let emailPattern  = /^[\w-]+(?:\.[\w-]+)*@[a-zA-Z\d-]+\.[a-zA-Z]{2,}$/
                      if (!(emailPattern.test(griddata[grid.value]))) {
                        alertdata.push("Please Provide Valid Email - "+ (i+1) + " - " + grid.Label)
                        flag = false
                      }
                    }
                    else if(grid.Type.toLowerCase() == 'percentage' || grid.Type.toLowerCase() == 'number'){
                      if (griddata[grid.value] === '.' || griddata[grid.value] === '.0' || griddata[grid.value] === '.00') {
                        alertdata.push("Please Provide Valid Percent - "+ (i+1) + " - " + grid.Label)
                        flag = false
                      }
                    }
                  }
                }
              }
              else if(data.form.value[jsonvalue.Name] == null || data.form.value[jsonvalue.Name].lookupdata === "" || data.form.value[jsonvalue.Name].lookupdata == null || data.form.value[jsonvalue.Name].lookupdata == undefined){
                  flag = false
                  alertdata.push(jsonvalue.Label+" Should not be Empty")
                } 
              } else {
                if(data.form.value[jsonvalue.Name] == null || data.form.value[jsonvalue.Name] == undefined || data.form.value[jsonvalue.Name] === ""){
                  if(jsonvalue.Grid != undefined){
                    alertdata.push("Please give the Entry in Grid")
                  } else {
                    alertdata.push(jsonvalue.Label+" Should not be Empty")
                  }
                  flag = false
                }
            }
          }
          if(jsonvalue.Type != undefined && (jsonvalue.Type.toLowerCase() == 'percentage' || jsonvalue.Type.toLowerCase() == 'number')){
            if(data.form.value[jsonvalue.Name] === '.' || data.form.value[jsonvalue.Name] === '.0' ||data.form.value[jsonvalue.Name] === '.00'){
              flag = false
              alertdata.push("Please Provide Valid Percentge - "+jsonvalue.Label)
            }
          }
        }
        if(flag){
          if(((data.form.ds.selectedid == 0 || data.form.ds.selectedid == undefined) && this.RolesAndRights.Insert == '0') || ((data.form.ds.selectedid != 0 && data.form.ds.selectedid != undefined) && this.RolesAndRights.Update == '0')){
            let url: string
            if(data.form.ds.dbDataService.saveurl != undefined){
              url = data.form.ds.dbDataService.saveurl
            } else {
              url = data.form.ds.dbDataService.endPoint
            }
            let criteria={};
            for(let jsonvalue of jsondata){
              if((jsonvalue.PostData == undefined || jsonvalue.PostData)  && jsonvalue.Name){
                if(jsonvalue.PostDefaultValue != undefined || jsonvalue.PostDefaultValue){
                  if(data.form.value[jsonvalue.Name].lookupdata === ""){
                    criteria[jsonvalue.Name] = jsonvalue.PostDefaultValue
                  }
                  else if(data.form.value[jsonvalue.Name] === ""){
                    criteria[jsonvalue.Name] = jsonvalue.PostDefaultValue
                  }
                }
                // else if(jsonvalue.Name == 'PeriodId' && data.form.value[jsonvalue.Name] == -1){
                //   criteria[jsonvalue.Name] = this.LOGINDTO.WorkPeriodId
                // } else if(jsonvalue.Name == 'OrganizationUnitId' && data.form.value[jsonvalue.Name] == -1){
                //   criteria[jsonvalue.Name] = this.LOGINDTO.WorkOUId
                // } else if(jsonvalue.Name == 'OrganizationUnitName' && data.form.value[jsonvalue.Name] == -1){
                //   criteria[jsonvalue.Name] = this.LOGINDTO.OuName
                // }
                else {
                  criteria[jsonvalue.Name]=data.form.value[jsonvalue.Name]
                } 
              }
            }
           criteria = JSON.parse(JSON.stringify(criteria))
            for(let item in criteria){
              if(criteria[item] != null && typeof criteria[item] == 'object'){
                if(criteria[item].lookupdata != undefined || criteria[item].lookupdata === null){
                  if(typeof criteria[item].lookupdata == 'string' || typeof criteria[item].lookupdata == 'number'){
                    criteria[item] = criteria[item].lookupdata
                  } else if(criteria[item].length>0){
                  } else{
                    criteria[item] = ""
                  }
                } else {
                  for(let Arraydata in criteria[item]){
                    if(criteria[item][Arraydata] != null && typeof criteria[item][Arraydata] == 'object'){
                      if(criteria[item][Arraydata].lookupdata != undefined){
                        if(typeof criteria[item][Arraydata].lookupdata == 'string'){
                          criteria[item][Arraydata] = criteria[item][Arraydata].lookupdata
                        }else{
                          criteria[item][Arraydata] = ""
                        }
                      }
                    }
                  }
                }
              }
            }
            if(IsArrayNameRequired){
              criteria = criteria[RemoveArrayName]
            }
            console.log("criteria:", criteria)
            this.service.savedata(url, criteria).subscribe(res => {
              if (res.Body) {
                const dialogRef = this.dialog.open(DialogBoxComponent, {
                  width: '600px',
                  data: {
                    message: res.Body,
                    heading: 'Success',
                  }
                })
                data.form.clear();
                this.store.dispatch(new ResetForm); 
                data.form.ds.selectedid = 0; 
                this.store.dispatch(new FormUnEditable);
              } else if(res.includes("Success")){
                const dialogRef = this.dialog.open(DialogBoxComponent, {
                  width: '600px',
                  data: {
                    message: res,
                    heading: 'Success',
                  }
                })
                data.form.clear();
                this.store.dispatch(new ResetForm); 
                data.form.ds.selectedid = 0;
                this.store.dispatch(new FormUnEditable);
              } else {
                const dialogRef = this.dialog.open(DialogBoxComponent, {
                width: '600px',
                data: {
                  message: res,
                  heading: 'Error',
                  errorinfo: this.InnerExceptionError
                }
              });
            }
            })
          } else {
            let savemsg;
            if((data.form.ds.selectedid == 0 || data.form.ds.selectedid == undefined)){
              savemsg = "User doesn't have rights to Save Form"
            } else {
              savemsg = "User doesn't have rights to Update Form"
            }
            const dialogRef = this.dialog.open(DialogBoxComponent, {
              width: '600px',
              data: {
                message: savemsg,
                heading: 'Error',
              }
            });
          }
        } else {
          const dialogRef = this.dialog.open(DialogBoxComponent, {
            width: '600px',
            data: {
              message: alertdata,
              heading: 'Info',
            }
          });
        }
       
      })
    } else {
     
      if(((data.form.ds.selectedid == 0 || data.form.ds.selectedid == undefined) && this.RolesAndRights.Insert == '0') || ((data.form.ds.selectedid != 0 && data.form.ds.selectedid != undefined) && this.RolesAndRights.Update == '0')){
        let PostingParameterFieldName = data.form.ds.dbDataService.PostingParameterFieldName.split(',')
        let PostingParameterFieldValue = data.form.ds.dbDataService.PostingParameterFieldValue.split(',')
        console.log("PostingParameter:",PostingParameterFieldName)
        let url: string
        if(data.form.ds.dbDataService.saveurl != undefined){
          url = data.form.ds.dbDataService.saveurl
        } else {
          url = data.form.ds.dbDataService.endPoint
        }
        for(let i=0; i< PostingParameterFieldName.length ; i++ ){
          if(i==0){
            url = url + '?' + PostingParameterFieldName[i] + '=' + data.form.value[PostingParameterFieldValue[i]]
          } else {
            url = url + '&' + PostingParameterFieldName[i] + '=' + data.form.value[PostingParameterFieldValue[i]]
          }
        }
        let criteria="";
        this.service.savedata(url, criteria).subscribe(res => {
          console.log("Save Res:",res)
          if (res.Body) {
            const dialogRef = this.dialog.open(DialogBoxComponent, {
              width: '600px',
              data: {
                message: res.Body,
                heading: 'Success',
              }
            })
            data.form.clear();
            this.store.dispatch(new ResetForm); 
            data.form.ds.selectedid = 0;
            this.store.dispatch(new FormUnEditable);
          } else if(res.includes("Success")){
            const dialogRef = this.dialog.open(DialogBoxComponent, {
              width: '600px',
              data: {
                message: res,
                heading: 'Success',
              }
            })
            data.form.clear();
            this.store.dispatch(new ResetForm); 
            data.form.ds.selectedid = 0;
            this.store.dispatch(new FormUnEditable);
          } else {
            const dialogRef = this.dialog.open(DialogBoxComponent, {
            width: '600px',
            data: {
              message: res,
              heading: 'Error',
              errorinfo: this.InnerExceptionError
            }
          });
        }
        })
      } else{
        const dialogRef = this.dialog.open(DialogBoxComponent, {
          width: '600px',
          data: {
            message: "User doesn't have rights to Update Form",
            heading: 'Error',
          }
        });
      }
    }
  }

  public saveDraft(){
    let OUID:string
    const data = this.formComponent as any;
    const jsonFile = `assets/FormJSONS/`+data.title+`.json`;
    let jsondata;
    let flag = true;
    let alertdata = [];
    let IsArrayNameRequired:boolean = false;
    let RemoveArrayName: string = ""
    if(data.form.ds.dbDataService.PostingParameterFieldName == undefined){
      this.https.get(jsonFile).subscribe(res=>{
        jsondata = res['ObjectFields']
        for(let jsonvalue of jsondata){
          if(jsonvalue.IsFieldRequired==false){
            IsArrayNameRequired = true;
            RemoveArrayName = jsonvalue.Name;
          }
          if(jsonvalue.Required){
            if(typeof data.form.value[jsonvalue.Name] == 'object'){
              if((data.form.value[jsonvalue.Name] == undefined || data.form.value[jsonvalue.Name] == null || data.form.value[jsonvalue.Name].length == 0) && jsonvalue.Grid != undefined){
                flag = false
                alertdata.push("Please give the Entry in Grid")
              }
              else if((data.form.value[jsonvalue.Name] == null || data.form.value[jsonvalue.Name].lookupdata == undefined) && jsonvalue.Grid != undefined){
                for(let i=0; i < data.form.value[jsonvalue.Name].length; i++){
                  let griddata = data.form.value[jsonvalue.Name][i]
                  for(let grid of jsonvalue.Grid){
                    if(grid.Required!=undefined && grid.Required == true){
                      if(typeof grid.value == 'object'){
                        if(griddata[grid.value[griddata[grid.DependendField]]] === "" || griddata[grid.value[griddata[grid.DependendField]]] == null ||griddata[grid.value[griddata[grid.DependendField]]] == undefined){
                          flag = false
                          alertdata.push(grid.Label+ " - "+ (i+1) +" Should not be Empty")
                        }
                      } else if(griddata[grid.value] === "" || griddata[grid.value] == null || griddata[grid.value] == undefined){
                        flag = false
                        alertdata.push(grid.Label+ " - "+ (i+1) +" Should not be Empty")
                      } else if(grid.Type.toLowerCase() == 'mail'){
                        let emailPattern  = /^[\w-]+(?:\.[\w-]+)*@[a-zA-Z\d-]+\.[a-zA-Z]{2,}$/
                        if (!(emailPattern.test(griddata[grid.value]))) {
                          alertdata.push("Please Provide Valid Email - "+ (i+1) + " - " + grid.Label)
                          flag = false
                        }
                      }
                      else if(grid.Type.toLowerCase() == 'percentage' || grid.Type.toLowerCase() == 'number'){
                        if (griddata[grid.value] === '.' || griddata[grid.value] === '.0' || griddata[grid.value] === '.00') {
                          alertdata.push("Please Provide Valid Percent - "+ (i+1) + " - " + grid.Label)
                          flag = false
                        }
                      }

                    }
                  }
                }
              }
              else if(data.form.value[jsonvalue.Name] == null || data.form.value[jsonvalue.Name].lookupdata === "" || data.form.value[jsonvalue.Name].lookupdata == null || data.form.value[jsonvalue.Name].lookupdata == undefined){
                flag = false
                alertdata.push(jsonvalue.Label+" Should not be Empty")
                }
              } else {
                if(data.form.value[jsonvalue.Name] == null || data.form.value[jsonvalue.Name] == undefined || data.form.value[jsonvalue.Name] === ""){
                  if(jsonvalue.Grid != undefined){
                    alertdata.push("Please give the Entry in Grid")
                  } else {
                    alertdata.push(jsonvalue.Label+" Should not be Empty")
                  }
                  flag = false
                }
              }
          }
          if(jsonvalue.Type != undefined && (jsonvalue.Type.toLowerCase() == 'percentage' || jsonvalue.Type.toLowerCase() == 'number')){
            if(data.form.value[jsonvalue.Name] === '.' || data.form.value[jsonvalue.Name] === '.0' ||data.form.value[jsonvalue.Name] === '.00'){
              flag = false
              alertdata.push("Please Provide Valid Percentge - "+jsonvalue.Label)
            }
          }
        }
        if(flag){
          if(((data.form.ds.selectedid == 0 || data.form.ds.selectedid == undefined) && this.RolesAndRights.Insert == '0') || ((data.form.ds.selectedid != 0 && data.form.ds.selectedid != undefined) && this.RolesAndRights.Update == '0')){
            let url: string
            if(data.form.ds.dbDataService.saveurl != undefined){
              url = data.form.ds.dbDataService.saveurl
            } else {
              url = data.form.ds.dbDataService.endPoint
            }
            let criteria={};
            for(let jsonvalue of jsondata){
              if((jsonvalue.PostData == undefined || jsonvalue.PostData)  && jsonvalue.Name){
                if(jsonvalue.PostDefaultValue != undefined || jsonvalue.PostDefaultValue){
                  if(data.form.value[jsonvalue.Name].lookupdata === ""){
                    criteria[jsonvalue.Name] = jsonvalue.PostDefaultValue
                  }
                  else if(data.form.value[jsonvalue.Name] === ""){
                    criteria[jsonvalue.Name] = jsonvalue.PostDefaultValue
                  }
                }
                // else if(jsonvalue.Name == 'PeriodId' && data.form.value[jsonvalue.Name] == -1){
                else {
                  criteria[jsonvalue.Name]=data.form.value[jsonvalue.Name]
                }
              }
            }
           criteria = JSON.parse(JSON.stringify(criteria))
            for(let item in criteria){
              if(criteria[item] != null && typeof criteria[item] == 'object'){
                if(criteria[item].lookupdata != undefined || criteria[item].lookupdata === null){
                  if(typeof criteria[item].lookupdata == 'string' || typeof criteria[item].lookupdata == 'number'){
                    criteria[item] = criteria[item].lookupdata
                  } else if(criteria[item].length>0){
                  } else{
                    criteria[item] = ""
                  }
                } else {
                  for(let Arraydata in criteria[item]){
                    if(criteria[item][Arraydata] != null && typeof criteria[item][Arraydata] == 'object'){
                      if(criteria[item][Arraydata].lookupdata != undefined){
                        if(typeof criteria[item][Arraydata].lookupdata == 'string'){
                          criteria[item][Arraydata] = criteria[item][Arraydata].lookupdata
                        }else{
                          criteria[item][Arraydata] = ""
                        }
                      }
                    }
                  }
                }
              }
            }
            if(IsArrayNameRequired){
              criteria = criteria[RemoveArrayName]
            }
            console.log("criteria:", criteria)
            this.loginDTOs$.subscribe(res =>{
              OUID = res.WorkOUId
            })
            let jsonString = JSON.stringify(criteria);
            this.rolesandrights$.subscribe(res =>{
              let criteria = {
                "DraftId": 0,
                "EntityId": res.EntityId,
                "MenuId": res.MenuId,
                "OuId": OUID,
                "BizTransactionTypeId": res.BizTransactionTypeId,
                "DraftDraftCode": "WER",
                "DraftName": "WER",
                "DraftParticulars": "WER",
                "DraftJsonObject": jsonString,
                "DraftVersion": 1
              }
              this.service.savedraft(criteria).subscribe(apires =>{
                console.log("apires",apires)
              })
            })
          }
        }
      })
    }
  }

  public editRecord(){
    this.edit$.pipe(take(1)).subscribe(newValue => {
      if (!newValue) {
        this.disableButton = false;
        // this.editDisableButton = false;
        this.buttonOpacity = 1;
        this.store.dispatch(new FormEditable);
      } else {
        this.disableButton = true;
        // this.editDisableButton = true;
        this.buttonOpacity = 0.2;
        this.store.dispatch(new FormUnEditable);
      }
    });
    // let id: number = JSON.parse(data.Listrowselectedindex);
    // this.movenextpreid = id
  }
  public clearRecord() {
    if(this.RolesAndRights.Insert == '0'){
      const dialogRef = this.dialog.open(DialogBoxComponent, {
        width: '600px',
        data: {
          message: "Do you want to clear the Form?",
          heading: 'Info',
          button1: 'Yes',
          button2: 'No'
        }
      })
      dialogRef.afterClosed().subscribe(result => {
        if(result == 'Yes'){
          const data = this.formComponent as any;
          data.form.reset()
          this.store.dispatch(new ResetForm);
          data.form.ds.selectedid = 0;
        }
      });
    } else {
      const dialogRef = this.dialog.open(DialogBoxComponent, {
        width: '600px',
        data: {
          message: "User doesn't have rights to Add New Form",
          heading: 'Error',
        }
      });
    }
  }

  public savemodel(savedata,title,url){
    const jsonFile = `assets/FormJSONS/`+title+`.json`;
    let alertdata = [];
    let jsondata;
    let flag = true;
    this.https.get(jsonFile).subscribe(res=>{
      jsondata = res['ObjectFields']
      for(let jsonvalue of jsondata){
        
        if(jsonvalue.Required){
          if(savedata[jsonvalue.Name] == null){
            flag = false
            console.log("jsonvalue",jsonvalue);
            console.log("savedata[jsonvalue.Name]",savedata[jsonvalue.Name]);
            alertdata.push(jsonvalue.Label+" Should not be Empty")
          }
        }
      }
      console.log("Flaaa",flag)
        if(flag){
          let criteria={};
            for(let jsonvalue of jsondata){
              if((jsonvalue.PostData == undefined || jsonvalue.PostData)  && jsonvalue.Name){
                criteria[jsonvalue.Name]=savedata[jsonvalue.Name]
              }
            }

            for(let item in criteria){
              if(criteria[item] != null && typeof criteria[item] == 'object'){
                if(criteria[item].lookupdata != undefined){
                  if(typeof criteria[item].lookupdata == 'string' || typeof criteria[item].lookupdata == 'number'){
                    criteria[item] = criteria[item].lookupdata
                  }else{
                      criteria[item] = ""
                  }
                } else {
                  for(let Arraydata in criteria[item]){
                    if(criteria[item][Arraydata] != null && typeof criteria[item][Arraydata] == 'object'){
                      if(criteria[item][Arraydata].lookupdata != undefined){
                        if(typeof criteria[item][Arraydata].lookupdata == 'string'){
                          criteria[item][Arraydata] = criteria[item][Arraydata].lookupdata
                        }else{
                            criteria[item][Arraydata] = ""
                        }
                      }
                    }
                  }
                }
              }
            }
          console.log("Criteria",criteria)
          this.service.savedata(url, criteria).subscribe(res => {
            console.log("Save",res)
            if (res.Body) {
              const dialogRef = this.dialog.open(DialogBoxComponent, {
                width: '600px',
                data: {
                  message: res.Body,
                  heading: 'Success',
                }
              })
            }  
            else {
              const dialogRef = this.dialog.open(DialogBoxComponent, {
              width: '600px',
              data: {
                message: res,
                heading: 'Error',
                errorinfo: this.InnerExceptionError
              }
            });
          }
          })
        } else{
          const dialogRef = this.dialog.open(DialogBoxComponent, {
            width: '600px',
            data: {
              message: alertdata,
              heading: 'Info',
            }
          });
        }
      
    })
  }

  responsehandler(encodeddata: any) {
    const getresponse = JSON.stringify(encodeddata);
    const responseModel = JSON.parse(getresponse);
    let apiBody = responseModel.Body;
    const bytes = utf8.decode(apiBody);
    const decodedData = globalbase64js.decode(bytes);
    const Responsebodydecoded = JSON.parse(globalpako.inflate(decodedData, { to: 'string' }));
    const con = Responsebodydecoded.Body;
    this.store.dispatch(new InnerExceptionError(Responsebodydecoded.InnerException))
    let converter = Responsebodydecoded;
    try {
      converter = JSON.parse(con)
    } catch (error) {
      converter = con
    }
    return converter;
  }

  public deleteRecord() {
    const data = this.formComponent as any;
    const jsonFile = `assets/FormJSONS/`+data.title+`.json`;
    let jsondata;
    let flag = true;
    let alertdata = [];
    this.https.get(jsonFile).subscribe(res=>{
      jsondata = res['ObjectFields']
      let idField:string;
      if(data.form.ds.IdField != undefined){
        idField = data.form.ds.IdField
        if(data.form.ds.selectedid == 0 || data.form.ds.selectedid == undefined){
          flag = false
        }
      }
      // for(let jsonvalue of jsondata){
      //   if(jsonvalue.Required){
      //     if(typeof data.form.value[jsonvalue.Name] == 'object'){
      //       if(data.form.value[jsonvalue.Name].lookupdata === "" || data.form.value[jsonvalue.Name].lookupdata == null || data.form.value[jsonvalue.Name].lookupdata == undefined){
      //         flag = false
      //         alertdata.push(jsonvalue.Label+" Should not be Empty")
      //       }
      //     } else {
      //       if(data.form.value[jsonvalue.Name] === "" || data.form.value[jsonvalue.Name] == null || data.form.value[jsonvalue.Name] == undefined){
      //         flag = false
      //         alertdata.push(jsonvalue.Label+" Should not be Empty")
      //       }
      //     }
      //   }
      // }
      if(flag){
        if(this.RolesAndRights.Delete == '0'){
          const dialogRef = this.dialog.open(DialogBoxComponent, {
            width: '600px',
            data: {
              message: "Do you want to Delete this Data?",
              heading: 'Info',
              button1: 'Yes',
              button2: 'No'
            }
          })
          dialogRef.afterClosed().subscribe(result => {
            if(result == 'Yes'){
              const data = this.formComponent as any;
            let url : string
            let idField: string
            if(data.form.ds.dbDataService.deleteurl != undefined){
              url = data.form.ds.dbDataService.deleteurl
            } else {
              url = data.form.ds.dbDataService.endPoint
            }
            if(data.form.ds.dbDataService.IdField != undefined){
              idField = data.form.ds.dbDataService.IdField
            } else {
              idField = data.form.ds.IdField
            }
            let idfied = '?'+idField+'='
            let selectedid = data.form.ds.selectedid
            this.service.deletedata(url, idfied, selectedid).subscribe(res => {
              let delresponse;
              if(isDevMode()){
                delresponse = this.responsehandler(res)
              } else {
                delresponse = this.responsehandler(res.contents)
              }
              if (delresponse.Body) {
              this.dialog.open(DialogBoxComponent, {
                  width: '600px',
                  data: {
                    message: delresponse.Body,
                    heading: 'Success',
                  }
                });
                data.form.clear();
                this.store.dispatch(new ResetForm); 
                data.form.ds.selectedid = 0;
                this.store.dispatch(new FormUnEditable);
              }  else if(delresponse.includes("Success")){
                const dialogRef = this.dialog.open(DialogBoxComponent, {
                  width: '600px',
                  data: {
                    message: delresponse,
                    heading: 'Success',
                  }
                })
                data.form.clear();
                this.store.dispatch(new ResetForm); 
                data.form.ds.selectedid = 0;
                this.store.dispatch(new FormUnEditable);
              } else {
              this.dialog.open(DialogBoxComponent, {
                  width: '600px',
                  data: {
                    message: delresponse,
                    heading: 'Error',
                    errorinfo: this.InnerExceptionError
                  }
                });
              }
            })
            }
          });
         
        } else {
          const dialogRef = this.dialog.open(DialogBoxComponent, {
            width: '600px',
            data: {
              message: "User doesn't have rights to Delete Form",
              heading: 'Error',
            }
          });
        }
      } else {
        const dialogRef = this.dialog.open(DialogBoxComponent, {
          width: '600px',
          data: {
            message: "Please Select Record to be Deleted",
            heading: 'Info',
          }
        });
      }
    })
  }

  public movePrev() {
    const prev = this.formComponent.form as any;
    this.movenextpreviousid$.subscribe(res => {
      if (res) {
        console.log("resid", res)
        this.movenextpreid = res - 1
      }
    })
    this.store.dispatch(new Movenextpreviousid(this.movenextpreid));
    console.log("this.movenextpreid", this.movenextpreid)
    prev.movePrev();
  }
  public moveNext() {
    const next = this.formComponent.form as any;
    this.movenextpreviousid$.subscribe(res => {
      if (res) {
        console.log("resid", res)
        this.movenextpreid = res + 1
      }
    })
    this.store.dispatch(new Movenextpreviousid(this.movenextpreid));
    console.log("this.movenextpreid", this.movenextpreid)
    next.moveNext();
  }
  public moveFirst() {
    const first = this.formComponent.form as any;
    first.moveFirst();
  }

  public moveLast() {
    const Last = this.formComponent.form as any;
    Last.moveLast();
  }

  aboutclick() {
    const dialogRef = this.dialog.open(GbaboutComponent, { position: { right: '0' } });
    dialogRef.afterClosed().subscribe(result => {
    });
    this.router.navigate(['/gbabout']);
  }
  gblogclick() {
    const dialogRef = this.dialog.open(GblogComponent, { position: { right: '0' } });
    dialogRef.afterClosed().subscribe(result => {
    });
    this.router.navigate(['/gblog']);
  }

  public openemaildailogbox(templateRef){
    let dialogRef = this.dialog.open(templateRef, {
      width: '700px',
      height: '300px'
    });
  }


  public emailtosumbit(){
    this.dialog.closeAll()
    console.log("mailsendid",this.emailid.nativeElement.value)
    console.log("pdf",this.pdfinputmail.nativeElement.value)
    console.log("csv",this.csvinputmail.nativeElement.value)

    console.log("menudetails",this.menudetails)
    this.service.mailgenerationservice(this.menudetails,this.pdfinputmail.nativeElement.value,this.emailid.nativeElement.value).subscribe(res =>{
      console.log("LL",res)
    })
  }

  public reportviewsetting() {
    this.store.dispatch(new FormEditable);
    document.getElementById("reportviewsettingreportmodel").style.display = "block";
    const viewTypeMapping = {
      0: 'Grid',
      1: 'Template',
      2: 'Pivot',
      3: 'HTML',
      4: 'Page'
    };

    this.service.reportviewreportsetting(this.menudetails).subscribe(res => {
      console.log("resssss...", res)
      res.forEach(report => {
        report.ReportViewType = viewTypeMapping[report.ReportViewType];
      });
      this.Reportviewreportsettings = res;
    })
  }

  public Addnewreport() {
    this.reportsettingform = this.fb.group({
      ReportViewStatus: ['1'],
      SLNO: ['1'],
      ReportViewId: [0],
      ReportId: [this.menudetails[0].MenuReportId],
      ReportViewName: [],
      ReportViewType: [],
      PageId: [-1],
      TemplateId: [-1],
      ReportViewIsSystemView: [1],
      ReportViewIsDefaultView: [1],
      ApplicableMenuIds: [","+this.menudetails[0].MenuId],
      ReportViewPageFormat: [1],
      ReportViewPageOrientation: [0],
      ReportViewRemarks: [],
      ReportViewColumnFreeze:[],
      ReportViewPivotFormat: [""],
      ReportViewIsDataPivot: [1],
      ReportViewVersion: [1],
      ReportViewSourceType: [5],
      ReportViewFieldsDTOs: new FormArray([])
    })

    this.ReportViewFieldsDTOs = this.reportsettingform.get('ReportViewFieldsDTOs') as FormArray;    
    this.Reportviewsetttingtype = true;
    // document.getElementById("reportviewsettingsform").style.display = "block";
    document.getElementById("reportviewsettingstable").style.display = "none";
    document.getElementById("Addnewreport").style.display = "none";
    this.service.reportviewaddnewformsetting(this.menudetails).subscribe(res => {
      console.log("FORMRESS...", res)
      this.Reportviewaddnewsettings = res

      let modifiedArray = this.Reportviewaddnewsettings.ReportVsFieldsArray.map(obj => {
        return { ...obj, ReportViewFieldsFieldTitle: obj.ReportVsFieldsFieldTitle,ReportViewFieldsCalFormat: obj.ReportVsFieldsCalFormat ,ReportViewFieldsIsDisplay:obj.ReportVsFieldsIsDisplay,ReportViewFieldsIsMergeColumn:obj.ReportVsFieldsIsMergeColumn,ReportViewFieldsIsGroupcolumn:obj.ReportVsFieldsIsGroupColumn,ReportViewFieldsIsSubTotal:obj.ReportVsFieldsIsSubtotalColumn,ReportViewFieldsIsGrandTotal:obj.ReportVsFieldsIsGranttotalColumn,ReportViewFieldsSuppressIfEmpty:obj.ReportVsFieldsObjectFieldType,ReportId:obj.ReportVsFieldsReportId,ReportViewFieldsSlNo:obj.ReportVsFieldsSlNo,ReportViewFieldsDisplaySlNo:obj.ReportVsFieldsDisplaySlNo,ReportViewFieldsFieldWidth:obj.ReportVsFieldsFieldWidth ,ReportViewId:0};
    });

    modifiedArray.forEach(field => {
      field.ReportViewFieldsIsDisplay = field.ReportViewFieldsIsDisplay ? 0 : 1;
      field.ReportViewFieldsIsMergeColumn = field.ReportViewFieldsIsMergeColumn ? 0 : 1;
      field.ReportViewFieldsIsGroupcolumn = field.ReportViewFieldsIsGroupcolumn ? 0 : 1;
      field.ReportViewFieldsIsSubTotal = field.ReportViewFieldsIsSubTotal ? 0 : 1;
      field.ReportViewFieldsIsGrandTotal = field.ReportViewFieldsIsGrandTotal ? 0 : 1;
      field.ReportViewFieldsSuppressIfEmpty = field.ReportViewFieldsSuppressIfEmpty ? 0 : 1;
    });

      for (let data of modifiedArray) {
        this.addItem(data)
      }
      console.log("Add new BTN", this.reportsettingform.value)
    })


   



  }

  createItem(data): FormGroup {
    return this.fb.group({
      ReportViewFieldsDisplaySlNo: [data.ReportViewFieldsDisplaySlNo],
      ReportVsFieldsId: [data.ReportVsFieldsId],
      ReportViewFieldsId: ["0"],
      ReportId: [data.ReportId],
      ReportViewId: [data.ReportViewId],
      ReportVsFieldsSlNo: [data.ReportViewFieldsSlNo],
      ReportVsFieldsFieldName: [data.ReportVsFieldsFieldName],
      ReportViewFieldsFieldTitle: [data.ReportViewFieldsFieldTitle],
      ReportViewFieldsFieldWidth: [data.ReportViewFieldsFieldWidth],
      ReportViewFieldsCalFormat: [data.ReportViewFieldsCalFormat],
      ReportViewFieldsIsDisplay: [data.ReportViewFieldsIsDisplay],
      ReportViewFieldsIsMergeColumn: [data.ReportViewFieldsIsMergeColumn],
      ReportViewFieldsIsGroupcolumn: [data.ReportViewFieldsIsGroupcolumn],
      ReportViewFieldsIsSubTotal: [data.ReportViewFieldsIsSubTotal],
      ReportViewFieldsIsGrandTotal: [data.ReportViewFieldsIsGrandTotal],
      ReportViewFieldsSuppressIfEmpty: [data.ReportViewFieldsSuppressIfEmpty],
      ReportViewFieldsSlNo: [data.ReportViewFieldsSlNo]
    });
  }

  addItem(data): void {
    this.ReportViewFieldsDTOs = this.reportsettingform.get('ReportViewFieldsDTOs') as FormArray;
    this.ReportViewFieldsDTOs.push(this.createItem(data));
  }

  public Getselectedid(SelectedPicklistDatas:any): void {
    console.log("SelectedPicklistDatas:",SelectedPicklistDatas)
    let reportid = SelectedPicklistDatas.SelectedData
    this.service.reportviewformsetting(this.menudetails,reportid).subscribe(([response1, response2]) => {
      this.Reportviewaddnewsettings = response1
      this.Reportviewaddnewsettings.forEach(field => {
        field.ReportViewFieldsIsDisplay = field.ReportViewFieldsIsDisplay ? 0 : 1;
        field.ReportViewFieldsIsMergeColumn = field.ReportViewFieldsIsMergeColumn ? 0 : 1;
        field.ReportViewFieldsIsGroupcolumn = field.ReportViewFieldsIsGroupcolumn ? 0 : 1;
        field.ReportViewFieldsIsSubTotal = field.ReportViewFieldsIsSubTotal ? 0 : 1;
        field.ReportViewFieldsIsGrandTotal = field.ReportViewFieldsIsGrandTotal ? 0 : 1;
        field.ReportViewFieldsSuppressIfEmpty = field.ReportViewFieldsSuppressIfEmpty ? 0 : 1;
      });
      this.reportsettingformpatchvalue(this.Reportviewaddnewsettings[0],response2[0])
      this.ReportViewFieldsDTOs = this.reportsettingform.get('ReportViewFieldsDTOs') as FormArray;
      this.ReportViewFieldsDTOs.clear()
      for (let data of this.Reportviewaddnewsettings) {
        this.addItem(data)
      }

      console.log("Report Setting:", this.reportsettingform.value)
    })
  }

  public reportsettingformpatchvalue(tabledata,fielddata){
    this.reportsettingform.patchValue({
      ReportViewStatus:fielddata.ReportViewStatus,
      SLNO: 1,
      ReportViewId: tabledata.ReportViewId,
      ReportId: this.menudetails[0].MenuReportId,
      ReportViewName: tabledata.ReportViewName,
      ReportViewType: tabledata.ReportViewFieldsDisplayType,
      PageId: fielddata.PageId,
      TemplateId: fielddata.TemplateId,
      ReportViewIsSystemView: fielddata.ReportViewIsSystemView,
      ReportViewIsDefaultView: fielddata.ReportViewIsDefaultView,
      ApplicableMenuIds: this.menudetails[0].MenuId,
      ReportViewPageFormat: fielddata.ReportViewPageFormat,
      ReportViewPageOrientation: fielddata.ReportViewPageOrientation,
      ReportViewRemarks: fielddata.ReportViewRemarks,
      ReportViewColumnFreeze:fielddata.ReportViewColumnFreeze,
      ReportViewPivotFormat: fielddata.ReportViewPivotFormat,
      ReportViewIsDataPivot: fielddata.ReportViewIsDataPivot,
      ReportViewVersion: fielddata.ReportViewVersion,
      ReportViewSourceType: fielddata.ReportViewSourceType,
    });
  }

  public reportsettingclose(){
    this.Reportviewsetttingtype = false;
    document.getElementById("Addnewreport").style.display = "block";
    document.getElementById("reportviewsettingreportmodel").style.display = "none";
    document.getElementById("reportviewsettingstable").style.display = "block";
  }

  public reportsettingsave(){
    console.log("JJJ",this.reportsettingform.value)
   if(this.reportsettingform.get('ReportViewName').value.lookupdata){
    console.log("IF")
    this.reportsettingform.get('ReportViewName').patchValue(this.reportsettingform.get('ReportViewName').value.lookupdata)
   }
   console.log("this.reportsettingform.get('ReportViewName').value:",this.reportsettingform.get('ReportViewName').value)
    this.reportsettingform.get('ReportViewFieldsDTOs').value.forEach(field => {
      field.ReportViewFieldsIsDisplay = field.ReportViewFieldsIsDisplay ? 0 : 1;
      field.ReportViewFieldsIsMergeColumn = field.ReportViewFieldsIsMergeColumn ? 0 : 1;
      field.ReportViewFieldsIsGroupcolumn = field.ReportViewFieldsIsGroupcolumn ? 0 : 1;
      field.ReportViewFieldsIsSubTotal = field.ReportViewFieldsIsSubTotal ? 0 : 1;
      field.ReportViewFieldsIsGrandTotal = field.ReportViewFieldsIsGrandTotal ? 0 : 1;
      field.ReportViewFieldsSuppressIfEmpty = field.ReportViewFieldsSuppressIfEmpty ? 0 : 1;
    });
    this.service.reportviewsettingsave(this.reportsettingform.value).subscribe(res=>{
      console.log("res:",res)
    })
  }

  public Infoform(){
    const infolog = this.formComponent as any;
    console.log("infolog",infolog)
    let status;
    if(infolog.form.value[infolog.form.formid + "Status"] == 1){
      status = "Active"
    }
    else{
      status = "Not FOUND"
    }
    this.Infoformid = infolog.form.value[infolog.form.formid + "Id"];
    this.Inforformname = infolog.form.value[infolog.form.formid + "Name"];
    this.Infolastcreatedon = infolog.form.value[infolog.form.formid + "CreatedOn"];
    this.Infolastmodifiedon = infolog.form.value[infolog.form.formid + "ModifiedOn"];
    this.Infostatus = status;
    this.Infoversion = infolog.form.value[infolog.form.formid + "Version"];
    console.log ("JJ",this.Infoformid,this.Inforformname,this.Infolastcreatedon,this.Infolastmodifiedon,this.Infostatus,this.Infoversion)
    document.getElementById("infoformmodel").style.display = "block";
  }

  public infomodelclose(){
    document.getElementById("infoformmodel").style.display = "none";
  }

  public reportviewscheularmodel(){
    const viewTypeMapping = {
      1: 'Form',
      2: 'Report',
    };
    this.store.dispatch(new FormEditable);
    document.getElementById("reportschedulermodel").style.display = "block";

    this.service.reportschedularreport(this.menudetails).subscribe(res => {
      res.forEach(report => {
        report.Type = viewTypeMapping[report.Type];
        const actionType = parseInt(report.ActionType); // Assuming ActionType is a number
        const correspondingType = this.ReportSchedularActiontypes.find(type => type.key === actionType);
        if (correspondingType) {
          report.ActionType = correspondingType.value;
        }
      });
      this.ReporttableScheduler = res
      console.log("xxx...", this.ReporttableScheduler)
    })
  }


  public Addnewschdeular() {
    this.reportScheduler = this.fb.group({
      MenuId: [this.menudetails[0].MenuId],
      MenuCode: [this.menudetails[0].MenuName],
      MenuName: [this.menudetails[0].MenuName],
      JobName: [""],
      ActionName: [""],
      WebServiceId: [this.menudetails[0].WebServiceId],
      ReportId: [this.menudetails[0].MenuReportId],
      ReportFormatId: [-1],
      CriteriaConfigId: [-1],
      To: [""],
      SchedulerId: [-1],
      SchedulerName:[""],
      JobDefineId: [0],
      MailtemplateId: [0],
      ActionId: [0],
      EventtypeActionId: [0],
      ReplyTo: ["noreply@myunisoft.com"],
      ToDeliveryType: [3],
      ToContentType: [0],
      MailCCDeliveryType: [3],
      MailCCContentType: [0],
      MailCC: ["noreply@myunisoft.com"],
      MailBCCDeliveryType: [3],
      MailBCCContentType: [0],
      MailBCC: ["noreply@myunisoft.com"],
      ExportFormat: [1],
      ReportType: [3],
      GroupByFields: ["NONE"],
      ActionForType: [1],
      Type: [0],
      ActionType: [0],
      EventTypeActionDetailEvalCondition: [""],
      EventTypeId: [-1399999739],
      EventTypeName:[""],
      FileId: [-1],
      FileName: ["NONE"],
      IsReportMenu: [0],
      ReportViewId: [-1],
      WebServiceUriTemplate: [this.menudetails[0].WebServiceUriTemplate]
    })
    document.getElementById("reportschedulerform").style.display = "block";
    document.getElementById("reportschedulertable").style.display = "none";
    document.getElementById("Addnewscheduler").style.display = "none";

    console.log("this.reportScheduler",this.reportScheduler.value)
  }

  public schedulerclose(){
    document.getElementById("reportschedulermodel").style.display = "none";
    document.getElementById("reportschedulerform").style.display = "none";
    document.getElementById("reportschedulertable").style.display = "block";
  }

  public GetselectedSchedularperiod(SelectedSchedularperiod:any){
    console.log("PERIOD",SelectedSchedularperiod)
    if(SelectedSchedularperiod.SelectedData==0){
      this.reportScheduler.get('SchedulerId').patchValue(-1);
    } else {
      this.reportScheduler.get('SchedulerId').patchValue(SelectedSchedularperiod.SelectedData);
    }
  }

  public GetselectedSchedulartemplate(SelectedSchedulartemplate:any){
    console.log("PERIOD",SelectedSchedulartemplate)
    this.reportScheduler.get('MailtemplateId').setValue(SelectedSchedulartemplate.SelectedData);
  }

  public reportschedulerformsave(){
    this.reportScheduler.removeControl('SchedulerName');
    console.log("Savedetails",this.reportScheduler.value)

    const value = this.reportScheduler.get('JobName').value;
    this.reportScheduler.get('ActionName').patchValue(value);

    this.service.reportschedularsave(this.reportScheduler.value).subscribe(res =>{
      console.log("savedresponse",res)
    })
  }

  public schedulercondition(){
    // document.getElementById("Evalconditon").style.display = "block";
    this.showEvalCondition = !this.showEvalCondition;
  }

  public AdvancedSchduler(){
    this.AdvancedSchdulerModels = !this.AdvancedSchdulerModels;
  }

  public AdvancedTo(){
    document.getElementById("AdvancedSchdulerTo").style.display = "block"
    document.getElementById("AdvancedSchdulerMailCC").style.display = "none"
    document.getElementById("AdvancedSchdulerMailBCC").style.display = "none"
    document.getElementById("AdvancedSchdulerReportMail").style.display = "none"
  }

  public AdvancedCC(){
    document.getElementById("AdvancedSchdulerTo").style.display = "none"
    document.getElementById("AdvancedSchdulerMailCC").style.display = "block"
    document.getElementById("AdvancedSchdulerMailBCC").style.display = "none"
    document.getElementById("AdvancedSchdulerReportMail").style.display = "none"
  }

  public AdvancedBCC(){
    document.getElementById("AdvancedSchdulerTo").style.display = "none"
    document.getElementById("AdvancedSchdulerMailCC").style.display = "none"
    document.getElementById("AdvancedSchdulerMailBCC").style.display = "block"
    document.getElementById("AdvancedSchdulerReportMail").style.display = "none"
  }

  public AdvancedReportMail(){
    document.getElementById("AdvancedSchdulerTo").style.display = "none"
    document.getElementById("AdvancedSchdulerMailCC").style.display = "none"
    document.getElementById("AdvancedSchdulerMailBCC").style.display = "none"
    document.getElementById("AdvancedSchdulerReportMail").style.display = "block"
  }

  public CommentModel(){
    const data = this.formComponent as any;
    if(data.form.ds.selectedid){
      document.getElementById("commentmodel").style.display = "block";
      let Selectedid = data.form.ds.selectedid
      this.service.commentlistservice(Selectedid).subscribe(res =>{
        console.log("report",res)
        let data = this.organizeComments(res);
        console.log("organizedComments",data);
        this.CommentListviewData = data
      })
    }else{
      const dialogRef = this.dialog.open(DialogBoxComponent, {
        width: '600px',
        data: {
          message: 'Please Select a Data',
          heading: 'Eror',
      }})
    }
  }

  public CommentDetailview(selectvalue){
    console.log("RRRR"+selectvalue);
    this.commemtlistviewcontentvisible = false
    document.getElementById("commentlistdetailcontent").style.display = "block";
    this.SelectedCommentData = selectvalue
  }

  public organizeComments(comments) {
    let commentsMap = {};
    let rootComments = [];
 
    comments.forEach(comment => {
      commentsMap[comment.CommentId] = comment;
    });
 
    comments.forEach(comment => {
      if (comment.ParentCommentId !== -1) {
        let parent = commentsMap[comment.ParentCommentId];
        if (parent) {
          if (!parent.children) {
            parent.children = [];
          }
          parent.children.push(comment);
        }
      } else {
        rootComments.push(comment);
      }
    });
 
    return rootComments;
  }

  public Newcomment(){
    document.getElementById("commentlist").style.display = "none";
    this.commemtlistviewcontentvisible = false
    document.getElementById("newcomment").style.display = "block";
    this.form.controls['UserId'].setValue(this.CommentListviewData[0].UserId);
    this.form.controls['CommentObjectTypeId'].setValue(this.CommentListviewData[0].CommentObjectTypeId);
    this.form.controls['CommentObjectId'].setValue(this.CommentListviewData[0].CommentObjectId);
    const currentDate = new Date();
    const timestamp = currentDate.getTime();
    let todaydatetime = `/Date(${timestamp})/`;
    this.form.controls['CommentTimeStamp'].setValue(todaydatetime);

    console.log("valueeee",this.form.value)

  }

  public newcommentsave(){
    this.service.commentnewsaveservice(this.form.value).subscribe(res =>{
      console.log("savedetails",res)
      document.getElementById("commentmodel").style.display = "none";
    })
  }

  public commentclose(){
    document.getElementById("commentmodel").style.display = "none";
  }
 
}



// Previous Code

// import { Component, Input, OnInit, isDevMode } from '@angular/core';
// import { GBBasePageComponentNG } from '@goodbooks/uicore';
// import { Observable } from 'rxjs';
// import { Select, Store } from '@ngxs/store';
// import { LayoutState } from './../../../../store/layout.state';
// import { ReportState } from './../../../../../commonreport/datastore/commonreport.state'
// import { FilterState } from './../../../../../gbfilter/store/gbfilter.state'
// import { AuthState } from './../../../../../common/shared/stores/auth.state'
// import { MenuserviceService } from './../../../../services/menu/menuservice.service'
// import { MatDialog } from '@angular/material/dialog';
// import * as utf8 from "utf8";
// import { Router } from '@angular/router';
// import { GblogComponent } from 'features/gblog/components/gblog/gblog.component';
// import { GbaboutComponent } from 'features/gbabout/components/gbabout/gbabout.component';
// import { FormEditOption, FormEditable, FormUnEditable, Movenextpreviousid, ResetForm } from 'features/layout/store/layout.actions';
// import { SelectedViewtype } from 'features/commonreport/datastore/commonreport.action';
// import { take } from 'rxjs/operators';
// import { DialogBoxComponent } from 'features/erpmodules/samplescreens/screens/DialogBox/DialogBox.component';
// const globalbase64js = require("base-64");
// const globalpako = require('pako');
// @Component({
//   selector: 'app-gbformactions',
//   templateUrl: './gbformactions.component.html',
//   styleUrls: ['./gbformactions.component.scss']
// })
// export class GbformactionsComponent implements OnInit {
//   @Select(LayoutState.ThemeClass) themeClass$: Observable<string>;
//   @Select(ReportState.SelectedViewtype) selectedview$: Observable<any>;
//   @Select(LayoutState.Visibilebuttons) visiblebuttons$: Observable<string>;
//   @Select(LayoutState.FormValidationvalues) formvaldiation$: Observable<string>;
//   @Select(ReportState.CriteriaConfigArray) rowcerteria$: Observable<any>;
//   @Select(ReportState.Sourcecriteriadrilldown) sourcerowcriteria$: Observable<any>;
//   @Select(FilterState.Reportformatselection) reportformatselection$: Observable<any>;
//   @Select(FilterState.Filter) filtercriteria$: Observable<any>;
//   @Select(AuthState.AddDTO) LOGINDTO$: Observable<any>;
//   @Select(LayoutState.Path) path$: Observable<any>;
//   @Select(LayoutState.FormEdit) edit$: Observable<boolean>;
//   @Select(ReportState.ListofViewtypes) listofview$: Observable<any>;
//   @Select(LayoutState.Versionservicurl) jsaperdetails$: Observable<any>

//   @Input() public _formComponent: GBBasePageComponentNG;
//   @Input() public FormGroup: GBBasePageComponentNG;
//   public viewmode;
//   public exceldata;
//   public menudetails;
//   public movenextpreid
//   dummy = 1;
//   public disableButton = true;
//   rowData: any;
//   color;
//   public viewtypes = [];
//   public Validation;
//   public FormReset;
//   constructor(public store: Store, public dialog: MatDialog, private router: Router, public service: MenuserviceService) {
//   }

//   get formComponent(): GBBasePageComponentNG {
//     return this._formComponent;
//   }

//   get formgrp(): GBBasePageComponentNG {
//     return this.FormGroup;
//   }
//   @Input()
//   set formComponent(val: GBBasePageComponentNG) {
//     this._formComponent = val;
//   }
//   public cc = [];
//   public rr = [];

//   ngOnInit(): void {
//     this.store.dispatch(new FormUnEditable);
//     this.edit$.subscribe(res => {
//       this.disableButton = !res;
//     })
//     this.listofview$.subscribe(res => {
//       if (res) {
//         this.viewtypes = [];
//         for(let data of res){
//           this.viewtypes.push(data)
//         }
//         this.viewtypes.unshift({ReportViewId:'', ReportViewType:'STD-HTML-VIEW' ,SecondTemplateLocation:'STDHTMLVIEW' ,ReportViewName:'STDHTMLVIEW',TemplateLocation:'STDHTMLVIEW'})
//         console.log("View Types:", this.viewtypes)
//       }
//     })
//     this.visiblebuttons$.subscribe(res => {
//       if (res) {
//         this.viewmode = res;
//       }
//     })

//     this.rowcerteria$.subscribe(res => {
//       if (res) {
//         this.menudetails = res;
//         this.exceldata = res[0].ReportVsViewsArray;
//       }
//     })
//   }

//   public selectedview(view) {
//     this.store.dispatch(new SelectedViewtype(view))
//   }

//   public addRecord() {
//     const add = this.formComponent.form as any;
//     add.clear();
//   }

//   public path() {
//     this.path$.subscribe(res => {
//       if (res) {
//         console.log("PATH", res)
//         let testpath = res
//         this.router.navigate([testpath]);
//       }
//     })
//   }

//   public Excel(data) {
//     this.service.excelservice(data, this.menudetails, '').subscribe(res => {
//       console.log("Base645464", res);
//       if (res) {
//         let name;
//         this.convertBase64ToExcel(res, name);
//       }
//     });
//   }
//   convertBase64ToExcel = function (bssix, fname) {
//     var data = bssix;

//     var contentType = 'application/vnd.ms-excel';
//     var blob1 = this.b64toBlobCommon(data, contentType);
//     var blobUrl1 = URL.createObjectURL(blob1);

//     //window.open(blobUrl1);
//     var downloadLink = document.createElement('a');
//     downloadLink.href = blobUrl1;
//     downloadLink.download = 'Report' + '.xlsx';
//     document.body.appendChild(downloadLink);
//     downloadLink.click();
//     document.body.removeChild(downloadLink);
//   };

//   b64toBlobCommon = function (b64Data, contentType, sliceSize) {
//     contentType = contentType || '';
//     sliceSize = sliceSize || 512;

//     var byteCharacters = atob(b64Data);
//     var byteArrays = [];

//     for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
//       var slice = byteCharacters.slice(offset, offset + sliceSize);

//       var byteNumbers = new Array(slice.length);
//       for (var i = 0; i < slice.length; i++) {
//         byteNumbers[i] = slice.charCodeAt(i);
//       }

//       var byteArray = new Uint8Array(byteNumbers);

//       byteArrays.push(byteArray);
//     }

//     var blob = new Blob(byteArrays, { type: contentType });
//     return blob;
//   };



//   public CSV(data) {
//     this.service.excelservice(data, this.menudetails, '').subscribe(res => {
//       console.log("Base645464", res);
//       if (res) {
//         let name;
//         this.convertBase64ToCSV(res, name);
//       }
//     });
//   }
//   convertBase64ToCSV = function (bssix, fname) {
//     var data = bssix;

//     var contentType = 'application/vnd.ms-excel';
//     var blob1 = this.b64toBlobCommoncsv(data, contentType);
//     var blobUrl1 = URL.createObjectURL(blob1);

//     //window.open(blobUrl1);
//     var downloadLink = document.createElement('a');
//     downloadLink.href = blobUrl1;
//     downloadLink.download = 'Report' + '.csv';
//     document.body.appendChild(downloadLink);
//     downloadLink.click();
//     document.body.removeChild(downloadLink);
//   };

//   b64toBlobCommoncsv = function (b64Data, contentType, sliceSize) {
//     contentType = contentType || '';
//     sliceSize = sliceSize || 512;

//     var byteCharacters = atob(b64Data);
//     var byteArrays = [];

//     for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
//       var slice = byteCharacters.slice(offset, offset + sliceSize);

//       var byteNumbers = new Array(slice.length);
//       for (var i = 0; i < slice.length; i++) {
//         byteNumbers[i] = slice.charCodeAt(i);
//       }

//       var byteArray = new Uint8Array(byteNumbers);

//       byteArrays.push(byteArray);
//     }

//     var blob = new Blob(byteArrays, { type: contentType });
//     return blob;
//   };

//   LoginDTO;
//   ReportviewID;
//   public pdf() {
//     let filterdata;
//     this.sourcerowcriteria$.subscribe(data => {
//       let finaldata = JSON.stringify(data.CriteriaDTO);
//       filterdata = finaldata
//       console.log("filterdata", data)
//     })
//     let configurationjasperdetail = localStorage.getItem("JasperDetailUrl");
//     // this.jsaperdetails$.subscribe((data: any) => {
//     //   console.log("dataa", data.serverDetails.domain)
//     //   configurationjasperdetail = data.JasperDetails.Server;
//     // })
//     this.LOGINDTO$.subscribe(dto => {
//       let Logindto = dto;
//       this.LoginDTO = JSON.stringify(Logindto);
//       console.log("this.LoginDTO", this.LoginDTO)
//     })
//     this.selectedview$.subscribe(data => {
//       console.log("dataview", data)
//       this.ReportviewID = data.Id
//     })
//     let DefaultReportFormatId;
//     this.reportformatselection$.subscribe(data => {
//       console.log("reportformatselection", data)
//       DefaultReportFormatId = data;
//     })
//     this.rowcerteria$.subscribe(res => {
//       if (res) {
//         let btoafilter = btoa(filterdata);
//         console.log("btoafilter", btoafilter)
//         let Reportname = res[0].ReportName
//         let ReportFilename = res[0].ReportFile
//         let weburl = res[0].WebServiceUriTemplate
//         let re = /FirstNumber=1/gi;
//         var newstr = weburl.replace(re, 'FirstNumber=-1');
//         let x = /MaxResult=10/gi
//         var Finalweburl = newstr.replace(x, 'MaxResult=-1');
//         let symbol = /&/gi
//         var Finalweburls = Finalweburl.replace(symbol, '@');
//         let WebServiceUriTemplate = Finalweburls
//         let MenuReportId = res[0].MenuReportId
//         console.log("Final", this.LoginDTO)
//         console.log("ReportFilename==", ReportFilename, "WebServiceUriTemplate==", WebServiceUriTemplate, "DefaultReportFormatId==", DefaultReportFormatId, "MenuReportId===", MenuReportId)
//         let Base64LoginDTO = btoa(this.LoginDTO) // string to Base64 Converter (btoa) || (atob) to convert Base64 to string
//         console.log("Base64LoginDTO", Base64LoginDTO);
//         let pdfdata = configurationjasperdetail + "/jasperserver/flow.html?_flowId=viewReportFlow&j_username=jasperadmin&j_password=jasperadmin&param=1&standAlone=true&_flowId=viewReportFlow&ParentFolderUri=Freports&reportUnit=" + ReportFilename + "&output=pdf&Criteria=" + btoafilter + "&JasperReportTitle=" + Reportname + "&Method=POST&URL=" + WebServiceUriTemplate + "&Login=" + Base64LoginDTO + "&ReportFormatId=" + DefaultReportFormatId + "&ReportId=" + MenuReportId + "&ReportViewId=" + this.ReportviewID + "&IsDynamicJasperRequired=1";
//         window.open(pdfdata, "_blank");
//       }
//       else {
//         const dialogRef = this.dialog.open(DialogBoxComponent, {
//           data: {
//             message: ['Please Select Any Report'],
//             heading: 'Error',
//             // button:'Ok'
//             // Add any other properties you want to pass
//           }
//         });
//       }
//     })
//     // const add = this.formComponent as any;
//     // var doc = new jsPDF('p', 'pt');

//     // add.columnData.forEach(element => {
//     //   var temp = element.headerName;
//     //   this.cc.push(temp);
//     // });

//     // add.rowData.forEach(element => {
//     //   var temp = [element.Code, element.Name];
//     //   this.rr.push(temp);
//     // });
//   }

//   public saveRecord() {
//     let flag = 1;
//     let data = this.formComponent as any
//     let conditioncheck = [null,undefined,'']
//     let validationkeys = Object.keys(this.Validation[0])
//     let alertdata = []
//     for(let formdata of Object.keys(data.form.value)){
//       if(validationkeys.includes(formdata)){
//         if(conditioncheck.includes(data.form.value[formdata])){
//           flag = 0;
//           alertdata.push(this.Validation[0][formdata]+" Should not be Empty")
//           // alert(this.Validation[0][formdata]+" Should not be Empty")
//         }
//       }
//     }
//     if(flag == 0){
//       const dialogRef = this.dialog.open(DialogBoxComponent, {
//         data: {
//           message: alertdata,
//           heading: 'Warning',
//           // button:'Ok'
//           // Add any other properties you want to pass
//         }
//       });
//     }
//     if (data.form.value && flag == 1) {
//       this.store.dispatch(new FormUnEditable);
//       let Value = data.form.value
//       let Keys = Object.keys(Value)
//       for (let i = 0; i < Keys.length; i++) {
//         if (typeof Value[Keys[i]] == 'object') {
//           let objectitem = ""
//           if (Value[Keys[i]] != null) {
//             for (let item of Value[Keys[i]]) {
//               objectitem = objectitem + item + ",";
//               data.form.value[Keys[i]] = objectitem;
//             }
//             data.form.value[Keys[i]] = data.form.value[Keys[i]].slice(0, -1);
//           }
//         }
//       }
//       let url = data.service.dbDataService.endPoint
//       let criteria = data.form.value
//       this.service.savedata(url, criteria).subscribe(res => {
//         if (res.Body) {
//           const dialogRef = this.dialog.open(DialogBoxComponent, {
//             data: {
//               message: res.Body,
//               heading: 'Success',
//               // button:'Ok'
//               // Add any other properties you want to pass
//             }
//           });
//           // alert(res.Body)
//         }
//         else {const dialogRef = this.dialog.open(DialogBoxComponent, {
//           data: {
//             message: res,
//             heading: 'Error',
//             // button:'Ok'
//             // Add any other properties you want to pass
//           }
//         });
//           // alert(res)
//         }
//         data.form.reset(this.FormReset)

//       })
//     }
//   }
//   public clearRecord() {
//     const data = this.formComponent as any;
//     data.form.reset(this.FormReset)
//   }
//   public editRecord() {
//     this.formvaldiation$.subscribe(res => {
//       this.Validation = res[0]
//       this.Validation = this.Validation.Validation
//       this.FormReset = res[1]
//       this.FormReset = this.FormReset.Reset
//       console.log("Validation:",this.Validation)
//       console.log("FormReset:",this.FormReset)
//     })
//     const data = this.formComponent as any;
//     this.edit$.pipe(take(1)).subscribe(newValue => {
//       if (!newValue) {
//         this.disableButton = false;
//         this.store.dispatch(new FormEditable);
//       } else {
//         this.disableButton = true;
//         this.store.dispatch(new FormUnEditable);
//       }
//     });
//     let id: number = JSON.parse(data.Listrowselectedindex);
//     this.movenextpreid = id

//   }
//   public deleteRecord() {
//     const data = this.formComponent as any;
//     let url = data.service.dbDataService.endPoint
//     let idfied = data.service.dbDataService.idField
//     let selectedid = data.service.dbDataService.selectid
//     this.service.deletedata(url, idfied, selectedid).subscribe(res => {
//       console.log("Delete Response:",res)
//       console.log("isDevMode():",isDevMode())
//       let delresponse;
//       if(isDevMode()){
//         delresponse = this.responsehandler(res)
//       } else {
//         delresponse = this.responsehandler(res.contents)
//       }
     
//       console.log("Delete delresponse:",delresponse)
//       if (delresponse.Body) {
//         const dialogRef = this.dialog.open(DialogBoxComponent, {
//           data: {
//             message: delresponse.Body,
//             heading: 'Success',
//             // button:'Ok'
//             // Add any other properties you want to pass
//           }
//         });
//         // alert(delresponse.Body)
//       }
//       else {
//         const dialogRef = this.dialog.open(DialogBoxComponent, {
//           data: {
//             message: delresponse,
//             heading: 'Error',
//             // button:'Ok'
//             // Add any other properties you want to pass
//           }
//         });
//         // alert(delresponse)
//       }

//       data.form.reset(this.FormReset)
//     })
//   }
//   responsehandler(encodeddata: any) {
//     const getresponse = JSON.stringify(encodeddata);
//     const responseModel = JSON.parse(getresponse);
//     let apiBody = responseModel.Body;
//     const bytes = utf8.decode(apiBody);
//     const decodedData = globalbase64js.decode(bytes);
//     const Responsebodydecoded = JSON.parse(globalpako.inflate(decodedData, { to: 'string' }));
//     const con = Responsebodydecoded.Body;
//     let converter = Responsebodydecoded;
//     try {
//       converter = JSON.parse(con)
//     } catch (error) {
//       converter = con
//     }
//     return converter;
//   }
//   public movePrev() {
//     this.movenextpreid = this.movenextpreid - 1
//     this.store.dispatch(new Movenextpreviousid(this.movenextpreid));
//     console.log("this.movenextpreid", this.movenextpreid)
//   }
//   public moveNext() {
//     this.movenextpreid = this.movenextpreid + 1
//     this.store.dispatch(new Movenextpreviousid(this.movenextpreid));
//     console.log("this.movenextpreid", this.movenextpreid)
//   }
//   public moveFirst() {
//     const first = this.formComponent.form as any;
//     first.moveFirst();
//   }

//   public moveLast() {
//     const Last = this.formComponent.form as any;
//     Last.moveLast();
//   }

//   aboutclick() {
//     const dialogRef = this.dialog.open(GbaboutComponent, { position: { right: '0' } });
//     dialogRef.afterClosed().subscribe(result => {
//       console.log(`Dialog result: ${result}`);
//     });
//     this.router.navigate(['/gbabout']);
//   }
//   gblogclick() {
//     const dialogRef = this.dialog.open(GblogComponent, { position: { right: '0' } });
//     dialogRef.afterClosed().subscribe(result => {
//       console.log(`Dialog result: ${result}`);
//     });
//     this.router.navigate(['/gblog']);
//   }

// }