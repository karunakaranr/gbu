import { Component, OnInit } from '@angular/core';
import { ReportState } from './../../../../../commonreport/datastore/commonreport.state';
import { configvalue } from './../../../../models/config.model';
import { Configid } from './../../../../../commonreport/datastore/commonreport.action';
import { Observable } from 'rxjs';
import { CriteriaConfigArray } from './../../../../../commonreport/model/report.model'
import { FormBuilder, FormGroup } from '@angular/forms';
import { Select, Store } from '@ngxs/store';
import { LayoutState } from './../../../../store/layout.state';
import { ConfigService } from './../../../../services/config/config.service';
import { Title } from './../../../../store/layout.actions'



@Component({
  selector: 'app-gbformselector',
  templateUrl: './gbformselector.component.html',
  styleUrls: ['./gbformselector.component.scss']
})
export class GbformselectorComponent implements OnInit {
  criterias: FormGroup;
  criteriacofig: CriteriaConfigArray;
  configvalues: configvalue;
  public viewmode;
  public length;
  @Select(LayoutState.Visibilebuttons) visiblebuttons$: Observable<string>;
  constructor(public fb: FormBuilder, public dataService: ConfigService, private store: Store) {
    this.criterias = this.fb.group({
      AttributesCriteriaList: this.fb.array([])
    }) as FormGroup;
  }
  @Select(ReportState.CriteriaConfigArray) rowcerteria$: Observable<any>;
  @Select(LayoutState.ThemeClass) themeClass$: Observable<string>;



  ngOnInit(): void {
    this.visiblebuttons$.subscribe(res => {
      if (res) {
        this.viewmode = res;
      }
    })
    this.rowcerteria$.subscribe(res => {
      if (res) {
        this.criteriacofig = res[0].ReportCriteriaArray;
        let data = {
          "SectionCriteriaList": [
            {
              "SectionId": 0,
              "AttributesCriteriaList": [
                {
                  "FieldName": "CriteriaUserId",
                  "OperationType": this.criteriacofig[0].CriteriaAttributeDefaultOperation,
                  "FieldValue": "-1499998941",
                  "InArray": null,
                  "JoinType": 2
                },
                {
                  "FieldName": "CriteriaConfigType",
                  "OperationType": this.criteriacofig[0].CriteriaAttributeDefaultOperation,
                  "FieldValue": this.criteriacofig[0].CriteriaAttributeType,
                  "InArray": null,
                  "JoinType": 2
                },
                {
                  "FieldName": "CriteriaServiceId",
                  "OperationType": this.criteriacofig[0].CriteriaAttributeDefaultOperation,
                  "FieldValue": this.criteriacofig[0].WebServiceId,
                  "InArray": null,
                  "JoinType": 2
                },
                {
                  "FieldName": "CriteriaMenuId",
                  "OperationType": this.criteriacofig[0].CriteriaAttributeDefaultOperation,
                  "FieldValue": this.criteriacofig[0].MenuId,
                  "InArray": null,
                  "JoinType": 2
                },
                {
                  "FieldName": "CriteriaOUId",
                  "OperationType": this.criteriacofig[0].CriteriaAttributeDefaultOperation,
                  "FieldValue": this.criteriacofig[0].CriteriaAttributeContextValueFieldName,
                  "InArray": null,
                  "JoinType": 2
                },
                {
                  "FieldName": "Name",
                  "OperationType": 2,
                  "FieldValue": "",
                  "InArray": null,
                  "JoinType": 0
                }
              ],
              "OperationType": 0
            }
          ]
        }
        this.dataService.CriteriaConfig(data, '').subscribe(res => {
          console.log("response of the Config service", res)
          this.length=res.length;
          console.log("Length:",this.length)
          // if (res.length === 0) {
          //   document.getElementById("menus").style.display = "none";
          // } else {
          //   document.getElementById("menus").style.display = "block";
          // }
          this.configvalues = res;
        });
      }
    })

  }

  public selecteddata(data) {
    let id = data.Id
    let name = data.View;
    this.store.dispatch(new Title(name))
    this.store.dispatch(new Configid(id))
  }
}